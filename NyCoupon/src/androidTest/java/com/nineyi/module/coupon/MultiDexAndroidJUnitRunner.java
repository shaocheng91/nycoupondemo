package com.nineyi.module.coupon;

import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.test.runner.AndroidJUnitRunner;

public class MultiDexAndroidJUnitRunner extends AndroidJUnitRunner {
    @Override
    public void onCreate(Bundle arguments) {
        //To make it work on MultiDex environment.
        //https://plus.google.com/+OleksandrKucherenko/posts/i7qZdVEy3Ue
        MultiDex.install(getTargetContext());

        super.onCreate(arguments);
    }
}
