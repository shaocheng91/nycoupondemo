package com.nineyi.module.coupon.ui.utils;

import android.content.Context;
import android.content.res.Resources;

import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.ui.utils.stringdecorate.BoldStringDecorate;
import com.nineyi.module.coupon.ui.utils.stringdecorate.EndOfString;
import com.nineyi.module.coupon.ui.utils.stringdecorate.EndOfStringDecorate;
import com.nineyi.module.coupon.ui.utils.stringdecorate.HeadConcateStringDecorate;
import com.nineyi.module.coupon.ui.utils.stringdecorate.ReSizeStringDecorate;

/**
 * Created by shaocheng on 2017/10/5.
 */

public class StringUtils {
    private static int MAX_ECOUPON_COUNT = 1000;

    public static CharSequence getFormattedPrice(Context context, double price) {
        EndOfStringDecorate large;

        Resources res = context.getResources();
        if (price >= MAX_ECOUPON_COUNT) { //prevent price ui overlap to other ui cause by price too many digit
            large = new ReSizeStringDecorate(new EndOfString(res.getString(R.string.taiwan_price_format, price)), 45);
        } else {
            large = new ReSizeStringDecorate(new EndOfString(res.getString(R.string.taiwan_price_format, price)), 60);
        }

        return large.getCharSequence();
    }
}
