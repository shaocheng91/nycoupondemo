package com.nineyi.module.coupon.ui.use.online.viewholder;

import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseUnusableView;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUnuseable;

import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseUnusableViewHolder extends CouponOnlineUseViewHolder<CouponOnlineUseItemCouponUnuseable> {

    private final CouponOnlineUseUnusableView mView;

    public CouponOnlineUseUnusableViewHolder(View itemView) {
        super(itemView);

        this.mView = (CouponOnlineUseUnusableView) itemView;
    }

    @Override
    public void bindData(CouponOnlineUseItemCouponUnuseable data, int position) {
        mView.showCoupon(data.getCoupon(), false, position);
    }
}
