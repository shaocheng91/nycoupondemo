package com.nineyi.module.coupon.ui.my;

import com.nineyi.module.base.navigator.SchemeRouter;
import com.nineyi.module.base.provider.tint.TintProvider;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.base.retrofit.appcompat.NyActionBarActivity;
import com.nineyi.module.base.ui.NineYiColor;
import com.nineyi.module.coupon.CouponComponent;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.LoginManager;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.ui.keyin.KeyInPresenter;
import com.nineyi.module.coupon.ui.keyin.KeyInView;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


/**
 * Created by ReeceCheng on 2017/10/16.
 */

public class MyCouponActivity extends NyActionBarActivity {

    private final CompositeDisposableHelper mCompositeDisposableHelper = new CompositeDisposableHelper();
    private MyCouponPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CouponComponent couponComponent = CouponComponent.get();

        CouponManager couponManager = couponComponent.couponManager();
        MsgManager msgManager = couponComponent.msgManager();
        LoginManager loginManager = couponComponent.loginManager();

        setContentView(R.layout.my_coupon_layout);
        initialToolbar();

        LinearLayout container = (LinearLayout) findViewById(R.id.my_coupon_layout_container);

        KeyInView keyInView = new KeyInView(this);
        keyInView.setMsgManager(msgManager);

        KeyInPresenter keyInPresenter = new KeyInPresenter(couponManager, loginManager, keyInView, mCompositeDisposableHelper, false);
        keyInView.setPresenter(keyInPresenter);

        MyCouponView view = new MyCouponView(this);
        mPresenter = new MyCouponPresenter(couponManager, view, mCompositeDisposableHelper);
        view.setPresenter(mPresenter);

        View moreView = getLayoutInflater().inflate(R.layout.my_coupon_more_layout, null);
        moreView.findViewById(R.id.my_coupon_more_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SchemeRouter.navToCouponList(MyCouponActivity.this);
            }
        });

        container.addView(keyInView);
        container.addView(view, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        container.addView(moreView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void initialToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            setActionBarTitle(getString(R.string.my_coupon_title));
            setHomeAsUp(toolbar);

            toolbar.setNavigationIcon(TintProvider.getInstance()
                    .getTintDrawable(getResources().getDrawable(R.drawable.btn_navi_back),
                            NineYiColor.getGlobalNaviIconColor(), NineYiColor.getGlobalNaviIconColor()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getCouponList(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        mCompositeDisposableHelper.clear();
    }

}
