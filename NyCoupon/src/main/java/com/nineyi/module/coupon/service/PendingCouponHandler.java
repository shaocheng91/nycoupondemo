package com.nineyi.module.coupon.service;

import com.nineyi.module.coupon.ui.list.CouponListPresenter;


/**
 * Created by ReeceCheng on 2017/10/26.
 */

public class PendingCouponHandler {
    CouponListPresenter.OnTakeCouponSuccessCallback mCallback;
    final CouponSharePreferenceHelper couponSharePreferenceHelper;

    public PendingCouponHandler(CouponSharePreferenceHelper helper) {
        couponSharePreferenceHelper = helper;
    }

    public void clear() {
        couponSharePreferenceHelper.removePendingCouponId();
        couponSharePreferenceHelper.removePendingCouponIsFirstDownload();
    }

    public int loadPendingCouponId() {
        return couponSharePreferenceHelper.loadPendingCouponId();
    }

    public boolean loadPendingCouponIsFirstDownload() {
        return couponSharePreferenceHelper.loadPendingCouponIsFirstDownload();
    }

    public void savePendingCouponId(int couponId) {
        couponSharePreferenceHelper.savePendingCouponId(couponId);
    }

    public void savePendingCouponIsFirstDownload(boolean isFirstDownload) {
        couponSharePreferenceHelper.savePendingCouponIsFirstDownload(isFirstDownload);
    }

    public void removeCallback() {
        mCallback = null;
    }

    public boolean hasOnTakeCouponSuccessCallback() {
        return mCallback != null;
    }

    public CouponListPresenter.OnTakeCouponSuccessCallback getCallback() {
        return mCallback;
    }

    public void setCallback(CouponListPresenter.OnTakeCouponSuccessCallback callback) {
        mCallback = callback;
    }

    public boolean hasPendingCouponToCollect() {
        final int couponId = loadPendingCouponId();
        return couponId != CouponSharePreferenceHelper.NO_PENDING_COUPON;
    }
}