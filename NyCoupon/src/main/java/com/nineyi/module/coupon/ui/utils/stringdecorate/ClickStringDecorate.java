package com.nineyi.module.coupon.ui.utils.stringdecorate;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;


/**
 * Created by tedliang on 2016/4/14.
 */
public class ClickStringDecorate extends StringDecorate {

    private ClickableSpan mClickableSpan;
    public ClickStringDecorate(EndOfStringDecorate nextStringDecorate, ClickableSpan clickableSpan) {
        super(nextStringDecorate);
        mClickableSpan = clickableSpan;
    }

    @Override
    public CharSequence getCharSequence() {
        SpannableString ss = new SpannableString(mNextStringDecorate.getCharSequence());
        ss.setSpan(mClickableSpan, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
