package com.nineyi.module.coupon.service;

/**
 * Created by shaocheng on 2017/10/5.
 */

public interface MigrationManager {
    long getAppRegisterDateMilliseconds();
}
