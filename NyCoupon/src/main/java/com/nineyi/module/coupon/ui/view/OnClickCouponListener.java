package com.nineyi.module.coupon.ui.view;

import com.nineyi.module.coupon.model.Coupon;

/**
 * Created by shaocheng on 2017/10/19.
 */

public interface OnClickCouponListener {
    void onClickCoupon(Coupon coupon);
}
