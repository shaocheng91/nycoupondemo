package com.nineyi.module.coupon.ui.utils.stringdecorate;

public class EndOfString implements EndOfStringDecorate {

    protected String mCharacter;

    public EndOfString(String str) {
        mCharacter = str;
    }

    @Override
    public CharSequence getCharSequence() {
        return mCharacter;
    }
}