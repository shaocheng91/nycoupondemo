package com.nineyi.module.coupon.ui.keyin;

import com.nineyi.module.coupon.ui.BaseView;

/**
 * Created by shaocheng on 2017/9/22.
 */

public interface KeyInContract {
    interface View extends BaseView<Presenter> {

        void showTakeCouponError(String message);

        void showTakeCouponSuccess();

        void showNetworkError();

        void showTakingCoupon();

        void hideKeyboard();

        void showWarning(boolean show);
    }
    interface Presenter {
        void takeCoupon(String code);
    }
}
