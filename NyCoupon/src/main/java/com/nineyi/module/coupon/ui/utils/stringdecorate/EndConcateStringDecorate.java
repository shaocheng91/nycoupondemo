package com.nineyi.module.coupon.ui.utils.stringdecorate;

import android.text.SpannableString;
import android.text.TextUtils;


public class EndConcateStringDecorate extends StringDecorate {

    public EndConcateStringDecorate(String character, EndOfStringDecorate specialRoom) {
        super(specialRoom);
        this.mCharacter = character;
    }

    @Override
    public CharSequence getCharSequence() {
        SpannableString ss = new SpannableString(mCharacter);
        return TextUtils.concat(mNextStringDecorate.getCharSequence(), ss);
    }

}