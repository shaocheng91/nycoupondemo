package com.nineyi.module.coupon.ui.use.offline;

import com.nineyi.data.model.ecoupon.NyBarCode;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.CouponOffline;
import com.nineyi.module.coupon.ui.utils.CouponBarcodeGenerator;
import com.nineyi.module.coupon.ui.utils.CouponBarcodeHelper;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * Created by ReeceCheng on 2017/11/3.
 */

public class CouponOfflineUseView extends RelativeLayout implements CouponOfflineUseContract.View {

    private static final int MEMBER = 0;
    private static final int COUPON1 = 1;
    private static final int COUPON2 = 2;
    private static final int COUPON3 = 3;

    private CouponOfflineUseContract.Presenter mPresenter;

    ProgressBar mProgressBar;

    LinearLayout mMemberBarcodeLayout;
    ImageView mMemberBarcode;
    TextView mMemberCode;

    LinearLayout mCouponBarcodeLayout;
    TextView mCouponBarcodeTitle;
    ImageView mCouponBarcodeBarcode1;
    ImageView mCouponBarcodeBarcode2;
    ImageView mCouponBarcodeBarcode3;
    TextView mCouponBarcodeCode1;
    TextView mCouponBarcodeCode2;
    TextView mCouponBarcodeCode3;

    public CouponOfflineUseView(Context context) {
        super(context);
        init(context);
    }

    public CouponOfflineUseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponOfflineUseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public CouponOfflineUseView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.coupon_offline_view, this);

        mProgressBar = (ProgressBar) findViewById(R.id.coupon_offline_progressbar);

        mMemberBarcodeLayout = (LinearLayout) findViewById(R.id.coupon_offline_member_barcode_layout);
        mMemberBarcode = (ImageView) findViewById(R.id.coupon_offline_member_barcode);
        mMemberCode = (TextView) findViewById(R.id.coupon_offline_member_code);

        mCouponBarcodeLayout = (LinearLayout) findViewById(R.id.coupon_offline_coupon_barcode_layout);
        mCouponBarcodeTitle = (TextView) findViewById(R.id.coupon_offline_coupon_barcode_title);
        mCouponBarcodeBarcode1 = (ImageView) findViewById(R.id.coupon_offline_coupon_barcode_barcode1);
        mCouponBarcodeBarcode2 = (ImageView) findViewById(R.id.coupon_offline_coupon_barcode_barcode2);
        mCouponBarcodeBarcode3 = (ImageView) findViewById(R.id.coupon_offline_coupon_barcode_barcode3);
        mCouponBarcodeCode1 = (TextView) findViewById(R.id.coupon_offline_coupon_barcode_code1);
        mCouponBarcodeCode2 = (TextView) findViewById(R.id.coupon_offline_coupon_barcode_code2);
        mCouponBarcodeCode3 = (TextView) findViewById(R.id.coupon_offline_coupon_barcode_code3);
    }

    @Override
    public void setPresenter(CouponOfflineUseContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showOfflineUse(CouponOffline offline) {
        CouponBarcodeHelper helper = new CouponBarcodeHelper();
        CouponBarcodeGenerator.GenFinishedCallback genCallback = new CouponBarcodeGenerator.GenFinishedCallback() {
            @Override
            public void onGenFinished(Bitmap barcode, String code, int id) {
                showBarcodeWhenGenFinished(barcode, code, id);
            }

            @Override
            public void onGenError(String code, int id) {
                showBarcodeWhenGenError(code, id);
            }
        };

        NyBarCode member = offline.getVipMemberBarcode();
        if(member != null && checkNotNullOrEmpty(member.BarCode) && checkNotNullOrEmpty(member.BarCodeTypeDef)) {
            helper.genBarcode(getContext(), member.BarCode, member.BarCodeTypeDef, MEMBER, genCallback);
        }

        if(offline.isUseCustomCode()) {
            NyBarCode custom1 = offline.getCouponCustomCode1();
            NyBarCode custom2 = offline.getCouponCustomCode2();
            NyBarCode custom3 = offline.getCouponCustomCode3();

            if (custom1 != null && checkNotNullOrEmpty(custom1.BarCode) && checkNotNullOrEmpty(custom1.BarCodeTypeDef)) {
                helper.genBarcode(getContext(), custom1.BarCode, custom1.BarCodeTypeDef, COUPON1, genCallback);
            }

            if (custom2 != null && checkNotNullOrEmpty(custom2.BarCode) && checkNotNullOrEmpty(custom2.BarCodeTypeDef)) {
                helper.genBarcode(getContext(), custom2.BarCode, custom2.BarCodeTypeDef, COUPON2, genCallback);
            }

            if (custom3 != null && checkNotNullOrEmpty(custom3.BarCode) && checkNotNullOrEmpty(custom3.BarCodeTypeDef)) {
                helper.genBarcode(getContext(), custom3.BarCode, custom3.BarCodeTypeDef, COUPON3, genCallback);
            }
        } else {
            NyBarCode coupon = offline.getCouponBarcode();
            if (coupon != null && checkNotNullOrEmpty(coupon.BarCode) && checkNotNullOrEmpty(coupon.BarCodeTypeDef)) {
                helper.genBarcode(getContext(), coupon.BarCode, coupon.BarCodeTypeDef, COUPON1, genCallback);
            }
        }
    }

    private boolean checkNotNullOrEmpty(String s) {
        return s != null && !"".equals(s);
    }

    private void showBarcodeWhenGenFinished(Bitmap barcode, String code, int id) {
        mProgressBar.setVisibility(GONE);

        switch (id) {
            case MEMBER:
                if(barcode != null) {
                    mMemberBarcode.setImageBitmap(barcode);
                }

                mMemberCode.setText(code);

                mMemberBarcodeLayout.setVisibility(VISIBLE);
                break;
            case COUPON1:
                if(barcode != null) {
                    mCouponBarcodeBarcode1.setImageBitmap(barcode);
                }

                mCouponBarcodeCode1.setText(code);

                mCouponBarcodeTitle.setVisibility(VISIBLE);
                mCouponBarcodeLayout.setVisibility(VISIBLE);
                mCouponBarcodeBarcode1.setVisibility(VISIBLE);
                mCouponBarcodeCode1.setVisibility(VISIBLE);
                break;
            case COUPON2:
                if(barcode != null) {
                    mCouponBarcodeBarcode2.setImageBitmap(barcode);
                }

                mCouponBarcodeCode2.setText(code);

                mCouponBarcodeTitle.setVisibility(VISIBLE);
                mCouponBarcodeLayout.setVisibility(VISIBLE);
                mCouponBarcodeBarcode2.setVisibility(VISIBLE);
                mCouponBarcodeCode2.setVisibility(VISIBLE);
                break;
            case COUPON3:
                if(barcode != null) {
                    mCouponBarcodeBarcode3.setImageBitmap(barcode);
                }

                mCouponBarcodeCode3.setText(code);

                mCouponBarcodeTitle.setVisibility(VISIBLE);
                mCouponBarcodeLayout.setVisibility(VISIBLE);
                mCouponBarcodeBarcode3.setVisibility(VISIBLE);
                mCouponBarcodeCode3.setVisibility(VISIBLE);
                break;
        }
    }

    private void showBarcodeWhenGenError(String code, int id) {
        showBarcodeWhenGenFinished(null, code, id);
    }
}
