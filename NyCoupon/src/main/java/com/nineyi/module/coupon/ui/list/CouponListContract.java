package com.nineyi.module.coupon.ui.list;

import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.ui.BaseView;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;

import java.util.List;

/**
 * Created by shaocheng on 2017/9/22.
 */

public interface CouponListContract {
    interface View extends BaseView<Presenter> {
        void showCouponList(List<CouponListItem> coupons);
        void showCollectCouponSuccess();
        void showCollectCouponError(int titleStringId);
        void showCollectCouponError(int titleStringId, String message);
        void showTakingCoupon();
        void onRefresh();
        void showLoading();
        void showError();
        void showEmpty();
    }

    interface Presenter {
        void refreshCouponList();
        void collectPendingCouponIfExists();
        void loadCouponList(boolean forceReload);
        void collectCoupon(Coupon coupon, CouponListPresenter.OnTakeCouponSuccessCallback callback);
        boolean isLoggedIn();
    }
}
