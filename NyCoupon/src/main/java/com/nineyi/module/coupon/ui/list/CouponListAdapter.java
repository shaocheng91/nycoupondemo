package com.nineyi.module.coupon.ui.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nineyi.module.base.navigator.SchemeRouter;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.ui.detail.DetailContract;
import com.nineyi.module.coupon.ui.list.viewholder.CouponListItemCouponAvailableViewHolder;
import com.nineyi.module.coupon.ui.list.viewholder.CouponListItemCouponViewHolder;
import com.nineyi.module.coupon.ui.list.viewholder.CouponListItemEmptyViewHolder;
import com.nineyi.module.coupon.ui.list.viewholder.CouponListItemViewHolder;
import com.nineyi.module.coupon.ui.list.viewholder.CouponListItemSeparatorViewHolder;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemCoupon;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemCouponAvailable;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemSeparator;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;
import com.nineyi.module.coupon.ui.view.CouponTicketAvailable;
import com.nineyi.module.coupon.ui.view.CouponTicketCollected;
import com.nineyi.module.coupon.ui.view.CouponTicketInvalid;
import com.nineyi.module.coupon.ui.view.OnClickCouponListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shaocheng on 2017/10/6.
 */

public class CouponListAdapter extends RecyclerView.Adapter<CouponListItemViewHolder> {

    public static final int VIEW_TYPE_SEPARATOR = 0;
    public static final int VIEW_TYPE_AVAILABLE = 1;
    public static final int VIEW_TYPE_COLLECTED = 2;
    public static final int VIEW_TYPE_INVALID = 3;
    public static final int VIEW_TYPE_EMPTY = 4;

    private final Context context;
    private final CouponListContract.Presenter mPresenter;
    private final LayoutInflater layoutInflater;
    private final CountdownManager mCountdownManager;
    private List<CouponListItem> data = new ArrayList<>();
    private NavManager mNavManager;


    public CouponListAdapter(Context context, CouponListContract.Presenter presenter,
                             CountdownManager countdownManager, NavManager navManager) {
        this.context = context;
        this.mPresenter = presenter;
        this.mCountdownManager = countdownManager;
        this.mNavManager = navManager;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public CouponListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_SEPARATOR:
                View separator = layoutInflater.inflate(R.layout.coupon_list_item_separator, parent, false);
                return new CouponListItemSeparatorViewHolder(separator);
            case VIEW_TYPE_AVAILABLE:
                CouponTicketAvailable couponTicketAvailable = new CouponTicketAvailable(context);
                couponTicketAvailable.setPresenter(mPresenter);

                couponTicketAvailable.setOnClickCouponListener(new OnClickCouponListener() {
                    @Override
                    public void onClickCoupon(Coupon coupon) {
                        SchemeRouter.navToCouponDetail(context, coupon.getId(), DetailContract.ARG_FROM_OTHER);
                    }
                });

                couponTicketAvailable.setCountdownManager(mCountdownManager);

                return new CouponListItemCouponAvailableViewHolder(couponTicketAvailable);
            case VIEW_TYPE_COLLECTED:
                CouponTicketCollected couponTicketCollected = new CouponTicketCollected(context);
                couponTicketCollected.setCountdownManager(mCountdownManager);
                couponTicketCollected.setOnClickCouponListener(new OnClickCouponListener() {
                    @Override
                    public void onClickCoupon(Coupon coupon) {
                        SchemeRouter.navToCouponDetail(context, coupon.getId(), DetailContract.ARG_FROM_OTHER);
                    }
                });

                return new CouponListItemCouponViewHolder(couponTicketCollected);
            case VIEW_TYPE_INVALID:
                CouponTicketInvalid couponTicketInvalid = new CouponTicketInvalid(context);
                couponTicketInvalid.setOnClickCouponListener(new OnClickCouponListener() {
                    @Override
                    public void onClickCoupon(Coupon coupon) {
                        SchemeRouter.navToCouponDetail(context, coupon.getId(), DetailContract.ARG_FROM_OTHER);
                    }
                });

                return new CouponListItemCouponViewHolder(couponTicketInvalid);
            case VIEW_TYPE_EMPTY:
                View empty = layoutInflater.inflate(R.layout.coupon_list_item_empty, parent, false);
                return new CouponListItemEmptyViewHolder(empty);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(CouponListItemViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_SEPARATOR:
                CouponListItemSeparator separator = (CouponListItemSeparator) data.get(position);
                ((CouponListItemSeparatorViewHolder) holder).mTitle.setText(separator.getTitle());
                break;
            case VIEW_TYPE_AVAILABLE:
                CouponListItemCouponAvailable available = ((CouponListItemCouponAvailable) data.get(position));

                ((CouponListItemCouponAvailableViewHolder) holder)
                        .mTicketAvailable.showCoupon(available.getCoupon(), available.isCollecting(), available.isCollected());
                break;
            case VIEW_TYPE_COLLECTED:
            case VIEW_TYPE_INVALID:
                CouponListItemCoupon item = ((CouponListItemCoupon) data.get(position));
                ((CouponListItemCouponViewHolder) holder).mTicket.showCoupon(item.getCoupon());
                break;
            case VIEW_TYPE_EMPTY:
            default:
        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<CouponListItem> data) {
        this.data = data;
    }

    public void clear() {
        mCountdownManager.clear();
        data.clear();
        notifyDataSetChanged();
    }
}
