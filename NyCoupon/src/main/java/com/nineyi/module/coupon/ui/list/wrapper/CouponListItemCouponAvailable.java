package com.nineyi.module.coupon.ui.list.wrapper;

import com.nineyi.module.coupon.model.Coupon;

/**
 * Created by shaocheng on 2017/10/6.
 */
public class CouponListItemCouponAvailable extends CouponListItemCoupon {
    private boolean isCollected;
    private boolean isCollecting;

    public CouponListItemCouponAvailable(int viewType, Coupon coupon) {
        super(viewType, coupon);
        this.isCollected = false;
        this.isCollecting = false;
    }

    public boolean isCollected() {
        return isCollected;
    }

    public void setCollected(boolean collected) {
        isCollected = collected;
    }

    public boolean isCollecting() {
        return isCollecting;
    }

    public void setCollecting(boolean b) {
        this.isCollecting = b;
    }
}
