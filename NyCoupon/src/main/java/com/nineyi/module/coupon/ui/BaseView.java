package com.nineyi.module.coupon.ui;

/**
 * Created by shaocheng on 2017/9/28.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
