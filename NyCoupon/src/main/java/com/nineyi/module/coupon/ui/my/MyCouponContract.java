package com.nineyi.module.coupon.ui.my;

import com.nineyi.module.coupon.ui.BaseView;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;

import java.util.List;


/**
 * Created by ReeceCheng on 2017/10/31.
 */

public class MyCouponContract {
    interface View extends BaseView<MyCouponContract.Presenter> {
        void showCouponList(List<CouponListItem> coupons);
        void showEmpty();
        void showError();
    }

    interface Presenter {
        void getCouponList(boolean useCache);
    }
}
