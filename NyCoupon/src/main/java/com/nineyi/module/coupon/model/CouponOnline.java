package com.nineyi.module.coupon.model;

import com.nineyi.data.model.gson.NineyiDate;


/**
 * Created by ReeceCheng on 2017/10/20.
 */

public class CouponOnline {

    private String code;
    private double discountPrice;
    private String typeDef;
    private NineyiDate usingEndDateTime;
    private NineyiDate usingStartDateTime;
    private int id;
    private int shopId;
    private int detailId;
    private double eCouponMaxDiscountLimit;
    private double eCouponUsingMinPrice;
    private boolean isAchieveUsingMinPrice;

    private CouponOnline(Builder builder) {
        this.code = builder.code;
        this.discountPrice = builder.discountPrice;
        this.typeDef = builder.typeDef;
        this.usingEndDateTime = builder.usingEndDateTime;
        this.usingStartDateTime = builder.usingStartDateTime;
        this.id = builder.id;
        this.shopId = builder.shopId;
        this.detailId = builder.detailId;
        this.eCouponMaxDiscountLimit = builder.eCouponMaxDiscountLimit;
        this.eCouponUsingMinPrice = builder.eCouponUsingMinPrice;
        this.isAchieveUsingMinPrice = builder.isAchieveUsingMinPrice;
    }

    public String getCode() {
        return code;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public String getTypeDef() {
        return typeDef;
    }

    public NineyiDate getUsingEndDateTime() {
        return usingEndDateTime;
    }

    public NineyiDate getUsingStartDateTime() {
        return usingStartDateTime;
    }

    public int getId() {
        return id;
    }

    public int getShopId() {
        return shopId;
    }

    public int getDetailId() {
        return detailId;
    }

    public double getECouponMaxDiscountLimit() {
        return eCouponMaxDiscountLimit;
    }

    public double getECouponUsingMinPrice() {
        return eCouponUsingMinPrice;
    }

    public boolean isAchieveUsingMinPrice() {
        return isAchieveUsingMinPrice;
    }

    public static final class Builder {
        private String code;
        private double discountPrice;
        private String typeDef;
        private NineyiDate usingEndDateTime;
        private NineyiDate usingStartDateTime;
        private int id;
        private int shopId;
        private int detailId;
        private double eCouponMaxDiscountLimit;
        private double eCouponUsingMinPrice;
        private boolean isAchieveUsingMinPrice;

        public Builder setCode(String code) {
            this.code = code;
            return this;
        }

        public Builder setDiscountPrice(double discountPrice) {
            this.discountPrice = discountPrice;
            return this;
        }

        public Builder setTypeDef(String typeDef) {
            this.typeDef = typeDef;
            return this;
        }

        public Builder setUsingEndDateTime(NineyiDate usingEndDateTime) {
            this.usingEndDateTime = usingEndDateTime;
            return this;
        }

        public Builder setUsingStartDateTime(NineyiDate usingStartDateTime) {
            this.usingStartDateTime = usingStartDateTime;
            return this;
        }

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setShopId(int shopId) {
            this.shopId = shopId;
            return this;
        }

        public Builder setDetailId(int detailId) {
            this.detailId = detailId;
            return this;
        }

        public Builder setECouponMaxDiscountLimit(double eCouponMaxDiscountLimit) {
            this.eCouponMaxDiscountLimit = eCouponMaxDiscountLimit;
            return this;
        }

        public Builder setECouponUsingMinPrice(double eCouponUsingMinPrice) {
            this.eCouponUsingMinPrice = eCouponUsingMinPrice;
            return this;
        }

        public Builder setIsAchieveUsingMinPrice(boolean isAchieveUsingMinPrice) {
            this.isAchieveUsingMinPrice = isAchieveUsingMinPrice;
            return this;
        }

        public CouponOnline build() {
            return new CouponOnline(this);
        }
    }

}
