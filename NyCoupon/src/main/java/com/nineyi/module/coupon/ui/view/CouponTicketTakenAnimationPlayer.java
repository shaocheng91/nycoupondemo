package com.nineyi.module.coupon.ui.view;

import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;

import static android.view.View.ALPHA;
import static android.view.View.ROTATION;
import static android.view.View.TRANSLATION_X;
import static android.view.View.TRANSLATION_Y;


/**
 * Created by ReeceCheng on 2017/10/25.
 */

class CouponTicketTakenAnimationPlayer {

    private AnimatorSet mAnimatorSet;
    private ObjectAnimator mLoadingAnimator;

    /**
     * View 往右飛出螢幕外(translationX)、並且淡出(alpha)
     *
     * @param context
     * @param countdown
     * @param countdownTitle
     * @param endTime
     * @param takeButton
     * @param loading
     * @param animatorListenerAdapter
     */
    void action1(final Context context, final View countdown, final View countdownTitle, final View endTime, final View takeButton, final View loading, AnimatorListenerAdapter animatorListenerAdapter) {
        int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        final float endTranslation = screenWidth - takeButton.getX();

        PropertyValuesHolder translationX = PropertyValuesHolder.ofFloat(TRANSLATION_X, 0, endTranslation);
        PropertyValuesHolder alpha = PropertyValuesHolder.ofFloat(ALPHA, 0.5f, 0f);

        ObjectAnimator countdownAnimator = ObjectAnimator.ofPropertyValuesHolder(countdown, translationX, alpha);
        ObjectAnimator countdownTitleAnimator = ObjectAnimator.ofPropertyValuesHolder(countdownTitle, translationX, alpha);
        ObjectAnimator endTimeAnimator = ObjectAnimator.ofPropertyValuesHolder(endTime, translationX, alpha);
        ObjectAnimator takeButtonAnimator = ObjectAnimator.ofPropertyValuesHolder(takeButton, translationX, alpha);
        ObjectAnimator loadingAnimator = ObjectAnimator.ofPropertyValuesHolder(loading, translationX, alpha);

        setupAnimation(countdownAnimator, new DecelerateInterpolator(),200);
        setupAnimation(countdownTitleAnimator, new DecelerateInterpolator(),200);
        setupAnimation(endTimeAnimator, new DecelerateInterpolator(),200);
        setupAnimation(takeButtonAnimator, new DecelerateInterpolator(),200);
        setupAnimation(loadingAnimator, new DecelerateInterpolator(),200);

        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(endTimeAnimator, takeButtonAnimator, countdownAnimator, countdownTitleAnimator, loadingAnimator);
        mAnimatorSet.addListener(animatorListenerAdapter);
        mAnimatorSet.start();
    }

    /**
     * View 由下往上推出(translationY)、並且淡入(alpha)
     *
     * @param countdown
     * @param countdownTitle
     * @param endTime
     * @param taken
     * @param shouldShowUseCountDown
     */
    void action2(final View countdown, final View countdownTitle, final View endTime, final View taken, boolean shouldShowUseCountDown) {
        PropertyValuesHolder translationX = PropertyValuesHolder.ofFloat(TRANSLATION_Y, 40, 0);
        PropertyValuesHolder alpha = PropertyValuesHolder.ofFloat(ALPHA, 0f, 1f);

        ObjectAnimator takeAnimator = ObjectAnimator.ofPropertyValuesHolder(taken, translationX, alpha);
        setupAnimation(takeAnimator, new DecelerateInterpolator(),300);

        if(shouldShowUseCountDown) {
            ObjectAnimator countdownAnimator = ObjectAnimator.ofPropertyValuesHolder(countdown, translationX, alpha);
            ObjectAnimator countdownTitleAnimator = ObjectAnimator.ofPropertyValuesHolder(countdownTitle, translationX, alpha);

            setupAnimation(countdownAnimator, new DecelerateInterpolator(),300);
            setupAnimation(countdownTitleAnimator, new DecelerateInterpolator(),300);

            mAnimatorSet = new AnimatorSet();
            mAnimatorSet.playTogether(countdownAnimator, countdownTitleAnimator, takeAnimator);
            mAnimatorSet.start();

        } else {
            ObjectAnimator endTimeAnimator = ObjectAnimator.ofPropertyValuesHolder(endTime, translationX, alpha);
            setupAnimation(endTimeAnimator, new DecelerateInterpolator(),300);

            mAnimatorSet = new AnimatorSet();
            mAnimatorSet.playTogether(endTimeAnimator, takeAnimator);
            mAnimatorSet.start();
        }
    }

    /**
     * 旋轉 loading 圖
     *
     * @param takeButton
     * @param loading
     */
    void loading(Button takeButton, View loading) {
        takeButton.setText("");
        loading.setVisibility(View.VISIBLE);

        mLoadingAnimator = ObjectAnimator.ofFloat(loading, ROTATION, 720);
        mLoadingAnimator.setRepeatMode(ValueAnimator.RESTART);
        mLoadingAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mLoadingAnimator.setDuration(600);
        mLoadingAnimator.setInterpolator(new DecelerateInterpolator(1.3f));

        mLoadingAnimator.start();
    }

    void stopLoading() {
        if(mLoadingAnimator != null) {
            mLoadingAnimator.cancel();
        }
    }

    private void setupAnimation(ObjectAnimator animator, Interpolator interpolator, int duration) {
        animator.setInterpolator(interpolator);
        animator.setDuration(duration);
    }
}
