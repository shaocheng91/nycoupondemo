package com.nineyi.module.coupon.ui.use.online.wrapper;

import com.nineyi.module.coupon.model.CouponOnline;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseItemCouponUseable extends CouponOnlineUseItemCoupon {
    public CouponOnlineUseItemCouponUseable(CouponOnline coupon) {
        super(coupon);
    }
}
