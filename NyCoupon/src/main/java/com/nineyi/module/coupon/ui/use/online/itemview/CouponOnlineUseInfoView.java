package com.nineyi.module.coupon.ui.use.online.itemview;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.base.ui.countdown.CountdownObserver;
import com.nineyi.module.base.utils.TimeUtils;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.ui.use.online.OnOnlineCouponItemClickListener;
import com.nineyi.module.coupon.ui.utils.CouponCountdownCalculator;
import com.nineyi.module.coupon.ui.utils.ECouponUtils;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Date;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseInfoView extends ConstraintLayout implements CountdownObserver {

    private CountdownManager mCountdownManager;
    private WeakReference<CountdownObserver> mWeakObserver;

    private TextView mRule;
    private TextView mLimit;
    private TextView mEndTime;

    private long mEndTimeLong;

    public CouponOnlineUseInfoView(Context context) {
        super(context);
        init(context);
    }

    public CouponOnlineUseInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponOnlineUseInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.coupon_online_use_item_information, this);

        mRule = (TextView) findViewById(R.id.coupon_online_use_item_information_rule);
        mLimit = (TextView) findViewById(R.id.coupon_online_use_item_information_limit);
        mEndTime = (TextView) findViewById(R.id.coupon_online_use_item_information_endtime);

        DisplayMetrics dm = getResources().getDisplayMetrics();

        MarginLayoutParams layoutParams = new MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(UiUtils.convertDipToPixel(38, dm), 0, UiUtils.convertDipToPixel(1, dm), 0);
        setLayoutParams(layoutParams);

        setPadding(0, 0, 0, UiUtils.convertDipToPixel(10, dm));
        setBackground(getResources().getDrawable(R.drawable.bg_coupon_online_use_information));

        mWeakObserver = new WeakReference<CountdownObserver>(this);
    }

    public void showInfo(final CouponOnline coupon,  boolean isSelected, int position) {
        mRule.setText(getContext().getString(R.string.coupon_list_item_rule, coupon.getECouponUsingMinPrice()));

        double maxDiscountLimitPercent = coupon.getECouponMaxDiscountLimit() * 100;
        mLimit.setText(String.format(getContext().getString(R.string.coupon_online_use_info_max_discount), maxDiscountLimitPercent));

        mEndTimeLong = coupon.getUsingEndDateTime().getTimeLong();
        boolean shouldShowCountdown = TimeUtils.isWithinDays(mEndTimeLong, 1);

        if(shouldShowCountdown) {
            mCountdownManager.unregister(mWeakObserver);
            mCountdownManager.register(mWeakObserver);

            String leftTime = new CouponCountdownCalculator().calculateLeftTime(mEndTimeLong, System.currentTimeMillis());

            String useEndString = getContext().getString(R.string.coupon_online_use_info_countdown, leftTime);
            mEndTime.setText(useEndString);
        } else {
            mCountdownManager.unregister(mWeakObserver);

            String useEnd = ECouponUtils.getFormatDateMonthDay(getContext(), new Date(mEndTimeLong));

            String useEndString = getContext().getString(R.string.coupon_online_use_info_end_time, useEnd);
            mEndTime.setText(useEndString);
        }
    }

    @Override
    public void countdown(long systemTime) {
        String leftTime = new CouponCountdownCalculator().calculateLeftTime(mEndTimeLong, systemTime);

        String useEndString = getContext().getString(R.string.coupon_online_use_info_countdown, leftTime);
        mEndTime.setText(useEndString);

        if(mEndTimeLong < systemTime) {
            mCountdownManager.unregister(mWeakObserver);
        }
    }

    public void setCountdownManager(CountdownManager countdownManager) {
        mCountdownManager = countdownManager;
    }
}