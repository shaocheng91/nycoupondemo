package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.CouponTint;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItem;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.List;


/**
 * Created by ReeceCheng on 2017/10/16.
 */

public class CouponOnlineUseView extends RelativeLayout implements CouponOnlineUseContract.View {

    RecyclerView mRecyclerView;
    CouponOnlineUseAdapter mAdapter;
    View mEmptyView;

    private CouponOnlineUseContract.Presenter mPresenter;
    private CouponTint mCouponTint;

    private CountdownManager mCountdownManager;

    public CouponOnlineUseView(@NonNull Context context, CouponTint couponTint) {
        super(context);
        this.mCouponTint = couponTint;
        init(context);
    }

    public CouponOnlineUseView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public CouponOnlineUseView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponOnlineUseView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public CouponOnlineUseView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr,
            @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.coupon_online_use_view, this);

        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(params);

        mRecyclerView = (RecyclerView) findViewById(R.id.coupon_online_use_recyclerview);
        mRecyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        mRecyclerView.addItemDecoration(new CouponOnlineUseDecoration());

        mEmptyView = findViewById(R.id.coupon_online_use_empty_layout);

        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);

        mCountdownManager = new CountdownManager();
    }

    @Override
    public void setPresenter(CouponOnlineUseContract.Presenter presenter) {
        mPresenter = presenter;
        mAdapter = new CouponOnlineUseAdapter(getContext(), mPresenter, mCouponTint, mCountdownManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void showCouponList(List<CouponOnlineUseItem> wrappers) {
        if (wrappers.isEmpty()) {
            mRecyclerView.setVisibility(GONE);
            mEmptyView.setVisibility(VISIBLE);
        } else {
            mAdapter.setData(wrappers);
            mAdapter.notifyDataSetChanged();

            mEmptyView.setVisibility(GONE);
            mRecyclerView.setVisibility(VISIBLE);
        }
    }
}
