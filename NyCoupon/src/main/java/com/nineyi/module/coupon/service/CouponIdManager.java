package com.nineyi.module.coupon.service;

/**
 * Created by shaocheng on 2017/10/6.
 */

public interface CouponIdManager {
    String getGUID();
}
