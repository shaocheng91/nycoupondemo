package com.nineyi.module.coupon.ui.use.online.viewholder;

import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseUsableView;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUseable;

import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseUsableViewHolder extends CouponOnlineUseViewHolder<CouponOnlineUseItemCouponUseable> {

    private final CouponOnlineUseUsableView mView;

    public CouponOnlineUseUsableViewHolder(View itemView) {
        super(itemView);

        this.mView = (CouponOnlineUseUsableView) itemView;
    }

    @Override
    public void bindData(CouponOnlineUseItemCouponUseable data, int position) {
        mView.showCoupon(data.getCoupon(), data.isSelected(), position);
    }
}
