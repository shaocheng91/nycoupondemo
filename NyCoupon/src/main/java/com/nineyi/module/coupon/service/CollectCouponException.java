package com.nineyi.module.coupon.service;

/**
 * Created by shaocheng on 2017/10/30.
 */

public class CollectCouponException extends Exception {
    public enum ErrorType {
        FAIL,
        INVALID,
        ALREADY_COLLECTED,
        UNKNOWN
    }

    public final ErrorType errorType;
    public final String serverMessage;

    public CollectCouponException(ErrorType errorType) {
        this(errorType, null);
    }

    public CollectCouponException(ErrorType errorType, String serverMessage) {
        this.errorType = errorType;
        this.serverMessage = serverMessage;
    }
}
