package com.nineyi.module.coupon.ui.list.wrapper;

import com.nineyi.module.coupon.model.Coupon;

/**
 * Created by shaocheng on 2017/10/6.
 */

public class CouponListItemCoupon implements CouponListItem {
    private final int mViewType;
    private final Coupon coupon;

    public CouponListItemCoupon(int viewType, Coupon coupon) {
        this.mViewType = viewType;
        this.coupon = coupon;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    @Override
    public int getViewType() {
        return mViewType;
    }
}
