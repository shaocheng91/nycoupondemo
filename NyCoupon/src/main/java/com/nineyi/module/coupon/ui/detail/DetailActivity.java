package com.nineyi.module.coupon.ui.detail;

import android.support.v4.app.Fragment;

import com.nineyi.module.base.appcompat.NyBaseContentFragmentActivity;
import com.nineyi.module.base.navigator.argument.provider.CouponDetailArgumentProvider;

/**
 * Created by shaocheng on 2017/10/19.
 */

public class DetailActivity extends NyBaseContentFragmentActivity {
    @Override
    public Fragment onCreateContentFragment() {
        CouponDetailArgumentProvider argumentProvider = new CouponDetailArgumentProvider(getIntent().getExtras());

        int couponId = argumentProvider.getCouponId();
        String from = argumentProvider.getFrom();

        return DetailFragment.newInstance(couponId, from);
    }
}
