package com.nineyi.module.coupon.model;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.module.coupon.service.CouponManager;


/**
 * Created by shaocheng on 2017/9/22.
 */

public class Coupon {

    public static final Coupon EMPTY = new Coupon.Builder().build();

    public enum Status {
        USED,
        COLLECTED,
        AVAILABLE,
        FIRST_DOWNLOAD_NOT_QUALIFIED,
        FIRST_DOWNLOAD_COLLECTED,
        NO_MORE_COUPONS,
        BEFORE_USE_TIME,
        AFTER_USE_TIME,
        BEFORE_COLLECT_TIME,
        AFTER_COLLECT_TIME,
        UNKNOWN
    }

    private String code;
    private String imgUrl;
    private String totalGiftLimit;
    private int qtyLimit;
    private double discountPrice;
    private String typeDef;
    private NineyiDate startDateTime;
    private NineyiDate endDateTime;
    private NineyiDate usingEndDateTime;
    private NineyiDate usingStartDateTime;
    private int totalQty;
    private int id;
    private String name;
    private String description;
    private String eCouponWording;
    private int shopId;
    private String shopName;
    private boolean isSingleCode;
    private int couponTotalCount;
    private String takeEndTimeWarningText;
    private double eCouponMaxDiscountLimit;
    private double eCouponUsingMinPrice;

    private long eCouponId;
    private boolean hasNormalCoupon;
    private String couponCode;

    private Status status;
    private boolean isCode;
    private boolean isShowPresent;

    private boolean isOnline;
    private boolean isOffline;
    private boolean isUsed;

    private Coupon(Builder builder) {
        code = builder.code;
        imgUrl = builder.imgUrl;
        totalGiftLimit = builder.totalGiftLimit;
        qtyLimit = builder.qtyLimit;
        discountPrice = builder.discountPrice;
        typeDef = builder.typeDef;
        startDateTime = builder.startDateTime;
        endDateTime = builder.endDateTime;
        usingEndDateTime = builder.usingEndDateTime;
        usingStartDateTime = builder.usingStartDateTime;
        totalQty = builder.totalQty;
        id = builder.id;
        name = builder.name;
        description = builder.description;
        eCouponWording = builder.eCouponWording;
        shopId = builder.shopId;
        shopName = builder.shopName;
        isSingleCode = builder.isSingleCode;
        couponTotalCount = builder.couponTotalCount;
        takeEndTimeWarningText = builder.takeEndTimeWarningText;
        eCouponMaxDiscountLimit = builder.eCouponMaxDiscountLimit;

        eCouponId = builder.eCouponId;
        hasNormalCoupon = builder.hasNormalCoupon;
        couponCode = builder.couponCode;

        status = builder.status;

        eCouponUsingMinPrice = builder.eCouponUsingMinPrice;

        isOnline = builder.isOnline;
        isOffline = builder.isOffline;
        isUsed = builder.isUsed;
    }

    public String getCode() {
        return code;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getTotalGiftLimit() {
        return totalGiftLimit;
    }

    public int getQtyLimit() {
        return qtyLimit;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public String getTypeDef() {
        return typeDef;
    }

    public NineyiDate getStartDateTime() {
        return startDateTime;
    }

    public NineyiDate getEndDateTime() {
        return endDateTime;
    }

    public NineyiDate getUsingEndDateTime() {
        return usingEndDateTime;
    }

    public NineyiDate getUsingStartDateTime() {
        return usingStartDateTime;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String geteCouponWording() {
        return eCouponWording;
    }

    public int getShopId() {
        return shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public boolean isSingleCode() {
        return isSingleCode;
    }

    public int getCouponTotalCount() {
        return couponTotalCount;
    }

    public String getTakeEndTimeWarningText() {
        return takeEndTimeWarningText;
    }

    public double geteCouponMaxDiscountLimit() {
        return eCouponMaxDiscountLimit;
    }

    public long geteCouponId() {
        return eCouponId;
    }

    public boolean isHasNormalCoupon() {
        return hasNormalCoupon;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public double getECouponUsingMinPrice() {
        return eCouponUsingMinPrice;
    }

    public Status getStatus() {
        return status;
    }

    public boolean isFirstDownload() {
        return CouponManager.isFirstDownload(typeDef);
    }

    public boolean isDrawOut() {
        return CouponManager.isDrawOut(typeDef);
    }

    public boolean isShowPresent() {
        return CouponManager.isShowPresent(typeDef);
    }

    public boolean isCode() {
        return CouponManager.isCode(typeDef);
    }

    public boolean isUnknown() {
        return CouponManager.isUnKnown(typeDef);
    }

    public boolean isOnline() {
        return isOnline;
    }

    public boolean isOffline() {
        return isOffline;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public static final class Builder {
        private String code;
        private String imgUrl;
        private String totalGiftLimit;
        private int qtyLimit;
        private double discountPrice;
        private String typeDef;
        private NineyiDate startDateTime;
        private NineyiDate endDateTime;
        private NineyiDate usingEndDateTime;
        private NineyiDate usingStartDateTime;
        private int totalQty;
        private int id;
        private String name;
        private String description;
        private String eCouponWording;
        private int shopId;
        private String shopName;
        private boolean isSingleCode;
        private int couponTotalCount;
        private String takeEndTimeWarningText;
        private double eCouponMaxDiscountLimit;
        private long eCouponId;
        private boolean hasNormalCoupon;
        private String couponCode;
        private Status status;
        private double eCouponUsingMinPrice;
        private boolean isOnline;
        private boolean isOffline;
        private boolean isUsed;

        public Builder() {
        }

        public Builder code(String val) {
            code = val;
            return this;
        }

        public Builder imgUrl(String val) {
            imgUrl = val;
            return this;
        }

        public Builder totalGiftLimit(String val) {
            totalGiftLimit = val;
            return this;
        }

        public Builder qtyLimit(int val) {
            qtyLimit = val;
            return this;
        }

        public Builder discountPrice(double val) {
            discountPrice = val;
            return this;
        }

        public Builder typeDef(String val) {
            typeDef = val;
            return this;
        }

        public Builder startDateTime(NineyiDate val) {
            startDateTime = val;
            return this;
        }

        public Builder endDateTime(NineyiDate val) {
            endDateTime = val;
            return this;
        }

        public Builder usingEndDateTime(NineyiDate val) {
            usingEndDateTime = val;
            return this;
        }

        public Builder usingStartDateTime(NineyiDate val) {
            usingStartDateTime = val;
            return this;
        }

        public Builder totalQty(int val) {
            totalQty = val;
            return this;
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder eCouponWording(String val) {
            eCouponWording = val;
            return this;
        }

        public Builder shopId(int val) {
            shopId = val;
            return this;
        }

        public Builder shopName(String val) {
            shopName = val;
            return this;
        }

        public Builder isSingleCode(boolean val) {
            isSingleCode = val;
            return this;
        }

        public Builder couponTotalCount(int val) {
            couponTotalCount = val;
            return this;
        }

        public Builder takeEndTimeWarningText(String val) {
            takeEndTimeWarningText = val;
            return this;
        }

        public Builder eCouponMaxDiscountLimit(double val) {
            eCouponMaxDiscountLimit = val;
            return this;
        }

        public Builder eCouponId(long val) {
            eCouponId = val;
            return this;
        }

        public Builder hasNormalCoupon(boolean val) {
            hasNormalCoupon = val;
            return this;
        }

        public Builder couponCode(String val) {
            couponCode = val;
            return this;
        }

        public Builder status(Status val) {
            status = val;
            return this;
        }

        public Builder eCouponUsingMinPrice(double val) {
            eCouponUsingMinPrice = val;
            return this;
        }

        public Builder isOnline(boolean val) {
            isOnline = val;
            return this;
        }

        public Builder isOffline(boolean val) {
            isOffline = val;
            return this;
        }

        public Builder isUsed(boolean val) {
            isUsed = val;
            return this;
        }

        public Coupon build() {
            return new Coupon(this);
        }
    }
}

