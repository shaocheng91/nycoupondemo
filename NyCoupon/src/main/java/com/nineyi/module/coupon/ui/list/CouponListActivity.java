package com.nineyi.module.coupon.ui.list;

import android.support.v4.app.Fragment;

import com.nineyi.module.base.appcompat.NyBaseContentFragmentActivity;

/**
 * Created by shaocheng on 2017/10/19.
 */

public class CouponListActivity extends NyBaseContentFragmentActivity {
    @Override
    public Fragment onCreateContentFragment() {
        return new CouponListFragment();
    }
}
