package com.nineyi.module.coupon.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.base.ui.countdown.CountdownObserver;
import com.nineyi.module.base.utils.TimeUtils;
import com.nineyi.module.coupon.ui.utils.CouponCountdownCalculator;
import com.nineyi.module.coupon.ui.utils.ECouponUtils;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;

import java.lang.ref.WeakReference;
import java.util.Date;


/**
 * Created by ReeceCheng on 2017/9/29.
 */

public class CouponTicketCollected extends CardView implements CouponTicket, CountdownObserver {

    private CountdownManager mCountdownManager;
    private WeakReference<CountdownObserver> mWeakObserver;

    private Coupon mCoupon;
    long mEndTimeLong;

    TextView mCouponTitle;
    TextView mUseTagOffline;
    TextView mUseTagOnline;
    TextView mPrice;
    TextView mRule;
    TextView mEndTime;
    TextView mCountDownTitle;
    TextView mCountDown;

    public CouponTicketCollected(Context context) {
        super(context);
        init(context);
    }

    public CouponTicketCollected(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponTicketCollected(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {

        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UiUtils.convertDipToPixel(137, getResources().getDisplayMetrics()));
        setLayoutParams(params);

        setRadius(UiUtils.convertDipToPixel(5, getResources().getDisplayMetrics()));

        inflate(context, R.layout.coupon_ticket_collected, this);

        mCouponTitle = (TextView) findViewById(R.id.coupon_list_item_title);
        mUseTagOffline = (TextView) findViewById(R.id.coupon_list_item_use_tag_offline);
        mUseTagOnline = (TextView) findViewById(R.id.coupon_list_item_use_tag_online);
        mPrice = (TextView) findViewById(R.id.coupon_list_item_price);
        mRule = (TextView) findViewById(R.id.coupon_list_item_rule);
        mEndTime = (TextView) findViewById(R.id.coupon_list_item_end_time);
        mCountDownTitle = (TextView) findViewById(R.id.coupon_list_item_countdown_title);
        mCountDown = (TextView) findViewById(R.id.coupon_list_item_countdown);

        mWeakObserver = new WeakReference<CountdownObserver>(this);
    }

    @Override
    public void showCoupon(Coupon coupon) {

        mCoupon = coupon;

        if (coupon.isFirstDownload()) {
            mCouponTitle.setText(R.string.coupon_list_item_title_first_download);
        } else {
            mCouponTitle.setText(R.string.coupon_list_item_title);
        }

        if (mCoupon.isOnline()) {
            mUseTagOnline.setVisibility(VISIBLE);
        } else {
            mUseTagOnline.setVisibility(GONE);
        }

        if (mCoupon.isOffline()) {
            mUseTagOffline.setVisibility(VISIBLE);
        } else {
            mUseTagOffline.setVisibility(GONE);
        }

        // TODO Will it be too long?
        mPrice.setText(getContext().getString(R.string.taiwan_price_format, coupon.getDiscountPrice()));

        mRule.setText(getResources().getString(R.string.coupon_list_item_rule, coupon.getECouponUsingMinPrice()));

        if (shouldShowCountDown(coupon)) {
            mEndTimeLong = coupon.getUsingEndDateTime().getTimeLong();

            mCountdownManager.register(mWeakObserver);

            String leftTime = new CouponCountdownCalculator().calculateLeftTime(mEndTimeLong, System.currentTimeMillis());
            mCountDown.setText(leftTime);
            mCountDownTitle.setText(R.string.coupon_list_item_use_countdown);

            mCountDown.setVisibility(VISIBLE);
            mCountDownTitle.setVisibility(VISIBLE);
            mEndTime.setVisibility(GONE);
        } else {
            String useEnd = ECouponUtils.getFormatDateYearMonthDay(getContext(), new Date(coupon.getUsingEndDateTime().getTimeLong()));
            String useEndString = getContext().getString(R.string.coupon_list_item_use_end_time, useEnd);

            mEndTime.setText(useEndString);

            mCountDownTitle.setVisibility(GONE);
            mCountDown.setVisibility(GONE);
            mEndTime.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setOnClickCouponListener(final OnClickCouponListener listener) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickCoupon(mCoupon);
            }
        });
    }

    public void setCountdownManager(CountdownManager countdownManager) {
        mCountdownManager = countdownManager;
    }

    private boolean shouldShowCountDown(Coupon coupon) {
        long endTime = coupon.getUsingEndDateTime().getTimeLong();
        return TimeUtils.isWithinDays(endTime, 1);
    }

    @Override
    public void countdown(long systemTime) {
        String leftTime = new CouponCountdownCalculator().calculateLeftTime(mEndTimeLong, systemTime);
        mCountDown.setText(leftTime);

        if(mEndTimeLong < systemTime) {
            mCountdownManager.unregister(mWeakObserver);
        }
    }


}
