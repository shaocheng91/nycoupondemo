package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.coupon.model.CouponOnline;

import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/18.
 */

public interface OnOnlineCouponItemClickListener {
    void onClick(View view, CouponOnline coupon, int position);
    void onClickToDetail(CouponOnline coupon);
}
