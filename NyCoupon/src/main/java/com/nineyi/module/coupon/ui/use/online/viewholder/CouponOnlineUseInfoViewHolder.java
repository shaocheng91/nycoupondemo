package com.nineyi.module.coupon.ui.use.online.viewholder;

import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseInfoView;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponInformation;

import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseInfoViewHolder extends CouponOnlineUseViewHolder<CouponOnlineUseItemCouponInformation> {

    private CouponOnlineUseInfoView mView;

    public CouponOnlineUseInfoViewHolder(View itemView) {
        super(itemView);

        mView = (CouponOnlineUseInfoView) itemView;
    }

    @Override
    public void bindData(CouponOnlineUseItemCouponInformation data, int position) {
        mView.showInfo(data.getCoupon(), false, position);
    }
}
