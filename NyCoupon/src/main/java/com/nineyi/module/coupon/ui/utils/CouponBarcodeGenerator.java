package com.nineyi.module.coupon.ui.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;


/**
 * Created by ReeceCheng on 2017/11/3.
 */
public class CouponBarcodeGenerator extends AsyncTask<String, Integer, Bitmap>  {
    public interface GenFinishedCallback {
        void onGenFinished(Bitmap barcode, String code, int id);
        void onGenError(String code, int id);
    }

    private int mWidth;
    private int mHeight;
    private BarcodeFormat mFormat;
    private String mCode;
    private int mId;

    private GenFinishedCallback callback;

    public CouponBarcodeGenerator(int width, int height, BarcodeFormat format) {
        mWidth = width;
        mHeight = height;
        mFormat = format;
    }

    public CouponBarcodeGenerator(int width, int height, BarcodeFormat format, int id) {
        mWidth = width;
        mHeight = height;
        mFormat = format;
        mId = id;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        mCode = params[0];
        return genBarcode(params[0]);
    }

    private Bitmap genBarcode(String code) {
        Bitmap bitmap;
        MultiFormatWriter writer = new MultiFormatWriter();

        try {
            BitMatrix bm = writer.encode(code, mFormat, mWidth, mHeight);
            bitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < mWidth; i++) {
                for (int j = 0; j < mHeight; j++) {
                    bitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
                }
            }
        } catch (Exception e) {
            Log.e(CouponBarcodeGenerator.class.toString() ,e.getMessage());
            callback.onGenError(code, mId);
            return null;
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (callback != null) {
            callback.onGenFinished(bitmap, mCode, mId);
        }
    }

    public void gen(String code) {
        this.execute(code);
    }


    public void setCallback(GenFinishedCallback callback) {
        this.callback = callback;
    }
}