package com.nineyi.module.coupon.model;

import com.nineyi.data.model.ecoupon.NyBarCode;


/**
 * Created by ReeceCheng on 2017/11/3.
 */

public class CouponOffline {
    private int couponId;
    private int couponSlaveId;
    private NyBarCode vipMemberBarcode;
    private NyBarCode couponBarcode;
    private boolean isUseCustomCode;
    private NyBarCode couponCustomCode1;
    private NyBarCode couponCustomCode2;
    private NyBarCode couponCustomCode3;

    private CouponOffline(Builder builder) {
        couponId = builder.couponId;
        couponSlaveId = builder.couponSlaveId;
        vipMemberBarcode = builder.vipMemberBarcode;
        couponBarcode = builder.couponBarcode;
        isUseCustomCode = builder.isUseCustomCode;
        couponCustomCode1 = builder.couponCustomCode1;
        couponCustomCode2 = builder.couponCustomCode2;
        couponCustomCode3 = builder.couponCustomCode3;
    }

    public int getCouponId() {
        return couponId;
    }

    public int getCouponSlaveId() {
        return couponSlaveId;
    }

    public NyBarCode getVipMemberBarcode() {
        return vipMemberBarcode;
    }

    public NyBarCode getCouponBarcode() {
        return couponBarcode;
    }

    public boolean isUseCustomCode() {
        return isUseCustomCode;
    }

    public NyBarCode getCouponCustomCode1() {
        return couponCustomCode1;
    }

    public NyBarCode getCouponCustomCode2() {
        return couponCustomCode2;
    }

    public NyBarCode getCouponCustomCode3() {
        return couponCustomCode3;
    }

    public static final class Builder {

        private int couponId;
        private int couponSlaveId;
        private NyBarCode vipMemberBarcode;
        private NyBarCode couponBarcode;
        private boolean isUseCustomCode;
        private NyBarCode couponCustomCode1;
        private NyBarCode couponCustomCode2;
        private NyBarCode couponCustomCode3;

        public Builder() {
        }

        public Builder couponId(int val) {
            couponId = val;
            return this;
        }

        public Builder couponSlaveId(int val) {
            couponSlaveId = val;
            return this;
        }

        public Builder vipMemberBarcode(NyBarCode val) {
            vipMemberBarcode = val;
            return this;
        }

        public Builder couponBarcode(NyBarCode val) {
            couponBarcode = val;
            return this;
        }

        public Builder isUseCustomCode(boolean val) {
            isUseCustomCode = val;
            return this;
        }

        public Builder couponCustomCode1(NyBarCode val) {
            couponCustomCode1 = val;
            return this;
        }

        public Builder couponCustomCode2(NyBarCode val) {
            couponCustomCode2 = val;
            return this;
        }

        public Builder couponCustomCode3(NyBarCode val) {
            couponCustomCode3 = val;
            return this;
        }

        public CouponOffline build() {
            return new CouponOffline(this);
        }
    }
}
