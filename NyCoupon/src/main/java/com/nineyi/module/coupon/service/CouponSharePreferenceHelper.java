package com.nineyi.module.coupon.service;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class CouponSharePreferenceHelper {

    public static final String PENDING_COUPON_ID = "com.nineyi.module.coupon.pending_coupon_id";
    public static final String PENDING_COUPON_IS_FIRST_DOWNLOAD = "com.nineyi.module.coupon.pending_coupon_is_first_download";

    public static final int NO_PENDING_COUPON = -1;

    private final SharedPreferences prefs;

    public CouponSharePreferenceHelper(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void savePendingCouponId(int id) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PENDING_COUPON_ID, id);
        editor.apply();
    }

    public void savePendingCouponIsFirstDownload(boolean isFirstDownload) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PENDING_COUPON_IS_FIRST_DOWNLOAD, isFirstDownload);
        editor.apply();
    }

    public int loadPendingCouponId() {
        return prefs.getInt(PENDING_COUPON_ID, NO_PENDING_COUPON);
    }

    public boolean loadPendingCouponIsFirstDownload() {
        return prefs.getBoolean(PENDING_COUPON_IS_FIRST_DOWNLOAD, false);
    }

    public void removePendingCouponId() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(PENDING_COUPON_ID);
        editor.apply();
    }

    public void removePendingCouponIsFirstDownload() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(PENDING_COUPON_IS_FIRST_DOWNLOAD);
        editor.apply();
    }
}

