package com.nineyi.module.coupon.ui.use.offline;

import com.nineyi.module.coupon.model.CouponOffline;
import com.nineyi.module.coupon.ui.BaseView;


/**
 * Created by ReeceCheng on 2017/11/3.
 */

public interface CouponOfflineUseContract {
    interface View extends BaseView<CouponOfflineUseContract.Presenter> {
        void showOfflineUse(CouponOffline offline);
    }

    interface Presenter {
        void useCoupon(int couponId, boolean useCache);
    }
}
