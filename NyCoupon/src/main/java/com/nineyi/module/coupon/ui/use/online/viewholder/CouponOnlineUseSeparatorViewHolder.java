package com.nineyi.module.coupon.ui.use.online.viewholder;

import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemSeparator;

import android.view.View;
import android.widget.TextView;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseSeparatorViewHolder extends CouponOnlineUseViewHolder<CouponOnlineUseItemSeparator> {

    private final TextView mTitle;

    public CouponOnlineUseSeparatorViewHolder(View itemView) {
        super(itemView);

        mTitle = (TextView) itemView.findViewById(R.id.coupon_online_use_item_separator_title);
    }

    @Override
    public void bindData(CouponOnlineUseItemSeparator data, int position) {
        mTitle.setText(data.getTitle());
    }
}
