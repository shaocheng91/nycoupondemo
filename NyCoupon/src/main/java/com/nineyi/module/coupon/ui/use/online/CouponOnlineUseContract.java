package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.ui.BaseView;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItem;

import java.util.List;


/**
 * Created by ReeceCheng on 2017/10/16.
 */

public interface CouponOnlineUseContract {
    interface View extends BaseView<Presenter> {
        void showCouponList(List<CouponOnlineUseItem> wrappers);
    }

    interface Presenter {
        void getCouponList();
        void goToCouponDetail(CouponOnline coupon);
        void selectedCoupon(CouponOnline coupon);
        int getSelectedCouponId();
        int getNotUseCouponId();
        void submit();
    }
}
