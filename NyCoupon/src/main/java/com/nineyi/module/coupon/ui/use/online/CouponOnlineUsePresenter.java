package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.data.model.shoppingcart.v4.ShoppingCartData;
import com.nineyi.module.base.navigator.SchemeRouter;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.CouponShoppingCartDataManager;
import com.nineyi.module.coupon.ui.detail.DetailContract;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItem;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUnuseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponNotUse;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemEmpty;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemSeparator;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by ReeceCheng on 2017/10/16.
 */

public class CouponOnlineUsePresenter implements CouponOnlineUseContract.Presenter {

    enum Group {
        USEABLE,
        UNUSEABLE
    }

    interface OnCouponSelectedSubmitListener {
        void onSubmit();
    }

    private static final int NOT_USE_COUPON_ID = 0;

    private final Context mContext;
    private final CouponOnlineUseContract.View mView;

    private ShoppingCartData mShoppingCartData;

    private final CouponManager mCouponManager;
    private final CouponShoppingCartDataManager mCouponShoppingCartDataManager;

    private OnCouponSelectedSubmitListener mSubmitListener;

    private CouponOnline mSelectedCoupon;

    public CouponOnlineUsePresenter(
            Context context,
            CouponOnlineUseContract.View view,
            CouponManager couponManager,
            CouponShoppingCartDataManager couponShoppingCartDataManager,
            CompositeDisposableHelper helper,
            OnCouponSelectedSubmitListener listener) {

        this.mContext = context;
        this.mView = view;
        this.mCouponShoppingCartDataManager = couponShoppingCartDataManager;
        this.mShoppingCartData = couponShoppingCartDataManager.getShoppingCartData();
        this.mCouponManager = couponManager;
        this.mSubmitListener = listener;
    }

    @Override
    public void getCouponList() {
        List<CouponOnline> coupons = mCouponManager.getOnlineUseCouponList(mShoppingCartData);
        List<CouponOnlineUseItem> all = convertCouponsToItems(coupons);

        mView.showCouponList(all);
    }

    @VisibleForTesting
    List<CouponOnlineUseItem> convertCouponsToItems(List<CouponOnline> coupons) {
        List<CouponOnlineUseItem> items = new ArrayList<>();

        if (coupons.isEmpty()) {
            return items;
        }

        Map<Group, List<CouponOnline>> groupedCoupons = groupCoupons(coupons);

        items.add(new CouponOnlineUseItemSeparator(mContext.getString(R.string.coupon_online_use_separator_can_use)));

        List<CouponOnlineUseItem> usableCoupons = getUseableCoupons(groupedCoupons);
        if (usableCoupons.isEmpty()) {
            items.add(new CouponOnlineUseItemEmpty());
        } else {
            items.addAll(usableCoupons);
            items.add(new CouponOnlineUseItemCouponNotUse(null));
        }

        items.addAll(getUnuseableCoupons(groupedCoupons));

        return items;
    }

    private List<CouponOnlineUseItem> getUseableCoupons(Map<Group, List<CouponOnline>> groupedCoupons) {
        List<CouponOnlineUseItem> all = new ArrayList<>();

        List<CouponOnline> useable = groupedCoupons.get(Group.USEABLE);

        // TODO Check sort
        // Collections.sort(useable, new CouponListPresenter.NotTakenCouponComparator());

        for (CouponOnline coupon: useable) {
            all.add(new CouponOnlineUseItemCouponUseable(coupon));
        }

        return all;
    }

    private List<CouponOnlineUseItem> getUnuseableCoupons(Map<Group, List<CouponOnline>> groupedCoupons) {
        List<CouponOnlineUseItem> all = new ArrayList<>();

        List<CouponOnline> unuseable = groupedCoupons.get(Group.UNUSEABLE);
        if (!unuseable.isEmpty()) {
            all.add(new CouponOnlineUseItemSeparator(mContext.getString(R.string.coupon_online_use_separator_cant_use)));

            // TODO Check sort
            //                    Collections.sort(useable, new CouponListPresenter.NotTakenCouponComparator());

            for (CouponOnline coupon: unuseable) {
                all.add(new CouponOnlineUseItemCouponUnuseable(coupon));
            }
        }

        return all;
    }

    private Map<Group, List<CouponOnline>> groupCoupons(List<CouponOnline> coupons) {
        Map<Group, List<CouponOnline>> groupedCoupons = new HashMap<>();

        groupedCoupons.put(Group.USEABLE, new ArrayList<CouponOnline>());
        groupedCoupons.put(Group.UNUSEABLE, new ArrayList<CouponOnline>());


        for(CouponOnline coupon : coupons) {
            Group groupKey = choosesGroupForCoupon(coupon);
            groupedCoupons.get(groupKey).add(coupon);
        }

        return groupedCoupons;
    }


    @VisibleForTesting
    Group choosesGroupForCoupon(CouponOnline coupon) {
        if(coupon.isAchieveUsingMinPrice()) {
            return Group.USEABLE;
        } else {
            return Group.UNUSEABLE;
        }
    }

    @Override
    public void goToCouponDetail(CouponOnline coupon) {
        SchemeRouter.navToCouponDetail(mContext, coupon.getDetailId(), DetailContract.ARG_FROM_SHOPPING_CART);
    }

    @Override
    public void selectedCoupon(CouponOnline coupon) {
        mSelectedCoupon = coupon;
    }

    @Override
    public int getSelectedCouponId() {
        if(mShoppingCartData != null) {
            return mShoppingCartData.getSelectedECouponSlaveId();
        } else {
            return NOT_USE_COUPON_ID;
        }
    }

    @Override
    public int getNotUseCouponId() {
        return NOT_USE_COUPON_ID;
    }

    @Override
    public void submit() {
        if(mSelectedCoupon != null) {
            mShoppingCartData.setSelectedECouponSlaveId(mSelectedCoupon.getId());
            mCouponShoppingCartDataManager.updateSelectedEcouponSlaveId(mSelectedCoupon.getId());
        } else {
            // for not use.
            mShoppingCartData.setSelectedECouponSlaveId(NOT_USE_COUPON_ID);
            mCouponShoppingCartDataManager.updateSelectedEcouponSlaveId(NOT_USE_COUPON_ID);
        }

        mCouponShoppingCartDataManager.setShouldRefreshShoppingCart();

        mSubmitListener.onSubmit();
    }

    @VisibleForTesting
    CouponOnline getSelectedCoupon() {
        return mSelectedCoupon;
    }
}
