package com.nineyi.module.coupon.ui.use.online.viewholder;

import android.view.View;

import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemEmpty;


/**
 * Created by ShaoCheng on 2017/10/25.
 */

public class CouponOnlineUseEmptyViewHolder extends CouponOnlineUseViewHolder<CouponOnlineUseItemEmpty> {

    public CouponOnlineUseEmptyViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bindData(CouponOnlineUseItemEmpty data, int position) {
        // Do nothing
    }
}
