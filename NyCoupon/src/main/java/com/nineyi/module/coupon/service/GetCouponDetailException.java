package com.nineyi.module.coupon.service;

/**
 * Created by shaocheng on 2017/10/3.
 */

public class GetCouponDetailException extends Exception {

    public enum ErrorCode {
        EMPTY,
        UNKNOWN
    }

    public final ErrorCode errorCode;

    public GetCouponDetailException() {
        this.errorCode = ErrorCode.UNKNOWN;
    }

    public GetCouponDetailException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

}
