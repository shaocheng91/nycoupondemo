package com.nineyi.module.coupon.ui.use.offline;

import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.model.CouponOffline;
import com.nineyi.module.coupon.service.CouponManager;

import android.content.Context;

import io.reactivex.observers.DisposableSingleObserver;


/**
 * Created by ReeceCheng on 2017/11/3.
 */

public class CouponOfflineUsePresenter implements CouponOfflineUseContract.Presenter {

    private Context mContext;
    private CouponOfflineUseContract.View mView;
    private CouponManager mCouponManager;
    private CompositeDisposableHelper mCompositeDisposableHelper;

    private CouponOffline mCouponOffline;

    public CouponOfflineUsePresenter(
            Context context,
            CouponOfflineUseContract.View view,
            CouponManager couponManager,
            CompositeDisposableHelper helper) {

        this.mContext = context;
        this.mView = view;
        this.mCouponManager = couponManager;
        this.mCompositeDisposableHelper = helper;
    }

    @Override
    public void useCoupon(int couponId, boolean useCache) {

        if(useCache && mCouponOffline != null) {
            mView.showOfflineUse(mCouponOffline);
        } else {
            mCompositeDisposableHelper.add(mCouponManager.useCoupon(couponId).subscribeWith(new DisposableSingleObserver<CouponOffline>(){
                @Override
                public void onSuccess(CouponOffline offline) {
                    mCouponOffline = offline;
                    mView.showOfflineUse(offline);
                }

                @Override
                public void onError(Throwable e) {
                    // TODO ERROR handling
                }
            }));
        }
    }

}