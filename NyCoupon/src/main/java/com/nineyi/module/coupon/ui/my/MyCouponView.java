package com.nineyi.module.coupon.ui.my;

import com.nineyi.module.base.navigator.SchemeRouter;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.List;


/**
 * Created by shaocheng on 2017/9/22.
 */

public class MyCouponView extends RelativeLayout implements MyCouponContract.View {

    RecyclerView mRecyclerView;
    ProgressBar mProgressBar;
    View mEmptyView;
    Button mEmptyButton;
    View mErrorView;
    Button mErrorActionButton;

    private MyCouponContract.Presenter mPresenter;
    private MyCouponAdapter mAdapter;
    private CountdownManager mCountdownManager;

    public MyCouponView(Context context) {
        super(context);
        init(context);
    }

    public MyCouponView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyCouponView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public MyCouponView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.my_coupon, this);

        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.bg_coupon_detail));

        mEmptyView = findViewById(R.id.my_coupon_empty);
        mEmptyButton = (Button) findViewById(R.id.my_coupon_empty_button);
        mEmptyButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SchemeRouter.navToCouponList(getContext());
            }
        });

        mErrorView = findViewById(R.id.my_coupon_error);
        mErrorActionButton = (Button) findViewById(R.id.coupon_common_action_button);
        mErrorActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.getCouponList(true);
            }
        });

        mProgressBar = (ProgressBar) findViewById(R.id.my_coupon_progressbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_coupon_recyclerview);
        mRecyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        mRecyclerView.addItemDecoration(new MyCouponDecoration());

        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);

        mCountdownManager = new CountdownManager();
    }

    @Override
    public void setPresenter(MyCouponContract.Presenter presenter) {
        mPresenter = presenter;
        mAdapter = new MyCouponAdapter(getContext(), mCountdownManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void showCouponList(List<CouponListItem> wrappers) {
        mProgressBar.setVisibility(GONE);

        if (wrappers.isEmpty()) {
            showOnlyChildView(mEmptyView);
        } else {
            mAdapter.setData(wrappers);
            mAdapter.notifyDataSetChanged();

            showOnlyChildView(mRecyclerView);
        }
    }

    @Override
    public void showError() {
        mProgressBar.setVisibility(GONE);
        showOnlyChildView(mErrorView);
    }

    @Override
    public void showEmpty() {
        mProgressBar.setVisibility(GONE);
        showOnlyChildView(mEmptyView);
    }

    private void showOnlyChildView(View child) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View c = getChildAt(i);
            if (c == child) {
                c.setVisibility(VISIBLE);
            } else {
                c.setVisibility(GONE);
            }
        }
    }
}
