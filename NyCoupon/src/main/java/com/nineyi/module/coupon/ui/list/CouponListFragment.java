package com.nineyi.module.coupon.ui.list;

import com.nineyi.module.base.appcompat.PullToRefreshFragmentV3;
import com.nineyi.module.coupon.CouponComponent;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.CouponSharePreferenceHelper;
import com.nineyi.module.coupon.service.LoginManager;
import com.nineyi.module.coupon.service.MigrationManager;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.service.PendingCouponHandler;
import com.nineyi.module.coupon.service.PromotionSharePreferenceHelper;
import com.nineyi.module.coupon.ui.keyin.KeyInPresenter;
import com.nineyi.module.coupon.ui.keyin.KeyInView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;


/**
 * Created by shaocheng on 2017/9/22.
 */

public class CouponListFragment extends PullToRefreshFragmentV3 implements CouponListView.OnCouponListRefreshedListener {

    private KeyInView mKeyInView;
    private KeyInPresenter mKeyInPresenter;

    private CouponListView mCouponListView;
    private CouponListPresenter mCouponListPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_coupon_list,
                createPullRefreshLayout(inflater, container), true);

        setActionBarTitle(R.string.coupon_area);

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.container);

        CouponComponent couponComponent = CouponComponent.get();
        CouponManager couponManager = couponComponent.couponManager();
        LoginManager loginManager = couponComponent.loginManager();
        MigrationManager migrationManager = couponComponent.migrationmanager();
        PromotionSharePreferenceHelper promotionSharePreferenceHelper =
                couponComponent.promotionSharePreferenceHelper();
        MsgManager msgManager = couponComponent.msgManager();
        NavManager navManager = couponComponent.navManager();
        CouponSharePreferenceHelper couponSharePreferenceHelper = couponComponent.couponSharePreferenceHelper();
        PendingCouponHandler pendingCouponHandler = new PendingCouponHandler(couponSharePreferenceHelper);

        mKeyInView = new KeyInView(getActivity());
        mKeyInView.setMsgManager(msgManager);

        mKeyInPresenter = new KeyInPresenter(couponManager, loginManager, mKeyInView, mCompositeDisposableHelper, false);
        mKeyInView.setPresenter(mKeyInPresenter);

        linearLayout.addView(mKeyInView);

        mCouponListView = new CouponListView(getActivity());
        mCouponListView.setMsgManager(msgManager);
        mCouponListView.setNavManager(navManager);
        mCouponListView.setOnCouponListRefreshedListener(this);

        mCouponListPresenter = new CouponListPresenter(
                getActivity(),
                couponManager,
                migrationManager,
                mCouponListView,
                promotionSharePreferenceHelper,
                mCompositeDisposableHelper,
                loginManager,
                navManager,
                pendingCouponHandler);

        mCouponListView.setPresenter(mCouponListPresenter);

        linearLayout.addView(mCouponListView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        setupPullRefreshLayout();

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();

        mCouponListPresenter.collectPendingCouponIfExists();

        mCouponListPresenter.loadCouponList(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mNineyiMenuHelper.intMenu(false, false);
        inflater.inflate(R.menu.coupon_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // TODO 之後的 Sprint 要導去正確的頁面
        if (item.getItemId() == R.id.coupon_action_history) {
            Toast.makeText(getContext(), "History", Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == R.id.coupon_action_explain) {
            Toast.makeText(getContext(), "Explain", Toast.LENGTH_SHORT).show();
        }

        return true;
    }


    @Override
    public void onRefresh() {
        mCouponListPresenter.refreshCouponList();
    }

    @Override
    public void onStop() {
        super.onStop();
        mCompositeDisposableHelper.clear();
    }

    @Override
    public void onCouponListRefreshed() {
        stopPullRefreshing();
    }
}
