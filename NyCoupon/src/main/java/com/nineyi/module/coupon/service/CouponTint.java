package com.nineyi.module.coupon.service;

import android.widget.ImageView;


/**
 * Created by ReeceCheng on 2017/11/15.
 */

public interface CouponTint {
    void setImageDrawableTint(ImageView v, int pressedColor, int normalColor);
}
