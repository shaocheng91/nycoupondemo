package com.nineyi.module.coupon.ui.list.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.nineyi.module.coupon.ui.view.CouponTicket;

/**
 * Created by shaocheng on 2017/10/6.
 */

public class CouponListItemCouponViewHolder extends CouponListItemViewHolder {
    public final CouponTicket mTicket;

    public CouponListItemCouponViewHolder(View itemView) {
        super(itemView);

        mTicket = (CouponTicket) itemView;
    }
}
