package com.nineyi.module.coupon.ui.list.wrapper;

import com.nineyi.module.coupon.ui.list.CouponListAdapter;

/**
 * Created by shaocheng on 2017/10/6.
 */

public class CouponListItemSeparator implements CouponListItem {
    private final String title;

    public CouponListItemSeparator(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int getViewType() {
        return CouponListAdapter.VIEW_TYPE_SEPARATOR;
    }
}
