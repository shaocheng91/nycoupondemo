package com.nineyi.module.coupon.ui.use.online.wrapper;

import com.nineyi.module.coupon.model.CouponOnline;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseItemCoupon implements CouponOnlineUseItem {
    private final CouponOnline mCoupon;
    private boolean mSelected;

    public CouponOnlineUseItemCoupon(CouponOnline coupon) {
        this.mCoupon = coupon;
    }

    public CouponOnline getCoupon() {
        return mCoupon;
    }

    public  void setSelected(boolean selected) {
        mSelected = selected;
    }

    public boolean isSelected() {
        return mSelected;
    }
}
