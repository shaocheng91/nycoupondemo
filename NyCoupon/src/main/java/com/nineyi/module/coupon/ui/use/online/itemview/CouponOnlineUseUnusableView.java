package com.nineyi.module.coupon.ui.use.online.itemview;

import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.service.CouponTint;
import com.nineyi.module.coupon.ui.use.online.OnOnlineCouponItemClickListener;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseUnusableView extends LinearLayout implements ICouponOnlineUseView {

    private CouponOnline mCoupon;

    OnOnlineCouponItemClickListener mClickListener;

    RadioButton mCheckBtn;
    TextView mReason;
    TextView mPrice;
    Button mToDetailBtn;

    CouponTint mCouponTint;

    public CouponOnlineUseUnusableView(Context context) {
        super(context);
        init(context);
    }

    public CouponOnlineUseUnusableView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponOnlineUseUnusableView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public CouponOnlineUseUnusableView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void setCouponTint(CouponTint couponTint) {
        mCouponTint = couponTint;

        ImageView reasonIcon = (ImageView) findViewById(R.id.coupon_online_use_unuseable_reason_icon);
        mCouponTint.setImageDrawableTint(reasonIcon, Color.parseColor("#999999"), Color.parseColor("#999999"));
    }

    private void init(Context context) {
        inflate(context, R.layout.coupon_online_use_item_unuseable_layout, this);

        mReason = (TextView) findViewById(R.id.coupon_online_use_unuseable_reason);
        mPrice = (TextView) findViewById(R.id.coupon_online_use_item_price);
        mToDetailBtn = (Button) findViewById(R.id.coupon_online_use_item_to_detail_btn);
        mCheckBtn = (RadioButton) findViewById(R.id.coupon_online_use_item_radio_btn);

        mToDetailBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onClickToDetail(mCoupon);
            }
        });

        mCheckBtn.setEnabled(false);
    }

    public void setOnOnlineCouponItemClickListener(OnOnlineCouponItemClickListener listener) {
        mClickListener = listener;
    }

    @Override
    public void showCoupon(final CouponOnline coupon, boolean isSelected, int position) {
        mCoupon = coupon;
        mPrice.setText(getContext().getString(R.string.taiwan_price_format, coupon.getDiscountPrice()));

        // TODO 線下使用

        if(!coupon.isAchieveUsingMinPrice()) {
            mReason.setText(getContext().getString(R.string.coupon_online_use_unusable_reason, coupon.getECouponUsingMinPrice()));
        }
    }
}