package com.nineyi.module.coupon.ui.list;

import com.nineyi.module.base.ui.UiUtils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_SEPARATOR;


/**
 * Created by ReeceCheng on 2017/10/3.
 */

public class CouponListDecoration extends RecyclerView.ItemDecoration {
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int decoration = UiUtils.convertDipToPixel(10, view.getContext().getResources().getDisplayMetrics());

        int position = parent.getChildAdapterPosition(view);

        int viewType = parent.getAdapter().getItemViewType(position);
        int size = parent.getAdapter().getItemCount();

        if (viewType == VIEW_TYPE_SEPARATOR) {
            if (position == 0) {
                outRect.top = UiUtils.convertDipToPixel(15, view.getContext().getResources().getDisplayMetrics());
            } else {
                outRect.top = UiUtils.convertDipToPixel(50, view.getContext().getResources().getDisplayMetrics());
            }
        } else {
            if (position != 0) {
                outRect.top = decoration;
            }
        }

        if (position == size - 1) {
            outRect.bottom = UiUtils.convertDipToPixel(20, view.getContext().getResources().getDisplayMetrics());
        }

        outRect.left = decoration;
        outRect.right = decoration;
    }
}
