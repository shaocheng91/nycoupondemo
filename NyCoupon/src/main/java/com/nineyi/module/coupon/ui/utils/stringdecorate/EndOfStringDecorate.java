package com.nineyi.module.coupon.ui.utils.stringdecorate;

public interface EndOfStringDecorate {

    CharSequence getCharSequence();
}