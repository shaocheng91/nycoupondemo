package com.nineyi.module.coupon.ui.detail;

import android.content.Context;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.coupon.R;

/**
 * Created by shaocheng on 2017/10/16.
 */

public class DetailItemView extends LinearLayout {

    TextView mTitle;
    TextView mContent;

    public DetailItemView(@NonNull Context context) {
        super(context);
        init();
    }

    public DetailItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DetailItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DetailItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.detail_item, this);

        setOrientation(VERTICAL);
        setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.white)); // TODO Don't use system white

        int topBottomPadding = UiUtils.convertDipToPixel(8, getResources().getDisplayMetrics());
        int leftRightPadding = UiUtils.convertDipToPixel(10, getResources().getDisplayMetrics());
        setPadding(leftRightPadding, topBottomPadding, leftRightPadding, topBottomPadding);

        mTitle = (TextView) findViewById(R.id.detail_item_title);
        mContent = (TextView) findViewById(R.id.detail_item_content);
    }

    public void setTitleAndContent(String title, CharSequence content) {
        mTitle.setText(title);
        mContent.setText(content);
    }
}
