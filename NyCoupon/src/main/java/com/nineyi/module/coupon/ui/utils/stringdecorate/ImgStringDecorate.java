package com.nineyi.module.coupon.ui.utils.stringdecorate;

import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;


public class ImgStringDecorate extends StringDecorate {

    Drawable mSearchBitmap;

    public ImgStringDecorate(Drawable searchBitmap, EndOfStringDecorate specialRoom) {
        super(specialRoom);
        mSearchBitmap = searchBitmap;
    }

    @Override
    public CharSequence getCharSequence() {
        SpannableStringBuilder ssb = new SpannableStringBuilder(" ");
        ssb.setSpan(new ImageSpan(mSearchBitmap, ImageSpan.ALIGN_BASELINE), 0, ssb.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return TextUtils.concat(mNextStringDecorate.getCharSequence(), ssb);
    }
}