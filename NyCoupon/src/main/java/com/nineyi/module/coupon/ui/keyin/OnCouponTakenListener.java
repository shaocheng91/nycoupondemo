package com.nineyi.module.coupon.ui.keyin;

/**
 * Created by shaocheng on 2017/9/22.
 */

public interface OnCouponTakenListener {
    void onCouponTaken();
}
