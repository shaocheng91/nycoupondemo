package com.nineyi.module.coupon.ui.list.viewholder;

import android.view.View;
import android.widget.TextView;

import com.nineyi.module.coupon.R;


/**
 * Created by ReeceCheng on 2017/10/3.
 */

public class CouponListItemSeparatorViewHolder extends CouponListItemViewHolder {

    public TextView mTitle;

    public CouponListItemSeparatorViewHolder(View itemView) {
        super(itemView);

        mTitle = (TextView) itemView.findViewById(R.id.coupon_list_item_title);
    }
}
