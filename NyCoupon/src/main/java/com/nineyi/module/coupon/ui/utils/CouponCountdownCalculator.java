package com.nineyi.module.coupon.ui.utils;

/**
 * Created by ReeceCheng on 2017/10/12.
 */

public class CouponCountdownCalculator {

    public String calculateLeftTime(long endTime, long systemTime) {
        int hour;
        int min;
        int sec;

        if(endTime >= systemTime) {
            Long second = (endTime - systemTime) / 1000;
            hour = (int)(second / 60 / 60);
            min = (int)(second / 60 % 60);
            sec = (int)(second % 60);
        } else {
            hour = 0;
            min = 0;
            sec = 0;
        }

        return String.format("%02d : %02d : %02d", hour, min, sec);
    }

}
