package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItem;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCoupon;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponInformation;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponNotUse;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUseable;

import android.support.annotation.VisibleForTesting;

import java.util.List;


/**
 * Created by ReeceCheng on 2017/10/23.
 */

public class CouponOnlineUseAdapterController {

    @VisibleForTesting
    void findDefaultSelected(CouponOnlineUseAdapter adapter, CouponOnlineUseContract.Presenter presenter, List<CouponOnlineUseItem> data) {
        if(data.isEmpty()) {
            return;
        }

        int selectedCouponId = presenter.getSelectedCouponId();

        for(int i=0; i<data.size(); i++) {
            CouponOnlineUseItem item = data.get(i);

            if(item instanceof CouponOnlineUseItemCouponUseable) {
                CouponOnlineUseItemCoupon couponItem = (CouponOnlineUseItemCoupon) item;

                if (couponItem.getCoupon().getId() == selectedCouponId) {
                    adapter.selectDefault(couponItem, i);
                    return;
                }

            } else if(item instanceof CouponOnlineUseItemCouponNotUse && selectedCouponId == presenter.getNotUseCouponId()) { // 0 is 不使用
                adapter.selectDefault((CouponOnlineUseItemCoupon)item, i);
                return;
            }

        }
    }

    protected void toggleInfoView(CouponOnlineUseAdapter adapter, CouponOnlineUseAdapter.LastSelected lastSelected, CouponOnline coupon, List<CouponOnlineUseItem> data, int position) {
        if(lastSelected.getPosition() == position) {
            return;
        }

        int selectedPosition = position;

        unselectLastItem(adapter, lastSelected);
        CouponOnlineUseItem newItem = selectNewItem(adapter, data, selectedPosition);
        removeLastInfoView(adapter, data, lastSelected);
        selectedPosition = resetPosition(data, lastSelected, selectedPosition, newItem);
        int infoViewPosition = insertNewInfoView(adapter, data, coupon, selectedPosition);
        saveLastSelected(coupon, newItem, lastSelected, selectedPosition, infoViewPosition);
    }

    @VisibleForTesting
    void unselectLastItem(CouponOnlineUseAdapter adapter, CouponOnlineUseAdapter.LastSelected lastSelected) {
        // remove selected to last item
        if(lastSelected.hasItem()) {
            CouponOnlineUseItem item = lastSelected.getItem();
            if(item instanceof CouponOnlineUseItemCoupon) {
                ((CouponOnlineUseItemCoupon) item).setSelected(false);
                adapter.notifyItemChanged(lastSelected.getPosition());
            }
        }
    }

    @VisibleForTesting
    CouponOnlineUseItem selectNewItem(CouponOnlineUseAdapter adapter, List<CouponOnlineUseItem> data, int selectedPosition) {
        CouponOnlineUseItem newItem = data.get(selectedPosition);
        if(newItem instanceof CouponOnlineUseItemCoupon) {
            ((CouponOnlineUseItemCoupon) newItem).setSelected(true);
            adapter.notifyItemChanged(selectedPosition);
        }

        return newItem;
    }

    @VisibleForTesting
    void removeLastInfoView(CouponOnlineUseAdapter adapter, List<CouponOnlineUseItem> data, CouponOnlineUseAdapter.LastSelected lastSelected) {
        if (lastSelected.hasInfoView()) {
            data.remove(lastSelected.getInfoViewPosition());
            adapter.notifyItemRemoved(lastSelected.getInfoViewPosition());
            adapter.notifyItemRangeChanged(lastSelected.getPosition(), adapter.getItemCount());
        }
    }

    @VisibleForTesting
    int resetPosition(List<CouponOnlineUseItem> data, CouponOnlineUseAdapter.LastSelected lastSelected, int selectedPosition, CouponOnlineUseItem newItem) {
        if (lastSelected.hasInfoView()) {
            for (int i = 0; i < data.size(); i++) {
                CouponOnlineUseItem item = data.get(i);
                if (item instanceof CouponOnlineUseItemCoupon && newItem instanceof CouponOnlineUseItemCoupon) {
                    if (((CouponOnlineUseItemCoupon) item).isSelected() == ((CouponOnlineUseItemCoupon) newItem).isSelected()) {
                        return i;
                    }

                }
            }
        }

        return selectedPosition;
    }

    @VisibleForTesting
    int insertNewInfoView(CouponOnlineUseAdapter adapter, List<CouponOnlineUseItem> data, CouponOnline coupon, int selectedPosition) {
        if(coupon != null) {
            // after coupon view
            int infoViewPosition = selectedPosition + 1;

            data.add(infoViewPosition, new CouponOnlineUseItemCouponInformation(coupon));
            adapter.notifyItemInserted(infoViewPosition);
            adapter.notifyItemRangeChanged(selectedPosition, adapter.getItemCount());

            return infoViewPosition;
        }

        return selectedPosition;
    }

    @VisibleForTesting
    void saveLastSelected(CouponOnline coupon, CouponOnlineUseItem newItem, CouponOnlineUseAdapter.LastSelected lastSelected, int selectedPosition, int infoViewPosition) {
        if(coupon != null) {
            lastSelected.setItem(newItem);
            lastSelected.setPosition(selectedPosition);
            lastSelected.setInfoViewPosition(infoViewPosition);
            lastSelected.setHasInfoView(true);
        } else {
            lastSelected.setItem(newItem);
            lastSelected.setPosition(selectedPosition);
            lastSelected.setInfoViewPosition(CouponOnlineUseAdapter.LastSelected.NO_POSITION);
            lastSelected.setHasInfoView(false);
        }
    }
}
