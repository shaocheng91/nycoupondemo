package com.nineyi.module.coupon.ui.detail;

import android.app.Activity;

import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.ui.BaseView;

/**
 * Created by shaocheng on 2017/10/16.
 */
public interface DetailContract {

    String ARG_FROM_SHOPPING_CART = "arg_from_shopping_cart";
    String ARG_FROM_OTHER = "arg_from_other";

    enum Action {
        COLLECT,
        USE_OFFLINE,
        USE_ONLINE,
        NONE
    }

    interface OnShowMenuListener {
        void showMenu(boolean show);
    }

    interface View extends BaseView<DetailContract.Presenter> {
        void showLoading();
        void showDetail(Coupon coupon);
        void showAction(DetailContract.Action action);
        void showAction(DetailContract.Action action, int reasonStringId);
        void hideAction();
        boolean clickOnMenu(Activity activity, int menuItemId);
        void showShareButton(boolean showButton);
        void showActionLoading();
        void showCollectSuccess();
        void showCollectError(int stringId, String serverMessage);
        void goToLogin();
        void goToHome();
        void goToVerifyCoupon();
        void showEmpty();
        void showError();
        void showAlert(int stringId);
        void showDetailError();
        void scheduleToRefresh(long usingEndDateTime);
        void onResume();
        void onPause();
    }

    interface Presenter {
        void loadCouponDetail(boolean mustCallGetStatusApi);
        void doAction(Action action);
    }
}
