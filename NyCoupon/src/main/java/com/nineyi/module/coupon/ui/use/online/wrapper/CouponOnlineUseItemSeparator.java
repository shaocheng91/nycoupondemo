package com.nineyi.module.coupon.ui.use.online.wrapper;

/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseItemSeparator implements CouponOnlineUseItem {
    private final String mTitle;

    public CouponOnlineUseItemSeparator(String title) {
        this.mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }
}
