package com.nineyi.module.coupon.ui.list;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.CollectCouponException;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.GetCouponListException;
import com.nineyi.module.coupon.service.LoginManager;
import com.nineyi.module.coupon.service.MigrationManager;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.service.PendingCouponHandler;
import com.nineyi.module.coupon.service.PromotionSharePreferenceHelper;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemCouponAvailable;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemEmpty;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemCoupon;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemSeparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_AVAILABLE;
import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_COLLECTED;
import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_INVALID;

/**
 * Created by shaocheng on 2017/9/22.
 */

public class CouponListPresenter implements CouponListContract.Presenter {

    public interface OnTakeCouponSuccessCallback {
        void onTakeCouponSuccess();
    }

    enum Group {
        COLLECTED,
        AVAILABLE,
        INVALID,
        HIDDEN
    }

    private final Context mContext;
    private final CouponManager mCouponManager;
    private final MigrationManager mMigrationManager;
    private final CouponListContract.View mView;
    private final PromotionSharePreferenceHelper mPromotionSharePreferenceHelper;
    private final CompositeDisposableHelper mCompositeDisposableHelper;
    private final LoginManager mLoginManager;
    private final NavManager mNavManager;
    private final PendingCouponHandler mPendingCouponHandler;

    private boolean mIsLoggedIn;
    private List<CouponListItem> mData;

    public CouponListPresenter(Context mContext, CouponManager couponManager, MigrationManager mMigrationManager,
            CouponListContract.View view, PromotionSharePreferenceHelper mPromotionSharePreferenceHelper,
            CompositeDisposableHelper mCompositeDisposableHelper, LoginManager mLoginManager, NavManager mNavManager,
            PendingCouponHandler pendingCouponHandler) {

        this.mContext = mContext;
        this.mCouponManager = couponManager;
        this.mMigrationManager = mMigrationManager;
        this.mView = view;
        this.mPromotionSharePreferenceHelper = mPromotionSharePreferenceHelper;
        this.mCompositeDisposableHelper = mCompositeDisposableHelper;
        this.mLoginManager = mLoginManager;
        this.mNavManager = mNavManager;
        this.mPendingCouponHandler = pendingCouponHandler;

        mIsLoggedIn = mLoginManager.isLogin();

        mView.setPresenter(this);
    }

    @Override
    public void refreshCouponList() {
        // TODO 改成 repo ...
        mCompositeDisposableHelper.add(mCouponManager.getCouponList().subscribeWith(new DisposableSingleObserver<List<Coupon>>() {
            @Override
            public void onSuccess(@NonNull List<Coupon> coupons) {

                mData = convertCouponToListItem(coupons);
                mView.showCouponList(mData);
            }

            @Override
            public void onError(@NonNull Throwable exception) {

                if (exception instanceof GetCouponListException) {
                    GetCouponListException e = (GetCouponListException) exception;
                    if (e.errorCode == GetCouponListException.ErrorCode.EMPTY) {
                        mView.showEmpty();
                        return;
                    }

                }
                mView.showError();
            }
        }));
    }

    @Override
    public void collectPendingCouponIfExists() {
        if (mPendingCouponHandler.hasPendingCouponToCollect()) {
            collectPendingCoupon();
        }
    }

    @Override
    public void loadCouponList(boolean forceReload) {
        if (mData == null || forceReload) { // TODO 會有BUG 先關掉 || isNowLoggedIn()) {
            mView.showLoading();

            refreshCouponList();
        }
    }

    private boolean isNowLoggedIn() {
        boolean previous = mIsLoggedIn;
        mIsLoggedIn = mLoginManager.isLogin();
        return !previous && mIsLoggedIn;
    }

    @VisibleForTesting
    List<CouponListItem> convertCouponToListItem(List<Coupon> coupons) {
        List<CouponListItem> all = new ArrayList<>();

        if (coupons.isEmpty()) {
            return all;
        }

        Map<Group, List<Coupon>> groupedCoupons = groupCoupons(coupons);

        List<Coupon> available = groupedCoupons.get(Group.AVAILABLE);
        List<Coupon> collected = groupedCoupons.get(Group.COLLECTED);

        if (available.isEmpty() && collected.isEmpty()) {
            all.add(new CouponListItemSeparator(mContext.getString(R.string.coupon_list_item_separator_title_available)));

            all.add(new CouponListItemEmpty());
        } else {
            all.add(new CouponListItemSeparator(mContext.getString(R.string.coupon_list_item_separator_title_available)));

            if (!available.isEmpty()) {

                Collections.sort(available, new AvailableCouponComparator());

                for (Coupon coupon: available) {
                    all.add(new CouponListItemCouponAvailable(VIEW_TYPE_AVAILABLE, coupon));
                }
            }

            if (!collected.isEmpty()) {

                Collections.sort(collected, new CollectedCouponComparator());

                for (Coupon coupon: collected) {
                    all.add(new CouponListItemCoupon(VIEW_TYPE_COLLECTED, coupon));
                }
            }
        }

        List<Coupon> invalid = groupedCoupons.get(Group.INVALID);
        if (!invalid.isEmpty()) {
            all.add(new CouponListItemSeparator(mContext.getString(R.string.coupon_list_item_separator_title_invalid)));

            for (Coupon coupon: invalid) {
                all.add(new CouponListItemCoupon(VIEW_TYPE_INVALID, coupon));
            }
        }

        return all;
    }

    private Map<Group, List<Coupon>> groupCoupons(List<Coupon> coupons) {
        Map<Group, List<Coupon>> groupedCoupons = new HashMap<>();

        groupedCoupons.put(Group.COLLECTED, new ArrayList<Coupon>());
        groupedCoupons.put(Group.AVAILABLE, new ArrayList<Coupon>());
        groupedCoupons.put(Group.INVALID, new ArrayList<Coupon>());
        groupedCoupons.put(Group.HIDDEN, new ArrayList<Coupon>());

        for (Coupon coupon : coupons) {
            Group groupKey = chooseGroupForCoupon(coupon);

            groupedCoupons.get(groupKey).add(coupon);
        }

        return groupedCoupons;
    }

    @VisibleForTesting
    Group chooseGroupForCoupon(Coupon coupon) {
        switch (coupon.getStatus()) {
            case BEFORE_USE_TIME:
            case COLLECTED:
                return Group.COLLECTED;
            case AVAILABLE:
                return Group.AVAILABLE;
            case FIRST_DOWNLOAD_NOT_QUALIFIED:
            case FIRST_DOWNLOAD_COLLECTED:
            case NO_MORE_COUPONS:
                return Group.INVALID;
            case AFTER_USE_TIME:
            case BEFORE_COLLECT_TIME:
            case AFTER_COLLECT_TIME:
            case USED:
            case UNKNOWN:
            default:
                return Group.HIDDEN;
        }
    }

    @Override
    public void collectCoupon(final Coupon coupon, final OnTakeCouponSuccessCallback callback) {
        if (mLoginManager.isLogin()) {

            showCouponListItemCollecting(coupon.getId());

            mCompositeDisposableHelper.add(
                    mCouponManager.takeCoupon(coupon.getId(), coupon.isFirstDownload()).subscribeWith(new DisposableCompletableObserver() {
                        @Override
                        public void onComplete() {
                            mView.showCollectCouponSuccess();
                            callback.onTakeCouponSuccess();
                            showCouponLIstItemCollected(coupon.getId());
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                            if (e instanceof CollectCouponException) {
                                CollectCouponException collectCouponException = (CollectCouponException) e;
                                switch (collectCouponException.errorType) {
                                    case INVALID:
                                        mView.showCollectCouponError(R.string.coupon_collect_error_first_download_invalid);
                                        break;
                                    case ALREADY_COLLECTED:
                                        mView.showCollectCouponError(R.string.coupon_collect_error_first_download_already_collected);
                                        break;
                                    case FAIL:
                                    case UNKNOWN:
                                    default:
                                        mView.showCollectCouponError(R.string.ecoupon_get_fail_title,
                                                collectCouponException.serverMessage);
                                }
                                return;
                            }

                            mView.showCollectCouponError(R.string.ecoupon_get_fail_title);

                            // TODO Other error handing (e.g. timeout)
                        }
                    }));

        } else {
            savePendingCoupon(coupon, callback);

            mNavManager.navigateLogin(mContext);
        }
    }

    private void showCouponListItemCollecting(int id) {
        CouponListItemCouponAvailable item = (CouponListItemCouponAvailable) findCouponListItemById(id);
        if (item != null) {
            item.setCollecting(true);
        }
        mView.showCouponList(mData);
    }

    private void showCouponLIstItemCollected(int couponId) {
        CouponListItemCouponAvailable item = (CouponListItemCouponAvailable) findCouponListItemById(couponId);
        if (item != null) {
            item.setCollected(true);
        }
        mView.showCouponList(mData);
    }

    private CouponListItemCoupon findCouponListItemById(int couponId) {
        if (mData != null) {
            for (int i = 0; i < mData.size(); i++) {
                CouponListItem item = mData.get(i);
                if (item instanceof CouponListItemCoupon && ((CouponListItemCoupon) item).getCoupon().getId() == couponId) {
                    return (CouponListItemCoupon) item;
                }
            }
        }
        return null;
    }

    private void savePendingCoupon(Coupon coupon, final OnTakeCouponSuccessCallback callback) {
        mPendingCouponHandler.savePendingCouponId(coupon.getId());
        mPendingCouponHandler.savePendingCouponIsFirstDownload(coupon.isFirstDownload());
        mPendingCouponHandler.setCallback(callback);
    }

    @VisibleForTesting
    void collectPendingCoupon() {
        if (!mLoginManager.isLogin()) {
            mPendingCouponHandler.clear();
            return;
        }

        final int couponId = mPendingCouponHandler.loadPendingCouponId();
        boolean isFirstDownload = mPendingCouponHandler.loadPendingCouponIsFirstDownload();

        showCouponListItemCollecting(couponId);

        mCompositeDisposableHelper.add(
                mCouponManager.takeCoupon(couponId, isFirstDownload).subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        mView.showCollectCouponSuccess();
                        showCouponLIstItemCollected(couponId);

                        if(mPendingCouponHandler.hasOnTakeCouponSuccessCallback()) {
                            mPendingCouponHandler.getCallback().onTakeCouponSuccess();
                            mPendingCouponHandler.removeCallback();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (e instanceof CollectCouponException) {
                            CollectCouponException collectCouponException = (CollectCouponException) e;
                            switch (collectCouponException.errorType) {
                                case INVALID:
                                    mView.showCollectCouponError(R.string.coupon_collect_error_first_download_invalid);
                                    break;
                                case ALREADY_COLLECTED:
                                    mView.showCollectCouponError(R.string.coupon_collect_error_first_download_already_collected);
                                    break;
                                case FAIL:
                                case UNKNOWN:
                                default:
                                    mView.showCollectCouponError(R.string.ecoupon_get_fail_title,
                                            collectCouponException.serverMessage);
                            }
                            return;
                        }

                        mView.showCollectCouponError(R.string.ecoupon_get_fail_title);

                        // TODO Other error handing (e.g. timeout)
                    }
                }));

        mPendingCouponHandler.clear();
    }

    @Override
    public boolean isLoggedIn() {
        return mLoginManager.isLogin();
    }

    // TODO Might change in future
    @VisibleForTesting
    static class CollectedCouponComparator implements Comparator<Coupon> {
        @Override
        public int compare(Coupon o1, Coupon o2) {
            long x = o1.getUsingEndDateTime().getTimeLong();
            long y = o2.getUsingEndDateTime().getTimeLong();
            return (x < y) ? -1 : ((x == y) ? 0 : 1);
        }
    }

    // TODO Might change in future
    @VisibleForTesting
    static class AvailableCouponComparator implements Comparator<Coupon> {
        @Override
        public int compare(Coupon o1, Coupon o2) {
            long x = o1.getEndDateTime().getTimeLong();
            long y = o2.getEndDateTime().getTimeLong();
            return (x < y) ? -1 : ((x == y) ? 0 : 1);
        }
    }
}
