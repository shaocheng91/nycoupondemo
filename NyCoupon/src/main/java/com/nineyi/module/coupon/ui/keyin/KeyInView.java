package com.nineyi.module.coupon.ui.keyin;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.MsgManager;


/**
 * Created by ReeceCheng on 2016/1/26.
 */
public class KeyInView extends LinearLayout
        implements KeyInContract.View, View.OnClickListener {

    EditText mKeyInInput;
    Button mKeyInButton;
    ViewGroup mWarningPanel;
    ViewGroup mInputPanel;

    MsgManager mMsgManager;

    private KeyInContract.Presenter mPresenter;

    public KeyInView(Context context) {
        super(context);
        init();
    }

    public KeyInView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyInView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public KeyInView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setOrientation(LinearLayout.VERTICAL);

        inflate(getContext(), R.layout.keyin, this);

        mWarningPanel = (ViewGroup) findViewById(R.id.coupon_keyin_warning_panel);
        mInputPanel = (ViewGroup) findViewById(R.id.coupon_keyin_input_panel);

        mKeyInInput = (EditText) findViewById(R.id.ecoupon_keyin_input);

        mKeyInInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s != null) {
                    String value = s.toString();
                    if (!value.toUpperCase().equals(value)) {
                        mKeyInInput.setText(value.toUpperCase());
                    }
                }
            }
        });

        Spannable sFirst = new SpannableString(getResources().getString(R.string.ecoupon_keyin_hint_first));
        Spannable sSecond = new SpannableString(getResources().getString(R.string.ecoupon_keyin_hint_second));
        Spannable sThird = new SpannableString(getResources().getString(R.string.ecoupon_keyin_hint_third));

        sFirst.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.ecoupon_topbar_inputhint_textcolor)),
                0, sFirst.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sSecond.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.font_color_price)),
                0, sSecond.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sThird.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.ecoupon_topbar_inputhint_textcolor)),
                0, sThird.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mKeyInInput.setHint(TextUtils.concat(sFirst, sSecond, sThird));

        mKeyInButton = (Button) findViewById(R.id.ecoupon_keyin_get_button);
        mKeyInButton.setOnClickListener(this);

        // TODO
//        Drawable d = AppCompatTintUtil.getTintDrawable(R.drawable.btn_ecoupon_keyin_take,
//                NineYiColor.getRelatedSalesColor(), NineYiColor.getRelatedSalesColor());
//
//        AppCompatTintUtil.setBackgroundDrawable(keyinBtn, d);
    }

    @Override
    public void setPresenter(KeyInContract.Presenter presenter) {
        mPresenter = presenter;
    }

    public void setMsgManager(MsgManager mMsgManager) {
        this.mMsgManager = mMsgManager;
    }

    @Override
    public void showTakeCouponError(String message) {
        mMsgManager.showShortToast(getContext(), message);
    }

    @Override
    public void showNetworkError() {
        // TODO
        //        ECouponDescriptionDialog.show(getContext(), getContext().getString(R.string.ecoupon_keyin_dialog_fail), "",
//                getContext().getString(R.string.ok),
//                getContext().getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String guid = ECouponUtils.getGUID(getContext());
//                        callSetMemberECouponByCode(guid);
//                    }
//                }, new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                    }
//                });
        mMsgManager.showShortToast(getContext(), getContext().getString(R.string.ecoupon_get_fail_title));
    }

    @Override
    public void showTakingCoupon() {
        mMsgManager.showShortToast(getContext(), getContext().getString(R.string.ecoupon_keyin_loading));
    }

    // Pick click listener
    @Override
    public void onClick(View v) {
        String input = mKeyInInput.getText().toString();
        if("".equals(input)) {
            String emptyAlert = getContext().getString(R.string.ecoupon_keyin_empty_alert);
            mMsgManager.showShortToast(getContext(), emptyAlert);
        } else {
            mPresenter.takeCoupon(input);
        }
    }

    @Override
    public void hideKeyboard() {
        if (mKeyInInput.hasFocus()) {
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mKeyInInput.getWindowToken(), 0);
        }
    }

    @Override
    public void showWarning(boolean show) {
        if (show) {
            mWarningPanel.setVisibility(VISIBLE);
        } else {
            mWarningPanel.setVisibility(GONE);

            int topMargin = UiUtils.convertDipToPixel(15, getResources().getDisplayMetrics());
            ((LinearLayout.LayoutParams) mInputPanel.getLayoutParams()).setMargins(0, topMargin, 0, 0);
        }
    }

    @Override
    public void showTakeCouponSuccess() {
        mMsgManager.showShortToast(getContext(), getContext().getString(R.string.ecoupon_get_title));

        mKeyInInput.setText("");

    }

}
