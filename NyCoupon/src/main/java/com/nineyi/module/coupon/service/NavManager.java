package com.nineyi.module.coupon.service;

import android.content.Context;

/**
 * Created by shaocheng on 2017/10/11.
 */

public interface NavManager {
    void navigateLogin(Context context);
}
