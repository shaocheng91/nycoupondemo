package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.base.provider.tint.TintProvider;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.base.retrofit.appcompat.NyActionBarActivity;
import com.nineyi.module.base.ui.NineYiColor;
import com.nineyi.module.coupon.CouponComponent;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.CouponShoppingCartDataManager;
import com.nineyi.module.coupon.service.CouponTint;
import com.nineyi.module.coupon.service.LoginManager;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.ui.keyin.KeyInPresenter;
import com.nineyi.module.coupon.ui.keyin.KeyInView;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;


/**
 * Created by ReeceCheng on 2017/10/16.
 */

public class CouponOnlineUseActivity extends NyActionBarActivity {

    CompositeDisposableHelper mCompositeDisposableHelper = new CompositeDisposableHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CouponComponent couponComponent = CouponComponent.get();

        CouponManager couponManager = couponComponent.couponManager();
        CouponShoppingCartDataManager couponShoppingCartDataManager = couponComponent.getCouponShoppingCartDataManager();
        MsgManager msgManager = couponComponent.msgManager();
        LoginManager loginManager = couponComponent.loginManager();
        CouponTint couponTint = couponComponent.getCouponTint();

        setContentView(R.layout.coupon_online_use_layout);
        initialToolbar();

        LinearLayout container = (LinearLayout) findViewById(R.id.coupon_online_use_layout_container);

        KeyInView keyInView = new KeyInView(this);
        keyInView.setMsgManager(msgManager);

        KeyInPresenter keyInPresenter = new KeyInPresenter(couponManager, loginManager, keyInView, mCompositeDisposableHelper, true);
        keyInView.setPresenter(keyInPresenter);

        container.addView(keyInView);

        CouponOnlineUseView view = new CouponOnlineUseView(this, couponTint);

        final CouponOnlineUsePresenter presenter = new CouponOnlineUsePresenter(
                CouponOnlineUseActivity.this,
                view,
                couponManager,
                couponShoppingCartDataManager,
                mCompositeDisposableHelper,
                mSubmitListener);

        view.setPresenter(presenter);

        Button submitButton = (Button) findViewById(R.id.coupon_online_use_submit_button);
        submitButton.setText(getString(R.string.coupon_online_use_submit_no_use));
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.submit();
            }
        });

        container.addView(view, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        presenter.getCouponList();
    }


    private void initialToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            setActionBarTitle(getString(R.string.coupon_online_use_title));
            setHomeAsUp(toolbar);

            toolbar.setNavigationIcon(TintProvider.getInstance()
                    .getTintDrawable(getResources().getDrawable(R.drawable.btn_navi_back),
                            NineYiColor.getGlobalNaviIconColor(), NineYiColor.getGlobalNaviIconColor()));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mCompositeDisposableHelper.clear();
    }

    CouponOnlineUsePresenter.OnCouponSelectedSubmitListener mSubmitListener = new CouponOnlineUsePresenter.OnCouponSelectedSubmitListener() {
        @Override
        public void onSubmit() {
            finish();
        }
    };

}
