package com.nineyi.module.coupon.ui.use.online.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public abstract class CouponOnlineUseViewHolder<T> extends RecyclerView.ViewHolder {

    public CouponOnlineUseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindData(T data, int position);
}
