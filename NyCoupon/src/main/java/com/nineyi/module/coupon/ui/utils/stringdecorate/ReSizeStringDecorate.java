package com.nineyi.module.coupon.ui.utils.stringdecorate;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;


public class ReSizeStringDecorate extends StringDecorate {

    private int mCharacterSize;

    public ReSizeStringDecorate(EndOfStringDecorate enLargeString, int characterSize) {
        super(enLargeString);
        mCharacterSize = characterSize;
    }

    @Override
    public CharSequence getCharSequence() {
        SpannableString ss = new SpannableString(mNextStringDecorate.getCharSequence());
        ss.setSpan(new AbsoluteSizeSpan(mCharacterSize, true), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

}