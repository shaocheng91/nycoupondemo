package com.nineyi.module.coupon.ui.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.base.retrofit.RetrofitActionBarFragment;
import com.nineyi.module.coupon.CouponComponent;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.CouponShareHelper;
import com.nineyi.module.coupon.service.LoginManager;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.service.NavManager;


/**
 * Created by shaocheng on 2017/9/22.
 */

public class DetailFragment extends RetrofitActionBarFragment implements DetailContract.OnShowMenuListener {

    private static final String ARGS_KEY_COUPON_ID = "coupon_id";
    private static final String ARGS_KEY_FROM = "from";

    private DetailView mDetailView;
    private DetailPresenter mDetailPresenter;

    // 這邊宣告成 Boxed boolean 是為了解決 onCreateOptionsMenu() 和 showMenu() 可能會有
    // race condition 的問題
    private Boolean mShowMenu;
    private MenuItem mShareItem;

    public static DetailFragment newInstance(int couponId, String from) {
        DetailFragment f = new DetailFragment();

        Bundle args = new Bundle();
        args.putInt(ARGS_KEY_COUPON_ID, couponId);
        args.putString(ARGS_KEY_FROM, from);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        setActionBarTitle(R.string.title_coupon_detail);

        CouponComponent couponComponent = CouponComponent.get();
        CouponManager couponManager = couponComponent.couponManager();
        CouponShareHelper shareHelper = couponComponent.shareHelper();
        MsgManager msgManager = couponComponent.msgManager();
        NavManager navManager = couponComponent.navManager();
        LoginManager loginManager = couponComponent.loginManager();

        mDetailView = new DetailView(getActivity());
        mDetailView.setShareHelper(shareHelper);
        mDetailView.setMenuDelegate(this);
        mDetailView.setMsgManager(msgManager);
        mDetailView.setNavManager(navManager);

        int couponId = getArguments().getInt(ARGS_KEY_COUPON_ID);
        String from = getArguments().getString(ARGS_KEY_FROM);
        mDetailPresenter = new DetailPresenter(mDetailView, new CompositeDisposableHelper(), couponManager, couponId, from, loginManager);

        mDetailView.setPresenter(mDetailPresenter);

        return mDetailView;
    }

    @Override
    public void onResume() {
        super.onResume();

        mDetailView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        mDetailView.onPause();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.coupon_detail_share_type, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (!getUserVisibleHint()) {
            return false;
        }

        mDetailView.clickOnMenu(getActivity(), item.getItemId());
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mNineyiMenuHelper.intMenu(false, false);

        inflater.inflate(R.menu.coupon_detail_menu, menu);

        mShareItem = menu.findItem(R.id.action_navi_share);
        registerForContextMenu(mShareItem.getActionView());

        mShareItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.showContextMenu();
            }
        });

        mShareItem.setVisible(false);

        // TODO Tint
//            ImageButton imgBtn = (ImageButton) mShareItem.getActionView().findViewById(R.id.share_icon_imagebutton);

//            AppCompatTintUtil.setImageDrawableTint(imgBtn,
//                    NineYiColor.getGlobalNaviIconColor(),
//                    NineYiColor.getGlobalNaviIconColor());

        // 解 race condition - 要 mShowMenu 和 mShareItem 同時好了再處理下一步
        if (mShowMenu != null) {
            mShareItem.setVisible(mShowMenu);
        }
    }

    @Override
    public void showMenu(boolean show) {
        mShowMenu = show;

        // 解 race condition - 要 mShowMenu 和 mShareItem 同時好了再處理下一步
        if (mShareItem != null) {
            mShareItem.setVisible(show);
        }
    }
}
