package com.nineyi.module.coupon.ui.utils;

import com.google.zxing.BarcodeFormat;

import com.nineyi.module.base.ui.UiUtils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;


/**
 * Created by ReeceCheng on 2017/11/6.
 */

public class CouponBarcodeHelper {
    public void genBarcode(Context context, String code, String codeType, int genId, CouponBarcodeGenerator.GenFinishedCallback callback) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int widthPixels = displayMetrics.widthPixels;

        try {

            BarcodeFormat format = BarcodeFormat.valueOf(codeType.toUpperCase());

            CouponBarcodeGenerator generator = new CouponBarcodeGenerator(widthPixels, UiUtils
                    .convertDipToPixel(40, displayMetrics), format, genId);

            generator.setCallback(callback);
            generator.gen(code);
        } catch (Exception e) {
            callback.onGenError(code, genId);
        }

    }
}
