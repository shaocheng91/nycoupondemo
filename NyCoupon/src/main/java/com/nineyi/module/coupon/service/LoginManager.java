package com.nineyi.module.coupon.service;

/**
 * Created by shaocheng on 2017/9/28.
 */

public interface LoginManager {
    boolean isLogin();
}
