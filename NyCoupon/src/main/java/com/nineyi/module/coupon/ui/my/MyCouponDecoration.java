package com.nineyi.module.coupon.ui.my;

import com.nineyi.module.base.ui.UiUtils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/3.
 */

public class MyCouponDecoration extends RecyclerView.ItemDecoration {
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int decoration = UiUtils.convertDipToPixel(10, view.getContext().getResources().getDisplayMetrics());

        int position = parent.getChildAdapterPosition(view);

        if(position == 0) {
            outRect.top = UiUtils.convertDipToPixel(15, view.getContext().getResources().getDisplayMetrics());
        }

        outRect.left = decoration;
        outRect.right = decoration;
    }
}
