package com.nineyi.module.coupon.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.ui.utils.ECouponUtils;

import java.util.Date;


/**
 * Created by ReeceCheng on 2017/9/29.
 */

public class CouponTicketInvalid extends CardView implements CouponTicket {

    TextView mCouponTitle;
    TextView mUseTagOffline;
    TextView mUseTagOnline;
    TextView mPrice;
    TextView mRule;
    TextView mEndTime;
    TextView mStatus;

    private Coupon mCoupon;

    public CouponTicketInvalid(Context context) {
        super(context);
        init(context);
    }

    public CouponTicketInvalid(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponTicketInvalid(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {

        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UiUtils.convertDipToPixel(137, getResources().getDisplayMetrics()));
        setLayoutParams(params);

        setRadius(UiUtils.convertDipToPixel(5, getResources().getDisplayMetrics()));

        inflate(context, R.layout.coupon_ticket_invalid, this);

        mCouponTitle = (TextView) findViewById(R.id.coupon_list_item_title);
        mUseTagOffline = (TextView) findViewById(R.id.coupon_list_item_use_tag_offline);
        mUseTagOnline = (TextView) findViewById(R.id.coupon_list_item_use_tag_online);
        mPrice = (TextView) findViewById(R.id.coupon_list_item_price);
        mRule = (TextView) findViewById(R.id.coupon_list_item_rule);
        mEndTime = (TextView) findViewById(R.id.coupon_list_item_end_time);
        mStatus = (TextView) findViewById(R.id.coupon_list_item_status);
    }

    @Override
    public void showCoupon(Coupon coupon) {

        mCoupon = coupon;

        if (coupon.isFirstDownload()) {
            mCouponTitle.setText(R.string.coupon_list_item_title_first_download);
        } else {
            mCouponTitle.setText(R.string.coupon_list_item_title);
        }

        if (mCoupon.isOnline()) {
            mUseTagOnline.setVisibility(VISIBLE);
        } else {
            mUseTagOnline.setVisibility(GONE);
        }

        if (mCoupon.isOffline()) {
            mUseTagOffline.setVisibility(VISIBLE);
        } else {
            mUseTagOffline.setVisibility(GONE);
        }

        // TODO Will it be too long?
        mPrice.setText(getContext().getString(R.string.taiwan_price_format, coupon.getDiscountPrice()));

        mRule.setText(getResources().getString(R.string.coupon_list_item_rule, coupon.getECouponUsingMinPrice()));

        if (coupon.isHasNormalCoupon()) {
            // TODO 有可能嗎？
            String useEnd = ECouponUtils.getFormatDateYearMonthDay(getContext(), new Date(coupon.getUsingEndDateTime().getTimeLong()));
            String useEndString = getContext().getString(R.string.coupon_list_item_use_end_time, useEnd);

            mEndTime.setText(useEndString);
        } else {
            String takeEnd = ECouponUtils.getFormatDateYearMonthDay(getContext(), new Date(coupon.getEndDateTime().getTimeLong()));
            String takeEndString = getContext().getString(R.string.coupon_list_item_take_end_time, takeEnd);

            mEndTime.setText(takeEndString);
        }

        switch (coupon.getStatus()) {
            case FIRST_DOWNLOAD_COLLECTED:
            case FIRST_DOWNLOAD_NOT_QUALIFIED:
                mStatus.setText(R.string.coupon_list_item_status_invalidate);
                break;
            case NO_MORE_COUPONS:
                mStatus.setText(R.string.coupon_list_item_status_out_of_stock);
                break;
            default:
                // 列表頁不可能，詳細頁不顯示
                mStatus.setText("");
        }
    }

    @Override
    public void setOnClickCouponListener(final OnClickCouponListener listener) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickCoupon(mCoupon);
            }
        });
    }

    public void hideStatus() {
        mStatus.setVisibility(GONE);
    }
}
