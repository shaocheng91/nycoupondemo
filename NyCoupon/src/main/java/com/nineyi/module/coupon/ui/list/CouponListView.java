package com.nineyi.module.coupon.ui.list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;

import java.util.List;


/**
 * Created by shaocheng on 2017/9/22.
 */

public class CouponListView extends RelativeLayout implements CouponListContract.View {

    public interface OnCouponListRefreshedListener {
        void onCouponListRefreshed();
    }

    View mErrorView;
    Button mErrorActionButton;

    RecyclerView mRecyclerView;
    ProgressBar mProgressBar;
    View mEmptyView;

    private CouponListContract.Presenter mPresenter;
    private MsgManager mMsgManager;
    private CouponListAdapter mAdapter;
    private CountdownManager mCountdownManager;
    private NavManager mNavManager;
    private OnCouponListRefreshedListener mOnCouponListRefreshedListener;

    public CouponListView(Context context) {
        super(context);
        init(context);
    }

    public CouponListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CouponListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.coupon_list, this);

        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.bg_coupon_detail));

        mEmptyView = findViewById(R.id.coupon_list_empty);

        mProgressBar = (ProgressBar) findViewById(R.id.coupon_list_progressbar);

        mErrorView = findViewById(R.id.coupon_list_error);
        mErrorActionButton = (Button) findViewById(R.id.coupon_common_action_button);
        mErrorActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.loadCouponList(true);
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.coupon_list_content);
        mRecyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        mRecyclerView.addItemDecoration(new CouponListDecoration());

        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);

        mCountdownManager = new CountdownManager();
    }

    @Override
    public void setPresenter(CouponListContract.Presenter presenter) {
        mPresenter = presenter;
        mAdapter = new CouponListAdapter(getContext(), mPresenter, mCountdownManager, mNavManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void setMsgManager(MsgManager MsgManager) {
        this.mMsgManager = MsgManager;
    }

    public void setNavManager(NavManager navManager) {
        this.mNavManager = navManager;
    }

    public void setOnCouponListRefreshedListener(OnCouponListRefreshedListener onCouponListRefreshedListener) {
        this.mOnCouponListRefreshedListener = onCouponListRefreshedListener;
    }

    @Override
    public void showCouponList(List<CouponListItem> wrappers) {
        if (wrappers.isEmpty()) {
            showOnlyChildView(mEmptyView);
        } else {
            mAdapter.setData(wrappers);
            mAdapter.notifyDataSetChanged();

            showOnlyChildView(mRecyclerView);
        }

        mOnCouponListRefreshedListener.onCouponListRefreshed();
    }

    @Override
    public void showCollectCouponSuccess() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showCollectCouponError(int titleStringId) {
        showCollectCouponError(titleStringId, null);
    }

    @Override
    public void showCollectCouponError(int titleStringId, String message) {

        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(titleStringId);

        if (message != null && !message.isEmpty()) {
            b.setMessage(message);
        }

        b.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.loadCouponList(true);
            }
        });

        b.show();
    }

    @Override
    public void showLoading() {
        showOnlyChildView(mProgressBar);
    }

    @Override
    public void showError() {
        showOnlyChildView(mErrorView);
        mOnCouponListRefreshedListener.onCouponListRefreshed();
    }

    @Override
    public void showEmpty() {
        showOnlyChildView(mEmptyView);
        mOnCouponListRefreshedListener.onCouponListRefreshed();
    }

    private void showOnlyChildView(View child) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View c = getChildAt(i);
            if (c == child) {
                c.setVisibility(VISIBLE);
            } else {
                c.setVisibility(GONE);
            }
        }
    }

    @Override
    public void showTakingCoupon() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        mAdapter.clear();
    }
}
