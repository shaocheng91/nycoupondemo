package com.nineyi.module.coupon.service;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.nineyi.data.NineYiWSConfig;
import com.nineyi.data.model.ecoupon.ECouponDetail;
import com.nineyi.data.model.ecoupon.ECouponShopECoupon;
import com.nineyi.data.model.ecoupon.ECouponShopECouponList;

import java.util.Calendar;
import java.util.HashMap;

import static com.nineyi.module.coupon.service.CouponManager.isDrawOut;


public class PromotionSharePreferenceHelper {

    SharedPreferences prefs;

    static final String BROADCAST_FLAG = "com.ecouponshare.broadcast";
    static final String PROMOTION = "com.ecouponshare.promotion";
    static final String LAST_TRIGGER = "com.ecouponshare.last.trigger";
    static final String NEVER_MENTION = "com.ecouponshare.never.mention";

    // ECoupon
    static final String ECOUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_PICKED = "com.ecoupon.is.firstdownload.picked";
    static final String ECOUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_DIALOG_SHOWED = "com.ecoupon.is.firstdownload.dialog.showed";

    // Coupon
    static final String COUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_PICKED = "com.coupon.is.firstdownload.picked";
    static final String COUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_DIALOG_SHOWED = "com.coupon.is.firstdownload.dialog.showed";

    public PromotionSharePreferenceHelper(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setBroadcast(boolean flag) {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(BROADCAST_FLAG, flag);
        editor.commit();
    }

    public void setNeverMention() {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(NEVER_MENTION, true);
        editor.commit();
    }

    public boolean isNeverMention() {
        boolean flag = false;
        if (prefs.contains(NEVER_MENTION)) {
            flag = prefs.getBoolean(NEVER_MENTION, false);
        }

        return flag;
    }

    public boolean isBroadcast() {
        boolean flag = false;
        if (prefs.contains(BROADCAST_FLAG)) {
            flag = prefs.getBoolean(BROADCAST_FLAG, false);
        }

        return flag;
    }

    public void resetBroadcastFlag() {
        setBroadcast(false);
    }

    /**
     * save for check has new eCoupon or not when next time open app
     */
    public void saveECouponTracing(ECouponShopECouponList memberList) {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROMOTION, NineYiWSConfig.sGson.toJson(memberList));
        editor.commit();
    }

    // get saved eCoupon json code
    public ECouponShopECouponList getECouponTracing() {
        if (prefs.contains(PROMOTION)) {
            String memberlist = prefs.getString(PROMOTION, "");
            return NineYiWSConfig.sGson.fromJson(memberlist, ECouponShopECouponList.class);
        }
        return null;
    }

    public boolean hasNewECoupon(ECouponShopECouponList eCouponData) {
        ECouponShopECouponList preMemberList = getECouponTracing();
        if (preMemberList != null) {
            HashMap<String, Integer> map = toECouponMap(preMemberList);
            for (ECouponShopECoupon info : eCouponData.ShopECouponList) {
                for (ECouponDetail detail : info.ECouponList) {
                    if (!map.containsKey(String.valueOf(detail.Id))) {
                        return true;
                    }
                }
            }

            return false;
        }

        return true;
    }

    public int getNewDrawOutECoupon(ECouponShopECouponList eCouponData) {
        ECouponShopECouponList preMemberList = getECouponTracing();
        if (preMemberList != null) {
            HashMap<String, Integer> map = toECouponMap(preMemberList);
            for (ECouponShopECoupon info : eCouponData.ShopECouponList) {
                for (int i = 0; i < info.ECouponList.size(); i++) {
                    if (!map.containsKey(String.valueOf(info.ECouponList.get(i).Id))) {
                        if (isDrawOut(info.ECouponList.get(i).TypeDef)) {
                            return i;
                        }
                    }
                }
            }
        } else {
            for (ECouponShopECoupon info : eCouponData.ShopECouponList) {
                for (int i = 0; i < info.ECouponList.size(); i++) {
                    if (isDrawOut(info.ECouponList.get(i).TypeDef)) {
                        return i;
                    }

                }
            }
        }

        return -1;
    }

    private HashMap<String, Integer> toECouponMap(ECouponShopECouponList preMemberList) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (ECouponShopECoupon info : preMemberList.ShopECouponList) {
            for (ECouponDetail detail : info.ECouponList) {
                map.put(String.valueOf(detail.Id), detail.Id);
            }
        }
        return map;
    }

    public boolean isOverADay() {
        int dayOfYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(getTriggerTime());
        int preDayOfYear = c.get(Calendar.DAY_OF_YEAR);
//TODO        Utils.logDebugMsg("isOverADay " + (dayOfYear > preDayOfYear));

        return dayOfYear > preDayOfYear;
    }

    public void setTriggerTime(long millisec) {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(LAST_TRIGGER, millisec);
        editor.commit();
    }

    public long getTriggerTime() {
        if (prefs.contains(LAST_TRIGGER)) {
            return prefs.getLong(LAST_TRIGGER, 0);
        }

        return 0;
    }

    // ECoupon
    public boolean isECouponFirstDownloadPicked() {
        return prefs.getBoolean(ECOUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_PICKED, false);
    }

    public void eCouponFirstDownloadPicked() {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ECOUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_PICKED, true);
        editor.commit();
    }

    public boolean isECouponFirstDownloadDialogShowed() {
        return prefs.getBoolean(ECOUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_DIALOG_SHOWED, false);
    }

    public void eCouponFirstDownloadDialogShowed() {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ECOUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_DIALOG_SHOWED, true);
        editor.commit();
    }

    // Coupon
    public boolean isCouponFirstDownloadPicked() {
        return prefs.getBoolean(COUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_PICKED, false);
    }

    public void couponFirstDownloadPicked() {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(COUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_PICKED, true);
        editor.commit();
    }

    public boolean isCouponFirstDownloadDialogShowed() {
        return prefs.getBoolean(COUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_DIALOG_SHOWED, false);
    }

    public void couponFirstDownloadDialogShowed() {
        android.content.SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(COUPON_PREFS_KEY_IS_FIRSTDOWNLOAD_DIALOG_SHOWED, true);
        editor.commit();
    }
}