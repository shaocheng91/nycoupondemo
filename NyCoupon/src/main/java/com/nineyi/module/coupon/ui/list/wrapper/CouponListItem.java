package com.nineyi.module.coupon.ui.list.wrapper;

import com.nineyi.module.coupon.model.Coupon;


/**
 * Created by ReeceCheng on 2017/10/3.
 */

public interface CouponListItem {
    int getViewType();
}
