package com.nineyi.module.coupon.ui.list.viewholder;

import android.view.View;

/**
 * Created by shaocheng on 2017/10/26.
 */

public class CouponListItemEmptyViewHolder extends CouponListItemViewHolder {
    public CouponListItemEmptyViewHolder(View itemView) {
        super(itemView);
    }
}
