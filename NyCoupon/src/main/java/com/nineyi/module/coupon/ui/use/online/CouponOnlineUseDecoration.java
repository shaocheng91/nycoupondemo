package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.coupon.ui.list.CouponListAdapter;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseDecoration extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int decoration = UiUtils.convertDipToPixel(10, view.getContext().getResources().getDisplayMetrics());

        int position = parent.getChildLayoutPosition(view);
        int viewType = parent.getAdapter().getItemViewType(position);
        int lastItem = parent.getAdapter().getItemCount()-1;


        outRect.left = decoration;
        outRect.right = decoration;
        outRect.top = decoration;

        if(viewType == CouponOnlineUseAdapter.VIEW_TYPE_INFORMATION) {
            outRect.top = 0;
        }

        if(viewType == CouponOnlineUseAdapter.VIEW_TYPE_USEABLE || viewType == CouponOnlineUseAdapter.VIEW_TYPE_UNUSEABLE) {
            outRect.left = decoration;
            outRect.right = UiUtils.convertDipToPixel(7, view.getContext().getResources().getDisplayMetrics());
        }

        if(position == lastItem) {
            outRect.bottom = decoration;
        }

        if(viewType == CouponOnlineUseAdapter.VIEW_TYPE_NOT_USE) {
            outRect.bottom = UiUtils.convertDipToPixel(50, view.getContext().getResources().getDisplayMetrics());
        }

    }
}
