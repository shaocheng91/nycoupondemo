package com.nineyi.module.coupon.ui.list.viewholder;

import android.view.View;

import com.nineyi.module.coupon.ui.view.CouponTicket;
import com.nineyi.module.coupon.ui.view.CouponTicketAvailable;

/**
 * Created by shaocheng on 2017/10/6.
 */

public class CouponListItemCouponAvailableViewHolder extends CouponListItemViewHolder {
    public final CouponTicketAvailable mTicketAvailable;

    public CouponListItemCouponAvailableViewHolder(View itemView) {
        super(itemView);

        mTicketAvailable = (CouponTicketAvailable) itemView;
    }
}
