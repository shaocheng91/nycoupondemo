package com.nineyi.module.coupon.service;

import android.app.Activity;

import com.nineyi.module.coupon.model.Coupon;

/**
 * Created by shaocheng on 2017/10/19.
 */

public interface CouponShareHelper {
    void shareToFb(Activity activity, int id, String imgUrl, String shopName, double discoutPrice);
    void shareToLine(Activity activity, int id, String shopName, double discountPrice);
    void shareToSMS(Activity activity, int id, String shopName, double discountPrice);
    void shareToMail(Activity activity, int id, String shopName, double discountPrice);
}
