package com.nineyi.module.coupon.service;

import com.nineyi.data.model.shoppingcart.v4.ShoppingCartData;


/**
 * Created by ReeceCheng on 2017/10/18.
 */

public interface CouponShoppingCartDataManager {
    void updateEcouponList(String couponList);
    void updateSelectedEcouponSlaveId(int couponId);
    void setShouldRefreshShoppingCart();
    ShoppingCartData getShoppingCartData();
}
