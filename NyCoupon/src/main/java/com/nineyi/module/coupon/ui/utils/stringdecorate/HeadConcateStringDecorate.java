package com.nineyi.module.coupon.ui.utils.stringdecorate;

import android.text.SpannableString;
import android.text.TextUtils;


public class HeadConcateStringDecorate extends StringDecorate {

    public HeadConcateStringDecorate(String character, EndOfStringDecorate specialRoom) {
        super(specialRoom);
        this.mCharacter = character;
    }

    @Override
    public CharSequence getCharSequence() {
        SpannableString ss = new SpannableString(mCharacter);
        return TextUtils.concat(ss, mNextStringDecorate.getCharSequence());
    }

}