package com.nineyi.module.coupon.ui.detail;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nineyi.module.base.navigator.SchemeRouter;
import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.CouponShareHelper;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.ui.view.CouponTicketAvailable;
import com.nineyi.module.coupon.ui.view.CouponTicketCollected;
import com.nineyi.module.coupon.ui.view.CouponTicketInvalid;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.nineyi.module.coupon.ui.detail.DetailContract.Action.NONE;


/**
 * Created by shaocheng on 2017/10/16.
 */

public class DetailView extends RelativeLayout implements DetailContract.View {

    private DetailContract.Presenter mPresenter;

    View mContent;
    LinearLayout mContentInner;

    View mActionContainer;
    TextView mActionButton;
    View mActionButtonLoading;

    View mEmpty;
    View mProgressBar;

    View mError;
    Button mErrorActionButton;

    private Coupon mCoupon;
    private DetailContract.Action mAction;

    private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    private CouponShareHelper mShareHelper;
    private DetailContract.OnShowMenuListener mMenuDelegate;
    private MsgManager mMsgManager;
    private NavManager mNavManager;

    private ObjectAnimator mActionLoadingAnimator;

    private Runnable mLoadDetailRunnable = new Runnable() {
        @Override
        public void run() {
            mPresenter.loadCouponDetail(false);
        }
    };

    public DetailView(@NonNull Context context) {
        super(context);
        init();
    }

    public DetailView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DetailView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DetailView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.detail, this);

        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.bg_coupon_detail));

        mContent = findViewById(R.id.content);
        mContentInner = (LinearLayout) findViewById(R.id.content_inner);

        mActionContainer = findViewById(R.id.detail_button_container);

        mActionButton = (TextView) findViewById(R.id.detail_action);
        mActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.doAction(mAction);
            }
        });
        mActionButtonLoading = findViewById(R.id.detail_action_loading);

        mEmpty = findViewById(R.id.empty_view);
        mProgressBar = findViewById(R.id.progress);

        mError = findViewById(R.id.coupon_detail_error);
        mErrorActionButton = (Button) findViewById(R.id.coupon_common_action_button);
        mErrorActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.loadCouponDetail(true);
            }
        });

        mActionLoadingAnimator = ObjectAnimator.ofFloat(mActionButtonLoading, ROTATION, 720);
        mActionLoadingAnimator.setRepeatMode(ValueAnimator.RESTART);
        mActionLoadingAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mActionLoadingAnimator.setDuration(600);
        mActionLoadingAnimator.setInterpolator(new DecelerateInterpolator(1.3f));
    }


    public void setShareHelper(CouponShareHelper shareHelper) {
        this.mShareHelper = shareHelper;
    }


    public void setMenuDelegate(DetailContract.OnShowMenuListener menuDelegate) {
        this.mMenuDelegate = menuDelegate;
    }


    public void setMsgManager(MsgManager msgManager) {
        this.mMsgManager = msgManager;
    }


    public void setNavManager(NavManager mNavManager) {
        this.mNavManager = mNavManager;
    }

    @Override
    public void setPresenter(DetailContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResume() {
        mPresenter.loadCouponDetail(true);
    }

    @Override
    public void onPause() {
        removeCallbacks(mLoadDetailRunnable);
    }

    @Override
    public void showLoading() {
        showChild(mProgressBar);
    }

    @Override
    public void showDetail(Coupon coupon) {

        mCoupon = coupon;

        clearViews();

        showCoupon();

        showTime();

        showLimitation();
        showUseCase();

        if (coupon.isCode()) {
            showCode();
        }

        showName();
        showDescription();
        showNotice();

        showChild(mContent);
    }

    private void clearViews() {
        mContentInner.removeAllViews();
    }

    @Override
    public boolean clickOnMenu(Activity activity, int menuItemId) {
        if (menuItemId == R.id.share_fb) {
            mShareHelper.shareToFb(activity, mCoupon.getId(), mCoupon.getImgUrl(), mCoupon.getShopName(), mCoupon.getDiscountPrice());
            return true;
        } else if (menuItemId == R.id.share_line) {
            mShareHelper.shareToLine(activity, mCoupon.getId(), mCoupon.getShopName(), mCoupon.getDiscountPrice());
            return true;
        } else if (menuItemId == R.id.share_sms) {
            mShareHelper.shareToSMS(activity, mCoupon.getId(), mCoupon.getShopName(), mCoupon.getDiscountPrice());
            return true;
        } else if (menuItemId == R.id.share_mail) {
            mShareHelper.shareToMail(activity, mCoupon.getId(), mCoupon.getShopName(), mCoupon.getDiscountPrice());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void showShareButton(boolean showButton) {
        mMenuDelegate.showMenu(showButton);
    }

    @Override
    public void showActionLoading() {

        mActionLoadingAnimator.start();

        mActionButton.setText("");
        mActionButton.setClickable(false);

        mActionButtonLoading.setVisibility(VISIBLE);
    }

    @Override
    public void showCollectSuccess() {
        mMsgManager.showShortToast(getContext(), getContext().getString(R.string.coupon_detail_collect_success));
    }

    @Override
    public void showCollectError(int titleStringId, String message) {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(getContext().getString(titleStringId));
        if (message != null && !message.isEmpty()) {
            b.setMessage(message);
        }
        b.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showLoading();

                mPresenter.loadCouponDetail(false);
            }
        });
        b.show();
    }

    @Override
    public void showEmpty() {
        showChild(mEmpty);
    }

    @Override
    public void showError() {
        showChild(mError);
    }

    private void showChild(View mProgressBar) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child == mProgressBar) {
                child.setVisibility(VISIBLE);
            } else {
                child.setVisibility(GONE);
            }
        }
    }

    @Override
    public void showAlert(int stringId) {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(getContext().getString(stringId));
        b.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        b.show();
    }

    @Override
    public void showDetailError() {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(getContext().getString(R.string.coupon_detail_error));
        b.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // TODO Finish activity
                ((Activity) getContext()).finish();
            }
        });
        b.show();
    }

    @Override
    public void goToLogin() {
        mNavManager.navigateLogin(getContext());
    }

    @Override
    public void goToHome() {
        SchemeRouter.navToHome(getContext());
    }

    @Override
    public void goToVerifyCoupon() {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(getContext().getString(R.string.coupon_offline_dialog_confirm_use_title));
        b.setMessage(getContext().getString(R.string.coupon_offline_dialog_confirm_use_msg));
        b.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SchemeRouter.navToCouponOfflineUse(getContext(), mCoupon.getId());
            }
        });
        b.setNegativeButton(R.string.cancel, null);
        b.show();

    }

    private void showCoupon() {

        View view = chooseCouponView();
        if (view == null) {
            // TODO Error handling
            return;
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = UiUtils.convertDipToPixel(10, getResources().getDisplayMetrics());
        layoutParams.rightMargin = UiUtils.convertDipToPixel(10, getResources().getDisplayMetrics());
        layoutParams.leftMargin = UiUtils.convertDipToPixel(10, getResources().getDisplayMetrics());

        mContentInner.addView(view, 0, layoutParams);
    }

    private View chooseCouponView() {
        switch (mCoupon.getStatus()) {
            case BEFORE_USE_TIME:
            case COLLECTED:
                CouponTicketCollected collected = new CouponTicketCollected(getContext());
                collected.setCountdownManager(new CountdownManager());
                collected.showCoupon(mCoupon);
                return collected;
            case BEFORE_COLLECT_TIME:
            case AVAILABLE:
                CouponTicketAvailable available = new CouponTicketAvailable(getContext());
                available.setCountdownManager(new CountdownManager());
                available.showCoupon(mCoupon, false, false);
                available.hideTakeButton();
                return available;
            case USED:
            case FIRST_DOWNLOAD_NOT_QUALIFIED:
            case FIRST_DOWNLOAD_COLLECTED:
            case NO_MORE_COUPONS:
            case AFTER_COLLECT_TIME:
            case AFTER_USE_TIME:
                CouponTicketInvalid notAvailable = new CouponTicketInvalid(getContext());
                notAvailable.showCoupon(mCoupon);
                notAvailable.hideStatus();
                return notAvailable;
            case UNKNOWN:
            default:
                // Do nothing
        }
        return null;
    }

    private void showTime() {
        switch (mCoupon.getStatus()) {
            case COLLECTED:
            case BEFORE_USE_TIME:
            case AFTER_USE_TIME:
            case USED:
                showUsingTime();
                return;
            case AVAILABLE:
            case FIRST_DOWNLOAD_NOT_QUALIFIED:
            case FIRST_DOWNLOAD_COLLECTED:
            case NO_MORE_COUPONS:
            case UNKNOWN:
            case BEFORE_COLLECT_TIME:
            case AFTER_COLLECT_TIME:
            default:
                showTakingTime();
        }
    }

    private void showTakingTime() {
        if (mCoupon.getStartDateTime() != null && mCoupon.getEndDateTime() != null) {

            addDetailItemView(getContext().getString(R.string.detail_item_title_take_period),
                    DATE_FORMAT.format(new Date(mCoupon.getStartDateTime().getTimeLong())) + " - " +
                            DATE_FORMAT.format(new Date(mCoupon.getEndDateTime().getTimeLong())));
        }
    }

    private void showUsingTime() {
        if (mCoupon.getUsingStartDateTime() != null && mCoupon.getUsingEndDateTime() != null) {

            addDetailItemView(getContext().getString(R.string.detail_item_title_use_period),
                    DATE_FORMAT.format(new Date(mCoupon.getUsingStartDateTime().getTimeLong())) + " - " +
                            DATE_FORMAT.format(new Date(mCoupon.getUsingEndDateTime().getTimeLong())));
        }
    }

    private void showLimitation() {
        String usingMinPrice = getContext().getString(R.string.price_format_with_dollar_sign, mCoupon.getECouponUsingMinPrice());

        addDetailItemView(getContext().getString(R.string.detail_item_title_limitation),
                getContext().getString(R.string.detail_item_content_limitation, usingMinPrice,
                        mCoupon.geteCouponMaxDiscountLimit() * 100));

    }

    private void showUseCase() {
        StringBuilder sb = new StringBuilder();

        if(mCoupon.isOnline()) {
            sb.append(getContext().getString(R.string.detail_online_use));
        }

        if(mCoupon.isOffline()) {
            if(sb.length() > 0) {
                sb.append("/");
            }
            sb.append(getContext().getString(R.string.detail_offline_use));
        }

        addDetailItemView(getContext().getString(R.string.detail_item_title_usable_place), sb.toString());
    }

    private void showCode() {
        addDetailItemView(getContext().getString(R.string.detail_item_title_code), mCoupon.getCode());
    }

    private void showName() {
        if (mCoupon.getName() != null && mCoupon.getName().trim().length() != 0) {
            addDetailItemView(getContext().getString(R.string.detail_item_title_name), mCoupon.getName());
        }
    }

    private void showDescription() {
        if (mCoupon.getDescription() != null && mCoupon.getDescription().trim().length() != 0) {
            addDetailItemView(getContext().getString(R.string.detail_item_title_description), mCoupon.getDescription());
        }
    }

    private void showNotice() {
        double maxDiscountLimitPercent = mCoupon.geteCouponMaxDiscountLimit() * 100;

        CharSequence content;
        if (mCoupon.isFirstDownload()) {
            content = Html.fromHtml(String.format(getContext().getString(R.string.detail_notice_firstdownload), maxDiscountLimitPercent));
        } else {
            content = Html.fromHtml(String.format(getContext().getString(R.string.detail_notice), maxDiscountLimitPercent));
        }

        addDetailItemView(getContext().getString(R.string.detail_item_title_notice), content);
    }

    private void addDetailItemView(String title, CharSequence content) {
        DetailItemView itemView = new DetailItemView(getContext());

        itemView.setTitleAndContent(title, content);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        if (firstDetailItem()) {
            layoutParams.topMargin = UiUtils.convertDipToPixel(10, getResources().getDisplayMetrics());
        } else {
            layoutParams.topMargin = UiUtils.convertDipToPixel(8, getResources().getDisplayMetrics());
        }

        mContentInner.addView(itemView, layoutParams);
    }

    private boolean firstDetailItem() {
        for (int i = 0; i < mContentInner.getChildCount(); i++) {
            View view = mContentInner.getChildAt(i);
            if (view instanceof DetailItemView) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void showAction(DetailContract.Action action) {
        showAction(action, -1);
    }

    @Override
    public void showAction(DetailContract.Action action, int noActionReasonStringId) {
        mAction = action;

        switch (mAction) {
            case COLLECT:
                mActionButton.setText(getContext().getString(R.string.detail_action_collect));
                break;
            case USE_OFFLINE:
                mActionButton.setText(getContext().getString(R.string.detail_action_use_at_store));
                break;
            case USE_ONLINE:
                mActionButton.setText(getContext().getString(R.string.detail_action_back_to_home));
                break;
            case NONE:
                mActionButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.btn_coupon_detail_action_none));
                mActionButton.setTextColor(ContextCompat.getColor(getContext(), R.color.btn_coupon_detail_action_text_disable));
                mActionButton.setText(noActionReasonStringId);
                break;
            default:
                // Do nothing
        }

        mActionButtonLoading.setVisibility(GONE);
        if (mActionLoadingAnimator != null) {
            mActionLoadingAnimator.cancel();
        }
        mActionButton.setVisibility(VISIBLE);

        if (mAction == NONE) {
            mActionButton.setClickable(false);
        } else {
            mActionButton.setClickable(true);
        }
    }

    @Override
    public void hideAction() {
        mActionContainer.setVisibility(GONE);
    }

    @Override
    public void scheduleToRefresh(long time) {
        postDelayed(mLoadDetailRunnable, time - System.currentTimeMillis());
    }
}
