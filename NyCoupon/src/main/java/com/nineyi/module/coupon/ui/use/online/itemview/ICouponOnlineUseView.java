package com.nineyi.module.coupon.ui.use.online.itemview;

import com.nineyi.module.coupon.model.CouponOnline;


/**
 * Created by ReeceCheng on 2017/11/15.
 */

public interface ICouponOnlineUseView {
    void showCoupon(final CouponOnline coupon,  boolean isSelected, int position);
}
