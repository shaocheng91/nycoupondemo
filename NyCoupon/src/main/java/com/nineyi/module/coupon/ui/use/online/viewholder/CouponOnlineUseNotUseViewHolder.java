package com.nineyi.module.coupon.ui.use.online.viewholder;

import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseNotUseView;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponNotUse;

import android.view.View;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseNotUseViewHolder extends CouponOnlineUseViewHolder<CouponOnlineUseItemCouponNotUse> {

    private final CouponOnlineUseNotUseView mView;

    public CouponOnlineUseNotUseViewHolder(View itemView) {
        super(itemView);

        this.mView = (CouponOnlineUseNotUseView) itemView;
    }

    @Override
    public void bindData(CouponOnlineUseItemCouponNotUse data, int position) {
        // coupon is null for Not Use
        mView.showCoupon(data.getCoupon(), data.isSelected(), position);
    }
}
