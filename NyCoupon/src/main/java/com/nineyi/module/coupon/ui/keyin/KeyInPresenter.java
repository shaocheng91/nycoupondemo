package com.nineyi.module.coupon.ui.keyin;

import android.app.Activity;

import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.LoginManager;

import de.greenrobot.event.EventBus;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableCompletableObserver;

/**
 * Created by shaocheng on 2017/9/22.
 */

public class KeyInPresenter implements KeyInContract.Presenter {
    private final CouponManager mCouponManager;
    private final LoginManager mLoginManager;
    private final KeyInContract.View mView;
    private final CompositeDisposableHelper mCompositeDisposableHelper;

    public KeyInPresenter(CouponManager mCouponManager, LoginManager mLoginManager,
                          KeyInContract.View mView, CompositeDisposableHelper compositeDisposableHelper,
                          boolean mShowWarning) {
        this.mCouponManager = mCouponManager;
        this.mLoginManager = mLoginManager;
        this.mView = mView;
        this.mCompositeDisposableHelper = compositeDisposableHelper;

        mView.showWarning(mShowWarning);
    }

    @Override
    public void takeCoupon(String code) {

        if (!mLoginManager.isLogin()) {
            // TODO
//            Bundle b = new Bundle();
//            b.putBoolean(ECouponKeyinView.BUNDLE_SHOULD_ACCESSTOKEN, true);
//            b.putString(LoginActivityNavigator.BUNDLE_EXTRA_POSTSTICKY_FROM,
//                    LoginActivityNavigator.BUNDLE_EXTRA_FROM_ECOUPON_KEYIN);
//            ActivityNavUtils.navigateLoginFragment(mActivity, null, b, true);
            return;
        }

        mCompositeDisposableHelper.add(
                mCouponManager.takeCoupon(code).subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        mView.showTakeCouponSuccess();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.showTakeCouponError(e.getMessage());

                    }
                }));

        mView.showTakingCoupon();
    }

    public void setup(Activity activity) {
        EventBus.getDefault().registerSticky(this);
    }

    public void destroy() {
        mView.hideKeyboard();
        EventBus.getDefault().unregister(this);
    }

    // TODO
//    public void onEventMainThread(LoginStickyEvent event) {
//        EventBus.getDefault().removeStickyEvent(event);
//
//        Bundle args = event.getParams();
//
//        if (args.containsKey(LoginActivityNavigator.BUNDLE_EXTRA_POSTSTICKY_FROM)) {
//            String from = args.getString(LoginActivityNavigator.BUNDLE_EXTRA_POSTSTICKY_FROM);
//
//            if (from != null && from.equals(LoginActivityNavigator.BUNDLE_EXTRA_FROM_ECOUPON_KEYIN)) {
//                sendPickECouponByCode();
//            }
//        }
//
//    }
}
