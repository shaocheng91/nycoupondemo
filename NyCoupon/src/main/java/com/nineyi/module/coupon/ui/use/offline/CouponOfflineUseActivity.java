package com.nineyi.module.coupon.ui.use.offline;

import com.nineyi.module.base.navigator.argument.provider.CouponOfflineUseArgumentProvider;
import com.nineyi.module.base.provider.tint.TintProvider;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.base.retrofit.appcompat.NyActionBarActivity;
import com.nineyi.module.base.ui.NineYiColor;
import com.nineyi.module.coupon.CouponComponent;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.CouponManager;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;


/**
 * Created by ReeceCheng on 2017/11/3.
 */

public class CouponOfflineUseActivity extends NyActionBarActivity {

    private CouponOfflineUsePresenter mPresenter;
    private CompositeDisposableHelper mCompositeDisposableHelper = new CompositeDisposableHelper();

    private Button mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CouponComponent couponComponent = CouponComponent.get();

        CouponManager couponManager = couponComponent.couponManager();

        setContentView(R.layout.coupon_offline_layout);
        initialToolbar();

        mBackButton = (Button) findViewById(R.id.coupon_offline_use_back_button);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LinearLayout container = (LinearLayout) findViewById(R.id.coupon_offline_use_layout_container);

        CouponOfflineUseView view = new CouponOfflineUseView(this);
        mPresenter = new CouponOfflineUsePresenter(this, view, couponManager, mCompositeDisposableHelper);

        view.setPresenter(mPresenter);

        container.addView(view, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    private void initialToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            setActionBarTitle(getString(R.string.coupon_offline_use));
            setHomeAsUp(toolbar);

            toolbar.setNavigationIcon(TintProvider.getInstance()
                    .getTintDrawable(getResources().getDrawable(R.drawable.btn_navi_back),
                            NineYiColor.getGlobalNaviIconColor(), NineYiColor.getGlobalNaviIconColor()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CouponOfflineUseArgumentProvider provider = new CouponOfflineUseArgumentProvider(getIntent().getExtras());
        mPresenter.useCoupon(provider.getCouponId(), true);
    }

    @Override
    public void onStop() {
        super.onStop();
        mCompositeDisposableHelper.clear();;
    }
}
