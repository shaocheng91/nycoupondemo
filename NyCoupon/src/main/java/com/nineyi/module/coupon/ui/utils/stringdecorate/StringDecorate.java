package com.nineyi.module.coupon.ui.utils.stringdecorate;

public abstract class StringDecorate implements EndOfStringDecorate {

    protected EndOfStringDecorate mNextStringDecorate;
    protected String mCharacter;

    public StringDecorate(EndOfStringDecorate specialRoom) {
        this.mNextStringDecorate = specialRoom;
    }

    @Override
    public CharSequence getCharSequence() {
        return mNextStringDecorate.getCharSequence();
    }
}