package com.nineyi.module.coupon.ui.list.wrapper;

import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_EMPTY;

/**
 * Created by shaocheng on 2017/10/26.
 */

public class CouponListItemEmpty implements CouponListItem {
    @Override
    public int getViewType() {
        return VIEW_TYPE_EMPTY;
    }
}
