package com.nineyi.module.coupon.ui.utils.stringdecorate;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;


public class BoldStringDecorate extends StringDecorate {

    public BoldStringDecorate(EndOfStringDecorate nextStringDecorate) {
        super(nextStringDecorate);
    }

    @Override
    public CharSequence getCharSequence() {
        SpannableString ss = new SpannableString(mNextStringDecorate.getCharSequence());
        ss.setSpan(new StyleSpan(Typeface.BOLD), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
