package com.nineyi.module.coupon.ui.utils.stringdecorate;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;


public class ColorStringDecorate extends StringDecorate {

    int mColor;

    public ColorStringDecorate(EndOfStringDecorate nextStringDecorate, int color) {
        super(nextStringDecorate);
        mColor = color;
    }

    @Override
    public CharSequence getCharSequence() {
        SpannableString ss = new SpannableString(mNextStringDecorate.getCharSequence());
        ss.setSpan(new ForegroundColorSpan(mColor), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

}