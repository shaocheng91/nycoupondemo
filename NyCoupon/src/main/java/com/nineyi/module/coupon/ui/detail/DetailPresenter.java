package com.nineyi.module.coupon.ui.detail;

import android.support.annotation.VisibleForTesting;

import com.nineyi.data.model.ecoupon.ECouponDetail;
import com.nineyi.data.model.ecoupon.ECouponMemberECouponStatusList;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.CollectCouponException;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.GetCouponDetailException;
import com.nineyi.module.coupon.service.LoginManager;

import java.util.Date;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import io.reactivex.observers.DisposableSingleObserver;

import static com.nineyi.module.coupon.service.GetCouponDetailException.ErrorCode.EMPTY;
import static com.nineyi.module.coupon.ui.detail.DetailContract.ARG_FROM_SHOPPING_CART;

/**
 * Created by shaocheng on 2017/10/16.
 */

public class DetailPresenter implements DetailContract.Presenter {

    private final DetailContract.View mView;
    private final CompositeDisposableHelper mCompositeDisposableHelper;
    private final CouponManager mCouponManager;
    private final int mCouponId;
    private final String mFrom;
    private final LoginManager mLoginManager;

    private Cache mCache;

    private boolean mHasPendingCouponToCollect;

    public DetailPresenter(DetailContract.View view,
                           CompositeDisposableHelper mCompositeDisposableHelper,
                           CouponManager couponManager, int couponId, String mFrom, LoginManager loginManager) {
        this.mView = view;
        this.mCompositeDisposableHelper = mCompositeDisposableHelper;
        this.mCouponManager = couponManager;
        this.mCouponId = couponId;
        this.mFrom = mFrom;
        this.mLoginManager = loginManager;
    }

    @Override
    public void loadCouponDetail(boolean mustCallGetStatusApi) {

        if (mHasPendingCouponToCollect && mLoginManager.isLogin()) {
            mCompositeDisposableHelper.add(callGetStatusApiOnly()
                    .subscribeWith(new DefaultCacheObserver() {
                        @Override
                        public void onSuccess(Cache cache) {

                            super.onSuccess(cache);

                            Coupon coupon = mCouponManager.buildCouponByDetailAndStatus(mCache.eCouponDetail,
                                    mCache.eCouponMemberECouponStatusList, new Date());
                            switch (coupon.getStatus()) {
                                case AVAILABLE:
                                case FIRST_DOWNLOAD_NOT_QUALIFIED:
                                case FIRST_DOWNLOAD_COLLECTED:
                                    collectCoupon();
                                    break;
                                case BEFORE_USE_TIME:
                                case AFTER_USE_TIME:
                                case COLLECTED:
                                    mView.showAlert(R.string.coupon_detail_has_collected);
                                    break;
                                case USED:
                                    mView.showAlert(R.string.coupon_detail_has_collected_and_used);
                                    break;
                                case BEFORE_COLLECT_TIME:
                                case AFTER_COLLECT_TIME:
                                case NO_MORE_COUPONS:
                                case UNKNOWN:
                                default:
                                    // Do nothing
                            }
                        }
                    }));
            return;
        }

        if (mCache != null) {
            if (mustCallGetStatusApi) {
                mCompositeDisposableHelper.add(callGetStatusApiOnly().subscribeWith(new DefaultCacheObserver()));
            } else {
                showCoupon();
            }
        } else {
            mView.showLoading();

            mCompositeDisposableHelper.add(callBothGetDetailAndGetStatusApi().subscribeWith(new DefaultCacheObserver()));
        }
    }

    private Single<Cache> callGetStatusApiOnly() {
        return zipDetailAndStatus(Single.just(mCache.eCouponDetail), mCouponManager.getCouponStatus(mCouponId));
    }

    private Single<Cache> callBothGetDetailAndGetStatusApi() {
        return zipDetailAndStatus(mCouponManager.getCouponDetail(mCouponId), mCouponManager.getCouponStatus(mCouponId));
    }

    private Single<Cache> zipDetailAndStatus(Single<ECouponDetail> detailSingle, Single<ECouponMemberECouponStatusList> statusSingle) {
        return Single.zip(detailSingle, statusSingle,
                new BiFunction<ECouponDetail, ECouponMemberECouponStatusList, Cache>() {
                    @Override
                    public Cache apply(@NonNull ECouponDetail eCouponDetail, @NonNull ECouponMemberECouponStatusList eCouponMemberECouponStatusList) throws Exception {
                        return new Cache(eCouponDetail, eCouponMemberECouponStatusList);
                    }
                });
    }

    @VisibleForTesting
    boolean shouldShowShareButton(Coupon coupon) {
        if (coupon.isShowPresent() || coupon.isCode() || coupon.isUnknown()) {
            return false;
        }

        if (ARG_FROM_SHOPPING_CART.equals(mFrom)) {
            return false;
        }

        return true;
    }

    private void showCoupon() {
        Coupon coupon = mCouponManager.buildCouponByDetailAndStatus(mCache.eCouponDetail,
                mCache.eCouponMemberECouponStatusList, new Date());
        Coupon.Status status = coupon.getStatus() == null? Coupon.Status.UNKNOWN: coupon.getStatus();

        mView.showDetail(coupon);

        if (shouldShowAction()) {
            switch (status) {
                case USED:
                    mView.showAction(DetailContract.Action.NONE, R.string.detail_action_used);
                    break;
                case COLLECTED:
                    if(coupon.isOffline()) {
                        mView.showAction(DetailContract.Action.USE_OFFLINE);
                    } else if(coupon.isOnline()) {
                        mView.showAction(DetailContract.Action.USE_ONLINE);
                    }
                    break;
                case AVAILABLE:
                    mView.showAction(DetailContract.Action.COLLECT);
                    break;
                case FIRST_DOWNLOAD_NOT_QUALIFIED:
                case FIRST_DOWNLOAD_COLLECTED:
                    mView.showAction(DetailContract.Action.NONE, R.string.coupon_list_item_status_invalidate);
                    break;
                case NO_MORE_COUPONS:
                    mView.showAction(DetailContract.Action.NONE, R.string.coupon_list_item_status_out_of_stock);
                    break;
                case BEFORE_COLLECT_TIME:
                case BEFORE_USE_TIME:
                    mView.showAction(DetailContract.Action.NONE,
                            R.string.coupon_detail_no_action_reason_before_time);
                    break;
                case AFTER_COLLECT_TIME:
                case AFTER_USE_TIME:
                    mView.showAction(DetailContract.Action.NONE,
                            R.string.coupon_detail_no_action_reason_after_time);
                    break;
                case UNKNOWN:
                default:
                    mView.showDetailError();
            }
        } else {
            mView.hideAction();
        }

        boolean showButton = shouldShowShareButton(coupon);
        mView.showShareButton(showButton);
    }

    private boolean shouldShowAction() {
        if (ARG_FROM_SHOPPING_CART.equals(mFrom)) {
            return false;
        }

        return true;
    }

    @Override
    public void doAction(DetailContract.Action action) {
        switch (action) {
            case COLLECT:
                collectCoupon();
                return;
            case USE_ONLINE:
                mView.goToHome();
                return;
            case USE_OFFLINE:
                mView.goToVerifyCoupon();
                break;
            default:
                // Do nothing
                return;
        }
    }

    private void collectCoupon() {
        if (mCache == null) {
            return;
        }
        Coupon coupon = mCouponManager.buildCouponByDetailAndStatus(mCache.eCouponDetail,
                mCache.eCouponMemberECouponStatusList, new Date());

        if (mLoginManager.isLogin()) {
            mView.showActionLoading();

            mCompositeDisposableHelper.add(
                    mCouponManager.takeCoupon(coupon.getId(), coupon.isFirstDownload())
                    .andThen(callGetStatusApiOnly())
                    .subscribeWith(new DefaultCacheObserver() {
                        @Override
                        public void onSuccess(Cache cache) {
                            mHasPendingCouponToCollect = false;

                            mView.showCollectSuccess();

                            super.onSuccess(cache);
                        }

                        @Override
                        public void onError(Throwable exception) {
                            mHasPendingCouponToCollect = false;

                            if (exception instanceof CollectCouponException) {
                                CollectCouponException collectCouponException = (CollectCouponException) exception;
                                switch (collectCouponException.errorType) {
                                    case INVALID:
                                    case ALREADY_COLLECTED:
                                        mView.showCollectError(R.string.coupon_collect_error_first_download_invalid, "");
                                        break;
                                    case FAIL:
                                    case UNKNOWN:
                                    default:
                                        mView.showCollectError(R.string.ecoupon_get_fail_title, collectCouponException.serverMessage);
                                }
                                return;
                            }

                            super.onError(exception);
                        }
                    }));
        } else {
            mHasPendingCouponToCollect = true;

            mView.goToLogin();
        }

    }

    private class DefaultCacheObserver extends DisposableSingleObserver<Cache> {

        @Override
        public void onSuccess(Cache cache) {
            mCache = cache;

            mView.scheduleToRefresh(cache.eCouponDetail.UsingEndDateTime.getTimeLong());
            mView.scheduleToRefresh(cache.eCouponDetail.EndDateTime.getTimeLong());

            showCoupon();
        }

        @Override
        public void onError(Throwable exception) {

            mView.showShareButton(false);

            if (exception instanceof GetCouponDetailException) {
                GetCouponDetailException e = (GetCouponDetailException) exception;
                if (e.errorCode == EMPTY) {
                    mView.showEmpty();
                    return;
                }
            }

            mView.showError();
        }
    }

    class Cache {
        ECouponDetail eCouponDetail;
        ECouponMemberECouponStatusList eCouponMemberECouponStatusList;

        public Cache(ECouponDetail eCouponDetail, ECouponMemberECouponStatusList eCouponMemberECouponStatusList) {
            this.eCouponDetail = eCouponDetail;
            this.eCouponMemberECouponStatusList = eCouponMemberECouponStatusList;
        }
    }
}
