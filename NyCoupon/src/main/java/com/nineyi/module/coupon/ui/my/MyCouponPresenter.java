package com.nineyi.module.coupon.ui.my;

import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemCoupon;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;

import static com.nineyi.module.coupon.ui.my.MyCouponAdapter.VIEW_TYPE_COLLECTED;


/**
 * Created by ReeceCheng on 2017/10/31.
 */

public class MyCouponPresenter implements MyCouponContract.Presenter {

    private final CouponManager mCouponManager;
    private final MyCouponContract.View mView;
    private final CompositeDisposableHelper mCompositeDisposableHelper;

    private List<CouponListItem> mList;

    public MyCouponPresenter(CouponManager couponManager, MyCouponContract.View view,
            CompositeDisposableHelper mCompositeDisposableHelper) {

        this.mCouponManager = couponManager;
        this.mView = view;
        this.mCompositeDisposableHelper = mCompositeDisposableHelper;

        mView.setPresenter(this);
    }


    @Override
    public void getCouponList(boolean useCache) {
        if(useCache) {
            if(mList == null) {
                getCouponListInternal();
            } else {
                if(mList.isEmpty()) {
                    mView.showEmpty();
                } else {
                    mView.showCouponList(mList);
                }
            }
        } else {
            getCouponListInternal();
        }

    }

    private void getCouponListInternal() {
        mCompositeDisposableHelper.add(
                mCouponManager.getMemberCouponList().subscribeWith(new DisposableSingleObserver<List<Coupon>>() {
                    @Override
                    public void onSuccess(List<Coupon> coupons) {
                        mList = new ArrayList<>();

                        if (coupons.isEmpty()) {
                            mView.showEmpty();
                            return;
                        }

                        for (Coupon coupon : coupons) {
                            mList.add(new CouponListItemCoupon(VIEW_TYPE_COLLECTED, coupon));
                        }

                        mView.showCouponList(mList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mList = null;
                        mView.showError();
                    }
                }));
    }

}
