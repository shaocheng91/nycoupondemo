package com.nineyi.module.coupon.ui.my;

import com.nineyi.module.base.navigator.SchemeRouter;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.ui.detail.DetailContract;
import com.nineyi.module.coupon.ui.list.viewholder.CouponListItemCouponViewHolder;
import com.nineyi.module.coupon.ui.list.viewholder.CouponListItemViewHolder;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemCoupon;
import com.nineyi.module.coupon.ui.view.CouponTicketCollected;
import com.nineyi.module.coupon.ui.view.OnClickCouponListener;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ReeceCheng on 2017/10/31.
 */

public class MyCouponAdapter extends RecyclerView.Adapter<CouponListItemViewHolder>  {

    public static final int VIEW_TYPE_SEPARATOR = 0;
    public static final int VIEW_TYPE_AVAILABLE = 1;
    public static final int VIEW_TYPE_COLLECTED = 2;
    public static final int VIEW_TYPE_INVALID = 3;
    public static final int VIEW_TYPE_EMPTY = 4;

    private final Context context;
    private final CountdownManager mCountdownManager;
    private List<CouponListItem> mData = new ArrayList<>();

    public MyCouponAdapter(Context context, CountdownManager countdownManager) {
        this.context = context;
        this.mCountdownManager = countdownManager;
    }

    public void setData(List<CouponListItem> data) {
        this.mData = data;
    }

    @Override
    public CouponListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CouponTicketCollected couponTicketCollected = new CouponTicketCollected(context);
        couponTicketCollected.setCountdownManager(mCountdownManager);
        couponTicketCollected.setOnClickCouponListener(new OnClickCouponListener() {
            @Override
            public void onClickCoupon(Coupon coupon) {
                SchemeRouter.navToCouponDetail(context, coupon.getId(), DetailContract.ARG_FROM_OTHER);
            }
        });

        return new CouponListItemCouponViewHolder(couponTicketCollected);
    }

    @Override
    public void onBindViewHolder(CouponListItemViewHolder holder, int position) {
        CouponListItemCoupon item = ((CouponListItemCoupon) mData.get(position));
        ((CouponListItemCouponViewHolder) holder).mTicket.showCoupon(item.getCoupon());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
