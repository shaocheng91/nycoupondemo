package com.nineyi.module.coupon.ui.use.online.itemview;

import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.ui.use.online.OnOnlineCouponItemClickListener;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseNotUseView extends LinearLayout implements ICouponOnlineUseView {

    private CouponOnline mCoupon;
    private int mPosition;

    OnOnlineCouponItemClickListener mClickListener;

    RadioButton mCheckBtn;

    public CouponOnlineUseNotUseView(Context context) {
        super(context);
        init(context);
    }

    public CouponOnlineUseNotUseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponOnlineUseNotUseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public CouponOnlineUseNotUseView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.coupon_online_use_item_not_use_layout, this);
        mCheckBtn = (RadioButton) findViewById(R.id.coupon_online_use_item_radio_btn);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // return null for no coupon
                mClickListener.onClick(CouponOnlineUseNotUseView.this, mCoupon, mPosition);
            }
        });
    }

    public void setOnOnlineCouponItemClickListener(OnOnlineCouponItemClickListener listener) {
        mClickListener = listener;
    }

    @Override
    public void showCoupon(final CouponOnline coupon, boolean isSelected, int position) {
        mCoupon = coupon;
        mPosition = position;
        mCheckBtn.setChecked(isSelected);

        // do nothing
    }

}
