package com.nineyi.module.coupon.ui.utils;

import android.content.Context;

import com.nineyi.module.coupon.model.Coupon;

import java.text.SimpleDateFormat;
import java.util.Date;

import con.nineyi.module.base.R;

public class ECouponUtils {

    public static String getFormatDateYearMonthDay(Context context, Date date) {
        return getFormatDate(date, context.getString(R.string.date_format_yyyy_mm_dd));
    }

    public static String getFormatDate(Date date, String format) {
        SimpleDateFormat sdFormat = new SimpleDateFormat(format);
        return sdFormat.format(date);
    }

    public static String getFormatDateMonthDay(Context context, Date date) {
        return getFormatDate(date, context.getString(R.string.date_format_mm_dd));
    }

    public static boolean isInECouponDate(long appRegisterDate, Coupon detail) {
        long startDateTime = detail.getStartDateTime().getTimeLong();
        long endDateTime = detail.getEndDateTime().getTimeLong();

        return appRegisterDate >= startDateTime && appRegisterDate <= endDateTime;
    }
}