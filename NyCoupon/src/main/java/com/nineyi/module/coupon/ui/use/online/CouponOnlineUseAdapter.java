package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.service.CouponTint;
import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseInfoView;
import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseNotUseView;
import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseUnusableView;
import com.nineyi.module.coupon.ui.use.online.itemview.CouponOnlineUseUsableView;
import com.nineyi.module.coupon.ui.use.online.viewholder.CouponOnlineUseEmptyViewHolder;
import com.nineyi.module.coupon.ui.use.online.viewholder.CouponOnlineUseInfoViewHolder;
import com.nineyi.module.coupon.ui.use.online.viewholder.CouponOnlineUseNotUseViewHolder;
import com.nineyi.module.coupon.ui.use.online.viewholder.CouponOnlineUseSeparatorViewHolder;
import com.nineyi.module.coupon.ui.use.online.viewholder.CouponOnlineUseUnusableViewHolder;
import com.nineyi.module.coupon.ui.use.online.viewholder.CouponOnlineUseUsableViewHolder;
import com.nineyi.module.coupon.ui.use.online.viewholder.CouponOnlineUseViewHolder;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItem;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCoupon;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponInformation;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUnuseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponNotUse;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemEmpty;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseAdapter extends RecyclerView.Adapter<CouponOnlineUseViewHolder> {

    class LastSelected {

        static final int NO_POSITION = -1;

        private int position;
        private int infoViewPosition;
        private boolean hasInfoView;
        private CouponOnlineUseItem item;

        LastSelected() {
            position = NO_POSITION;
            infoViewPosition = NO_POSITION;
            hasInfoView = false;
            item = null;
        }

        public boolean hasItem() {
            return item != null;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int getInfoViewPosition() {
            return infoViewPosition;
        }

        public void setInfoViewPosition(int infoViewPosition) {
            this.infoViewPosition = infoViewPosition;
        }

        public boolean hasInfoView() {
            return hasInfoView;
        }

        public void setHasInfoView(boolean hasInfoView) {
            this.hasInfoView = hasInfoView;
        }

        public CouponOnlineUseItem getItem() {
            return item;
        }

        public void setItem(CouponOnlineUseItem item) {
            this.item = item;
        }
    }

    public static final int VIEW_TYPE_SEPARATOR = 0;
    public static final int VIEW_TYPE_USEABLE = 1;
    public static final int VIEW_TYPE_UNUSEABLE = 2;
    public static final int VIEW_TYPE_INFORMATION = 3;
    public static final int VIEW_TYPE_NOT_USE = 4;
    public static final int VIEW_TYPE_EMPTY = 5;

    private final Context mContext;
    private final CouponOnlineUseContract.Presenter mPresenter;
    private CouponTint mCouponTint;
    private final LayoutInflater mLayoutInflater;

    private List<CouponOnlineUseItem> mData = new ArrayList<>();
    private LastSelected mLastSelected;

    private CountdownManager mCountdownManager;

    public CouponOnlineUseAdapter(Context context, CouponOnlineUseContract.Presenter presenter, CouponTint couponTint, CountdownManager countdownManager) {
        this.mContext = context;
        this.mPresenter = presenter;
        this.mCouponTint = couponTint;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mCountdownManager = countdownManager;

        this.mLastSelected = new LastSelected();
    }

    @Override
    public CouponOnlineUseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_SEPARATOR:
                View separator = mLayoutInflater.inflate(R.layout.coupon_online_use_item_separator, parent, false);
                return new CouponOnlineUseSeparatorViewHolder(separator);

            case VIEW_TYPE_USEABLE:
                CouponOnlineUseUsableView usableView = new CouponOnlineUseUsableView(mContext);
                usableView.setOnOnlineCouponItemClickListener(mClickListener);

                return new CouponOnlineUseUsableViewHolder(usableView);

            case VIEW_TYPE_UNUSEABLE:
                CouponOnlineUseUnusableView unusableView = new CouponOnlineUseUnusableView(mContext);
                unusableView.setCouponTint(mCouponTint);
                unusableView.setOnOnlineCouponItemClickListener(mClickListener);

                return new CouponOnlineUseUnusableViewHolder(unusableView);
            case VIEW_TYPE_INFORMATION:
                CouponOnlineUseInfoView infoView = new CouponOnlineUseInfoView(mContext);
                infoView.setCountdownManager(mCountdownManager);
                return new CouponOnlineUseInfoViewHolder(infoView);

            case VIEW_TYPE_NOT_USE:
                CouponOnlineUseNotUseView notUseView = new CouponOnlineUseNotUseView(mContext);
                notUseView.setOnOnlineCouponItemClickListener(mClickListener);

                return new CouponOnlineUseNotUseViewHolder(notUseView);

            case VIEW_TYPE_EMPTY:

                View empty = mLayoutInflater.inflate(R.layout.coupon_online_use_item_empty, parent, false);
                return new CouponOnlineUseEmptyViewHolder(empty);
            default:
                return null;
        }
    }

    private OnOnlineCouponItemClickListener mClickListener = new OnOnlineCouponItemClickListener() {
        @Override
        public void onClick(View view, CouponOnline coupon, int position) {
            mPresenter.selectedCoupon(coupon);
            toggleInfoView(coupon, position);
        }

        @Override
        public void onClickToDetail(CouponOnline coupon) {
            mPresenter.goToCouponDetail(coupon);
        }
    };

    @VisibleForTesting
    void toggleInfoView(CouponOnline coupon, int position) {
        CouponOnlineUseAdapterController controller = new CouponOnlineUseAdapterController();
        controller.toggleInfoView(this, mLastSelected, coupon, mData, position);
    }


    @Override
    public void onBindViewHolder(CouponOnlineUseViewHolder holder, int position) {
        holder.bindData(mData.get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        CouponOnlineUseItem item = mData.get(position);
        if (item instanceof CouponOnlineUseItemCouponUseable) {
            return VIEW_TYPE_USEABLE;
        } else if (item instanceof CouponOnlineUseItemCouponUnuseable) {
            return VIEW_TYPE_UNUSEABLE;
        } else if (item instanceof CouponOnlineUseItemCouponNotUse) {
            return VIEW_TYPE_NOT_USE;
        } else if (item instanceof CouponOnlineUseItemCouponInformation) {
            return VIEW_TYPE_INFORMATION;
        } else if (item instanceof CouponOnlineUseItemEmpty) {
            return VIEW_TYPE_EMPTY;
        } else {
            return VIEW_TYPE_SEPARATOR;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<CouponOnlineUseItem> data) {
        this.mData = data;
        new CouponOnlineUseAdapterController().findDefaultSelected(this, mPresenter, data);
    }

    @VisibleForTesting
    void selectDefault(CouponOnlineUseItemCoupon item, int position) {
        mPresenter.selectedCoupon(item.getCoupon());
        toggleInfoView(item.getCoupon(), position);
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }
}
