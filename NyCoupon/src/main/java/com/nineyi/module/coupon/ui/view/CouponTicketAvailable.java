package com.nineyi.module.coupon.ui.view;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.base.ui.countdown.CountdownObserver;
import com.nineyi.module.base.utils.TimeUtils;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.ui.list.CouponListContract;
import com.nineyi.module.coupon.ui.list.CouponListPresenter;
import com.nineyi.module.coupon.ui.utils.CouponCountdownCalculator;
import com.nineyi.module.coupon.ui.utils.ECouponUtils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Date;


/**
 * Created by ReeceCheng on 2017/9/29.
 */

public class CouponTicketAvailable extends CardView implements CouponTicket, CountdownObserver {

    private static final boolean SHOW_LOGIN_DIALOG = true;

    private CountdownManager mCountdownManager;
    private WeakReference<CountdownObserver> mWeakObserver;

    private Coupon mCoupon;
    long mEndTimeLong;

    CouponListContract.Presenter mPresenter;

    TextView mCouponTitle;
    TextView mUseTagOffline;
    TextView mUseTagOnline;
    TextView mPrice;
    TextView mRule;
    TextView mEndTime;
    TextView mCountDownTitle;
    TextView mCountDown;
    TextView mTaken;
    Button mTakeButton;
    ImageView mLoading;

    CouponTicketTakenAnimationPlayer mPlayer;

    public CouponTicketAvailable(Context context) {
        super(context);
        init(context);
    }

    public CouponTicketAvailable(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponTicketAvailable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UiUtils.convertDipToPixel(137, getResources().getDisplayMetrics()));
        setLayoutParams(params);

        setRadius(UiUtils.convertDipToPixel(5, getResources().getDisplayMetrics()));

        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        inflate(context, R.layout.coupon_ticket_available, this);

        mCouponTitle = (TextView) findViewById(R.id.coupon_list_item_title);
        mUseTagOffline = (TextView) findViewById(R.id.coupon_list_item_use_tag_offline);
        mUseTagOnline = (TextView) findViewById(R.id.coupon_list_item_use_tag_online);
        mPrice = (TextView) findViewById(R.id.coupon_list_item_price);
        mRule = (TextView) findViewById(R.id.coupon_list_item_rule);
        mEndTime = (TextView) findViewById(R.id.coupon_list_item_end_time);
        mCountDownTitle = (TextView) findViewById(R.id.coupon_list_item_countdown_title);
        mCountDown = (TextView) findViewById(R.id.coupon_list_item_countdown);
        mTakeButton = (Button) findViewById(R.id.coupon_list_item_take_button);
        mTaken = (TextView) findViewById(R.id.coupon_list_item_countdown_taken);
        mLoading = (ImageView) findViewById(R.id.coupon_list_item_loading);

        mPlayer = new CouponTicketTakenAnimationPlayer();
        mWeakObserver = new WeakReference<CountdownObserver>(this);
    }

    public void setPresenter(CouponListContract.Presenter presenter) {
        mPresenter = presenter;
    }

    public void showCoupon(final Coupon coupon, boolean showLoading, boolean showCollected) {
        showCoupon(coupon);

        if (showLoading) {
            mPlayer.loading(mTakeButton, mLoading);
        } else {
            if (showCollected) {
                showUseEndTime(shouldShowUseCountDown(mCoupon));
            } else {
                showTakeEndTime(mCoupon);
            }
        }
    }

    @Override
    public void showCoupon(final Coupon coupon) {

        mCoupon = coupon;

        if (mCoupon.isFirstDownload()) {
            mCouponTitle.setText(R.string.coupon_list_item_title_first_download);
        } else {
            mCouponTitle.setText(R.string.coupon_list_item_title);
        }

        if (mCoupon.isOnline()) {
            mUseTagOnline.setVisibility(VISIBLE);
        } else {
            mUseTagOnline.setVisibility(GONE);
        }

        if (mCoupon.isOffline()) {
            mUseTagOffline.setVisibility(VISIBLE);
        } else {
            mUseTagOffline.setVisibility(GONE);
        }

        // TODO Will it be too long?
        mPrice.setText(getContext().getString(R.string.taiwan_price_format, mCoupon.getDiscountPrice()));

        mRule.setText(getResources().getString(R.string.coupon_list_item_rule, coupon.getECouponUsingMinPrice()));

        mLoading.setVisibility(GONE);

        mTakeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mPresenter.isLoggedIn() && SHOW_LOGIN_DIALOG) {
                    showLoginDialog();
                } else {
                    mPresenter.collectCoupon(mCoupon, callback);
                }
            }
        });
    }

    final CouponListPresenter.OnTakeCouponSuccessCallback callback = new CouponListPresenter.OnTakeCouponSuccessCallback() {
        @Override
        public void onTakeCouponSuccess() {
            showAnimationForTakeCouponSuccess();
        }
    };

    private void showLoginDialog() {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(R.string.coupon_login_dialog_title);

        b.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPlayer.stopLoading();

                dialog.dismiss();
            }
        });

        b.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.collectCoupon(mCoupon, callback);
            }
        });

        b.show();
    }

    private void showAnimationForTakeCouponSuccess() {
        mPlayer.stopLoading();
        mPlayer.action1(getContext(), mCountDown, mCountDownTitle, mEndTime, mTakeButton, mLoading, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                boolean shouldShowCountDown = shouldShowUseCountDown(mCoupon);

                showUseEndTime(shouldShowCountDown);

                mPlayer.action2(mCountDown, mCountDownTitle, mEndTime, mTaken, shouldShowCountDown);
            }
        });
    }

    private void showUseEndTime(boolean shouldShowCountDown) {
        mTakeButton.setVisibility(GONE);
        mTaken.setVisibility(VISIBLE);

        if(shouldShowCountDown) {
            mCountDownTitle.setTranslationX(0);
            mCountDown.setTranslationX(0);

            mCountDownTitle.setAlpha(1);
            mCountDown.setAlpha(1);

            mEndTimeLong = mCoupon.getUsingEndDateTime().getTimeLong();

            mCountdownManager.unregister(mWeakObserver);
            mCountdownManager.register(mWeakObserver);

            String leftTime = new CouponCountdownCalculator().calculateLeftTime(mEndTimeLong, System.currentTimeMillis());
            mCountDown.setText(leftTime);
            mCountDownTitle.setText(R.string.coupon_list_item_use_countdown);

            mCountDownTitle.setVisibility(VISIBLE);
            mCountDown.setVisibility(VISIBLE);
            mEndTime.setVisibility(GONE);
        } else {
            mEndTime.setTranslationX(0);
            mEndTime.setAlpha(1);

            String takeEnd = ECouponUtils
                    .getFormatDateYearMonthDay(getContext(), new Date(mCoupon.getUsingEndDateTime().getTimeLong()));
            String takeEndString = getContext().getString(R.string.coupon_list_item_use_end_time, takeEnd);
            mEndTime.setText(takeEndString);

            mCountDownTitle.setVisibility(GONE);
            mCountDown.setVisibility(GONE);
            mEndTime.setVisibility(VISIBLE);
        }
    }

    private void showTakeEndTime(Coupon coupon) {
        mTakeButton.setTranslationX(0);
        mTaken.setTranslationX(0);
        mCountDownTitle.setTranslationX(0);
        mCountDown.setTranslationX(0);
        mEndTime.setTranslationX(0);

        mTakeButton.setAlpha(1);
        mTaken.setAlpha(1);
        mCountDownTitle.setAlpha(1);
        mCountDown.setAlpha(1);
        mEndTime.setAlpha(1);

        mTakeButton.setVisibility(VISIBLE);
        mTaken.setVisibility(GONE);
        mLoading.setVisibility(GONE);

        mTakeButton.setText(R.string.coupon_list_item_take);

        if (shouldShowCountDown(coupon)) {
            mEndTimeLong = coupon.getEndDateTime().getTimeLong();

            mCountdownManager.register(mWeakObserver);

            String leftTime = new CouponCountdownCalculator().calculateLeftTime(mEndTimeLong, System.currentTimeMillis());
            mCountDown.setText(leftTime);
            mCountDownTitle.setText(R.string.coupon_list_item_take_countdown);

            mCountDownTitle.setVisibility(VISIBLE);
            mCountDown.setVisibility(VISIBLE);
            mEndTime.setVisibility(GONE);
        } else {
            String takeEnd = ECouponUtils.getFormatDateYearMonthDay(getContext(), new Date(coupon.getEndDateTime().getTimeLong()));
            String takeEndString = getContext().getString(R.string.coupon_list_item_take_end_time, takeEnd);

            mEndTime.setText(takeEndString);

            mCountDownTitle.setVisibility(GONE);
            mCountDown.setVisibility(GONE);
            mEndTime.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setOnClickCouponListener(final OnClickCouponListener listener) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickCoupon(mCoupon);
            }
        });
    }

    public void setCountdownManager(CountdownManager countdownManager) {
        mCountdownManager = countdownManager;
    }

    private boolean shouldShowUseCountDown(Coupon coupon) {
        long endTime = coupon.getUsingEndDateTime().getTimeLong();
        return TimeUtils.isWithinDays(endTime, 1);
    }

    private boolean shouldShowCountDown(Coupon coupon) {
        long endTime = coupon.getEndDateTime().getTimeLong();
        return TimeUtils.isWithinDays(endTime, 1);
    }

    @Override
    public void countdown(long systemTime) {
        String leftTime = new CouponCountdownCalculator().calculateLeftTime(mEndTimeLong, systemTime);
        mCountDown.setText(leftTime);

        if(mEndTimeLong < systemTime) {
            mCountdownManager.unregister(mWeakObserver);
        }
    }

    public void hideTakeButton() {
        mTakeButton.setVisibility(View.GONE);
    }
}
