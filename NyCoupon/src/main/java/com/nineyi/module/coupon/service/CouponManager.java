package com.nineyi.module.coupon.service;

import com.nineyi.data.ReturnCodeType;
import com.nineyi.data.model.apiresponse.ReturnCode;
import com.nineyi.data.model.ecoupon.ECouponVerify;
import com.nineyi.data.model.ecoupon.ECouponCouponDetail;
import com.nineyi.data.model.ecoupon.ECouponDetail;
import com.nineyi.data.model.ecoupon.ECouponIncludeDetail;
import com.nineyi.data.model.ecoupon.ECouponMemberECouponStatusList;
import com.nineyi.data.model.ecoupon.ECouponMemberList;
import com.nineyi.data.model.ecoupon.ECouponShopECoupon;
import com.nineyi.data.model.ecoupon.ECouponShopECouponList;
import com.nineyi.data.model.ecoupon.ECouponStatusList;
import com.nineyi.data.model.shoppingcart.v4.ShoppingCartData;
import com.nineyi.module.base.utils.NyCollectionUtils;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.model.CouponOffline;
import com.nineyi.module.coupon.model.CouponOnline;

import org.reactivestreams.Publisher;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;

import static com.nineyi.module.coupon.model.Coupon.Status.AVAILABLE;
import static com.nineyi.module.coupon.model.Coupon.Status.BEFORE_COLLECT_TIME;
import static com.nineyi.module.coupon.model.Coupon.Status.BEFORE_USE_TIME;
import static com.nineyi.module.coupon.model.Coupon.Status.COLLECTED;
import static com.nineyi.module.coupon.model.Coupon.Status.AFTER_COLLECT_TIME;
import static com.nineyi.module.coupon.model.Coupon.Status.FIRST_DOWNLOAD_COLLECTED;
import static com.nineyi.module.coupon.model.Coupon.Status.FIRST_DOWNLOAD_NOT_QUALIFIED;
import static com.nineyi.module.coupon.model.Coupon.Status.NO_MORE_COUPONS;
import static com.nineyi.module.coupon.model.Coupon.Status.UNKNOWN;
import static com.nineyi.module.coupon.model.Coupon.Status.USED;
import static com.nineyi.module.coupon.model.Coupon.Status.AFTER_USE_TIME;
import static com.nineyi.module.coupon.service.CollectCouponException.ErrorType.ALREADY_COLLECTED;
import static com.nineyi.module.coupon.service.CollectCouponException.ErrorType.FAIL;
import static com.nineyi.module.coupon.service.CollectCouponException.ErrorType.INVALID;
import static com.nineyi.module.coupon.service.CouponService.BIRTHDAY;
import static com.nineyi.module.coupon.service.CouponService.CODE;
import static com.nineyi.module.coupon.service.CouponService.DRAW_OUT;
import static com.nineyi.module.coupon.service.CouponService.FIRST_DOWNLOAD;
import static com.nineyi.module.coupon.service.CouponService.OPEN_CARD;
import static com.nineyi.module.coupon.service.GetCouponDetailException.ErrorCode.EMPTY;

/**
 * Created by shaocheng on 2017/9/22.
 */

public class CouponManager {

    private final Context mContext;
    private final int mShopId;
    private final CouponService mCouponService;
    private final CouponIdManager mIdManager;
    private final PromotionSharePreferenceHelper mPromotionSharePreferenceHelper;
    private final MigrationManager mMigrationManager;

    public CouponManager(Context context, int shopId, CouponService mCouponService,
                         CouponIdManager idManager,
                         PromotionSharePreferenceHelper promotionSharePreferenceHelper, MigrationManager migrationManager) {
        this.mContext = context;
        this.mShopId = shopId;
        this.mCouponService = mCouponService;
        this.mIdManager = idManager;
        this.mPromotionSharePreferenceHelper = promotionSharePreferenceHelper;
        this.mMigrationManager = migrationManager;
    }

    public Single<List<Coupon>> getCouponList() {
        return mCouponService.getECouponListWithoutCode(mShopId)
                .map(new Function<ECouponShopECouponList, List<ECouponShopECoupon>>() {
                    @Override
                    public List<ECouponShopECoupon> apply(@NonNull ECouponShopECouponList eCouponShopECouponList) throws Exception {
                        ECouponShopECouponList eCouponList = eCouponShopECouponList;

                        if (!ReturnCodeType.API0001.toString().equals(eCouponList.ReturnCode)) {
                            throw new GetCouponListException(GetCouponListException.ErrorCode.UNKNOWN);
                        }

                        if (eCouponList.ShopECouponList == null || eCouponList.ShopECouponList.isEmpty()) {
                            throw new GetCouponListException(GetCouponListException.ErrorCode.UNKNOWN);
                        }

                        ECouponShopECoupon eCouponShopECoupon = eCouponList.ShopECouponList.get(0);
                        if (eCouponShopECoupon == null ||
                                eCouponShopECoupon.ECouponList == null ||
                                eCouponShopECoupon.ECouponList.isEmpty()) {
                            throw new GetCouponListException(GetCouponListException.ErrorCode.EMPTY);
                        }

                        eCouponList = filterECouponShopECouponList(eCouponList);

                        if (mPromotionSharePreferenceHelper.hasNewECoupon(eCouponList)) {
                            mPromotionSharePreferenceHelper.saveECouponTracing(eCouponList);
                        }

                        return eCouponList.ShopECouponList;
                    }
                })
                .flatMap(new Function<List<ECouponShopECoupon>, Publisher<ECouponStatusList>>() {
                    @Override
                    public Publisher<ECouponStatusList> apply(List<ECouponShopECoupon> shopECouponList) throws Exception {

                        // TODO 還要傳 Facebook token 嗎？

                        return mCouponService.getMemberECouponStatusList(getStatusListString(shopECouponList), String.valueOf(0));
                    }
                }, new BiFunction<List<ECouponShopECoupon>, ECouponStatusList, List<Coupon>>() {
                    @Override
                    public List<Coupon> apply(List<ECouponShopECoupon> eCouponList, @NonNull ECouponStatusList eCouponStatusList) throws Exception {
                        HashMap<Integer, ECouponMemberECouponStatusList> statusMap = convertToMap(eCouponStatusList);

                        List<Coupon> coupons = new ArrayList<Coupon>();

                        Date now = new Date();

                        for (ECouponDetail eCouponDetail : eCouponList.get(0).ECouponList) {

                            ECouponMemberECouponStatusList status = null;
                            if (statusMap.containsKey(eCouponDetail.Id)) {
                                status = statusMap.get(eCouponDetail.Id);
                            }

                            Coupon coupon = buildCouponByDetailAndStatus(eCouponDetail, status, now);
                            coupons.add(coupon);
                        }

                        return coupons;
                    }
                })
                .single(new ArrayList<Coupon>());
    }

    public Completable takeCoupon(String code) {
        return mCouponService.setMemberECouponByCode(mShopId, code, mIdManager.getGUID())
                .flatMapCompletable(new Function<ReturnCode, CompletableSource>() {
                    @Override
                    public CompletableSource apply(@NonNull ReturnCode code) throws Exception {
                        if (!ReturnCodeType.API0001.toString().equals(code.ReturnCode)) {
                            throw new RuntimeException("");
                        }

                        return Completable.complete();
                    }
                });
    }

    public Completable takeCoupon(int couponId, boolean isFirstDownload) {
        if (isFirstDownload) {
            return mCouponService.setMemberFirstDownloadECouponByECouponId(couponId, mIdManager.getGUID())
                    .flatMapCompletable(new Function<ReturnCode, CompletableSource>() {
                        @Override
                        public CompletableSource apply(@NonNull ReturnCode code) throws Exception {
                            if (code == null) {
                                throw new CollectCouponException(CollectCouponException.ErrorType.UNKNOWN);
                            }

                            // TODO More specific error handling
                            if (!ReturnCodeType.API0001.toString().equals(code.ReturnCode)) {

                                if (ReturnCodeType.API0002.toString().equals(code.ReturnCode)) {

                                    throw new CollectCouponException(FAIL, code.Message);

                                } else if (ReturnCodeType.API0003.toString().equals(code.ReturnCode)) {

                                    mPromotionSharePreferenceHelper.eCouponFirstDownloadPicked();

                                    throw new CollectCouponException(INVALID, code.Message);


                                } else if (ReturnCodeType.API2001.toString().equals(code.ReturnCode)) {

                                    mPromotionSharePreferenceHelper.eCouponFirstDownloadPicked();

                                    throw new CollectCouponException(ALREADY_COLLECTED, code.Message);

                                } else {

                                    throw new CollectCouponException(CollectCouponException.ErrorType.UNKNOWN, code.Message);
                                }
                            }

                            mPromotionSharePreferenceHelper.eCouponFirstDownloadPicked();

                            return Completable.complete();
                        }
                    });
        } else {
            return mCouponService.setMemberECouponByECouponId(couponId, mIdManager.getGUID())
                    .flatMapCompletable(new Function<ReturnCode, CompletableSource>() {
                        @Override
                        public CompletableSource apply(@NonNull ReturnCode code) throws Exception {

                            if (code == null || !ReturnCodeType.API0001.toString().equals(code.ReturnCode)) {
                                throw new CollectCouponException(CollectCouponException.ErrorType.UNKNOWN, code.Message);
                            }

                            return Completable.complete();
                        }
                    });
        }
    }

    public List<CouponOnline> getOnlineUseCouponList(final ShoppingCartData shoppingCartData) {
        List<ECouponCouponDetail> data = shoppingCartData.getECouponCouponDetailList();
        List<CouponOnline> coupons = new ArrayList<>();

        for (ECouponCouponDetail detail : data) {
            CouponOnline.Builder builder = new CouponOnline.Builder()
                    .setCode(detail.Code)
                    .setDiscountPrice(detail.DiscountPrice)
                    .setTypeDef(detail.TypeDef)
                    .setUsingEndDateTime(detail.UsingEndDateTime)
                    .setUsingStartDateTime(detail.UsingStartDateTime)
                    .setId(detail.Id).setShopId(detail.ShopId)
                    .setDetailId(detail.ECouponId)
                    .setECouponMaxDiscountLimit(detail.ECouponMaxDiscountLimit)
                    .setECouponUsingMinPrice(detail.ECouponUsingMinPrice)
                    .setIsAchieveUsingMinPrice(detail.IsAchieveUsingMinPrice);

            coupons.add(builder.build());
        }

        return coupons;
    }

    public Single<List<Coupon>> getMemberCouponList() {
        return mCouponService.getMemberECouponList(mShopId).map(new Function<ECouponMemberList, List<Coupon>>() {
                    @Override
                    public List<Coupon> apply(ECouponMemberList eCouponMemberList) throws Exception {

                List<Coupon> coupons = new ArrayList<>();

                if(checkECouponMemberListNotNull(eCouponMemberList)) {
                    ArrayList<ECouponCouponDetail> couponDetails = eCouponMemberList.ECouponList.get(0).ECouponList;

                    for (ECouponCouponDetail detail : couponDetails) {
                        Coupon.Builder builder = new Coupon.Builder().code(detail.Code).discountPrice(detail.DiscountPrice).typeDef(detail.TypeDef)
                                .usingEndDateTime(detail.UsingEndDateTime).usingStartDateTime(detail.UsingStartDateTime)
                                .id(detail.ECouponId).shopId(detail.ShopId).eCouponId(detail.ECouponId).eCouponMaxDiscountLimit(detail.ECouponMaxDiscountLimit);

                        coupons.add(builder.build());
                    }
                }

                return coupons;
            }
        })
        .single(new ArrayList<Coupon>());
    }

    private boolean checkECouponMemberListNotNull(ECouponMemberList eCouponMemberList) {
        return eCouponMemberList != null
                && eCouponMemberList.ECouponList != null && !eCouponMemberList.ECouponList.isEmpty()
                && eCouponMemberList.ECouponList.get(0) != null
                && eCouponMemberList.ECouponList.get(0).ECouponList != null && !eCouponMemberList.ECouponList.get(0).ECouponList.isEmpty();
    }

    private HashMap<Integer, ECouponMemberECouponStatusList> convertToMap(ECouponStatusList list) {
        HashMap<Integer, ECouponMemberECouponStatusList> statusMap = new HashMap<>();

        if (list.MemberECouponStatusList == null) {
            return statusMap;
        }

        for (ECouponMemberECouponStatusList status : list.MemberECouponStatusList) {
            statusMap.put((int) status.ECouponId, status);
        }

        return statusMap;
    }

    public Single<CouponOffline> useCoupon(int couponId) {
        return mCouponService.setECouponVerify(mShopId, couponId).map(new Function<ECouponVerify, CouponOffline>() {
            @Override
            public CouponOffline apply(ECouponVerify ecouponVerify) throws Exception {

                if (!ReturnCodeType.API0001.toString().equals(ecouponVerify.ReturnCode)) {
                    // TODO Error handling
                    throw new RuntimeException("API ERROR");
                }

                return new CouponOffline.Builder()
                        .couponId(ecouponVerify.Data.ECouponId)
                        .couponSlaveId(ecouponVerify.Data.ECouponSlaveId)
                        .couponBarcode(ecouponVerify.Data.ECouponCode)
                        .vipMemberBarcode(ecouponVerify.Data.VipMemberBarCode)
                        .isUseCustomCode(ecouponVerify.Data.IsUsingCustomVerificationCode)
                        .couponCustomCode1(ecouponVerify.Data.ECouponCustomVerificationCode1)
                        .couponCustomCode2(ecouponVerify.Data.ECouponCustomVerificationCode2)
                        .couponCustomCode3(ecouponVerify.Data.ECouponCustomVerificationCode3)
                        .build();

            }
        }).single(new CouponOffline.Builder().build());
    }

    private String getStatusListString(List<ECouponShopECoupon> eCouponList) {
        ArrayList<Integer> ids = findECouponIds(eCouponList);
        return ids.toString().replace("[", "").replace("]", "").replace(", ", ",");
    }

    private ArrayList<Integer> findECouponIds(List<ECouponShopECoupon> eCouponList) {
        ArrayList<Integer> ids = new ArrayList<>();

        for (ECouponShopECoupon shopECoupon : eCouponList) {
            for (ECouponDetail detail : shopECoupon.ECouponList) {
                if (isTypeValid(detail.TypeDef)) {
                    ids.add(detail.Id);
                }
            }
        }

        return ids;
    }

    private ECouponShopECouponList filterECouponShopECouponList(ECouponShopECouponList shoplist) {
        for (ECouponShopECoupon shopECoupon : shoplist.ShopECouponList) {
            NyCollectionUtils.filter(shopECoupon.ECouponList, new NyCollectionUtils.Predicate<ECouponDetail>() {
                @Override
                public boolean evaluate(ECouponDetail o) {
                    return isTypeValid(o.TypeDef);
                }
            });
        }
        return shoplist;
    }

    public Single<ECouponDetail> getCouponDetail(int couponId) {
        return mCouponService.getECouponDetail(couponId)
                .map(new Function<ECouponIncludeDetail, ECouponDetail>() {

                    @Override
                    public ECouponDetail apply(ECouponIncludeDetail eCouponIncludeDetail) throws Exception {
                        if (!ReturnCodeType.API0001.toString().equals(eCouponIncludeDetail.ReturnCode)) {

                            if (ReturnCodeType.API0009.toString().equals(eCouponIncludeDetail.ReturnCode)) {
                                throw new GetCouponDetailException(EMPTY);
                            }

                            throw new GetCouponDetailException();
                        }

                        if (eCouponIncludeDetail.ECouponDetail == null) {
                            throw new GetCouponDetailException();
                        }

                        return eCouponIncludeDetail.ECouponDetail;
                    }
                })
                .single(new ECouponDetail());
    }

    public Single<ECouponMemberECouponStatusList> getCouponStatus(int couponId) {
        return mCouponService.getMemberECouponStatusList(String.valueOf(couponId), String.valueOf(0))
                .map(new Function<ECouponStatusList, ECouponMemberECouponStatusList>() {
                    @Override
                    public ECouponMemberECouponStatusList apply(ECouponStatusList eCouponStatusList) throws Exception {
                        if (eCouponStatusList.MemberECouponStatusList != null &&
                                eCouponStatusList.MemberECouponStatusList.size() > 0) {
                            return eCouponStatusList.MemberECouponStatusList.get(0);
                        }

                        return new ECouponMemberECouponStatusList();
                    }
                })
                .single(new ECouponMemberECouponStatusList());
    }

    public Coupon buildCouponByDetailAndStatus(ECouponDetail detail, ECouponMemberECouponStatusList status, Date now) {
        Coupon.Builder builder = new Coupon.Builder()
                .code(detail.Code)
                .imgUrl(detail.ImgUrl)
                .totalGiftLimit(detail.TotalGiftLimit)
                .qtyLimit(detail.QtyLimit)
                .discountPrice(detail.DiscountPrice)
                .typeDef(detail.TypeDef)
                .startDateTime(detail.StartDateTime)
                .endDateTime(detail.EndDateTime)
                .usingEndDateTime(detail.UsingEndDateTime)
                .usingStartDateTime(detail.UsingStartDateTime)
                .totalQty(detail.TotalQty)
                .id(detail.Id)
                .name(detail.Name)
                .description(detail.Description)
                .eCouponWording(detail.EcouponWording)
                .shopId(detail.ShopId)
                .shopName(detail.ShopName)
                .isSingleCode(detail.IsSingleCode)
                .couponTotalCount(detail.CouponTotalCount)
                .takeEndTimeWarningText(detail.TakeEndTimeWarningText)
                .eCouponMaxDiscountLimit(detail.ECouponMaxDiscountLimit)
                .eCouponUsingMinPrice(detail.ECouponUsingMinPrice)
                .isOnline(detail.IsOnline)
                .isOffline(detail.IsOffline);

        if (status != null)  {
            builder.eCouponId(status.ECouponId)
                    .hasNormalCoupon(status.HasNormalCoupon)
                    .couponCode(status.CouponCode)
                    .isUsed(status.IsUsing);
        } else {
            builder.eCouponId(detail.Id)
                    .hasNormalCoupon(false)
                    .couponCode(detail.Code);
        }

        builder.status(determineStatus(detail, status, now));

        return builder.build();
    }

    @VisibleForTesting
    Coupon.Status determineStatus(ECouponDetail detail, ECouponMemberECouponStatusList status, Date now) {
        if (isShowPresent(detail.TypeDef)) {
            Coupon.Status s = checkIsCollected(detail, status, now);
            if (s != UNKNOWN) {
                return s;
            } else {
                return UNKNOWN; // Not possible. Cannot collect show present coupon
            }
        } else if (isDrawOut(detail.TypeDef) || isCode(detail.TypeDef)) {
            Coupon.Status s = checkIsCollected(detail, status, now);
            if (s != UNKNOWN) {
                return s;
            } else {
                return checkIsAvailable(detail, now);
            }
        } else if (isFirstDownload(detail.TypeDef)) {
            Coupon.Status s = checkIsCollected(detail, status, now);
            if (s != UNKNOWN) {
                return s;
            } else {
                s = checkIsAvailable(detail, now);
                if (s != AVAILABLE) {
                    return s;
                } else {
                    if (mPromotionSharePreferenceHelper.isECouponFirstDownloadPicked()) {
                        return FIRST_DOWNLOAD_COLLECTED;
                    } else {
                        long appRegisterDate = mMigrationManager.getAppRegisterDateMilliseconds();

                        if (isInECouponDate(appRegisterDate, detail)) {
                            return AVAILABLE;

                        } else {
                            return FIRST_DOWNLOAD_NOT_QUALIFIED;
                        }
                    }
                }
            }
        } else {
            return UNKNOWN;
        }
    }

    private Coupon.Status checkIsAvailable(ECouponDetail detail, Date now) {
        if (now.getTime() < detail.StartDateTime.getTimeLong()) {
            return BEFORE_COLLECT_TIME;
        } else if (now.getTime() > detail.EndDateTime.getTimeLong()) {
            return AFTER_COLLECT_TIME;
        } else {
            if (detail.CouponTotalCount > 0) {
                return AVAILABLE;
            } else {
                return NO_MORE_COUPONS;
            }
        }
    }

    private Coupon.Status checkIsCollected(ECouponDetail detail, ECouponMemberECouponStatusList status, Date now) {
        if (status != null && status.HasNormalCoupon) {
            if (status.IsUsing) {
                return USED;
            } else {
                if (now.getTime() < detail.UsingStartDateTime.getTimeLong()) {
                    return BEFORE_USE_TIME;
                } else if (now.getTime() > detail.UsingEndDateTime.getTimeLong()) {
                    return AFTER_USE_TIME;
                } else {
                    return COLLECTED;
                }
            }
        }
        return UNKNOWN;
    }

    private boolean withinCollectingTime(ECouponDetail detail, Date now) {
        return now.getTime() >= detail.StartDateTime.getTimeLong() &&
                now.getTime() <= detail.EndDateTime.getTimeLong();
    }

    private static boolean isInECouponDate(long appRegisterDate, ECouponDetail detail) {
        long startDateTime = detail.StartDateTime.getTimeLong();
        long endDateTime = detail.EndDateTime.getTimeLong();

        return appRegisterDate >= startDateTime && appRegisterDate <= endDateTime;
    }

    public static boolean isDrawOut(String TypeDef) {
        return TypeDef.toLowerCase().equals(DRAW_OUT) || TypeDef.toLowerCase().equals(OPEN_CARD) || TypeDef.toLowerCase()
                .equals(BIRTHDAY);
    }

    public static boolean isUnKnown(String typeDef) {
        return (!isDrawOut(typeDef) && !isCode(typeDef) && !isFirstDownload(typeDef));
    }

    public static boolean isShowPresent(String TypeDef) {
        return TypeDef.toLowerCase().equals(OPEN_CARD) || TypeDef.toLowerCase().equals(BIRTHDAY);
    }

    public static boolean isCode(String TypeDef) {
        return TypeDef.toLowerCase().equals(CODE);
    }

    public static boolean isFirstDownload(String TypeDef) {
        return TypeDef.toLowerCase().equals(FIRST_DOWNLOAD);
    }

    public static boolean isTypeValid(String TypeDef) {
        return (TypeDef.toLowerCase().equals(DRAW_OUT) || TypeDef.toLowerCase().equals(OPEN_CARD) || TypeDef.toLowerCase()
                .equals(BIRTHDAY) || TypeDef.toLowerCase().equals(FIRST_DOWNLOAD) || TypeDef.toLowerCase()
                .equals(CODE));
    }
}
