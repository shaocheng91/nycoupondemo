package com.nineyi.module.coupon.service;

/**
 * Created by shaocheng on 2017/10/3.
 */

public class GetCouponListException extends Exception {

    public enum ErrorCode {
        EMPTY,
        UNKNOWN
    }

    public final ErrorCode errorCode;

    public GetCouponListException() {
        this.errorCode = ErrorCode.UNKNOWN;
    }

    public GetCouponListException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

}
