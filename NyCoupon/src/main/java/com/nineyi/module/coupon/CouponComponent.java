package com.nineyi.module.coupon;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.CouponService;
import com.nineyi.module.coupon.service.CouponIdManager;
import com.nineyi.module.coupon.service.CouponSharePreferenceHelper;
import com.nineyi.module.coupon.service.CouponShoppingCartDataManager;
import com.nineyi.module.coupon.service.CouponTint;
import com.nineyi.module.coupon.service.LoginManager;
import com.nineyi.module.coupon.service.MigrationManager;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.service.PromotionSharePreferenceHelper;
import com.nineyi.module.coupon.service.CouponShareHelper;

/**
 * Created by shaocheng on 2017/10/3.
 */

public class CouponComponent {
    private static CouponComponent INSTANCE;

    public static void init(
            Context context,
            int shopId,
            CouponService couponService,
            MigrationManager migrationManager,
            LoginManager loginManager,
            CouponIdManager idManager,
            NavManager navManager,
            CouponShoppingCartDataManager couponShoppingCartDataManager,
            CouponShareHelper shareHelper,
            CouponTint couponTint) {

        INSTANCE = new CouponComponent(context, shopId, couponService, migrationManager,
                loginManager, idManager, navManager, couponShoppingCartDataManager, shareHelper, couponTint);
    }

    @VisibleForTesting
    public static void set(CouponComponent couponComponent) {
        INSTANCE = couponComponent;
    }

    public static CouponComponent get() {
        if (INSTANCE == null) {
            throw new RuntimeException("Coupon component is not init");
        }
        return INSTANCE;
    }

    private final Context mContext;
    private final int shopId;
    private MigrationManager mMigrationManager;
    private final CouponManager mCouponManager;
    private final CouponService couponService;
    private final LoginManager loginManager;
    private final PromotionSharePreferenceHelper mPromotionSharePreferenceHelper;
    private final CouponIdManager mIdManager;
    private final MsgManager mMsgManager;
    private final NavManager navManager;
    private final CouponSharePreferenceHelper mCouponSharePreferenceHelper;
    private final CouponShoppingCartDataManager mCouponShoppingCartDataManager;
    private final CouponShareHelper mShareHelper;
    private final CouponTint mCouponTint;

    public CouponComponent(
            Context context,
            int shopId,
            CouponService couponService,
            MigrationManager migrationManager,
            LoginManager loginManager,
            CouponIdManager idManager,
            NavManager navManager,
            CouponShoppingCartDataManager couponShoppingCartDataManager,
            CouponShareHelper shareHelper,
            CouponTint couponTint) {


        this.mContext = context;
        this.shopId = shopId;
        this.couponService = couponService;
        this.mMigrationManager = migrationManager;
        this.loginManager = loginManager;
        this.mIdManager = idManager;
        this.navManager = navManager;
        this.mCouponShoppingCartDataManager = couponShoppingCartDataManager;
        this.mShareHelper = shareHelper;
        this.mCouponTint = couponTint;

        mPromotionSharePreferenceHelper = new PromotionSharePreferenceHelper(context);
        mCouponSharePreferenceHelper = new CouponSharePreferenceHelper(context);

        mCouponManager = new CouponManager(context, shopId, couponService, mIdManager, mPromotionSharePreferenceHelper, mMigrationManager);
        mMsgManager = new MsgManager();
    }

    public CouponManager couponManager() {
        return mCouponManager;
    }

    public LoginManager loginManager() {
        return loginManager;
    }

    public MigrationManager migrationmanager() {
        return mMigrationManager;
    }

    public PromotionSharePreferenceHelper promotionSharePreferenceHelper() {
        return mPromotionSharePreferenceHelper;
    }

    public CouponIdManager idManager() {
        return mIdManager;
    }

    public MsgManager msgManager() {
        return mMsgManager;
    }

    public NavManager navManager() {
        return navManager;
    }

    public CouponSharePreferenceHelper couponSharePreferenceHelper() {
        return mCouponSharePreferenceHelper;
    }

    public CouponShoppingCartDataManager getCouponShoppingCartDataManager() {
        return mCouponShoppingCartDataManager;
    }

    public CouponShareHelper shareHelper() {
        return mShareHelper;
    }

    public CouponTint getCouponTint() {
        return mCouponTint;
    }
}
