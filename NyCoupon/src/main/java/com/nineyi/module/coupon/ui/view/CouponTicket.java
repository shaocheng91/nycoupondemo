package com.nineyi.module.coupon.ui.view;

import com.nineyi.module.coupon.model.Coupon;


/**
 * Created by shaocheng on 2017/10/19.
 */

public interface CouponTicket {
    void showCoupon(Coupon coupon);
    void setOnClickCouponListener(OnClickCouponListener listener);
}
