package com.nineyi.module.coupon.ui.use.online.itemview;

import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.ui.use.online.OnOnlineCouponItemClickListener;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;


/**
 * Created by ReeceCheng on 2017/10/17.
 */

public class CouponOnlineUseUsableView extends LinearLayout implements ICouponOnlineUseView {

    private CouponOnline mCoupon;
    private int mPosition;

    OnOnlineCouponItemClickListener mClickListener;

    ConstraintLayout mRoot;
    RadioButton mCheckBtn;
    TextView mPrice;
    Button mToDetailBtn;

    public CouponOnlineUseUsableView(Context context) {
        super(context);
        init(context);
    }

    public CouponOnlineUseUsableView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CouponOnlineUseUsableView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public CouponOnlineUseUsableView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.coupon_online_use_item_useable_layout, this);

        mRoot = (ConstraintLayout) findViewById(R.id.coupon_online_use_item_useable);
        mPrice = (TextView) findViewById(R.id.coupon_online_use_item_price);
        mToDetailBtn = (Button) findViewById(R.id.coupon_online_use_item_to_detail_btn);
        mCheckBtn = (RadioButton) findViewById(R.id.coupon_online_use_item_radio_btn);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onClick(CouponOnlineUseUsableView.this, mCoupon, mPosition);
            }
        });

        mToDetailBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onClickToDetail(mCoupon);
            }
        });
    }

    public void setOnOnlineCouponItemClickListener(OnOnlineCouponItemClickListener listener) {
        mClickListener = listener;
    }

    @Override
    public void showCoupon(final CouponOnline coupon,  boolean isSelected, int position) {
        mCoupon = coupon;
        mPosition = position;
        mCheckBtn.setChecked(isSelected);

        if(isSelected) {
            mRoot.setBackground(getContext().getResources().getDrawable(R.drawable.min_couponbg_primary_selected));
        } else {
            mRoot.setBackground(getContext().getResources().getDrawable(R.drawable.min_couponbg_primary_unselected));
        }

        mPrice.setText(getContext().getString(R.string.taiwan_price_format, coupon.getDiscountPrice()));
    }

}