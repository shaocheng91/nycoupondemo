package com.nineyi.module.coupon.service;

import com.nineyi.data.model.apiresponse.ReturnCode;
import com.nineyi.data.model.ecoupon.ECouponVerify;
import com.nineyi.data.model.ecoupon.ECouponIncludeDetail;
import com.nineyi.data.model.ecoupon.ECouponMemberList;
import com.nineyi.data.model.ecoupon.ECouponShopECouponList;
import com.nineyi.data.model.ecoupon.ECouponStatusList;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by shaocheng on 2017/10/3.
 */

public interface CouponService {

    String DRAW_OUT = "drawout";
    String CODE = "code";
    String OPEN_CARD = "opencard";
    String BIRTHDAY = "birthday";
    String FIRST_DOWNLOAD = "firstdownload";

    Flowable<ECouponShopECouponList> getECouponListWithoutCode(int shopId);

    Flowable<ECouponStatusList> getMemberECouponStatusList(String eCouponIdListString, String fbId);

    Flowable<ReturnCode> setMemberECouponByCode(int shopId, String code, String guid);

    Flowable<ReturnCode> setMemberECouponByECouponId(int ecouponId, String guid);

    Flowable<ReturnCode> setMemberFirstDownloadECouponByECouponId(int ecouponId, String guid);

    Flowable<ECouponIncludeDetail> getECouponDetail(int ecouponId);

    Flowable<String> getMemberECouponListByUsingDateTime(int shopId);

    Flowable<ECouponVerify> setECouponVerify(int shopId, int couponId);

    Flowable<ECouponMemberList> getMemberECouponList(int shopId);
}
