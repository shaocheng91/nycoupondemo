package com.nineyi.module.coupon.service;

import android.content.Context;

import com.nineyi.module.base.utils.MsgUtils;

/**
 * Created by shaocheng on 2017/9/29.
 */

public class MsgManager {
    public void showNetworkErrorToast(Context context) {
        MsgUtils.showNetworkErrorToast(context);
    }

    public void showDataErrorToast(Context context) {
        MsgUtils.showDataErrorToast(context);
    }

    public void showLongToast(Context context, String text) {
        MsgUtils.showLongToast(context, text);
    }

    public void showShortToast(Context context, String text) {
        MsgUtils.showShortToast(context, text);
    }
}
