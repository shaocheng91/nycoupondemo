package com.nineyi.module.coupon.ui.use.offline;

import com.nineyi.data.model.ecoupon.NyBarCode;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.model.CouponOffline;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.content.Context;
import android.view.View;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;


/**
 * Created by ReeceCheng on 2017/11/7.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponOfflineUseViewTest {
    @Mock
    CouponOfflineUseContract.Presenter mPresenter;

    private Context mContext;

    private CouponOfflineUseView mView;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        mView = new CouponOfflineUseView(mContext);
        mView.setPresenter(mPresenter);
    }

    @Test
    public void testShowBarcode_hasMember() {
        // dismiss for begin
        assertEquals(View.GONE, mView.mMemberBarcodeLayout.getVisibility());

        CouponOffline mockOffline = mock(CouponOffline.class);

        NyBarCode vip = spy(NyBarCode.class);
        vip.BarCode = "1234567890";
        vip.BarCodeTypeDef = "Code_128";

        doReturn(vip).when(mockOffline).getVipMemberBarcode();
        mView.showOfflineUse(mockOffline);

        assertEquals(View.VISIBLE, mView.mMemberBarcodeLayout.getVisibility());
        assertEquals(View.VISIBLE, mView.mMemberCode.getVisibility());
        assertEquals(View.VISIBLE, mView.mMemberBarcode.getVisibility());

        assertEquals("1234567890", mView.mMemberCode.getText().toString());
    }

    @Test
    public void testShowBarcode_hasNoMember() {
        CouponOffline mockOffline = mock(CouponOffline.class);

        doReturn(null).when(mockOffline).getVipMemberBarcode();
        mView.showOfflineUse(mockOffline);

        assertEquals(View.GONE, mView.mMemberBarcodeLayout.getVisibility());
    }

    @Test
    public void testShowBarcode_hasBarcodeOnly() {
        // dismiss for begin
        assertEquals(View.GONE, mView.mCouponBarcodeLayout.getVisibility());

        CouponOffline mockOffline = mock(CouponOffline.class);

        NyBarCode couponBarcode = spy(NyBarCode.class);
        couponBarcode.BarCode = "1234567890";
        couponBarcode.BarCodeTypeDef = "Code_128";

        doReturn(null).when(mockOffline).getVipMemberBarcode();
        doReturn(couponBarcode).when(mockOffline).getCouponBarcode();
        mView.showOfflineUse(mockOffline);

        assertEquals(View.VISIBLE, mView.mCouponBarcodeLayout.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeBarcode1.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeCode1.getVisibility());
        assertEquals("1234567890", mView.mCouponBarcodeCode1.getText().toString());

        assertEquals(View.GONE, mView.mMemberBarcodeLayout.getVisibility());
    }


    @Test
    public void testShowBarcode_hasBarcodeAndMember() {
        // dismiss for begin
        assertEquals(View.GONE, mView.mCouponBarcodeLayout.getVisibility());

        CouponOffline mockOffline = mock(CouponOffline.class);

        NyBarCode couponBarcode = spy(NyBarCode.class);
        couponBarcode.BarCode = "1234567890";
        couponBarcode.BarCodeTypeDef = "Code_128";

        NyBarCode vip = spy(NyBarCode.class);
        vip.BarCode = "abcdefghijk";
        vip.BarCodeTypeDef = "Code_128";

        doReturn(couponBarcode).when(mockOffline).getCouponBarcode();
        doReturn(vip).when(mockOffline).getVipMemberBarcode();
        mView.showOfflineUse(mockOffline);

        assertEquals(View.VISIBLE, mView.mCouponBarcodeLayout.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeBarcode1.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeCode1.getVisibility());
        assertEquals("1234567890", mView.mCouponBarcodeCode1.getText().toString());

        assertEquals(View.VISIBLE, mView.mMemberBarcodeLayout.getVisibility());
        assertEquals(View.VISIBLE, mView.mMemberBarcode.getVisibility());
        assertEquals(View.VISIBLE, mView.mMemberCode.getVisibility());
        assertEquals("abcdefghijk", mView.mMemberCode.getText().toString());
    }

    @Test
    public void testShowBarcode_customCode() {
        // dismiss for begin
        assertEquals(View.GONE, mView.mCouponBarcodeLayout.getVisibility());

        CouponOffline mockOffline = mock(CouponOffline.class);

        NyBarCode customeCode1 = spy(NyBarCode.class);
        customeCode1.BarCode = "1234567890";
        customeCode1.BarCodeTypeDef = "Code_128";

        NyBarCode customeCode2 = spy(NyBarCode.class);
        customeCode2.BarCode = "abcdefghijk";
        customeCode2.BarCodeTypeDef = "Code_128";

        NyBarCode customeCode3 = spy(NyBarCode.class);
        customeCode3.BarCode = "!@#$%^&*(";
        customeCode3.BarCodeTypeDef = "Code_128";

        doReturn(true).when(mockOffline).isUseCustomCode();
        doReturn(customeCode1).when(mockOffline).getCouponCustomCode1();
        doReturn(customeCode2).when(mockOffline).getCouponCustomCode2();
        doReturn(customeCode3).when(mockOffline).getCouponCustomCode3();
        mView.showOfflineUse(mockOffline);

        assertEquals(View.VISIBLE, mView.mCouponBarcodeLayout.getVisibility());

        assertEquals(View.VISIBLE, mView.mCouponBarcodeBarcode1.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeCode1.getVisibility());
        assertEquals("1234567890", mView.mCouponBarcodeCode1.getText().toString());

        assertEquals(View.VISIBLE, mView.mCouponBarcodeBarcode2.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeCode2.getVisibility());
        assertEquals("abcdefghijk", mView.mCouponBarcodeCode2.getText().toString());

        assertEquals(View.VISIBLE, mView.mCouponBarcodeBarcode3.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeCode3.getVisibility());
        assertEquals("!@#$%^&*(", mView.mCouponBarcodeCode3.getText().toString());
    }

    @Test
    public void testShowBarcode_sideCaseCustomCode() {
        // dismiss for begin
        assertEquals(View.GONE, mView.mCouponBarcodeLayout.getVisibility());

        CouponOffline mockOffline = mock(CouponOffline.class);

        NyBarCode customeCode1 = spy(NyBarCode.class);
        customeCode1.BarCode = null;
        customeCode1.BarCodeTypeDef = "Code_128";

        NyBarCode customeCode2 = spy(NyBarCode.class);
        customeCode2.BarCode = "abcdefghijk";
        customeCode2.BarCodeTypeDef = "????";


        doReturn(true).when(mockOffline).isUseCustomCode();
        doReturn(customeCode1).when(mockOffline).getCouponCustomCode1();
        doReturn(customeCode2).when(mockOffline).getCouponCustomCode2();
        doReturn(null).when(mockOffline).getCouponCustomCode3();
        mView.showOfflineUse(mockOffline);

        assertEquals(View.VISIBLE, mView.mCouponBarcodeLayout.getVisibility());

        assertEquals(View.GONE, mView.mCouponBarcodeBarcode1.getVisibility());
        assertEquals(View.GONE, mView.mCouponBarcodeCode1.getVisibility());

        assertEquals(View.VISIBLE, mView.mCouponBarcodeBarcode2.getVisibility());
        assertEquals(View.VISIBLE, mView.mCouponBarcodeCode2.getVisibility());
        assertEquals("abcdefghijk", mView.mCouponBarcodeCode2.getText().toString());

        assertEquals(View.GONE, mView.mCouponBarcodeBarcode3.getVisibility());
        assertEquals(View.GONE, mView.mCouponBarcodeCode3.getVisibility());
    }

}
