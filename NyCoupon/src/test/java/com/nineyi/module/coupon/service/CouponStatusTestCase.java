package com.nineyi.module.coupon.service;

import java.util.Date;

/**
 * Created by shaocheng on 2017/11/16.
 */

public class CouponStatusTestCase {
    public String typeDef;
    public long usingStartDateTime;
    public long usingEndDateTime;
    public long startDateTime;
    public long endDateTime;
    public int couponTotalCount;
    public boolean hasNormalCoupon;
    public boolean isUsing;
    public String status;
    public boolean isFirstDownloadCollected;
    public long firstDownloadAppTime;
}
