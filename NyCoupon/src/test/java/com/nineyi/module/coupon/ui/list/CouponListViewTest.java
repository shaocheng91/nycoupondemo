package com.nineyi.module.coupon.ui.list;

import android.content.Context;
import android.view.View;

import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by shaocheng on 2017/9/15.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponListViewTest {

    @Mock
    CouponListContract.Presenter presenter;

    @Mock
    MsgManager msgManager;

    @Mock
    NavManager mNavManager;

    @Mock
    CouponListView.OnCouponListRefreshedListener mOnCouponListRefreshedListener;

    Context mContext;

    CouponListView mView;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        mView = new CouponListView(mContext);
        mView.setPresenter(presenter);
        mView.setMsgManager(msgManager);
        mView.setNavManager(mNavManager);
        mView.setOnCouponListRefreshedListener(mOnCouponListRefreshedListener);
    }

    @Test
    public void testShowTakingCoupon() {
        mView.showTakingCoupon();

        assertEquals(VISIBLE, mView.mProgressBar.getVisibility());
    }

    @Test
    public void testShowTakeCouponSuccess() {
        mView.showCollectCouponSuccess();

        assertEquals(GONE, mView.mProgressBar.getVisibility());
    }

    @Test
    public void testShowCouponList_empty() {
        mView.showCouponList(new ArrayList<CouponListItem>());

        checkChildOnlyVisible(mView.mEmptyView);

        verify(mOnCouponListRefreshedListener).onCouponListRefreshed();
    }

    @Test
    public void testShowEmpty() {
        mView.showEmpty();

        checkChildOnlyVisible(mView.mEmptyView);

        verify(mOnCouponListRefreshedListener).onCouponListRefreshed();
    }

    @Test
    public void testShowError() {
        mView.showError();

        checkChildOnlyVisible(mView.mErrorView);

        verify(mOnCouponListRefreshedListener).onCouponListRefreshed();
    }

    private void checkChildOnlyVisible(View childView) {
        int count = mView.getChildCount();
        for (int i = 0; i < count; i++) {
            View c = mView.getChildAt(i);
            if (c == childView) {
                assertEquals(VISIBLE, c.getVisibility());
            } else {
                assertEquals(GONE, c.getVisibility());
            }
        }
    }

    @Test
    public void clickOnErrorRetry() {
        mView.mErrorActionButton.callOnClick();

        verify(presenter).loadCouponList(true);
    }
}


