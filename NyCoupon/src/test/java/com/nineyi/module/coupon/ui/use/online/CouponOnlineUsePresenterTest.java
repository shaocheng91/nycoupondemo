package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.data.model.shoppingcart.v4.ShoppingCartData;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.CouponShoppingCartDataManager;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItem;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponNotUse;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUnuseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemEmpty;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemSeparator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


/**
 * Created by ReeceCheng on 2017/10/18.
 */


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponOnlineUsePresenterTest {

    Context mContext;

    @Mock
    CouponOnlineUseContract.View mockView;

    @Mock
    CouponManager mockCouponManager;

    @Mock
    CouponShoppingCartDataManager mockCouponShoppingCartDataManager;

    @Mock
    CompositeDisposableHelper mockCompositeDisposableHelper;

    @Mock
    CouponOnlineUsePresenter.OnCouponSelectedSubmitListener mockSubmitListener;

    @Mock
    ShoppingCartData mockShoppingCartData;


    CouponOnlineUsePresenter mPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        doReturn(mockShoppingCartData).when(mockCouponShoppingCartDataManager).getShoppingCartData();

        mPresenter = new CouponOnlineUsePresenter(mContext, mockView,
                mockCouponManager, mockCouponShoppingCartDataManager, mockCompositeDisposableHelper, mockSubmitListener);
    }

    /**
     * group is USEABLE if isAchieveUsingMinPrice is TRUE
     */
    @Test
    public void testChooseGroup_useable() {
        CouponOnline mockCoupon = mock(CouponOnline.class);
        doReturn(true).when(mockCoupon).isAchieveUsingMinPrice();

        CouponOnlineUsePresenter.Group actual = mPresenter.choosesGroupForCoupon(mockCoupon);
        assertEquals(CouponOnlineUsePresenter.Group.USEABLE, actual);
    }

    /**
     * group is UNUSEABLE if isAchieveUsingMinPrice is FALSE
     */
    @Test
    public void testChooseGroup_unuseable() {
        CouponOnline mockCoupon = mock(CouponOnline.class);
        doReturn(false).when(mockCoupon).isAchieveUsingMinPrice();

        CouponOnlineUsePresenter.Group actual = mPresenter.choosesGroupForCoupon(mockCoupon);
        assertEquals(CouponOnlineUsePresenter.Group.UNUSEABLE, actual);
    }

    @Test
    public void testSelectedCoupon() {
        CouponOnline mockCoupon = mock(CouponOnline.class);
        doReturn(1991).when(mockCoupon).getId();

        mPresenter.selectedCoupon(mockCoupon);
        assertEquals(1991, mPresenter.getSelectedCoupon().getId());
    }

    @Test
    public void testSubmit_hasSelectedCoupon() {
        CouponOnline mockCoupon = mock(CouponOnline.class);
        doReturn(1991).when(mockCoupon).getId();

        mPresenter.selectedCoupon(mockCoupon);
        mPresenter.submit();

        verify(mockCouponShoppingCartDataManager, times(1)).updateSelectedEcouponSlaveId(1991);
        verify(mockShoppingCartData, times(1)).setSelectedECouponSlaveId(1991);
        verify(mockCouponShoppingCartDataManager, times(1)).setShouldRefreshShoppingCart();

        verify(mockCouponShoppingCartDataManager, times(0)).updateSelectedEcouponSlaveId(0);
        verify(mockShoppingCartData, times(0)).setSelectedECouponSlaveId(0);

        verify(mockSubmitListener, times(1)).onSubmit();
    }

    @Test
    public void testSubmit_hasNoSelectedCoupon() {
        mPresenter.selectedCoupon(null);
        mPresenter.submit();

        verify(mockCouponShoppingCartDataManager, times(1)).updateSelectedEcouponSlaveId(0);
        verify(mockShoppingCartData, times(1)).setSelectedECouponSlaveId(0);

        verify(mockCouponShoppingCartDataManager, times(1)).setShouldRefreshShoppingCart();

        verify(mockSubmitListener, times(1)).onSubmit();
    }

    @Test
    public void testConvertCouponsToItems_empty() {
        List<CouponOnline> mockCoupons = new ArrayList<>();

        List<CouponOnlineUseItem> items = mPresenter.convertCouponsToItems(mockCoupons);

        assertTrue(items.isEmpty());
    }

    @Test
    public void testConvertCouponsToItems_oneUsable() {
        List<CouponOnline> mockCoupons = new ArrayList<>();

        CouponOnline usableCoupon = mock(CouponOnline.class);
        doReturn(true).when(usableCoupon).isAchieveUsingMinPrice();

        mockCoupons.add(usableCoupon);

        List<CouponOnlineUseItem> items = mPresenter.convertCouponsToItems(mockCoupons);

        assertEquals(3, items.size());

        /*
         *
         * 可使用 --------------
         *
         *    |----------------
         * 0  | 我是可使用折價卷
         *    |
         *    |----------------
         *
         *    |----------------
         * 0  | 不使用折價卷
         *    |----------------
         */
        assertTrue(items.get(0) instanceof CouponOnlineUseItemSeparator);
        assertTrue(items.get(1) instanceof CouponOnlineUseItemCouponUseable);
        assertTrue(items.get(2) instanceof CouponOnlineUseItemCouponNotUse);
    }

    @Test
    public void testConvertCouponsToItems_oneUnusable() {
        List<CouponOnline> mockCoupons = new ArrayList<>();

        CouponOnline usableCoupon = mock(CouponOnline.class);
        doReturn(false).when(usableCoupon).isAchieveUsingMinPrice();

        mockCoupons.add(usableCoupon);

        List<CouponOnlineUseItem> items = mPresenter.convertCouponsToItems(mockCoupons);

        assertEquals(4, items.size());

         /*
         *
         * 可使用 --------------
         *
         * 沒有使用的折價卷
         *
         * 無法使用 ------------
         *
         *    |----------------
         * X  | 我是無法使用折價卷
         *    |
         *    |----------------
         */
        assertTrue(items.get(0) instanceof CouponOnlineUseItemSeparator);
        assertTrue(items.get(1) instanceof CouponOnlineUseItemEmpty);
        assertTrue(items.get(2) instanceof CouponOnlineUseItemSeparator);
        assertTrue(items.get(3) instanceof CouponOnlineUseItemCouponUnuseable);
    }

    @Test
    public void testConvertCouponsToItems_oneUsableOneUnusable() {
        List<CouponOnline> mockCoupons = new ArrayList<>();

        CouponOnline unusableCoupon = mock(CouponOnline.class);
        doReturn(false).when(unusableCoupon).isAchieveUsingMinPrice();

        mockCoupons.add(unusableCoupon);

        CouponOnline usableCoupon = mock(CouponOnline.class);
        doReturn(true).when(usableCoupon).isAchieveUsingMinPrice();


        mockCoupons.add(usableCoupon);

        List<CouponOnlineUseItem> items = mPresenter.convertCouponsToItems(mockCoupons);

        assertEquals(5, items.size());

         /*
         *
         * 可使用 --------------
         *
         *    |----------------
         * 0  | 我是可使用折價卷
         *    |
         *    |----------------
         *
         *    |----------------
         * 0  | 不使用折價卷
         *    |----------------
         *
         * 無法使用 ------------
         *
         *    |----------------
         * X  | 我是無法使用折價卷
         *    |
         *    |----------------
         */
        assertTrue(items.get(0) instanceof CouponOnlineUseItemSeparator);
        assertTrue(items.get(1) instanceof CouponOnlineUseItemCouponUseable);
        assertTrue(items.get(2) instanceof CouponOnlineUseItemCouponNotUse);
        assertTrue(items.get(3) instanceof CouponOnlineUseItemSeparator);
        assertTrue(items.get(4) instanceof CouponOnlineUseItemCouponUnuseable);
    }
}
