package com.nineyi.module.coupon.ui.detail;

import android.content.Context;

import com.nineyi.data.model.ecoupon.ECouponDetail;
import com.nineyi.data.model.ecoupon.ECouponMemberECouponStatusList;
import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.CollectCouponException;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.LoginManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Date;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;

import static com.nineyi.module.coupon.ui.detail.DetailContract.Action.NONE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by shaocheng on 2017/9/15.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DetailPresenterTest {

    @Mock
    CouponManager mCouponManager;

    ECouponDetail mockECouponDetail;

    ECouponMemberECouponStatusList mockECouponMemberECouponStatusList;

    @Mock
    Coupon mockCoupon;

    @Mock
    DetailContract.View mView;

    @Mock
    CompositeDisposableHelper mCompositeDisposableHelper;

    @Mock
    LoginManager mLoginManager;

    Context mContext;

    DetailPresenter mPresenter;

    int mCouponId = 123;

    String mFrom = DetailContract.ARG_FROM_OTHER;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        doReturn(true).when(mLoginManager).isLogin();

        mockECouponDetail = new ECouponDetail();
        mockECouponDetail.EndDateTime = new NineyiDate("", "", "");
        mockECouponDetail.UsingEndDateTime = new NineyiDate("", "", "");
        doReturn(Single.just(mockECouponDetail)).when(mCouponManager).getCouponDetail(mCouponId);

        mockECouponMemberECouponStatusList = new ECouponMemberECouponStatusList();
        doReturn(Single.just(mockECouponMemberECouponStatusList)).when(mCouponManager).getCouponStatus(mCouponId);

        doReturn(Coupon.Status.AVAILABLE).when(mockCoupon).getStatus();

        doReturn(mCouponId).when(mockCoupon).getId();

        doReturn(mockCoupon).when(mCouponManager).buildCouponByDetailAndStatus(any(ECouponDetail.class), any(ECouponMemberECouponStatusList.class), any(Date.class));
        
        doReturn(Completable.complete()).when(mCouponManager).takeCoupon(mCouponId, true);
        doReturn(Completable.complete()).when(mCouponManager).takeCoupon(mCouponId, false);

        mContext = RuntimeEnvironment.application;

        mPresenter = new DetailPresenter(mView, mCompositeDisposableHelper, mCouponManager, mCouponId, mFrom, mLoginManager);
    }

    @Test
    public void testLoadCouponDetail_cache_should_work() {

        mPresenter.loadCouponDetail(false);

        verify(mCouponManager).getCouponDetail(mCouponId);
        verify(mCouponManager).getCouponStatus(mCouponId);

        mPresenter.loadCouponDetail(false);

        verify(mCouponManager).getCouponDetail(mCouponId);
        verify(mCouponManager).getCouponStatus(mCouponId);

        mPresenter.loadCouponDetail(true);

        verify(mCouponManager).getCouponDetail(mCouponId);
        verify(mCouponManager, times(2)).getCouponStatus(mCouponId);
    }

    @Test
    public void testLoadCouponDetail_success_couponAvailable() {

        mPresenter.loadCouponDetail(false);

        verify(mView).showLoading();
        verify(mView).showDetail(mockCoupon);
        verify(mView).showShareButton(true);
    }

    @Test
    public void testLoadCouponDetail_success_couponCollected_online() {

        doReturn(Coupon.Status.COLLECTED).when(mockCoupon).getStatus();
        doReturn(true).when(mockCoupon).isOnline();

        mPresenter.loadCouponDetail(false);

        verify(mView).showLoading();
        verify(mView).showDetail(mockCoupon);
        verify(mView).showShareButton(true);
    }

    @Test
    public void testLoadCouponDetail_success_couponCollected_offline() {

        doReturn(Coupon.Status.COLLECTED).when(mockCoupon).getStatus();
        doReturn(true).when(mockCoupon).isOffline();

        mPresenter.loadCouponDetail(false);

        verify(mView).showLoading();
        verify(mView).showDetail(mockCoupon);
        verify(mView).showShareButton(true);
    }

    @Test
    public void testLoadCouponDetail_success_couponNotAvailable_notQualified() {

        doReturn(Coupon.Status.FIRST_DOWNLOAD_NOT_QUALIFIED).when(mockCoupon).getStatus();

        mPresenter.loadCouponDetail(false);

        verify(mView).showLoading();
        verify(mView).showDetail(mockCoupon);
        verify(mView).showAction(NONE, R.string.coupon_list_item_status_invalidate);
        verify(mView).showShareButton(true);
    }

    @Test
    public void testLoadCouponDetail_success_couponNotAvailable_noMoreCoupons() {

        doReturn(Coupon.Status.NO_MORE_COUPONS).when(mockCoupon).getStatus();

        mPresenter.loadCouponDetail(false);

        verify(mView).showLoading();
        verify(mView).showDetail(mockCoupon);
        verify(mView).showAction(NONE, R.string.coupon_list_item_status_out_of_stock);
        verify(mView).showShareButton(true);
    }
    
    @Test
    public void testLoadCouponDetail_error() {

        doReturn(Single.error(new Exception())).when(mCouponManager).getCouponDetail(mCouponId);

        mPresenter.loadCouponDetail(false);

        verify(mView).showShareButton(false);
    }

    @Test
    public void testShouldShowShareButton_showPresentCoupon() {
        final Coupon mockCoupon = mock(Coupon.class);
        doReturn(true).when(mockCoupon).isShowPresent();

        assertFalse(mPresenter.shouldShowShareButton(mockCoupon));
    }

    @Test
    public void testShouldShowShareButton_codeCoupon() {
        final Coupon mockCoupon = mock(Coupon.class);
        doReturn(true).when(mockCoupon).isCode();

        assertFalse(mPresenter.shouldShowShareButton(mockCoupon));
    }

    @Test
    public void testShouldShowShareButton_unknownCoupon() {
        final Coupon mockCoupon = mock(Coupon.class);
        doReturn(true).when(mockCoupon).isUnknown();

        assertFalse(mPresenter.shouldShowShareButton(mockCoupon));
    }

    @Test
    public void testShouldShowShareButton_fromShoppingCart() {
        final Coupon mockCoupon = mock(Coupon.class);
        doReturn(true).when(mockCoupon).isUnknown();

        mPresenter = new DetailPresenter(mView, mCompositeDisposableHelper, mCouponManager, mCouponId,
                DetailContract.ARG_FROM_SHOPPING_CART, mLoginManager);

        assertFalse(mPresenter.shouldShowShareButton(mockCoupon));
    }

    @Test
    public void testShouldShowShareButton() {
        final Coupon mockCoupon = mock(Coupon.class);

        assertTrue(mPresenter.shouldShowShareButton(mockCoupon));
    }

    @Test
    public void testDoAction_collect_online() {
        doReturn(mCouponId).when(mockCoupon).getId();
        doReturn(false).when(mockCoupon).isFirstDownload();
        doReturn(Coupon.Status.AVAILABLE).when(mockCoupon).getStatus();
        doReturn(true).when(mockCoupon).isOnline();

        doReturn(mockCoupon).when(mCouponManager).buildCouponByDetailAndStatus(any(ECouponDetail.class),
                any(ECouponMemberECouponStatusList.class), any(Date.class));

        doReturn(Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                doReturn(Coupon.Status.COLLECTED).when(mockCoupon).getStatus();
            }
        })).when(mCouponManager).takeCoupon(mCouponId, false);

        mPresenter.loadCouponDetail(false);

        mPresenter.doAction(DetailContract.Action.COLLECT);

        verify(mView).showActionLoading();
        verify(mView).showCollectSuccess();
        verify(mView, times(2)).showDetail(mockCoupon);
        verify(mView, times(2)).showShareButton(true);
    }

    @Test
    public void testDoAction_collect_offline() {
        doReturn(mCouponId).when(mockCoupon).getId();
        doReturn(false).when(mockCoupon).isFirstDownload();
        doReturn(Coupon.Status.AVAILABLE).when(mockCoupon).getStatus();
        doReturn(true).when(mockCoupon).isOffline();

        doReturn(mockCoupon).when(mCouponManager).buildCouponByDetailAndStatus(any(ECouponDetail.class),
                any(ECouponMemberECouponStatusList.class), any(Date.class));

        doReturn(Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                doReturn(Coupon.Status.COLLECTED).when(mockCoupon).getStatus();
            }
        })).when(mCouponManager).takeCoupon(mCouponId, false);

        mPresenter.loadCouponDetail(false);

        mPresenter.doAction(DetailContract.Action.COLLECT);

        verify(mView).showActionLoading();
        verify(mView).showCollectSuccess();
        verify(mView, times(2)).showDetail(mockCoupon);
        verify(mView, times(2)).showShareButton(true);
    }

    @Test
    public void testDoAction_collect_error() {
        doReturn(mCouponId).when(mockCoupon).getId();
        doReturn(false).when(mockCoupon).isFirstDownload();
        doReturn(Coupon.Status.AVAILABLE).when(mockCoupon).getStatus();

        doReturn(mockCoupon).when(mCouponManager).buildCouponByDetailAndStatus(any(ECouponDetail.class),
                any(ECouponMemberECouponStatusList.class), any(Date.class));

        doReturn(Completable.error(new CollectCouponException(CollectCouponException.ErrorType.FAIL)))
                .when(mCouponManager).takeCoupon(mCouponId, false);

        mPresenter.loadCouponDetail(false);

        mPresenter.doAction(DetailContract.Action.COLLECT);

        verify(mView).showDetail(mockCoupon);
        verify(mView).showShareButton(true);

        verify(mView).showActionLoading();

        verify(mView).showCollectError(R.string.ecoupon_get_fail_title, null);
    }

    @Test
    public void testDoAction_collect_notLoggedIn() {
        doReturn(false).when(mLoginManager).isLogin();

        doReturn(mCouponId).when(mockCoupon).getId();
        doReturn(false).when(mockCoupon).isFirstDownload();
        doReturn(Coupon.Status.AVAILABLE).when(mockCoupon).getStatus();

        doReturn(mockCoupon).when(mCouponManager).buildCouponByDetailAndStatus(any(ECouponDetail.class),
                any(ECouponMemberECouponStatusList.class), any(Date.class));

        mPresenter.loadCouponDetail(false);

        mPresenter.doAction(DetailContract.Action.COLLECT);

        verify(mView).showDetail(mockCoupon);
        verify(mView).showShareButton(true);

        verify(mView).goToLogin();
    }

    @Test
    public void testCollectPendingCoupon() {
        doReturn(false).when(mLoginManager).isLogin();

        doReturn(mCouponId).when(mockCoupon).getId();
        doReturn(false).when(mockCoupon).isFirstDownload();
        doReturn(Coupon.Status.AVAILABLE).when(mockCoupon).getStatus();
        doReturn(true).when(mockCoupon).isOnline();

        doReturn(mockCoupon).when(mCouponManager).buildCouponByDetailAndStatus(any(ECouponDetail.class),
                any(ECouponMemberECouponStatusList.class), any(Date.class));

        doReturn(Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                doReturn(Coupon.Status.COLLECTED).when(mockCoupon).getStatus();
            }
        })).when(mCouponManager).takeCoupon(mCouponId, false);

        mPresenter.loadCouponDetail(false);

        verify(mView).showDetail(mockCoupon);
        verify(mView).showShareButton(true);

        mPresenter.doAction(DetailContract.Action.COLLECT);

        verify(mView).goToLogin();

        doReturn(true).when(mLoginManager).isLogin();

        mPresenter.loadCouponDetail(false);

        verify(mView).showActionLoading();
        verify(mView).showCollectSuccess();

        verify(mView, times(3)).showDetail(mockCoupon);
        verify(mView, times(3)).showShareButton(true);
    }

    @Test
    public void testWhenNotLoginDoNotCollectPendingCoupon() {
        doReturn(false).when(mLoginManager).isLogin();

        doReturn(mCouponId).when(mockCoupon).getId();
        doReturn(false).when(mockCoupon).isFirstDownload();
        doReturn(Coupon.Status.AVAILABLE).when(mockCoupon).getStatus();
        doReturn(true).when(mockCoupon).isOnline();

        doReturn(mockCoupon).when(mCouponManager).buildCouponByDetailAndStatus(any(ECouponDetail.class),
                any(ECouponMemberECouponStatusList.class), any(Date.class));

        mPresenter.loadCouponDetail(false);

        mPresenter.doAction(DetailContract.Action.COLLECT);

        verify(mView).showDetail(mockCoupon);
        verify(mView).showShareButton(true);

        verify(mView).goToLogin();

        mPresenter.loadCouponDetail(false);

        verify(mView, times(0)).showActionLoading();
        verify(mView, times(0)).showCollectSuccess();
        verify(mView, times(2)).showDetail(mockCoupon);
        verify(mView, times(2)).showShareButton(true);
    }
}


