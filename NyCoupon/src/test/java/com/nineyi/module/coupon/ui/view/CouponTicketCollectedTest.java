package com.nineyi.module.coupon.ui.view;

import android.content.Context;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.module.base.ui.countdown.CountdownManager;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.ui.list.CouponListContract;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by shaocheng on 2017/9/15.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponTicketCollectedTest {

    @Mock
    CouponListContract.Presenter presenter;

    @Mock
    NineyiDate mockTime;

    @Mock
    CountdownManager mCountdownManager;

    private Context mContext;

    private CouponTicketCollected mView;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        mView = new CouponTicketCollected(mContext);
        mView.setCountdownManager(mCountdownManager);
    }

    @Test
    public void testShowCouponFirstDownload() {

        Coupon coupon = mock(Coupon.class);
        doReturn(true).when(coupon).isFirstDownload();
        doReturn(mockTime).when(coupon).getUsingEndDateTime();

        mView.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title_first_download), mView.mCouponTitle.getText().toString());
    }

    @Test
    public void testShowCouponNormal() {
        Coupon coupon = mock(Coupon.class);
        doReturn(mockTime).when(coupon).getUsingEndDateTime();

        mView.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title), mView.mCouponTitle.getText().toString());
    }

    @Test
    public void testShowCoupon_checkPrice() {
        Coupon coupon = mock(Coupon.class);
        doReturn(mockTime).when(coupon).getUsingEndDateTime();
        doReturn(100.0).when(coupon).getDiscountPrice();

        mView.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title), mView.mCouponTitle.getText().toString());
        assertEquals("100", mView.mPrice.getText().toString());
    }

    @Test
    public void testShowCoupon_noCountDown() {
        long usingEndTimeLong = new Date().getTime() + TimeUnit.DAYS.toMillis(3);

        Coupon coupon = mock(Coupon.class);
        doReturn(mockTime).when(coupon).getUsingEndDateTime();
        doReturn(usingEndTimeLong).when(mockTime).getTimeLong();

        mView.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title), mView.mCouponTitle.getText().toString());

        String expected = mContext.getString(R.string.coupon_list_item_use_end_time,
                new SimpleDateFormat("yyyy/MM/dd").format(usingEndTimeLong));
        assertEquals(expected, mView.mEndTime.getText().toString());

        assertEquals(GONE, mView.mCountDownTitle.getVisibility());
        assertEquals(GONE, mView.mCountDown.getVisibility());
        assertEquals(VISIBLE, mView.mEndTime.getVisibility());
    }
}


