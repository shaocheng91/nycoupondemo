package com.nineyi.module.coupon.ui.list;

import android.content.Context;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.CollectCouponException;
import com.nineyi.module.coupon.service.CouponManager;
import com.nineyi.module.coupon.service.CouponSharePreferenceHelper;
import com.nineyi.module.coupon.service.GetCouponListException;
import com.nineyi.module.coupon.service.LoginManager;
import com.nineyi.module.coupon.service.MigrationManager;
import com.nineyi.module.coupon.service.NavManager;
import com.nineyi.module.coupon.service.PendingCouponHandler;
import com.nineyi.module.coupon.service.PromotionSharePreferenceHelper;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItem;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemCoupon;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemEmpty;
import com.nineyi.module.coupon.ui.list.wrapper.CouponListItemSeparator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

import static com.nineyi.module.coupon.model.Coupon.Status.AVAILABLE;
import static com.nineyi.module.coupon.model.Coupon.Status.COLLECTED;
import static com.nineyi.module.coupon.model.Coupon.Status.FIRST_DOWNLOAD_COLLECTED;
import static com.nineyi.module.coupon.model.Coupon.Status.FIRST_DOWNLOAD_NOT_QUALIFIED;
import static com.nineyi.module.coupon.model.Coupon.Status.NO_MORE_COUPONS;
import static com.nineyi.module.coupon.model.Coupon.Status.USED;
import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_AVAILABLE;
import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_COLLECTED;
import static com.nineyi.module.coupon.ui.list.CouponListAdapter.VIEW_TYPE_INVALID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by shaocheng on 2017/9/15.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponListPresenterTest {

    Context mContext;

    @Mock
    CouponManager mockCouponManager;

    @Mock
    MigrationManager mockMigrationManager;

    @Mock
    CouponListContract.View mockView;

    @Mock
    PromotionSharePreferenceHelper mockPromotionSharePreferenceHelper;

    @Mock
    CompositeDisposableHelper mockCompositeDisposableHelper;

    @Mock
    LoginManager loginManager;

    @Mock
    NavManager navManager;

    @Mock
    CouponSharePreferenceHelper couponSharePreferenceHelper;

    @Spy
    @InjectMocks
    PendingCouponHandler mPendingCouponHandler;

    @Mock
    CouponListPresenter.OnTakeCouponSuccessCallback mOnTakeCouponSuccessCallback;

    CouponListPresenter mPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;


        mPresenter = new CouponListPresenter(mContext, mockCouponManager,
                mockMigrationManager, mockView, mockPromotionSharePreferenceHelper,
                mockCompositeDisposableHelper, loginManager, navManager, mPendingCouponHandler);
    }

    @Test
    public void testChooseGroup_available() {

        Coupon mockCoupon = mock(Coupon.class);
        doReturn(AVAILABLE).when(mockCoupon).getStatus();

        CouponListPresenter.Group actual = mPresenter.chooseGroupForCoupon(mockCoupon);

        assertEquals(CouponListPresenter.Group.AVAILABLE, actual);
    }

    @Test
    public void testChooseGroup_collected() {

        Coupon mockCoupon = mock(Coupon.class);
        doReturn(COLLECTED).when(mockCoupon).getStatus();

        CouponListPresenter.Group actual = mPresenter.chooseGroupForCoupon(mockCoupon);

        assertEquals(CouponListPresenter.Group.COLLECTED, actual);
    }

    @Test
    public void testChooseGroup_noMoreCoupons() {

        Coupon mockCoupon = mock(Coupon.class);
        doReturn(NO_MORE_COUPONS).when(mockCoupon).getStatus();

        CouponListPresenter.Group actual = mPresenter.chooseGroupForCoupon(mockCoupon);

        assertEquals(CouponListPresenter.Group.INVALID, actual);
    }

    @Test
    public void testChooseGroup_firstDownloadCollected() {

        Coupon mockCoupon = mock(Coupon.class);
        doReturn(FIRST_DOWNLOAD_COLLECTED).when(mockCoupon).getStatus();

        CouponListPresenter.Group actual = mPresenter.chooseGroupForCoupon(mockCoupon);

        assertEquals(CouponListPresenter.Group.INVALID, actual);
    }

    @Test
    public void testChooseGroup_firstDownloadNotQualified() {
        Coupon mockCoupon = mock(Coupon.class);
        doReturn(FIRST_DOWNLOAD_NOT_QUALIFIED).when(mockCoupon).getStatus();

        CouponListPresenter.Group actual = mPresenter.chooseGroupForCoupon(mockCoupon);

        assertEquals(CouponListPresenter.Group.INVALID, actual);
    }

    @Test
    public void testChooseGroup_used() {
        Coupon mockCoupon = mock(Coupon.class);
        doReturn(USED).when(mockCoupon).getStatus();

        CouponListPresenter.Group actual = mPresenter.chooseGroupForCoupon(mockCoupon);

        assertEquals(CouponListPresenter.Group.HIDDEN, actual);
    }

    @Test
    public void testTakeCoupon_success() {
        doReturn(true).when(loginManager).isLogin();

        int couponId = 7;
        boolean isFirstDownload = false;

        Coupon mockCoupon = mock(Coupon.class);
        doReturn(couponId).when(mockCoupon).getId();
        doReturn(isFirstDownload).when(mockCoupon).isFirstDownload();

        doReturn(Completable.complete()).when(mockCouponManager).takeCoupon(couponId, isFirstDownload);
        doReturn(Single.just(new ArrayList<Coupon>())).when(mockCouponManager).getCouponList();

        mPresenter.collectCoupon(mockCoupon, mOnTakeCouponSuccessCallback);

        verify(mockCouponManager, times(1)).takeCoupon(couponId, isFirstDownload);

        verify(mockCompositeDisposableHelper, times(1)).add(Matchers.<Disposable>any());

        verify(mockView, times(1)).showCollectCouponSuccess();
        verify(mOnTakeCouponSuccessCallback, times(1)).onTakeCouponSuccess();
    }

    @Test
    public void testTakeCoupon_error() {
        doReturn(true).when(loginManager).isLogin();

        int couponId = 7;
        boolean isFirstDownload = false;

        Coupon mockCoupon = mock(Coupon.class);
        doReturn(couponId).when(mockCoupon).getId();
        doReturn(isFirstDownload).when(mockCoupon).isFirstDownload();

        String errMsg = "error!";
        doReturn(Completable.error(new CollectCouponException(CollectCouponException.ErrorType.FAIL, errMsg))).when(mockCouponManager).takeCoupon(couponId, isFirstDownload);

        mPresenter.collectCoupon(mockCoupon, mOnTakeCouponSuccessCallback);

        verify(mockCouponManager, times(1)).takeCoupon(couponId, isFirstDownload);

        verify(mockCompositeDisposableHelper, times(1)).add(Matchers.<Disposable>any());

        verify(mockView, times(1)).showCollectCouponError(R.string.ecoupon_get_fail_title, errMsg);
    }

    @Test
    public void testTakeCoupon_notLogin() {
        doReturn(false).when(loginManager).isLogin();

        int couponId = 7;
        boolean isFirstDownload = false;

        Coupon mockCoupon = mock(Coupon.class);
        doReturn(couponId).when(mockCoupon).getId();
        doReturn(isFirstDownload).when(mockCoupon).isFirstDownload();

        mPresenter.collectCoupon(mockCoupon, mOnTakeCouponSuccessCallback);

        verify(loginManager, times(2)).isLogin();
        verify(mPendingCouponHandler, times(1)).savePendingCouponId(couponId);
        verify(mPendingCouponHandler, times(1)).savePendingCouponIsFirstDownload(isFirstDownload);

        verify(navManager, times(1)).navigateLogin(mContext);
    }

    @Test
    public void testTakePendingCoupon() {
        doReturn(true).when(loginManager).isLogin();

        int couponId = 7;
        doReturn(couponId).when(mPendingCouponHandler).loadPendingCouponId();

        boolean isFirstDownload = false;
        doReturn(isFirstDownload).when(mPendingCouponHandler).loadPendingCouponIsFirstDownload();

        doReturn(Completable.complete()).when(mockCouponManager).takeCoupon(couponId, isFirstDownload);
        doReturn(Single.just(new ArrayList<Coupon>())).when(mockCouponManager).getCouponList();
        doReturn(mOnTakeCouponSuccessCallback).when(mPendingCouponHandler).getCallback();
        doReturn(true).when(mPendingCouponHandler).hasOnTakeCouponSuccessCallback();

        mPresenter.collectPendingCoupon();

        verify(mockCouponManager, times(1)).takeCoupon(couponId, isFirstDownload);
        verify(mOnTakeCouponSuccessCallback, times(1)).onTakeCouponSuccess();

        // Clear up
        verify(mPendingCouponHandler, times(1)).clear();
        verify(couponSharePreferenceHelper, times(1)).removePendingCouponId();
        verify(couponSharePreferenceHelper, times(1)).removePendingCouponIsFirstDownload();
    }

    @Test
    public void testTakePendingCoupon_alreadyTaken() {
        doReturn(true).when(loginManager).isLogin();

        int couponId = 7;
        doReturn(couponId).when(mPendingCouponHandler).loadPendingCouponId();

        boolean isFirstDownload = false;
        doReturn(isFirstDownload).when(mPendingCouponHandler).loadPendingCouponIsFirstDownload();

        doReturn(Completable.error(new CollectCouponException(CollectCouponException.ErrorType.ALREADY_COLLECTED)))
                .when(mockCouponManager).takeCoupon(couponId, isFirstDownload);
        doReturn(Single.just(new ArrayList<Coupon>())).when(mockCouponManager).getCouponList();

        mPresenter.collectPendingCoupon();

        verify(mockCouponManager, times(1)).takeCoupon(couponId, isFirstDownload);

        verify(mockView).showCollectCouponError(R.string.coupon_collect_error_first_download_already_collected);

        // Clear up
        verify(mPendingCouponHandler, times(1)).clear();
        verify(couponSharePreferenceHelper, times(1)).removePendingCouponId();
        verify(couponSharePreferenceHelper, times(1)).removePendingCouponIsFirstDownload();
    }

    @Test
    public void testTakePendingCoupon_notLoggedIn() {
        doReturn(false).when(loginManager).isLogin();

        mPresenter.collectPendingCoupon();

        verify(mPendingCouponHandler, times(1)).clear();
        verify(couponSharePreferenceHelper, times(1)).removePendingCouponId();
        verify(couponSharePreferenceHelper, times(1)).removePendingCouponIsFirstDownload();

        verify(mockCouponManager, times(0)).takeCoupon(anyInt(), anyBoolean());
        verify(mockCouponManager, times(0)).getCouponList();
    }

    @Test
    public void testTakenCouponComparator_lessThan() {
        CouponListPresenter.CollectedCouponComparator comparator = new CouponListPresenter.CollectedCouponComparator();

        Coupon c1 = mock(Coupon.class);
        doReturn(new NineyiDate("", "100", "")).when(c1).getUsingEndDateTime();

        Coupon c2 = mock(Coupon.class);
        doReturn(new NineyiDate("", "200", "")).when(c2).getUsingEndDateTime();

        assertEquals(-1, comparator.compare(c1, c2));
    }

    @Test
    public void testTakenCouponComparator_greaterThan() {
        CouponListPresenter.CollectedCouponComparator comparator = new CouponListPresenter.CollectedCouponComparator();

        Coupon c1 = mock(Coupon.class);
        doReturn(new NineyiDate("", "200", "")).when(c1).getUsingEndDateTime();

        Coupon c2 = mock(Coupon.class);
        doReturn(new NineyiDate("", "100", "")).when(c2).getUsingEndDateTime();

        assertEquals(1, comparator.compare(c1, c2));
    }

    @Test
    public void testTakenCouponComparator_equal() {
        CouponListPresenter.CollectedCouponComparator comparator = new CouponListPresenter.CollectedCouponComparator();

        Coupon c1 = mock(Coupon.class);
        doReturn(new NineyiDate("", "200", "")).when(c1).getUsingEndDateTime();

        Coupon c2 = mock(Coupon.class);
        doReturn(new NineyiDate("", "200", "")).when(c2).getUsingEndDateTime();

        assertEquals(0, comparator.compare(c1, c2));
    }

    @Test
    public void testNotTakenCouponComparator_lessThan() {
        CouponListPresenter.AvailableCouponComparator comparator = new CouponListPresenter.AvailableCouponComparator();

        Coupon c1 = mock(Coupon.class);
        doReturn(new NineyiDate("", "100", "")).when(c1).getEndDateTime();

        Coupon c2 = mock(Coupon.class);
        doReturn(new NineyiDate("", "200", "")).when(c2).getEndDateTime();

        assertEquals(-1, comparator.compare(c1, c2));
    }

    @Test
    public void testNotTakenCouponComparator_greaterThan() {
        CouponListPresenter.AvailableCouponComparator comparator = new CouponListPresenter.AvailableCouponComparator();

        Coupon c1 = mock(Coupon.class);
        doReturn(new NineyiDate("", "200", "")).when(c1).getEndDateTime();

        Coupon c2 = mock(Coupon.class);
        doReturn(new NineyiDate("", "100", "")).when(c2).getEndDateTime();

        assertEquals(1, comparator.compare(c1, c2));
    }

    @Test
    public void testNotTakenCouponComparator_equal() {
        CouponListPresenter.AvailableCouponComparator comparator = new CouponListPresenter.AvailableCouponComparator();

        Coupon c1 = mock(Coupon.class);
        doReturn(new NineyiDate("", "100", "")).when(c1).getEndDateTime();

        Coupon c2 = mock(Coupon.class);
        doReturn(new NineyiDate("", "100", "")).when(c2).getEndDateTime();

        assertEquals(0, comparator.compare(c1, c2));
    }

    @Test
    public void testConvertCouponToListItem_empty() {
        List<Coupon> coupons = new ArrayList<>();

        List<CouponListItem> items = mPresenter.convertCouponToListItem(coupons);

        assertTrue(items.isEmpty());
    }

    @Test
    public void testConvertCouponToListItem_oneInvalid() {
        List<Coupon> coupons = new ArrayList<>();

        Coupon invalid = mock(Coupon.class);

        coupons.add(invalid);

        CouponListPresenter presenter = spy(mPresenter);
        doReturn(CouponListPresenter.Group.INVALID).when(presenter).chooseGroupForCoupon(invalid);

        List<CouponListItem> items = presenter.convertCouponToListItem(coupons);

        // 無法領取 ---------------
        //
        // -----------------------
        // | 我是無法領取的折價券
        // |
        // -----------------------

        assertEquals(4, items.size());

        assertTrue(items.get(0) instanceof CouponListItemSeparator);
        assertEquals(mContext.getString(R.string.coupon_list_item_separator_title_available),
                ((CouponListItemSeparator) items.get(0)).getTitle());

        assertTrue(items.get(1) instanceof CouponListItemEmpty);

        assertTrue(items.get(2) instanceof CouponListItemSeparator);
        assertEquals(mContext.getString(R.string.coupon_list_item_separator_title_invalid),
                ((CouponListItemSeparator) items.get(2)).getTitle());

        assertTrue(items.get(3) instanceof CouponListItemCoupon);
        assertEquals(invalid, ((CouponListItemCoupon) items.get(3)).getCoupon());
        assertEquals(VIEW_TYPE_INVALID, items.get(3).getViewType());
    }

    @Test
    public void testConvertCouponToListItem_oneAvailable() {
        List<Coupon> coupons = new ArrayList<>();

        Coupon available = mock(Coupon.class);

        coupons.add(available);

        CouponListPresenter presenter = spy(mPresenter);
        doReturn(CouponListPresenter.Group.AVAILABLE).when(presenter).chooseGroupForCoupon(available);

        List<CouponListItem> items = presenter.convertCouponToListItem(coupons);

        // 可領取 ---------------
        //
        // -----------------------
        // | 我是尚未領取的折價券
        // |
        // -----------------------

        assertEquals(2, items.size());

        assertTrue(items.get(0) instanceof CouponListItemSeparator);
        assertEquals(mContext.getString(R.string.coupon_list_item_separator_title_available),
                ((CouponListItemSeparator) items.get(0)).getTitle());

        assertTrue(items.get(1) instanceof CouponListItemCoupon);
        assertEquals(available, ((CouponListItemCoupon) items.get(1)).getCoupon());
        assertEquals(VIEW_TYPE_AVAILABLE, items.get(1).getViewType());
    }

    @Test
    public void testConvertCouponToListItem_oneCollected() {
        List<Coupon> coupons = new ArrayList<>();

        Coupon collected = mock(Coupon.class);

        coupons.add(collected);

        CouponListPresenter presenter = spy(mPresenter);
        doReturn(CouponListPresenter.Group.COLLECTED).when(presenter).chooseGroupForCoupon(collected);

        List<CouponListItem> items = presenter.convertCouponToListItem(coupons);

        // 可領取 ---------------
        //
        // -----------------------
        // | 我是已領取的折價券
        // |
        // -----------------------

        assertEquals(2, items.size());

        assertTrue(items.get(0) instanceof CouponListItemSeparator);
        assertEquals(mContext.getString(R.string.coupon_list_item_separator_title_available),
                ((CouponListItemSeparator) items.get(0)).getTitle());

        assertTrue(items.get(1) instanceof CouponListItemCoupon);
        assertEquals(collected, ((CouponListItemCoupon) items.get(1)).getCoupon());
        assertEquals(VIEW_TYPE_COLLECTED, items.get(1).getViewType());
    }

    @Test
    public void testConvertCouponToListItem_oneAvailableOneCollectedOneInvalid() {

        List<Coupon> coupons = new ArrayList<>();

        CouponListPresenter presenter = spy(mPresenter);

        Coupon available = mock(Coupon.class);
        coupons.add(available);

        doReturn(CouponListPresenter.Group.AVAILABLE).when(presenter).chooseGroupForCoupon(available);

        Coupon collected = mock(Coupon.class);
        coupons.add(collected);

        doReturn(CouponListPresenter.Group.COLLECTED).when(presenter).chooseGroupForCoupon(collected);

        Coupon invalid = mock(Coupon.class);
        coupons.add(invalid);

        doReturn(CouponListPresenter.Group.INVALID).when(presenter).chooseGroupForCoupon(invalid);

        List<CouponListItem> items = presenter.convertCouponToListItem(coupons);

        // 可領取 -----------------
        //
        // -----------------------
        // | 我是尚未領取的折價券
        // |
        // -----------------------
        // -----------------------
        // | 我是已領取的折價券
        // |
        // -----------------------
        //
        // 無法領取 ---------------
        //
        // -----------------------
        // | 我是無法領取的折價券
        // |
        // -----------------------

        assertEquals(5, items.size());

        assertTrue(items.get(0) instanceof CouponListItemSeparator);
        assertEquals(mContext.getString(R.string.coupon_list_item_separator_title_available),
                ((CouponListItemSeparator) items.get(0)).getTitle());

        assertTrue(items.get(1) instanceof CouponListItemCoupon);
        assertEquals(available, ((CouponListItemCoupon) items.get(1)).getCoupon());
        assertEquals(VIEW_TYPE_AVAILABLE, items.get(1).getViewType());

        assertTrue(items.get(2) instanceof CouponListItemCoupon);
        assertEquals(collected, ((CouponListItemCoupon) items.get(2)).getCoupon());
        assertEquals(VIEW_TYPE_COLLECTED, items.get(2).getViewType());

        assertTrue(items.get(3) instanceof CouponListItemSeparator);
        assertEquals(mContext.getString(R.string.coupon_list_item_separator_title_invalid),
                ((CouponListItemSeparator) items.get(3)).getTitle());

        assertTrue(items.get(4) instanceof CouponListItemCoupon);
        assertEquals(invalid, ((CouponListItemCoupon) items.get(4)).getCoupon());
        assertEquals(VIEW_TYPE_INVALID, items.get(4).getViewType());
    }

    @Test
    public void testLoadCouponList_error() {
        doReturn(false).when(mPendingCouponHandler).hasPendingCouponToCollect();
        doReturn(Single.error(new GetCouponListException())).when(mockCouponManager).getCouponList();

        mPresenter.loadCouponList(false);

        verify(mockView).showError();
    }

    @Test
    public void testLoadCouponList_error_empty() {
        doReturn(false).when(mPendingCouponHandler).hasPendingCouponToCollect();
        doReturn(Single.error(new GetCouponListException(GetCouponListException.ErrorCode.EMPTY))).when(mockCouponManager).getCouponList();

        mPresenter.loadCouponList(false);

        verify(mockView).showEmpty();
    }

//    /**
//     * 如果user在別的地方login了，那麼onResume時要再重新load
//     */
//    @Test
//    public void testLoadCouponList_isNowLoggedIn() {
//        doReturn(false).when(mPendingCouponHandler).hasPendingCouponToCollect();
//        doReturn(Single.just(new ArrayList<Coupon>())).when(mockCouponManager).getCouponList();
//
//        mPresenter.loadCouponList(false);
//
//        verify(mockCouponManager).getCouponList();
//
//        doReturn(true).when(loginManager).isLogin();
//
//        mPresenter.loadCouponList(false);
//
//        verify(mockCouponManager, times(2)).getCouponList();
//    }

    @Test
    public void testCollectPendingCouponIfExists() {
        doReturn(true).when(mPendingCouponHandler).hasPendingCouponToCollect();
        doReturn(true).when(loginManager).isLogin();

        int couponId = 777;
        doReturn(couponId).when(mPendingCouponHandler).loadPendingCouponId();

        boolean isFirstDownload = false;
        doReturn(isFirstDownload).when(mPendingCouponHandler).loadPendingCouponIsFirstDownload();

        doReturn(Completable.complete()).when(mockCouponManager).takeCoupon(couponId, isFirstDownload);

        mPresenter.collectPendingCouponIfExists();

        verify(mockView).showCollectCouponSuccess();
    }
}


