package com.nineyi.module.coupon.ui.detail;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.MsgManager;
import com.nineyi.module.coupon.ui.view.CouponTicketAvailable;
import com.nineyi.module.coupon.ui.view.CouponTicketCollected;
import com.nineyi.module.coupon.ui.view.CouponTicketInvalid;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.nineyi.module.coupon.ui.detail.DetailContract.Action.COLLECT;
import static com.nineyi.module.coupon.ui.detail.DetailContract.Action.NONE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by shaocheng on 2017/9/15.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DetailViewTest {

    Context mContext;

    @Mock
    DetailContract.Presenter mPresenter;

    @Mock
    MsgManager mMsgManager;

    DetailView mView;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        mView = new DetailView(mContext);
        mView.setPresenter(mPresenter);
        mView.setMsgManager(mMsgManager);

        assertEquals(GONE, mView.mEmpty.getVisibility());
        assertEquals(GONE, mView.mContent.getVisibility());
        assertEquals(VISIBLE, mView.mProgressBar.getVisibility());
    }

    @Test
    public void testShowDetail_statusAvailable() {
        Coupon coupon = createMockCoupon();
        doReturn(Coupon.Status.AVAILABLE).when(coupon).getStatus();

        mView.showDetail(coupon);
        mView.showAction(COLLECT);

        assertTrue(mView.mContentInner.getChildAt(0) instanceof CouponTicketAvailable);

        assertDetailItemIsShown(mView, R.string.detail_item_title_take_period);
        assertDetailItemIsNotShown(mView, R.string.detail_item_title_use_period);

        assertDetailItemIsShown(mView, R.string.detail_item_title_limitation);

        assertDetailItemIsNotShown(mView, R.string.detail_item_title_code);

        assertDetailItemIsShown(mView, R.string.detail_item_title_name);
        assertDetailItemIsShown(mView, R.string.detail_item_title_description);
        assertDetailItemIsShown(mView, R.string.detail_item_title_notice);

        assertEquals(mContext.getString(R.string.detail_action_collect), mView.mActionButton.getText().toString());

        assertTrue(mView.mActionButton.isClickable());

        assertEquals(GONE, mView.mEmpty.getVisibility());
        assertEquals(GONE, mView.mProgressBar.getVisibility());
        assertEquals(VISIBLE, mView.mContent.getVisibility());

    }

    @Test
    public void testShowDetail_statusCollected() {
        Coupon coupon = createMockCoupon();
        doReturn(Coupon.Status.COLLECTED).when(coupon).getStatus();

        mView.showDetail(coupon);
        mView.showAction(DetailContract.Action.USE_OFFLINE);

        assertTrue(mView.mContentInner.getChildAt(0) instanceof CouponTicketCollected);

        assertDetailItemIsNotShown(mView, R.string.detail_item_title_take_period);
        assertDetailItemIsShown(mView, R.string.detail_item_title_use_period);

        assertDetailItemIsShown(mView, R.string.detail_item_title_limitation);

        assertDetailItemIsNotShown(mView, R.string.detail_item_title_code);

        assertDetailItemIsShown(mView, R.string.detail_item_title_name);
        assertDetailItemIsShown(mView, R.string.detail_item_title_description);
        assertDetailItemIsShown(mView, R.string.detail_item_title_notice);

        assertEquals(mContext.getString(R.string.detail_action_use_at_store), mView.mActionButton.getText().toString());

        assertTrue(mView.mActionButton.isClickable());

        assertEquals(GONE, mView.mEmpty.getVisibility());
        assertEquals(GONE, mView.mProgressBar.getVisibility());
        assertEquals(VISIBLE, mView.mContent.getVisibility());
    }

    @Test
    public void testShowDetail_statusNoMoreCoupons() {
        Coupon coupon = createMockCoupon();
        doReturn(Coupon.Status.NO_MORE_COUPONS).when(coupon).getStatus();

        mView.showDetail(coupon);
        mView.showAction(NONE, R.string.coupon_list_item_status_out_of_stock);

        assertTrue(mView.mContentInner.getChildAt(0) instanceof CouponTicketInvalid);

        assertDetailItemIsShown(mView, R.string.detail_item_title_take_period);
        assertDetailItemIsNotShown(mView, R.string.detail_item_title_use_period);

        assertDetailItemIsShown(mView, R.string.detail_item_title_limitation);

        assertDetailItemIsNotShown(mView, R.string.detail_item_title_code);

        assertDetailItemIsShown(mView, R.string.detail_item_title_name);
        assertDetailItemIsShown(mView, R.string.detail_item_title_description);
        assertDetailItemIsShown(mView, R.string.detail_item_title_notice);

        assertFalse(mView.mActionButton.isClickable());

        assertEquals(GONE, mView.mEmpty.getVisibility());
        assertEquals(GONE, mView.mProgressBar.getVisibility());
        assertEquals(VISIBLE, mView.mContent.getVisibility());
    }

    @Test
    public void testShowDetail_codeCoupon() {
        Coupon coupon = createMockCoupon();
        doReturn(true).when(coupon).isCode();

        mView.showDetail(coupon);
        mView.showAction(COLLECT);

        assertTrue(mView.mContentInner.getChildAt(0) instanceof CouponTicketAvailable);

        assertDetailItemIsShown(mView, R.string.detail_item_title_take_period);
        assertDetailItemIsNotShown(mView, R.string.detail_item_title_use_period);

        assertDetailItemIsShown(mView, R.string.detail_item_title_limitation);

        assertDetailItemIsShown(mView, R.string.detail_item_title_code);

        assertDetailItemIsShown(mView, R.string.detail_item_title_name);
        assertDetailItemIsShown(mView, R.string.detail_item_title_description);
        assertDetailItemIsShown(mView, R.string.detail_item_title_notice);

        assertEquals(mContext.getString(R.string.detail_action_collect), mView.mActionButton.getText().toString());

        assertTrue(mView.mActionButton.isClickable());

        checkChildOnlyVisible(mView.mContent);

    }

    private Coupon createMockCoupon() {
        Coupon coupon = mock(Coupon.class);
        doReturn(Coupon.Status.AVAILABLE).when(coupon).getStatus();
        doReturn("name").when(coupon).getName();
        doReturn("description").when(coupon).getDescription();
        doReturn(new NineyiDate("", "", "")).when(coupon).getEndDateTime();
        doReturn(new NineyiDate("", "", "")).when(coupon).getStartDateTime();
        doReturn(new NineyiDate("", "", "")).when(coupon).getUsingStartDateTime();
        doReturn(new NineyiDate("", "", "")).when(coupon).getUsingEndDateTime();
        return coupon;
    }

    private void assertDetailItemIsShown(DetailView mView, int id) {
        for (int i = 0; i < mView.mContentInner.getChildCount(); i++) {
            View view = mView.mContentInner.getChildAt(i);
            if (view instanceof DetailItemView) {
                DetailItemView itemView = (DetailItemView) view;
                if (mContext.getString(id).equals(itemView.mTitle.getText())) {
                    return;
                }
            }
        }
        fail();
    }

    private void assertDetailItemIsNotShown(DetailView mView, int id) {
        for (int i = 0; i < mView.mContentInner.getChildCount(); i++) {
            View view = mView.mContentInner.getChildAt(i);
            if (view instanceof DetailItemView) {
                DetailItemView itemView = (DetailItemView) view;
                if (mContext.getString(id).equals(itemView.mTitle.getText())) {
                    fail();
                }
            }
        }
    }

    @Test
    public void testShowNoActionReason() {

        mView.mActionButton = mock(TextView.class);

        int stringId = 7;
        mView.showAction(NONE, stringId);

        verify(mView.mActionButton).setText(stringId);
    }

    @Test
    public void testClickOnAction() {
        Coupon coupon = createMockCoupon();

        mView.showDetail(coupon);
        mView.showAction(COLLECT);

        mView.mActionButton.callOnClick();

        verify(mPresenter).doAction(COLLECT);
    }

    @Test
    public void testShowLimitation() {
        Coupon coupon = createMockCoupon();
        doReturn(10000.0).when(coupon).getECouponUsingMinPrice();
        doReturn(0.87).when(coupon).geteCouponMaxDiscountLimit();

        mView.showDetail(coupon);

        DetailItemView item = findDetailItem(R.string.detail_item_title_limitation);
        assertEquals(mContext.getString(R.string.detail_item_content_limitation, "$10,000", 87.0),
                item.mContent.getText().toString());

    }

    private DetailItemView findDetailItem(int id) {
        for (int i = 0; i < mView.mContentInner.getChildCount(); i++) {
            View view = mView.mContentInner.getChildAt(i);
            if (view instanceof DetailItemView) {
                DetailItemView itemView = (DetailItemView) view;
                if (mContext.getString(id).equals(itemView.mTitle.getText())) {
                    return itemView;
                }
            }
        }
        return null;
    }

    @Test
    public void testShowLoading() {
        mView.showLoading();

        checkChildOnlyVisible(mView.mProgressBar);
    }

    @Test
    public void testShowActionLoading() {
        mView.showActionLoading();

        assertFalse(mView.mActionButton.isClickable());
        assertTrue(mView.mActionButton.getText().toString().isEmpty());
        assertEquals(VISIBLE, mView.mActionButtonLoading.getVisibility());
    }

    @Test
    public void testShowCollectSuccess() {
        mView.showCollectSuccess();

        verify(mMsgManager).showShortToast(mContext, mContext.getString(R.string.coupon_detail_collect_success));
    }

    @Test
    public void testShowEmpty() {
        mView.showEmpty();

        checkChildOnlyVisible(mView.mEmpty);
    }

    @Test
    public void testShowError() {
        mView.showError();

        checkChildOnlyVisible(mView.mError);
    }

    private void checkChildOnlyVisible(View childView) {
        int count = mView.getChildCount();
        for (int i = 0; i < count; i++) {
            View c = mView.getChildAt(i);
            if (c == childView) {
                assertEquals(VISIBLE, c.getVisibility());
            } else {
                assertEquals(GONE, c.getVisibility());
            }
        }
    }

    @Test
    public void clickOnErrorRetry() {
        mView.mErrorActionButton.callOnClick();

        verify(mPresenter).loadCouponDetail(true);
    }
}


