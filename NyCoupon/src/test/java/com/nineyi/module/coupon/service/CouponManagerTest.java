package com.nineyi.module.coupon.service;

/**
 * Created by shaocheng on 2017/10/11.
 */

import android.content.Context;

import com.google.gson.Gson;
import com.nineyi.data.ReturnCodeType;
import com.nineyi.data.model.ecoupon.ECouponDetail;
import com.nineyi.data.model.ecoupon.ECouponIncludeDetail;
import com.nineyi.data.model.ecoupon.ECouponMemberECouponStatusList;
import com.nineyi.data.model.ecoupon.ECouponShopECoupon;
import com.nineyi.data.model.ecoupon.ECouponShopECouponList;
import com.nineyi.data.model.ecoupon.ECouponStatusList;
import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.model.Coupon;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Flowable;

import static com.nineyi.module.coupon.service.CouponService.BIRTHDAY;
import static com.nineyi.module.coupon.service.CouponService.CODE;
import static com.nineyi.module.coupon.service.CouponService.DRAW_OUT;
import static com.nineyi.module.coupon.service.CouponService.FIRST_DOWNLOAD;
import static com.nineyi.module.coupon.service.CouponService.OPEN_CARD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponManagerTest {

    @Mock
    CouponService mCouponService;

    @Mock
    CouponIdManager mIdManager;

    @Mock
    PromotionSharePreferenceHelper mPromotionSharePreferenceHelper;

    @Mock
    MigrationManager mMigrationManager;

    private int mShopId = 64;

    private Context mContext;

    private CouponManager mCouponManager;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        mCouponManager = new CouponManager(mContext, mShopId, mCouponService, mIdManager, mPromotionSharePreferenceHelper, mMigrationManager);
    }

    @Test
    public void testGetCouponList_normal() {
        ECouponDetail eCouponDetail = new ECouponDetail();
        eCouponDetail.Id = 111;
        eCouponDetail.TypeDef = DRAW_OUT;
        eCouponDetail.DiscountPrice = 1000;
        eCouponDetail.CouponTotalCount = 1;
        eCouponDetail.StartDateTime = new NineyiDate("", "", "");
        eCouponDetail.EndDateTime = new NineyiDate("", "", "");
        eCouponDetail.UsingStartDateTime = new NineyiDate("", "", "");
        eCouponDetail.UsingEndDateTime = new NineyiDate("", "", "");

        ECouponDetail eCouponDetail2 = new ECouponDetail();
        eCouponDetail2.Id = 222;
        eCouponDetail2.TypeDef = DRAW_OUT;
        eCouponDetail2.DiscountPrice = 2000;
        eCouponDetail2.CouponTotalCount = 0;
        eCouponDetail2.StartDateTime = new NineyiDate("", "", "");
        eCouponDetail2.EndDateTime = new NineyiDate("", "", "");
        eCouponDetail2.UsingStartDateTime = new NineyiDate("", "", "");
        eCouponDetail2.UsingEndDateTime = new NineyiDate("", "", "");


        ArrayList<ECouponDetail> eCouponList = new ArrayList<>();
        eCouponList.add(eCouponDetail);
        eCouponList.add(eCouponDetail2);

        ECouponShopECoupon eCouponShopECoupon = new ECouponShopECoupon();
        eCouponShopECoupon.ECouponList = eCouponList;

        ArrayList<ECouponShopECoupon> shopECouponList = new ArrayList<>();
        shopECouponList.add(eCouponShopECoupon);

        ECouponShopECouponList eCouponShopECouponList = new ECouponShopECouponList();
        eCouponShopECouponList.ReturnCode = "API0001";
        eCouponShopECouponList.ShopECouponList = shopECouponList;

        doReturn(Flowable.just(eCouponShopECouponList)).when(mCouponService).getECouponListWithoutCode(mShopId);

        ECouponMemberECouponStatusList eCouponMemberECouponStatusList = new ECouponMemberECouponStatusList();
        eCouponMemberECouponStatusList.ECouponId = 111;
        eCouponMemberECouponStatusList.HasNormalCoupon = true;

        ECouponMemberECouponStatusList eCouponMemberECouponStatusList2 = new ECouponMemberECouponStatusList();
        eCouponMemberECouponStatusList2.ECouponId = 222;
        eCouponMemberECouponStatusList2.HasNormalCoupon = false;

        ArrayList<ECouponMemberECouponStatusList> memberECouponStatusList = new ArrayList<>();
        memberECouponStatusList.add(eCouponMemberECouponStatusList);
        memberECouponStatusList.add(eCouponMemberECouponStatusList2);

        ECouponStatusList eCouponStatusList = new ECouponStatusList();
        eCouponStatusList.MemberECouponStatusList = memberECouponStatusList;

        doReturn(Flowable.just(eCouponStatusList)).when(mCouponService).getMemberECouponStatusList(anyString(), anyString());

        List<Coupon> actual = mCouponManager.getCouponList().blockingGet();

        assertEquals(2, actual.size());

        assertEquals(111, actual.get(0).getId());
        assertTrue(actual.get(0).isHasNormalCoupon());

        assertEquals(222, actual.get(1).getId());
        assertFalse(actual.get(1).isHasNormalCoupon());
    }

    /**
     * 未登入時沒有 status，但我們仍應該顯示所有 coupons
     */
    @Test
    public void testGetCouponList_withoutStatus() {
        ECouponShopECouponList eCouponShopECouponList = new ECouponShopECouponList();
        eCouponShopECouponList.ReturnCode = ReturnCodeType.API0001.toString();
        eCouponShopECouponList.ShopECouponList = new ArrayList<>();

        ECouponShopECoupon eCouponShopECoupon = new ECouponShopECoupon();
        eCouponShopECoupon.ShopId = mShopId;
        eCouponShopECoupon.ECouponList = new ArrayList<>();

        eCouponShopECouponList.ShopECouponList.add(eCouponShopECoupon);

        ECouponDetail eCouponDetail1 = new ECouponDetail();
        eCouponDetail1.TypeDef = DRAW_OUT;
        eCouponDetail1.StartDateTime = new NineyiDate("", "", "");
        eCouponDetail1.EndDateTime = new NineyiDate("", "", "");
        eCouponShopECoupon.ECouponList.add(eCouponDetail1);

        ECouponDetail eCouponDetail2 = new ECouponDetail();
        eCouponDetail2.TypeDef = DRAW_OUT;
        eCouponDetail2.StartDateTime = new NineyiDate("", "", "");
        eCouponDetail2.EndDateTime = new NineyiDate("", "", "");
        eCouponShopECoupon.ECouponList.add(eCouponDetail2);

        doReturn(Flowable.just(eCouponShopECouponList)).when(mCouponService).getECouponListWithoutCode(mShopId);

        // Status list is null
        ECouponStatusList eCouponStatusList = new ECouponStatusList();

        doReturn(Flowable.just(eCouponStatusList)).when(mCouponService).getMemberECouponStatusList(anyString(), anyString());

        List<Coupon> actual = mCouponManager.getCouponList().blockingGet();

        assertEquals(2, actual.size());
    }

    @Test
    public void testGetCouponList_error() {
        ECouponShopECouponList eCouponShopECouponList = new ECouponShopECouponList();
        eCouponShopECouponList.ReturnCode = ReturnCodeType.API0002.toString();

        doReturn(Flowable.just(eCouponShopECouponList)).when(mCouponService).getECouponListWithoutCode(mShopId);

        try {
            mCouponManager.getCouponList().blockingGet();
        } catch (Exception e) {
            assertTrue(e.getCause() instanceof GetCouponListException);
            return;
        }
        fail();
    }

    @Test
    public void testGetCoupon_couponIsNull() {
        ECouponIncludeDetail eCouponIncludeDetail = new ECouponIncludeDetail();
        eCouponIncludeDetail.ReturnCode = ReturnCodeType.API0001.toString();

        int id = 64;
        doReturn(Flowable.just(eCouponIncludeDetail)).when(mCouponService).getECouponDetail(id);

        try {
            mCouponManager.getCouponDetail(id).blockingGet();
        } catch (Exception e) {
            assertTrue(e.getCause() instanceof GetCouponDetailException);
            assertEquals(GetCouponDetailException.ErrorCode.UNKNOWN,
                    ((GetCouponDetailException) e.getCause()).errorCode);
            return;
        }
        fail();
    }

    @Test
    public void testGetCoupon_API0009() {
        ECouponIncludeDetail eCouponIncludeDetail = new ECouponIncludeDetail();
        eCouponIncludeDetail.ReturnCode = ReturnCodeType.API0009.toString();

        int id = 64;
        doReturn(Flowable.just(eCouponIncludeDetail)).when(mCouponService).getECouponDetail(id);

        try {
            mCouponManager.getCouponDetail(id).blockingGet();
        } catch (Exception e) {
            assertTrue(e.getCause() instanceof GetCouponDetailException);
            assertEquals(GetCouponDetailException.ErrorCode.EMPTY,
                    ((GetCouponDetailException) e.getCause()).errorCode);
            return;
        }
        fail();
    }

    @Test
    public void testGetCoupon_returnCodeIsNotAPI0001() {
        ECouponIncludeDetail eCouponIncludeDetail = new ECouponIncludeDetail();
        eCouponIncludeDetail.ReturnCode = "API7777";

        int id = 64;
        doReturn(Flowable.just(eCouponIncludeDetail)).when(mCouponService).getECouponDetail(id);

        try {
            mCouponManager.getCouponDetail(id).blockingGet();
        } catch (Exception e) {
            assertTrue(e.getCause() instanceof GetCouponDetailException);
            assertEquals(GetCouponDetailException.ErrorCode.UNKNOWN,
                    ((GetCouponDetailException) e.getCause()).errorCode);
            return;
        }
        fail();
    }

    @Test
    public void testGetCouponStatus_success() {
        int id = 64;

        ECouponStatusList eCouponStatusList = new ECouponStatusList();
        eCouponStatusList.MemberECouponStatusList = new ArrayList<>();

        ECouponMemberECouponStatusList expected = new ECouponMemberECouponStatusList();
        eCouponStatusList.MemberECouponStatusList.add(expected);

        doReturn(Flowable.just(eCouponStatusList)).when(mCouponService).getMemberECouponStatusList(
                String.valueOf(id), String.valueOf(0));

        ECouponMemberECouponStatusList actual = mCouponManager.getCouponStatus(id).blockingGet();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetCouponStatus_empty() {
        int id = 64;

        doReturn(Flowable.empty()).when(mCouponService).getMemberECouponStatusList(
                String.valueOf(id), String.valueOf(0));

        ECouponMemberECouponStatusList eCouponMemberECouponStatusList = mCouponManager.getCouponStatus(id).blockingGet();
        assertFalse(eCouponMemberECouponStatusList.HasNormalCoupon);
    }

    @Test
    public void testGetCouponStatus_returnDataCorrupted() {
        int id = 64;

        ECouponStatusList eCouponStatusList = new ECouponStatusList();

        // eCouponStatusList.MemberECouponStatusList is null

        doReturn(Flowable.just(eCouponStatusList)).when(mCouponService).getMemberECouponStatusList(
                String.valueOf(id), String.valueOf(0));

        ECouponMemberECouponStatusList eCouponMemberECouponStatusList = mCouponManager.getCouponStatus(id).blockingGet();
        assertFalse(eCouponMemberECouponStatusList.HasNormalCoupon);
    }

    @Test
    public void testDetermineStatus() {

        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("coupon_status_tests.json");

        CouponStatusTestCase[] testCases = new Gson().fromJson(new InputStreamReader(inputStream), CouponStatusTestCase[].class);

        for (CouponStatusTestCase testCase: testCases) {
            checkCouponStatusTestCase(testCase);
        }

    }

    private void checkCouponStatusTestCase(CouponStatusTestCase testCase) {
        Date now = new Date();

        ECouponDetail detail = new ECouponDetail();
        detail.TypeDef = testCase.typeDef;
        detail.StartDateTime = new NineyiDate("", Long.toString(now.getTime() + testCase.startDateTime), "");
        detail.EndDateTime = new NineyiDate("", Long.toString(now.getTime() + testCase.endDateTime), "");
        detail.UsingStartDateTime = new NineyiDate("", Long.toString(now.getTime() + testCase.usingStartDateTime), "");
        detail.UsingEndDateTime = new NineyiDate("", Long.toString(now.getTime() + testCase.usingEndDateTime), "");
        detail.CouponTotalCount = testCase.couponTotalCount;

        ECouponMemberECouponStatusList status = new ECouponMemberECouponStatusList();
        status.HasNormalCoupon = testCase.hasNormalCoupon;
        status.IsUsing = testCase.isUsing;

        doReturn(testCase.isFirstDownloadCollected).when(mPromotionSharePreferenceHelper).isECouponFirstDownloadPicked();
        doReturn(testCase.firstDownloadAppTime + now.getTime()).when(mMigrationManager).getAppRegisterDateMilliseconds();

        assertEquals(Coupon.Status.valueOf(testCase.status), mCouponManager.determineStatus(detail, status, now));
    }

    @Test
    public void testIsDrawOut() {
        assertTrue(CouponManager.isDrawOut(DRAW_OUT));
        assertFalse(CouponManager.isDrawOut(CODE));
        assertTrue(CouponManager.isDrawOut(OPEN_CARD));
        assertTrue(CouponManager.isDrawOut(BIRTHDAY));
        assertFalse(CouponManager.isDrawOut(FIRST_DOWNLOAD));
    }

    @Test
    public void testIsUnknown() {
        assertFalse(CouponManager.isUnKnown(DRAW_OUT));
        assertFalse(CouponManager.isUnKnown(CODE));
        assertFalse(CouponManager.isUnKnown(OPEN_CARD));
        assertFalse(CouponManager.isUnKnown(BIRTHDAY));
        assertFalse(CouponManager.isUnKnown(FIRST_DOWNLOAD));
        assertTrue(CouponManager.isUnKnown("unknown_type"));
    }

    @Test
    public void testIsShowPresent() {
        assertFalse(CouponManager.isShowPresent(DRAW_OUT));
        assertFalse(CouponManager.isShowPresent(CODE));
        assertTrue(CouponManager.isShowPresent(OPEN_CARD));
        assertTrue(CouponManager.isShowPresent(BIRTHDAY));
        assertFalse(CouponManager.isShowPresent(FIRST_DOWNLOAD));
    }

    @Test
    public void testIsCode() {
        assertFalse(CouponManager.isCode(DRAW_OUT));
        assertTrue(CouponManager.isCode(CODE));
        assertFalse(CouponManager.isCode(OPEN_CARD));
        assertFalse(CouponManager.isCode(BIRTHDAY));
        assertFalse(CouponManager.isCode(FIRST_DOWNLOAD));
    }

    @Test
    public void testIsFirstDownload() {
        assertFalse(CouponManager.isFirstDownload(DRAW_OUT));
        assertFalse(CouponManager.isFirstDownload(CODE));
        assertFalse(CouponManager.isFirstDownload(OPEN_CARD));
        assertFalse(CouponManager.isFirstDownload(BIRTHDAY));
        assertTrue(CouponManager.isFirstDownload(FIRST_DOWNLOAD));
    }

    @Test
    public void testIsTypeValid() {
        assertTrue(CouponManager.isTypeValid(DRAW_OUT));
        assertTrue(CouponManager.isTypeValid(CODE));
        assertTrue(CouponManager.isTypeValid(OPEN_CARD));
        assertTrue(CouponManager.isTypeValid(BIRTHDAY));
        assertTrue(CouponManager.isTypeValid(FIRST_DOWNLOAD));
        assertFalse(CouponManager.isTypeValid("unknown_type"));
    }
}

