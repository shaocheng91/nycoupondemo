package com.nineyi.module.coupon.ui.view;

import android.content.Context;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.CouponComponent;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.model.Coupon;
import com.nineyi.module.coupon.service.MigrationManager;
import com.nineyi.module.coupon.service.PromotionSharePreferenceHelper;
import com.nineyi.module.coupon.ui.list.CouponListContract;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static android.view.View.VISIBLE;
import static com.nineyi.module.coupon.model.Coupon.Status.FIRST_DOWNLOAD_NOT_QUALIFIED;
import static com.nineyi.module.coupon.model.Coupon.Status.NO_MORE_COUPONS;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * Created by shaocheng on 2017/9/15.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponTicketInvalidTest {

    @Mock
    CouponListContract.Presenter presenter;

    @Mock
    NineyiDate mockTime;

    @Mock
    CouponComponent mockCouponComponent;

    @Mock
    MigrationManager mockMigrationManager;

    @Mock
    PromotionSharePreferenceHelper mockPromotionSharePreferenceHelper;

    private Context mContext;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        CouponComponent.set(mockCouponComponent);
        doReturn(mockMigrationManager).when(mockCouponComponent).migrationmanager();
        doReturn(mockPromotionSharePreferenceHelper).when(mockCouponComponent).promotionSharePreferenceHelper();
    }

    @Test
    public void testShowCoupon_firstDownload() {
        CouponTicketInvalid view = new CouponTicketInvalid(mContext);

        Coupon coupon = mock(Coupon.class);
        doReturn(FIRST_DOWNLOAD_NOT_QUALIFIED).when(coupon).getStatus();
        doReturn(true).when(coupon).isFirstDownload();
        doReturn(mockTime).when(coupon).getEndDateTime();

        view.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title_first_download), view.mCouponTitle.getText().toString());
    }

    @Test
    public void testShowCoupon_normal() {
        CouponTicketInvalid view = new CouponTicketInvalid(mContext);

        Coupon coupon = mock(Coupon.class);
        doReturn(FIRST_DOWNLOAD_NOT_QUALIFIED).when(coupon).getStatus();
        doReturn(mockTime).when(coupon).getEndDateTime();

        view.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title), view.mCouponTitle.getText().toString());
    }

    @Test
    public void testShowCoupon_checkPrice() {
        CouponTicketInvalid view = new CouponTicketInvalid(mContext);

        Coupon coupon = mock(Coupon.class);
        doReturn(FIRST_DOWNLOAD_NOT_QUALIFIED).when(coupon).getStatus();
        doReturn(mockTime).when(coupon).getEndDateTime();
        doReturn(100.0).when(coupon).getDiscountPrice();

        view.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title), view.mCouponTitle.getText().toString());
        assertEquals("100", view.mPrice.getText().toString());
    }

    @Test
    public void testShowCoupon_showTakeEndTime() {
        CouponTicketInvalid view = new CouponTicketInvalid(mContext);

        Coupon coupon = mock(Coupon.class);
        doReturn(FIRST_DOWNLOAD_NOT_QUALIFIED).when(coupon).getStatus();
        doReturn(mockTime).when(coupon).getEndDateTime();

        view.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_title), view.mCouponTitle.getText().toString());

        String expected = mContext.getString(R.string.coupon_list_item_take_end_time, "1970/01/01");
        assertEquals(expected, view.mEndTime.getText().toString());

        assertEquals(VISIBLE, view.mEndTime.getVisibility());
    }

    @Test
    public void testShowCoupon_checkStatus_noMoreCoupons() {
        CouponTicketInvalid view = new CouponTicketInvalid(mContext);

        Coupon coupon = mock(Coupon.class);
        doReturn(NO_MORE_COUPONS).when(coupon).getStatus();
        doReturn(mockTime).when(coupon).getEndDateTime();

        view.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_status_out_of_stock), view.mStatus.getText().toString());

    }

    @Test
    public void testShowCoupon_checkStatus_notQualified() {
        doReturn(true).when(mockPromotionSharePreferenceHelper).isECouponFirstDownloadPicked();

        CouponTicketInvalid view = new CouponTicketInvalid(mContext);

        Coupon coupon = mock(Coupon.class);
        doReturn(FIRST_DOWNLOAD_NOT_QUALIFIED).when(coupon).getStatus();
        doReturn(mockTime).when(coupon).getEndDateTime();

        view.showCoupon(coupon);

        assertEquals(mContext.getString(R.string.coupon_list_item_status_invalidate), view.mStatus.getText().toString());

    }
}


