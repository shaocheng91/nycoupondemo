package com.nineyi.module.coupon.ui.use.online;

import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.model.CouponOnline;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItem;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCoupon;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponInformation;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponNotUse;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUnuseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemCouponUseable;
import com.nineyi.module.coupon.ui.use.online.wrapper.CouponOnlineUseItemSeparator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


/**
 * Created by ReeceCheng on 2017/10/23.
 */


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponOnlineUseAdapterTest {

    Context mContext;

    @Mock
    CouponOnlineUseAdapter mAdapter;

    @Mock
    CouponOnlineUseAdapter.LastSelected mLastSelected;

    @Mock
    CouponOnlineUseContract.Presenter mPresenter;

    CouponOnlineUseAdapterController mController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        mController = new CouponOnlineUseAdapterController();
    }

    private List prepareData() {
        List data = spy(new ArrayList<CouponOnlineUseItem>());

        CouponOnlineUseItemSeparator separator1 = new CouponOnlineUseItemSeparator("title");
        CouponOnlineUseItemCouponUseable itemCoupon1 = new CouponOnlineUseItemCouponUseable(mock(CouponOnline.class));
        CouponOnlineUseItemCouponUseable itemCoupon2 = new CouponOnlineUseItemCouponUseable(mock(CouponOnline.class));
        CouponOnlineUseItemCouponNotUse itemCoupon3 = new CouponOnlineUseItemCouponNotUse(mock(CouponOnline.class));

        CouponOnlineUseItemSeparator separator2 = new CouponOnlineUseItemSeparator("title");
        CouponOnlineUseItemCouponUnuseable itemCoupon4 = new CouponOnlineUseItemCouponUnuseable(mock(CouponOnline.class));

        itemCoupon1.setSelected(false);
        itemCoupon2.setSelected(false);
        itemCoupon3.setSelected(false);
        itemCoupon4.setSelected(false);

        data.add(separator1);
        data.add(itemCoupon1);
        data.add(itemCoupon2);
        data.add(itemCoupon3);
        data.add(separator2);
        data.add(itemCoupon4);

        return data;
    }

    /**
     * 執行 unselectLastItem 後 item 的 selected 必須為 false
     * 並且 notify item change
     */
    @Test
    public void testToggleInfoView_unselectLastItem() {
        CouponOnline mockCoupon = mock(CouponOnline.class);

        CouponOnlineUseItemCoupon itemCoupon = new CouponOnlineUseItemCoupon(mockCoupon);
        itemCoupon.setSelected(true);

        doReturn(true).when(mLastSelected).hasItem();
        doReturn(true).when(mLastSelected).hasInfoView();
        doReturn(1).when(mLastSelected).getPosition();
        doReturn(2).when(mLastSelected).getInfoViewPosition();
        doReturn(itemCoupon).when(mLastSelected).getItem();

        mController.unselectLastItem(mAdapter, mLastSelected);

        verify(mAdapter, times(1)).notifyItemChanged(1);
        assertEquals(false, ((CouponOnlineUseItemCoupon) mLastSelected.getItem()).isSelected());
    }

    /**
     * 值行 selectNewItem 後 item 的 selected 必須為 true
     * 並且 notify item change
     */
    @Test
    public void testToggleInfoView_selectNewItem() {
        List data = mock(List.class);

        CouponOnline mockCoupon = mock(CouponOnline.class);
        CouponOnlineUseItemCoupon itemCoupon = new CouponOnlineUseItemCoupon(mockCoupon);
        itemCoupon.setSelected(false);

        doReturn(true).when(mLastSelected).hasItem();
        doReturn(true).when(mLastSelected).hasInfoView();
        doReturn(1).when(mLastSelected).getPosition();
        doReturn(2).when(mLastSelected).getInfoViewPosition();
        doReturn(itemCoupon).when(data).get(3);

        CouponOnlineUseItem item = mController.selectNewItem(mAdapter, data, 3);

        verify(mAdapter, times(1)).notifyItemChanged(3);
        assertEquals(true, ((CouponOnlineUseItemCoupon) item).isSelected());
    }

    /**
     * 必須有呼叫 remove, notifyItemRemoved 與 notifyItemRangeRemoved
     */
    @Test
    public void testToggleInfoView_removeLastInfoView() {
        List data = mock(List.class);

        doReturn(true).when(mLastSelected).hasItem();
        doReturn(true).when(mLastSelected).hasInfoView();
        doReturn(1).when(mLastSelected).getPosition();
        doReturn(2).when(mLastSelected).getInfoViewPosition();
        doReturn(data.size()).when(mAdapter).getItemCount();

        mController.removeLastInfoView(mAdapter, data, mLastSelected);

        verify(data, times(1)).remove(2);
        verify(mAdapter, times(1)).notifyItemRemoved(2);
        verify(mAdapter, times(1)).notifyItemRangeRemoved(mLastSelected.getPosition(), mAdapter.getItemCount());
    }

    /**
     * selectedPosition 為 3
     * mock newItem 為 position 2
     * reset 後的 position 應該改成 newItem 的 position (2)
     */
    @Test
    public void testToggleInfoView_resetPosition() {
        List data = mock(List.class);

        CouponOnlineUseItemCoupon newItem = new CouponOnlineUseItemCoupon(mock(CouponOnline.class));
        newItem.setSelected(true);

        doReturn(true).when(mLastSelected).hasItem();
        doReturn(true).when(mLastSelected).hasInfoView();
        doReturn(1).when(mLastSelected).getPosition();
        doReturn(2).when(mLastSelected).getInfoViewPosition();
        doReturn(10).when(data).size();
        doReturn(newItem).when(data).get(2);
        doReturn(data.size()).when(mAdapter).getItemCount();

        int position = mController.resetPosition(data, mLastSelected, 3, newItem);

        assertEquals(2, position);
    }

    /**
     *  insert 後的 position 必須為 3
     *  必須有呼叫到 notifyItemInserted 與 notifyItemRangeChanged
     */
    @Test
    public void testToggleInfoView_insertNewInfoView() {
        List data = prepareData();

        CouponOnline coupon = mock(CouponOnline.class);

        doReturn(true).when(mLastSelected).hasItem();
        doReturn(true).when(mLastSelected).hasInfoView();
        doReturn(1).when(mLastSelected).getPosition();
        doReturn(2).when(mLastSelected).getInfoViewPosition();
        doReturn(data.size()).when(mAdapter).getItemCount();

        int infoPosition = mController.insertNewInfoView(mAdapter, data, coupon, 2);

        assertTrue(data.get(3) instanceof CouponOnlineUseItemCouponInformation);
        verify(mAdapter, times(1)).notifyItemInserted(3);
        verify(mAdapter, times(1)).notifyItemRangeChanged(2, mAdapter.getItemCount());
        assertEquals(3, infoPosition);
    }

    /**
     * 測試 coupon 為 null 的狀況
     *
     * 不能呼叫到 data.add()、notifyItemInserted 與 notifyItemRangeChanged
     * 且 return 的 position （infoPosition) 必須為 2
     */
    @Test
    public void testToggleInfoView_insertNewInfoViewNoCoupon() {
        List data = prepareData();
        CouponOnline coupon = null;

        doReturn(true).when(mLastSelected).hasItem();
        doReturn(true).when(mLastSelected).hasInfoView();
        doReturn(1).when(mLastSelected).getPosition();
        doReturn(2).when(mLastSelected).getInfoViewPosition();
        doReturn(data.size()).when(mAdapter).getItemCount();

        int infoPosition = mController.insertNewInfoView(mAdapter, data, coupon, 2);

        verify(data, times(0)).add(Mockito.anyInt(), Mockito.any());
        verify(mAdapter, times(0)).notifyItemInserted(3);
        verify(mAdapter, times(0)).notifyItemRangeChanged(2, mAdapter.getItemCount());
        assertEquals(2, infoPosition);
    }

    @Test
    public void testToggleInfoView_saveLastSelected() {
        CouponOnline coupon = mock(CouponOnline.class);
        CouponOnlineUseItemCoupon itemCoupon = new CouponOnlineUseItemCoupon(coupon);

        mController.saveLastSelected(coupon, itemCoupon, mLastSelected, 2, 3);

        verify(mLastSelected, times(1)).setItem(itemCoupon);
        verify(mLastSelected, times(1)).setPosition(2);
        verify(mLastSelected, times(1)).setInfoViewPosition(3);
        verify(mLastSelected, times(1)).setHasInfoView(true);
    }

    @Test
    public void testToggleInfoView_saveLastSelectedNoCoupon() {
        CouponOnline coupon = null;
        CouponOnlineUseItemCoupon itemCoupon = new CouponOnlineUseItemCoupon(coupon);

        mController.saveLastSelected(coupon, itemCoupon, mLastSelected, 2, 3);

        verify(mLastSelected, times(1)).setItem(itemCoupon);
        verify(mLastSelected, times(1)).setPosition(2);
        verify(mLastSelected, times(1)).setInfoViewPosition(CouponOnlineUseAdapter.LastSelected.NO_POSITION);
        verify(mLastSelected, times(1)).setHasInfoView(false);
    }

    /**
     * 當 data 中的 couponId 與 presenter.getSelectedCouponId 相等時
     * 該 position 必須為 selectDefault 的 position
     */
    @Test
    public void testFindDefaultSelected_selectedCoupon() {
        List<CouponOnlineUseItem> data = spy(new ArrayList<CouponOnlineUseItem>());

        CouponOnline coupon = mock(CouponOnline.class);
        CouponOnlineUseItemCouponUseable itemCoupon2 = new CouponOnlineUseItemCouponUseable(coupon);

        doReturn(12345).when(coupon).getId();
        doReturn(12345).when(mPresenter).getSelectedCouponId();

        CouponOnlineUseItemCouponUseable itemCoupon1 = new CouponOnlineUseItemCouponUseable(mock(CouponOnline.class));
        CouponOnlineUseItemCouponUseable itemCoupon3 = new CouponOnlineUseItemCouponUseable(mock(CouponOnline.class));

        data.add(itemCoupon1);
        data.add(itemCoupon2);
        data.add(itemCoupon3);

        mController.findDefaultSelected(mAdapter, mPresenter, data);

        verify(mAdapter, times(1)).selectDefault(itemCoupon2, 1);
    }

    /**
     * 當 data 中的 presenter.getSelectedCouponId 為 NotUse (presenter.getNotUseCouponId) 時
     * 該 position 必須為 notUse 的 position
     */
    @Test
    public void testFindDefaultSelected_selectedNotUse() {
        List<CouponOnlineUseItem> data = spy(new ArrayList<CouponOnlineUseItem>());

        CouponOnlineUseItemCouponNotUse notUse = new CouponOnlineUseItemCouponNotUse(null);

        doReturn(mPresenter.getNotUseCouponId()).when(mPresenter).getSelectedCouponId();

        CouponOnline coupon = mock(CouponOnline.class);
        doReturn(12345).when(coupon).getId();

        CouponOnlineUseItemCouponUseable itemCoupon1 = new CouponOnlineUseItemCouponUseable(coupon);
        CouponOnlineUseItemCouponUseable itemCoupon3 = new CouponOnlineUseItemCouponUseable(coupon);

        data.add(itemCoupon1);
        data.add(itemCoupon3);
        data.add(notUse);

        mController.findDefaultSelected(mAdapter, mPresenter, data);

        verify(mAdapter, times(1)).selectDefault(notUse, 2);
    }

}
