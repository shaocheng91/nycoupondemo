package com.nineyi.module.coupon.ui.keyin;

import android.content.Context;
import android.widget.LinearLayout;

import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.R;
import com.nineyi.module.coupon.service.MsgManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by shaocheng on 2017/9/15.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class KeyInViewTest {

    Context mContext;

    @Mock
    KeyInContract.Presenter mPresenter;

    @Mock
    MsgManager mMsgManager;

    KeyInView view;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        view = new KeyInView(mContext);
        view.setPresenter(mPresenter);
        view.mMsgManager = mMsgManager;
    }

    @Test
    public void testInitView() {
        assertEquals(LinearLayout.VERTICAL, view.getOrientation());
    }

    @Test
    public void testEditTextShouldBeUpperCased() {
        view.mKeyInInput.setText("aaa");

        assertEquals("AAA", view.mKeyInInput.getText().toString());
    }

    @Test
    public void testHintIsSetCorrectly() {
        // TODO How do we test color?
        String expected = mContext.getString(R.string.ecoupon_keyin_hint_first)
                + mContext.getString(R.string.ecoupon_keyin_hint_second)
                + mContext.getResources().getString(R.string.ecoupon_keyin_hint_third);
        assertEquals(expected, view.mKeyInInput.getHint().toString());
    }

    @Test
    public void testClickOnTake_emptyInput() {
        view.mKeyInButton.callOnClick();

        verify(mMsgManager, times(1)).showShortToast(mContext, mContext.getResources().getString(R.string.ecoupon_keyin_empty_alert));
        verify(mPresenter, times(0)).takeCoupon(Matchers.<String>any());
    }

    @Test
    public void testClickOnTake_normalInput() {
        String couponCode = "AAAA";

        view.mKeyInInput.setText(couponCode);

        view.mKeyInButton.callOnClick();

        verify(mPresenter, times(1)).takeCoupon(couponCode);
    }

    @Test
    public void testShowErrorWithMsg() {
        String message = "msg";

        view.showTakeCouponError(message);

        verify(mMsgManager, times(1)).showShortToast(mContext, message);
    }

    @Test
    public void testShowTakingCoupon() {
        view.showTakingCoupon();
        verify(mMsgManager, times(1)).showShortToast(mContext, mContext.getResources().getString(R.string.ecoupon_keyin_loading));
    }

    @Test
    public void testShowNetworkError() {
        view.showNetworkError();

        verify(mMsgManager, times(1)).showShortToast(mContext, mContext.getString(R.string.ecoupon_get_fail_title));
        assertEquals("", view.mKeyInInput.getText().toString());
    }

    @Test
    public void testShowTakeSuccess() {
        view.showTakeCouponSuccess();

        verify(mMsgManager, times(1)).showShortToast(mContext, mContext.getString(R.string.ecoupon_get_title));
        assertEquals("", view.mKeyInInput.getText().toString());
    }
}

