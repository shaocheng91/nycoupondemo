package com.nineyi.module.coupon.ui.use.online;

import android.content.Context;

import com.nineyi.module.coupon.BuildConfig;
import com.nineyi.module.coupon.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * Created by shaocheng on 2017/10/25.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CouponOnlineUseViewTest {

    @Mock
    CouponOnlineUseContract.Presenter mPresenter;

    private Context mContext;

    private CouponOnlineUseView mView;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mContext = RuntimeEnvironment.application;

        mView = new CouponOnlineUseView(mContext);
        mView.setPresenter(mPresenter);
    }

    @Test
    public void testShowCouponList_empty() {
        List items = mock(List.class);
        doReturn(true).when(items).isEmpty();

        mView.showCouponList(items);

        assertEquals(GONE, mView.mRecyclerView.getVisibility());
        assertEquals(VISIBLE, mView.mEmptyView.getVisibility());
    }

    @Test
    public void testShowCouponList_notEmpty() {
        List items = mock(List.class);
        doReturn(false).when(items).isEmpty();

        mView.showCouponList(items);

        assertEquals(VISIBLE, mView.mRecyclerView.getVisibility());
        assertEquals(GONE, mView.mEmptyView.getVisibility());
    }
}
