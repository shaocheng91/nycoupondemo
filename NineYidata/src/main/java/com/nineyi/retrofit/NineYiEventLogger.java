package com.nineyi.retrofit;

import android.util.Log;


/**
 * Created by tedliang on 2017/6/1.
 */

public class NineYiEventLogger {
    private static volatile NineYiEventLogger sInstance;

    private boolean isAllowRecord() {
        return mIsAllowRecord;
    }

    public void setIsAllowRecord(boolean isAllowRecord) {
        mIsAllowRecord = isAllowRecord;
    }

    private boolean mIsAllowRecord = false;
    public static NineYiEventLogger getInstance() {
        if (sInstance == null) {
            synchronized (NineYiEventLogger.class) {
                if (sInstance == null) {
                    sInstance = new NineYiEventLogger();
                }
            }
        }

        return sInstance;
    }

    private StringBuilder mStringBuilder = new StringBuilder();
    private NineYiEventLogger(){}

    public void writeLog(String string){
        if(isAllowRecord()) {
            mStringBuilder.append(string + "\n");
        }
    }

    public void clearLog(){
        try {
            mStringBuilder.delete(0, mStringBuilder.length());
        } catch (Exception e) {
            Log.d("NineYiEventLogger", "clearLog failed");
        }
    }

    public String readLog() {
        return mStringBuilder.toString();
    }

}
