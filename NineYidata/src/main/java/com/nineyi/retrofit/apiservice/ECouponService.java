package com.nineyi.retrofit.apiservice;

import com.nineyi.data.model.apiresponse.ReturnCode;
import com.nineyi.data.model.ecoupon.ECouponVerify;
import com.nineyi.data.model.ecoupon.ECouponIncludeDetail;
import com.nineyi.data.model.ecoupon.ECouponMemberList;
import com.nineyi.data.model.ecoupon.ECouponShopECouponList;
import com.nineyi.data.model.ecoupon.ECouponStatusList;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by toby on 2015/11/9.
 */
public interface ECouponService {

    @POST("/WebApi/ECoupon/GetMemberECouponListByUsingDateTime")
    Flowable<String> getMemberECouponListByUsingDateTime(@Query("ShopId") int shopId);

    @GET("/WebApi/ECoupon/GetMemberECouponList/")
    Flowable<ECouponMemberList> getMemberECouponList(@Query("ShopId") int shopId);

    @GET("/WebApi/ECoupon/GetECouponDetail/")
    Flowable<ECouponIncludeDetail> getECouponDetail(@Query("id") int ecouponId);

    @GET("/Webapi/ECoupon/GetMemberECouponStatusList/")
    Flowable<ECouponStatusList> getMemberECouponStatusList(@Query("eCouponIdListString") String eCouponIdListString, @Query("FBId") String fbId);

    @POST("/WebApi/ECoupon/SetMemberECouponByECouponId/")
    Flowable<ReturnCode> setMemberECouponByECouponId(@Query("ECouponId") int ecouponId, @Query("GUID") String guid);

    @POST("/WebApi/ECoupon/SetMemberFirstDownloadECouponByECouponId/")
    Flowable<ReturnCode> setMemberFirstDownloadECouponByECouponId(@Query("eCouponId") int ecouponId, @Query("GUID") String guid);

    @POST("/WebApi/ECoupon/SetMemberECouponByCode")
    Flowable<ReturnCode> setMemberECouponByCode(@Query("ShopId") int shopId, @Query("Code") String code, @Query("GUID") String guid);

    @GET("/Webapi/ECoupon/GetECouponListWithoutCode")
    Flowable<ECouponShopECouponList> getECouponListWithoutCode(@Query("ShopId") int shopId);

    @POST("WebAPI/ECoupon/Verify")
    Flowable<ECouponVerify> setECouponVerify(@Query("ShopId") int shopId, @Query("ECouponId") int ecouponId);
}
