package com.nineyi.retrofit.apiservice;


import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;


/**
 * Created by tedliang on 2017/6/6.
 */

public interface TrackService {
    @GET("/collect")
    Flowable<String> collect(@QueryMap Map<String, String> options);
}
