package com.nineyi.retrofit.apiservice;

import com.nineyi.data.model.appmain.Announcement;
import com.nineyi.data.model.category.SalePageListReturnCode;
import com.nineyi.data.model.category.ShopCategoryList;
import com.nineyi.data.model.infomodule.InfoModuleItemOfficial;
import com.nineyi.data.model.layout.LayoutTemplateCodeAndData;
import com.nineyi.data.model.layout.LayoutTemplateData;
import com.nineyi.data.model.layout.RecommendSalePageListReturnCode;
import com.nineyi.data.model.promotion.PromotionListReturnCode;
import com.nineyi.data.model.promotion.discount.PromotionDiscount;
import com.nineyi.data.model.promotion.v2.PromoteSalePage;
import com.nineyi.data.model.promotion.v2.PromotionV2Detail;
import com.nineyi.data.model.salepage.SalePage;
import com.nineyi.data.model.salepage.SalePageHotList;
import com.nineyi.data.model.salepage.SalePageMoreInfo;
import com.nineyi.data.model.salepagev2info.SalePageAdditionalInfo;
import com.nineyi.data.model.salepagev2info.SalePageV2Info;
import com.nineyi.data.model.shopapp.ShopContractSetting;
import com.nineyi.data.model.shopinfo.ShopIntroduction;
import com.nineyi.data.model.shoppingcart.buyextra.BuyExtra;

import java.util.ArrayList;
import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


/**
 * Created by toby on 2015/11/9.
 */
public interface CdnService {
    @GET("/webapi/LayoutTemplateData/GetShopHomeAllLoayoutTemplateData/{shopId}")
    Flowable<ArrayList<LayoutTemplateCodeAndData>> getShopAllHomeLayoutTemplate(@Path("shopId") int shopId);

    @GET("/webapi/Official/GetInfoModuleForApp/{shopId}")
    Flowable<ArrayList<InfoModuleItemOfficial>> getInfoModuleOfficial(@Path("shopId") int shopId, @QueryMap Map<String, String> options);

    @GET("/webapi/SalePage/GetOfficialRecommendSalePageList/{shopId}")
    Flowable<RecommendSalePageListReturnCode> getRecommendSalePageList(@Path("shopId") int shopId, @QueryMap Map<String, String> options);

    @GET("/webapi/Shop/GetShopCategoryListV2/{shopId}")
    Flowable<ShopCategoryList> getShopCategoryList(@Path("shopId") int shopId);

    // 商品詳細資訊
    @GET("/webapi/SalePage/GetSalePage/{id}")
    Flowable<SalePage> getSalePage(@Path("id") int salePageId);


    @GET("/webapi/LayoutTemplateData/GetLayoutTemplateData/{shopId}/{adCode}")
    Flowable<ArrayList<LayoutTemplateData>> getLayoutTemplateData(@Path("shopId") int shopId, @Path("adCode") String adCode);

    @GET("/webapi/ShopCategory/GetSalePageList/{shopId}/{shopCategoryId}")
    Flowable<SalePageListReturnCode> getSalePageListByShopCategory(@Path("shopId") int shopId, @Path("shopCategoryId") int categoryId,
                                                                     @Query("maxCount")int maxCount, @Query("order") String orderby,
                                                                     @Query("startIndex") int startIndex, @Query("isCuratorable") boolean isCuratorable);

    @GET("/webapi/Shop/GetShopintroduction/{shopId}")
    Flowable<ShopIntroduction> getShopIntroduction(@Path("shopId") int shopId);

    //取得商店分類的類別活動清單
    @GET("/webapi/ShopCategory/GetPromotionList/{shopId}/{shopCategoryId}")
    Flowable<PromotionListReturnCode> getPromotionList(@Path("shopId") int shopId, @Path("shopCategoryId") int categoryId);


    // 取得商品頁更多資訊
    @GET("/webapi/SalePage/GetSalePageMoreInfo/{productId}")
    Flowable<SalePageMoreInfo> getSalePageMoreInfo(@Path("productId") int productId);
//
//    // 商城商品頁 HotList (最下方相關商品)
//    @GET("/webapi/salePage/GetSalePageHotListByCategoryId/{id}")
//    Flowable<SalePageHotList> getSalePageHotListByCategoryId(@Path("id") int categoryId,
//                                                               @Query("sid") int salePageId, @Query("o") String orderBy);
//
//    // 商店商品頁 HotList (最下方相關商品)
    @GET("/webapi/SalePage/GetSalePageHotListByShopCategoryId/{categoryId}")
    Flowable<SalePageHotList> getSalePageHotListByShopCategoryId(@Path("categoryId") int categoryId,
                                                                   @Query("o") String orderBy, @Query("sid") int salePageId);
//
//    // 商店資訊 (ex: LINE ID)
//    @GET("/webapi/Shop/GetShopintroduction/")
//    Call<ShopIntroduction> getShopIntroduction(@Query("shopid") int shopId);


    @GET("/WebAPI/PromotionV2/GetDetail/{promotionId}")
    Flowable<PromotionV2Detail> getPromotionDetailV2(@Path("promotionId") int promotionId);

    @GET("/WebAPI/PromotionV2/GetSalePageList/{shopId}/{promoteId}")
    Flowable<PromoteSalePage> getPromoteSalePageList(@Path("shopId") int shopId, @Path("promoteId") int promoteId,
            @Query("categoryId") int categoryId, @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @GET("/webapi/AppAnnouncement/GetAppAnnouncementList/{shopId}/{platform}")
    Flowable<Announcement> getAppAnnouncementList(@Path("shopId") int shopId, @Path("platform") String platform,
            @Query("appVersion") String appVer, @Query("osVersion") String osVer);

    @GET("WebAPI/PromotionV2/GetList/{shopId}")
    Flowable<PromotionDiscount> getShopPromotionDiscountList(@Path("shopId") int shopId, @Query("orderBy") String orderBy,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount, @Query("typeDef") String typeDef);

    @GET("/WebAPI/Shop/GetShopContractSetting/{shopId}")
    Flowable<ShopContractSetting> getShopContractSetting(@Path("shopId") int shopId);

    @GET("/webapi/SalePageV2/GetSalePageV2Info/{shopId}/{salePageId}")
    Flowable<SalePageV2Info> getSalePageV2Info(@Path("shopId") int shopId, @Path("salePageId") int salePageId);

    @GET("/webapi/SalePageV2/GetSalePageAdditionalInfo/{shopId}/{salePageId}?source=androidapp")
    Flowable<SalePageAdditionalInfo> getSalePageAdditionalInfo(@Path("shopId") int shopId, @Path("salePageId") int salePageId);

    @GET("/webapi/salepagev2/GetBuyExtraItemList/{shopId}")
    Flowable<BuyExtra> getBuyExtra(@Path("shopId") int shopId);
}

