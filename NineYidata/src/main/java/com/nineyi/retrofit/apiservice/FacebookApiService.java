package com.nineyi.retrofit.apiservice;

import com.nineyi.data.model.fanpage.FBContainer;
import com.nineyi.data.model.fanpage.FBLogin;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by tedliang on 2016/9/14.
 */
public interface FacebookApiService {
    @GET("oauth/access_token")
    Flowable<FBLogin> getFBToken(@Query("client_id") String client_id, @Query("client_secret") String client_secret, @Query("grant_type") String grant_type);

    @GET("{fbId}/posts?fields=from,message,picture,link,source,name,description,icon,type,status_type,object_id,created_time,child_attachments")
    Flowable<FBContainer> getFBData(@Path("fbId") String fbId, @Query("access_token") String accessToken);

    @GET("/{photoId}/attachments")
    Flowable<String> getFBPhotos(@Path("photoId") String id, @Query("access_token") String accessToken);
}
