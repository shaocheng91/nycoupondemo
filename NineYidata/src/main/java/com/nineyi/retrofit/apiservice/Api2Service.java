package com.nineyi.retrofit.apiservice;

import com.nineyi.data.model.php.PhpCouponList;
import com.nineyi.data.model.php.PhpCouponTakeStatus;
import com.nineyi.data.model.php.PhpCouponUseStatus;
import com.nineyi.data.model.php.PhpInfomoduleList;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by toby on 2015/11/9.
 */
public interface Api2Service {

    // Info module

    @GET("/o2o/api/article/list/{id}")
    Flowable<PhpInfomoduleList> articleList(@Path("id") int listId, @Query("infoModuleStartIndex") int startIndex,
                                              @Query("infoModuleMaxCount") int maxCount);


    // O2O
    @GET("/o2o/api/coupon/list/{shopId}")
    Flowable<PhpCouponList> couponList(@Path("shopId") int shopId);

    @GET("/o2o/api/coupon/detail/{couponId}")
    Flowable<PhpCouponList> couponDetail(@Path("couponId") int couponId);

    @GET("/o2o/api/v2/coupon/take/{couponId}")
    Flowable<PhpCouponTakeStatus> takeCoupon(@Path("couponId") int couponId);

    @POST("/o2o/api/coupon/use/{couponId}")
    Flowable<PhpCouponUseStatus> useCoupon(@Path("couponId") int couponId, @Query("lat") Double lat, @Query("lon") Double lon);

    @POST("/o2o/api/coupon/serialnumber/{couponId}")
    Flowable<PhpCouponUseStatus> getCouponSerialNumber(@Path("couponId") int couponId);

    @GET("/o2o/api/coupon/my/{shopId}")
    Flowable<PhpCouponList> getMyCoupon(@Path("shopId") int shopId);

}
