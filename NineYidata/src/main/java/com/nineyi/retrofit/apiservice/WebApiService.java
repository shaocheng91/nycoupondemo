package com.nineyi.retrofit.apiservice;

import com.nineyi.data.model.NotifyMessage;
import com.nineyi.data.model.activity.ActivityDetail;
import com.nineyi.data.model.activity.ActivityList;
import com.nineyi.data.model.apiresponse.ReturnCode;
import com.nineyi.data.model.infomodule.InfoModuleRecommandation;
import com.nineyi.data.model.infomodule.albumdetailv2.InfoModuleAlbumDetail;
import com.nineyi.data.model.infomodule.albumlist.AlbumModule;
import com.nineyi.data.model.infomodule.articledetailv2.InfoModuleArticleDetail;
import com.nineyi.data.model.infomodule.articlelist.ArticleModule;
import com.nineyi.data.model.infomodule.videodetailv2.InfoModuleVideoDetail;
import com.nineyi.data.model.infomodule.videolist.VideoModule;
import com.nineyi.data.model.locationwizard.LocationWizardBeaconInfoRoot;
import com.nineyi.data.model.locationwizard.LocationWizardMemberInfoRoot;
import com.nineyi.data.model.login.VipMemberBenefitsModel;
import com.nineyi.data.model.memberzone.CrmMemberTierData;
import com.nineyi.data.model.memberzone.CrmShopMemberCard;
import com.nineyi.data.model.memberzone.PresentStatus;
import com.nineyi.data.model.memberzone.ShippingStatus;
import com.nineyi.data.model.memberzone.ThirdPartyTradeOrderRoot;
import com.nineyi.data.model.memberzone.VipMemberDataRoot;
import com.nineyi.data.model.memberzone.VipMemberItemData;
import com.nineyi.data.model.memberzone.VipMemberPostData;
import com.nineyi.data.model.memberzone.VipShopInfo;
import com.nineyi.data.model.newlbs.DONETLbsList;
import com.nineyi.data.model.newo2o.CityAreaListRoot;
import com.nineyi.data.model.newo2o.LocationListByCityRoot;
import com.nineyi.data.model.newo2o.LocationListRoot;
import com.nineyi.data.model.notify.EmailNotification;
import com.nineyi.data.model.notify.EmailNotificationData;
import com.nineyi.data.model.notify.NotifyProfileInputData;
import com.nineyi.data.model.notify.NotifyProfileReturnCode;
import com.nineyi.data.model.notify.UpdateEmailNotification;
import com.nineyi.data.model.openapp.AppNotificationData;
import com.nineyi.data.model.openapp.AppProfile;
import com.nineyi.data.model.promotion.FreeGiftSalePage;
import com.nineyi.data.model.promotion.PromotionDetail;
import com.nineyi.data.model.promotion.basket.Basket;
import com.nineyi.data.model.promotion.discount.PromotionDiscount;
import com.nineyi.data.model.ranking.SaleRanking;
import com.nineyi.data.model.referee.LocationRefereeSetting;
import com.nineyi.data.model.referee.RefereeEmployeeList;
import com.nineyi.data.model.referee.RefereeInsert;
import com.nineyi.data.model.referee.RefereeLocationList;
import com.nineyi.data.model.reward.MemberRewardPointRoot;
import com.nineyi.data.model.reward.RewardPointDetailRoot;
import com.nineyi.data.model.reward.RewardPointRoot;
import com.nineyi.data.model.salepage.SalePageListByIds;
import com.nineyi.data.model.salepage.SalePagePrice;
import com.nineyi.data.model.salepage.SalePageRealTimeDatum;
import com.nineyi.data.model.salepage.SellingQty;
import com.nineyi.data.model.salepage.SkuPostRawData;
import com.nineyi.data.model.search.SearchTerm;
import com.nineyi.data.model.search.ShopCategoryListBySearch;
import com.nineyi.data.model.search.ShopHotKeywordList;
import com.nineyi.data.model.search.ShopPayTypeAndShippingTypeList;
import com.nineyi.data.model.search.ShopSalePageBySearch;
import com.nineyi.data.model.shopapp.ShopStatus;
import com.nineyi.data.model.shopintroduction.ShopInformation;
import com.nineyi.data.model.shoppingcart.v4.LocationListForPickup;
import com.nineyi.data.model.shoppingcart.v4.ShopShipping;
import com.nineyi.data.model.trace.TraceSalePageList;

import java.util.ArrayList;
import java.util.Map;

import io.reactivex.Flowable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by toby on 2015/11/9.
 */
public interface WebApiService {

    @GET("/webapi/APPNotification/checkwebapistatus")
    Flowable<AppNotificationData> getAppNotification();

    @GET("/WebAPI/APPNotification/GetMobileAppSettings/{shopId}")
    Flowable<AppProfile> getAppSettings(@Path("shopId") int shopId);

    @POST("/webapi/APPNotification/APPRegister")
    Flowable<ReturnCode> getNotifyRegisterRequest(@Body Map<String, String> map);

    @FormUrlEncoded
    @POST("/webapi/APPNotification/UpdateToken")
    Flowable<ReturnCode> getNotifyUpdateTokenRequest(@Field("GUID") String guid, @Field("token") String token);

    @FormUrlEncoded
    @POST("/webapi/APPNotification/UpdateAppAdTrackingId")
    Flowable<ReturnCode> getUpdateAdTrackingId(@Field("guId") String guid, @Field("adTrackingId") String adTrackingId);

    @FormUrlEncoded
    @POST("/webapi/VipMember/GetVipInfo")
    Flowable<VipMemberDataRoot> getVipInfo(@Field("shopId") int shopId, @Field("isBinding") boolean isBinding);

    @GET("/webapi/ShoppingCartV4/GetShoppingCart?source=AndroidApp&channel=Brand&device=Mobile")
    Flowable<String> getShoppingCart(@Query("shopId") int shopId, @Query("appVer") String appVer);

    @FormUrlEncoded
    @POST("/webapi/ShoppingCartV4/Calculate")
    Flowable<String> calculateShoppingCart(@Field("shoppingCart") String shoppingCart, @Field("appVer") String appVer);

    @FormUrlEncoded
    @POST("/webapi/PayV2/RequestPayProcessUrlV2")
    Flowable<ReturnCode> requestPayProcess(@Field("shoppingCart") String shoppingCart);

    @FormUrlEncoded
    @POST("/webapi/ShoppingCartV3/UpdateShoppingCartQty")
    Flowable<ReturnCode> updateShoppingCartQty(@Field("qty") String qty);

    @FormUrlEncoded
    @POST("/webapi/TraceSalePageList/InsertItem/")
    Flowable<String> getTraceSalePageInsertItem(@Field("salePageId") String salePageId, @Field("shopId") String shopId);

    @FormUrlEncoded
    @POST("/webapi/ShoppingCartV2/RemoveItem/")
    Flowable<String> getShoppingCartRemoveItem(@Field("shopId") int shopId, @Field("salePageId") int salePageId, @Field("saleProductSKUId") int saleProductSKUId, @Field("salePageSeq") int salePageSeq);

    @FormUrlEncoded
    @POST("/webapi/ShoppingCartV2/GetCount")
    Flowable<String> getShoppingCartItemCount(@Field("shopId") String shopId);

    @GET("/webapi/Promotion/GetDetail")
    Flowable<PromotionDetail> getPromotionDetailList(@Query("Id") int promotionId, @Query("startIndex") Integer startIndex, @Query("maxCount") Integer maxCount);

    @GET("/WebAPI/LocationV2/GetLocationListForPickup")
    Flowable<LocationListForPickup> getShoppingCartLocationList(@Query("shopId") int shopId, @Query("startIndex") int startIndex,
            @Query("maxCount") int maxCount, @Query("searchKey") String searchKey);

    @GET("/webapi/SearchV2/GetShopHotKeywordList")
    Flowable<ShopHotKeywordList> getShopHotKeywordList(@Query("shopId") int shopId, @Query("maxCount") int maxCount);

    @FormUrlEncoded
    @POST("/webapi/Search/GetShopSalePageTermListByKeyword")
    Flowable<ArrayList<SearchTerm>> getShopSalePageTermListByKeyword(@Field("keyword") String keyword, @Field("shopId") int shopId);

    @GET("/webapi/SearchV2/GetShopPayTypeAndShippingTypeList")
    Flowable<ShopPayTypeAndShippingTypeList> getShopPayTypeAndShippingTypeList(@Query("shopId") int shopId, @Query("r") String r);

    @GET("/webapi/SearchV2/GetShopSalePageBySearch")
    Flowable<ShopSalePageBySearch> getShopSalePageBySearch(@Query("shopId") int shopId, @Query("keyword") String keyword, @Query("order") String orderBy,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount, @Query("displayScore") boolean displayScore,
            @Query("shopCategoryId") String shopCategoryId, @Query("minPrice") String minPrice, @Query("maxPrice") String maxPrice,
            @Query("payType") String payType, @Query("shippingType") String shippingType, @Query("scoreThreshold") double scoreThreshold,
            @Query("isResearch") boolean isResearch);

    @GET("/webapi/SearchV2/GetShopCategoryListBySearch")
    Flowable<ShopCategoryListBySearch> getShopCategoryListBySearch(@Query("shopId") int shopId, @Query("keyword") String keyword, @Query("minPrice") String minPrice, @Query("maxPrice") String maxPrice,
            @Query("payType") String payType, @Query("shippingType") String shippingType, @Query("scoreThreshold") double scoreThreshold, @Query("isResearch") boolean isResearch);

    @GET("WebAPI/PromotionV2/GetList/{shopId}")
    Flowable<PromotionDiscount> getShopPromotionDiscountList(@Path("shopId") int shopId, @Query("OrderBy") String orderBy,
            @Query("StartIndex") int startIndex, @Query("MaxCount") int maxCount);

    @FormUrlEncoded
    @POST("/WebAPI/PromotionV2/Calculate")
    Flowable<Basket> getBasketListAfterCalculate(@Field("promotionDetailDiscount") String basketItemValue, @Field("appVer") String appVer);

    @GET("/webapi/HotSaleRanking/GetHotSaleRankingList/{shopId}")
    Flowable<SaleRanking> getHotSaleRanking(@Path("shopId") int shopId, @Query("maxCount") int maxCount, @Query("period") String period);

    @GET("/webapi/InfoModule/GetEditorPickList/")
    Flowable<InfoModuleRecommandation> getInfoModuleRecommandation(@Query("shopId") int shopId);

    @FormUrlEncoded
    @POST("/webapi/ShoppingCartV4/InsertItem/")
    Flowable<ReturnCode> getAddToShoppingCart(@Field("shopId") int shopId, @Field("salePageId") long salePageId, @Field("saleProductSKUId") long skuId, @Field("qty") int qty);

    @GET("webapi/Shop/GetShopShippingDetailList/{shopId}")
    Flowable<ShopShipping> getShopCategoryListBySearch(@Path("shopId") int shopId, @Query("appVer") String appVer);

    @GET("/webapi/LocationV2/GetCityAreaList")
    Flowable<CityAreaListRoot> getCityAreaList(@Query("shopId") int shopId);

    @GET("/webapi/LocationV2/GetLocationList")
    Flowable<LocationListRoot> getLocationList(@Query("shopId") int shopId, @Query("lat") Double lat, @Query("lon") Double lon);

    @GET("/webapi/LocationV2/GetLocationListByArea")
    Flowable<LocationListByCityRoot> getLocationListByArea(@Query("shopId") int shopId, @Query("areaId") int areaId,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @GET("/webapi/LocationV2/GetOverseaLocationList")
    Flowable<LocationListByCityRoot> getOverseaLocationList(@Query("shopId") int shopId,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @GET("/webapi/LocationV2/GetLocationListByCity")
    Flowable<LocationListByCityRoot> getLocationListByCity(@Query("shopId") int shopId, @Query("cityid") int cityId,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @POST("/webapi/APPNotification/GetAllAppPhshProfileDataV2/")
    Flowable<NotifyProfileReturnCode> getAllAppPhshProfileDataV2(@Query("GUID") String guid);

    @POST("/webapi/APPNotification/SetAPPPushProfileDataV2/")
    Flowable<ReturnCode> setAPPPushProfileDataV2(@Body NotifyProfileInputData data);

    @POST("/webapi/Shop/CheckAppIsEnable/")
    Flowable<ShopStatus> checkAppIsEnable(@Query("shopid") int shopId, @Query("OSType") String osType);

    @POST("/webapi/TraceSalePageList/GetCount/")
    Flowable<Integer> traceSalePageList(@Query("shopId") int shopId);

    @GET("/webapi/Activity/GetActivityList")
    Flowable<ActivityList> getActivityList(@Query("shopId") int shopId);

    @GET("/webapi/AppReferee/GetAppRefereeProfile/")
    Flowable<LocationRefereeSetting> getAppRefereeProfile(@Query("shopid") int shopId);

    @POST("/webapi/SalePage/GetSalePageRealTimeData/{productId}")
    Flowable<SalePageRealTimeDatum> getSalePageRealTimeData(@Path("productId") int productId);

    @POST("/webapi/SalePage/GetSalepageDataByIds/")
    Flowable<SalePageListByIds> getSalepageDataByIds(@Query("ids") String ids);

    @GET("/webapi/LocationRewardPoint/GetRewardPointDetail")
    Flowable<RewardPointDetailRoot> getRewardPointDetail(@Query("RewardPointId") int rewardId);

    @GET("/webapi/LocationRewardPoint/GetMemberRewardPoint")
    Flowable<MemberRewardPointRoot> getMemberRewardPoint(@Query("RewardPointId") int rewardID);

    @POST("/webapi/Auth/MergeMemberFavorites")
    Flowable<String> mergeMemberFavorites();

    @POST("/webapi/TraceSalePageList/GetDataList")
    Flowable<TraceSalePageList> getDataList(@Query("shopId") int shopId);

    @GET("/WebApi/VipMember/GetVipMemberBenefits")
    Flowable<VipMemberBenefitsModel> getVipMemberBenefits(@Query("shopId") int shopId);

    @GET("/webapi/AppReferee/GetLocationEmployeeList/")
    Flowable<RefereeEmployeeList> getLocationEmployeeList(@Query("shopid") int shopId, @Query("locationId") int locationId);

    @GET("/webapi/AppReferee/InsertAppReferee/")
    Flowable<RefereeInsert> insertAppReferee(@Query("guid") String guid, @Query("shopId") int shopId,
            @Query("locationId") int locationId, @Query("empId") Integer empId,
            @Query("isRequireLogin") boolean isRequireLogin);

    @POST("/webapi/Location/GetMemberInfo")
    Flowable<LocationWizardMemberInfoRoot> getMemberInfo();

    @POST("/webapi/Location/AddLocationMemberRequest")
    Flowable<LocationWizardBeaconInfoRoot> addLocationMemberRequest(
            @Query("uuid") String uuid,
            @Query("major") String major,
            @Query("minor") String minor,
            @Query("locationid") String locationId,
            @Query("memberid") String memberId,
            @Query("requestType") String requestType,
            @Query("couponId") int couponId,
            @Query("usercouponId") int userCouponId);

    @GET("/webapi/Activity/GetActivityDetail/{activityId}")
    Flowable<ActivityDetail> getActivityDetail(@Path("activityId") int activityId, @Query("shopId") int shopId);

    @POST("/webapi/TraceSalePageList/DeleteItem/")
    Flowable<String> deleteItem(@Query("salePageId") int salePageId, @Query("shopId") int shopId);

    @GET("/webapi/InfoModuleV2/GetArticleDetail")
    Flowable<InfoModuleArticleDetail> getArticleDetail(@Query("articleId") int articleId);

    @GET("/webapi/InfoModuleV2/GetAlbumDetail")
    Flowable<InfoModuleAlbumDetail> getAlbumDetail(@Query("albumId") int articleId);

    @GET("/webapi/InfoModuleV2/GetVideoDetail")
    Flowable<InfoModuleVideoDetail> getVideoDetail(@Query("videoId") int videoId);

    @POST("/webapi/VipMember/BindingShopLocationVIPMember")
    Flowable<ReturnCode> bindingShopLocationVIPMember(@Body VipMemberPostData postData);

    @GET("/webapi/VipMember/GetVIPMemberItemV2")
    Flowable<VipMemberItemData> getVIPMemberItem(@Query("shopId") int shopId, @Query("memberCardId") long memberCardId,
            @Query("ruleEnum") int isBinding, @Query("appVer") String appVer,
            @Query("isDropDownListAddress") boolean isDropDown);

    @GET("/webAPI/VIPMember/GetThirdPartyTradesOrderConfiguration")
    Flowable<ThirdPartyTradeOrderRoot> getThirdPartyTradesOrderConfiguration(@Query("shopId") int shopId);

    @POST("/webapi/VipMember/GetNonVIPOpenCardPresentStatus")
    Flowable<PresentStatus> getNonVIPOpenCardPresentStatus(@Query("shopId") int shopId, @Query("appVer") String appVer);

    @POST("/webapi/VipMember/GetOpenCardPresentStatus")
    Flowable<PresentStatus> getOpenCardPresentStatus(@Query("shopId") int shopId, @Query("appVer") String appVer);

    @POST("/webapi/VipMember/GetBirthdayPresentStatus")
    Flowable<PresentStatus> getBirthdayPresentStatus(@Query("shopId") int shopId, @Query("appVer") String appVer);

    @GET("/webapi/VipMember/GetVipShopInfo")
    Flowable<VipShopInfo> getVipShopInfo(@Query("shopId") int shopId);

    @POST("/webapi/VipMember/RegisterVIPMember")
    Flowable<ReturnCode> registerVIPMember(@Body VipMemberPostData postData);

    @POST("/webapi/VIPMember/InsertOrUpdateVIPMember")
    Flowable<ReturnCode> insertOrUpdateVIPMember(@Body VipMemberPostData postData);

    @GET("/webapi/InfoModuleV2/GetArticleList")
    Flowable<ArticleModule> getArticleList(@Query("shopId") int shopId,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @GET("/webapi/InfoModuleV2/GetAlbumList")
    Flowable<AlbumModule> getAlbumList(@Query("shopId") int shopId,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @GET("/webapi/InfoModuleV2/GetVideoList")
    Flowable<VideoModule> getVideoList(@Query("shopId") int shopId,
            @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @GET("/webapi/AppReferee/GetLocationList")
    Flowable<RefereeLocationList> getAppRefereeLocationList(@Query("shopid") int shopId);

    @GET("/webapi/SalePage/GetIsGiftSalePage/{salePageId}")
    Flowable<FreeGiftSalePage> getIsGiftSalePage(@Path("salePageId") int salePageId);

    @GET("/webapi/SalePage/GetSalePagePrice/{productId}")
    Flowable<SalePagePrice> getSalePagePrice(@Path("productId") int productId);

    @POST("/webapi/ProductStock/GetSellingQtyListNew/")
    Flowable<ArrayList<SellingQty>> getSellingQtyListNew(@Body SkuPostRawData ids);

    @GET("/webapi/Lbs/GetLbsList")
    Flowable<DONETLbsList> getLbsList(@Query("shopId") int shopId, @Query("lat") Double lat, @Query("lon") Double lon);

    @GET("/webapi/LocationRewardPoint/GetRewardPointList")
    Flowable<RewardPointRoot> getRewardPointList(@Query("ShopId") int shopId);

    @POST("/webapi/notificationcenter/getfrontendList/")
    Flowable<ArrayList<NotifyMessage>> getFrontendList(@Query("shopId") int shopId, @Query("startIndex") int startIndex, @Query("maxCount") int maxCount);

    @POST("/webapi/CrmMember/GetCrmMemberTier/{shopId}")
    Flowable<CrmMemberTierData> getCrmMemberTier(@Path("shopId") int shopId);


    @POST("/WebAPI/CrmShopMemberCard/GetCrmShopMemberCardInfo/{shopId}")
    Flowable<CrmShopMemberCard> getCrmShopMemberCardInfo(@Path("shopId") int shopId);

    @GET("/WebAPI/VipMember/GetVipMemberEmailNotification/{shopId}")
    Flowable<EmailNotification> getEmailNotificationSetting(@Path("shopId") int shopId);

    @POST("/WebAPI/VipMember/UpdateVipMemberEmailNotification")
    Flowable<UpdateEmailNotification> updateEmailNotificationSetting(@Query("shopId") int shopId, @Body
            EmailNotificationData data);

    @GET("/webapi/Shop/GetShopInformation/{shopId}")
    Flowable<ShopInformation> getShopInformation(@Path("shopId") int shopId, @Query("appVer") String appVer);

    //會員分級活動清單
    @GET("/WebAPI/PromotionV2/GetListForCrmMemberTier/{shopId}/{memberCardId}")
    Flowable<PromotionDiscount> getListForCrmMemberTier(@Path("shopId") int shopId, @Path("memberCardId") int memberCardId);

    @GET("/webapi/MemberTradesOrder/GetShippingStatusForUser")
    Flowable<ShippingStatus> getShippingStatusForUser(@Query("shopId") int shopId);

    @GET("/webapi/AppAnnouncement/CheckLoginMemberStatus")
    Flowable<ReturnCode> checkLoginMemberStatus();
}
