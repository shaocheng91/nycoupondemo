package com.nineyi.retrofit.apiservice;

import com.nineyi.data.model.apiresponse.ReturnCode;
import com.nineyi.data.model.login.LoginReturnCode;
import com.nineyi.data.model.login.LoginThirdPartyReturnCode;
import com.nineyi.data.model.login.ThirdPartyAuthInfoRoot;

import io.reactivex.Flowable;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by tedliang on 2016/9/26.
 */

public interface LoginApiService {
    @POST("/WebAPI/AuthV3/CreateFacebookMemberRegisterRequest")
    Flowable<LoginReturnCode> createFacebookMemberRegisterRequest(@Query("token") String token, @Query("cellPhone") String cellPhone, @Query("shopId") int shopId);

    @POST("/WebAPI/AuthV3/CreateNineYiMemberRegisterRequest")
    Flowable<LoginReturnCode> createNineYiMemberRegisterRequest(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId);

    @POST("/WebAPI/AuthV3/GetFacebookMemberRegisterStatus")
    Flowable<LoginReturnCode> getFacebookMemberRegisterStatus(@Query("token") String token, @Query("shopId") int shopId);

    @POST("/WebAPI/AuthV3/GetNineYiMemberRegisterStatus")
    Flowable<LoginReturnCode> getNineYiMemberRegisterStatus(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId);

    @POST("/WebAPI/AuthV3/LoginFacebookMember")
    Flowable<LoginReturnCode> loginFacebookMember(@Query("token") String token,
            @Query("shopId") int shopId,
            @Query("source") String source,
            @Query("device") String device,
            @Query("appVer") String appVer);

    @POST("/webapi/AuthV3/GetThirdpartyMemberRegisterStatus")
    Flowable<LoginThirdPartyReturnCode> getThirdpartyMemberRegisterStatus(@Query("loginId") String loginId,
            @Query("password") String password, @Query("shopId") int shopId);

    @POST("/WebAPI/AuthV3/CreateThirdpartyMemberRegisterRequest")
    Flowable<ReturnCode> createThirdpartyMemberRegisterRequest(@Query("authSessionToken") String token,
            @Query("cellPhone") String cellPhone, @Query("shopId") int shopId);

    @POST("/WebAPI/AuthV3/CreateNineYiMemberResetPasswordRequest")
    Flowable<LoginReturnCode> createNineYiMemberResetPasswordRequest(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId);

    @POST("/WebAPI/AuthV3/LoginNineYiMember")
    Flowable<LoginReturnCode> loginNineYiMember(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId,
            @Query("password") String password, @Query("source") String source, @Query("device") String device,
            @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/GetPhoneDialVerifyStatus")
    Flowable<LoginReturnCode> getPhoneDialVerifyStatus(@Query("verifyType") String verifyType);

    @POST("/WebAPI/AuthV3/GetShopThirdpartyAuthInfo")
    Flowable<ThirdPartyAuthInfoRoot> getShopThirdpartyAuthInfo(@Query("shopId") int shopId, @Query("device") String device);

    @POST("/WebAPI/AuthV3/GetThirdpartyMemberRegisterStatusWithToken")
    Flowable<LoginThirdPartyReturnCode> getThirdpartyMemberRegisterStatusWithToken(@Query("shopId") int shopId, @Query("accessToken") String accessToken);

    @POST("/WebAPI/AuthV3/LoginThirdpartyMember")
    Flowable<LoginReturnCode> loginThirdpartyMember(@Query("authSessionToken") String token, @Query("shopId") int shopId, @Query("source") String source, @Query("device") String device, @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/ResendVerifyCode")
    Flowable<LoginReturnCode> resendVerifyCode(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("memberType") String memberType, @Query("verifyType") String verifyType);

    @POST("/WebAPI/AuthV3/ConfirmFacebookMemberVerifyCode")
    Flowable<LoginReturnCode> confirmFacebookMemberVerifyCode(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("code") String code, @Query("token") String token, @Query("source") String source, @Query("device") String device, @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/ConfirmFacebookMemberPhoneDialVerify")
    Flowable<LoginReturnCode> confirmFacebookMemberPhoneDialVerify(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("token") String token, @Query("source") String source, @Query("device") String device, @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/ConfirmNineYiMemberVerifyCode")
    Flowable<LoginReturnCode> confirmNineYiMemberVerifyCode(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("code") String code, @Query("verifyType") String verifyType);

    @POST("/WebAPI/AuthV3/ConfirmNineYiMemberPhoneDialVerify")
    Flowable<LoginReturnCode> confirmNineYiMemberPhoneDialVerify(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("verifyType") String verifyType);

    @POST("/WebAPI/AuthV3/ConfirmThirdpartyMemberVerifyCode")
    Flowable<LoginReturnCode> confirmThirdpartyMemberVerifyCode(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("authSessionToken") String token, @Query("code") String code, @Query("source") String source, @Query("device") String device, @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/ConfirmThirdpartyMemberPhoneDialVerify")
    Flowable<LoginReturnCode> confirmThirdpartyMemberPhoneDialVerify(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("authSessionToken") String token, @Query("source") String source, @Query("device") String device, @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/FinishNineYiMemberResetPassword")
    Flowable<LoginReturnCode> finishNineYiMemberResetPassword(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("password") String password, @Query("source") String source, @Query("device") String device, @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/FinishNineYiMemberRegister")
    Flowable<LoginReturnCode> finishNineYiMemberRegister(@Query("cellPhone") String cellPhone, @Query("shopId") int shopId, @Query("password") String password, @Query("source") String source, @Query("device") String device, @Query("appVer") String appVer);

    @POST("/WebAPI/AuthV3/ChangeNineYiMemberPassword")
    Flowable<LoginReturnCode> changeNineYiMemberPassword(@Query("oldPassword") String oldPassword, @Query("newPassword") String newPassword, @Query("shopId") int shopId);
}
