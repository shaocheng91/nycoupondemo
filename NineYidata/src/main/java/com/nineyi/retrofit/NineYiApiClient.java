package com.nineyi.retrofit;

import com.nineyi.data.NineYiWSConfig;
import com.nineyi.data.aes.AesCipher;
import com.nineyi.data.aes.CipherData;
import com.nineyi.data.model.NotifyMessage;
import com.nineyi.data.model.activity.ActivityDetail;
import com.nineyi.data.model.activity.ActivityList;
import com.nineyi.data.model.apirequest.InfoModuleOfficialValues;
import com.nineyi.data.model.apirequest.RecommendSalePageListValue;
import com.nineyi.data.model.apiresponse.ReturnCode;
import com.nineyi.data.model.appmain.Announcement;
import com.nineyi.data.model.category.SalePageListReturnCode;
import com.nineyi.data.model.category.ShopCategoryList;
import com.nineyi.data.model.ecoupon.ECouponVerify;
import com.nineyi.data.model.ecoupon.ECouponIncludeDetail;
import com.nineyi.data.model.ecoupon.ECouponMemberList;
import com.nineyi.data.model.ecoupon.ECouponShopECouponList;
import com.nineyi.data.model.ecoupon.ECouponStatusList;
import com.nineyi.data.model.fanpage.FBContainer;
import com.nineyi.data.model.fanpage.FBLogin;
import com.nineyi.data.model.infomodule.InfoModuleClientOfficial;
import com.nineyi.data.model.infomodule.InfoModuleItemOfficial;
import com.nineyi.data.model.infomodule.InfoModuleRecommandation;
import com.nineyi.data.model.infomodule.albumdetailv2.InfoModuleAlbumDetail;
import com.nineyi.data.model.infomodule.albumlist.AlbumModule;
import com.nineyi.data.model.infomodule.articledetailv2.InfoModuleArticleDetail;
import com.nineyi.data.model.infomodule.articlelist.ArticleModule;
import com.nineyi.data.model.infomodule.videodetailv2.InfoModuleVideoDetail;
import com.nineyi.data.model.infomodule.videolist.VideoModule;
import com.nineyi.data.model.layout.LayoutTemplateCodeAndData;
import com.nineyi.data.model.layout.LayoutTemplateData;
import com.nineyi.data.model.layout.RecommendSalePageListReturnCode;
import com.nineyi.data.model.locationwizard.LocationWizardBeaconInfoRoot;
import com.nineyi.data.model.locationwizard.LocationWizardMemberInfoRoot;
import com.nineyi.data.model.login.LoginReturnCode;
import com.nineyi.data.model.login.LoginThirdPartyReturnCode;
import com.nineyi.data.model.login.ThirdPartyAuthInfoRoot;
import com.nineyi.data.model.login.VipMemberBenefitsModel;
import com.nineyi.data.model.memberzone.CrmMemberTierData;
import com.nineyi.data.model.memberzone.CrmShopMemberCard;
import com.nineyi.data.model.memberzone.PresentStatus;
import com.nineyi.data.model.memberzone.ShippingStatus;
import com.nineyi.data.model.memberzone.ThirdPartyTradeOrderRoot;
import com.nineyi.data.model.memberzone.VipMemberDataRoot;
import com.nineyi.data.model.memberzone.VipMemberItemData;
import com.nineyi.data.model.memberzone.VipMemberPostData;
import com.nineyi.data.model.memberzone.VipShopInfo;
import com.nineyi.data.model.newlbs.DONETLbsList;
import com.nineyi.data.model.newo2o.CityAreaListRoot;
import com.nineyi.data.model.newo2o.LocationListByCityRoot;
import com.nineyi.data.model.newo2o.LocationListRoot;
import com.nineyi.data.model.notify.EmailNotification;
import com.nineyi.data.model.notify.EmailNotificationData;
import com.nineyi.data.model.notify.NotifyProfileInputData;
import com.nineyi.data.model.notify.NotifyProfileReturnCode;
import com.nineyi.data.model.notify.NotifyRegister;
import com.nineyi.data.model.notify.UpdateEmailNotification;
import com.nineyi.data.model.openapp.AppNotificationData;
import com.nineyi.data.model.openapp.AppProfile;
import com.nineyi.data.model.php.PhpCouponList;
import com.nineyi.data.model.php.PhpCouponTakeStatus;
import com.nineyi.data.model.php.PhpCouponUseStatus;
import com.nineyi.data.model.promotion.FreeGiftSalePage;
import com.nineyi.data.model.promotion.PromotionDetail;
import com.nineyi.data.model.promotion.PromotionListReturnCode;
import com.nineyi.data.model.promotion.basket.Basket;
import com.nineyi.data.model.promotion.discount.PromotionDiscount;
import com.nineyi.data.model.promotion.v2.PromoteSalePage;
import com.nineyi.data.model.promotion.v2.PromotionV2Detail;
import com.nineyi.data.model.ranking.SaleRanking;
import com.nineyi.data.model.referee.LocationRefereeSetting;
import com.nineyi.data.model.referee.RefereeEmployeeList;
import com.nineyi.data.model.referee.RefereeInsert;
import com.nineyi.data.model.referee.RefereeLocationList;
import com.nineyi.data.model.reward.MemberRewardPointRoot;
import com.nineyi.data.model.reward.RewardPointDetailRoot;
import com.nineyi.data.model.reward.RewardPointRoot;
import com.nineyi.data.model.salepage.SalePage;
import com.nineyi.data.model.salepage.SalePageHotList;
import com.nineyi.data.model.salepage.SalePageListByIds;
import com.nineyi.data.model.salepage.SalePageMoreInfo;
import com.nineyi.data.model.salepage.SalePagePrice;
import com.nineyi.data.model.salepage.SalePageRealTimeDatum;
import com.nineyi.data.model.salepage.SellingQty;
import com.nineyi.data.model.salepage.SkuPostRawData;
import com.nineyi.data.model.salepagev2info.SalePageAdditionalInfo;
import com.nineyi.data.model.salepagev2info.SalePageV2Info;
import com.nineyi.data.model.salepagev2info.SalePageWrapper;
import com.nineyi.data.model.search.SearchTerm;
import com.nineyi.data.model.search.ShopCategoryListBySearch;
import com.nineyi.data.model.search.ShopHotKeywordList;
import com.nineyi.data.model.search.ShopPayTypeAndShippingTypeList;
import com.nineyi.data.model.search.ShopSalePageBySearch;
import com.nineyi.data.model.shopapp.ShopContractSetting;
import com.nineyi.data.model.shopapp.ShopStatus;
import com.nineyi.data.model.shopinfo.ShopIntroduction;
import com.nineyi.data.model.shopintroduction.ShopInformation;
import com.nineyi.data.model.shoppingcart.buyextra.BuyExtra;
import com.nineyi.data.model.shoppingcart.v4.LocationListForPickup;
import com.nineyi.data.model.shoppingcart.v4.ShopShipping;
import com.nineyi.data.model.trace.TraceSalePageList;
import com.nineyi.retrofit.apiservice.Api2Service;
import com.nineyi.retrofit.apiservice.CdnService;
import com.nineyi.retrofit.apiservice.ECouponService;
import com.nineyi.retrofit.apiservice.FacebookApiService;
import com.nineyi.retrofit.apiservice.LoginApiService;
import com.nineyi.retrofit.apiservice.TrackService;
import com.nineyi.retrofit.apiservice.WebApiService;
import com.nineyi.utils.ObservableUtils;
import com.nineyidata.BuildConfig;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import retrofit2.http.Query;


/**
 * Entry point for all requests to NineYi web API.
 *
 * Created by toby on 2015/11/27.
 */
public class NineYiApiClient {

    private static NineYiApiClient sInstance;

    private WebApiService mWebApiService;

    private Api2Service mApi2Service;

    private CdnService mCdnService;

    private ECouponService mECouponService;

    private FacebookApiService mFacebookApiService;

    private LoginApiService mLoginApiService;

    private TrackService mTrackService;

    private NineYiApiClient() {
    }

    public static void init(WebApiService webApiService, Api2Service api2Service, ECouponService eCouponService,
            TrackService trackService, CdnService cdnService, FacebookApiService facebookApiService, LoginApiService loginApiService) {
        // TODO IllegalArgumentException

        if (sInstance == null) {
            sInstance = new NineYiApiClient();
        }

        sInstance.mWebApiService = webApiService;

        sInstance.mApi2Service = api2Service;

        sInstance.mECouponService = eCouponService;

        sInstance.mCdnService = cdnService;

        sInstance.mFacebookApiService = facebookApiService;

        sInstance.mLoginApiService = loginApiService;

        sInstance.mTrackService = trackService;
    }

    public static Flowable<String> getShoppingCart(int shopId, String appVer) {
        return sInstance.getWebApiService().getShoppingCart(shopId, appVer);
    }

    private TrackService getTrackService(){
        if(mTrackService == null){
            if(BuildConfig.DEBUG){
                throw new IllegalStateException();
            }
        }
        return mTrackService;
    }

    private WebApiService getWebApiService() {
        if (mWebApiService == null) {
            // TODO throw IllegalStateException
        }

        return mWebApiService;
    }

    private Api2Service getApi2Service() {
        if (mApi2Service == null) {
            // TODO throw IllegalStateException
        }

        return mApi2Service;
    }

    private CdnService getCdnService() {
        if (mCdnService == null) {
            // TODO throw IllegalStateException
        }

        return mCdnService;
    }

    private ECouponService getECouponService() {
        if (mECouponService == null) {
            // TODO throw IllegalStateException
        }

        return mECouponService;
    }

    private FacebookApiService getFacebookApiService() {
        if (mFacebookApiService == null) {
            // TODO throw IllegalStateException
        }

        return mFacebookApiService;
    }

    private LoginApiService getLoginApiService() {
        if (mLoginApiService == null) {
            // TODO throw IllegalStateException
        }

        return mLoginApiService;
    }

    public static void setCdnService(CdnService cdnService) {
        sInstance.mCdnService = cdnService;
    }

    public static Flowable<AppNotificationData> getAppNotification() {
        return sInstance.getWebApiService().getAppNotification()
                .compose(ObservableUtils.<AppNotificationData>schedulersHandling());
    }

    public static Flowable<AppProfile> getAppSettings(int shopId) {
        return sInstance.getWebApiService().getAppSettings(shopId)
                .compose(ObservableUtils.<AppProfile>schedulersHandling());
    }

    public static Flowable<String> getShoppingCartV4Request(int shopId, String appVer) {
        return sInstance.getWebApiService().getShoppingCart(shopId, appVer)
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<BuyExtra> getBuyExtra(int shopId) {
        return sInstance.getCdnService().getBuyExtra(shopId)
                .compose(ObservableUtils.<BuyExtra>schedulersHandling());
    }

    public static Flowable<ReturnCode> getUpdateShoppingCartQty(String qty) {
        return sInstance.getWebApiService().updateShoppingCartQty(qty)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<String> getTraceSalePageInsertItem(int salePageId, int shopId) {
        return sInstance.getWebApiService()
                .getTraceSalePageInsertItem(String.valueOf(salePageId), String.valueOf(shopId))
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<String> getShoppingCartRemoveItem(int shopId, int salePageId, int saleProductSKUId,
            int salePageGroupSeq) {
        return sInstance.getWebApiService()
                .getShoppingCartRemoveItem(shopId, salePageId, saleProductSKUId, salePageGroupSeq)
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<String> getShoppingCartItemCount(int shopId) {
        return sInstance.getWebApiService().getShoppingCartItemCount(String.valueOf(shopId))
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<String> calculateRequest(String shoppingCart, String appVer) {
        return sInstance.getWebApiService().calculateShoppingCart(shoppingCart, appVer)
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<ReturnCode> requestPayProcess(String shoppingCart) {
        return sInstance.getWebApiService().requestPayProcess(shoppingCart)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<ReturnCode> getNotifyRegisterRequest(String uuid, int shopId, String token) {
        NotifyRegister item = new NotifyRegister();
        item.UDID = uuid;
        item.ShopID = shopId;
        item.platformID = "Android";
        item.token = token;

        String jsonText = NineYiWSConfig.sGson.toJson(item);

        AesCipher cipher = NineYiWSConfig.getAppBuildConfig().aesCipher;
        CipherData data = cipher.encryptString(jsonText);
        return sInstance.getWebApiService().getNotifyRegisterRequest(data.generateMap())
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<ReturnCode> getNotifyUpdateTokenRequest(String guid, String token) {
        return sInstance.getWebApiService().getNotifyUpdateTokenRequest(guid, token)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<ReturnCode> getUpdateAdTrackingId(String guid, String adtrackingId) {
        return sInstance.getWebApiService().getUpdateAdTrackingId(guid, adtrackingId)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<String> getMemberECouponListByUsingDateTime(int shopId) {
        return sInstance.getECouponService().getMemberECouponListByUsingDateTime(shopId)
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<PromotionListReturnCode> getPromotionList(int shopId, int categoryId) {
        return sInstance.getCdnService().getPromotionList(shopId, categoryId)
                .compose(ObservableUtils.<PromotionListReturnCode>schedulersHandling());
    }

    public static Flowable<PromotionDetail> getPromotionDetailList(int promotionId, Integer startIndex, Integer maxCount) {
        return sInstance.getWebApiService().getPromotionDetailList(promotionId, startIndex, maxCount)
                .compose(ObservableUtils.<PromotionDetail>schedulersHandling());
    }

    public static Flowable<LocationListForPickup> getShoppingCartLocationList(int shopId, int startIndex,
            int maxCount) {
        return sInstance.getWebApiService().getShoppingCartLocationList(shopId, startIndex, maxCount, "")
                .compose(ObservableUtils.<LocationListForPickup>schedulersHandling());
    }

    public static Flowable<VipMemberDataRoot> getVipInfo(int shopId, boolean isBinding) {
        return sInstance.getWebApiService().getVipInfo(shopId, isBinding)
                .compose(ObservableUtils.<VipMemberDataRoot>schedulersHandling());
    }

    public static Flowable<ShopHotKeywordList> getShopHotKeywordList(int shopId, int maxCount) {
        return sInstance.getWebApiService().getShopHotKeywordList(shopId, maxCount)
                .compose(ObservableUtils.<ShopHotKeywordList>schedulersHandling());
    }

    public static Flowable<ArrayList<SearchTerm>> getShopSalePageTermListByKeyword(String keyword, int shopId) {
        return sInstance.getWebApiService().getShopSalePageTermListByKeyword(keyword, shopId)
                .compose(ObservableUtils.<ArrayList<SearchTerm>>schedulersHandling());
    }


    public static Flowable<ShopPayTypeAndShippingTypeList> getShopPayTypeAndShippingTypeList(int shopId,
            String isClearCache) {
        return sInstance.getWebApiService().getShopPayTypeAndShippingTypeList(shopId, isClearCache)
                .compose(ObservableUtils.<ShopPayTypeAndShippingTypeList>schedulersHandling());
    }

    public static Flowable<ShopSalePageBySearch> getShopSalePageBySearch(int shopId, String keyword, String orderBy,
            int startIndex, int maxCount, boolean displayScore, String shopCategoryId, String minPrice, String maxPrice,
            String payType, String shippingType, double scoreThreshold, boolean isResearch) {

        return sInstance.getWebApiService()
                .getShopSalePageBySearch(shopId, keyword, orderBy, startIndex, maxCount, displayScore, shopCategoryId,
                        minPrice, maxPrice, payType, shippingType, scoreThreshold, isResearch)
                .compose(ObservableUtils.<ShopSalePageBySearch>schedulersHandling());
    }


    public static Flowable<ShopCategoryListBySearch> getShopCategoryListBySearch(int shopId, String keyword,
            String minPrice, String maxPrice, String payType, String shippingType, double scoreThreshold,
            boolean isResearch) {

        return sInstance.getWebApiService()
                .getShopCategoryListBySearch(shopId, keyword, minPrice, maxPrice, payType, shippingType, scoreThreshold,
                        isResearch).compose(ObservableUtils.<ShopCategoryListBySearch>schedulersHandling());
    }

    public static Flowable<PromotionDiscount> getShopPromotionDiscountList(int shopId, String orderBy, int startIndex,
            int maxCount, String typeDef) {
        return sInstance.getCdnService().getShopPromotionDiscountList(shopId, orderBy, startIndex, maxCount, typeDef)
                .compose(ObservableUtils.<PromotionDiscount>schedulersHandling());
    }

    public static Flowable<PromotionV2Detail> getPromotionDetailV2(int promotionId) {
        return sInstance.getCdnService().getPromotionDetailV2(promotionId)
                .compose(ObservableUtils.<PromotionV2Detail>schedulersHandling());
    }

    public static Flowable<PromoteSalePage> getPromoteSalePageList(int shopId, int promoteId, int categoryId,
            int startIndex, int maxCount) {
        return sInstance.getCdnService().getPromoteSalePageList(shopId, promoteId, categoryId, startIndex, maxCount)
                .compose(ObservableUtils.<PromoteSalePage>schedulersHandling());
    }

    public static Flowable<SalePage> getSalePage(int salePageId) {
        return sInstance.getCdnService().getSalePage(salePageId)
                .compose(ObservableUtils.<SalePage>schedulersHandling());
    }

    public static Flowable<Basket> getBasketListAfterCalculate(String basketItem, String appVer) {
        return sInstance.getWebApiService().getBasketListAfterCalculate(basketItem, appVer)
                .compose(ObservableUtils.<Basket>schedulersHandling());
    }

    public static Flowable<ReturnCode> getAddToShoppingCart(int shopId, long salePageId, long skuId, int qty) {
        return sInstance.getWebApiService().getAddToShoppingCart(shopId, salePageId, skuId, qty)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<ArrayList<LayoutTemplateCodeAndData>> getShopAllHomeLayoutTemplate(int shopId) {
        return sInstance.getCdnService().getShopAllHomeLayoutTemplate(shopId)
                .compose(ObservableUtils.<ArrayList<LayoutTemplateCodeAndData>>schedulersHandling());
    }

    public static Flowable<ArrayList<InfoModuleItemOfficial>> getInfoModuleOfficial(
            InfoModuleOfficialValues infoModuleOfficialValues) {
        Map<String, String> map = new LinkedHashMap<>();
        for (int i = 0, size = infoModuleOfficialValues.infoModuleClients.size(); i < size; i++) {
            InfoModuleClientOfficial info = infoModuleOfficialValues.infoModuleClients.get(i);

            map.put("infos[" + i + "].InfoModuleId", info.InfoModuleId);
            map.put("infos[" + i + "].InfoModuleType", info.InfoModuleType);
            map.put("infos[" + i + "].Order", String.valueOf(info.Order));
        }
        int shopId = infoModuleOfficialValues.shopId;
        return sInstance.getCdnService().getInfoModuleOfficial(shopId, map)
                .compose(ObservableUtils.<ArrayList<InfoModuleItemOfficial>>schedulersHandling());
    }

    public static Flowable<SaleRanking> getHotSaleRanking(int shopId, int maxCount, String period) {
        return sInstance.getWebApiService().getHotSaleRanking(shopId, maxCount, period)
                .compose(ObservableUtils.<SaleRanking>schedulersHandling());
    }

    public static Flowable<RecommendSalePageListReturnCode> getRecommendSalePageList(
            RecommendSalePageListValue recommendSalePageListValue) {

        Map<String, String> map = new LinkedHashMap<>();
        for (int i = 0; i < recommendSalePageListValue.cidList.size(); i++) {
            map.put("cidList[" + i + "]", recommendSalePageListValue.cidList.get(i).toString());
        }

        map.put("maxCount", String.valueOf(recommendSalePageListValue.maxCount));
        map.put("orderBy", String.valueOf(recommendSalePageListValue.orderby.toLowerCase()));
        map.put("startIndex", String.valueOf(recommendSalePageListValue.startIndex));

        int shopId = recommendSalePageListValue.shopId;
        return sInstance.getCdnService().getRecommendSalePageList(shopId, map)
                .compose(ObservableUtils.<RecommendSalePageListReturnCode>schedulersHandling());
    }

    public static Flowable<ArrayList<LayoutTemplateData>> getLayoutTemplateData(int shopId, String adCode) {
        adCode = "MobileHome_" + adCode;
        return sInstance.getCdnService().getLayoutTemplateData(shopId, adCode)
                .compose(ObservableUtils.<ArrayList<LayoutTemplateData>>schedulersHandling());
    }

    public static Flowable<SalePageListReturnCode> getSalePageListByShopCategory(int shopId, int categoryId, int maxCount, String orderby,
                                                                                   int startIndex, boolean isCuratorable) {
        return sInstance.getCdnService().getSalePageListByShopCategory(shopId, categoryId, maxCount, orderby, startIndex, isCuratorable)
                .compose(ObservableUtils.<SalePageListReturnCode>schedulersHandling());
    }

    public static Flowable<InfoModuleRecommandation> getInfoModuleRecommandation(int shopId) {
        return sInstance.getWebApiService().getInfoModuleRecommandation(shopId)
                .compose(ObservableUtils.<InfoModuleRecommandation>schedulersHandling());
    }

    public static Flowable<FBLogin> getFBToken(String clientId, String clientSecret) {
        String grantType = "client_credentials";
        return sInstance.getFacebookApiService().getFBToken(clientId, clientSecret, grantType)
                .compose(ObservableUtils.<FBLogin>schedulersHandling());
    }

    public static Flowable<FBContainer> getFBData(String fbId, String accessToken) {
        return sInstance.getFacebookApiService().getFBData(fbId, accessToken)
                .compose(ObservableUtils.<FBContainer>schedulersHandling());
    }

    public static Flowable<String> getFBPhotos(String photoId, String accessToken) {
        return sInstance.getFacebookApiService().getFBPhotos(photoId, accessToken)
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<ShopShipping> getShopShippingDetailList(int shopId, String appVer) {
        return sInstance.getWebApiService().getShopCategoryListBySearch(shopId, appVer)
                .compose(ObservableUtils.<ShopShipping>schedulersHandling());
    }

    public static Flowable<CityAreaListRoot> getCityAreaList(int shopId) {
        return sInstance.getWebApiService().getCityAreaList(shopId)
                .compose(ObservableUtils.<CityAreaListRoot>schedulersHandling());
    }

    public static Flowable<LocationListRoot> getLocationList(int shopId, Double latitude, Double longitude) {
        return sInstance.getWebApiService().getLocationList(shopId, latitude, longitude)
                .compose(ObservableUtils.<LocationListRoot>schedulersHandling());
    }

    public static Flowable<LocationListByCityRoot> getLocationListByArea(int shopId, int areaId, int startIndex,
            int maxCount) {
        return sInstance.getWebApiService().getLocationListByArea(shopId, areaId, startIndex, maxCount)
                .compose(ObservableUtils.<LocationListByCityRoot>schedulersHandling());
    }

    public static Flowable<LocationListByCityRoot> getOverseaLocationList(int shopId, int startIndex, int maxCount) {
        return sInstance.getWebApiService().getOverseaLocationList(shopId, startIndex, maxCount)
                .compose(ObservableUtils.<LocationListByCityRoot>schedulersHandling());
    }

    public static Flowable<LocationListByCityRoot> getLocationListByCity(int shopId, int cityId, int startIndex,
            int maxCount) {
        return sInstance.getWebApiService().getLocationListByCity(shopId, cityId, startIndex, maxCount)
                .compose(ObservableUtils.<LocationListByCityRoot>schedulersHandling());
    }

    public static Flowable<ShopIntroduction> getShopIntroduction(int shopId) {
        return sInstance.getCdnService().getShopIntroduction(shopId)
                .compose(ObservableUtils.<ShopIntroduction>schedulersHandling());
    }

    public static Flowable<NotifyProfileReturnCode> getAllAppPhshProfileDataV2(String guid) {
        return sInstance.getWebApiService().getAllAppPhshProfileDataV2(guid)
                .compose(ObservableUtils.<NotifyProfileReturnCode>schedulersHandling());
    }

    public static Flowable<ReturnCode> setAPPPushProfileDataV2(NotifyProfileReturnCode profileReturnCode) {
        NotifyProfileInputData item = new NotifyProfileInputData();
        item.appPushProfileDataEntites = profileReturnCode.Data.APPPushProfileList;
        return sInstance.getWebApiService().setAPPPushProfileDataV2(item)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<Announcement> getAppAnnouncementList(int shopId, String platform, String appVer,
            String osVer) {
        String osVersion = "Android_" + osVer;
        return sInstance.getCdnService().getAppAnnouncementList(shopId, platform, appVer, osVersion)
                .compose(ObservableUtils.<Announcement>schedulersHandling());
    }

    public static Flowable<ShopStatus> checkAppIsEnable(int shopId, String osType) {
        return sInstance.getWebApiService().checkAppIsEnable(shopId, osType)
                .compose(ObservableUtils.<ShopStatus>schedulersHandling());
    }

    public static Flowable<Integer> traceSalePageList(int shopId) {
        return sInstance.getWebApiService().traceSalePageList(shopId)
                .compose(ObservableUtils.<Integer>schedulersHandling());
    }

    public static Flowable<ActivityList> getActivityList(int shopId) {
        return sInstance.getWebApiService().getActivityList(shopId)
                .compose(ObservableUtils.<ActivityList>schedulersHandling());
    }

    public static Flowable<ShopCategoryList> getShopCategoryList(int shopId) {
        return sInstance.getCdnService().getShopCategoryList(shopId)
                .compose(ObservableUtils.<ShopCategoryList>schedulersHandling());
    }

    public static Flowable<LocationRefereeSetting> getAppRefereeProfile(int shopId) {
        return sInstance.getWebApiService().getAppRefereeProfile(shopId)
                .compose(ObservableUtils.<LocationRefereeSetting>schedulersHandling());
    }

    public static Flowable<PhpCouponList> couponList(int shopId) {
        return sInstance.getApi2Service().couponList(shopId)
                .compose(ObservableUtils.<PhpCouponList>schedulersHandling());
    }

    public static Flowable<SalePageRealTimeDatum> getSalePageRealTimeData(int productId) {
        return sInstance.getWebApiService().getSalePageRealTimeData(productId)
                .compose(ObservableUtils.<SalePageRealTimeDatum>schedulersHandling());
    }

    public static Flowable<SalePageHotList> getSalePageHotListByShopCategoryId(int categoryId, String orderBy, int salePageId) {
        return sInstance.getCdnService()
                .getSalePageHotListByShopCategoryId(categoryId, orderBy, salePageId)
                .compose(ObservableUtils.<SalePageHotList>schedulersHandling());

    }

    public static Flowable<SalePageMoreInfo> getSalePageMoreInfo(int productId) {
        return sInstance.getCdnService().getSalePageMoreInfo(productId)
                .compose(ObservableUtils.<SalePageMoreInfo>schedulersHandling());

    }

    public static Flowable<SalePageListByIds> getSalepageDataByIds(ArrayList<Integer> ids) {
        return sInstance.getWebApiService().getSalepageDataByIds(convertToIntString(ids))
                .compose(ObservableUtils.<SalePageListByIds>schedulersHandling());
    }

    public static Flowable<RewardPointDetailRoot> getRewardPointDetail(int rewardId) {
        return sInstance.getWebApiService().getRewardPointDetail(rewardId)
                .compose(ObservableUtils.<RewardPointDetailRoot>schedulersHandling());
    }

    public static Flowable<MemberRewardPointRoot> getMemberRewardPoint(int rewardId) {
        return sInstance.getWebApiService().getMemberRewardPoint(rewardId)
                .compose(ObservableUtils.<MemberRewardPointRoot>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> createFacebookMemberRegisterRequest(String token, String cellPhone,
            int shopId) {
        return sInstance.getLoginApiService().createFacebookMemberRegisterRequest(token, cellPhone, shopId)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> createNineYiMemberRegisterRequest(String cellPhone, int shopId) {
        return sInstance.getLoginApiService().createNineYiMemberRegisterRequest(cellPhone, shopId)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> getFacebookMemberRegisterStatus(String token, int shopId) {
        return sInstance.getLoginApiService().getFacebookMemberRegisterStatus(token, shopId)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> getNineYiMemberRegisterStatus(String cellPhone, int shopId) {
        return sInstance.getLoginApiService().getNineYiMemberRegisterStatus(cellPhone, shopId)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> loginFacebookMember(@Query("token") String token,
            @Query("shopId") int shopId, @Query("source") String source, @Query("device") String device,
            @Query("appVer") String appVer) {
        return sInstance.getLoginApiService().loginFacebookMember(token, shopId, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<String> getMemberRewardPoint() {
        return sInstance.getWebApiService().mergeMemberFavorites()
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<TraceSalePageList> getDataList(int shopId) {
        return sInstance.getWebApiService().getDataList(shopId)
                .compose(ObservableUtils.<TraceSalePageList>schedulersHandling());
    }

    public static Flowable<LoginThirdPartyReturnCode> getThirdpartyMemberRegisterStatus(
            @Query("loginId") String loginId, @Query("password") String password, @Query("shopId") int shopId) {
        return sInstance.getLoginApiService().getThirdpartyMemberRegisterStatus(loginId, password, shopId)
                .compose(ObservableUtils.<LoginThirdPartyReturnCode>schedulersHandling());
    }

    public static Flowable<ReturnCode> createThirdpartyMemberRegisterRequest(@Query("authSessionToken") String token,
            @Query("cellPhone") String cellPhone, @Query("shopId") int shopId) {
        return sInstance.getLoginApiService().createThirdpartyMemberRegisterRequest(token, cellPhone, shopId)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> createNineYiMemberResetPasswordRequest(String cellPhone, int shopId) {
        return sInstance.getLoginApiService().createNineYiMemberResetPasswordRequest(cellPhone, shopId)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> loginNineYiMember(@Query("cellPhone") String cellPhone,
            @Query("password") String password, @Query("shopId") int shopId, @Query("source") String source,
            @Query("device") String device, @Query("appVer") String appVer) {
        return sInstance.getLoginApiService().loginNineYiMember(cellPhone, shopId, password, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> getPhoneDialVerifyStatus(String verifyType) {
        return sInstance.getLoginApiService().getPhoneDialVerifyStatus(verifyType)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<ThirdPartyAuthInfoRoot> getShopThirdpartyAuthInfo(int shopId, String device) {
        return sInstance.getLoginApiService().getShopThirdpartyAuthInfo(shopId, device)
                .compose(ObservableUtils.<ThirdPartyAuthInfoRoot>schedulersHandling());
    }

    public static Flowable<VipMemberBenefitsModel> getVipMemberBenefits(int shopId) {
        return sInstance.getWebApiService().getVipMemberBenefits(shopId)
                .compose(ObservableUtils.<VipMemberBenefitsModel>schedulersHandling());
    }

    public static Flowable<LoginThirdPartyReturnCode> getThirdpartyMemberRegisterStatusWithToken(int shopId,
            String accessToken) {
        return sInstance.getLoginApiService().getThirdpartyMemberRegisterStatusWithToken(shopId, accessToken)
                .compose(ObservableUtils.<LoginThirdPartyReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> loginThirdpartyMember(String token, int shopId, String source,
            String device, String appVer) {
        return sInstance.getLoginApiService().loginThirdpartyMember(token, shopId, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> resendVerifyCode(String cellPhone, int shopId, String memberType,
            String verifyType) {
        return sInstance.getLoginApiService().resendVerifyCode(cellPhone, shopId, memberType, verifyType)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> confirmFacebookMemberVerifyCode(String cellPhone, int shopId, String code,
            String token, String source, String device, String appVer) {
        return sInstance.getLoginApiService()
                .confirmFacebookMemberVerifyCode(cellPhone, shopId, code, token, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> confirmFacebookMemberPhoneDialVerify(String cellPhone, int shopId,
            String token, String source, String device, String appVer) {
        return sInstance.getLoginApiService()
                .confirmFacebookMemberPhoneDialVerify(cellPhone, shopId, token, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> confirmNineYiMemberVerifyCode(String cellPhone, int shopId, String code,
            String verifyType) {
        return sInstance.getLoginApiService().confirmNineYiMemberVerifyCode(cellPhone, shopId, code, verifyType)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> confirmNineYiMemberPhoneDialVerify(String cellPhone, int shopId,
            String verifyType) {
        return sInstance.getLoginApiService().confirmNineYiMemberPhoneDialVerify(cellPhone, shopId, verifyType)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> confirmThirdpartyMemberVerifyCode(String cellPhone, int shopId,
            String token, String code, String source, String device, String appVer) {
        return sInstance.getLoginApiService()
                .confirmThirdpartyMemberVerifyCode(cellPhone, shopId, token, code, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> confirmThirdpartyMemberPhoneDialVerify(String cellPhone, int shopId,
            String token, String source, String device, String appVer) {
        return sInstance.getLoginApiService()
                .confirmThirdpartyMemberPhoneDialVerify(cellPhone, shopId, token, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> finishNineYiMemberResetPassword(String cellPhone, int shopId,
            String password, String source, String device, String appVer) {
        return sInstance.getLoginApiService()
                .finishNineYiMemberResetPassword(cellPhone, shopId, password, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> finishNineYiMemberRegister(String cellPhone, int shopId, String password,
            String source, String device, String appVer) {
        return sInstance.getLoginApiService()
                .finishNineYiMemberRegister(cellPhone, shopId, password, source, device, appVer)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<RefereeEmployeeList> getLocationEmployeeList(int shopId, int locationId) {
        return sInstance.getWebApiService().getLocationEmployeeList(shopId, locationId)
                .compose(ObservableUtils.<RefereeEmployeeList>schedulersHandling());
    }

    public static Flowable<RefereeInsert> insertAppReferee(String guid, int shopId, int locationId, Integer empId,
            boolean isRequireLogin) {
        return sInstance.getWebApiService().insertAppReferee(guid, shopId, locationId, empId, isRequireLogin)
                .compose(ObservableUtils.<RefereeInsert>schedulersHandling());
    }

    public static Flowable<ECouponMemberList> getMemberECouponList(int shopId){
        return sInstance.getECouponService().getMemberECouponList(shopId)
                .compose(ObservableUtils.<ECouponMemberList>schedulersHandling());
    }

    public static Flowable<ECouponVerify> setECouponVerify(int shopId, int ecouponId){
        return sInstance.getECouponService().setECouponVerify(shopId, ecouponId)
                .compose(ObservableUtils.<ECouponVerify>schedulersHandling());
    }

    public static Flowable<ECouponIncludeDetail> getECouponDetail(@Query("id") int ecouponId){
        return sInstance.getECouponService().getECouponDetail(ecouponId)
                .compose(ObservableUtils.<ECouponIncludeDetail>schedulersHandling());
    }

    public static Flowable<ECouponStatusList> getMemberECouponStatusList(String eCouponIdListString, String fbId){
        return sInstance.getECouponService().getMemberECouponStatusList(eCouponIdListString, fbId)
                .compose(ObservableUtils.<ECouponStatusList>schedulersHandling());
    }

    public static Flowable<ReturnCode> setMemberECouponByECouponId(int ecouponId, String guid){
        return sInstance.getECouponService().setMemberECouponByECouponId(ecouponId, guid)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<ReturnCode> setMemberFirstDownloadECouponByECouponId(int ecouponId, String guid){
        return sInstance.getECouponService().setMemberFirstDownloadECouponByECouponId(ecouponId, guid)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<LocationWizardMemberInfoRoot> getMemberInfo(){
        return sInstance.getWebApiService().getMemberInfo()
                .compose(ObservableUtils.<LocationWizardMemberInfoRoot>schedulersHandling());
    }

    public static Flowable<LocationWizardBeaconInfoRoot> addLocationMemberRequest(String uuid, String major,
            String minor, String locationId, String memberId, String requestType, int couponId, int userCouponId) {
        return sInstance.getWebApiService()
                .addLocationMemberRequest(uuid, major, minor, locationId, memberId, requestType, couponId, userCouponId)
                .compose(ObservableUtils.<LocationWizardBeaconInfoRoot>schedulersHandling());
    }

    public static Flowable<ActivityDetail> getActivityDetail(int activityId, int shopId){
        return sInstance.getWebApiService().getActivityDetail(activityId, shopId)
                .compose(ObservableUtils.<ActivityDetail>schedulersHandling());
    }

    public static Flowable<String> deleteItem(int salePageId, int shopId){
        return sInstance.getWebApiService().deleteItem(salePageId, shopId)
                .compose(ObservableUtils.<String>schedulersHandling());
    }

    public static Flowable<InfoModuleArticleDetail> getArticleDetail(int articleId){
        return sInstance.getWebApiService().getArticleDetail(articleId)
                .compose(ObservableUtils.<InfoModuleArticleDetail>schedulersHandling());
    }

    public static Flowable<InfoModuleAlbumDetail> getAlbumDetail(int albumId){
        return sInstance.getWebApiService().getAlbumDetail(albumId)
                .compose(ObservableUtils.<InfoModuleAlbumDetail>schedulersHandling());
    }

    public static Flowable<InfoModuleVideoDetail> getVideoDetail(int videoId){
        return sInstance.getWebApiService().getVideoDetail(videoId)
                .compose(ObservableUtils.<InfoModuleVideoDetail>schedulersHandling());
    }

    public static Flowable<ReturnCode> bindingShopLocationVIPMember(VipMemberPostData postData){
        return sInstance.getWebApiService().bindingShopLocationVIPMember(postData)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<PhpCouponList> couponDetail(int couponId){
        return sInstance.getApi2Service().couponDetail(couponId)
                .compose(ObservableUtils.<PhpCouponList>schedulersHandling());
    }

    public static Flowable<PhpCouponTakeStatus> takeCoupon(int couponId){
        return sInstance.getApi2Service().takeCoupon(couponId)
                .compose(ObservableUtils.<PhpCouponTakeStatus>schedulersHandling());
    }

    public static Flowable<PhpCouponUseStatus> useCoupon(int couponId, Double lat, Double lon){
        return sInstance.getApi2Service().useCoupon(couponId, lat, lon)
                .compose(ObservableUtils.<PhpCouponUseStatus>schedulersHandling());
    }

    public static Flowable<ReturnCode> setMemberECouponByCode(int shopId, String code, String guid){
        return sInstance.getECouponService().setMemberECouponByCode(shopId, code, guid)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<VipMemberItemData> getVIPMemberItem(int shopId, long memberCardId,
            int isBinding, String appVer){
        return sInstance.getWebApiService().getVIPMemberItem(shopId, memberCardId, isBinding, appVer, true)
                .compose(ObservableUtils.<VipMemberItemData>schedulersHandling());
    }

    public static Flowable<ThirdPartyTradeOrderRoot> getThirdPartyTradesOrderConfiguration(int shopId){
        return sInstance.getWebApiService().getThirdPartyTradesOrderConfiguration(shopId)
                .compose(ObservableUtils.<ThirdPartyTradeOrderRoot>schedulersHandling());
    }

    public static Flowable<PresentStatus> getNonVIPOpenCardPresentStatus(int shopId,  String appVer){
        return sInstance.getWebApiService().getNonVIPOpenCardPresentStatus(shopId, appVer)
                .compose(ObservableUtils.<PresentStatus>schedulersHandling());
    }

    public static Flowable<PresentStatus> getOpenCardPresentStatus( int shopId,  String appVer){
        return sInstance.getWebApiService().getOpenCardPresentStatus(shopId, appVer)
                .compose(ObservableUtils.<PresentStatus>schedulersHandling());
    }

    public static Flowable<PresentStatus> getBirthdayPresentStatus( int shopId,  String appVer){
        return sInstance.getWebApiService().getBirthdayPresentStatus(shopId, appVer)
                .compose(ObservableUtils.<PresentStatus>schedulersHandling());
    }

    public static Flowable<LoginReturnCode> changeNineYiMemberPassword( String oldPassword,  String newPassword,  int shopId){
        return sInstance.getLoginApiService().changeNineYiMemberPassword(oldPassword, newPassword, shopId)
                .compose(ObservableUtils.<LoginReturnCode>schedulersHandling());
    }

    public static Flowable<VipShopInfo> getVipShopInfo(int shopId){
        return sInstance.getWebApiService().getVipShopInfo(shopId)
                .compose(ObservableUtils.<VipShopInfo>schedulersHandling());
    }

    public static Flowable<ReturnCode> registerVIPMember(VipMemberPostData postData){
        return sInstance.getWebApiService().registerVIPMember(postData)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<ReturnCode> insertOrUpdateVIPMember(VipMemberPostData postData){
        return sInstance.getWebApiService().insertOrUpdateVIPMember(postData)
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }

    public static Flowable<ArticleModule> getArticleList(int shopId, int startIndex, int maxCount){
        return sInstance.getWebApiService().getArticleList(shopId, startIndex, maxCount)
                .compose(ObservableUtils.<ArticleModule>schedulersHandling());
    }

    public static Flowable<AlbumModule> getAlbumList(int shopId, int startIndex, int maxCount){
        return sInstance.getWebApiService().getAlbumList(shopId, startIndex, maxCount)
                .compose(ObservableUtils.<AlbumModule>schedulersHandling());
    }

    public static Flowable<VideoModule> getVideoList(int shopId, int startIndex, int maxCount){
        return sInstance.getWebApiService().getVideoList(shopId, startIndex, maxCount)
                .compose(ObservableUtils.<VideoModule>schedulersHandling());
    }

    public static Flowable<RefereeLocationList> getAppRefereeLocationList(int shopId){
        return sInstance.getWebApiService().getAppRefereeLocationList(shopId)
                .compose(ObservableUtils.<RefereeLocationList>schedulersHandling());
    }

    public static Flowable<FreeGiftSalePage> getIsGiftSalePage(int salePageId){
        return sInstance.getWebApiService().getIsGiftSalePage(salePageId)
                .compose(ObservableUtils.<FreeGiftSalePage>schedulersHandling());
    }

    public static Flowable<SalePagePrice> getSalePagePrice(int productId){
        return sInstance.getWebApiService().getSalePagePrice(productId)
                .compose(ObservableUtils.<SalePagePrice>schedulersHandling());
    }

    public static Flowable<ArrayList<SellingQty>> getSellingQtyListNew(ArrayList<Integer> ids){
        SkuPostRawData skuPostRawData = new SkuPostRawData(convertToIntString(ids));
        return sInstance.getWebApiService().getSellingQtyListNew(skuPostRawData)
                .compose(ObservableUtils.<ArrayList<SellingQty>>schedulersHandling());
    }

    public static Flowable<DONETLbsList> getLbsList(int shopId, Double lat, Double lon){
        return sInstance.getWebApiService().getLbsList(shopId, lat, lon)
                .compose(ObservableUtils.<DONETLbsList>schedulersHandling());
    }

    public static Flowable<ECouponShopECouponList> getECouponListWithoutCode(int shopId){
        return sInstance.getECouponService().getECouponListWithoutCode(shopId)
                .compose(ObservableUtils.<ECouponShopECouponList>schedulersHandling());
    }

    public static Flowable<RewardPointRoot> getRewardPointList(int shopId){
        return sInstance.getWebApiService().getRewardPointList(shopId)
                .compose(ObservableUtils.<RewardPointRoot>schedulersHandling());
    }

    public static Flowable<ArrayList<NotifyMessage>> getFrontendList( int shopId,  int startIndex,  int maxCount){
        return sInstance.getWebApiService().getFrontendList(shopId, startIndex, maxCount)
                .compose(ObservableUtils.<ArrayList<NotifyMessage>>schedulersHandling());
    }

    public static Flowable<PhpCouponUseStatus> getCouponSerialNumber(int couponId){
        return sInstance.getApi2Service().getCouponSerialNumber(couponId)
                .compose(ObservableUtils.<PhpCouponUseStatus>schedulersHandling());
    }

    public static Flowable<PhpCouponList> getMyCoupon(int shopId){
        return sInstance.getApi2Service().getMyCoupon(shopId)
                .compose(ObservableUtils.<PhpCouponList>schedulersHandling());
    }

    public static Flowable<CrmMemberTierData> getCrmMemberTier(int shopId) {
        return sInstance.getWebApiService().getCrmMemberTier(shopId).compose(ObservableUtils.<CrmMemberTierData>schedulersHandling());
    }

    public static Flowable<CrmShopMemberCard> getCrmShopMemberCardInfo(int shopId) {
        return sInstance.getWebApiService().getCrmShopMemberCardInfo(shopId).compose(ObservableUtils.<CrmShopMemberCard>schedulersHandling());
    }

    public static Flowable<ShopContractSetting> getShopContractSetting(int shopId) {
        return sInstance.getCdnService().getShopContractSetting(shopId).compose(ObservableUtils.<ShopContractSetting>schedulersHandling());
    }

    public static Flowable<EmailNotification> getEmailNotificationSetting(int shopId) {
        return sInstance.getWebApiService().getEmailNotificationSetting(shopId).compose(ObservableUtils.<EmailNotification>schedulersHandling());
    }

    public static Flowable<UpdateEmailNotification> updateEmailNotificationSetting(int shopId, EmailNotificationData data) {
        return sInstance.getWebApiService().updateEmailNotificationSetting(shopId, data).compose(ObservableUtils.<UpdateEmailNotification>schedulersHandling());
    }

    public static Flowable<ShopInformation> getShopInformation(int shopId, String appVer) {
        return sInstance.getWebApiService().getShopInformation(shopId, appVer).compose(ObservableUtils.<ShopInformation>schedulersHandling());
    }

    public static Flowable<String> trackCollect(Map<String, String> maps){
        return sInstance.getTrackService().collect(maps).compose(ObservableUtils.<String>schedulersHandling());
    }

    private static String convertToIntString(List<Integer> list) {
        return list.toString().replaceAll("[\\s\\[\\]]", ""); // Remove space, [, ]
    }

    public static Flowable<PromotionDiscount> getListForCrmMemberTier(int shopId, int memberCardId) {
        return sInstance.getWebApiService().getListForCrmMemberTier(shopId, memberCardId)
                .compose(ObservableUtils.<PromotionDiscount>schedulersHandling());
    }

    public static Flowable<SalePageWrapper> getSalePageV2AndAdditionalInfo(int shopId, int salePageId) {
        return Flowable.zip(sInstance.getCdnService().getSalePageV2Info(shopId, salePageId),
                sInstance.getCdnService().getSalePageAdditionalInfo(shopId, salePageId),
                new BiFunction<SalePageV2Info, SalePageAdditionalInfo, SalePageWrapper>() {
                    @Override
                    public SalePageWrapper apply(@NonNull SalePageV2Info salePageV2Info,
                            @NonNull SalePageAdditionalInfo salePageAdditionalInfo) throws Exception {
                        return new SalePageWrapper(salePageV2Info, salePageAdditionalInfo);
                    }
                }).compose(ObservableUtils.<SalePageWrapper>schedulersHandling());
    }

    public static Flowable<SalePageV2Info> getSalePageV2Info(int shopId, int salePageId) {
        return sInstance.getCdnService().getSalePageV2Info(shopId, salePageId).compose(ObservableUtils.<SalePageV2Info>schedulersHandling());
    }


    public static Flowable<ShippingStatus> getShippingStatusForUser(int shopId) {
        return sInstance.getWebApiService().getShippingStatusForUser(shopId)
                .compose(ObservableUtils.<ShippingStatus>schedulersHandling());
    }

    public static Flowable<ReturnCode> checkLoginMemberStatus(){
        return sInstance.getWebApiService().checkLoginMemberStatus()
                .compose(ObservableUtils.<ReturnCode>schedulersHandling());
    }
}
