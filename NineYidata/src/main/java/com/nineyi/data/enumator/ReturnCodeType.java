package com.nineyi.data.enumator;


public enum ReturnCodeType {

    API0000, // API資料正確準備執行
    API0001, // API資料正確執行完成
    API0002, // API資料格式錯誤
    API0003, // API資料Signature不正確
    API0004, // API0004
    API0006, // API0006
    API0007, // API0007
    API0009,
    API2001, //已經領取
    API2002,
    API5001,
    API5002,
    API5003,
    API5004,
    API5009,
    API5011,
    API5012,
    API5019,
    API5101,
    API5102,
    API5109


}
