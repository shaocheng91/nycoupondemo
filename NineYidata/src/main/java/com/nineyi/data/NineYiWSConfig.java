package com.nineyi.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.nineyi.data.gson.ConditionDiscountTypeEnumAdapter;
import com.nineyi.data.gson.ConditionTypeEnumAdapter;
import com.nineyi.data.gson.NineYiDateDeserializer;
import com.nineyi.data.gson.NineYiDateSerializer;
import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.promote.ConditionDiscountTypeEnum;
import com.nineyi.promote.ConditionTypeEnum;


public class NineYiWSConfig {

    static AppBuildConfig sAppConfig = AppBuildConfig.createDefault();

    //    private static boolean sInitialized;

    private NineYiWSConfig() {
        // No public constructor
    }

    public static void initialize(AppBuildConfig config) {
        sAppConfig = config;
    }

    public static AppBuildConfig getAppBuildConfig() {
        return sAppConfig;
    }

    public static Gson sGson;

    static {
        sGson = createDefaultGsonBuilder()
                    .create();
    }

    public static GsonBuilder createDefaultGsonBuilder() {
        return new GsonBuilder()
                .registerTypeAdapter(NineyiDate.class, new NineYiDateDeserializer())
                .registerTypeAdapter(NineyiDate.class, new NineYiDateSerializer())
                .registerTypeAdapter(ConditionTypeEnum.class, new ConditionTypeEnumAdapter())
                .registerTypeAdapter(ConditionDiscountTypeEnum.class, new ConditionDiscountTypeEnumAdapter())
                .serializeNulls();
    }
    public static final String PLATFORM_ID_ANDROID = "Android";

    public static final String HTTP_SCHEME = "http";
    public static final String HTTPS_SCHEME = "https";

    public static final String HTTPS_WITH_COLON = HTTPS_SCHEME + ":";

    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";

    public static final String ROOT_PATH = "/";

    public static final String MIME_JSON = "application/json";
    public static final String MIME_TEXT_HTML = "text/html";

//    public static final String WEB_FB_LOGIN = HTTPS + "m.facebook.com/dialog/oauth?client_id=";
    public static final String FACEBOOK_DOMAIN = "https://graph.facebook.com/v2.10/";
    public static final String FACEBOOK_PICTURE_PATH_WITH_FORMAT = FACEBOOK_DOMAIN + "%s/picture?type=small";

    public static final String HTTP_GOO_GL = "http://goo.gl/";
    public static final String HTTPS_GOO_GL = "https://goo.gl/";

//    public static final String SCHEMA_LOGIN_SUCCESS = "loginsuccess://";
    public static final String SCHEMA_GOOGLE_PLAY = "\\S+play.google.com\\S+|market://\\S+";
//    public static final String FB_PERMISSION_EMAIL = "email";

    public static final String API_PATH_PREFIX = "/o2o";// /o2o, /APITest/api/o2o
    public static final String WEB_API = "/webapi";

    // URI for cookies

    public static final String getWebServerUri() {
        return HTTPS + sAppConfig.webServerHostName + ROOT_PATH;
    }

    public static final String getAppServerUri() {
        return HTTPS + sAppConfig.appServerHostName + ROOT_PATH;
    }

    public static final String getAppServerNoSlashUri() {
        return HTTPS + sAppConfig.appServerHostName;
    }

    public static final String getApiServerUri() {
        return HTTPS + sAppConfig.apiServerHostName + ROOT_PATH;
    }

    public static final String getPhpServerUri() {
        return HTTPS + sAppConfig.api2ServerHostName + ROOT_PATH;
    }

    public static final String getEcouponServerUri() {
        return HTTPS + sAppConfig.ecouponServerHostName + ROOT_PATH;
    }

    public static final boolean hasLocationMemberModule() {
        return sAppConfig.hasLocationMemberModule;
    }
    
    /*
     *  URL
     */

    public static final String GOOGLE_PLAY_APP_DOWNLOAD_URL_PREFIX = "market://details?id=";

    static final public String GOOGLE_PLAY_WEB_DOWNLOAD_URL_PREFIX = "https://play.google.com/store/apps/details?id=";

    /**
     * add new friend to line for http or https url scheme
     */
    public static final String APP_LINE_ADD_FRIENDS_URL = "http://line.naver.jp/ti/p/";
    public static final String APP_LINE_ADD_FRIENDS_HTTPS_URL = "https://line.me/R/ti";
    public static final String APP_LINE_PACKAGE_NAME = "package=jp.naver.line.android";

    private static final String WEB_PARAMS_SHOP = "utm_source=ShopApp&utm_medium=s%06d&utm_campaign=Android&act=s%06dA";
    private static final String WEB_PARAMS_MALL = "utm_source=MallApp&utm_medium=Mall&utm_campaign=Android&act=MallAppA";

    // general parameter list format
    public static final String getWebParams() {
        if (sAppConfig.isMallApp()) {
            return WEB_PARAMS_MALL;
        } else {
            return String.format(WEB_PARAMS_SHOP, sAppConfig.shopId, sAppConfig.shopId);
        }
    }

    // WebView 全部 URL 整理 -> http://portal.nexdoor.cc/pages/viewpage.action?pageId=49319985

    // Mall URL

    // 商城首頁
    public static final String getWebMallFrontPageUrl() {
        return HTTPS + sAppConfig.appServerHostName + ROOT_PATH + "?" + getWebParams();
    }


    public static final String WEB_91_OPEN_SHOP_URL = HTTP + "tw.91app.com/act/openshop/intro.html#form";

    // 商城服務介紹
//    public String WEB_MALL_SERVICE_INTRO_URL = HTTPS + sAppConfig.appServerHostName + "/Help/ServiceInfo?" + WEB_PARAMS;

    //商城介紹，商城專用
    public static final String getMallAboutUrl() {
        return HTTP + sAppConfig.appServerHostName + "/Help/About?" + getWebParams();
    }

    // 客服中心
    public static final String getWebCustomerServiceCenterUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/Question/CustomerServiceCenter?"
                + "&shopId=" + sAppConfig.shopId + "&" + getWebParams();
    }


    public static final String getWebQuestionInsertUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/Question/QuestionInsert/0?"
                + "&sId=" + sAppConfig.shopId + "&" + getWebParams();
    }

    public static final String getCustomerSendQuestionUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/Question/QuestionInsert/0?"
                + "sId=" + sAppConfig.shopId;
    }

    // 商城我要開店
    public static final String getWebMallOpenShopUrl() {
        return HTTPS + "www.91app.com/Shop#/home/index?" + getWebParams();
    }

    // 無法註冊
    public static final String getWebCantRegisterUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/Login/RegisterInfo?" + "&shopId=" + sAppConfig.shopId;
    }

    // Shop URL
    public static final String getWebShopInfoBaseUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/shop/introduce/%d?t=%d&";
    }

    // for 商城裡的商店首頁「商店資訊」按鈕
    public static final String getWebShopServiceIntroUrl() {
        return String.format(getWebShopInfoBaseUrl(), sAppConfig.shopId, 1) + getWebParams();
    }

    // 客服表單
    public static final String getWebShopQuestionUrl() {
        return String.format(getWebShopInfoBaseUrl(), sAppConfig.shopId, 3) + getWebParams();
    }

    // 訂單查詢V2
    public static final String getWebMyTradesOrderListV2Url() {
        return HTTPS + sAppConfig.appServerHostName + "/V2/TradesOrder/TradesOrderList?"
                + "&shopId=" + sAppConfig.shopId;
    }

    // 舊官網訂單查詢導頁
    public static final String getWebThirdPartyTradesOrderListUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/V2/VIPMember/ThirdPartyTradesOrderRelay?"
                + "&shopId=" + sAppConfig.shopId + "&" + getWebParams();
    }

    // 收件人記事本
    public static final String getWebMyLocationBooksUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/MyAccount/LocationBooks?"
                + "&shopId=" + sAppConfig.shopId + "&" + getWebParams();
    }

    // 統編記事本
    public static final String getWebMyInvoiceBooksUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/MyAccount/InvoiceList?"
                + "&shopId=" + sAppConfig.shopId + "&" + getWebParams();
    }

    // 登入 Q&A
    public static final String getWebLoginInfoUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/Login/LoginInfo?"
                + "&shopId=" + sAppConfig.shopId + "&" + getWebParams();
    }

    // 訂單明細
    public static final String getWebMyTradesOrderDetailUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/TradesOrder/tradesOrderDetail?tid=%s&sid=%s&seq=%s&"
                + "&shopId=" + sAppConfig.shopId;
    }

    // 訂單明細V2
    public static final String getWebMyTradesOrderDetailV2Url(String tg) {
        return HTTPS + sAppConfig.appServerHostName + "/V2/TradesOrder/TradesOrderDetail/"+ tg;
    }

    // 發票明細
    public static final String getWebMyInvoiceDetailUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/Invoice/Invoice?ts=%s&tid=%s&sid=%s&seq=%s&src=%s&";
    }

    public static final String getWebMyInvoiceDetailV2Url(String params) {
        return HTTPS + sAppConfig.appServerHostName + "/V2/Invoice/InvoiceDetail/" + params;
    }

    // ECoupon How to use 領取方式、使用方式的說明
    public static final String getWebEcouponHowToUseUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/ecoupon/HowtoUseForAndroid";
    }

    // ECoupon How to use 僅有使用方式的說明
    public static final String getWebEcouponHowToUseNewUrl() {
        return HTTPS + sAppConfig.appServerHostName + "/ecoupon/HowtoUseNewForAndroid";
    }

    public static final String getOfficialPrivacyPath(int shopId) {
        return HTTPS + sAppConfig.appServerHostName + "/MyAccount/AppPrivacy?shopId=" + shopId;
    }

    // 精選app推薦
    public static final String getWebAppPromoteUrl() {
        return HTTP + sAppConfig.appServerHostName + "/act/newmall/newshop.html" ;
    }


    /**
     * URL for redirect webview to native fragment
     */

    // Open link with native view (these values must be LowerCase)

    public static final String WEB_PATH_SHOP_CATEGORY = "(/v2/official/|/shop/)salepagecategory/\\d+/{0,1}";
    public static final String WEB_PATH_SALE_PAGE = "(/v2/official/|/)salepage/index/\\d+/{0,1}";
    public static final String WEB_PATH_SHOP_HOME = "(/v2/official/\\d+/{0,1}|/\\d+/{0,1})";
    public static final String WEB_PATH_COUPON_LIST = "(/v2/official/|/)coupon/list/{0,1}";
    public static final String WEB_PATH_COUPON_DETAIL = "(/v2/official/|/)coupon/detail/\\d+/{0,1}";
    public static final String WEB_PATH_ARTICLE_DETAIL = "(/v2/official/|/)article/detail/\\d+/{0,1}";
    public static final String WEB_PATH_VIDEO_DETAIL = "(/v2/official/|/)video/detail/\\d+/{0,1}";
    public static final String WEB_PATH_ALBUM_DETAIL = "(/v2/official/|/)album/detail/\\d+/{0,1}";
    public static final String WEB_PATH_SEARCH = ".*/search";
    public static final String WEB_PATH_ECOUPON_LIST = "(/v2/official/|/)ecoupon/list/{0,1}";
    public static final String WEB_PATH_ECOUPON_DETAIL = "(/v2/official/|/)ecoupon/detail/\\d+/{0,1}";
    public static final String WEB_PATH_ECOUPON_MYECOUPON = "(/v2/official/|/)ecoupon/myecoupon/{0,1}";
    public static final String WEB_PATH_PROMOTION_PAGE = "(/v2/|/)promotion/detail/\\d+/{0,1}";

    public static final String WEB_PATH_SHOP = "/shop/";
    public static final String WEB_PATH_REG_SHOP = "/shop/\\d+/{0,1}";
    public static final String WEB_PATH_SHOP_HOME_WITH_ID = "/shop/home/\\d+/{0,1}";
    public static final String WEB_PATH_MEMBER_ZONE = "/v2/vipmember/profile/{0,1}";
    public static final String WEB_PATH_MALL_HOME = "(/v2/official/{0,1}|/{0,1})";
    public static final String WEB_PATH_MALL_CATEGORY = "/home/salepagecategory/\\d+/{0,1}";
    public static final String WEB_PATH_RANKING_MALL = "/hotsaleranking/\\w+/{0,1}";
    public static final String WEB_PATH_RANKING_SHOP = "/shop/hotsaleranking/";
    public static final String WEB_PATH_SHOP_STORELIST = "/shop/storelist/\\d+/{0,1}";
    public static final String WEB_PATH_ARTICLEDETAI = WEB_PATH_SHOP + "articledetail/";
    public static final String WEB_PATH_ARTICLEDETAIL_PAGE = WEB_PATH_ARTICLEDETAI + "\\d+/\\d+/{0,1}";
    public static final String WEB_PATH_VIDEODETAIL = WEB_PATH_SHOP + "videodetail/";
    public static final String WEB_PATH_VIDEODETAIL_PAGE = WEB_PATH_VIDEODETAIL + "\\d+/\\d+/{0,1}";
    public static final String WEB_PATH_ALBUMDETAIL = WEB_PATH_SHOP + "albumdetail/";
    public static final String WEB_PATH_ALBUMDETAIL_PAGE = WEB_PATH_ALBUMDETAIL + "\\d+/\\d+/{0,1}";
    public static final String WEB_PATH_ACTIVITY_DETAIL_PAGE = "/v2/activity/\\d+/{0,1}";
    public static final String WEB_PATH_INFOMODULE_LIST = "/v2/shop/infomodulelist";
    public static final String WEB_PATH_ARTICLE_LIST = "/articlelist/{0,1}";
    public static final String WEB_PATH_VIDEO_LIST = "/videolist/{0,1}";
    public static final String WEB_PATH_ALBUM_LIST = "/albumlist/{0,1}";
    public static final String WEB_PATH_TRADES_ORDER_LIST = "/tradesorder/tradesorderlist/{0,1}";
    public static final String WEB_PATH_QUESTION = "/question/customerservicecenter/{0,1}";
    public static final String WEB_PATH_QUESTION_INSERT = "/question/questioninsert/0";
    public static final String WEB_PATH_PROMOTION_LIST = "/v2/promotion/list/{0,1}";

    // ref ==============================

    public static final String WEB_PATH_REF_V2_REGEX = "(/v2/official|)";

    public static final String getWebPathRefShopHome() {
        return "/ref/" + sAppConfig.shopId;
    }

    public static final String getWebPathRefShopSalePage() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/salepage/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopMallCategory() {
        return getWebPathRefShopHome() + "/mallsalepagecategory/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopShopCategory() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/shopsalepagecategory/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopLocationList() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/locationlist/{0,1}";
    }

    public static final String getWebPathRefShopCoupon() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/coupon/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopCouponList() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/couponlist/{0,1}";
    }

    public static final String getWebPathRefShopArticle() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/article/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopVideo() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/video/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopAlbum() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/album/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopEcoupon() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/ecoupon/\\d+/{0,1}";
    }

    public static final String getWebPathRefShopEcouponList() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/ecouponlist/{0,1}";
    }

    public static final String getWebPathRefShopMyEcoupon() {
        return getWebPathRefShopHome() + "/myecoupon/{0,1}";
    }

    public static final String getWebPathRefRankingWeekly() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/hotsaleweekly/{0,1}";
    }

    public static final String getWebPathRefRankingDaily() {
        return getWebPathRefShopHome() + "/hotsaledaily/{0,1}";
    }

    public static final String getWebPathRegInfoModuleArticle() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/articlelist/{0,1}";
    }

    public static final String getWebPathRegInfoModuleVideo() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/videolist/{0,1}";
    }

    public static final String getWebPathRegInfoModuleAlbum() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/albumlist/{0,1}";
    }

    public static final String getWebPathRegPromotion() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/promotion/\\d+/{0,1}";
    }

    public static final String getWebPathRegActivity() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/activity/\\d+/{0,1}";
    }

    public static final String getWebPathRegTradesOrderList(){
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/tradesorderlist/{0,1}";
    }

    public static final String getWebPathRegQuestionList(){
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/questionlist/{0,1}";

    }

    public static final String getWebPathRegLocationList() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/locationlist/{0,1}";
    }

    public static final String getWebPathRegHotSaleWeekly() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/hotsaleweekly/{0,1}";
    }

    public static final String getWebPathRegArticleList() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/articlelist/{0,1}";
    }

    public static final String getWebPathRegVideoList() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/videolist/{0,1}";
    }

    public static final String getWebPathRegAlbumList() {
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/albumlist/{0,1}";
    }

    public static final String getWebPathRegLocationRewardPoint(){
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/locationrewardpoint/\\d+/{0,1}";
    }

    public static final String getWebPathRegPromotionList(){
        return WEB_PATH_REF_V2_REGEX + getWebPathRefShopHome() + "/promotionlist/{0,1}";
    }

//    public static final String WEB_PREFIX_SALE_PAGE_URL = HTTPS + sAppConfig.appServerHostName + WEB_PATH_SALE_PAGE;
//    public static final String WEB_PREFIX_SHOP_HOME_URL = HTTPS + sAppConfig.appServerHostName + WEB_PATH_SHOP_HOME_WITH_ID;
//    public static final String WEB_PREFIX_MALL_CATEGORY_URL = HTTPS + sAppConfig.appServerHostName
//            + WEB_PATH_MALL_CATEGORY;
//    public static final String WEB_PREFIX_SHOP_CATEGORY_URL = HTTPS + sAppConfig.appServerHostName
//            + WEB_PATH_SHOP_CATEGORY;

    public static final String WEB_FBURL = HTTP + "www.facebook.com/";
    /*
     * Web API
     */

    // category tree
    // 取得商店分類清單 (GET)
    public static final String getShopCategoryListUrl(String shopId) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/Shop/GetShopCategoryList/"+ shopId;
    }

    // 取得商城分類清單 (GET) 
    public static final String getMallCategoryListUrl() {
        return HTTP + sAppConfig.cdnServerHostName + "/webapi/Category/GetAllList";
    }

    // 取得商店廣告版位資訊 (GET)
    public static final String getLayoutTemplateDataUrl(String shopId, String adCode) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/LayoutTemplateData/GetLayoutTemplateData/" + shopId + "/" + adCode;
    }

    // 取得商店全部廣告版位資訊  (GET)
    public static final String getOfficialAllLayoutTemplateDataUrl(String shopId) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/LayoutTemplateData/GetShopHomeAllLoayoutTemplateData/"+shopId;
    }


    //category list
    // 取得商店分類商品列表 (GET)
    public static final String getShopSalePageListUrl(String categoryId) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/SalePage/GetSalePageListByShopCategory/" + categoryId;
    }

    // Search 商店自動完成 (GET)
    public static final String getSearchShopTermAutoCompleteUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Shop/GetShopTermListByKeyword";
    }

    public static final String WS_SEARCH_SHOP_TERM_AUTO_COMPLETE_PROPERTY_KEYWORD = "keyword";

    // Search 商品自動完成 (GET)
    public static final String getSearchSalePageTermAutoCompleteUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Search/GetShopSalePageTermListByKeyword";
    }

    public static final String WS_SEARCH_SALE_PAGE_TERM_AUTO_COMPLETE_PROPERTY_KEYWORD = "keyword";
    public static final String WS_SEARCH_SALE_PAGE_TERM_AUTO_COMPLETE_PROPERTY_SHOID = "shopId";


    // sale page

    // 商品詳細資訊 (GET)
    public static final String getSalePageUrl(String salePageId) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/SalePage/GetSalePage/" + salePageId;
    }

    public static final String getSalePageRealtimeDataUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/SalePage/GetSalePageRealTimeData/";
    }

    // 商品更多資訊 (POST)
    public static final String getSalePageMoreInfoUrl(String salePageId) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/SalePage/GetSalePageMoreInfo/" + salePageId;
    }

    // 商品價格 (POST)
    public static final String getSalePagePriceUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/SalePage/GetSalePagePrice/";
    }

    // 商品庫存列表 (POST)
    public static final String getSellingQtyListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/ProductStock/GetSellingQtyListNew/";
    }

    public static final String WS_SELLING_QTY_LIST_PROPERTY_SALE_PAGE_IDS = "ids";

    // 取得商品列表資料 - 以商品ID array方式 (POST)
    public static final String getSalePageListByIdsUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/SalePage/GetSalepageDataByIds/";
    }

    public static final String WS_SALE_PAGE_LIST_BY_IDS_PROPERTY_IDS = "ids";

    // 商店取得商品頁HotList - 最下方相關商品 (POST)
    public static final String getSalePageHotListShopUrl(String categoryId) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/SalePage/GetSalePageHotListByShopCategoryId/" + categoryId;
    }

    public static final String WS_SALE_PAGE_HOT_LIST_SHOP_PROPERTY_SALEPAGE_ID = "sid";
    public static final String WS_SALE_PAGE_HOT_LIST_SHOP_PROPERTY_ORDER_BY = "o";

    public static final String WS_SHOP_STATUS_PROPERTY_SHOP_ID = "shopid";

    // 商店資訊(LINE ID) (POST)
    public static final String getShopIntroductionUrl(String shopId) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/Shop/GetShopintroduction/"+ shopId;
    }

    // 合併購物車 & 收藏 (POST)
    public static final String getMergeMemberFavoritesUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Auth/MergeMemberFavorites";
    }

    // Notification

    // 裝置註冊 (POST)
    public static final String getAppRegisterUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/APPNotification/APPRegister/";
    }

    // 推播選項設定 (POST)
    public static final String getNotifyProfileSetUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/APPNotification/SetAPPPushProfileDataV2/";
    }

    // 取得 member 所有推播設定資料
    public static final String getNotifyProfileGetUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/APPNotification/GetAllAppPhshProfileDataV2/";
    }

    public static final String WS_NOTIFY_PROFILE_GET_PROPERTY_GUID = "GUID";

    // 更新 push token (POST)
    public static final String getUpdateTokenUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/APPNotification/UpdateToken/";
    }

    public static final String WS_NOTIFY_UPDATE_TOKEN_PROPERTY_GUID = "GUID";
    public static final String WS_NOTIFY_UPDATE_TOKEN_PROPERTY_TOKEN = "token";


    // 取得訊息清單 (POST)
    public static final String getNotificationFrontendListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/notificationcenter/getfrontendList/";
    }

    public static final String WS_NOTIFY_MESSAGE_LIST_PROPERTY_SHOP_ID = "shopId";
    public static final String WS_NOTIFY_MESSAGE_LIST_PROPERTY_START_INDEX = "startIndex";
    public static final String WS_NOTIFY_MESSAGE_LIST_PROPERTY_MAX_COUNT = "maxCount";

    // 取得訊息數量 (POST)
    public static final String getNotifyMessageCountUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/notificationcenter/getfrontendCount/";
    }

    public static final String WS_NOTIFY_MESSAGE_COUNT_PROPERTY_SHOP_ID = "shopId";

    // 商品收藏 - 新增 (POST)
    public static final String getTraceSalePageAddUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceSalePageList/InsertItem/";
    }

    public static final String WS_TRACE_SALE_PAGE_ADD_PROPERTY_SALE_PAGE_ID = "salePageId";
    public static final String WS_TRACE_SALE_PAGE_ADD_PROPERTY_SHOP_ID = "shopId";

    // 商品收藏 - 數量 (POST)
    public static final String getTraceSalePageCountUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceSalePageList/GetCount/";
    }

    public static final String WS_TRACE_SALE_PAGE_COUNT_PROPERTY_SHOP_ID = "shopId";

    // 商品收藏 - 列表 (POST)
    public static final String getTraceSalePageListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceSalePageList/GetDataList/";
    }

    public static final String WS_TRACE_SALE_PAGE_LIST_PROPERTY_SHOP_ID = "shopId";

    // 商品收藏 - 刪除 (POST)
    public static final String getTraceSalePageDelUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceSalePageList/DeleteItem/";
    }

    public static final String WS_TRACE_SALE_PAGE_DEL_PROPERTY_SALE_PAGE_ID = "salePageId";
    public static final String WS_TRACE_SALE_PAGE_DEL_PROPERTY_SHOP_ID = "shopId";

    // 商店收藏 - 新增 (POST)
    public static final String getTraceShopAddUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceShopList/InsertItem/";
    }

    public static final String WS_TRACE_SHOP_ADD_PROPERTY_SHOP_ID = "shopId";

    // 商店收藏 - 數量 (POST)
    public static final String getTraceShopCountUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceShopList/GetCount/";
    }

    // 商店收藏 - 列表 (POST)
    public static final String getTraceShopListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceShopList/GetDataList/";
    }

    // 商店收藏 - 刪除 (POST)
    public static final String getTraceShopDelUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/TraceShopList/DeleteItem/";
    }

    public static final String WS_TRACE_SHOP_DEL_PROPERTY_SHOP_ID = "shopId";

    // 門市小幫手 - 取得會員資料 (POST)
    public static final String getLocationWizardCouponMemberInfoUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Location/GetMemberInfo";
    }

    //public static final String WS_LOCATION_WIZARD_COUPON_MEMBER_INFO = "http://testapi.nexdoor.cc/APITest/api/Location/webapi/Location/GetMemberInfo";

    // 門市小幫手－ 接收使用iBeacon訊號的會員匹配請求 (POST)
    public static final String getLocationWizardBeaconInfoUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Location/AddLocationMemberRequest";
    }

    //public static final String WS_LOCATION_WIZARD_BEACON_INFO = "http://testapi.nexdoor.cc/APITest/api/Location/webapi/Location/AddLocationMemberRequest";
    public static final String WS_LOCATION_WIZARD_BEACON_UUID = "uuid";
    public static final String WS_LOCATION_WIZARD_BEACON_MAJOR = "major";
    public static final String WS_LOCATION_WIZARD_BEACON_MINOR = "minor";
    public static final String WS_LOCATION_WIZARD_BEACON_LOCATION_ID = "locationid";
    public static final String WS_LOCATION_WIZARD_BEACON_MEMBER_ID = "memberid";
    public static final String WS_LOCATION_WIZARD_REQUEST_TYPE = "requestType";
    public static final String WS_LOCATION_WIZARD_COUPON_ID = "couponId";
    public static final String WS_LOCATION_WIZARD_USERCOUPON_ID = "usercouponId";


    // shopping cart

    // 購物車：加入商品 (POST)
    public static final String getShoppingCartAddUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/ShoppingCartV2/InsertItem/";
    }

    public static final String WS_SHOPPING_CART_ADD_PROPERTY_SHOP_ID = "shopId";
    public static final String WS_SHOPPING_CART_ADD_PROPERTY_SALE_PAGE_ID = "salePageId";
    public static final String WS_SHOPPING_CART_ADD_PROPERTY_SALE_PRODUCT_SKU_ID = "saleProductSKUId";
    public static final String WS_SHOPPING_CART_ADD_PROPERTY_QTY = "qty";

    // 購物車：取得商品數 (POST)
    public static final String getShoppingCartCountUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/ShoppingCartV2/GetCount";
    }

    public static final String WS_SHOPPING_CART_COUNT_PROPERTY_SHOP_ID = "shopId";

    // APP 推薦 (POST)
    public static final String getPromoteAppHomeListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Shop/GetAppPromoteData/";
    }

    public static final String WS_PROMOTE_APP_HOME_LIST_PROPERTY_OS_TYPE = "OSType";

    // 取得折扣活動內容 (POST)
    public static final String getPromotionDetailUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Promotion/GetDetail";
    }

    // Free gift detail
    public static final String getPromotionFreeGiftSalePageUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/SalePage/GetIsGiftSalePage/";
    }

    public static final String WS_PROMOTION_DETAIL_PROPERTY_ID = "Id";


    // Server WebAPI status (GET)
    public static final String getCheckWebApiStatusUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/APPNotification/checkwebapistatus";
    }

    // Server WebAPI CDN Domain and BranchMetricsService switch on/off (Default off) (GET)
    public static final String getWebApiAppSettingsUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/APPNotification/GetMobileAppSettings";
    }

    // geofence 分店列表
    // test:/APITest/api/o2o_admin/api/lbs/

    // test:/{webapi_host}/webapi/Lbs
    public static final String getLbsListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Lbs/GetLbsList";
    }

    public static final String GEOFENCE_TYPE_LOCATION = "location";
    public static final String GEOFENCE_TYPE_COUPON = "coupon";

    // coupon all list
    public static final String getCouponListUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/coupon/list/";
    }

    // coupon my list
    public static final String getCouponMyListUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/coupon/my/";
    }

    // coupon detail
    public static final String getCouponDetailUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/coupon/detail/";
    }

    // coupon take
    public static final String getCouponTakeV2Url() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/v2/coupon/take/";
    }

    // coupon use
    public static final String getCouponUseUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/coupon/use/";
    }

    public static final String getCouponSerialNumberUrl(){
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/coupon/serialnumber/";
    }

    //-- 資訊模組 (info module)
    public static final String getPhpInfoModuleArticleListUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/article/list/";
    }

    public static final String getPhpInfoModuleVideoListUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/video/list/";
    }

    public static final String getPhpInfoModuleAlbumListUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + API_PATH_PREFIX + "/api/album/list/";
    }

    // (.Net) Infomodule
    public static final String getInfomoduleAlbumList() {
        return HTTPS + sAppConfig.apiServerHostName + WEB_API + "/InfoModuleV2/GetAlbumList";
    }

    public static final String getInfomoduleAlbumDetail() {
        return HTTPS + sAppConfig.apiServerHostName + WEB_API + "/InfoModuleV2/GetAlbumDetail";
    }

    public static final String getInfomoduleArticleList() {
        return HTTPS + sAppConfig.apiServerHostName + WEB_API + "/InfoModuleV2/GetArticleList";
    }

    public static final String getInfomoduleArticleDetail() {
        return HTTPS + sAppConfig.apiServerHostName + WEB_API + "/InfoModuleV2/GetArticleDetail";
    }

    public static final String getInfomoduleVideoList() {
        return HTTPS + sAppConfig.apiServerHostName + WEB_API + "/InfoModuleV2/GetVideoList";
    }

    public static final String getInfomoduleVideoDetail() {
        return HTTPS + sAppConfig.apiServerHostName + WEB_API + "/InfoModuleV2/GetVideoDetail";
    }

    public static final String WS_APP_REFEREE_INSERT_PROPERTY_GUID = "guid";
    public static final String WS_APP_REFEREE_INSERT_PROPERTY_SHOP_ID = "shopId";
    public static final String WS_APP_REFEREE_INSERT_PROPERTY_LOCATIONID = "locationId";
    public static final String WS_APP_REFEREE_INSERT_PROPERTY_EMPID = "empId";
    public static final String WS_APP_REFEREE_INSERT_PROPERTY_ISREQUIRED_LOGIN = "isRequireLogin";

    // Ecoupon
    public static final String getEcouponGetMemberEcouponListUrl() {
        return HTTPS + sAppConfig.ecouponServerHostName + "/WebApi/ECoupon/GetMemberECouponList/?";
    }

    public static final String getEcouponDetailUrl() {
        return HTTPS + sAppConfig.ecouponServerHostName + "/WebApi/ECoupon/GetECouponDetail/?";
    }

    public static final String getEcouponPickEcouponIdUrl() {
        return HTTPS + sAppConfig.ecouponServerHostName + "/WebApi/ECoupon/SetMemberECouponByECouponId/?";
    }

    public static final String getEcouponPickFirstDownloadEcouponIdUrl() {
        return HTTPS + sAppConfig.ecouponServerHostName + "/WebApi/ECoupon/SetMemberFirstDownloadECouponByECouponId/?";
    }

    public static final String getEcouponPickCodeUrl() {
        return HTTPS + sAppConfig.ecouponServerHostName + "/WebApi/ECoupon/SetMemberECouponByCode/?";
    }

    public static final String getEcouponGetEcouponListWithoutCodeUrl() {
        return HTTPS + sAppConfig.ecouponServerHostName + "/Webapi/ECoupon/GetECouponListWithoutCode/?";
    }

    public static final String getEcouponGetMemberEcouponStatusListUrl() {
        return HTTPS + sAppConfig.ecouponServerHostName + "/Webapi/ECoupon/GetMemberECouponStatusList/?";
    }


    public static final String getAnnouncementUrl(String shopId, String platform) {
        return HTTPS + sAppConfig.cdnServerHostName + "/webapi/AppAnnouncement/GetAppAnnouncementList/"+ shopId +"/"+ platform;
    }

    // MemberZone
    public static final String getVipInfoUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/GetVipInfo";
    }

    public static final String WS_MEMBERZONE_GETVIPINFO_SHOPID = "shopId";
    public static final String WS_MEMBERZONE_GETVIPINFO_ISBINDING = "isBinding";

    public static final String getMemberzoneHasTakenActivationGiftUrl() {
        return HTTPS + sAppConfig.api2ServerHostName + "/o2o/api/coupon/IsUsingActivationGift/";
    }

    public static final String getMemberzoneMemberItemUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/GetVIPMemberItem";
    }

    public static final String WS_MEMBERZONE_MEMBERITEM_PROPERTY_APP_VER = "appVer";

    public static final String getMemberzoneUpdateMemberDataUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VIPMember/InsertOrUpdateVIPMember";
    }

    public static final String getMemberzoneBindingVipMemberUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/BindingShopLocationVIPMember";
    }

    public static final String getMemberzoneRegisterVipMemberUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/RegisterVIPMember";
    }

    public static final String getMemberzoneGetVipShopInfoUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/GetVipShopInfo";
    }

    //舊官網訂單查詢
    public final static String WS_LOGIN_THIRDPARTY_SHOPID = "shopId";
    public static final String getGetThirdPartyTradesOrderSettingUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webAPI/VIPMember/GetThirdPartyTradesOrderConfiguration";
    }

    // MemberPresent
    public final static String WS_MEMBERZONE_GETNONVIP_OPENCARD_SHOPID = "shopId";
    public final static String WS_MEMBERZONE_OPENCARD_PRESENT_SHOPID = "shopId";
    public final static String WS_MEMBERZONE_BIRTHDAY_PRESENT_SHOPID = "shopId";

    // MemberGiftStatus
    public static final String getMemberzoneGetOpenCardStatusUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/GetOpenCardPresentStatus";
    }

    public static final String getMemberzoneGetBirthdayStatusUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/GetBirthdayPresentStatus";
    }

    public static final String getMemberzoneGetNonVipOpenCardUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/VipMember/GetNonVIPOpenCardPresentStatus";
    }

    // RefereeSetting
    public static final String getRefereeLocationSettingUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/AppReferee/GetAppRefereeProfile/";
    }

    public static final String getRefereeLocationListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/AppReferee/GetLocationList/";
    }

    public static final String getRefereeEmployeeListUrl() {
        return HTTPS + sAppConfig.apiServerHostName
                + "/webapi/AppReferee/GetLocationEmployeeList/";
    }

    public static final String getRefereeInsertUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/AppReferee/InsertAppReferee/";
    }

    // NewLogin
    public static final String getLoginGetRegisterStatusUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/GetNineYiMemberRegisterStatus";
    }

    public static final String getLoginRegisterUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/CreateNineYiMemberRegisterRequest";
    }

    public static final String getLoginGetPhoneDialVerifyStatusUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/GetPhoneDialVerifyStatus";
    }

    public static final String getLoginConfirmVerifyCodeUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ConfirmNineYiMemberVerifyCode";
    }

    public static final String getLoginConfirmPhoneDialVerifyUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ConfirmNineYiMemberPhoneDialVerify";
    }

    public static final String getLoginFinishRegisterUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/FinishNineYiMemberRegister";
    }

    public static final String getLoginLoginAppUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/LoginNineYiMember";
    }

    public static final String getLoginGetFBRegisterStatusUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/GetFacebookMemberRegisterStatus";
    }

    public static final String getLoginFBRegisterUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/CreateFacebookMemberRegisterRequest";
    }

    public static final String getLoginResendVerifyCodeUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ResendVerifyCode";
    }

    public static final String getLoginFBConfirmVerifyCodeUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ConfirmFacebookMemberVerifyCode";
    }

    public static final String getLoginFBConfirmPhoneDialVerifyUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ConfirmFacebookMemberPhoneDialVerify";
    }

    public static final String getLoginFBLoginUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/LoginFacebookMember";
    }

    public static final String getLoginResetPasswordUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/CreateNineYiMemberResetPasswordRequest";
    }

    public static final String getLoginFinishResetPasswordUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/FinishNineYiMemberResetPassword";
    }

    public static final String getLoginChangePasswordUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ChangeNineYiMemberPassword";
    }

    //GetActivity
    public final static String WS_GETACTIVITY_DETAIL_SHOPID = "shopId";


    public static final String getGetActivityDetailUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Activity/GetActivityDetail";
    }

    //GetRewardPoint 活動積點
    public final static String WS_GETREWARDPOINT_LIST_SHOPID = "ShopId";
    public final static String WS_GETREWARDPOINT_DETAIL_REWARDPOINTID = "RewardPointId";
    public final static String WS_GETREWARDPOINT_MEMBERTOTAL_REWARDPOINTID = "RewardPointId";

    public static final String getGetRewardPointListUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/LocationRewardPoint/GetRewardPointList";
    }

    public static final String getGetRewardPointDetailUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/LocationRewardPoint/GetRewardPointDetail";
    }

    public static final String getGetRewardPointMemberTotalPointUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/LocationRewardPoint/GetMemberRewardPoint";
    }

    //第三方官網登入
    public final static String WS_LOGIN_THIRDPARTY_REGISTER_STATUS_LOGINID = "loginId";
    public final static String WS_LOGIN_THIRDPARTY_REGISTER_STATUS_PASSWORD = "password";
    public final static String WS_LOGIN_THIRDPARTY_REGISTER_STATUS_SHOPID = "shopId";
    public static final String getThirdPartyMemberRegisterStatusUrl(){
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/AuthV3/GetThirdpartyMemberRegisterStatus";
    }

    public final static String WS_LOGIN_THIRDPARTY_CREATE_MEMBER_AUTHSESSIONTOKEN = "authSessionToken";
    public final static String WS_LOGIN_THIRDPARTY_CREATE_MEMBER_CELLPHONE = "cellPhone";
    public final static String WS_LOGIN_THIRDPARTY_CREATE_MEMBER_SHOPID = "shopId";
    public static final String getThirdPartyMemberRegisterRequestUrl(){
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/CreateThirdpartyMemberRegisterRequest";
    }

    public final static String WS_LOGIN_THIRDPARTY_VERIFY_CODE_CELLPHONE = "cellPhone";
    public final static String WS_LOGIN_THIRDPARTY_VERIFY_CODE_SHOPID = "shopId";
    public final static String WS_LOGIN_THIRDPARTY_VERIFY_CODE_AUTHSESSIONTOKEN = "authSessionToken";
    public final static String WS_LOGIN_THIRDPARTY_VERIFY_CODE_CODE = "code";
    public final static String WS_LOGIN_THIRDPARTY_VERIFY_CODE_SOURCE = "source";
    public final static String WS_LOGIN_THIRDPARTY_VERIFY_CODE_DEVICE = "device";
    public final static String WS_LOGIN_THIRDPARTY_VERIFY_CODE_APPVER = "appVer";
    public static final String getThirdPartyMemberConfirmVerifyCodeUrl(){
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ConfirmThirdpartyMemberVerifyCode";
    }

    public final static String WS_LOGIN_THIRDPARTY_CONFIRM_DIAL_CELLPHONE = "cellPhone";
    public final static String WS_LOGIN_THIRDPARTY_CONFIRM_DIAL_SHOPID = "shopId";
    public final static String WS_LOGIN_THIRDPARTY_CONFIRM_DIAL_AUTHSESSIONTOKEN = "authSessionToken";
    public final static String WS_LOGIN_THIRDPARTY_CONFIRM_DIAL_SOURCE = "source";
    public final static String WS_LOGIN_THIRDPARTY_CONFIRM_DIAL_DEVICE = "device";
    public final static String WS_LOGIN_THIRDPARTY_CONFIRM_DIAL_APPVER = "appVer";
    public static final String getThirdPartyMemberPhoneDialVerifyUrl(){
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/ConfirmThirdpartyMemberPhoneDialVerify";
    }
////第三方登入
    public final static String WS_LOGIN_THIRDPARTY_INFO_SHOPID = "shopId";
    public final static String WS_LOGIN_THIRDPARTY_INFO_DEVICE = "device";
    public static final String getThirdPartyShopThirdpartyAuthInfoUrl(){    //取得第三方會員登入設定
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/GetShopThirdpartyAuthInfo";
    }

    public final static String WS_LOGIN_THIRDPARTY_REGISTER_SHOPID = "shopId";
    public final static String WS_LOGIN_THIRDPARTY_REGISTER_ACCESSTOKEN = "accessToken";
    public static final String getThirdPartyMemberRegisterStatusWithTokenUrl(){     //取得第三方會員註冊狀態
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/GetThirdpartyMemberRegisterStatusWithToken";
    }

    public final static String WS_LOGIN_THIRDPARTY_MEMBER_SHOPID = "shopId";
    public final static String WS_LOGIN_THIRDPARTY_MEMBER_AUTHSESSIONTOKEN = "authSessionToken";
    public final static String WS_LOGIN_THIRDPARTY_MEMBER_SOURCE = "source";
    public final static String WS_LOGIN_THIRDPARTY_MEMBER_DEVICE = "device";
    public final static String WS_LOGIN_THIRDPARTY_MEMBER_APPVER = "appVer";
    public static final String getThirdPartyLoginThirdpartyMemberUrl(){     //第三方會員登入
        return HTTPS + sAppConfig.apiServerHostName + "/WebAPI/AuthV3/LoginThirdpartyMember";
    }

    public final static String WS_LOGIN_THIRDPARTY_SUCCESS_ACCESS_TOKEN = "/v2/Login/ThirdpartyOAuthSuccess";

    //商城reborn
    public final static String getMallConfigureationProfileUrl() {
        return HTTPS + sAppConfig.apiServerHostName + "/webapi/Mall/GetMallConfigurationProfile";
    }

    public static String getVipMemberBenefits() {
        return HTTPS + sAppConfig.apiServerHostName + "/WebApi/VipMember/GetVipMemberBenefits";
    }
}
