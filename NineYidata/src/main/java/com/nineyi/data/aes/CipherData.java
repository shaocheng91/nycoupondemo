package com.nineyi.data.aes;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;


public class CipherData implements Parcelable {

    public static final String TIMESTAMP = "timeStamp";
    public static final String SIGNATURE = "signature";
    public static final String CIPHERTEXT = "cipherText";

    private String timeStamp;
    private String signature;
    private String cipherText;

    public String getTimestamp() {
        return timeStamp;
    }

    public void setTimestamp(String timestamp) {
        this.timeStamp = timestamp;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getCipherText() {
        return cipherText;
    }

    public void setCipherText(String cipherText) {
        this.cipherText = cipherText;
    }

    public CipherData() {
    }

    public Map<String,String> generateMap(){
        HashMap<String,String> map = new HashMap<>();
        map.put(TIMESTAMP, this.timeStamp);
        map.put(CIPHERTEXT, this.cipherText);
        map.put(SIGNATURE, this.signature);
        return map;
    }

    protected CipherData(Parcel in) {
        timeStamp = in.readString();
        signature = in.readString();
        cipherText = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(timeStamp);
        dest.writeString(signature);
        dest.writeString(cipherText);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CipherData> CREATOR = new Parcelable.Creator<CipherData>() {
        @Override
        public CipherData createFromParcel(Parcel in) {
            return new CipherData(in);
        }

        @Override
        public CipherData[] newArray(int size) {
            return new CipherData[size];
        }
    };
}
