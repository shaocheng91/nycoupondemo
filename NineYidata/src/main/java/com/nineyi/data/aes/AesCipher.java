/**
 * Usage:
 *
 *
 String TAG = "MainActivity";
 String AES_KEY = "34wwb3wgfdsse543ddsds602e96f9c54";
 String SHA_512_KEY = "34wwb3wgfd";
 String SALT_KEY = "fdsfds";
 AesCipher aesCipher = new AesCipher(AES_KEY, SHA_512_KEY, SALT_KEY);
 CipherData cipherData = aesCipher.encryptString("AlanPu");
 Log.v(TAG, "encrypted: ["+cipherData.getCipherText()+"]");
 Log.v(TAG, "timestamp: ["+cipherData.getTimestamp()+"]");
 Log.v(TAG, "signature: ["+cipherData.getSignature()+"]");
 Log.v(TAG, "right data: " + aesCipher.verifyCipherData(cipherData));
 Log.v(TAG, "decrypted: [" + aesCipher.decryptCipherData(cipherData)+"]");
 Log.v(TAG, "parameters:" + cipherData.generateParametersList());
 */

package com.nineyi.data.aes;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;


public class AesCipher {

    //	private static final String TAG = "AesCipher";
    private static String TEXT_ENCODING = "UTF-16LE";

    private byte[] AesKey;
    private byte[] Sha512Key;
    private String SaltKey;

    public AesCipher(byte[] AesKey, byte[] Sha512Key, byte[] SaltKey) {
        this.AesKey = AesKey;
        this.Sha512Key = Sha512Key;
        try {
            this.SaltKey = new String(SaltKey, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String getTimestamp() {
        String timestamp = Long.toString(System.currentTimeMillis() / 1000 + 28800); // UTC+8
        //		Log.v(TAG, "Timestamp: " + timestamp);
        return timestamp;
    }

    public String encodeAes(String str) {
        String result = "";
        try {
            byte[] rowBytes = str.getBytes(TEXT_ENCODING);

            SecretKeySpec skeySpec = new SecretKeySpec(this.AesKey, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(this.AesKey, 0, 16);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);

            byte[] encryptedData = cipher.doFinal(rowBytes);

            byte[] base64bytes = Base64.encode(encryptedData, Base64.NO_WRAP);
            result = new String(base64bytes, "UTF-8");

            //			Log.v(TAG, "encrypted base64: [" + result + "]");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String decodeAes(String str) {
        String result = "";
        try {
            byte[] base64encryptBytes = Base64.decode(str, Base64.DEFAULT);

            SecretKeySpec skeySpec = new SecretKeySpec(this.AesKey, "AES/CBC/PKCS7Padding");
            IvParameterSpec ivSpec = new IvParameterSpec(this.AesKey, 0, 16);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
            byte[] decrypted = cipher.doFinal(base64encryptBytes);

            result = new String(decrypted, TEXT_ENCODING);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String generateSignature(String timestamp, String cipherText) {
        String result = "";
        try {

            final SecretKeySpec secretKey = new SecretKeySpec(this.Sha512Key, "HmacSHA512");
            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKey);

            final byte[] macBytes = mac
                    .doFinal(String.format("%s%s%s", timestamp, this.SaltKey, cipherText).getBytes("UTF-8"));
            result = AesCipher.bytesToHex(macBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public CipherData encryptString(String str) {
        CipherData data = new CipherData();
        String cipherData = this.encodeAes(str);
        data.setCipherText(cipherData);
        String timestamp = this.getTimestamp();
        data.setTimestamp(timestamp);
        String signature = this.generateSignature(timestamp, cipherData);
        data.setSignature(signature);
        return data;
    }

    public boolean verifyCipherData(CipherData data) {
        String localSignature = this.generateSignature(data.getTimestamp(), data.getCipherText());
        return localSignature.equals(data.getSignature());
    }

    public String decryptCipherData(CipherData data) {
        String result = "";
        if (this.verifyCipherData(data)) {
            return decodeAes(data.getCipherText());
        }
        return result;
    }


    private static String bytesToHex(byte[] bytes) {
        // convert to hex
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
