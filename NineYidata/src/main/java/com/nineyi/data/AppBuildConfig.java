package com.nineyi.data;

import com.nineyi.data.aes.AesCipher;


/**
 * Created by toby on 2015/5/25.
 */
public class AppBuildConfig {

    public final int shopId;
    public final String apiServerHostName;
    public final String api2ServerHostName;
    public final String webServerHostName;
    public String cdnServerHostName;
    public final String ecouponServerHostName;
    public final String trackServerHostName;
    public final String appServerHostName;
    public final boolean hasLocationMemberModule;
    public final boolean isSslEnabled;
    public final AesCipher aesCipher;
    public final DefaultLogger logger;
    public final boolean isDataDroidLoggable;
    public final String appVer;
    public final boolean isOverrideHostNameVerify;
    public final boolean isDebug;

    public AppBuildConfig(Builder builder) {
        shopId = builder.getShopId();
        apiServerHostName = builder.getApiServerHostName();
        api2ServerHostName = builder.getApi2ServerHostName();
        cdnServerHostName = builder.getCdnServerHostName();
        ecouponServerHostName = builder.getEcouponServerHostName();
        trackServerHostName = builder.getTrackServerHostName();
        webServerHostName = builder.getWebsiteDomainName();
        appServerHostName = builder.getAppDomainName();
        isSslEnabled = builder.isSslEnabled();
        aesCipher = builder.getAesCipher();
        logger = builder.getLogger();
        isDataDroidLoggable = builder.isLoggable();
        appVer = builder.getAppVer();
        isOverrideHostNameVerify = builder.isOverrideHostNameVerify();
        hasLocationMemberModule = builder.hasLocationMemberModule();
        isDebug = builder.isDebug();
    }

    public static AppBuildConfig createDefault() {
        // return new Builder(context).build();
        return new Builder(0).build();
    }

    public boolean isMallApp() {
        return shopId == 0; // to be define
    }

    public void setCdnServer(String hostName) {
        cdnServerHostName = hostName;
    }

}
