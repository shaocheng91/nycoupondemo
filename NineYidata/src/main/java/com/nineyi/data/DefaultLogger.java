package com.nineyi.data;

import android.util.Log;


/**
 * Created by toby on 2015/6/3.
 */
public class DefaultLogger {

    private int logLevel;

    public DefaultLogger(int logLevel) {
        this.logLevel = logLevel;
    }

    public DefaultLogger() {
        this.logLevel = Log.INFO;
    }

    public boolean isLoggable(String tag, int level) {
        return this.logLevel <= level;
    }

    public int getLogLevel() {
        return this.logLevel;
    }

    public void setLogLevel(int logLevel) {
        this.logLevel = logLevel;
    }

    public void d(String tag, String text, Throwable throwable) {
        if (this.isLoggable(tag, Log.DEBUG)) {
            Log.d(tag, text, throwable);
        }

    }

    public void v(String tag, String text, Throwable throwable) {
        if (this.isLoggable(tag, Log.VERBOSE)) {
            Log.v(tag, text, throwable);
        }

    }

    public void i(String tag, String text, Throwable throwable) {
        if (this.isLoggable(tag, Log.INFO)) {
            Log.i(tag, text, throwable);
        }

    }

    public void w(String tag, String text, Throwable throwable) {
        if (this.isLoggable(tag, Log.WARN)) {
            Log.w(tag, text, throwable);
        }

    }

    public void e(String tag, String text, Throwable throwable) {
        if (this.isLoggable(tag, Log.ERROR)) {
            Log.e(tag, text, throwable);
        }

    }

    public void d(String tag, String text) {
        this.d(tag, text, (Throwable) null);
    }

    public void v(String tag, String text) {
        this.v(tag, text, (Throwable) null);
    }

    public void i(String tag, String text) {
        this.i(tag, text, (Throwable) null);
    }

    public void w(String tag, String text) {
        this.w(tag, text, (Throwable) null);
    }

    public void e(String tag, String text) {
        this.e(tag, text, (Throwable) null);
    }

    public void log(int priority, String tag, String msg) {
        this.log(priority, tag, msg, false);
    }

    public void log(int priority, String tag, String msg, boolean forceLog) {
        if (forceLog || this.isLoggable(tag, priority)) {
            Log.println(priority, tag, msg);
        }

    }
}
