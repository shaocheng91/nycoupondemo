package com.nineyi.data;

import com.nineyi.data.aes.AesCipher;


/**
 * Created by tedliang on 15/11/30.
 */
public class Builder {

    private int shopId;

    public int getShopId() {
        return shopId;
    }

    public String getApiServerHostName() {
        return apiServerHostName;
    }

    public String getApi2ServerHostName() {
        return api2ServerHostName;
    }

    public String getCdnServerHostName() {
        return cdnServerHostName;
    }

    public String getEcouponServerHostName() {
        return ecouponServerHostName;
    }

    public String getTrackServerHostName(){
        return trackServerHostName;
    }

    public String getWebsiteDomainName() {
        return websiteDomainName;
    }

    public boolean isSslEnabled() {
        return isSslEnabled;
    }

    public AesCipher getAesCipher() {
        return aesCipher;
    }

    public DefaultLogger getLogger() {
        return logger;
    }

    public boolean isLoggable() {
        return isLoggable;
    }

    public String getAppVer() {
        return appVer;
    }

    public String getAppDomainName() {
       return appDomainName;
    }

    public boolean isOverrideHostNameVerify() {
        return isOverrideHostNameVerify;
    }

    public boolean hasLocationMemberModule() {
        return hasLocationMemberModule;
    }

    public boolean isDebug() {
        return isDebug;
    }

    private String apiServerHostName = "webapi.91mai.com";
    private String api2ServerHostName = "api2.91mai.com";
    private String cdnServerHostName = "webapi.91mai.com";
    private String ecouponServerHostName = "ecoupon.91mai.com";
    private String websiteDomainName = "tw.91mai.com";
    private String trackServerHostName = "track.91app.io";
//    private String appDomainName = "appservice.91app.com";
    private String appDomainName = "tw.91mai.com";
    private boolean hasLocationMemberModule;
    private boolean isSslEnabled;
    private AesCipher aesCipher;
    private DefaultLogger logger;
    private boolean isLoggable;
    private String appVer;
    private boolean isOverrideHostNameVerify;
    private boolean isDebug;

    public Builder(int shopId) {
        this.shopId = shopId;
    }

    public Builder webApiServer(String hostName) {
        this.apiServerHostName = hostName;
        return this;
    }

    public Builder api2Server(String hostName) {
        this.api2ServerHostName = hostName;
        return this;
    }

    public Builder cdnServer(String hostName) {
        this.cdnServerHostName = hostName;
        return this;
    }

    public Builder ecouponServer(String hostName) {
        this.ecouponServerHostName = hostName;
        return this;
    }

    public Builder websiteDomain(String domainName) {
        this.websiteDomainName = domainName;
        return this;
    }

    public Builder trackDomain(String domainName){
        this.trackServerHostName = domainName;
        return this;
    }

    public Builder enableSSL(boolean enabled) {
        this.isSslEnabled = enabled;
        return this;
    }

    public Builder aesCipher(AesCipher aesCipher) {
        this.aesCipher = aesCipher;
        return this;
    }

    public Builder logger(DefaultLogger logger) {
        this.logger = logger;
        return this;
    }

    public Builder enableDataDroidLog(boolean isLoggable) {
        this.isLoggable = isLoggable;
        return this;
    }

    public Builder appVer(String appVer) {
        this.appVer = appVer;
        return this;
    }

    public Builder appDomainServer(String appDomain){
        this.appDomainName = appDomain;
        return this;
    }

    public Builder isOverrideHostNameVerify(boolean isOverrideHostNameVerify) {
        this.isOverrideHostNameVerify = isOverrideHostNameVerify;
        return this;
    }

    public Builder locationMemberModule(boolean hasLocationMemberModule){
        this.hasLocationMemberModule = hasLocationMemberModule;
        return this;
    }

    public Builder isDebug(boolean isDebug) {
        this.isDebug = isDebug;
        return this;
    }

    public AppBuildConfig build() {
        return new AppBuildConfig(this);
    }


}
