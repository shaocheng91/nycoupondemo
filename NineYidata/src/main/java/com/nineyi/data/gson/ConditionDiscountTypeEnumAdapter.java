package com.nineyi.data.gson;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.nineyi.promote.ConditionDiscountTypeEnum;

import java.io.IOException;

public class ConditionDiscountTypeEnumAdapter  extends TypeAdapter<ConditionDiscountTypeEnum> {

    @Override
    public ConditionDiscountTypeEnum read(JsonReader reader) throws IOException {
        JsonToken next = reader.peek();

        if (next == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }

        if (next == JsonToken.NUMBER) {
            int token = (int)reader.nextDouble();
            return ConditionDiscountTypeEnum.from(token);
        } else {
            reader.skipValue();
            return null;
        }
    }

    @Override
    public void write(JsonWriter writer, ConditionDiscountTypeEnum value) throws IOException {
        if (value == null) {
            writer.nullValue();
        } else {
            writer.value(value.toInt());
        }
    }
}
