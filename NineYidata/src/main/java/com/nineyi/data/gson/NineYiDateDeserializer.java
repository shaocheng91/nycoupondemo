package com.nineyi.data.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.nineyi.data.model.gson.NineyiDate;

import java.lang.reflect.Type;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class NineYiDateDeserializer implements JsonDeserializer<NineyiDate> {

    // For regex: Date(1452009600000+0800)
    public static final Pattern NINEYI_DATE_PATTERN = Pattern.compile("Date\\((-{0,1}\\d+)([-+]\\d+)?\\)");
    public static final Pattern NINEYI_DATE_NUMBER_PATTERN = Pattern.compile("(-{0,1}\\d+)([-+]\\d+)?");
    public static final Pattern NINEYI_DATE_RAW_PATTERN = Pattern.compile("Date\\((.*)\\)");

    @Override
    public NineyiDate deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2)
            throws JsonParseException {
        String date = element.getAsString();

        String raw = "0";
        String time = "0";
        String timezone = "0";

        Matcher dateNumberMatcher = NINEYI_DATE_PATTERN.matcher(date);
        if (dateNumberMatcher.find()) {
            time = dateNumberMatcher.group(1);
            timezone = dateNumberMatcher.group(2);
        }

        Matcher rawMatcher = NINEYI_DATE_RAW_PATTERN.matcher(date);
        if(rawMatcher.find()) {
            raw = rawMatcher.group(1);
        }

        return new NineyiDate(raw, time, timezone);
    }
}