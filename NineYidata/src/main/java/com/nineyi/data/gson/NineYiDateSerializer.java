package com.nineyi.data.gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import com.nineyi.data.model.gson.NineyiDate;

import java.lang.reflect.Type;


/**
 * Created by tedliang on 15/8/24.
 */
public class NineYiDateSerializer implements JsonSerializer<NineyiDate> {

    @Override
    public JsonElement serialize(NineyiDate src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive("/Date("+src.getRaw()+")/");
    }
}
