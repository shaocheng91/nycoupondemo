package com.nineyi.data.gson;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.nineyi.promote.ConditionTypeEnum;

import java.io.IOException;

public class ConditionTypeEnumAdapter extends TypeAdapter<ConditionTypeEnum> {
    @Override
    public ConditionTypeEnum read(JsonReader reader) throws IOException {
        JsonToken next = reader.peek();

        if (next == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }

        if (next == JsonToken.NUMBER) {
            int token = (int)reader.nextDouble();
            return ConditionTypeEnum.from(token);
        } else {
            reader.skipValue();
            return null;
        }
    }

    @Override
    public void write(JsonWriter writer, ConditionTypeEnum value) throws IOException {
        if (value == null) {
            writer.nullValue();
        } else {
            writer.value(value.toInt());
        }
    }
}
