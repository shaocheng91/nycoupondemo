package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBImage implements Parcelable {

    int height;
    String src;
    int width;

    public String getSrc() {
        return src;
    }

    protected FBImage(Parcel in) {
        height = in.readInt();
        src = in.readString();
        width = in.readInt();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(height);
        dest.writeString(src);
        dest.writeInt(width);

    }

    public static final Parcelable.Creator<FBImage> CREATOR = new Parcelable.Creator<FBImage>() {
        @Override
        public FBImage createFromParcel(Parcel in) {
            return new FBImage(in);
        }

        @Override
        public FBImage[] newArray(int size) {
            return new FBImage[size];
        }
    };
}