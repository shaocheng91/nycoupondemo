package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShoppingCartSalePageShort implements Parcelable {

    public int SalePageId;
    public int SaleProductSKUId;
    public String Title;
    public int Price;
    public String PicUrl;

    public ShoppingCartSalePageShort() {
    }

    // Parcelable management
    private ShoppingCartSalePageShort(Parcel in) {
        SalePageId = in.readInt();
        SaleProductSKUId = in.readInt();
        Title = in.readString();
        Price = in.readInt();
        PicUrl = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SalePageId);
        dest.writeInt(SaleProductSKUId);
        dest.writeString(Title);
        dest.writeInt(Price);
        dest.writeString(PicUrl);
    }

    public static final Parcelable.Creator<ShoppingCartSalePageShort> CREATOR
            = new Parcelable.Creator<ShoppingCartSalePageShort>() {
        public ShoppingCartSalePageShort createFromParcel(Parcel in) {
            return new ShoppingCartSalePageShort(in);
        }

        public ShoppingCartSalePageShort[] newArray(int size) {
            return new ShoppingCartSalePageShort[size];
        }
    };
}
