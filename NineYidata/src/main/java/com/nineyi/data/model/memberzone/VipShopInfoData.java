package com.nineyi.data.model.memberzone;

import com.nineyi.data.model.referee.LocationDataRoot;

import android.os.Parcel;
import android.os.Parcelable;


public class VipShopInfoData implements Parcelable {


    private VipShopDataName LocationBindingButton;


    public VipShopInfoData() {

    }

    public VipShopInfoData(Parcel in) {
        LocationBindingButton = in.readParcelable(LocationDataRoot.class.getClassLoader());
    }

    public VipShopDataName getLocationBindingButton() {
        return LocationBindingButton;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipShopInfoData> CREATOR = new Creator<VipShopInfoData>() {
        public VipShopInfoData createFromParcel(Parcel in) {
            return new VipShopInfoData(in);
        }

        public VipShopInfoData[] newArray(int size) {
            return new VipShopInfoData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(LocationBindingButton, flags);
    }


}