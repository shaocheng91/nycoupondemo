
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.installment.InstallmentShopInstallmentList;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class DisplayPayTypeList implements Parcelable {

    @SerializedName("DisplayName")
    @Expose
    private String DisplayName;
    @SerializedName("StatisticsTypeDef")
    @Expose
    private String StatisticsTypeDef;
    @SerializedName("InstallmentType")
    @Expose
    private InstallmentShopInstallmentList InstallmentType;
    @SerializedName("IsRecommand")
    @Expose
    private Boolean IsRecommand;
    @SerializedName("PayTypeList")
    @Expose
    private List<PayTypeList> PayTypeList = new ArrayList<PayTypeList>();

    /**
     * 
     * @return
     *     The DisplayName
     */
    public String getDisplayName() {
        return DisplayName;
    }

    /**
     * 
     * @param DisplayName
     *     The DisplayName
     */
    public void setDisplayName(String DisplayName) {
        this.DisplayName = DisplayName;
    }

    /**
     * 
     * @return
     *     The StatisticsTypeDef
     */
    public String getStatisticsTypeDef() {
        return StatisticsTypeDef;
    }

    /**
     * 
     * @param StatisticsTypeDef
     *     The StatisticsTypeDef
     */
    public void setStatisticsTypeDef(String StatisticsTypeDef) {
        this.StatisticsTypeDef = StatisticsTypeDef;
    }

    /**
     * 
     * @return
     *     The InstallmentType
     */
    public InstallmentShopInstallmentList getInstallmentType() {
        return InstallmentType;
    }

    /**
     * 
     * @param InstallmentType
     *     The InstallmentType
     */
    public void setInstallmentType(InstallmentShopInstallmentList InstallmentType) {
        this.InstallmentType = InstallmentType;
    }

    /**
     * 
     * @return
     *     The IsRecommand
     */
    public Boolean getIsRecommand() {
        return IsRecommand;
    }

    /**
     * 
     * @param IsRecommand
     *     The IsRecommand
     */
    public void setIsRecommand(Boolean IsRecommand) {
        this.IsRecommand = IsRecommand;
    }


    /**
     * 
     * @return
     *     The PayTypeList
     */
    public List<PayTypeList> getPayTypeList() {
        return PayTypeList;
    }

    /**
     * 
     * @param PayTypeList
     *     The PayTypeList
     */
    public void setPayTypeList(List<PayTypeList> PayTypeList) {
        this.PayTypeList = PayTypeList;
    }

    public DisplayPayTypeList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.DisplayName);
        dest.writeString(this.StatisticsTypeDef);
        dest.writeParcelable(this.InstallmentType, flags);
        dest.writeValue(this.IsRecommand);
        dest.writeTypedList(this.PayTypeList);
    }

    protected DisplayPayTypeList(Parcel in) {
        this.DisplayName = in.readString();
        this.StatisticsTypeDef = in.readString();
        this.InstallmentType = in.readParcelable(InstallmentShopInstallmentList.class.getClassLoader());
        this.IsRecommand = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.PayTypeList = in.createTypedArrayList(com.nineyi.data.model.shoppingcart.v4.PayTypeList.CREATOR);
    }

    public static final Creator<DisplayPayTypeList> CREATOR = new Creator<DisplayPayTypeList>() {
        @Override
        public DisplayPayTypeList createFromParcel(Parcel source) {
            return new DisplayPayTypeList(source);
        }

        @Override
        public DisplayPayTypeList[] newArray(int size) {
            return new DisplayPayTypeList[size];
        }
    };
}
