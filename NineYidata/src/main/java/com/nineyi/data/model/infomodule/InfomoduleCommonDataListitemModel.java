package com.nineyi.data.model.infomodule;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleCommonDataListitemModel implements Parcelable {

    public int Id;
    public String Type;
    public String Title;
    public String Subtitle;
    public String Uuid;
    public String ImageUrl;

    public InfomoduleCommonDataListitemModel(Parcel in) {
        Id = in.readInt();
        Type = in.readString();
        Title = in.readString();
        Subtitle = in.readString();
        Uuid = in.readString();
        ImageUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Type);
        dest.writeString(Title);
        dest.writeString(Subtitle);
        dest.writeString(Uuid);
        dest.writeString(ImageUrl);
    }

    public static Parcelable.Creator<InfomoduleCommonDataListitemModel> CREATOR
            = new Parcelable.Creator<InfomoduleCommonDataListitemModel>() {
        @Override
        public InfomoduleCommonDataListitemModel createFromParcel(Parcel source) {
            return new InfomoduleCommonDataListitemModel(source);
        }

        @Override
        public InfomoduleCommonDataListitemModel[] newArray(int size) {
            return new InfomoduleCommonDataListitemModel[size];
        }
    };
}
