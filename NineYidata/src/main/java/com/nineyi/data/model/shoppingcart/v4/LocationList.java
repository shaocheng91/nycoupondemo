package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class LocationList implements Parcelable{

    private static final String FIELD_LATITUDE = "Latitude";
    private static final String FIELD_WEEKEND_TIME = "WeekendTime";
    private static final String FIELD_CITY_NAME = "CityName";
    private static final String FIELD_LONGITUDE = "Longitude";
    private static final String FIELD_TEL_PREPEND = "TelPrepend";
    private static final String FIELD_ADDRESS = "Address";
    private static final String FIELD_NORMAL_TIME = "NormalTime";
    private static final String FIELD_CITY_ID = "CityId";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_TEL = "Tel";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_OPERATION_TIME = "OperationTime";

    @SerializedName(FIELD_LATITUDE)
    private double mLatitude;
    @SerializedName(FIELD_WEEKEND_TIME)
    private String mWeekendTime;
    @SerializedName(FIELD_CITY_NAME)
    private String mCityName;
    @SerializedName(FIELD_LONGITUDE)
    private double mLongitude;
    @SerializedName(FIELD_TEL_PREPEND)
    private String mTelPrepend;
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @SerializedName(FIELD_NORMAL_TIME)
    private String mNormalTime;
    @SerializedName(FIELD_CITY_ID)
    private int mCityId;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_TEL)
    private String mTel;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_OPERATION_TIME)
    private String mOperationTime;


    public LocationList(){

    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setWeekendTime(String weekendTime) {
        mWeekendTime = weekendTime;
    }

    public String getWeekendTime() {
        return mWeekendTime;
    }

    public void setCityName(String cityName) {
        mCityName = cityName;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setTelPrepend(String telPrepend) {
        mTelPrepend = telPrepend;
    }

    public String getTelPrepend() {
        return mTelPrepend;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setNormalTime(String normalTime) {
        mNormalTime = normalTime;
    }

    public String getNormalTime() {
        return mNormalTime;
    }

    public void setCityId(int cityId) {
        mCityId = cityId;
    }

    public int getCityId() {
        return mCityId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setTel(String tel) {
        mTel = tel;
    }

    public String getTel() {
        return mTel;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setOperationTime(String operationTime) {
        mOperationTime = operationTime;
    }

    public String getOperationTime() {
        return mOperationTime;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.mLatitude);
        dest.writeString(this.mWeekendTime);
        dest.writeString(this.mCityName);
        dest.writeDouble(this.mLongitude);
        dest.writeString(this.mTelPrepend);
        dest.writeString(this.mAddress);
        dest.writeString(this.mNormalTime);
        dest.writeInt(this.mCityId);
        dest.writeInt(this.mId);
        dest.writeString(this.mTel);
        dest.writeString(this.mName);
        dest.writeString(this.mOperationTime);
    }

    protected LocationList(Parcel in) {
        this.mLatitude = in.readDouble();
        this.mWeekendTime = in.readString();
        this.mCityName = in.readString();
        this.mLongitude = in.readDouble();
        this.mTelPrepend = in.readString();
        this.mAddress = in.readString();
        this.mNormalTime = in.readString();
        this.mCityId = in.readInt();
        this.mId = in.readInt();
        this.mTel = in.readString();
        this.mName = in.readString();
        this.mOperationTime = in.readString();
    }

    public static final Creator<LocationList> CREATOR = new Creator<LocationList>() {
        @Override
        public LocationList createFromParcel(Parcel source) {
            return new LocationList(source);
        }

        @Override
        public LocationList[] newArray(int size) {
            return new LocationList[size];
        }
    };
}