package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/7/12.
 */
public class ShopHotKeywordList implements Parcelable {
    public String ReturnCode;
    public ArrayList<String> Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeStringList(this.Data);
        dest.writeString(this.Message);
    }

    public ShopHotKeywordList() {
    }

    protected ShopHotKeywordList(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.createStringArrayList();
        this.Message = in.readString();
    }

    public static final Creator<ShopHotKeywordList> CREATOR = new Creator<ShopHotKeywordList>() {
        @Override
        public ShopHotKeywordList createFromParcel(Parcel source) {
            return new ShopHotKeywordList(source);
        }

        @Override
        public ShopHotKeywordList[] newArray(int size) {
            return new ShopHotKeywordList[size];
        }
    };
}
