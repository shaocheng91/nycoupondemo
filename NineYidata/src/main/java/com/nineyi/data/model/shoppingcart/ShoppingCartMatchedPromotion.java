package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShoppingCartMatchedPromotion implements Parcelable {

    public int Id;
    public String Name;
    public int ConditionId;
    public String ConditionDiscountTypeDef;
    public int ConditionDiscount;

    public ShoppingCartMatchedPromotion() {
    }

    // Parcelable management
    private ShoppingCartMatchedPromotion(Parcel in) {
        Id = in.readInt();
        Name = in.readString();
        ConditionId = in.readInt();
        ConditionDiscountTypeDef = in.readString();
        ConditionDiscount = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Name);
        dest.writeInt(ConditionId);
        dest.writeString(ConditionDiscountTypeDef);
        dest.writeInt(ConditionDiscount);
    }

    public static final Parcelable.Creator<ShoppingCartMatchedPromotion> CREATOR
            = new Parcelable.Creator<ShoppingCartMatchedPromotion>() {
        public ShoppingCartMatchedPromotion createFromParcel(Parcel in) {
            return new ShoppingCartMatchedPromotion(in);
        }

        public ShoppingCartMatchedPromotion[] newArray(int size) {
            return new ShoppingCartMatchedPromotion[size];
        }
    };
}
