package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.promotion.v2.PromotionTargetMemberTierList;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class PromotionList implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("IsPromotionMatchCondition")
    @Expose
    private Boolean isPromotionMatchCondition;
    @SerializedName("PromotionTypeTitle")
    @Expose
    private String promotionTypeTitle;
    @SerializedName("PromotionConditionTitle")
    @Expose
    private String promotionConditionTitle;
    @SerializedName("PromotionDiscountTitle")
    @Expose
    private String promotionDiscountTitle;
    @SerializedName("ConditionList")
    @Expose
    private List<Object> conditionList = new ArrayList<Object>();
    @SerializedName("TargetList")
    @Expose
    private List<Object> targetList = new ArrayList<Object>();
    @SerializedName("PromotionConditionTypeDef")
    @Expose
    private String promotionConditionTypeDef;
    @SerializedName("PromotionConditionDiscountTypeDef")
    @Expose
    private String promotionConditionDiscountTypeDef;
    @SerializedName("PromotionTargetMemberTierList")
    @Expose
    private List<PromotionTargetMemberTierList> promotionTargetMemberTierList;



    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The isPromotionMatchCondition
     */
    public Boolean getIsPromotionMatchCondition() {
        return isPromotionMatchCondition;
    }

    /**
     * @param isPromotionMatchCondition The IsPromotionMatchCondition
     */
    public void setIsPromotionMatchCondition(Boolean isPromotionMatchCondition) {
        this.isPromotionMatchCondition = isPromotionMatchCondition;
    }

    /**
     * @return The promotionTypeTitle
     */
    public String getPromotionTypeTitle() {
        return promotionTypeTitle;
    }

    /**
     * @param promotionTypeTitle The PromotionTypeTitle
     */
    public void setPromotionTypeTitle(String promotionTypeTitle) {
        this.promotionTypeTitle = promotionTypeTitle;
    }

    /**
     * @return The promotionConditionTitle
     */
    public String getPromotionConditionTitle() {
        return promotionConditionTitle;
    }

    /**
     * @param promotionConditionTitle The PromotionConditionTitle
     */
    public void setPromotionConditionTitle(String promotionConditionTitle) {
        this.promotionConditionTitle = promotionConditionTitle;
    }

    /**
     * @return The promotionDiscountTitle
     */
    public String getPromotionDiscountTitle() {
        return promotionDiscountTitle;
    }

    /**
     * @param promotionDiscountTitle The PromotionDiscountTitle
     */
    public void setPromotionDiscountTitle(String promotionDiscountTitle) {
        this.promotionDiscountTitle = promotionDiscountTitle;
    }

    /**
     * @return The conditionList
     */
    public List<Object> getConditionList() {
        return conditionList;
    }

    /**
     * @param conditionList The ConditionList
     */
    public void setConditionList(List<Object> conditionList) {
        this.conditionList = conditionList;
    }

    /**
     * @return The targetList
     */
    public List<Object> getTargetList() {
        return targetList;
    }

    /**
     * @param targetList The TargetList
     */
    public void setTargetList(List<Object> targetList) {
        this.targetList = targetList;
    }

    /**
     * @return The promotionConditionTypeDef
     */
    public String getPromotionConditionTypeDef() {
        return promotionConditionTypeDef;
    }

    /**
     * @param promotionConditionTypeDef The PromotionConditionTypeDef
     */
    public void setPromotionConditionTypeDef(String promotionConditionTypeDef) {
        this.promotionConditionTypeDef = promotionConditionTypeDef;
    }
    /**
     *
     * @return
     *     The promotionConditionDiscountTypeDef
     */
    public String getPromotionConditionDiscountTypeDef() {
        return promotionConditionDiscountTypeDef;
    }

    /**
     *
     * @param promotionConditionDiscountTypeDef
     *     The PromotionConditionDiscountTypeDef
     */
    public void setPromotionConditionDiscountTypeDef(String promotionConditionDiscountTypeDef) {
        this.promotionConditionDiscountTypeDef = promotionConditionDiscountTypeDef;
    }

    public List<PromotionTargetMemberTierList> getPromotionTargetMemberTierList() {
        return promotionTargetMemberTierList;
    }

    public void setPromotionTargetMemberTierList(List<PromotionTargetMemberTierList> promotionTargetMemberTierList) {
        this.promotionTargetMemberTierList = promotionTargetMemberTierList;
    }

    public PromotionList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeValue(this.isPromotionMatchCondition);
        dest.writeString(this.promotionTypeTitle);
        dest.writeString(this.promotionConditionTitle);
        dest.writeString(this.promotionDiscountTitle);
        dest.writeList(this.conditionList);
        dest.writeList(this.targetList);
        dest.writeString(this.promotionConditionTypeDef);
        dest.writeString(this.promotionConditionDiscountTypeDef);
        dest.writeTypedList(this.promotionTargetMemberTierList);
    }

    protected PromotionList(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.isPromotionMatchCondition = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.promotionTypeTitle = in.readString();
        this.promotionConditionTitle = in.readString();
        this.promotionDiscountTitle = in.readString();
        this.conditionList = new ArrayList<Object>();
        in.readList(this.conditionList, Object.class.getClassLoader());
        this.targetList = new ArrayList<Object>();
        in.readList(this.targetList, Object.class.getClassLoader());
        this.promotionConditionTypeDef = in.readString();
        this.promotionConditionDiscountTypeDef = in.readString();
        this.promotionTargetMemberTierList = in.createTypedArrayList(PromotionTargetMemberTierList.CREATOR);
    }

    public static final Creator<PromotionList> CREATOR = new Creator<PromotionList>() {
        @Override
        public PromotionList createFromParcel(Parcel source) {
            return new PromotionList(source);
        }

        @Override
        public PromotionList[] newArray(int size) {
            return new PromotionList[size];
        }
    };
}
