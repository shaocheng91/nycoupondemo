package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class CityAreaDataAreaList implements Parcelable{

    private static final String FIELD_CITY_ID = "CityId";
    private static final String FIELD_ZIP_CODE = "ZipCode";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_TITLE = "Title";


    @SerializedName(FIELD_CITY_ID)
    private int mCityId;
    @SerializedName(FIELD_ZIP_CODE)
    private int mZipCode;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    private int mType;

    public CityAreaDataAreaList(){

    }

    public void setType(int type) {
        mType = type;
    }

    public int getType() {
        return mType;
    }

    public void setCityId(int cityId) {
        mCityId = cityId;
    }

    public int getCityId() {
        return mCityId;
    }

    public void setZipCode(int zipCode) {
        mZipCode = zipCode;
    }

    public int getZipCode() {
        return mZipCode;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }


    public CityAreaDataAreaList(Parcel in) {
        mCityId = in.readInt();
        mZipCode = in.readInt();
        mId = in.readInt();
        mTitle = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CityAreaDataAreaList> CREATOR = new Creator<CityAreaDataAreaList>() {
        public CityAreaDataAreaList createFromParcel(Parcel in) {
            return new CityAreaDataAreaList(in);
        }

        public CityAreaDataAreaList[] newArray(int size) {
            return new CityAreaDataAreaList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mCityId);
        dest.writeInt(mZipCode);
        dest.writeInt(mId);
        dest.writeString(mTitle);
    }


}