package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class SalePageGiftList implements Parcelable {

    @SerializedName("SalePageId")
    @Expose
    private Integer salePageId;
    @SerializedName("SalePageGiftId")
    @Expose
    private Integer salePageGiftId;
    @SerializedName("SalePageGiftSlaveId")
    @Expose
    private Integer salePageGiftSlaveId;
    @SerializedName("SaleProductSKUId")
    @Expose
    private Integer saleProductSKUId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Qty")
    @Expose
    private Integer qty;
    @SerializedName("SellingQty")
    @Expose
    private Integer sellingQty;

    /**
     * @return The salePageId
     */
    public Integer getSalePageId() {
        return salePageId;
    }

    /**
     * @param salePageId The SalePageId
     */
    public void setSalePageId(Integer salePageId) {
        this.salePageId = salePageId;
    }

    /**
     * @return The salePageGiftId
     */
    public Integer getSalePageGiftId() {
        return salePageGiftId;
    }

    /**
     * @param salePageGiftId The SalePageGiftId
     */
    public void setSalePageGiftId(Integer salePageGiftId) {
        this.salePageGiftId = salePageGiftId;
    }

    /**
     * @return The salePageGiftSlaveId
     */
    public Integer getSalePageGiftSlaveId() {
        return salePageGiftSlaveId;
    }

    /**
     * @param salePageGiftSlaveId The SalePageGiftSlaveId
     */
    public void setSalePageGiftSlaveId(Integer salePageGiftSlaveId) {
        this.salePageGiftSlaveId = salePageGiftSlaveId;
    }

    /**
     * @return The saleProductSKUId
     */
    public Integer getSaleProductSKUId() {
        return saleProductSKUId;
    }

    /**
     * @param saleProductSKUId The SaleProductSKUId
     */
    public void setSaleProductSKUId(Integer saleProductSKUId) {
        this.saleProductSKUId = saleProductSKUId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * @param qty The Qty
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    /**
     * @return The sellingQty
     */
    public Integer getSellingQty() {
        return sellingQty;
    }

    /**
     * @param sellingQty The SellingQty
     */
    public void setSellingQty(Integer sellingQty) {
        this.sellingQty = sellingQty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.salePageId);
        dest.writeValue(this.salePageGiftId);
        dest.writeValue(this.salePageGiftSlaveId);
        dest.writeValue(this.saleProductSKUId);
        dest.writeString(this.title);
        dest.writeValue(this.qty);
        dest.writeValue(this.sellingQty);
    }

    protected SalePageGiftList(Parcel in) {
        this.salePageId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.salePageGiftId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.salePageGiftSlaveId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.saleProductSKUId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.qty = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sellingQty = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<SalePageGiftList> CREATOR = new Parcelable.Creator<SalePageGiftList>() {
        @Override
        public SalePageGiftList createFromParcel(Parcel source) {
            return new SalePageGiftList(source);
        }

        @Override
        public SalePageGiftList[] newArray(int size) {
            return new SalePageGiftList[size];
        }
    };
}
