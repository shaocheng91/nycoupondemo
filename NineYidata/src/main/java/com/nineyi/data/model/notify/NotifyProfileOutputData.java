package com.nineyi.data.model.notify;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class NotifyProfileOutputData implements Parcelable {

    public int MemberID;
    public ArrayList<NotifyProfile> APPPushProfileList;

    public NotifyProfileOutputData() {
    }

    // Parcelable management
    private NotifyProfileOutputData(Parcel in) {
        MemberID = in.readInt();
        APPPushProfileList = in.createTypedArrayList(NotifyProfile.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(MemberID);
        dest.writeTypedList(APPPushProfileList);
    }

    public static final Parcelable.Creator<NotifyProfileOutputData> CREATOR
            = new Parcelable.Creator<NotifyProfileOutputData>() {
        public NotifyProfileOutputData createFromParcel(Parcel in) {
            return new NotifyProfileOutputData(in);
        }

        public NotifyProfileOutputData[] newArray(int size) {
            return new NotifyProfileOutputData[size];
        }
    };
}
