package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ECouponShopECoupon implements Parcelable {

    public int ShopId;
    public String ShopName;
    public ArrayList<ECouponDetail> ECouponList;

    public ECouponShopECoupon() {
    }

    // Parcelable management
    private ECouponShopECoupon(Parcel in) {
        ShopId = in.readInt();
        ShopName = in.readString();
        ECouponList = in.createTypedArrayList(ECouponDetail.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopId);
        dest.writeString(ShopName);
        dest.writeTypedList(ECouponList);
    }

    public static final Parcelable.Creator<ECouponShopECoupon> CREATOR = new Parcelable.Creator<ECouponShopECoupon>() {
        public ECouponShopECoupon createFromParcel(Parcel in) {
            return new ECouponShopECoupon(in);
        }

        public ECouponShopECoupon[] newArray(int size) {
            return new ECouponShopECoupon[size];
        }
    };
}
