package com.nineyi.data.model.reward;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class RewardPointGiftList implements Parcelable{

    private static final String FIELD_IMAGE_URL = "ImageUrl";
    private static final String FIELD_BAR_CODE_TYPE_DEF = "BarCodeTypeDef";
    private static final String FIELD_POINT = "Point";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_BAR_CODE = "BarCode";
    private static final String FIELD_NAME = "Name";


    @SerializedName(FIELD_IMAGE_URL)
    private String mImageUrl;
    @SerializedName(FIELD_BAR_CODE_TYPE_DEF)
    private String mBarCodeTypeDef;
    @SerializedName(FIELD_POINT)
    private int mPoint;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_BAR_CODE)
    private String mBarCode;
    @SerializedName(FIELD_NAME)
    private String mName;


    public RewardPointGiftList(){

    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setBarCodeTypeDef(String barCodeTypeDef) {
        mBarCodeTypeDef = barCodeTypeDef;
    }

    public String getBarCodeTypeDef() {
        return mBarCodeTypeDef;
    }

    public void setPoint(int point) {
        mPoint = point;
    }

    public int getPoint() {
        return mPoint;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setBarCode(String barCode) {
        mBarCode = barCode;
    }

    public String getBarCode() {
        return mBarCode;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public RewardPointGiftList(Parcel in) {
        mImageUrl = in.readString();
        mBarCodeTypeDef = in.readString();
        mPoint = in.readInt();
        mId = in.readInt();
        mBarCode = in.readString();
        mName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RewardPointGiftList> CREATOR = new Creator<RewardPointGiftList>() {
        public RewardPointGiftList createFromParcel(Parcel in) {
            return new RewardPointGiftList(in);
        }

        public RewardPointGiftList[] newArray(int size) {
            return new RewardPointGiftList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mImageUrl);
        dest.writeString(mBarCodeTypeDef);
        dest.writeInt(mPoint);
        dest.writeInt(mId);
        dest.writeString(mBarCode);
        dest.writeString(mName);
    }


}