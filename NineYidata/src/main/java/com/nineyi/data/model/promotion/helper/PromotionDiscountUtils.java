package com.nineyi.data.model.promotion.helper;


/**
 * Created by Willy on 8/26/16.
 */
public class PromotionDiscountUtils {

    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final long SECONDS_OF_DAY = 86400L;
    private static final long HOUR_OF_DAT = 3600L;

    public static boolean isEndTimeLessThan24hours(long currentTimeMilliseconds, long endTimeMilliseconds) {
        if (endTimeMilliseconds - currentTimeMilliseconds < 0) {
            return false;
        }

        return ((endTimeMilliseconds - currentTimeMilliseconds) / (MILLISECONDS_PER_SECOND * HOUR_OF_DAT)) < 24;
    }

    public static boolean isStartTimeLessThan24hours(long currentTimeMilliseconds, long startTimeMilliseconds) {
        return ((currentTimeMilliseconds - startTimeMilliseconds) / (MILLISECONDS_PER_SECOND * HOUR_OF_DAT)) < 24;
    }

    /**
     * 檢查活動是否開始
     *
     * @param currentTimeMilliseconds 目前時間
     * @param startTimeMilliseconds 活動開始時間
     * @return
     */
    public static boolean isPromoteStart(long currentTimeMilliseconds, long startTimeMilliseconds) {
        return currentTimeMilliseconds >= startTimeMilliseconds;
    }

    /**
     * 檢查活動是否已結束
     * @param currentTimeMilliseconds 目前時間
     * @param endTimeMilliseconds 活動結束時間
     * @return
     */
    public static boolean isPromoteEnd(long currentTimeMilliseconds, long endTimeMilliseconds) {
        return currentTimeMilliseconds > endTimeMilliseconds;
    }
}
