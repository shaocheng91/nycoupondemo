package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public final class PhpCouponElement extends AbstractBaseModel implements Parcelable {

    public String name;
    public int count_limit;
    public int usage_limit;
    public String store;
    public String description;
    public String remark;
    public String use_start_date;
    public String use_end_date;
    public int type_id;
    public String kind;
    public boolean isEnabled;
    public boolean user_take_status;
    public boolean user_usage_status;
    public int user_coupon_id;
    public String serial_number_origin_type;


    public PhpCouponElement() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.name);
        dest.writeInt(this.count_limit);
        dest.writeInt(this.usage_limit);
        dest.writeString(this.store);
        dest.writeString(this.description);
        dest.writeString(this.remark);
        dest.writeString(this.use_start_date);
        dest.writeString(this.use_end_date);
        dest.writeInt(this.type_id);
        dest.writeString(this.kind);
        dest.writeByte(isEnabled ? (byte) 1 : (byte) 0);
        dest.writeByte(user_take_status ? (byte) 1 : (byte) 0);
        dest.writeByte(user_usage_status ? (byte) 1 : (byte) 0);
        dest.writeInt(this.user_coupon_id);
        dest.writeString(this.serial_number_origin_type);
    }

    protected PhpCouponElement(Parcel in) {
        super(in);
        this.name = in.readString();
        this.count_limit = in.readInt();
        this.usage_limit = in.readInt();
        this.store = in.readString();
        this.description = in.readString();
        this.remark = in.readString();
        this.use_start_date = in.readString();
        this.use_end_date = in.readString();
        this.type_id = in.readInt();
        this.kind = in.readString();
        this.isEnabled = in.readByte() != 0;
        this.user_take_status = in.readByte() != 0;
        this.user_usage_status = in.readByte() != 0;
        this.user_coupon_id = in.readInt();
        this.serial_number_origin_type = in.readString();
    }

    public static final Creator<PhpCouponElement> CREATOR = new Creator<PhpCouponElement>() {
        public PhpCouponElement createFromParcel(Parcel source) {
            return new PhpCouponElement(source);
        }

        public PhpCouponElement[] newArray(int size) {
            return new PhpCouponElement[size];
        }
    };
}
