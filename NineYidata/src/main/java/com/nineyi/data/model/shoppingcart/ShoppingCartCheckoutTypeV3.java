package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShoppingCartCheckoutTypeV3 implements Parcelable {

    public String Type;
    public String DisplayName;
    public String DeliveryTypeDefGroup;

    public ShoppingCartCheckoutTypeV3() {
    }

    // Parcelable management
    private ShoppingCartCheckoutTypeV3(Parcel in) {
        Type = in.readString();
        DisplayName = in.readString();
        DeliveryTypeDefGroup = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Type);
        dest.writeString(DisplayName);
        dest.writeString(DeliveryTypeDefGroup);
    }

    public static final Parcelable.Creator<ShoppingCartCheckoutTypeV3> CREATOR
            = new Parcelable.Creator<ShoppingCartCheckoutTypeV3>() {
        public ShoppingCartCheckoutTypeV3 createFromParcel(Parcel in) {
            return new ShoppingCartCheckoutTypeV3(in);
        }

        public ShoppingCartCheckoutTypeV3[] newArray(int size) {
            return new ShoppingCartCheckoutTypeV3[size];
        }
    };
}
