package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBCommentFrom implements Parcelable {

    String name;
    String id;

    protected FBCommentFrom(Parcel in) {
        name = in.readString();
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
    }

    public static final Parcelable.Creator<FBCommentFrom> CREATOR = new Parcelable.Creator<FBCommentFrom>() {
        @Override
        public FBCommentFrom createFromParcel(Parcel in) {
            return new FBCommentFrom(in);
        }

        @Override
        public FBCommentFrom[] newArray(int size) {
            return new FBCommentFrom[size];
        }
    };
}