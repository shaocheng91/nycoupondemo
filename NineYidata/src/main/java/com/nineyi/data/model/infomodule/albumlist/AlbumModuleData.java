
package com.nineyi.data.model.infomodule.albumlist;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class AlbumModuleData implements Parcelable {

    @SerializedName("List")
    @Expose
    private ArrayList<AlbumModuleDataList> list = new ArrayList<AlbumModuleDataList>();
    @SerializedName("Shop")
    @Expose
    private AlbumModuleDataShop shop;

    /**
     * 
     * @return
     *     The list
     */
    public ArrayList<AlbumModuleDataList> getList() {
        return list;
    }

    /**
     * 
     * @param list
     *     The List
     */
    public void setList(ArrayList<AlbumModuleDataList> list) {
        this.list = list;
    }

    /**
     * 
     * @return
     *     The shop
     */
    public AlbumModuleDataShop getShop() {
        return shop;
    }

    /**
     * 
     * @param shop
     *     The Shop
     */
    public void setShop(AlbumModuleDataShop shop) {
        this.shop = shop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.list);
        dest.writeParcelable(this.shop, flags);
    }

    public AlbumModuleData() {
    }

    protected AlbumModuleData(Parcel in) {
        this.list = new ArrayList<AlbumModuleDataList>();
        in.readList(this.list, AlbumModuleDataList.class.getClassLoader());
        this.shop = in.readParcelable(AlbumModuleDataShop.class.getClassLoader());
    }

    public static final Parcelable.Creator<AlbumModuleData> CREATOR = new Parcelable.Creator<AlbumModuleData>() {
        @Override
        public AlbumModuleData createFromParcel(Parcel source) {
            return new AlbumModuleData(source);
        }

        @Override
        public AlbumModuleData[] newArray(int size) {
            return new AlbumModuleData[size];
        }
    };
}
