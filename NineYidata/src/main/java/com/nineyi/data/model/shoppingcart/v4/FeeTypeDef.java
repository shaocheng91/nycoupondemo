package com.nineyi.data.model.shoppingcart.v4;

/**
 * Created by tedliang on 2016/5/23.
 */
public enum FeeTypeDef {
    Fixed,  //固定運費
    Free,   //免運
    OverPrice; //滿額免運
}
