package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2017/11/6.
 */

public class ECouponVerifyData implements Parcelable {

    public int ECouponId;
    public int ECouponSlaveId;
    public NyBarCode VipMemberBarCode;
    public NyBarCode ECouponCode;
    public boolean IsUsingCustomVerificationCode;
    public NyBarCode ECouponCustomVerificationCode1;
    public NyBarCode ECouponCustomVerificationCode2;
    public NyBarCode ECouponCustomVerificationCode3;

    public ECouponVerifyData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ECouponId);
        dest.writeInt(this.ECouponSlaveId);
        dest.writeParcelable(this.VipMemberBarCode, flags);
        dest.writeParcelable(this.ECouponCode, flags);
        dest.writeByte(this.IsUsingCustomVerificationCode ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.ECouponCustomVerificationCode1, flags);
        dest.writeParcelable(this.ECouponCustomVerificationCode2, flags);
        dest.writeParcelable(this.ECouponCustomVerificationCode3, flags);
    }

    protected ECouponVerifyData(Parcel in) {
        this.ECouponId = in.readInt();
        this.ECouponSlaveId = in.readInt();
        this.VipMemberBarCode = in.readParcelable(NyBarCode.class.getClassLoader());
        this.ECouponCode = in.readParcelable(NyBarCode.class.getClassLoader());
        this.IsUsingCustomVerificationCode = in.readByte() != 0;
        this.ECouponCustomVerificationCode1 = in.readParcelable(NyBarCode.class.getClassLoader());
        this.ECouponCustomVerificationCode2 = in.readParcelable(NyBarCode.class.getClassLoader());
        this.ECouponCustomVerificationCode3 = in.readParcelable(NyBarCode.class.getClassLoader());
    }

    public static final Creator<ECouponVerifyData> CREATOR = new Creator<ECouponVerifyData>() {
        @Override
        public ECouponVerifyData createFromParcel(Parcel source) {
            return new ECouponVerifyData(source);
        }

        @Override
        public ECouponVerifyData[] newArray(int size) {
            return new ECouponVerifyData[size];
        }
    };
}
