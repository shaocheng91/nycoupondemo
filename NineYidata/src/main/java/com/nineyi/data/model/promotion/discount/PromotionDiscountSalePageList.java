
package com.nineyi.data.model.promotion.discount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class PromotionDiscountSalePageList implements Parcelable {

    @SerializedName("SalePageId")
    @Expose
    private int salePageId;
    @SerializedName("SalePageImageUrl")
    @Expose
    private String salePageImageUrl;

    /**
     * 
     * @return
     *     The salePageId
     */
    public int getSalePageId() {
        return salePageId;
    }

    /**
     * 
     * @param salePageId
     *     The SalePageId
     */
    public void setSalePageId(int salePageId) {
        this.salePageId = salePageId;
    }

    /**
     * 
     * @return
     *     The salePageImageUrl
     */
    public String getSalePageImageUrl() {
        return salePageImageUrl;
    }

    /**
     * 
     * @param salePageImageUrl
     *     The SalePageImageUrl
     */
    public void setSalePageImageUrl(String salePageImageUrl) {
        this.salePageImageUrl = salePageImageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.salePageId);
        dest.writeString(this.salePageImageUrl);
    }

    public PromotionDiscountSalePageList() {
    }

    protected PromotionDiscountSalePageList(Parcel in) {
        this.salePageId = in.readInt();
        this.salePageImageUrl = in.readString();
    }

    public static final Parcelable.Creator<PromotionDiscountSalePageList> CREATOR
            = new Parcelable.Creator<PromotionDiscountSalePageList>() {
        @Override
        public PromotionDiscountSalePageList createFromParcel(Parcel source) {
            return new PromotionDiscountSalePageList(source);
        }

        @Override
        public PromotionDiscountSalePageList[] newArray(int size) {
            return new PromotionDiscountSalePageList[size];
        }
    };
}
