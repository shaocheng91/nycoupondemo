package com.nineyi.data.model.shopapp;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShopStatus implements Parcelable {

    public boolean IsEnable;

    public ShopStatus() {
    }

    // Parcelable management
    private ShopStatus(Parcel in) {
        IsEnable = in.readInt() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(IsEnable ? 1 : 0);
    }

    public static final Parcelable.Creator<ShopStatus> CREATOR = new Parcelable.Creator<ShopStatus>() {
        public ShopStatus createFromParcel(Parcel in) {
            return new ShopStatus(in);
        }

        public ShopStatus[] newArray(int size) {
            return new ShopStatus[size];
        }
    };
}
