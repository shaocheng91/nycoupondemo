
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class DisplayShippingTypeList implements Parcelable {
    public static final String DELIVERY_TYPE_LIST = "DeliveryTypeList";

    @SerializedName("ShippingProfileTypeDef")
    @Expose
    private String ShippingProfileTypeDef;
    @SerializedName("TotalFee")
    @Expose
    private Double TotalFee;
    @SerializedName("IsRecommand")
    @Expose
    private Boolean IsRecommand;
    @SerializedName(DELIVERY_TYPE_LIST)
    @Expose
    private List<DeliveryTypeList> mDeliveryTypeLists = new ArrayList<DeliveryTypeList>();

    /**
     * 
     * @return
     *     The ShippingProfileTypeDef
     */
    public String getShippingProfileTypeDef() {
        return ShippingProfileTypeDef;
    }

    /**
     * 
     * @param ShippingProfileTypeDef
     *     The ShippingProfileTypeDef
     */
    public void setShippingProfileTypeDef(String ShippingProfileTypeDef) {
        this.ShippingProfileTypeDef = ShippingProfileTypeDef;
    }

    /**
     * 
     * @return
     *     The TotalFee
     */
    public Double getTotalFee() {
        return TotalFee;
    }

    /**
     * 
     * @param TotalFee
     *     The TotalFee
     */
    public void setTotalFee(Double TotalFee) {
        this.TotalFee = TotalFee;
    }

    /**
     * 
     * @return
     *     The IsRecommand
     */
    public Boolean getIsRecommand() {
        return IsRecommand;
    }

    /**
     * 
     * @param IsRecommand
     *     The IsRecommand
     */
    public void setIsRecommand(Boolean IsRecommand) {
        this.IsRecommand = IsRecommand;
    }

    /**
     * 
     * @return
     *     The mDeliveryTypeLists
     */
    public List<DeliveryTypeList> getDeliveryTypeLists() {
        return mDeliveryTypeLists;
    }

    /**
     * 
     * @param DeliveryTypeList
     *     The mDeliveryTypeLists
     */
    public void setDeliveryTypeLists(List<DeliveryTypeList> DeliveryTypeList) {
        this.mDeliveryTypeLists = DeliveryTypeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ShippingProfileTypeDef);
        dest.writeValue(this.TotalFee);
        dest.writeValue(this.IsRecommand);
        dest.writeTypedList(this.mDeliveryTypeLists);
    }

    public DisplayShippingTypeList() {
    }

    protected DisplayShippingTypeList(Parcel in) {
        this.ShippingProfileTypeDef = in.readString();
        this.TotalFee = (Double) in.readValue(Double.class.getClassLoader());
        this.IsRecommand = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mDeliveryTypeLists = in.createTypedArrayList(DeliveryTypeList.CREATOR);
    }

    public static final Parcelable.Creator<DisplayShippingTypeList> CREATOR
            = new Parcelable.Creator<DisplayShippingTypeList>() {
        @Override
        public DisplayShippingTypeList createFromParcel(Parcel source) {
            return new DisplayShippingTypeList(source);
        }

        @Override
        public DisplayShippingTypeList[] newArray(int size) {
            return new DisplayShippingTypeList[size];
        }
    };
}
