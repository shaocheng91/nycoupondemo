package com.nineyi.data.model.newlbs;

import java.util.List;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import java.util.ArrayList;


public class LocationList implements Parcelable{

    private static final String FIELD_GALLERY = "Gallery";
    private static final String FIELD_WEEKEND_TIME = "WeekendTime";
    private static final String FIELD_STATUS = "Status";
    private static final String FIELD_ZIP_CODE = "ZipCode";
    private static final String FIELD_WIFI = "Wifi";
    private static final String FIELD_START_DATE_TIME = "StartDateTime";
    private static final String FIELD_LATITUDE = "Latitude";
    private static final String FIELD_MOBILE = "Mobile";
    private static final String FIELD_PARKING = "Parking";
    private static final String FIELD_TEL = "Tel";
    private static final String FIELD_IMAGE_URL = "ImageUrl";
    private static final String FIELD_SEQUENCE = "Sequence";
    private static final String FIELD_UUID = "Uuid";
    private static final String FIELD_CITY_NAME = "CityName";
    private static final String FIELD_LONGITUDE = "Longitude";
    private static final String FIELD_GALLERY_COUNT = "GalleryCount";
    private static final String FIELD_DOMESTIC = "Domestic";
    private static final String FIELD_CITY_ID = "CityId";
    private static final String FIELD_SHOP_ID = "ShopId";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_DISTANCE = "Distance";
    private static final String FIELD_AREA_NAME = "AreaName";
    private static final String FIELD_END_DATE_TIME = "EndDateTime";
    private static final String FIELD_OPERATION_TIME = "OperationTime";
    private static final String FIELD_CREDIT_CARD = "CreditCard";
    private static final String FIELD_TEL_PREPEND = "TelPrepend";
    private static final String FIELD_ADDRESS = "Address";
    private static final String FIELD_NORMAL_TIME = "NormalTime";
    private static final String FIELD_GALLERY_LIST = "GalleryList";
    private static final String FIELD_REMARK = "Remark";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_AREA_ID = "AreaId";
    private static final String FIELD_INTRODUCTION = "Introduction";


    @SerializedName(FIELD_GALLERY)
    private String mGallery;
    @SerializedName(FIELD_WEEKEND_TIME)
    private String mWeekendTime;
    @SerializedName(FIELD_STATUS)
    private String mStatus;
    @SerializedName(FIELD_ZIP_CODE)
    private String mZipCode;
    @SerializedName(FIELD_WIFI)
    private int mWifi;
    @SerializedName(FIELD_START_DATE_TIME)
    private String mStartDateTime;
    @SerializedName(FIELD_LATITUDE)
    private String mLatitude;
    @SerializedName(FIELD_MOBILE)
    private String mMobile;
    @SerializedName(FIELD_PARKING)
    private int mParking;
    @SerializedName(FIELD_TEL)
    private String mTel;
    @SerializedName(FIELD_IMAGE_URL)
    private String mImageUrl;
    @SerializedName(FIELD_SEQUENCE)
    private int mSequence;
    @SerializedName(FIELD_UUID)
    private String mUuid;
    @SerializedName(FIELD_CITY_NAME)
    private String mCityName;
    @SerializedName(FIELD_LONGITUDE)
    private String mLongitude;
    @SerializedName(FIELD_GALLERY_COUNT)
    private String mGalleryCount;
    @SerializedName(FIELD_DOMESTIC)
    private int mDomestic;
    @SerializedName(FIELD_CITY_ID)
    private String mCityId;
    @SerializedName(FIELD_SHOP_ID)
    private int mShopId;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_DISTANCE)
    private String mDistance;
    @SerializedName(FIELD_AREA_NAME)
    private String mAreaName;
    @SerializedName(FIELD_END_DATE_TIME)
    private String mEndDateTime;
    @SerializedName(FIELD_OPERATION_TIME)
    private String mOperationTime;
    @SerializedName(FIELD_CREDIT_CARD)
    private int mCreditCard;
    @SerializedName(FIELD_TEL_PREPEND)
    private String mTelPrepend;
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @SerializedName(FIELD_NORMAL_TIME)
    private String mNormalTime;
    @SerializedName(FIELD_GALLERY_LIST)
    private ArrayList<String> mGalleryLists;
    @SerializedName(FIELD_REMARK)
    private String mRemark;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_AREA_ID)
    private String mAreaId;
    @SerializedName(FIELD_INTRODUCTION)
    private String mIntroduction;


    public LocationList(){

    }

    public void setGallery(String gallery) {
        mGallery = gallery;
    }

    public String getGallery() {
        return mGallery;
    }

    public void setWeekendTime(String weekendTime) {
        mWeekendTime = weekendTime;
    }

    public String getWeekendTime() {
        return mWeekendTime;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setZipCode(String zipCode) {
        mZipCode = zipCode;
    }

    public String getZipCode() {
        return mZipCode;
    }

    public void setWifi(int wifi) {
        mWifi = wifi;
    }

    public int getWifi() {
        return mWifi;
    }

    public void setStartDateTime(String startDateTime) {
        mStartDateTime = startDateTime;
    }

    public String getStartDateTime() {
        return mStartDateTime;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setParking(int parking) {
        mParking = parking;
    }

    public int getParking() {
        return mParking;
    }

    public void setTel(String tel) {
        mTel = tel;
    }

    public String getTel() {
        return mTel;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setSequence(int sequence) {
        mSequence = sequence;
    }

    public int getSequence() {
        return mSequence;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setCityName(String cityName) {
        mCityName = cityName;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setGalleryCount(String galleryCount) {
        mGalleryCount = galleryCount;
    }

    public String getGalleryCount() {
        return mGalleryCount;
    }

    public void setDomestic(int domestic) {
        mDomestic = domestic;
    }

    public int getDomestic() {
        return mDomestic;
    }

    public void setCityId(String cityId) {
        mCityId = cityId;
    }

    public String getCityId() {
        return mCityId;
    }

    public void setShopId(int shopId) {
        mShopId = shopId;
    }

    public int getShopId() {
        return mShopId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setDistance(String distance) {
        mDistance = distance;
    }

    public String getDistance() {
        return mDistance;
    }

    public void setAreaName(String areaName) {
        mAreaName = areaName;
    }

    public String getAreaName() {
        return mAreaName;
    }

    public void setEndDateTime(String endDateTime) {
        mEndDateTime = endDateTime;
    }

    public String getEndDateTime() {
        return mEndDateTime;
    }

    public void setOperationTime(String operationTime) {
        mOperationTime = operationTime;
    }

    public String getOperationTime() {
        return mOperationTime;
    }

    public void setCreditCard(int creditCard) {
        mCreditCard = creditCard;
    }

    public int getCreditCard() {
        return mCreditCard;
    }

    public void setTelPrepend(String telPrepend) {
        mTelPrepend = telPrepend;
    }

    public String getTelPrepend() {
        return mTelPrepend;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setNormalTime(String normalTime) {
        mNormalTime = normalTime;
    }

    public String getNormalTime() {
        return mNormalTime;
    }

    public void setGalleryLists(ArrayList<String> galleryLists) {
        mGalleryLists = galleryLists;
    }

    public ArrayList<String> getGalleryLists() {
        return mGalleryLists;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setAreaId(String areaId) {
        mAreaId = areaId;
    }

    public String getAreaId() {
        return mAreaId;
    }

    public void setIntroduction(String introduction) {
        mIntroduction = introduction;
    }

    public String getIntroduction() {
        return mIntroduction;
    }

    public LocationList(Parcel in) {
        mGallery = in.readString();
        mWeekendTime = in.readString();
        mStatus = in.readString();
        mZipCode = in.readString();
        mWifi = in.readInt();
        mStartDateTime = in.readString();
        mLatitude = in.readString();
        mMobile = in.readString();
        mParking = in.readInt();
        mTel = in.readString();
        mImageUrl = in.readString();
        mSequence = in.readInt();
        mUuid = in.readString();
        mCityName = in.readString();
        mLongitude = in.readString();
        mGalleryCount = in.readString();
        mDomestic = in.readInt();
        mCityId = in.readString();
        mShopId = in.readInt();
        mId = in.readInt();
        mDistance = in.readString();
        mAreaName = in.readString();
        mEndDateTime = in.readString();
        mOperationTime = in.readString();
        mCreditCard = in.readInt();
        mTelPrepend = in.readString();
        mAddress = in.readString();
        mNormalTime = in.readString();
        in.readArrayList(String.class.getClassLoader());
        mRemark = in.readString();
        mName = in.readString();
        mAreaId = in.readString();
        mIntroduction = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationList> CREATOR = new Creator<LocationList>() {
        public LocationList createFromParcel(Parcel in) {
            return new LocationList(in);
        }

        public LocationList[] newArray(int size) {
            return new LocationList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mGallery);
        dest.writeString(mWeekendTime);
        dest.writeString(mStatus);
        dest.writeString(mZipCode);
        dest.writeInt(mWifi);
        dest.writeString(mStartDateTime);
        dest.writeString(mLatitude);
        dest.writeString(mMobile);
        dest.writeInt(mParking);
        dest.writeString(mTel);
        dest.writeString(mImageUrl);
        dest.writeInt(mSequence);
        dest.writeString(mUuid);
        dest.writeString(mCityName);
        dest.writeString(mLongitude);
        dest.writeString(mGalleryCount);
        dest.writeInt(mDomestic);
        dest.writeString(mCityId);
        dest.writeInt(mShopId);
        dest.writeInt(mId);
        dest.writeString(mDistance);
        dest.writeString(mAreaName);
        dest.writeString(mEndDateTime);
        dest.writeString(mOperationTime);
        dest.writeInt(mCreditCard);
        dest.writeString(mTelPrepend);
        dest.writeString(mAddress);
        dest.writeString(mNormalTime);
        dest.writeList(mGalleryLists);
        dest.writeString(mRemark);
        dest.writeString(mName);
        dest.writeString(mAreaId);
        dest.writeString(mIntroduction);
    }


}