package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class LocationListByCityRoot implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private LocationListByCityData mDatum;


    public LocationListByCityRoot(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(LocationListByCityData datum) {
        mDatum = datum;
    }

    public LocationListByCityData getDatum() {
        return mDatum;
    }

    public LocationListByCityRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mDatum = in.readParcelable(LocationListByCityData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationListByCityRoot> CREATOR = new Creator<LocationListByCityRoot>() {
        public LocationListByCityRoot createFromParcel(Parcel in) {
            return new LocationListByCityRoot(in);
        }

        public LocationListByCityRoot[] newArray(int size) {
            return new LocationListByCityRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        dest.writeParcelable(mDatum, flags);
    }


}