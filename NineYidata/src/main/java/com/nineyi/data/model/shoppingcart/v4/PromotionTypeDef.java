package com.nineyi.data.model.shoppingcart.v4;

/**
 * Created by Willy on 16/6/11.
 */
public enum PromotionTypeDef {
    ReachQty,
    TotalPrice,
    TotalQty,
    TotalPriceFreeGift
}
