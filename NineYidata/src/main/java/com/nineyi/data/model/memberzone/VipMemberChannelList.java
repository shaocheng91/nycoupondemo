package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class VipMemberChannelList implements Parcelable {

    private static final String FIELD_CHANNEL_NAME = "ChannelName";
    private static final String FIELD_CHANNEL_VALUE = "ChannelValue";
    private static final String FIELD_CHANNEL_TYPE = "ChannelType";

    @SerializedName(FIELD_CHANNEL_NAME)
    private String mChannelName;

    @SerializedName(FIELD_CHANNEL_VALUE)
    private String ChannelValue;

    @SerializedName(FIELD_CHANNEL_TYPE)
    private String mChannelType;

    public VipMemberChannelList() {
    }

    public String getChannelName() {
        return mChannelName;
    }

    public String getChannelValue() {
        return ChannelValue;
    }

    public String getChannelType() {
        return mChannelType;
    }

    public VipMemberChannelList(Parcel in) {
        mChannelName = in.readString();
        ChannelValue = in.readString();
        mChannelType = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipMemberChannelList> CREATOR = new Creator<VipMemberChannelList>() {
        public VipMemberChannelList createFromParcel(Parcel in) {
            return new VipMemberChannelList(in);
        }

        public VipMemberChannelList[] newArray(int size) {
            return new VipMemberChannelList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mChannelName);
        dest.writeString(ChannelValue);
        dest.writeString(mChannelType);
    }


}