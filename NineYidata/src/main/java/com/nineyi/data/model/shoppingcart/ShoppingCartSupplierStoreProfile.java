package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by tedliang on 15/10/21.
 */
public class ShoppingCartSupplierStoreProfile implements Parcelable {
    public int Id;
    public String DistributorDef;
    public String MasterCode;
    public String StoreSlaveCode;
    public String TemperatureTypeDef;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.DistributorDef);
        dest.writeString(this.MasterCode);
        dest.writeString(this.StoreSlaveCode);
        dest.writeString(this.TemperatureTypeDef);
    }

    public ShoppingCartSupplierStoreProfile() {
    }

    private ShoppingCartSupplierStoreProfile(Parcel in) {
        this.Id = in.readInt();
        this.DistributorDef = in.readString();
        this.MasterCode = in.readString();
        this.StoreSlaveCode = in.readString();
        this.TemperatureTypeDef = in.readString();
    }

    public static final Parcelable.Creator<ShoppingCartSupplierStoreProfile> CREATOR
            = new Parcelable.Creator<ShoppingCartSupplierStoreProfile>() {
        public ShoppingCartSupplierStoreProfile createFromParcel(Parcel source) {
            return new ShoppingCartSupplierStoreProfile(source);
        }

        public ShoppingCartSupplierStoreProfile[] newArray(int size) {
            return new ShoppingCartSupplierStoreProfile[size];
        }
    };
}
