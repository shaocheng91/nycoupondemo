package com.nineyi.data.model.promotion.v2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kelsey on 2017/2/8.
 */

public class PromotionTargetMemberTierList implements Parcelable {
    private int PromotionTargetMemberTierId;
    private String CrmShopMemberCardName;
    private boolean IsMatch;

    public String getCrmShopMemberCardName() {
        return CrmShopMemberCardName;
    }

    public void setCrmShopMemberCardName(String crmShopMemberCardName) {
        CrmShopMemberCardName = crmShopMemberCardName;
    }

    public int getPromotionTargetMemberTierId() {
        return PromotionTargetMemberTierId;
    }

    public void setPromotionTargetMemberTierId(int promotionTargetMemberTierId) {
        PromotionTargetMemberTierId = promotionTargetMemberTierId;
    }

    public boolean isMatch() {
        return IsMatch;
    }

    public void setMatch(boolean match) {
        IsMatch = match;
    }

    public PromotionTargetMemberTierList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.PromotionTargetMemberTierId);
        dest.writeString(this.CrmShopMemberCardName);
        dest.writeByte(this.IsMatch ? (byte) 1 : (byte) 0);
    }

    protected PromotionTargetMemberTierList(Parcel in) {
        this.PromotionTargetMemberTierId = in.readInt();
        this.CrmShopMemberCardName = in.readString();
        this.IsMatch = in.readByte() != 0;
    }

    public static final Creator<PromotionTargetMemberTierList> CREATOR = new Creator<PromotionTargetMemberTierList>() {
        @Override
        public PromotionTargetMemberTierList createFromParcel(Parcel source) {
            return new PromotionTargetMemberTierList(source);
        }

        @Override
        public PromotionTargetMemberTierList[] newArray(int size) {
            return new PromotionTargetMemberTierList[size];
        }
    };
}
