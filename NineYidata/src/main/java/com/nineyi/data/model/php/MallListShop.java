package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class MallListShop implements Parcelable {

    private static final String FIELD_THEME_COLOR = "theme_color";
    private static final String FIELD_ID = "id";
    private static final String FIELD_TITLE = "title";


    @SerializedName(FIELD_THEME_COLOR)
    private int mThemeColor;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_TITLE)
    private String mTitle;


    public MallListShop() {

    }

    public void setThemeColor(int themeColor) {
        mThemeColor = themeColor;
    }

    public int getThemeColor() {
        return mThemeColor;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public MallListShop(Parcel in) {
        mThemeColor = in.readInt();
        mId = in.readInt();
        mTitle = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MallListShop> CREATOR = new Creator<MallListShop>() {
        public MallListShop createFromParcel(Parcel in) {
            return new MallListShop(in);
        }

        public MallListShop[] newArray(int size) {
            return new MallListShop[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mThemeColor);
        dest.writeInt(mId);
        dest.writeString(mTitle);
    }


}