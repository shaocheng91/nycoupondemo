
package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.salepage.SalePageMoreInfo;
import com.nineyi.data.model.salepage.SalePageNotKeyProperty;

public class SalePageAdditionalInfoData implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ShopId")
    @Expose
    private Integer shopId;
    @SerializedName("MoreInfo")
    @Expose
    private SalePageMoreInfo moreInfo;
    @SerializedName("MoreInfoVideo")
    @Expose
    private MainImageVideo moreInfoVideo;
    @SerializedName("NotKeyPropertyList")
    @Expose
    private List<SalePageNotKeyProperty> notKeyPropertyList = null;

    protected SalePageAdditionalInfoData(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        shopId = in.readByte() == 0x00 ? null : in.readInt();
        moreInfo = (SalePageMoreInfo) in.readValue(SalePageMoreInfo.class.getClassLoader());
        moreInfoVideo = (MainImageVideo) in.readValue(MainImageVideo.class.getClassLoader());
        if (in.readByte() == 0x01) {
            notKeyPropertyList = new ArrayList<>();
            in.readList(notKeyPropertyList, SalePageNotKeyProperty.class.getClassLoader());
        } else {
            notKeyPropertyList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        if (shopId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(shopId);
        }
        dest.writeValue(moreInfo);
        dest.writeValue(moreInfoVideo);
        if (notKeyPropertyList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(notKeyPropertyList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SalePageAdditionalInfoData> CREATOR = new Parcelable.Creator<SalePageAdditionalInfoData>() {
        @Override
        public SalePageAdditionalInfoData createFromParcel(Parcel in) {
            return new SalePageAdditionalInfoData(in);
        }

        @Override
        public SalePageAdditionalInfoData[] newArray(int size) {
            return new SalePageAdditionalInfoData[size];
        }
    };

    public SalePageMoreInfo getMoreInfo() {
        return moreInfo;
    }

    public ArrayList<SalePageNotKeyProperty> getNotKeyPropertyList() {
        return new ArrayList<>(notKeyPropertyList);
    }

    public MainImageVideo getMainImageVideo() {
        return moreInfoVideo;
    }
}
