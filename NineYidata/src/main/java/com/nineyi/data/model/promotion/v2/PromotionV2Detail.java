package com.nineyi.data.model.promotion.v2;


/**
 * Created by tedliang on 2016/8/22.
 */
public class PromotionV2Detail {

    /**
     * ReturnCode : API0001
     * Message : Success
     * Data : {"PromotionId":123,"Title":"夏日洋裝滿299送防曬乳","Description":"我是活動描述，後台設定上限400字，但多數廠商都只是把標題複製過來放而已，都短短的，最多兩行，超過時顯示看更多，點看更多打開popup，顯示完整描述，拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉","StartDateTime":"/Date(1471489200000)/","EndDateTime":"/Date(1475249400000)/","PromotionTargetType":"PromotionSalePage","PromotionImageUrl":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Promotion%20Image&w=350&h=150","ConditionList":[{"Type":"TotalQty","DiscountType":"DiscountRate","TotalQty":5,"TotalPrice":0,"DiscountPrice":0,"DiscountRate":0.8},{"Type":"TotalQty","DiscountType":"DiscountRate","TotalQty":7,"TotalPrice":0,"DiscountPrice":0,"DiscountRate":0.6},{"Type":"TotalQty","DiscountType":"DiscountRate","TotalQty":10,"TotalPrice":0,"DiscountPrice":0,"DiscountRate":0.3}],"PromotionGiftList":[],"CategoryList":[{"CategoryId":21788,"Title":"人氣推薦","SalePageCount":5,"ParentId":0,"Sort":0,"IsParent":false,"Level":1,"ChildList":[]},{"CategoryId":22923,"Title":"沒人氣推薦","SalePageCount":10,"ParentId":0,"Sort":0,"IsParent":false,"Level":1,"ChildList":[]},{"CategoryId":22966,"Title":"超低溫","SalePageCount":999,"ParentId":0,"Sort":0,"IsParent":false,"Level":1,"ChildList":[]},{"CategoryId":22966,"Title":"超低溫","SalePageCount":1110,"ParentId":0,"Sort":0,"IsParent":true,"Level":1,"ChildList":[{"CategoryId":21790,"Title":"冷凍","SalePageCount":10,"ParentId":22966,"Sort":0,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":21791,"Title":"冷藏","SalePageCount":100,"ParentId":22966,"Sort":0,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22906,"Title":"常溫","SalePageCount":1000,"ParentId":22966,"Sort":0,"IsParent":false,"Level":2,"ChildList":[]}]},{"CategoryId":22475,"Title":"金物流Phrase1","SalePageCount":111110,"ParentId":0,"Sort":15,"IsParent":true,"Level":1,"ChildList":[{"CategoryId":22476,"Title":"Tag","SalePageCount":10,"ParentId":22475,"Sort":1,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22477,"Title":"商品頁加金物流","SalePageCount":100,"ParentId":22475,"Sort":2,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22478,"Title":"可下單商品","SalePageCount":1000,"ParentId":22475,"Sort":3,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22479,"Title":"購物車交集","SalePageCount":10000,"ParentId":22475,"Sort":4,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22504,"Title":"活動5000","SalePageCount":100000,"ParentId":22475,"Sort":5,"IsParent":false,"Level":2,"ChildList":[]}]}]}
     */

    private String ReturnCode;
    private String Message;
    /**
     * PromotionId : 123
     * Title : 夏日洋裝滿299送防曬乳
     * Description : 我是活動描述，後台設定上限400字，但多數廠商都只是把標題複製過來放而已，都短短的，最多兩行，超過時顯示看更多，點看更多打開popup，顯示完整描述，拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉拉
     * StartDateTime : /Date(1471489200000)/
     * EndDateTime : /Date(1475249400000)/
     * PromotionTargetType : PromotionSalePage
     * PromotionImageUrl : https://placeholdit.imgix.net/~text?txtsize=33&txt=Promotion%20Image&w=350&h=150
     * ConditionList : [{"Type":"TotalQty","DiscountType":"DiscountRate","TotalQty":5,"TotalPrice":0,"DiscountPrice":0,"DiscountRate":0.8},{"Type":"TotalQty","DiscountType":"DiscountRate","TotalQty":7,"TotalPrice":0,"DiscountPrice":0,"DiscountRate":0.6},{"Type":"TotalQty","DiscountType":"DiscountRate","TotalQty":10,"TotalPrice":0,"DiscountPrice":0,"DiscountRate":0.3}]
     * PromotionGiftList : []
     * CategoryList : [{"CategoryId":21788,"Title":"人氣推薦","SalePageCount":5,"ParentId":0,"Sort":0,"IsParent":false,"Level":1,"ChildList":[]},{"CategoryId":22923,"Title":"沒人氣推薦","SalePageCount":10,"ParentId":0,"Sort":0,"IsParent":false,"Level":1,"ChildList":[]},{"CategoryId":22966,"Title":"超低溫","SalePageCount":999,"ParentId":0,"Sort":0,"IsParent":false,"Level":1,"ChildList":[]},{"CategoryId":22966,"Title":"超低溫","SalePageCount":1110,"ParentId":0,"Sort":0,"IsParent":true,"Level":1,"ChildList":[{"CategoryId":21790,"Title":"冷凍","SalePageCount":10,"ParentId":22966,"Sort":0,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":21791,"Title":"冷藏","SalePageCount":100,"ParentId":22966,"Sort":0,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22906,"Title":"常溫","SalePageCount":1000,"ParentId":22966,"Sort":0,"IsParent":false,"Level":2,"ChildList":[]}]},{"CategoryId":22475,"Title":"金物流Phrase1","SalePageCount":111110,"ParentId":0,"Sort":15,"IsParent":true,"Level":1,"ChildList":[{"CategoryId":22476,"Title":"Tag","SalePageCount":10,"ParentId":22475,"Sort":1,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22477,"Title":"商品頁加金物流","SalePageCount":100,"ParentId":22475,"Sort":2,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22478,"Title":"可下單商品","SalePageCount":1000,"ParentId":22475,"Sort":3,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22479,"Title":"購物車交集","SalePageCount":10000,"ParentId":22475,"Sort":4,"IsParent":false,"Level":2,"ChildList":[]},{"CategoryId":22504,"Title":"活動5000","SalePageCount":100000,"ParentId":22475,"Sort":5,"IsParent":false,"Level":2,"ChildList":[]}]}]
     */

    private PromotionV2Data Data;

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String ReturnCode) {
        this.ReturnCode = ReturnCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public PromotionV2Data getData() {
        return Data;
    }

    public void setData(PromotionV2Data Data) {
        this.Data = Data;
    }

}
