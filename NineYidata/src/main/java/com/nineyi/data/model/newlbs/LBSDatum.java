package com.nineyi.data.model.newlbs;

import java.util.List;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import java.util.ArrayList;


public class LBSDatum implements Parcelable{

    private static final String FIELD_RANGE = "Range";
    private static final String FIELD_STORE_SORT = "StoreSort";
    private static final String FIELD_LOCATION_LIST = "LocationList";
    private static final String FIELD_LBS_LIST = "LbsList";


    @SerializedName(FIELD_RANGE)
    private int mRange;
    @SerializedName(FIELD_STORE_SORT)
    private boolean mStoreSort;
    @SerializedName(FIELD_LOCATION_LIST)
    private ArrayList<LocationList> mLocationLists;
    @SerializedName(FIELD_LBS_LIST)
    private ArrayList<LbsList> mLbsLists;


    public LBSDatum(){

    }

    public void setRange(int range) {
        mRange = range;
    }

    public int getRange() {
        return mRange;
    }

    public void setStoreSort(boolean storeSort) {
        mStoreSort = storeSort;
    }

    public boolean isStoreSort() {
        return mStoreSort;
    }

    public void setLocationLists(ArrayList<LocationList> locationLists) {
        mLocationLists = locationLists;
    }

    public ArrayList<LocationList> getLocationLists() {
        return mLocationLists;
    }

    public void setLbsLists(ArrayList<LbsList> lbsLists) {
        mLbsLists = lbsLists;
    }

    public ArrayList<LbsList> getLbsLists() {
        return mLbsLists;
    }

    public LBSDatum(Parcel in) {
        mRange = in.readInt();
        mStoreSort = in.readInt() == 1 ? true: false;
new ArrayList<LocationList>();
        in.readTypedList(mLocationLists, LocationList.CREATOR);
new ArrayList<LbsList>();
        in.readTypedList(mLbsLists, LbsList.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LBSDatum> CREATOR = new Creator<LBSDatum>() {
        public LBSDatum createFromParcel(Parcel in) {
            return new LBSDatum(in);
        }

        public LBSDatum[] newArray(int size) {
            return new LBSDatum[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mRange);
        dest.writeInt(mStoreSort ? 1 : 0);
        dest.writeTypedList(mLocationLists);
        dest.writeTypedList(mLbsLists);
    }


}