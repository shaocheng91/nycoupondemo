package com.nineyi.data.model.locationwizard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Qoo on 14/12/29.
 */
public class LocationWizardMemberInfoRoot implements Parcelable {

    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_DATA)
    private LocationWizardMemberInfo mCrmMemberInfo;


    public LocationWizardMemberInfoRoot() {

    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setCrmMemberInfo(LocationWizardMemberInfo datum) {
        mCrmMemberInfo = datum;
    }

    public LocationWizardMemberInfo getCrmMemberInfo() {
        return mCrmMemberInfo;
    }

    public LocationWizardMemberInfoRoot(Parcel in) {
        mMessage = in.readString();
        mReturnCode = in.readString();
        mCrmMemberInfo = in.readParcelable(LocationWizardMemberInfo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<LocationWizardMemberInfoRoot> CREATOR
            = new Parcelable.Creator<LocationWizardMemberInfoRoot>() {
        public LocationWizardMemberInfoRoot createFromParcel(Parcel in) {
            return new LocationWizardMemberInfoRoot(in);
        }

        public LocationWizardMemberInfoRoot[] newArray(int size) {
            return new LocationWizardMemberInfoRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMessage);
        dest.writeString(mReturnCode);
        dest.writeParcelable(mCrmMemberInfo, flags);
    }
}
