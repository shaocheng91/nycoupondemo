package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2015/6/24.
 */
public class PhpCouponTakeStatus implements Parcelable {
    public String status;
    public String msg;
    public String ReturnCode;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.ReturnCode);
    }

    public PhpCouponTakeStatus() {
    }

    protected PhpCouponTakeStatus(Parcel in) {
        this.status = in.readString();
        this.msg = in.readString();
        this.ReturnCode = in.readString();
    }

    public static final Creator<PhpCouponTakeStatus> CREATOR = new Creator<PhpCouponTakeStatus>() {
        public PhpCouponTakeStatus createFromParcel(Parcel source) {
            return new PhpCouponTakeStatus(source);
        }

        public PhpCouponTakeStatus[] newArray(int size) {
            return new PhpCouponTakeStatus[size];
        }
    };
}
