package com.nineyi.data.model.promotion.v2;

/**
 * Created by tedliang on 2016/8/26.
 */
public class PromoteSalePageList {

    private int SalePageId;
    private String Title;
    private String SalePageImageUrl;
    private double Price;
    private double SuggestPrice;
    private boolean IsSoldOut;

    public int getSalePageId() {
        return SalePageId;
    }

    public void setSalePageId(int SalePageId) {
        this.SalePageId = SalePageId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getSalePageImageUrl() {
        return SalePageImageUrl;
    }

    public void setSalePageImageUrl(String SalePageImageUrl) {
        this.SalePageImageUrl = SalePageImageUrl;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public double getSuggestPrice() {
        return SuggestPrice;
    }

    public void setSuggestPrice(double SuggestPrice) {
        this.SuggestPrice = SuggestPrice;
    }

    public boolean isSoldOut() {
        return IsSoldOut;
    }

    public void setIsSoldOut(boolean IsSoldOut) {
        this.IsSoldOut = IsSoldOut;
    }
}
