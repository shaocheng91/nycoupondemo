package com.nineyi.data.model.reward;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import java.util.ArrayList;


public class RewardPointListDatum implements Parcelable{

    private static final String FIELD_REWARD_POINT_LIST = "RewardPointList";


    @SerializedName(FIELD_REWARD_POINT_LIST)
    private ArrayList<RewardPointList> mRewardPointLists;


    public RewardPointListDatum(){

    }

    public void setRewardPointLists(ArrayList<RewardPointList> rewardPointLists) {
        mRewardPointLists = rewardPointLists;
    }

    public ArrayList<RewardPointList> getRewardPointLists() {
        return mRewardPointLists;
    }

    public RewardPointListDatum(Parcel in) {
new ArrayList<RewardPointList>();
        in.readTypedList(mRewardPointLists, RewardPointList.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RewardPointListDatum> CREATOR = new Creator<RewardPointListDatum>() {
        public RewardPointListDatum createFromParcel(Parcel in) {
            return new RewardPointListDatum(in);
        }

        public RewardPointListDatum[] newArray(int size) {
            return new RewardPointListDatum[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mRewardPointLists);
    }


}