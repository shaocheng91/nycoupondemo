package com.nineyi.data.model.promotion;

import android.os.Parcel;
import android.os.Parcelable;


public class FreeGift implements Parcelable{

    public double Price;
    public double TotalPrice;
    public int SaleProductSKUId;
    public String PicUrl;
    public int SalePageId;
    public int Id;
    public int TotalQty;
    public String Title;
    public int Qty;
    public int PromotionConditionId;
    public int PromotionId;
    public boolean IsSoldOut;


    public FreeGift(){

    }

    public FreeGift(Parcel in) {
        Price = in.readInt();
        TotalPrice = in.readInt();
        SaleProductSKUId = in.readInt();
        PicUrl = in.readString();
        SalePageId = in.readInt();
        Id = in.readInt();
        TotalQty = in.readInt();
        Title = in.readString();
        Qty = in.readInt();
        PromotionConditionId = in.readInt();
        PromotionId = in.readInt();
        IsSoldOut = in.readInt() == 1 ? true: false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<FreeGift> CREATOR = new Parcelable.Creator<FreeGift>() {
        public FreeGift createFromParcel(Parcel in) {
            return new FreeGift(in);
        }

        public FreeGift[] newArray(int size) {
            return new FreeGift[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(Price);
        dest.writeDouble(TotalPrice);
        dest.writeInt(SaleProductSKUId);
        dest.writeString(PicUrl);
        dest.writeInt(SalePageId);
        dest.writeInt(Id);
        dest.writeInt(TotalQty);
        dest.writeString(Title);
        dest.writeInt(Qty);
        dest.writeInt(PromotionConditionId);
        dest.writeInt(PromotionId);
        dest.writeInt(IsSoldOut ? 1 : 0);
    }


}