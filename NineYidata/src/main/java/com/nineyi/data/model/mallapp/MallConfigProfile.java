package com.nineyi.data.model.mallapp;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by tedliang on 2016/4/15.
 */

public class MallConfigProfile implements Parcelable {

    private static final String FIELD_MALL_REVAMP_INDEX_URI = "MallRevampIndexUri";
    private static final String FIELD_IS_LEGACY_MODE = "IsLegacyMode";


    @SerializedName(FIELD_MALL_REVAMP_INDEX_URI)
    private String mMallRevampIndexUri;
    @SerializedName(FIELD_IS_LEGACY_MODE)
    private boolean mIsLegacyMode;


    public MallConfigProfile() {

    }

    public void setMallRevampIndexUri(String mallRevampIndexUri) {
        mMallRevampIndexUri = mallRevampIndexUri;
    }

    public String getMallRevampIndexUri() {
        return mMallRevampIndexUri;
    }

    public void setIsLegacyMode(boolean isLegacyMode) {
        mIsLegacyMode = isLegacyMode;
    }

    public boolean isIsLegacyMode() {
        return mIsLegacyMode;
    }

    public MallConfigProfile(Parcel in) {
        mMallRevampIndexUri = in.readString();
        mIsLegacyMode = in.readInt() == 1 ? true : false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<MallConfigProfile> CREATOR = new Parcelable.Creator<MallConfigProfile>() {
        public MallConfigProfile createFromParcel(Parcel in) {
            return new MallConfigProfile(in);
        }

        public MallConfigProfile[] newArray(int size) {
            return new MallConfigProfile[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMallRevampIndexUri);
        dest.writeInt(mIsLegacyMode ? 1 : 0);
    }


}
