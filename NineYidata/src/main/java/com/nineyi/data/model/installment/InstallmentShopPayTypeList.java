package com.nineyi.data.model.installment;

import android.os.Parcel;
import android.os.Parcelable;


public final class InstallmentShopPayTypeList implements Parcelable {

    public boolean IsEnabled;
    public String TypeDef;
    public String TypeDefDesc;
    public int Id;
    public int ShopId;

    public InstallmentShopPayTypeList() {
    }

    // Parcelable management
    private InstallmentShopPayTypeList(Parcel in) {
        IsEnabled = in.readInt() == 1;
        TypeDef = in.readString();
        TypeDefDesc = in.readString();
        Id = in.readInt();
        ShopId = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(IsEnabled ? 1 : 0);
        dest.writeString(TypeDef);
        dest.writeString(TypeDefDesc);
        dest.writeInt(Id);
        dest.writeInt(ShopId);
    }

    public static final Parcelable.Creator<InstallmentShopPayTypeList> CREATOR
            = new Parcelable.Creator<InstallmentShopPayTypeList>() {
        public InstallmentShopPayTypeList createFromParcel(Parcel in) {
            return new InstallmentShopPayTypeList(in);
        }

        public InstallmentShopPayTypeList[] newArray(int size) {
            return new InstallmentShopPayTypeList[size];
        }
    };
}
