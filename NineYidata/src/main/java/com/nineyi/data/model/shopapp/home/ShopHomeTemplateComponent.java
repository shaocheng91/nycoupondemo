package com.nineyi.data.model.shopapp.home;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2017/4/11.
 */
public class ShopHomeTemplateComponent implements Parcelable {
    public String ComponentName;
    public String Type;
    public String AdCode;

    public ShopHomeTemplateComponent() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ComponentName);
        dest.writeString(this.Type);
        dest.writeString(this.AdCode);
    }

    protected ShopHomeTemplateComponent(Parcel in) {
        this.ComponentName = in.readString();
        this.Type = in.readString();
        this.AdCode = in.readString();
    }

    public static final Creator<ShopHomeTemplateComponent> CREATOR = new Creator<ShopHomeTemplateComponent>() {
        @Override
        public ShopHomeTemplateComponent createFromParcel(Parcel source) {
            return new ShopHomeTemplateComponent(source);
        }

        @Override
        public ShopHomeTemplateComponent[] newArray(int size) {
            return new ShopHomeTemplateComponent[size];
        }
    };
}
