package com.nineyi.data.model.memberzone;

import com.nineyi.data.model.php.AbstractBaseModel;

import android.os.Parcel;
import android.os.Parcelable;


public final class PresentStatusData extends AbstractBaseModel implements Parcelable {

    public boolean EnablePresentBtn;
    public String SourceType;
    public String SourceId;

    public PresentStatusData() {
    }

    // Parcelable management
    private PresentStatusData(Parcel in) {
        EnablePresentBtn = in.readInt() != 0;
        SourceType = in.readString();
        SourceId = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(EnablePresentBtn ? (byte) 1 : (byte) 0);
        dest.writeString(SourceType);
        dest.writeString(SourceId);
    }

    public static final Creator<PresentStatusData> CREATOR = new Creator<PresentStatusData>() {
        public PresentStatusData createFromParcel(Parcel in) {
            return new PresentStatusData(in);
        }

        public PresentStatusData[] newArray(int size) {
            return new PresentStatusData[size];
        }
    };
}
