package com.nineyi.data.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShopCategory implements Parcelable {

    public int ShopCategory_Id;
    public int ShopCategory_ParentId;
    public String ShopCategory_ParentName;
    public String ShopCategory_Name;
    public int ShopCategory_Sort;
    public boolean ShopCategory_IsParent;
    public int ShopCategory_Level;
    public int Count;
    public ArrayList<ShopCategory> ChildList;


    public ShopCategory() {
    }

    // Parcelable management
    private ShopCategory(Parcel in) {
        ShopCategory_Id = in.readInt();
        ShopCategory_ParentId = in.readInt();
        ShopCategory_ParentName = in.readString();
        ShopCategory_Name = in.readString();
        ShopCategory_Sort = in.readInt();
        ShopCategory_IsParent = in.readInt() == 1;
        ShopCategory_Level = in.readInt();
        Count = in.readInt();
        ChildList = in.createTypedArrayList(ShopCategory.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopCategory_Id);
        dest.writeInt(ShopCategory_ParentId);
        dest.writeString(ShopCategory_ParentName);
        dest.writeString(ShopCategory_Name);
        dest.writeInt(ShopCategory_Sort);
        dest.writeInt(ShopCategory_IsParent ? 1 : 0);
        dest.writeInt(ShopCategory_Level);
        dest.writeInt(Count);
        dest.writeTypedList(ChildList);
    }

    public static final Parcelable.Creator<ShopCategory> CREATOR = new Parcelable.Creator<ShopCategory>() {
        public ShopCategory createFromParcel(Parcel in) {
            return new ShopCategory(in);
        }

        public ShopCategory[] newArray(int size) {
            return new ShopCategory[size];
        }
    };

}
