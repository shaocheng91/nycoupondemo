package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBTarget implements Parcelable {

    String id;
    String url;

    protected FBTarget(Parcel in) {
        id = in.readString();
        url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(url);
    }

    public static final Creator<FBTarget> CREATOR = new Creator<FBTarget>() {
        @Override
        public FBTarget createFromParcel(Parcel in) {
            return new FBTarget(in);
        }

        @Override
        public FBTarget[] newArray(int size) {
            return new FBTarget[size];
        }
    };
}