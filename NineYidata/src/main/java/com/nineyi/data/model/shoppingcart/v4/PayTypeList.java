
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class PayTypeList implements Parcelable {

    @SerializedName("PayProfileTypeDef")
    @Expose
    private String PayProfileTypeDef;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("StatisticsTypeDef")
    @Expose
    private String StatisticsTypeDef;

    /**
     * 
     * @return
     *     The PayProfileTypeDef
     */
    public String getPayProfileTypeDef() {
        return PayProfileTypeDef;
    }

    /**
     * 
     * @param PayProfileTypeDef
     *     The PayProfileTypeDef
     */
    public void setPayProfileTypeDef(String PayProfileTypeDef) {
        this.PayProfileTypeDef = PayProfileTypeDef;
    }

    /**
     * 
     * @return
     *     The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * 
     * @param Name
     *     The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * 
     * @return
     *     The StatisticsTypeDef
     */
    public String getStatisticsTypeDef() {
        return StatisticsTypeDef;
    }

    /**
     * 
     * @param StatisticsTypeDef
     *     The StatisticsTypeDef
     */
    public void setStatisticsTypeDef(String StatisticsTypeDef) {
        this.StatisticsTypeDef = StatisticsTypeDef;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.PayProfileTypeDef);
        dest.writeString(this.Name);
        dest.writeString(this.StatisticsTypeDef);
    }

    public PayTypeList() {
    }

    protected PayTypeList(Parcel in) {
        this.PayProfileTypeDef = in.readString();
        this.Name = in.readString();
        this.StatisticsTypeDef = in.readString();
    }

    public static final Parcelable.Creator<PayTypeList> CREATOR = new Parcelable.Creator<PayTypeList>() {
        @Override
        public PayTypeList createFromParcel(Parcel source) {
            return new PayTypeList(source);
        }

        @Override
        public PayTypeList[] newArray(int size) {
            return new PayTypeList[size];
        }
    };
}
