package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class PhpInfomoduleList implements Parcelable {

    public ArrayList<PhpInfomoduleItem> feed;

    public PhpInfomoduleList() {
    }

    // Parcelable management
    private PhpInfomoduleList(Parcel in) {
        feed = in.createTypedArrayList(PhpInfomoduleItem.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(feed);
    }

    public static final Parcelable.Creator<PhpInfomoduleList> CREATOR = new Parcelable.Creator<PhpInfomoduleList>() {
        public PhpInfomoduleList createFromParcel(Parcel in) {
            return new PhpInfomoduleList(in);
        }

        public PhpInfomoduleList[] newArray(int size) {
            return new PhpInfomoduleList[size];
        }
    };
}
