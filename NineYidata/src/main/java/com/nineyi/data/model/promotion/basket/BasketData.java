
package com.nineyi.data.model.promotion.basket;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class BasketData implements Parcelable {

    public static final String PROMOTION_ID = "PromotionId";
    public static final String SALE_PAGE_LIST = "SalePageList";

    @SerializedName(PROMOTION_ID)
    @Expose
    private int promotionId;
    @SerializedName("ShopId")
    @Expose
    private long shopId;
    @SerializedName(SALE_PAGE_LIST)
    @Expose
    private List<BasketSalePageList> salePageList = new ArrayList<BasketSalePageList>();
    @SerializedName("TotalQty")
    @Expose
    private int totalQty;
    @SerializedName("TotalPrice")
    @Expose
    private double totalPrice;
    @SerializedName("PromotionDiscount")
    @Expose
    private double promotionDiscount;
    @SerializedName("PromotionConditionTitle")
    @Expose
    private String promotionConditionTitle;
    @SerializedName("PromotionDiscountTitle")
    @Expose
    private String promotionDiscountTitle;
    @SerializedName("RecommandConditionTitle")
    @Expose
    private String recommandConditionTitle;

    /**
     * 
     * @return
     *     The promotionId
     */
    public int getPromotionId() {
        return promotionId;
    }

    /**
     * 
     * @param promotionId
     *     The PromotionId
     */
    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    /**
     * 
     * @return
     *     The shopId
     */
    public long getShopId() {
        return shopId;
    }

    /**
     * 
     * @param shopId
     *     The ShopId
     */
    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    /**
     * 
     * @return
     *     The salePageList
     */
    public List<BasketSalePageList> getSalePageList() {
        return salePageList;
    }

    /**
     * 
     * @param salePageList
     *     The SalePageList
     */
    public void setSalePageList(List<BasketSalePageList> salePageList) {
        this.salePageList = salePageList;
    }

    /**
     * 
     * @return
     *     The totalQty
     */
    public int getTotalQty() {
        return totalQty;
    }

    /**
     * 
     * @param totalQty
     *     The TotalQty
     */
    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    /**
     * 
     * @return
     *     The totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * 
     * @param totalPrice
     *     The TotalPrice
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * 
     * @return
     *     The promotionDiscount
     */
    public double getPromotionDiscount() {
        return promotionDiscount;
    }

    /**
     * 
     * @param promotionDiscount
     *     The PromotionDiscount
     */
    public void setPromotionDiscount(double promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    /**
     * 
     * @return
     *     The promotionConditionTitle
     */
    public String getPromotionConditionTitle() {
        return promotionConditionTitle;
    }

    /**
     * 
     * @param promotionConditionTitle
     *     The PromotionConditionTitle
     */
    public void setPromotionConditionTitle(String promotionConditionTitle) {
        this.promotionConditionTitle = promotionConditionTitle;
    }

    /**
     * 
     * @return
     *     The promotionDiscountTitle
     */
    public String getPromotionDiscountTitle() {
        return promotionDiscountTitle;
    }

    /**
     * 
     * @param promotionDiscountTitle
     *     The PromotionDiscountTitle
     */
    public void setPromotionDiscountTitle(String promotionDiscountTitle) {
        this.promotionDiscountTitle = promotionDiscountTitle;
    }

    /**
     * 
     * @return
     *     The recommandConditionTitle
     */
    public String getRecommandConditionTitle() {
        return recommandConditionTitle;
    }

    /**
     * 
     * @param recommandConditionTitle
     *     The RecommandConditionTitle
     */
    public void setRecommandConditionTitle(String recommandConditionTitle) {
        this.recommandConditionTitle = recommandConditionTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.promotionId);
        dest.writeLong(this.shopId);
        dest.writeList(this.salePageList);
        dest.writeInt(this.totalQty);
        dest.writeDouble(this.totalPrice);
        dest.writeDouble(this.promotionDiscount);
        dest.writeString(this.promotionConditionTitle);
        dest.writeString(this.promotionDiscountTitle);
        dest.writeString(this.recommandConditionTitle);
    }

    public BasketData() {
    }

    protected BasketData(Parcel in) {
        this.promotionId = in.readInt();
        this.shopId = in.readLong();
        this.salePageList = new ArrayList<BasketSalePageList>();
        in.readList(this.salePageList, BasketSalePageList.class.getClassLoader());
        this.totalQty = in.readInt();
        this.totalPrice = in.readDouble();
        this.promotionDiscount = in.readDouble();
        this.promotionConditionTitle = in.readString();
        this.promotionDiscountTitle = in.readString();
        this.recommandConditionTitle = in.readString();
    }

    public static final Parcelable.Creator<BasketData> CREATOR = new Parcelable.Creator<BasketData>() {
        @Override
        public BasketData createFromParcel(Parcel source) {
            return new BasketData(source);
        }

        @Override
        public BasketData[] newArray(int size) {
            return new BasketData[size];
        }
    };
}
