package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public final class PhpContentElement extends AbstractBaseModel implements Parcelable {

    public String title;
    public String uuid;
    public String introduction;
    public String display_date;

    public PhpContentElement() {
    }

    // Parcelable management
    private PhpContentElement(Parcel in) {
        title = in.readString();
        uuid = in.readString();
        introduction = in.readString();
        display_date = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(uuid);
        dest.writeString(introduction);
        dest.writeString(display_date);
    }

    public static final Parcelable.Creator<PhpContentElement> CREATOR = new Parcelable.Creator<PhpContentElement>() {
        public PhpContentElement createFromParcel(Parcel in) {
            return new PhpContentElement(in);
        }

        public PhpContentElement[] newArray(int size) {
            return new PhpContentElement[size];
        }
    };
}
