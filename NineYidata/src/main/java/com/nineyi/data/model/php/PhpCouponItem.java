package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public final class PhpCouponItem extends AbstractBaseModel implements Parcelable {

    public PhpCouponElement coupon;

    public PhpCouponShopElement getShop() {
        return shop;
    }

    public PhpCouponElement getCoupon() {
        return coupon;
    }

    public PhpCouponShopElement shop;

    public PhpCouponItem() {
    }

    // Parcelable management
    private PhpCouponItem(Parcel in) {
        coupon = PhpCouponElement.CREATOR.createFromParcel(in);
        shop = PhpCouponShopElement.CREATOR.createFromParcel(in);

    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        coupon.writeToParcel(dest, flags);
        shop.writeToParcel(dest, flags);
    }

    public static final Parcelable.Creator<PhpCouponItem> CREATOR = new Parcelable.Creator<PhpCouponItem>() {
        public PhpCouponItem createFromParcel(Parcel in) {
            return new PhpCouponItem(in);
        }

        public PhpCouponItem[] newArray(int size) {
            return new PhpCouponItem[size];
        }
    };
}
