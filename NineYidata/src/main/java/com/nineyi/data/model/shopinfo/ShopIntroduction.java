package com.nineyi.data.model.shopinfo;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShopIntroduction implements Parcelable {

    public ShopIntroduceEntity ShopIntroduceEntity;

    public ShopIntroduction() {
    }

    // Parcelable management
    private ShopIntroduction(Parcel in) {
        ShopIntroduceEntity = ShopIntroduceEntity.CREATOR.createFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        ShopIntroduceEntity.writeToParcel(dest, flags);
    }

    public static final Parcelable.Creator<ShopIntroduction> CREATOR = new Parcelable.Creator<ShopIntroduction>() {
        public ShopIntroduction createFromParcel(Parcel in) {
            return new ShopIntroduction(in);
        }

        public ShopIntroduction[] newArray(int size) {
            return new ShopIntroduction[size];
        }
    };
}
