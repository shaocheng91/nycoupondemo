package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

import com.nineyi.data.model.salepage.SalePageShortBySearch;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/7/28.
 */
public class SalePageBySearch implements Parcelable {
    public ArrayList<SalePageShortBySearch> SalePageList;
    public String MaxScore;
    public String Took;
    public int TotalSize;
    public double CurrentScoreThreshold;
    public SearchPriceRange PriceRange;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.SalePageList);
        dest.writeString(this.MaxScore);
        dest.writeString(this.Took);
        dest.writeInt(this.TotalSize);
        dest.writeDouble(this.CurrentScoreThreshold);
        dest.writeParcelable(this.PriceRange, flags);
    }

    public SalePageBySearch() {
    }

    protected SalePageBySearch(Parcel in) {
        this.SalePageList = in.createTypedArrayList(SalePageShortBySearch.CREATOR);
        this.MaxScore = in.readString();
        this.Took = in.readString();
        this.TotalSize = in.readInt();
        this.CurrentScoreThreshold = in.readDouble();
        this.PriceRange = in.readParcelable(SearchPriceRange.class.getClassLoader());
    }

    public static final Parcelable.Creator<SalePageBySearch> CREATOR = new Parcelable.Creator<SalePageBySearch>() {
        @Override
        public SalePageBySearch createFromParcel(Parcel source) {
            return new SalePageBySearch(source);
        }

        @Override
        public SalePageBySearch[] newArray(int size) {
            return new SalePageBySearch[size];
        }
    };
}
