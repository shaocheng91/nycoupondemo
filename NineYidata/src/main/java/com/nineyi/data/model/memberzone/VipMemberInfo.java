package com.nineyi.data.model.memberzone;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;


public class VipMemberInfo implements Parcelable {

    private static final String FIELD_FULL_NAME = "FullName";
    private static final String FIELD_GENDER = "Gender";

    @SerializedName(FIELD_FULL_NAME)
    private String mFullName;
    @SerializedName(FIELD_GENDER)
    private int mGender;

    public VipMemberInfo() {

    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getFullName() {
        return mFullName;
    }

    public int getGender() {
        return mGender;
    }

    public VipMemberInfo(Parcel in) {
        mFullName = in.readString();
        mGender = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipMemberInfo> CREATOR = new Creator<VipMemberInfo>() {
        public VipMemberInfo createFromParcel(Parcel in) {
            return new VipMemberInfo(in);
        }

        public VipMemberInfo[] newArray(int size) {
            return new VipMemberInfo[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFullName);
        dest.writeInt(mGender);
    }


}