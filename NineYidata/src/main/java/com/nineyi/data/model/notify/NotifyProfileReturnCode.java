package com.nineyi.data.model.notify;

import android.os.Parcel;
import android.os.Parcelable;


public final class NotifyProfileReturnCode implements Parcelable {

    public String ReturnCode;
    public NotifyProfileOutputData Data;
    public String Message;

    public NotifyProfileReturnCode() {
    }

    // Parcelable management
    private NotifyProfileReturnCode(Parcel in) {
        ReturnCode = in.readString();
        Data = NotifyProfileOutputData.CREATOR.createFromParcel(in);
        Message = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        Data.writeToParcel(dest, flags);
        dest.writeString(Message);
    }

    public static final Parcelable.Creator<NotifyProfileReturnCode> CREATOR
            = new Parcelable.Creator<NotifyProfileReturnCode>() {
        public NotifyProfileReturnCode createFromParcel(Parcel in) {
            return new NotifyProfileReturnCode(in);
        }

        public NotifyProfileReturnCode[] newArray(int size) {
            return new NotifyProfileReturnCode[size];
        }
    };
}
