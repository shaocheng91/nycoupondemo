package com.nineyi.data.model.shoppingcart;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ShoppingCartCalculatedCondition implements Parcelable {

    private static final String FIELD_ID = "Id";
    private static final String FIELD_IS_MATCHED = "IsMatched";
    private static final String FIELD_QTY = "TotalQty";
    private static final String FIELD_PAYMENT = "TotalPayment";
    private static final String FIELD_REACHQTYCOUNT = "ReachQtyCount";

    @SerializedName(FIELD_ID)
    private long mId;
    @SerializedName(FIELD_IS_MATCHED)
    private boolean mIsMatched;
    @SerializedName(FIELD_QTY)
    private int mQty;
    @SerializedName(FIELD_PAYMENT)
    private double mTotalPayment;
    @SerializedName(FIELD_REACHQTYCOUNT)
    private int mReachQtyCount;


    public void setReachQtyCount(int reachQtyCount) {
        mReachQtyCount = reachQtyCount;
    }

    public int getReachQtyCount() {
        return mReachQtyCount;
    }


    public ShoppingCartCalculatedCondition() {

    }

    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setIsMatched(boolean isMatched) {
        mIsMatched = isMatched;
    }

    public boolean isIsMatched() {
        return mIsMatched;
    }

    public void setQty(int qty) {
        mQty = qty;
    }

    public int getQty() {
        return mQty;
    }

    public void setTotalPayment(int price) {
        mTotalPayment = price;
    }

    public double getTotalPayment() {
        return mTotalPayment;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ShoppingCartCalculatedCondition) {
            return ((ShoppingCartCalculatedCondition) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ((Long) mId).hashCode();
    }

    public ShoppingCartCalculatedCondition(Parcel in) {
        mId = in.readLong();
        mIsMatched = in.readInt() == 1;
        mQty = in.readInt();
        mTotalPayment = in.readDouble();
        mReachQtyCount = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShoppingCartCalculatedCondition> CREATOR
            = new Creator<ShoppingCartCalculatedCondition>() {
        public ShoppingCartCalculatedCondition createFromParcel(Parcel in) {
            return new ShoppingCartCalculatedCondition(in);
        }

        public ShoppingCartCalculatedCondition[] newArray(int size) {
            return new ShoppingCartCalculatedCondition[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeInt(mIsMatched ? 1 : 0);
        dest.writeInt(mQty);
        dest.writeDouble(mTotalPayment);
        dest.writeInt(mReachQtyCount);
    }


}