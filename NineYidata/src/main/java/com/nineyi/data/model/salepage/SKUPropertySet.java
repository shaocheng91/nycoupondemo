package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public class SKUPropertySet implements Parcelable {

    public int GoodsSKUId;
    public boolean IsShow;
    public String PropertySet;
    public int SaleProductSKUId;
    public int SellingQty;
    public int OnceQty;
    public String PropertyNameSet;

    public SKUPropertySet() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.GoodsSKUId);
        dest.writeByte(this.IsShow ? (byte) 1 : (byte) 0);
        dest.writeString(this.PropertySet);
        dest.writeInt(this.SaleProductSKUId);
        dest.writeInt(this.SellingQty);
        dest.writeInt(this.OnceQty);
        dest.writeString(this.PropertyNameSet);
    }

    protected SKUPropertySet(Parcel in) {
        this.GoodsSKUId = in.readInt();
        this.IsShow = in.readByte() != 0;
        this.PropertySet = in.readString();
        this.SaleProductSKUId = in.readInt();
        this.SellingQty = in.readInt();
        this.OnceQty = in.readInt();
        this.PropertyNameSet = in.readString();
    }

    public static final Creator<SKUPropertySet> CREATOR = new Creator<SKUPropertySet>() {
        @Override
        public SKUPropertySet createFromParcel(Parcel source) {
            return new SKUPropertySet(source);
        }

        @Override
        public SKUPropertySet[] newArray(int size) {
            return new SKUPropertySet[size];
        }
    };
}
