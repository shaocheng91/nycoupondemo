package com.nineyi.data.model.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;


public final class ReturnCode implements Parcelable {

    public String ReturnCode;
    public String Data;
    public String Message;
    public String uAUTH;

    public ReturnCode() {
    }

    // Parcelable management
    private ReturnCode(Parcel in) {
        ReturnCode = in.readString();
        Data = in.readString();
        Message = in.readString();
        uAUTH = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        dest.writeString(Data);
        dest.writeString(Message);
        dest.writeString(uAUTH);
    }

    public static final Parcelable.Creator<ReturnCode> CREATOR = new Parcelable.Creator<ReturnCode>() {
        public ReturnCode createFromParcel(Parcel in) {
            return new ReturnCode(in);
        }

        public ReturnCode[] newArray(int size) {
            return new ReturnCode[size];
        }
    };
}
