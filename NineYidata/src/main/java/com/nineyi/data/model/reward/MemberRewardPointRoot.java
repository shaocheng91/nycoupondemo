package com.nineyi.data.model.reward;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class MemberRewardPointRoot implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private MemberRewardPointDatum mDatum;


    public MemberRewardPointRoot(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(MemberRewardPointDatum datum) {
        mDatum = datum;
    }

    public MemberRewardPointDatum getDatum() {
        return mDatum;
    }

    public MemberRewardPointRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mDatum = in.readParcelable(MemberRewardPointDatum.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MemberRewardPointRoot> CREATOR = new Creator<MemberRewardPointRoot>() {
        public MemberRewardPointRoot createFromParcel(Parcel in) {
            return new MemberRewardPointRoot(in);
        }

        public MemberRewardPointRoot[] newArray(int size) {
            return new MemberRewardPointRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        		dest.writeParcelable(mDatum, flags);
    }


}