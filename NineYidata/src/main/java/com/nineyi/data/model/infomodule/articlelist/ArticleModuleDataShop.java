
package com.nineyi.data.model.infomodule.articlelist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ArticleModuleDataShop implements Parcelable {

    @SerializedName("ShopId")
    @Expose
    private Integer shopId;
    @SerializedName("ShopName")
    @Expose
    private String shopName;
    @SerializedName("HeaderStyle")
    @Expose
    private String headerStyle;

    /**
     * 
     * @return
     *     The shopId
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 
     * @param shopId
     *     The ShopId
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    /**
     * 
     * @return
     *     The shopName
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * 
     * @param shopName
     *     The ShopName
     */
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    /**
     * 
     * @return
     *     The headerStyle
     */
    public String getHeaderStyle() {
        return headerStyle;
    }

    /**
     * 
     * @param headerStyle
     *     The HeaderStyle
     */
    public void setHeaderStyle(String headerStyle) {
        this.headerStyle = headerStyle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.shopId);
        dest.writeString(this.shopName);
        dest.writeString(this.headerStyle);
    }

    public ArticleModuleDataShop() {
    }

    protected ArticleModuleDataShop(Parcel in) {
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.shopName = in.readString();
        this.headerStyle = in.readString();
    }

    public static final Parcelable.Creator<ArticleModuleDataShop> CREATOR = new Parcelable.Creator<ArticleModuleDataShop>() {
        @Override
        public ArticleModuleDataShop createFromParcel(Parcel source) {
            return new ArticleModuleDataShop(source);
        }

        @Override
        public ArticleModuleDataShop[] newArray(int size) {
            return new ArticleModuleDataShop[size];
        }
    };
}
