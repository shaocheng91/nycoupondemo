package com.nineyi.data.model.infomodule;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class InfoModuleRecommandation implements Parcelable {

    private static final String FIELD_DATA = "Data";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_RETURN_CODE = "ReturnCode";


    @SerializedName(FIELD_DATA)
    private List<InfoModuleData> mData;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;


    public InfoModuleRecommandation() {

    }

    public void setData(List<InfoModuleData> data) {
        mData = data;
    }

    public List<InfoModuleData> getData() {
        return mData;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public InfoModuleRecommandation(Parcel in) {
        mData = new ArrayList<InfoModuleData>();
        in.readTypedList(mData, InfoModuleData.CREATOR);
        mMessage = in.readString();
        mReturnCode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<InfoModuleRecommandation> CREATOR
            = new Parcelable.Creator<InfoModuleRecommandation>() {
        public InfoModuleRecommandation createFromParcel(Parcel in) {
            return new InfoModuleRecommandation(in);
        }

        public InfoModuleRecommandation[] newArray(int size) {
            return new InfoModuleRecommandation[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mData);
        dest.writeString(mMessage);
        dest.writeString(mReturnCode);
    }


}