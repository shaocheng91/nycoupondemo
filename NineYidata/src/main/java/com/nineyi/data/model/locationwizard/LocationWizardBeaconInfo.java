package com.nineyi.data.model.locationwizard;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Qoo on 15/1/2.
 */
public class LocationWizardBeaconInfo implements Parcelable {

    public LocationWizardBeaconInfo() {

    }

    public LocationWizardBeaconInfo(Parcel in) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<LocationWizardBeaconInfo> CREATOR
            = new Parcelable.Creator<LocationWizardBeaconInfo>() {
        public LocationWizardBeaconInfo createFromParcel(Parcel in) {
            return new LocationWizardBeaconInfo(in);
        }

        public LocationWizardBeaconInfo[] newArray(int size) {
            return new LocationWizardBeaconInfo[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

}
