package com.nineyi.data.model.appmain;

import android.os.Parcel;
import android.os.Parcelable;


public class AnnouncementAction implements Parcelable {

    private Integer Code;

    private String Message;
    private Integer AnnouncementId;
    private String Frequency;
    private String AnnouncedStart;
    private String AnnouncedEnd;

    public Integer getCode() {
        return Code;
    }

    public void setCode(Integer code) {
        this.Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public Integer getAnnouncementId() {
        return AnnouncementId;
    }

    public void setAnnouncementId(Integer announcementId) {
        this.AnnouncementId = announcementId;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        this.Frequency = frequency;
    }

    public String getAnnouncedStart() {
        return AnnouncedStart;
    }

    public void setAnnouncedStart(String announcedStart) {
        this.AnnouncedStart = announcedStart;
    }

    public String getAnnouncedEnd() {
        return AnnouncedEnd;
    }

    public void setAnnouncedEnd(String announcedEnd) {
        this.AnnouncedEnd = announcedEnd;
    }

    protected AnnouncementAction(Parcel in) {
        Code = in.readByte() == 0x00 ? null : in.readInt();
        Message = in.readString();
        AnnouncementId = in.readByte() == 0x00 ? null : in.readInt();
        Frequency = in.readString();
        AnnouncedStart = in.readString();
        AnnouncedEnd = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (Code == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(Code);
        }
        dest.writeString(Message);
        if (AnnouncementId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(AnnouncementId);
        }
        dest.writeString(Frequency);
        dest.writeString(AnnouncedStart);
        dest.writeString(AnnouncedEnd);
    }

    public static final Parcelable.Creator<AnnouncementAction> CREATOR = new Parcelable.Creator<AnnouncementAction>() {
        @Override
        public AnnouncementAction createFromParcel(Parcel in) {
            return new AnnouncementAction(in);
        }

        @Override
        public AnnouncementAction[] newArray(int size) {
            return new AnnouncementAction[size];
        }
    };
}