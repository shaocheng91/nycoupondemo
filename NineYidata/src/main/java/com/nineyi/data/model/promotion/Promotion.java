package com.nineyi.data.model.promotion;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.data.model.promotion.v2.PromotionTargetMemberTierList;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public final class Promotion implements Parcelable {

    public int Id;
    public String Title;
    public int ShopId;
    public String ShopName;
    public int CategoryId;
    public String CategoryName;
    public String IconURL;
    public String LogoURL;
    public NineyiDate StartTime;
    public NineyiDate EndTime;
    public String Detail;
    public String[] Rules;
    public String[] Conditions;
    public String PromotionTargetType;
    public ArrayList<Long> PromotionTargetList;
    public String PromotionConditionType;
    public int TotalQty;
    public double TotalPrice;
    public double DiscountPrice;
    public double DiscountRate;
    public String PromotionConditionDiscountType;
    public String PromotionString;
    public String PromotionDisplayName;
    public ArrayList<FreeGift> FreeGifts;
    public List<Integer> MustBySalePageList;


    public Promotion() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.Title);
        dest.writeInt(this.ShopId);
        dest.writeString(this.ShopName);
        dest.writeInt(this.CategoryId);
        dest.writeString(this.CategoryName);
        dest.writeString(this.IconURL);
        dest.writeString(this.LogoURL);
        dest.writeParcelable(this.StartTime, flags);
        dest.writeParcelable(this.EndTime, flags);
        dest.writeString(this.Detail);
        dest.writeStringArray(this.Rules);
        dest.writeStringArray(this.Conditions);
        dest.writeString(this.PromotionTargetType);
        dest.writeList(this.PromotionTargetList);
        dest.writeString(this.PromotionConditionType);
        dest.writeInt(this.TotalQty);
        dest.writeDouble(this.TotalPrice);
        dest.writeDouble(this.DiscountPrice);
        dest.writeDouble(this.DiscountRate);
        dest.writeString(this.PromotionConditionDiscountType);
        dest.writeString(this.PromotionString);
        dest.writeString(this.PromotionDisplayName);
        dest.writeTypedList(this.FreeGifts);
        dest.writeList(this.MustBySalePageList);
    }

    protected Promotion(Parcel in) {
        this.Id = in.readInt();
        this.Title = in.readString();
        this.ShopId = in.readInt();
        this.ShopName = in.readString();
        this.CategoryId = in.readInt();
        this.CategoryName = in.readString();
        this.IconURL = in.readString();
        this.LogoURL = in.readString();
        this.StartTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.EndTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.Detail = in.readString();
        this.Rules = in.createStringArray();
        this.Conditions = in.createStringArray();
        this.PromotionTargetType = in.readString();
        this.PromotionTargetList = new ArrayList<Long>();
        in.readList(this.PromotionTargetList, Long.class.getClassLoader());
        this.PromotionConditionType = in.readString();
        this.TotalQty = in.readInt();
        this.TotalPrice = in.readDouble();
        this.DiscountPrice = in.readDouble();
        this.DiscountRate = in.readDouble();
        this.PromotionConditionDiscountType = in.readString();
        this.PromotionString = in.readString();
        this.PromotionDisplayName = in.readString();
        this.FreeGifts = in.createTypedArrayList(FreeGift.CREATOR);
        this.MustBySalePageList = new ArrayList<Integer>();
        in.readList(this.MustBySalePageList, Integer.class.getClassLoader());
    }

    public static final Creator<Promotion> CREATOR = new Creator<Promotion>() {
        @Override
        public Promotion createFromParcel(Parcel source) {
            return new Promotion(source);
        }

        @Override
        public Promotion[] newArray(int size) {
            return new Promotion[size];
        }
    };
}
