package com.nineyi.data.model.shoppingcart.v4;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/9/7.
 */
public class ShopShipping {
    public String ReturnCode;
    public ArrayList<ShopShippingDetail> Data;
    public String Message;

}
