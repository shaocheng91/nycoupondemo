package com.nineyi.data.model.locationwizard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Qoo on 14/12/29.
 */
public class LocationWizardMemberInfo implements Parcelable {

    private static final String FIELD_MEMBER_ID = "MemberId";
    private static final String FIELD_MEMBER_CODE = "MemberCode";
    private static final String FIELD_MEMBER_CELL_PHONE = "MemberCellPhone";


    @SerializedName(FIELD_MEMBER_ID)
    private int mMemberId;
    @SerializedName(FIELD_MEMBER_CODE)
    private String mMemberCode;
    @SerializedName(FIELD_MEMBER_CELL_PHONE)
    private String mMemberCellPhone;


    public LocationWizardMemberInfo() {

    }

    public void setMemberId(int memberId) {
        mMemberId = memberId;
    }

    public int getMemberId() {
        return mMemberId;
    }

    public void setMemberCode(String memberCode) {
        mMemberCode = memberCode;
    }

    public String getMemberCode() {
        return mMemberCode;
    }

    public void setMemberCellPhone(String memberCellPhone) {
        mMemberCellPhone = memberCellPhone;
    }

    public String getMemberCellPhone() {
        return mMemberCellPhone;
    }

    public LocationWizardMemberInfo(Parcel in) {
        mMemberId = in.readInt();
        mMemberCode = in.readString();
        mMemberCellPhone = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<LocationWizardMemberInfo> CREATOR
            = new Parcelable.Creator<LocationWizardMemberInfo>() {
        public LocationWizardMemberInfo createFromParcel(Parcel in) {
            return new LocationWizardMemberInfo(in);
        }

        public LocationWizardMemberInfo[] newArray(int size) {
            return new LocationWizardMemberInfo[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mMemberId);
        dest.writeString(mMemberCode);
        dest.writeString(mMemberCellPhone);
    }
}
