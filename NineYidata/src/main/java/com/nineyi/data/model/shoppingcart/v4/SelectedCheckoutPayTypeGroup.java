
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class SelectedCheckoutPayTypeGroup implements Parcelable {

    @SerializedName("DisplayName")
    @Expose
    private String DisplayName;
    @SerializedName("StatisticsTypeDef")
    @Expose
    private String StatisticsTypeDef;
    @SerializedName("InstallmentType")
    @Expose
    private Object InstallmentType;
    @SerializedName("IsRecommand")
    @Expose
    private Boolean IsRecommand;
    @SerializedName("PayTypeList")
    @Expose
    private List<PayTypeList> mPayTypeList = new ArrayList<PayTypeList>();

    /**
     * 
     * @return
     *     The DisplayName
     */
    public String getDisplayName() {
        return DisplayName;
    }

    /**
     * 
     * @param DisplayName
     *     The DisplayName
     */
    public void setDisplayName(String DisplayName) {
        this.DisplayName = DisplayName;
    }

    /**
     * 
     * @return
     *     The StatisticsTypeDef
     */
    public String getStatisticsTypeDef() {
        return StatisticsTypeDef;
    }

    /**
     * 
     * @param StatisticsTypeDef
     *     The StatisticsTypeDef
     */
    public void setStatisticsTypeDef(String StatisticsTypeDef) {
        this.StatisticsTypeDef = StatisticsTypeDef;
    }

    /**
     * 
     * @return
     *     The InstallmentType
     */
    public Object getInstallmentType() {
        return InstallmentType;
    }

    /**
     * 
     * @param InstallmentType
     *     The InstallmentType
     */
    public void setInstallmentType(Object InstallmentType) {
        this.InstallmentType = InstallmentType;
    }

    /**
     * 
     * @return
     *     The IsRecommand
     */
    public Boolean getIsRecommand() {
        return IsRecommand;
    }

    /**
     * 
     * @param IsRecommand
     *     The IsRecommand
     */
    public void setIsRecommand(Boolean IsRecommand) {
        this.IsRecommand = IsRecommand;
    }

    /**
     * 
     * @return
     *     The mPayTypeList
     */
    public List<PayTypeList> getPayTypeList() {
        return mPayTypeList;
    }

    /**
     * 
     * @param PayTypeList
     *     The mPayTypeList
     */
    public void setPayTypeList(List<PayTypeList> PayTypeList) {
        this.mPayTypeList = PayTypeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.DisplayName);
        dest.writeString(this.StatisticsTypeDef);
//        dest.writeParcelable(this.InstallmentType, flags);
        dest.writeValue(this.IsRecommand);
        dest.writeTypedList(this.mPayTypeList);
    }

    public SelectedCheckoutPayTypeGroup() {
    }

    protected SelectedCheckoutPayTypeGroup(Parcel in) {
        this.DisplayName = in.readString();
        this.StatisticsTypeDef = in.readString();
//        this.InstallmentType = in.readParcelable(Object.class.getClassLoader());
        this.IsRecommand = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mPayTypeList = in.createTypedArrayList(PayTypeList.CREATOR);
    }

    public static final Parcelable.Creator<SelectedCheckoutPayTypeGroup> CREATOR
            = new Parcelable.Creator<SelectedCheckoutPayTypeGroup>() {
        @Override
        public SelectedCheckoutPayTypeGroup createFromParcel(Parcel source) {
            return new SelectedCheckoutPayTypeGroup(source);
        }

        @Override
        public SelectedCheckoutPayTypeGroup[] newArray(int size) {
            return new SelectedCheckoutPayTypeGroup[size];
        }
    };
}
