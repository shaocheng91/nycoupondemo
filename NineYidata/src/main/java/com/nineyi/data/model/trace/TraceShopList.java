package com.nineyi.data.model.trace;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class TraceShopList implements Parcelable {

    public String msg;
    public ArrayList<TraceShop> data;

    public TraceShopList() {
    }

    // Parcelable management
    private TraceShopList(Parcel in) {
        msg = in.readString();
        data = in.createTypedArrayList(TraceShop.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(msg);
        dest.writeTypedList(data);
    }

    public static final Parcelable.Creator<TraceShopList> CREATOR = new Parcelable.Creator<TraceShopList>() {
        public TraceShopList createFromParcel(Parcel in) {
            return new TraceShopList(in);
        }

        public TraceShopList[] newArray(int size) {
            return new TraceShopList[size];
        }
    };
}
