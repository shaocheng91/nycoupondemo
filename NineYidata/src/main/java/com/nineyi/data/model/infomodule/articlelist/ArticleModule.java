
package com.nineyi.data.model.infomodule.articlelist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ArticleModule implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private ArticleModuleData data;
    @SerializedName("Message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The returnCode
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * 
     * @param returnCode
     *     The ReturnCode
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * 
     * @return
     *     The data
     */
    public ArticleModuleData getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The Data
     */
    public void setData(ArticleModuleData data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.message);
    }

    public ArticleModule() {
    }

    protected ArticleModule(Parcel in) {
        this.returnCode = in.readString();
        this.data = in.readParcelable(ArticleModuleData.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Parcelable.Creator<ArticleModule> CREATOR = new Parcelable.Creator<ArticleModule>() {
        @Override
        public ArticleModule createFromParcel(Parcel source) {
            return new ArticleModule(source);
        }

        @Override
        public ArticleModule[] newArray(int size) {
            return new ArticleModule[size];
        }
    };
}
