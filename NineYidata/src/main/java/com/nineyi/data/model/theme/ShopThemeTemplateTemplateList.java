package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeTemplateTemplateList implements Parcelable {
    public String ReturnCode;
    public ShopThemeTemplateTemplateData Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, 0);
        dest.writeString(this.Message);
    }

    public ShopThemeTemplateTemplateList() {
    }

    protected ShopThemeTemplateTemplateList(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(ShopThemeTemplateTemplateData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<ShopThemeTemplateTemplateList> CREATOR = new Parcelable.Creator<ShopThemeTemplateTemplateList>() {
        public ShopThemeTemplateTemplateList createFromParcel(Parcel source) {
            return new ShopThemeTemplateTemplateList(source);
        }

        public ShopThemeTemplateTemplateList[] newArray(int size) {
            return new ShopThemeTemplateTemplateList[size];
        }
    };
}
