package com.nineyi.data.model.reward;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class RewardPointRoot implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private RewardPointListDatum mRewardPointListDatum;


    public RewardPointRoot(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setRewardPointListDatum(RewardPointListDatum rewardPointListDatum) {
        mRewardPointListDatum = rewardPointListDatum;
    }

    public RewardPointListDatum getRewardPointListDatum() {
        return mRewardPointListDatum;
    }

    public RewardPointRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mRewardPointListDatum = in.readParcelable(RewardPointListDatum.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RewardPointRoot> CREATOR = new Creator<RewardPointRoot>() {
        public RewardPointRoot createFromParcel(Parcel in) {
            return new RewardPointRoot(in);
        }

        public RewardPointRoot[] newArray(int size) {
            return new RewardPointRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        		dest.writeParcelable(mRewardPointListDatum, flags);
    }


}