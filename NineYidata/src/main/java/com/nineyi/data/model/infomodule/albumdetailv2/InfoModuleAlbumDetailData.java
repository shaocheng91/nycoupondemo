
package com.nineyi.data.model.infomodule.albumdetailv2;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.infomodule.InfoModuleCommonDetailDataItemList;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoModuleAlbumDetailData implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Subtitle")
    @Expose
    private String subtitle;
    @SerializedName("Uuid")
    @Expose
    private String uuid;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("BackgroundColor")
    @Expose
    private Integer backgroundColor;
    @SerializedName("Gallery")
    @Expose
    private String gallery;
    @SerializedName("GalleryList")
    @Expose
    private List<String> galleryList = new ArrayList<String>();
    @SerializedName("PublishedDate")
    @Expose
    private String publishedDate;
    @SerializedName("ItemList")
    @Expose
    private List<InfoModuleCommonDetailDataItemList> itemList = new ArrayList<InfoModuleCommonDetailDataItemList>();
    @SerializedName("Shop")
    @Expose
    private InfoModuleAlbumDetailShop shop;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The Type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * 
     * @param subtitle
     *     The Subtitle
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * 
     * @return
     *     The uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * 
     * @param uuid
     *     The Uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * 
     * @return
     *     The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * 
     * @param imageUrl
     *     The ImageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * 
     * @return
     *     The backgroundColor
     */
    public Integer getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * 
     * @param backgroundColor
     *     The BackgroundColor
     */
    public void setBackgroundColor(Integer backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * 
     * @return
     *     The gallery
     */
    public String getGallery() {
        return gallery;
    }

    /**
     * 
     * @param gallery
     *     The Gallery
     */
    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    /**
     * 
     * @return
     *     The galleryList
     */
    public List<String> getGalleryList() {
        return galleryList;
    }

    /**
     * 
     * @param galleryList
     *     The GalleryList
     */
    public void setGalleryList(List<String> galleryList) {
        this.galleryList = galleryList;
    }

    /**
     * 
     * @return
     *     The publishedDate
     */
    public String getPublishedDate() {
        return publishedDate;
    }

    /**
     * 
     * @param publishedDate
     *     The PublishedDate
     */
    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    /**
     * 
     * @return
     *     The itemList
     */
    public List<InfoModuleCommonDetailDataItemList> getItemList() {
        return itemList;
    }

    /**
     * 
     * @param itemList
     *     The ItemList
     */
    public void setItemList(List<InfoModuleCommonDetailDataItemList> itemList) {
        this.itemList = itemList;
    }

    /**
     * 
     * @return
     *     The shop
     */
    public InfoModuleAlbumDetailShop getShop() {
        return shop;
    }

    /**
     * 
     * @param shop
     *     The Shop
     */
    public void setShop(InfoModuleAlbumDetailShop shop) {
        this.shop = shop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.type);
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeString(this.uuid);
        dest.writeString(this.imageUrl);
        dest.writeValue(this.backgroundColor);
        dest.writeString(this.gallery);
        dest.writeStringList(this.galleryList);
        dest.writeString(this.publishedDate);
        dest.writeTypedList(this.itemList);
        dest.writeParcelable(this.shop, flags);
    }

    public InfoModuleAlbumDetailData() {
    }

    protected InfoModuleAlbumDetailData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = in.readString();
        this.title = in.readString();
        this.subtitle = in.readString();
        this.uuid = in.readString();
        this.imageUrl = in.readString();
        this.backgroundColor = (Integer) in.readValue(Integer.class.getClassLoader());
        this.gallery = in.readString();
        this.galleryList = in.createStringArrayList();
        this.publishedDate = in.readString();
        this.itemList = in.createTypedArrayList(InfoModuleCommonDetailDataItemList.CREATOR);
        this.shop = in.readParcelable(InfoModuleAlbumDetailShop.class.getClassLoader());
    }

    public static final Parcelable.Creator<InfoModuleAlbumDetailData> CREATOR
            = new Parcelable.Creator<InfoModuleAlbumDetailData>() {
        @Override
        public InfoModuleAlbumDetailData createFromParcel(Parcel source) {
            return new InfoModuleAlbumDetailData(source);
        }

        @Override
        public InfoModuleAlbumDetailData[] newArray(int size) {
            return new InfoModuleAlbumDetailData[size];
        }
    };
}
