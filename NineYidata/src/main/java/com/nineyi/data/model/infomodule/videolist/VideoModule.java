
package com.nineyi.data.model.infomodule.videolist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class VideoModule implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private VideoModuleData data;
    @SerializedName("Message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The returnCode
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * 
     * @param returnCode
     *     The ReturnCode
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * 
     * @return
     *     The data
     */
    public VideoModuleData getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The Data
     */
    public void setData(VideoModuleData data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.message);
    }

    public VideoModule() {
    }

    protected VideoModule(Parcel in) {
        this.returnCode = in.readString();
        this.data = in.readParcelable(VideoModuleData.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Parcelable.Creator<VideoModule> CREATOR = new Parcelable.Creator<VideoModule>() {
        @Override
        public VideoModule createFromParcel(Parcel source) {
            return new VideoModule(source);
        }

        @Override
        public VideoModule[] newArray(int size) {
            return new VideoModule[size];
        }
    };
}
