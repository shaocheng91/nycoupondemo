package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public final class PhpCouponUseStatus implements Parcelable {

    public String status;
    public String msg;
    public String code;
    public PhpCouponUseCode coupon;

    public PhpCouponUseStatus() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.code);
        dest.writeParcelable(this.coupon, 0);
    }

    protected PhpCouponUseStatus(Parcel in) {
        this.status = in.readString();
        this.msg = in.readString();
        this.code = in.readString();
        this.coupon = in.readParcelable(PhpCouponUseCode.class.getClassLoader());
    }

    public static final Creator<PhpCouponUseStatus> CREATOR = new Creator<PhpCouponUseStatus>() {
        public PhpCouponUseStatus createFromParcel(Parcel source) {
            return new PhpCouponUseStatus(source);
        }

        public PhpCouponUseStatus[] newArray(int size) {
            return new PhpCouponUseStatus[size];
        }
    };
}
