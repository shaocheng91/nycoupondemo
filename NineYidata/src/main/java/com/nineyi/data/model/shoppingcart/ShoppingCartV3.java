package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShoppingCartV3 implements Parcelable {

    public String ReturnCode;
    public ShoppingCartData Data;
    public String Message;

    public ShoppingCartV3() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, 0);
        dest.writeString(this.Message);
    }

    protected ShoppingCartV3(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(ShoppingCartData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Creator<ShoppingCartV3> CREATOR = new Creator<ShoppingCartV3>() {
        public ShoppingCartV3 createFromParcel(Parcel source) {
            return new ShoppingCartV3(source);
        }

        public ShoppingCartV3[] newArray(int size) {
            return new ShoppingCartV3[size];
        }
    };
}
