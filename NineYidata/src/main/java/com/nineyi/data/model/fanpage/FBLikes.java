package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class FBLikes implements Parcelable {

    List<FBLikeData> data;
    FBPaging paging;

    protected FBLikes(Parcel in) {
        if (in.readByte() == 0x01) {
            data = new ArrayList<FBLikeData>();
            in.readList(data, FBLikeData.class.getClassLoader());
        } else {
            data = null;
        }
        paging = (FBPaging) in.readValue(FBPaging.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
        dest.writeValue(paging);
    }

    public static final Parcelable.Creator<FBLikes> CREATOR = new Parcelable.Creator<FBLikes>() {
        @Override
        public FBLikes createFromParcel(Parcel in) {
            return new FBLikes(in);
        }

        @Override
        public FBLikes[] newArray(int size) {
            return new FBLikes[size];
        }
    };
}