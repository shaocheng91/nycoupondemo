package com.nineyi.data.model.mallapp;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class PromoteAppHomeList implements Parcelable {

    public ArrayList<PromoteApp> APPHotList;
    public ArrayList<PromoteApp> APPNewestList;

    public PromoteAppHomeList() {
    }

    // Parcelable management
    private PromoteAppHomeList(Parcel in) {
        APPHotList = in.createTypedArrayList(PromoteApp.CREATOR);
        APPNewestList = in.createTypedArrayList(PromoteApp.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(APPHotList);
        dest.writeTypedList(APPNewestList);
    }

    public static final Parcelable.Creator<PromoteAppHomeList> CREATOR = new Parcelable.Creator<PromoteAppHomeList>() {
        public PromoteAppHomeList createFromParcel(Parcel in) {
            return new PromoteAppHomeList(in);
        }

        public PromoteAppHomeList[] newArray(int size) {
            return new PromoteAppHomeList[size];
        }
    };
}
