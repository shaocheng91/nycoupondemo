package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePagePrice implements Parcelable {

    public double Price;
    public double SuggestPrice;

    public SalePagePrice(double price, double suggestPrice) {
        Price = price;
        SuggestPrice = suggestPrice;
    }

    public SalePagePrice() {
    }

    // Parcelable management
    private SalePagePrice(Parcel in) {
        Price = in.readDouble();
        SuggestPrice = in.readDouble();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(Price);
        dest.writeDouble(SuggestPrice);
    }

    public static final Parcelable.Creator<SalePagePrice> CREATOR = new Parcelable.Creator<SalePagePrice>() {
        public SalePagePrice createFromParcel(Parcel in) {
            return new SalePagePrice(in);
        }

        public SalePagePrice[] newArray(int size) {
            return new SalePagePrice[size];
        }
    };
}
