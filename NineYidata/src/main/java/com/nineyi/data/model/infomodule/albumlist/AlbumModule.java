
package com.nineyi.data.model.infomodule.albumlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class AlbumModule implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private AlbumModuleData data;
    @SerializedName("Message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The returnCode
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * 
     * @param returnCode
     *     The ReturnCode
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * 
     * @return
     *     The data
     */
    public AlbumModuleData getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The Data
     */
    public void setData(AlbumModuleData data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.message);
    }

    public AlbumModule() {
    }

    protected AlbumModule(Parcel in) {
        this.returnCode = in.readString();
        this.data = in.readParcelable(AlbumModuleData.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Parcelable.Creator<AlbumModule> CREATOR = new Parcelable.Creator<AlbumModule>() {
        @Override
        public AlbumModule createFromParcel(Parcel source) {
            return new AlbumModule(source);
        }

        @Override
        public AlbumModule[] newArray(int size) {
            return new AlbumModule[size];
        }
    };
}
