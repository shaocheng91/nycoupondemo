
package com.nineyi.data.model.infomodule.albumdetailv2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoModuleAlbumDetailShop implements Parcelable {

    @SerializedName("ShopId")
    @Expose
    private Integer shopId;
    @SerializedName("ShopName")
    @Expose
    private String shopName;
    @SerializedName("HeaderStyle")
    @Expose
    private String headerStyle;

    /**
     * 
     * @return
     *     The shopId
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 
     * @param shopId
     *     The ShopId
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    /**
     * 
     * @return
     *     The shopName
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * 
     * @param shopName
     *     The ShopName
     */
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    /**
     * 
     * @return
     *     The headerStyle
     */
    public String getHeaderStyle() {
        return headerStyle;
    }

    /**
     * 
     * @param headerStyle
     *     The HeaderStyle
     */
    public void setHeaderStyle(String headerStyle) {
        this.headerStyle = headerStyle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.shopId);
        dest.writeString(this.shopName);
        dest.writeString(this.headerStyle);
    }

    public InfoModuleAlbumDetailShop() {
    }

    protected InfoModuleAlbumDetailShop(Parcel in) {
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.shopName = in.readString();
        this.headerStyle = in.readString();
    }

    public static final Parcelable.Creator<InfoModuleAlbumDetailShop> CREATOR
            = new Parcelable.Creator<InfoModuleAlbumDetailShop>() {
        @Override
        public InfoModuleAlbumDetailShop createFromParcel(Parcel source) {
            return new InfoModuleAlbumDetailShop(source);
        }

        @Override
        public InfoModuleAlbumDetailShop[] newArray(int size) {
            return new InfoModuleAlbumDetailShop[size];
        }
    };
}
