package com.nineyi.data.model.salepage;

import com.nineyi.data.model.gson.NineyiDate;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class SalePageMajor implements Parcelable {

    public String Title;
    public Double Price;
    public ArrayList<SKUData> SKUList;
    public int ShippingTypeDef;
    public String ShippingTypeDefDesc;
    public NineyiDate ShippingDate;
    public NineyiDate ShippingEndDate;
    public String TemperatureTypeDef;
    public int Length;
    public int Width;
    public int Height;
    public int Weight;
    public String ShippingWaitingDays;
    public int SaleProductId;

    public SalePageMajor() {
    }

    // Parcelable management
    private SalePageMajor(Parcel in) {
        Title = in.readString();
        Price = in.readDouble();
        SKUList = in.createTypedArrayList(SKUData.CREATOR);
        ShippingTypeDef = in.readInt();
        ShippingTypeDefDesc = in.readString();
        ShippingDate = in.readParcelable(NineyiDate.class.getClassLoader());
        ShippingWaitingDays = in.readString();
        SaleProductId = in.readInt();
        ShippingEndDate = in.readParcelable(NineyiDate.class.getClassLoader());
        TemperatureTypeDef = in.readString();
        Length = in.readInt();
        Width = in.readInt();
        Height = in.readInt();
        Weight = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Title);
        dest.writeDouble(Price);
        dest.writeTypedList(SKUList);
        dest.writeInt(ShippingTypeDef);
        dest.writeString(ShippingTypeDefDesc);
        dest.writeParcelable(ShippingDate, 0);
        dest.writeString(ShippingWaitingDays);
        dest.writeInt(SaleProductId);
        dest.writeParcelable(ShippingEndDate, 0);
        dest.writeString(TemperatureTypeDef);
        dest.writeInt(Length);
        dest.writeInt(Width);
        dest.writeInt(Height);
        dest.writeInt(Weight);
    }

    public static final Parcelable.Creator<SalePageMajor> CREATOR = new Parcelable.Creator<SalePageMajor>() {
        public SalePageMajor createFromParcel(Parcel in) {
            return new SalePageMajor(in);
        }

        public SalePageMajor[] newArray(int size) {
            return new SalePageMajor[size];
        }
    };
}
