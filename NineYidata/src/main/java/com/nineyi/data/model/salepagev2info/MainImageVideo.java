
package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainImageVideo implements Parcelable {
    @SerializedName("SalePageVideoType")
    @Expose
    private Integer salePageVideoType;
    @SerializedName("SalePageVideoPositionType")
    @Expose
    private Integer salePageVideoPositionType;
    @SerializedName("VideoUrl")
    @Expose
    private String videoUrl;

    protected MainImageVideo(Parcel in) {
        salePageVideoType = in.readByte() == 0x00 ? null : in.readInt();
        salePageVideoPositionType = in.readByte() == 0x00 ? null : in.readInt();
        videoUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (salePageVideoType == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(salePageVideoType);
        }
        if (salePageVideoPositionType == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(salePageVideoPositionType);
        }
        dest.writeString(videoUrl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MainImageVideo> CREATOR = new Parcelable.Creator<MainImageVideo>() {
        @Override
        public MainImageVideo createFromParcel(Parcel in) {
            return new MainImageVideo(in);
        }

        @Override
        public MainImageVideo[] newArray(int size) {
            return new MainImageVideo[size];
        }
    };

    public Integer getSalePageVideoType() {
        return salePageVideoType;
    }

    public Integer getSalePageVideoPositionType() {
        return salePageVideoPositionType;
    }

    public String getVideoUrl() {
        return videoUrl;
    }
}
