package com.nineyi.data.model.reward;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.gson.NineyiDate;

import android.os.Parcel;


public class RewardPointList implements Parcelable{

    private static final String FIELD_IMAGE_URL = "ImageUrl";
    private static final String FIELD_EXCHANGE_START_DATE = "ExchangeStartDate";
    private static final String FIELD_START_DATE = "StartDate";
    private static final String FIELD_END_DATE = "EndDate";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_EXCHANGE_END_DATE = "ExchangeEndDate";


    @SerializedName(FIELD_IMAGE_URL)
    private String mImageUrl;
    @SerializedName(FIELD_EXCHANGE_START_DATE)
    private NineyiDate mExchangeStartDate;
    @SerializedName(FIELD_START_DATE)
    private NineyiDate mStartDate;
    @SerializedName(FIELD_END_DATE)
    private NineyiDate mEndDate;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_EXCHANGE_END_DATE)
    private NineyiDate mExchangeEndDate;


    public RewardPointList(){

    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setExchangeStartDate(NineyiDate exchangeStartDate) {
        mExchangeStartDate = exchangeStartDate;
    }

    public NineyiDate getExchangeStartDate() {
        return mExchangeStartDate;
    }

    public void setStartDate(NineyiDate startDate) {
        mStartDate = startDate;
    }

    public NineyiDate getStartDate() {
        return mStartDate;
    }

    public void setEndDate(NineyiDate endDate) {
        mEndDate = endDate;
    }

    public NineyiDate getEndDate() {
        return mEndDate;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setExchangeEndDate(NineyiDate exchangeEndDate) {
        mExchangeEndDate = exchangeEndDate;
    }

    public NineyiDate getExchangeEndDate() {
        return mExchangeEndDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mImageUrl);
        dest.writeParcelable(this.mExchangeStartDate, flags);
        dest.writeParcelable(this.mStartDate, flags);
        dest.writeParcelable(this.mEndDate, flags);
        dest.writeInt(this.mId);
        dest.writeString(this.mName);
        dest.writeParcelable(this.mExchangeEndDate, flags);
    }

    protected RewardPointList(Parcel in) {
        this.mImageUrl = in.readString();
        this.mExchangeStartDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.mStartDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.mEndDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.mId = in.readInt();
        this.mName = in.readString();
        this.mExchangeEndDate = in.readParcelable(NineyiDate.class.getClassLoader());
    }

    public static final Creator<RewardPointList> CREATOR = new Creator<RewardPointList>() {
        @Override
        public RewardPointList createFromParcel(Parcel source) {
            return new RewardPointList(source);
        }

        @Override
        public RewardPointList[] newArray(int size) {
            return new RewardPointList[size];
        }
    };
}