
package com.nineyi.data.model.promotion.discount;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.data.model.promotion.v2.PromotionTargetMemberTierList;

import android.os.Parcel;
import android.os.Parcelable;


public class PromotionDiscountList implements Parcelable {

    @SerializedName("PromotionId")
    @Expose
    private int promotionId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("TypeDef")
    @Expose
    private String typeDef;
    @SerializedName("DiscountTypeDef")
    @Expose
    private String discountTypeDef;
    @SerializedName("StartDateTime")
    @Expose
    private NineyiDate startDateTime;
    @SerializedName("EndDateTime")
    @Expose
    private NineyiDate endDateTime;
    @SerializedName("SalePageList")
    @Expose
    private List<PromotionDiscountSalePageList> salePageList = new ArrayList<PromotionDiscountSalePageList>();
    @SerializedName("PromotionTargetMemberTierList")
    @Expose
    private List<PromotionTargetMemberTierList> memberTierList = new ArrayList<PromotionTargetMemberTierList>();


    /**
     * 
     * @return
     *     The promotionId
     */
    public int getPromotionId() {
        return promotionId;
    }

    /**
     * 
     * @param promotionId
     *     The PromotionId
     */
    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The typeDef
     */
    public String getTypeDef() {
        return typeDef;
    }

    /**
     * 
     * @param typeDef
     *     The TypeDef
     */
    public void setTypeDef(String typeDef) {
        this.typeDef = typeDef;
    }

    /**
     * 
     * @return
     *     The discountTypeDef
     */
    public String getDiscountTypeDef() {
        return discountTypeDef;
    }

    /**
     * 
     * @param discountTypeDef
     *     The DiscountTypeDef
     */
    public void setDiscountTypeDef(String discountTypeDef) {
        this.discountTypeDef = discountTypeDef;
    }

    /**
     * 
     * @return
     *     The startDateTime
     */
    public NineyiDate getStartDateTime() {
        return startDateTime;
    }

    /**
     * 
     * @param startDateTime
     *     The StartDateTime
     */
    public void setStartDateTime(NineyiDate startDateTime) {
        this.startDateTime = startDateTime;
    }

    /**
     * 
     * @return
     *     The endDateTime
     */
    public NineyiDate getEndDateTime() {
        return endDateTime;
    }

    /**
     * 
     * @param endDateTime
     *     The EndDateTime
     */
    public void setEndDateTime(NineyiDate endDateTime) {
        this.endDateTime = endDateTime;
    }

    /**
     * 
     * @return
     *     The salePageList
     */
    public List<PromotionDiscountSalePageList> getSalePageList() {
        return salePageList;
    }

    /**
     * 
     * @param salePageList
     *     The SalePageList
     */
    public void setSalePageList(List<PromotionDiscountSalePageList> salePageList) {
        this.salePageList = salePageList;
    }

    public List<PromotionTargetMemberTierList> getMemberTierList() {
        return memberTierList;
    }

    public void setMemberTierList(List<PromotionTargetMemberTierList> memberTierList) {
        this.memberTierList = memberTierList;
    }

    public PromotionDiscountList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.promotionId);
        dest.writeString(this.name);
        dest.writeString(this.typeDef);
        dest.writeString(this.discountTypeDef);
        dest.writeParcelable(this.startDateTime, flags);
        dest.writeParcelable(this.endDateTime, flags);
        dest.writeTypedList(this.salePageList);
        dest.writeTypedList(this.memberTierList);
    }

    protected PromotionDiscountList(Parcel in) {
        this.promotionId = in.readInt();
        this.name = in.readString();
        this.typeDef = in.readString();
        this.discountTypeDef = in.readString();
        this.startDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.endDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.salePageList = in.createTypedArrayList(PromotionDiscountSalePageList.CREATOR);
        this.memberTierList = in.createTypedArrayList(PromotionTargetMemberTierList.CREATOR);
    }

    public static final Creator<PromotionDiscountList> CREATOR = new Creator<PromotionDiscountList>() {
        @Override
        public PromotionDiscountList createFromParcel(Parcel source) {
            return new PromotionDiscountList(source);
        }

        @Override
        public PromotionDiscountList[] newArray(int size) {
            return new PromotionDiscountList[size];
        }
    };
}
