package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import java.util.ArrayList;


public class LocationListByCityData implements Parcelable{

    private static final String FIELD_STORE_SORT = "StoreSort";
    private static final String FIELD_LIST = "List";


    @SerializedName(FIELD_STORE_SORT)
    private boolean mStoreSort;
    @SerializedName(FIELD_LIST)
    private ArrayList<LocationListDataList> mLists;


    public LocationListByCityData(){

    }

    public void setStoreSort(boolean storeSort) {
        mStoreSort = storeSort;
    }

    public boolean isStoreSort() {
        return mStoreSort;
    }

    public void setLists(ArrayList<LocationListDataList> lists) {
        mLists = lists;
    }

    public ArrayList<LocationListDataList> getLists() {
        return mLists;
    }

    public LocationListByCityData(Parcel in) {
        mStoreSort = in.readInt() == 1 ? true: false;
    new ArrayList<LocationListDataList>();
        in.readTypedList(mLists, LocationListDataList.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationListByCityData> CREATOR = new Creator<LocationListByCityData>() {
        public LocationListByCityData createFromParcel(Parcel in) {
            return new LocationListByCityData(in);
        }

        public LocationListByCityData[] newArray(int size) {
            return new LocationListByCityData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mStoreSort ? 1 : 0);
        dest.writeTypedList(mLists);
    }


}