package com.nineyi.data.model.reward;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class RewardPointDetailRoot implements Parcelable {

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private RewardPointDetailDatum mDatum;


    public RewardPointDetailRoot() {

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(RewardPointDetailDatum datum) {
        mDatum = datum;
    }

    public RewardPointDetailDatum getDatum() {
        return mDatum;
    }

    public RewardPointDetailRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mDatum = in.readParcelable(RewardPointDetailDatum.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RewardPointDetailRoot> CREATOR = new Creator<RewardPointDetailRoot>() {
        public RewardPointDetailRoot createFromParcel(Parcel in) {
            return new RewardPointDetailRoot(in);
        }

        public RewardPointDetailRoot[] newArray(int size) {
            return new RewardPointDetailRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        dest.writeParcelable(mDatum, flags);
    }


}