package com.nineyi.data.model.shopapp;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/11/15.
 */
public class ShopContractSetting implements Parcelable {
    public String ReturnCode;
    public ShopContractSettingData Data;
    public String Message;

    public ShopContractSetting() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.Message);
    }

    protected ShopContractSetting(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(ShopContractSettingData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Creator<ShopContractSetting> CREATOR = new Creator<ShopContractSetting>() {
        @Override
        public ShopContractSetting createFromParcel(Parcel source) {
            return new ShopContractSetting(source);
        }

        @Override
        public ShopContractSetting[] newArray(int size) {
            return new ShopContractSetting[size];
        }
    };
}
