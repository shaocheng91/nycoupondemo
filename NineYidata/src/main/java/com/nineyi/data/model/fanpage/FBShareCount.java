package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBShareCount implements Parcelable {

    String count;

    protected FBShareCount(Parcel in) {
        count = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(count);
    }

    public static final Parcelable.Creator<FBShareCount> CREATOR = new Parcelable.Creator<FBShareCount>() {
        @Override
        public FBShareCount createFromParcel(Parcel in) {
            return new FBShareCount(in);
        }

        @Override
        public FBShareCount[] newArray(int size) {
            return new FBShareCount[size];
        }
    };
}