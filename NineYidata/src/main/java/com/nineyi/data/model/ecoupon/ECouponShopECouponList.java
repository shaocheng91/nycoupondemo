package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ECouponShopECouponList implements Parcelable {

    public String ReturnCode;
    public ArrayList<ECouponShopECoupon> ShopECouponList;

    public ECouponShopECouponList() {
    }

    // Parcelable management
    private ECouponShopECouponList(Parcel in) {
        ReturnCode = in.readString();
        ShopECouponList = in.createTypedArrayList(ECouponShopECoupon.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        dest.writeTypedList(ShopECouponList);
    }

    public static final Parcelable.Creator<ECouponShopECouponList> CREATOR
            = new Parcelable.Creator<ECouponShopECouponList>() {
        public ECouponShopECouponList createFromParcel(Parcel in) {
            return new ECouponShopECouponList(in);
        }

        public ECouponShopECouponList[] newArray(int size) {
            return new ECouponShopECouponList[size];
        }
    };
}
