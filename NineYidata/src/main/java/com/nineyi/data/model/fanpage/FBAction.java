package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBAction implements Parcelable {

    String name;
    String link;

    protected FBAction(Parcel in) {
        name = in.readString();
        link = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(link);
    }

    public static final Parcelable.Creator<FBAction> CREATOR = new Parcelable.Creator<FBAction>() {
        @Override
        public FBAction createFromParcel(Parcel in) {
            return new FBAction(in);
        }

        @Override
        public FBAction[] newArray(int size) {
            return new FBAction[size];
        }
    };
}