package com.nineyi.data.model.openapp;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;

/**
 * Created by Willy on 16/1/12.
 */
public class AppProfileData implements Parcelable{

    private static final String FIELD_BRANCH_METRICS_SERVICE = "BranchMetricsService";
    private static final String FIELD_CDN_DOMAIN = "CDNDomain";


    @SerializedName(FIELD_BRANCH_METRICS_SERVICE)
    private BranchMetricsService mBranchMetricsService;
    @SerializedName(FIELD_CDN_DOMAIN)
    private String mCDNDomain;


    public AppProfileData(){

    }

    public void setBranchMetricsService(BranchMetricsService branchMetricsService) {
        mBranchMetricsService = branchMetricsService;
    }

    public BranchMetricsService getBranchMetricsService() {
        return mBranchMetricsService;
    }

    public void setCDNDomain(String cDNDomain) {
        mCDNDomain = cDNDomain;
    }

    public String getCDNDomain() {
        return mCDNDomain;
    }

    public AppProfileData(Parcel in) {
        mBranchMetricsService = in.readParcelable(BranchMetricsService.class.getClassLoader());
        mCDNDomain = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<AppProfileData> CREATOR = new Parcelable.Creator<AppProfileData>() {
        public AppProfileData createFromParcel(Parcel in) {
            return new AppProfileData(in);
        }

        public AppProfileData[] newArray(int size) {
            return new AppProfileData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mBranchMetricsService, flags);
        dest.writeString(mCDNDomain);
    }


}
