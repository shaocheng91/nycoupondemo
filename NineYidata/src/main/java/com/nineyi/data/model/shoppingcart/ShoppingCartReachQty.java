package com.nineyi.data.model.shoppingcart;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public final class ShoppingCartReachQty implements Parcelable {

    private static final String FIELD_SALE_PAGE_ID_LIST = "SalePageIdList";
    private static final String FIELD_IS_REACH_QTY = "IsReachQty";
    private static final String FIELD_TOTAL_QTY = "TotalQty";
    private static final String FIELD_TOTAL_PAYMENT = "TotalPayment";
    private static final String FIELD_PROMOTION_ID = "PromotionId";
    private static final String FIELD_CALCULATED_CONDITIONS = "CalculatedConditions";


    @SerializedName(FIELD_SALE_PAGE_ID_LIST)
    private int[] mSalePageIdLists;
    @SerializedName(FIELD_IS_REACH_QTY)
    private boolean mIsReachQty;
    @SerializedName(FIELD_TOTAL_QTY)
    private int mTotalQty;
    @SerializedName(FIELD_TOTAL_PAYMENT)
    private double mTotalPayment;
    @SerializedName(FIELD_PROMOTION_ID)
    private int mPromotionId;
    @SerializedName(FIELD_CALCULATED_CONDITIONS)
    private List<ShoppingCartCalculatedCondition> mCalculatedConditions;


    public ShoppingCartReachQty() {

    }

    public void setSalePageIdLists(int[] salePageIdLists) {
        mSalePageIdLists = salePageIdLists;
    }

    public int[] getSalePageIdLists() {
        return mSalePageIdLists;
    }

    public void setIsReachQty(boolean isReachQty) {
        mIsReachQty = isReachQty;
    }

    public boolean isIsReachQty() {
        return mIsReachQty;
    }

    public void setTotalQty(int totalQty) {
        mTotalQty = totalQty;
    }

    public int getTotalQty() {
        return mTotalQty;
    }

    public void setTotalPayment(int totalPayment) {
        mTotalPayment = totalPayment;
    }

    public double getTotalPayment() {
        return mTotalPayment;
    }

    public void setPromotionId(int promotionId) {
        mPromotionId = promotionId;
    }

    public int getPromotionId() {
        return mPromotionId;
    }

    public void setCalculatedConditions(List<ShoppingCartCalculatedCondition> calculatedConditions) {
        mCalculatedConditions = calculatedConditions;
    }

    public List<ShoppingCartCalculatedCondition> getCalculatedConditions() {
        return mCalculatedConditions;
    }

    public ShoppingCartReachQty(Parcel in) {
        mSalePageIdLists = in.createIntArray();
        mIsReachQty = in.readInt() == 1;
        mTotalQty = in.readInt();
        mTotalPayment = in.readDouble();
        mPromotionId = in.readInt();
        mCalculatedConditions = new ArrayList<ShoppingCartCalculatedCondition>();
        in.readTypedList(mCalculatedConditions, ShoppingCartCalculatedCondition.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<ShoppingCartReachQty> CREATOR
            = new Parcelable.Creator<ShoppingCartReachQty>() {
        public ShoppingCartReachQty createFromParcel(Parcel in) {
            return new ShoppingCartReachQty(in);
        }

        public ShoppingCartReachQty[] newArray(int size) {
            return new ShoppingCartReachQty[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeIntArray(mSalePageIdLists);
        dest.writeInt(mIsReachQty ? 1 : 0);
        dest.writeInt(mTotalQty);
        dest.writeDouble(mTotalPayment);
        dest.writeInt(mPromotionId);
        dest.writeTypedList(mCalculatedConditions);
    }


}