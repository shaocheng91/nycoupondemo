
package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryLevelName implements Parcelable {

    @SerializedName("Level1_ShopCategory_Id")
    @Expose
    private Integer level1ShopCategoryId;
    @SerializedName("Level1_ShopCategory_Name")
    @Expose
    private String level1ShopCategoryName;
    @SerializedName("Level2_ShopCategory_Id")
    @Expose
    private Integer level2ShopCategoryId;
    @SerializedName("Level2_ShopCategory_Name")
    @Expose
    private String level2ShopCategoryName;
    @SerializedName("Level3_ShopCategory_Id")
    @Expose
    private Integer level3ShopCategoryId;
    @SerializedName("Level3_ShopCategory_Name")
    @Expose
    private String level3ShopCategoryName;
    @SerializedName("ShopCategory_ShowId")
    @Expose
    private Integer shopCategoryShowId;
    @SerializedName("ShopCategory_ShowName")
    @Expose
    private String shopCategoryShowName;

    protected CategoryLevelName(Parcel in) {
        level1ShopCategoryId = in.readByte() == 0x00 ? null : in.readInt();
        level1ShopCategoryName = in.readString();
        level2ShopCategoryId = in.readByte() == 0x00 ? null : in.readInt();
        level2ShopCategoryName = in.readString();
        level3ShopCategoryId = in.readByte() == 0x00 ? null : in.readInt();
        level3ShopCategoryName = in.readString();
        shopCategoryShowId = in.readByte() == 0x00 ? null : in.readInt();
        shopCategoryShowName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (level1ShopCategoryId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(level1ShopCategoryId);
        }
        dest.writeString(level1ShopCategoryName);
        if (level2ShopCategoryId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(level2ShopCategoryId);
        }
        dest.writeString(level2ShopCategoryName);
        if (level3ShopCategoryId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(level3ShopCategoryId);
        }
        dest.writeString(level3ShopCategoryName);
        if (shopCategoryShowId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(shopCategoryShowId);
        }
        dest.writeString(shopCategoryShowName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CategoryLevelName> CREATOR = new Parcelable.Creator<CategoryLevelName>() {
        @Override
        public CategoryLevelName createFromParcel(Parcel in) {
            return new CategoryLevelName(in);
        }

        @Override
        public CategoryLevelName[] newArray(int size) {
            return new CategoryLevelName[size];
        }
    };
}
