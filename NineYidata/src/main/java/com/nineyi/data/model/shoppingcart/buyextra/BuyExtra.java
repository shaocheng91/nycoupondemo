package com.nineyi.data.model.shoppingcart.buyextra;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shaocheng on 2017/9/14.
 */

public class BuyExtra implements Parcelable {

    private String ReturnCode;
    private BuyExtraData Data;
    private String Message;

    private BuyExtra() {
    }

    private BuyExtra(Builder builder) {
        ReturnCode = builder.ReturnCode;
        Data = builder.Data;
        Message = builder.Message;
    }

    public String getReturnCode() {
        return ReturnCode;
    }

    public BuyExtraData getData() {
        return Data;
    }

    public String getMessage() {
        return Message;
    }

    public final static Creator<BuyExtra> CREATOR = new Creator<BuyExtra>() {
        @SuppressWarnings({
                "unchecked"
        })
        public BuyExtra createFromParcel(Parcel in) {
            BuyExtra instance = new BuyExtra();
            instance.ReturnCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.Data = ((BuyExtraData) in.readValue((BuyExtraData.class.getClassLoader())));
            instance.Message = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public BuyExtra[] newArray(int size) {
            return (new BuyExtra[size]);
        }

    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ReturnCode);
        dest.writeValue(Data);
        dest.writeValue(Message);
    }

    public int describeContents() {
        return 0;
    }

    public static final class Builder {
        private String ReturnCode;
        private BuyExtraData Data;
        private String Message;

        public Builder() {
        }

        public Builder ReturnCode(String val) {
            ReturnCode = val;
            return this;
        }

        public Builder Data(BuyExtraData val) {
            Data = val;
            return this;
        }

        public Builder Message(String val) {
            Message = val;
            return this;
        }

        public BuyExtra build() {
            return new BuyExtra(this);
        }
    }
}
