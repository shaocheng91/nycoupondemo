package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBFrom implements Parcelable {

    String category;
    String name;
    String id;

    public String getName() {
        return name;
    }

    protected FBFrom(Parcel in) {
        category = in.readString();
        name = in.readString();
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(name);
        dest.writeString(id);
    }

    public static final Parcelable.Creator<FBFrom> CREATOR = new Parcelable.Creator<FBFrom>() {
        @Override
        public FBFrom createFromParcel(Parcel in) {
            return new FBFrom(in);
        }

        @Override
        public FBFrom[] newArray(int size) {
            return new FBFrom[size];
        }
    };
}