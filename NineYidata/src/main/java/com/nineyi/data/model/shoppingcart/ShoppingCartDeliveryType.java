package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShoppingCartDeliveryType implements Parcelable {

    public int Id;
    public String FeeTypeDef;
    public String FeeTypeDefDesc;
    public double Fee;
    public double OverPrice;
    public String MargeTypeName;
    public String TypeName;
    public boolean IsReachFreeFee;
    public String DisplayTypeName;
    public String DisplayTypeNameForApp;
    public String DeliveryTypeDef;
    public String DeliveryTypeDefGroup;

    public ShoppingCartDeliveryType() {
    }

    // Parcelable management
    private ShoppingCartDeliveryType(Parcel in) {
        Id = in.readInt();
        FeeTypeDef = in.readString();
        FeeTypeDefDesc = in.readString();
        Fee = in.readDouble();
        OverPrice = in.readDouble();
        MargeTypeName = in.readString();
        TypeName = in.readString();
        IsReachFreeFee = in.readInt() == 1;
        DisplayTypeName = in.readString();
        DisplayTypeNameForApp = in.readString();
        DeliveryTypeDef = in.readString();
        DeliveryTypeDefGroup = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(FeeTypeDef);
        dest.writeString(FeeTypeDefDesc);
        dest.writeDouble(Fee);
        dest.writeDouble(OverPrice);
        dest.writeString(MargeTypeName);
        dest.writeString(TypeName);
        dest.writeInt(IsReachFreeFee ? 1 : 0);
        dest.writeString(DisplayTypeName);
        dest.writeString(DisplayTypeNameForApp);
        dest.writeString(DeliveryTypeDef);
        dest.writeString(DeliveryTypeDefGroup);
    }

    public static final Parcelable.Creator<ShoppingCartDeliveryType> CREATOR
            = new Parcelable.Creator<ShoppingCartDeliveryType>() {
        public ShoppingCartDeliveryType createFromParcel(Parcel in) {
            return new ShoppingCartDeliveryType(in);
        }

        public ShoppingCartDeliveryType[] newArray(int size) {
            return new ShoppingCartDeliveryType[size];
        }
    };
}
