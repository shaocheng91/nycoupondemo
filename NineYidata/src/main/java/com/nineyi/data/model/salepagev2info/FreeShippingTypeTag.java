
package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreeShippingTypeTag implements Parcelable {

    @SerializedName("HasFreeShippingTypeTag")
    @Expose
    private Boolean hasFreeShippingTypeTag;
    @SerializedName("BestFreeShippingTypeTag")
    @Expose
    private String bestFreeShippingTypeTag;


    protected FreeShippingTypeTag(Parcel in) {
        byte hasFreeShippingTypeTagVal = in.readByte();
        hasFreeShippingTypeTag = hasFreeShippingTypeTagVal == 0x02 ? null : hasFreeShippingTypeTagVal != 0x00;
        bestFreeShippingTypeTag = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (hasFreeShippingTypeTag == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (hasFreeShippingTypeTag ? 0x01 : 0x00));
        }
        dest.writeString(bestFreeShippingTypeTag);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<FreeShippingTypeTag> CREATOR = new Parcelable.Creator<FreeShippingTypeTag>() {
        @Override
        public FreeShippingTypeTag createFromParcel(Parcel in) {
            return new FreeShippingTypeTag(in);
        }

        @Override
        public FreeShippingTypeTag[] newArray(int size) {
            return new FreeShippingTypeTag[size];
        }
    };

    public String getBestFreeShippingTypeTag() {
        return bestFreeShippingTypeTag;
    }

    public Boolean getHasFreeShippingTypeTag() {
        return hasFreeShippingTypeTag;
    }
}
