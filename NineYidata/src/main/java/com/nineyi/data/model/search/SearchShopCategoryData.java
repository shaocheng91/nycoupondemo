package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

import com.nineyi.data.model.category.Category;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/8/1.
 */
public class SearchShopCategoryData implements Parcelable {

    public ArrayList<Category> ShopCategories;
    public int TotalSize;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.ShopCategories);
        dest.writeInt(this.TotalSize);
    }

    public SearchShopCategoryData() {
    }

    protected SearchShopCategoryData(Parcel in) {
        this.ShopCategories = in.createTypedArrayList(Category.CREATOR);
        this.TotalSize = in.readInt();
    }

    public static final Parcelable.Creator<SearchShopCategoryData> CREATOR = new Parcelable.Creator<SearchShopCategoryData>() {
        @Override
        public SearchShopCategoryData createFromParcel(Parcel source) {
            return new SearchShopCategoryData(source);
        }

        @Override
        public SearchShopCategoryData[] newArray(int size) {
            return new SearchShopCategoryData[size];
        }
    };
}
