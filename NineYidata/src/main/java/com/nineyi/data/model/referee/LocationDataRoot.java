package com.nineyi.data.model.referee;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class LocationDataRoot implements Parcelable {

    public ArrayList<RefereeLocationListInfo> LocationList;


    public LocationDataRoot() {

    }


    public LocationDataRoot(Parcel in) {

        LocationList = in.createTypedArrayList(RefereeLocationListInfo.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationDataRoot> CREATOR = new Creator<LocationDataRoot>() {
        public LocationDataRoot createFromParcel(Parcel in) {
            return new LocationDataRoot(in);
        }

        public LocationDataRoot[] newArray(int size) {
            return new LocationDataRoot[size];
        }
    };
    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeTypedList(LocationList);
    }


}