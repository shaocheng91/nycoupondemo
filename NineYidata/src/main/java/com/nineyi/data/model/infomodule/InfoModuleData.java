package com.nineyi.data.model.infomodule;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoModuleData implements Parcelable {

    private static final String FIELD_TYPE = "Type";
    private static final String FIELD_SUBTITLE = "Subtitle";
    private static final String FIELD_PUB_CONTENT_ID = "PubContentId";
    private static final String FIELD_FIRM_URL = "FirmUrl";
    private static final String FIELD_TITLE = "Title";
    private static final String FIELD_COVER_IMAGE_URL = "CoverImageUrl";
    private static final String FIELD_PUBLISHED_DATE = "PublishedDate";


    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FIELD_SUBTITLE)
    private String mSubtitle;
    @SerializedName(FIELD_PUB_CONTENT_ID)
    private int mPubContentId;
    @SerializedName(FIELD_FIRM_URL)
    private String mFirmUrl;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_COVER_IMAGE_URL)
    private String mCoverImageUrl;
    @SerializedName(FIELD_PUBLISHED_DATE)
    private String mPublishedDate;


    public InfoModuleData() {

    }

    public void setType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }

    public void setSubtitle(String subtitle) {
        mSubtitle = subtitle;
    }

    public String getSubtitle() {
        return mSubtitle;
    }

    public void setPubContentId(int pubContentId) {
        mPubContentId = pubContentId;
    }

    public int getPubContentId() {
        return mPubContentId;
    }

    public void setFirmUrl(String firmUrl) {
        mFirmUrl = firmUrl;
    }

    public String getFirmUrl() {
        return mFirmUrl;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        mCoverImageUrl = coverImageUrl;
    }

    public String getCoverImageUrl() {
        return mCoverImageUrl;
    }

    public void setPublishedDate(String publishedDate) {
        mPublishedDate = publishedDate;
    }

    public String getPublishedDate() {
        return mPublishedDate;
    }

    public InfoModuleData(Parcel in) {
        mType = in.readString();
        mSubtitle = in.readString();
        mPubContentId = in.readInt();
        mFirmUrl = in.readString();
        mTitle = in.readString();
        mCoverImageUrl = in.readString();
        mPublishedDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<InfoModuleData> CREATOR = new Parcelable.Creator<InfoModuleData>() {
        public InfoModuleData createFromParcel(Parcel in) {
            return new InfoModuleData(in);
        }

        public InfoModuleData[] newArray(int size) {
            return new InfoModuleData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeString(mSubtitle);
        dest.writeInt(mPubContentId);
        dest.writeString(mFirmUrl);
        dest.writeString(mTitle);
        dest.writeString(mCoverImageUrl);
        dest.writeString(mPublishedDate);
    }


}