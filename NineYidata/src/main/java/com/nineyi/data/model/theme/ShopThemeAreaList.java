package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeAreaList implements Parcelable {
    public ShopThemeArea Content;

    public ShopThemeAreaList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Content, flags);
    }

    protected ShopThemeAreaList(Parcel in) {
        this.Content = in.readParcelable(ShopThemeArea.class.getClassLoader());
    }

    public static final Creator<ShopThemeAreaList> CREATOR = new Creator<ShopThemeAreaList>() {
        @Override
        public ShopThemeAreaList createFromParcel(Parcel source) {
            return new ShopThemeAreaList(source);
        }

        @Override
        public ShopThemeAreaList[] newArray(int size) {
            return new ShopThemeAreaList[size];
        }
    };
}
