package com.nineyi.data.model.trace;

import android.os.Parcel;
import android.os.Parcelable;


public final class TraceSalePage implements Parcelable {

    public int SalePageId;
    public String DateTime;
    public String Title;
    public double Price;
    public double SuggestPrice;
    public boolean HasSKU;
    public int SKUId;
    public String PicUrl;
    public int Cid;
    public boolean IsFreeShippingFee;
    public boolean IsStoreDelivery;
    public boolean IsHasLowTemperature;
    public boolean IsHasPromotion;
    public String ShopName;
    public String ShopLogoUrl;
    public int ShopId;

    public TraceSalePage() {
    }

    // Parcelable management
    private TraceSalePage(Parcel in) {
        SalePageId = in.readInt();
        DateTime = in.readString();
        Title = in.readString();
        Price = in.readDouble();
        SuggestPrice = in.readDouble();
        HasSKU = in.readInt() == 1;
        SKUId = in.readInt();
        PicUrl = in.readString();
        Cid = in.readInt();
        IsFreeShippingFee = in.readInt() == 1;
        IsStoreDelivery = in.readInt() == 1;
        IsHasLowTemperature = in.readInt() == 1;
        IsHasPromotion = in.readInt() == 1;
        ShopName = in.readString();
        ShopLogoUrl = in.readString();
        ShopId = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SalePageId);
        dest.writeString(DateTime);
        dest.writeString(Title);
        dest.writeDouble(Price);
        dest.writeDouble(SuggestPrice);
        dest.writeInt(HasSKU ? 1 : 0);
        dest.writeInt(SKUId);
        dest.writeString(PicUrl);
        dest.writeInt(Cid);
        dest.writeInt(IsFreeShippingFee ? 1 : 0);
        dest.writeInt(IsStoreDelivery ? 1 : 0);
        dest.writeInt(IsHasLowTemperature ? 1 : 0);
        dest.writeInt(IsHasPromotion ? 1 : 0);
        dest.writeString(ShopName);
        dest.writeString(ShopLogoUrl);
        dest.writeInt(ShopId);
    }

    public static final Parcelable.Creator<TraceSalePage> CREATOR = new Parcelable.Creator<TraceSalePage>() {
        public TraceSalePage createFromParcel(Parcel in) {
            return new TraceSalePage(in);
        }

        public TraceSalePage[] newArray(int size) {
            return new TraceSalePage[size];
        }
    };
}
