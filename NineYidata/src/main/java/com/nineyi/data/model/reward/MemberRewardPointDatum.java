package com.nineyi.data.model.reward;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class MemberRewardPointDatum implements Parcelable{

    private static final String FIELD_MEMBER_ID = "MemberId";
    private static final String FIELD_REWARD_POINT_ID = "RewardPointId";
    private static final String FIELD_MEMBER_REWARD_POINT = "MemberRewardPoint";


    @SerializedName(FIELD_MEMBER_ID)
    private int mMemberId;
    @SerializedName(FIELD_REWARD_POINT_ID)
    private int mRewardPointId;
    @SerializedName(FIELD_MEMBER_REWARD_POINT)
    private int mMemberRewardPoint;


    public MemberRewardPointDatum(){

    }

    public void setMemberId(int memberId) {
        mMemberId = memberId;
    }

    public int getMemberId() {
        return mMemberId;
    }

    public void setRewardPointId(int rewardPointId) {
        mRewardPointId = rewardPointId;
    }

    public int getRewardPointId() {
        return mRewardPointId;
    }

    public void setMemberRewardPoint(int memberRewardPoint) {
        mMemberRewardPoint = memberRewardPoint;
    }

    public int getMemberRewardPoint() {
        return mMemberRewardPoint;
    }

    public MemberRewardPointDatum(Parcel in) {
        mMemberId = in.readInt();
        mRewardPointId = in.readInt();
        mMemberRewardPoint = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MemberRewardPointDatum> CREATOR = new Creator<MemberRewardPointDatum>() {
        public MemberRewardPointDatum createFromParcel(Parcel in) {
            return new MemberRewardPointDatum(in);
        }

        public MemberRewardPointDatum[] newArray(int size) {
            return new MemberRewardPointDatum[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mMemberId);
        dest.writeInt(mRewardPointId);
        dest.writeInt(mMemberRewardPoint);
    }


}