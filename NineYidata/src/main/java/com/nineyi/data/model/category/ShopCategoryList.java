package com.nineyi.data.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShopCategoryList implements Parcelable {

    public int TotalCount;
    public ArrayList<Category> List;

    public ShopCategoryList() {
    }

    // Parcelable management
    private ShopCategoryList(Parcel in) {
        TotalCount = in.readInt();
        List = in.createTypedArrayList(Category.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(TotalCount);
        dest.writeTypedList(List);
    }

    public static final Parcelable.Creator<ShopCategoryList> CREATOR = new Parcelable.Creator<ShopCategoryList>() {
        public ShopCategoryList createFromParcel(Parcel in) {
            return new ShopCategoryList(in);
        }

        public ShopCategoryList[] newArray(int size) {
            return new ShopCategoryList[size];
        }
    };
}
