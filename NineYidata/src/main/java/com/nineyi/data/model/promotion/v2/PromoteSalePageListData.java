package com.nineyi.data.model.promotion.v2;

import java.util.List;


/**
 * Created by tedliang on 2016/8/26.
 */
public class PromoteSalePageListData {

    /**
     * SalePageId : 12331
     * Title : 商品1
     * SalePageImageUrl : http://mobilewebmall.tw.qa/webapi/imagesV3/Original/SalePage/67378/0/125533?v=1
     * Price : 199
     * SuggestPrice : 350
     * IsSoldOut : false
     */

    private List<PromoteSalePageList> SalePageList;

    public List<PromoteSalePageList> getSalePageList() {
        return SalePageList;
    }

    public void setSalePageList(List<PromoteSalePageList> SalePageList) {
        this.SalePageList = SalePageList;
    }

}
