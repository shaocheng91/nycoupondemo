package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class FBSubPhoto implements Parcelable {

    List<FBPhotoData> data;

    public List<FBPhotoData> getData() {
        return data;
    }

    protected FBSubPhoto(Parcel in) {
        if (in.readByte() == 0x01) {
            data = new ArrayList<FBPhotoData>();
            in.readList(data, FBAttachment.class.getClassLoader());
        } else {
            data = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
    }

    public static final Parcelable.Creator<FBPhotos> CREATOR = new Parcelable.Creator<FBPhotos>() {
        @Override
        public FBPhotos createFromParcel(Parcel in) {
            return new FBPhotos(in);
        }

        @Override
        public FBPhotos[] newArray(int size) {
            return new FBPhotos[size];
        }
    };
}