package com.nineyi.data.model.infomodule.videodetail;

import com.nineyi.data.model.infomodule.InfomoduleCommonDetailDataModel;
import com.nineyi.data.model.infomodule.InfomoduleCommonShopSimpleInfoModel;
import com.nineyi.data.model.infomodule.InfomoduleCommonDetailDataItemListModel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleGetVideoDetailDataModel extends InfomoduleCommonDetailDataModel {

    public int Id;
    public String Type;
    public String Title;
    public String Subtitle;
    public String ClipLink;
    public String PublishedDate;
    public String Introduction;
    public ArrayList<InfomoduleCommonDetailDataItemListModel> ItemList;
    public InfomoduleCommonShopSimpleInfoModel Shop;

    public InfomoduleGetVideoDetailDataModel(Parcel in) {
        super(in);
        Id = in.readInt();
        Type = in.readString();
        Title = in.readString();
        Subtitle = in.readString();
        ClipLink = in.readString();
        PublishedDate = in.readString();
        Introduction = in.readString();
        ItemList = in.createTypedArrayList(InfomoduleCommonDetailDataItemListModel.CREATOR);
        Shop = InfomoduleCommonShopSimpleInfoModel.CREATOR.createFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(Id);
        dest.writeString(Type);
        dest.writeString(Title);
        dest.writeString(Subtitle);
        dest.writeString(ClipLink);
        dest.writeString(PublishedDate);
        dest.writeString(Introduction);
        dest.writeTypedList(ItemList);
        dest.writeParcelable(Shop, flags);
    }

    public static Parcelable.Creator<InfomoduleGetVideoDetailDataModel> CREATOR
            = new Parcelable.Creator<InfomoduleGetVideoDetailDataModel>() {
        @Override
        public InfomoduleGetVideoDetailDataModel createFromParcel(Parcel source) {
            return new InfomoduleGetVideoDetailDataModel(source);
        }

        @Override
        public InfomoduleGetVideoDetailDataModel[] newArray(int size) {
            return new InfomoduleGetVideoDetailDataModel[size];
        }
    };
}
