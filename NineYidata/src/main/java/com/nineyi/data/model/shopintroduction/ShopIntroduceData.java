
package com.nineyi.data.model.shopintroduction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ShopIntroduceData implements Parcelable {

    @SerializedName("ShopSummary")
    @Expose
    private String shopSummary;
    @SerializedName("SupplierName")
    @Expose
    private String supplierName;
    @SerializedName("SupplierCompanyPhone")
    @Expose
    private String supplierCompanyPhone;
    @SerializedName("SupplierCompanyCity")
    @Expose
    private String supplierCompanyCity;
    @SerializedName("SupplierCompanyDistrict")
    @Expose
    private String supplierCompanyDistrict;
    @SerializedName("SupplierCompanyAddress")
    @Expose
    private String supplierCompanyAddress;

    public String getShopSummary() {
        return shopSummary;
    }

    public void setShopSummary(String shopSummary) {
        this.shopSummary = shopSummary;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierCompanyPhone() {
        return supplierCompanyPhone;
    }

    public void setSupplierCompanyPhone(String supplierCompanyPhone) {
        this.supplierCompanyPhone = supplierCompanyPhone;
    }

    public String getSupplierCompanyCity() {
        return supplierCompanyCity;
    }

    public void setSupplierCompanyCity(String supplierCompanyCity) {
        this.supplierCompanyCity = supplierCompanyCity;
    }

    public String getSupplierCompanyDistrict() {
        return supplierCompanyDistrict;
    }

    public void setSupplierCompanyDistrict(String supplierCompanyDistrict) {
        this.supplierCompanyDistrict = supplierCompanyDistrict;
    }

    public String getSupplierCompanyAddress() {
        return supplierCompanyAddress;
    }

    public void setSupplierCompanyAddress(String supplierCompanyAddress) {
        this.supplierCompanyAddress = supplierCompanyAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.shopSummary);
        dest.writeString(this.supplierName);
        dest.writeString(this.supplierCompanyPhone);
        dest.writeString(this.supplierCompanyCity);
        dest.writeString(this.supplierCompanyDistrict);
        dest.writeString(this.supplierCompanyAddress);
    }

    public ShopIntroduceData() {
    }

    protected ShopIntroduceData(Parcel in) {
        this.shopSummary = in.readString();
        this.supplierName = in.readString();
        this.supplierCompanyPhone = in.readString();
        this.supplierCompanyCity = in.readString();
        this.supplierCompanyDistrict = in.readString();
        this.supplierCompanyAddress = in.readString();
    }

    public static final Parcelable.Creator<ShopIntroduceData> CREATOR = new Parcelable.Creator<ShopIntroduceData>() {
        @Override
        public ShopIntroduceData createFromParcel(Parcel source) {
            return new ShopIntroduceData(source);
        }

        @Override
        public ShopIntroduceData[] newArray(int size) {
            return new ShopIntroduceData[size];
        }
    };
}
