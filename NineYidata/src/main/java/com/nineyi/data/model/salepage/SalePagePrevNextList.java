package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class SalePagePrevNextList implements Parcelable {

    public int SalePageId;
    public int Number;
    public int CategoryId;
    public ArrayList<SalePageShort> PrevSalepageIds;
    public ArrayList<SalePageShort> NextSalepageIds;

    public SalePagePrevNextList() {
    }

    // Parcelable management
    private SalePagePrevNextList(Parcel in) {
        SalePageId = in.readInt();
        Number = in.readInt();
        CategoryId = in.readInt();
        PrevSalepageIds = in.createTypedArrayList(SalePageShort.CREATOR);
        NextSalepageIds = in.createTypedArrayList(SalePageShort.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SalePageId);
        dest.writeInt(Number);
        dest.writeInt(CategoryId);
        dest.writeTypedList(PrevSalepageIds);
        dest.writeTypedList(NextSalepageIds);
    }

    public static final Parcelable.Creator<SalePagePrevNextList> CREATOR
            = new Parcelable.Creator<SalePagePrevNextList>() {
        public SalePagePrevNextList createFromParcel(Parcel in) {
            return new SalePagePrevNextList(in);
        }

        public SalePagePrevNextList[] newArray(int size) {
            return new SalePagePrevNextList[size];
        }
    };
}
