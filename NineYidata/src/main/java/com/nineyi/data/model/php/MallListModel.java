package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class MallListModel implements Parcelable {

    private static final String FIELD_FEED = "feed";
    private static final String FIELD_TYPE = "type";


    @SerializedName(FIELD_FEED)
    private List<MallListFeed> mMallListFeeds;
    @SerializedName(FIELD_TYPE)
    private String mType;


    public MallListModel() {

    }

    public void setFeeds(List<MallListFeed> mallListFeeds) {
        mMallListFeeds = mallListFeeds;
    }

    public List<MallListFeed> getFeeds() {
        return mMallListFeeds;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }

    public MallListModel(Parcel in) {
        new ArrayList<MallListFeed>();
        in.readTypedList(mMallListFeeds, MallListFeed.CREATOR);
        mType = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MallListModel> CREATOR = new Creator<MallListModel>() {
        public MallListModel createFromParcel(Parcel in) {
            return new MallListModel(in);
        }

        public MallListModel[] newArray(int size) {
            return new MallListModel[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mMallListFeeds);
        dest.writeString(mType);
    }


}