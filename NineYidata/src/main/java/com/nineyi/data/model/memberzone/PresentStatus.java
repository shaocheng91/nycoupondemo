package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


public final class PresentStatus implements Parcelable {

    public PresentStatusData Data;
    public String ReturnCode;
    public String Message;

    public PresentStatus() {
    }

    // Parcelable management
    private PresentStatus(Parcel in) {
        Data = Data.CREATOR.createFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Data.writeToParcel(dest, flags);
    }

    public static final Creator<PresentStatus> CREATOR = new Creator<PresentStatus>() {
        public PresentStatus createFromParcel(Parcel in) {
            return new PresentStatus(in);
        }

        public PresentStatus[] newArray(int size) {
            return new PresentStatus[size];
        }
    };
}
