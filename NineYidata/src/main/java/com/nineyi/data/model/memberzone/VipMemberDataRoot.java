package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class VipMemberDataRoot implements Parcelable {

    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_DATA)
    private VipMemberData mDatum;


    public VipMemberDataRoot() {

    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setDatum(VipMemberData datum) {
        mDatum = datum;
    }

    public VipMemberData getDatum() {
        return mDatum;
    }

    public VipMemberDataRoot(Parcel in) {
        mMessage = in.readString();
        mReturnCode = in.readString();
        mDatum = in.readParcelable(VipMemberData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipMemberDataRoot> CREATOR = new Creator<VipMemberDataRoot>() {
        public VipMemberDataRoot createFromParcel(Parcel in) {
            return new VipMemberDataRoot(in);
        }

        public VipMemberDataRoot[] newArray(int size) {
            return new VipMemberDataRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMessage);
        dest.writeString(mReturnCode);
        dest.writeParcelable(mDatum, flags);
    }


}