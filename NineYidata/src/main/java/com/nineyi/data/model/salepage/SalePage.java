package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePage implements Parcelable {

    public SalePageData data;
    public int categoryId;
    public String categoryText;

    public SalePage() {
    }

    // Parcelable management
    private SalePage(Parcel in) {
        data = SalePageData.CREATOR.createFromParcel(in);
        categoryId = in.readInt();
        categoryText = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        data.writeToParcel(dest, flags);
        dest.writeInt(categoryId);
        dest.writeString(categoryText);
    }

    public static final Parcelable.Creator<SalePage> CREATOR = new Parcelable.Creator<SalePage>() {
        public SalePage createFromParcel(Parcel in) {
            return new SalePage(in);
        }

        public SalePage[] newArray(int size) {
            return new SalePage[size];
        }
    };
}
