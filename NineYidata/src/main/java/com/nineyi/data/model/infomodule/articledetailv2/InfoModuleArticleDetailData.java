
package com.nineyi.data.model.infomodule.articledetailv2;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.infomodule.InfoModuleCommonDetailDataItemList;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoModuleArticleDetailData implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Subtitle")
    @Expose
    private String subtitle;
    @SerializedName("Uuid")
    @Expose
    private String uuid;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("PublishedDate")
    @Expose
    private String publishedDate;
    @SerializedName("Introduction")
    @Expose
    private String introduction;
    @SerializedName("ItemList")
    @Expose
    private List<InfoModuleCommonDetailDataItemList> itemList = new ArrayList<InfoModuleCommonDetailDataItemList>();
    @SerializedName("Shop")
    @Expose
    private InfoModuleArticleDetailDataShop shop;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The Type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * 
     * @param subtitle
     *     The Subtitle
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * 
     * @return
     *     The uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * 
     * @param uuid
     *     The Uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * 
     * @return
     *     The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * 
     * @param imageUrl
     *     The ImageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * 
     * @return
     *     The publishedDate
     */
    public String getPublishedDate() {
        return publishedDate;
    }

    /**
     * 
     * @param publishedDate
     *     The PublishedDate
     */
    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    /**
     * 
     * @return
     *     The introduction
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 
     * @param introduction
     *     The Introduction
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 
     * @return
     *     The itemList
     */
    public List<InfoModuleCommonDetailDataItemList> getItemList() {
        return itemList;
    }

    /**
     * 
     * @param itemList
     *     The ItemList
     */
    public void setItemList(List<InfoModuleCommonDetailDataItemList> itemList) {
        this.itemList = itemList;
    }

    /**
     * 
     * @return
     *     The shop
     */
    public InfoModuleArticleDetailDataShop getShop() {
        return shop;
    }

    /**
     * 
     * @param shop
     *     The Shop
     */
    public void setShop(InfoModuleArticleDetailDataShop shop) {
        this.shop = shop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.type);
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeString(this.uuid);
        dest.writeString(this.imageUrl);
        dest.writeString(this.publishedDate);
        dest.writeString(this.introduction);
        dest.writeList(this.itemList);
        dest.writeParcelable(this.shop, flags);
    }

    public InfoModuleArticleDetailData() {
    }

    protected InfoModuleArticleDetailData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = in.readString();
        this.title = in.readString();
        this.subtitle = in.readString();
        this.uuid = in.readString();
        this.imageUrl = in.readString();
        this.publishedDate = in.readString();
        this.introduction = in.readString();
        this.itemList = new ArrayList<InfoModuleCommonDetailDataItemList>();
        in.readList(this.itemList, InfoModuleArticleDetailDataItemList.class.getClassLoader());
        this.shop = in.readParcelable(InfoModuleArticleDetailDataShop.class.getClassLoader());
    }

    public static final Parcelable.Creator<InfoModuleArticleDetailData> CREATOR = new Parcelable.Creator<InfoModuleArticleDetailData>() {
        @Override
        public InfoModuleArticleDetailData createFromParcel(Parcel source) {
            return new InfoModuleArticleDetailData(source);
        }

        @Override
        public InfoModuleArticleDetailData[] newArray(int size) {
            return new InfoModuleArticleDetailData[size];
        }
    };
}
