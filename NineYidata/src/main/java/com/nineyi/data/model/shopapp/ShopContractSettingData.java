package com.nineyi.data.model.shopapp;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/11/15.
 */
public class ShopContractSettingData implements Parcelable {
    public boolean IsCRMEnabled;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.IsCRMEnabled ? (byte) 1 : (byte) 0);
    }

    public ShopContractSettingData() {
    }

    protected ShopContractSettingData(Parcel in) {
        this.IsCRMEnabled = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ShopContractSettingData> CREATOR
            = new Parcelable.Creator<ShopContractSettingData>() {
        @Override
        public ShopContractSettingData createFromParcel(Parcel source) {
            return new ShopContractSettingData(source);
        }

        @Override
        public ShopContractSettingData[] newArray(int size) {
            return new ShopContractSettingData[size];
        }
    };
}
