
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class SelectedCheckoutShippingTypeGroup implements Parcelable {

    @SerializedName("ShippingProfileTypeDef")
    @Expose
    private String ShippingProfileTypeDef;
    @SerializedName("TotalFee")
    @Expose
    private Integer TotalFee;
    @SerializedName("IsRecommand")
    @Expose
    private Boolean IsRecommand;
    @SerializedName("DeliveryTypeList")
    @Expose
    private List<DeliveryTypeList> mDeliveryTypeList = new ArrayList<DeliveryTypeList>();

    /**
     * 
     * @return
     *     The ShippingProfileTypeDef
     */
    public String getShippingProfileTypeDef() {
        return ShippingProfileTypeDef;
    }

    /**
     * 
     * @param ShippingProfileTypeDef
     *     The ShippingProfileTypeDef
     */
    public void setShippingProfileTypeDef(String ShippingProfileTypeDef) {
        this.ShippingProfileTypeDef = ShippingProfileTypeDef;
    }

    /**
     * 
     * @return
     *     The TotalFee
     */
    public Integer getTotalFee() {
        return TotalFee;
    }

    /**
     * 
     * @param TotalFee
     *     The TotalFee
     */
    public void setTotalFee(Integer TotalFee) {
        this.TotalFee = TotalFee;
    }

    /**
     * 
     * @return
     *     The IsRecommand
     */
    public Boolean getIsRecommand() {
        return IsRecommand;
    }

    /**
     * 
     * @param IsRecommand
     *     The IsRecommand
     */
    public void setIsRecommand(Boolean IsRecommand) {
        this.IsRecommand = IsRecommand;
    }

    /**
     * 
     * @return
     *     The mDeliveryTypeList
     */
    public List<DeliveryTypeList> getDeliveryTypeList() {
        return mDeliveryTypeList;
    }

    /**
     * 
     * @param DeliveryTypeList
     *     The mDeliveryTypeList
     */
    public void setDeliveryTypeList(List<DeliveryTypeList> DeliveryTypeList) {
        this.mDeliveryTypeList = DeliveryTypeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ShippingProfileTypeDef);
        dest.writeValue(this.TotalFee);
        dest.writeValue(this.IsRecommand);
        dest.writeTypedList(this.mDeliveryTypeList);
    }

    public SelectedCheckoutShippingTypeGroup() {
    }

    protected SelectedCheckoutShippingTypeGroup(Parcel in) {
        this.ShippingProfileTypeDef = in.readString();
        this.TotalFee = (Integer) in.readValue(Integer.class.getClassLoader());
        this.IsRecommand = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mDeliveryTypeList = in.createTypedArrayList(DeliveryTypeList.CREATOR);
    }

    public static final Parcelable.Creator<SelectedCheckoutShippingTypeGroup> CREATOR
            = new Parcelable.Creator<SelectedCheckoutShippingTypeGroup>() {
        @Override
        public SelectedCheckoutShippingTypeGroup createFromParcel(Parcel source) {
            return new SelectedCheckoutShippingTypeGroup(source);
        }

        @Override
        public SelectedCheckoutShippingTypeGroup[] newArray(int size) {
            return new SelectedCheckoutShippingTypeGroup[size];
        }
    };
}
