package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;

import com.nineyi.data.model.gson.NineyiDate;


public final class ECouponDetail implements Parcelable {

    public String Code;
    public String ImgUrl;
    public String TotalGiftLimit;
    public int QtyLimit;
    public double DiscountPrice;
    public String TypeDef;
    public NineyiDate StartDateTime;
    public NineyiDate EndDateTime;
    public NineyiDate UsingEndDateTime;
    public NineyiDate UsingStartDateTime;
    public int TotalQty;
    public int Id;
    public String Name;
    public String Description;
    public String EcouponWording;
    public int ShopId;
    public String ShopName;
    public boolean IsSingleCode;
    public int CouponTotalCount;
    public String TakeEndTimeWarningText;
    public double ECouponMaxDiscountLimit;
    public double ECouponUsingMinPrice;
    public boolean IsOnline;
    public boolean IsOffline;


    public ECouponDetail() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Code);
        dest.writeString(this.ImgUrl);
        dest.writeString(this.TotalGiftLimit);
        dest.writeInt(this.QtyLimit);
        dest.writeDouble(this.DiscountPrice);
        dest.writeString(this.TypeDef);
        dest.writeParcelable(this.StartDateTime, flags);
        dest.writeParcelable(this.EndDateTime, flags);
        dest.writeParcelable(this.UsingEndDateTime, flags);
        dest.writeParcelable(this.UsingStartDateTime, flags);
        dest.writeInt(this.TotalQty);
        dest.writeInt(this.Id);
        dest.writeString(this.Name);
        dest.writeString(this.Description);
        dest.writeString(this.EcouponWording);
        dest.writeInt(this.ShopId);
        dest.writeString(this.ShopName);
        dest.writeByte(this.IsSingleCode ? (byte) 1 : (byte) 0);
        dest.writeInt(this.CouponTotalCount);
        dest.writeString(this.TakeEndTimeWarningText);
        dest.writeDouble(this.ECouponMaxDiscountLimit);
        dest.writeDouble(this.ECouponUsingMinPrice);
        dest.writeByte(this.IsOnline ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsOffline ? (byte) 1 : (byte) 0);
    }

    protected ECouponDetail(Parcel in) {
        this.Code = in.readString();
        this.ImgUrl = in.readString();
        this.TotalGiftLimit = in.readString();
        this.QtyLimit = in.readInt();
        this.DiscountPrice = in.readDouble();
        this.TypeDef = in.readString();
        this.StartDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.EndDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.UsingEndDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.UsingStartDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.TotalQty = in.readInt();
        this.Id = in.readInt();
        this.Name = in.readString();
        this.Description = in.readString();
        this.EcouponWording = in.readString();
        this.ShopId = in.readInt();
        this.ShopName = in.readString();
        this.IsSingleCode = in.readByte() != 0;
        this.CouponTotalCount = in.readInt();
        this.TakeEndTimeWarningText = in.readString();
        this.ECouponMaxDiscountLimit = in.readDouble();
        this.ECouponUsingMinPrice = in.readDouble();
        this.IsOnline = in.readByte() != 0;
        this.IsOffline = in.readByte() != 0;
    }

    public static final Creator<ECouponDetail> CREATOR = new Creator<ECouponDetail>() {
        @Override
        public ECouponDetail createFromParcel(Parcel source) {
            return new ECouponDetail(source);
        }

        @Override
        public ECouponDetail[] newArray(int size) {
            return new ECouponDetail[size];
        }
    };
}
