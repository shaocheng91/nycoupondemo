package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class ShoppingCartFreeGiftPromotion implements Parcelable{

    public int PromotionId;
    public int PromotionConditionId;
    public ArrayList<ShoppingCartFreeGiftSalePage> FreeGiftSalePageList;

    public ShoppingCartFreeGiftPromotion() {

    }

    public ShoppingCartFreeGiftPromotion(Parcel in) {
        PromotionId = in.readInt();
        PromotionConditionId = in.readInt();
        FreeGiftSalePageList = in.createTypedArrayList(ShoppingCartFreeGiftSalePage.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShoppingCartFreeGiftPromotion> CREATOR = new Creator<ShoppingCartFreeGiftPromotion>() {
        public ShoppingCartFreeGiftPromotion createFromParcel(Parcel in) {
            return new ShoppingCartFreeGiftPromotion(in);
        }

        public ShoppingCartFreeGiftPromotion[] newArray(int size) {
            return new ShoppingCartFreeGiftPromotion[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(PromotionId);
        dest.writeInt(PromotionConditionId);
        dest.writeTypedList(FreeGiftSalePageList);
    }


}