package com.nineyi.data.model.memberzone;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;


public class VipShopMemberCard implements Parcelable {

    private static final String FIELD_ID = "Id";
    private static final String FIELD_BENEFITS = "Benefits";
    private static final String FIELD_SHOP_ID = "ShopId";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_BARCODE_VALUE = "BarcodeValue";
    private static final String FIELD_BARCODE_TYPE_DEF = "BarcodeTypeDef";


    @SerializedName(FIELD_ID)
    private long mId;
    @SerializedName(FIELD_BENEFITS)
    private String mBenefit;
    @SerializedName(FIELD_SHOP_ID)
    private int mShopId;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_BARCODE_VALUE)
    private String mBarcodeValue;
    @SerializedName(FIELD_BARCODE_TYPE_DEF)
    private String mBarcodeTypeDef;


    public VipShopMemberCard() {

    }

    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setBenefit(String benefit) {
        mBenefit = benefit;
    }

    public String getBenefit() {
        return mBenefit;
    }

    public void setShopId(int shopId) {
        mShopId = shopId;
    }

    public int getShopId() {
        return mShopId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setBarcodeValue(String barcodeValue) {
        mBarcodeValue = barcodeValue;
    }

    public String getBarcodeValue() {
        return mBarcodeValue;
    }

    public void setBarcodeTypeDef(String barcodeTypeDef) {
        mBarcodeTypeDef = barcodeTypeDef;
    }

    public String getBarcodeTypeDef() {
        return mBarcodeTypeDef;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof VipShopMemberCard) {
            return ((VipShopMemberCard) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ((Long) mId).hashCode();
    }

    public VipShopMemberCard(Parcel in) {
        mId = in.readLong();
        mBenefit = in.readString();
        mShopId = in.readInt();
        mName = in.readString();
        mBarcodeValue = in.readString();
        mBarcodeTypeDef = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipShopMemberCard> CREATOR = new Creator<VipShopMemberCard>() {
        public VipShopMemberCard createFromParcel(Parcel in) {
            return new VipShopMemberCard(in);
        }

        public VipShopMemberCard[] newArray(int size) {
            return new VipShopMemberCard[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mBenefit);
        dest.writeInt(mShopId);
        dest.writeString(mName);
        dest.writeString(mBarcodeValue);
        dest.writeString(mBarcodeTypeDef);
    }


}