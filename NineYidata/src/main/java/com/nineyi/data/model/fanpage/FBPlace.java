package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBPlace implements Parcelable {

    String id;
    String name;
    FBLocation location;

    protected FBPlace(Parcel in) {
        id = in.readString();
        name = in.readString();
        location = (FBLocation) in.readValue(FBLocation.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeValue(location);
    }

    public static final Parcelable.Creator<FBPlace> CREATOR = new Parcelable.Creator<FBPlace>() {
        @Override
        public FBPlace createFromParcel(Parcel in) {
            return new FBPlace(in);
        }

        @Override
        public FBPlace[] newArray(int size) {
            return new FBPlace[size];
        }
    };
}