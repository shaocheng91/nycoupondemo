package com.nineyi.data.model.infomodule;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoModuleCommonDetailDataItemList implements Parcelable {

    @SerializedName("SalePageId")
    @Expose
    private Integer salePageId;
    @SerializedName("SubTitle")
    @Expose
    private String subTitle;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("SuggestPrice")
    @Expose
    private Double suggestPrice;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("Sort")
    @Expose
    private Integer sort;
    @SerializedName("ShopId")
    @Expose
    private Integer shopId;
    @SerializedName("SellingStartDateTime")
    @Expose
    private String sellingStartDateTime;
    @SerializedName("Tags")
    @Expose
    private List<String> tags = new ArrayList<String>();
    @SerializedName("PicList")
    @Expose
    private List<String> picList = new ArrayList<String>();
    @SerializedName("PicUrl")
    @Expose
    private String picUrl;

    /**
     *
     * @return
     *     The salePageId
     */
    public Integer getSalePageId() {
        return salePageId;
    }

    /**
     *
     * @param salePageId
     *     The SalePageId
     */
    public void setSalePageId(Integer salePageId) {
        this.salePageId = salePageId;
    }

    /**
     *
     * @return
     *     The subTitle
     */
    public String getSubTitle() {
        return subTitle;
    }

    /**
     *
     * @param subTitle
     *     The SubTitle
     */
    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    /**
     *
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     *     The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     *     The suggestPrice
     */
    public Double getSuggestPrice() {
        return suggestPrice;
    }

    /**
     *
     * @param suggestPrice
     *     The SuggestPrice
     */
    public void setSuggestPrice(Double suggestPrice) {
        this.suggestPrice = suggestPrice;
    }

    /**
     *
     * @return
     *     The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     *
     * @param price
     *     The Price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     *
     * @return
     *     The sort
     */
    public Integer getSort() {
        return sort;
    }

    /**
     *
     * @param sort
     *     The Sort
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     *
     * @return
     *     The shopId
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     *
     * @param shopId
     *     The ShopId
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    /**
     *
     * @return
     *     The sellingStartDateTime
     */
    public String getSellingStartDateTime() {
        return sellingStartDateTime;
    }

    /**
     *
     * @param sellingStartDateTime
     *     The SellingStartDateTime
     */
    public void setSellingStartDateTime(String sellingStartDateTime) {
        this.sellingStartDateTime = sellingStartDateTime;
    }

    /**
     *
     * @return
     *     The tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     *
     * @param tags
     *     The Tags
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     *
     * @return
     *     The picList
     */
    public List<String> getPicList() {
        return picList;
    }

    /**
     *
     * @param picList
     *     The PicList
     */
    public void setPicList(List<String> picList) {
        this.picList = picList;
    }

    /**
     *
     * @return
     *     The picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     *
     * @param picUrl
     *     The PicUrl
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.salePageId);
        dest.writeString(this.subTitle);
        dest.writeString(this.title);
        dest.writeValue(this.suggestPrice);
        dest.writeValue(this.price);
        dest.writeValue(this.sort);
        dest.writeValue(this.shopId);
        dest.writeString(this.sellingStartDateTime);
        dest.writeStringList(this.tags);
        dest.writeStringList(this.picList);
        dest.writeString(this.picUrl);
    }

    public InfoModuleCommonDetailDataItemList() {
    }

    protected InfoModuleCommonDetailDataItemList(Parcel in) {
        this.salePageId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subTitle = in.readString();
        this.title = in.readString();
        this.suggestPrice = (Double) in.readValue(Double.class.getClassLoader());
        this.price = (Double) in.readValue(Double.class.getClassLoader());
        this.sort = (Integer) in.readValue(Integer.class.getClassLoader());
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sellingStartDateTime = in.readString();
        this.tags = in.createStringArrayList();
        this.picList = in.createStringArrayList();
        this.picUrl = in.readString();
    }

    public static final Parcelable.Creator<InfoModuleCommonDetailDataItemList> CREATOR
            = new Parcelable.Creator<InfoModuleCommonDetailDataItemList>() {
        @Override
        public InfoModuleCommonDetailDataItemList createFromParcel(Parcel source) {
            return new InfoModuleCommonDetailDataItemList(source);
        }

        @Override
        public InfoModuleCommonDetailDataItemList[] newArray(int size) {
            return new InfoModuleCommonDetailDataItemList[size];
        }
    };
}
