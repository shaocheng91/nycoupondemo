package com.nineyi.data.model.trace;

import android.os.Parcel;
import android.os.Parcelable;


public final class TraceShop implements Parcelable {

    public int ShopId;
    public String ShopName;
    public String ShopLogoUrl;
    public String DateTime;

    public TraceShop() {
    }

    // Parcelable management
    private TraceShop(Parcel in) {
        ShopId = in.readInt();
        ShopName = in.readString();
        ShopLogoUrl = in.readString();
        DateTime = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopId);
        dest.writeString(ShopName);
        dest.writeString(ShopLogoUrl);
        dest.writeString(DateTime);
    }

    public static final Parcelable.Creator<TraceShop> CREATOR = new Parcelable.Creator<TraceShop>() {
        public TraceShop createFromParcel(Parcel in) {
            return new TraceShop(in);
        }

        public TraceShop[] newArray(int size) {
            return new TraceShop[size];
        }
    };
}
