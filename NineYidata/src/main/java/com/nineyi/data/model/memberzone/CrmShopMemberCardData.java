package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by ReeceCheng on 2016/11/7.
 */
public class CrmShopMemberCardData implements Parcelable {
    public MemberCardSetting MemberCardSetting;
    public ArrayList<MemberCardInfo> MemberCardList;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.MemberCardSetting, flags);
        dest.writeTypedList(this.MemberCardList);
    }

    public CrmShopMemberCardData() {
    }

    protected CrmShopMemberCardData(Parcel in) {
        this.MemberCardSetting = in
                .readParcelable(com.nineyi.data.model.memberzone.MemberCardSetting.class.getClassLoader());
        this.MemberCardList = in.createTypedArrayList(MemberCardInfo.CREATOR);
    }

    public static final Parcelable.Creator<CrmShopMemberCardData> CREATOR
            = new Parcelable.Creator<CrmShopMemberCardData>() {
        @Override
        public CrmShopMemberCardData createFromParcel(Parcel source) {
            return new CrmShopMemberCardData(source);
        }

        @Override
        public CrmShopMemberCardData[] newArray(int size) {
            return new CrmShopMemberCardData[size];
        }
    };
}
