package com.nineyi.data.model.memberzone;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;


public class VipMember implements Parcelable {

    private static final String FIELD_MEMBER_ID = "MemberId";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_STATUS_TYPE_DEF = "StatusTypeDef";
    private static final String FIELD_SHOP_ID = "ShopId";
    private static final String FIELD_OUTER_ID = "OuterId";


    @SerializedName(FIELD_MEMBER_ID)
    private int mMemberId;
    @SerializedName(FIELD_ID)
    private long mId;
    @SerializedName(FIELD_STATUS_TYPE_DEF)
    private String mStatusTypeDef;
    @SerializedName(FIELD_SHOP_ID)
    private int mShopId;
    @SerializedName(FIELD_OUTER_ID)
    private String mOuterId;


    public VipMember() {

    }

    public void setMemberId(int memberId) {
        mMemberId = memberId;
    }

    public int getMemberId() {
        return mMemberId;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setStatusTypeDef(String statusTypeDef) {
        mStatusTypeDef = statusTypeDef;
    }

    public String getStatusTypeDef() {
        return mStatusTypeDef;
    }

    public void setShopId(int shopId) {
        mShopId = shopId;
    }

    public int getShopId() {
        return mShopId;
    }

    public void setOuterId(String outerId) {
        mOuterId = outerId;
    }

    public String getOuterId() {
        return mOuterId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof VipMember) {
            return ((VipMember) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ((Long) mId).hashCode();
    }

    public VipMember(Parcel in) {
        mMemberId = in.readInt();
        mId = in.readLong();
        mStatusTypeDef = in.readString();
        mShopId = in.readInt();
        mOuterId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipMember> CREATOR = new Creator<VipMember>() {
        public VipMember createFromParcel(Parcel in) {
            return new VipMember(in);
        }

        public VipMember[] newArray(int size) {
            return new VipMember[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mMemberId);
        dest.writeLong(mId);
        dest.writeString(mStatusTypeDef);
        dest.writeInt(mShopId);
        dest.writeString(mOuterId);
    }


}