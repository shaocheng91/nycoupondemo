
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ReachQtyPromotionList implements Parcelable {

    @SerializedName("TotalPayment")
    @Expose
    private Double totalPayment;

    @SerializedName("TotalQty")
    @Expose
    private Integer totalQty;
    @SerializedName("PromotionId")
    @Expose
    private Long promotionId;
    @SerializedName("SalePageIdList")
    @Expose
    private List<Long> salePageIdList = new ArrayList<Long>();
    @SerializedName("CalculatedConditions")
    @Expose
    private List<CalculatedCondition> calculatedConditions = new ArrayList<CalculatedCondition>();

    public Integer getTotalQty() {
        return totalQty;
    }
    /**
     * 
     * @return
     *     The totalPayment
     */
    public Double getTotalPayment() {
        return totalPayment;
    }

    /**
     * 
     * @param totalPayment
     *     The TotalPayment
     */
    public void setTotalPayment(Double totalPayment) {
        this.totalPayment = totalPayment;
    }

    /**
     * 
     * @return
     *     The promotionId
     */
    public Long getPromotionId() {
        return promotionId;
    }

    /**
     * 
     * @param promotionId
     *     The PromotionId
     */
    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    /**
     * 
     * @return
     *     The salePageIdList
     */
    public List<Long> getSalePageIdList() {
        return salePageIdList;
    }

    /**
     * 
     * @param salePageIdList
     *     The SalePageIdList
     */
    public void setSalePageIdList(List<Long> salePageIdList) {
        this.salePageIdList = salePageIdList;
    }

    /**
     * 
     * @return
     *     The calculatedConditions
     */
    public List<CalculatedCondition> getCalculatedConditions() {
        return calculatedConditions;
    }

    /**
     * 
     * @param calculatedConditions
     *     The CalculatedConditions
     */
    public void setCalculatedConditions(List<CalculatedCondition> calculatedConditions) {
        this.calculatedConditions = calculatedConditions;
    }

    public ReachQtyPromotionList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.totalPayment);
        dest.writeValue(this.totalQty);
        dest.writeValue(this.promotionId);
        dest.writeList(this.salePageIdList);
        dest.writeTypedList(this.calculatedConditions);
    }

    protected ReachQtyPromotionList(Parcel in) {
        this.totalPayment = (Double) in.readValue(Double.class.getClassLoader());
        this.totalQty = (Integer) in.readValue(Integer.class.getClassLoader());
        this.promotionId = (Long) in.readValue(Long.class.getClassLoader());
        this.salePageIdList = new ArrayList<Long>();
        in.readList(this.salePageIdList, Long.class.getClassLoader());
        this.calculatedConditions = in.createTypedArrayList(CalculatedCondition.CREATOR);
    }

    public static final Creator<ReachQtyPromotionList> CREATOR = new Creator<ReachQtyPromotionList>() {
        @Override
        public ReachQtyPromotionList createFromParcel(Parcel source) {
            return new ReachQtyPromotionList(source);
        }

        @Override
        public ReachQtyPromotionList[] newArray(int size) {
            return new ReachQtyPromotionList[size];
        }
    };
}
