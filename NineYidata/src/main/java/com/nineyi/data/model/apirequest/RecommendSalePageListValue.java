package com.nineyi.data.model.apirequest;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by toby on 2015/3/10.
 */
public class RecommendSalePageListValue implements Parcelable {

    public int shopId;

    public String orderby;

    public int startIndex;

    public int maxCount;

    public List<Integer> cidList;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.shopId);
        dest.writeString(this.orderby);
        dest.writeInt(this.startIndex);
        dest.writeInt(this.maxCount);
        dest.writeList(this.cidList);
    }

    public RecommendSalePageListValue() {
    }

    protected RecommendSalePageListValue(Parcel in) {
        this.shopId = in.readInt();
        this.orderby = in.readString();
        this.startIndex = in.readInt();
        this.maxCount = in.readInt();
        this.cidList = new ArrayList<Integer>();
        in.readList(this.cidList, Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<RecommendSalePageListValue> CREATOR
            = new Parcelable.Creator<RecommendSalePageListValue>() {
        @Override
        public RecommendSalePageListValue createFromParcel(Parcel source) {
            return new RecommendSalePageListValue(source);
        }

        @Override
        public RecommendSalePageListValue[] newArray(int size) {
            return new RecommendSalePageListValue[size];
        }
    };
}
