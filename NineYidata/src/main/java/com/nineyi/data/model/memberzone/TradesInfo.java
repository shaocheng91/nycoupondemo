package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class TradesInfo implements Parcelable {

    private static final String FIELD_LAST_MOBILE_TRADES_DATE_TIME = "LastMobileTradesDateTime";
    private static final String FIELD_MOBILE_TRADES_SUM = "MobileTradesSum";
    private static final String FIELD_LAST_LOCATION_TRADES_DATE_TIME = "LastLocationTradesDateTime";
    private static final String FIELD_LOCATION_TRADES_SUM = "LocationTradesSum";
    private static final String FIELD_LAST_TRADES_DATA_UPDATED_DATE_TIME = "LastTradesDataUpdatedDateTime";


    @SerializedName(FIELD_LAST_MOBILE_TRADES_DATE_TIME)
    private String mLastMobileTradesDateTime;
    @SerializedName(FIELD_MOBILE_TRADES_SUM)
    private double mMobileTradesSum;
    @SerializedName(FIELD_LAST_LOCATION_TRADES_DATE_TIME)
    private String mLastLocationTradesDateTime;
    @SerializedName(FIELD_LOCATION_TRADES_SUM)
    private double mLocationTradesSum;
    @SerializedName(FIELD_LAST_TRADES_DATA_UPDATED_DATE_TIME)
    private String mLastTradesDataUpdatedDateTime;


    public TradesInfo() {

    }

    public void setLastMobileTradesDateTime(String lastMobileTradesDateTime) {
        mLastMobileTradesDateTime = lastMobileTradesDateTime;
    }

    public String getLastMobileTradesDateTime() {
        return mLastMobileTradesDateTime;
    }

    public void setMobileTradesSum(int mobileTradesSum) {
        mMobileTradesSum = mobileTradesSum;
    }

    public double getMobileTradesSum() {
        return mMobileTradesSum;
    }

    public void setLastLocationTradesDateTime(String lastLocationTradesDateTime) {
        mLastLocationTradesDateTime = lastLocationTradesDateTime;
    }

    public String getLastLocationTradesDateTime() {
        return mLastLocationTradesDateTime;
    }

    public void setLocationTradesSum(int locationTradesSum) {
        mLocationTradesSum = locationTradesSum;
    }

    public double getLocationTradesSum() {
        return mLocationTradesSum;
    }

    public void setLastTradesDataUpdatedDateTime(String lastTradesDataUpdatedDateTime) {
        mLastTradesDataUpdatedDateTime = lastTradesDataUpdatedDateTime;
    }

    public String getLastTradesDataUpdatedDateTime() {
        return mLastTradesDataUpdatedDateTime;
    }

    public TradesInfo(Parcel in) {
        mLastMobileTradesDateTime = in.readString();
        mMobileTradesSum = in.readDouble();
        mLastLocationTradesDateTime = in.readString();
        mLocationTradesSum = in.readDouble();
        mLastTradesDataUpdatedDateTime = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TradesInfo> CREATOR = new Creator<TradesInfo>() {
        public TradesInfo createFromParcel(Parcel in) {
            return new TradesInfo(in);
        }

        public TradesInfo[] newArray(int size) {
            return new TradesInfo[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mLastMobileTradesDateTime);
        dest.writeDouble(mMobileTradesSum);
        dest.writeString(mLastLocationTradesDateTime);
        dest.writeDouble(mLocationTradesSum);
        dest.writeString(mLastTradesDataUpdatedDateTime);
    }


}