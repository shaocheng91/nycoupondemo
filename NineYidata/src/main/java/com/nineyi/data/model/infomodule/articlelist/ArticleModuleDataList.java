
package com.nineyi.data.model.infomodule.articlelist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ArticleModuleDataList implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Subtitle")
    @Expose
    private String subtitle;
    @SerializedName("Uuid")
    @Expose
    private String uuid;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;

    public String getPublishedDate() {
        return publishedDate;
    }

    @SerializedName("PublishedDate")
    @Expose
    private String publishedDate;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The Type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * 
     * @param subtitle
     *     The Subtitle
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * 
     * @return
     *     The uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * 
     * @param uuid
     *     The Uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     *
     * @return
     *     The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     *     The ImageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArticleModuleDataList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.type);
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeString(this.uuid);
        dest.writeString(this.imageUrl);
        dest.writeString(this.publishedDate);
    }

    protected ArticleModuleDataList(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = in.readString();
        this.title = in.readString();
        this.subtitle = in.readString();
        this.uuid = in.readString();
        this.imageUrl = in.readString();
        this.publishedDate = in.readString();
    }

    public static final Creator<ArticleModuleDataList> CREATOR = new Creator<ArticleModuleDataList>() {
        @Override
        public ArticleModuleDataList createFromParcel(Parcel source) {
            return new ArticleModuleDataList(source);
        }

        @Override
        public ArticleModuleDataList[] newArray(int size) {
            return new ArticleModuleDataList[size];
        }
    };
}
