package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class VipMemberCustomRuleList implements Parcelable {

    private static final String FIELD_RULE_TITLE = "RuleTitle";
    private static final String FIELD_RULE_VALUE = "RuleValue";

    @SerializedName(FIELD_RULE_TITLE)
    private String mRuleTitle;

    @SerializedName(FIELD_RULE_VALUE)
    private String mRuleValue;


    public VipMemberCustomRuleList() {
    }

    public String getRuleTitle() {
        return mRuleTitle;
    }

    public String getRuleValue() {
        return mRuleValue;
    }

    public VipMemberCustomRuleList(Parcel in) {
        mRuleTitle = in.readString();
        mRuleValue = in.readString();
    }

    public static final Creator<VipMemberCustomRuleList> CREATOR = new Creator<VipMemberCustomRuleList>() {
        public VipMemberCustomRuleList createFromParcel(Parcel in) {
            return new VipMemberCustomRuleList(in);
        }

        public VipMemberCustomRuleList[] newArray(int size) {
            return new VipMemberCustomRuleList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mRuleTitle);
        dest.writeString(mRuleValue);
    }


}