package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/7/28.
 */
public class ShopSalePageBySearch implements Parcelable {
    public String ReturnCode;
    public SalePageBySearch Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.Message);
    }

    public ShopSalePageBySearch() {
    }

    protected ShopSalePageBySearch(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(SalePageBySearch.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<ShopSalePageBySearch> CREATOR = new Parcelable.Creator<ShopSalePageBySearch>() {
        @Override
        public ShopSalePageBySearch createFromParcel(Parcel source) {
            return new ShopSalePageBySearch(source);
        }

        @Override
        public ShopSalePageBySearch[] newArray(int size) {
            return new ShopSalePageBySearch[size];
        }
    };
}
