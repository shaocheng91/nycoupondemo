package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public final class PhpLbsScheduleOfWeek implements Parcelable {

    public int monday;
    public int tuesday;
    public int wednesday;
    public int thursday;
    public int friday;
    public int saturday;
    public int sunday;

    public PhpLbsScheduleOfWeek() {
    }

    // Parcelable management
    private PhpLbsScheduleOfWeek(Parcel in) {
        monday = in.readInt();
        tuesday = in.readInt();
        wednesday = in.readInt();
        thursday = in.readInt();
        friday = in.readInt();
        saturday = in.readInt();
        sunday = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(monday);
        dest.writeInt(tuesday);
        dest.writeInt(wednesday);
        dest.writeInt(thursday);
        dest.writeInt(friday);
        dest.writeInt(saturday);
        dest.writeInt(sunday);
    }

    public static final Parcelable.Creator<PhpLbsScheduleOfWeek> CREATOR
            = new Parcelable.Creator<PhpLbsScheduleOfWeek>() {
        public PhpLbsScheduleOfWeek createFromParcel(Parcel in) {
            return new PhpLbsScheduleOfWeek(in);
        }

        public PhpLbsScheduleOfWeek[] newArray(int size) {
            return new PhpLbsScheduleOfWeek[size];
        }
    };
}
