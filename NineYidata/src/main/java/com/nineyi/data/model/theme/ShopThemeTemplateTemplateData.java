package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeTemplateTemplateData implements Parcelable {
    public int ShopId;
    public int ThemeId;
    public String ThemeName;
    public String FriendlyName;
    public String DeviceMode;
    public ArrayList<ShopThemeTemplateList> TemplateList;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ShopId);
        dest.writeInt(this.ThemeId);
        dest.writeString(this.ThemeName);
        dest.writeString(this.FriendlyName);
        dest.writeString(this.DeviceMode);
        dest.writeTypedList(TemplateList);
    }

    public ShopThemeTemplateTemplateData() {
    }

    protected ShopThemeTemplateTemplateData(Parcel in) {
        this.ShopId = in.readInt();
        this.ThemeId = in.readInt();
        this.ThemeName = in.readString();
        this.FriendlyName = in.readString();
        this.DeviceMode = in.readString();
        this.TemplateList = in.createTypedArrayList(ShopThemeTemplateList.CREATOR);
    }

    public static final Parcelable.Creator<ShopThemeTemplateTemplateData> CREATOR = new Parcelable.Creator<ShopThemeTemplateTemplateData>() {
        public ShopThemeTemplateTemplateData createFromParcel(Parcel source) {
            return new ShopThemeTemplateTemplateData(source);
        }

        public ShopThemeTemplateTemplateData[] newArray(int size) {
            return new ShopThemeTemplateTemplateData[size];
        }
    };
}
