package com.nineyi.data.model.infomodule;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleCommonShopSimpleInfoModel implements Parcelable {

    public int ShopId;
    public String ShopName;
    public String HeaderStyle;

    public InfomoduleCommonShopSimpleInfoModel(Parcel in) {
        ShopId = in.readInt();
        ShopName = in.readString();
        HeaderStyle = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopId);
        dest.writeString(ShopName);
        dest.writeString(HeaderStyle);
    }

    public static Parcelable.Creator<InfomoduleCommonShopSimpleInfoModel> CREATOR
            = new Parcelable.Creator<InfomoduleCommonShopSimpleInfoModel>() {
        @Override
        public InfomoduleCommonShopSimpleInfoModel createFromParcel(Parcel source) {
            return new InfomoduleCommonShopSimpleInfoModel(source);
        }

        @Override
        public InfomoduleCommonShopSimpleInfoModel[] newArray(int size) {
            return new InfomoduleCommonShopSimpleInfoModel[size];
        }
    };
}
