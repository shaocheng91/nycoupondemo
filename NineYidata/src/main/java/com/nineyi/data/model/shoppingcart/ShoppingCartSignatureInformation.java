package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2015/11/4.
 */
public class ShoppingCartSignatureInformation implements Parcelable {
    public String Signature;
    public String TimeStamp;
    public ShoppingCartSignaturePlaintext SignaturePlaintext;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Signature);
        dest.writeString(this.TimeStamp);
        dest.writeParcelable(this.SignaturePlaintext, 0);
    }

    public ShoppingCartSignatureInformation() {
    }

    protected ShoppingCartSignatureInformation(Parcel in) {
        this.Signature = in.readString();
        this.TimeStamp = in.readString();
        this.SignaturePlaintext = in.readParcelable(ShoppingCartSignaturePlaintext.class.getClassLoader());
    }

    public static final Parcelable.Creator<ShoppingCartSignatureInformation> CREATOR = new Parcelable.Creator<ShoppingCartSignatureInformation>() {
        public ShoppingCartSignatureInformation createFromParcel(Parcel source) {
            return new ShoppingCartSignatureInformation(source);
        }

        public ShoppingCartSignatureInformation[] newArray(int size) {
            return new ShoppingCartSignatureInformation[size];
        }
    };
}
