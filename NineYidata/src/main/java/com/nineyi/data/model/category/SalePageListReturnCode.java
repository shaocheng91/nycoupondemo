package com.nineyi.data.model.category;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kelsey on 2016/12/7.
 */

public class SalePageListReturnCode implements Parcelable {
    private String ReturnCode;

    private SalePageListData Data;

    private String Message;

    public static Creator<SalePageListReturnCode> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.getReturnCode());
        dest.writeParcelable(this.getData(), flags);
        dest.writeString(this.getMessage());
    }

    public SalePageListReturnCode() {
    }

    protected SalePageListReturnCode(Parcel in) {
        this.setReturnCode(in.readString());
        this.setData((SalePageListData) in.readParcelable(SalePageListData.class.getClassLoader()));
        this.setMessage(in.readString());
    }

    private static final Creator<SalePageListReturnCode> CREATOR = new Creator<SalePageListReturnCode>() {
        @Override
        public SalePageListReturnCode createFromParcel(Parcel source) {
            return new SalePageListReturnCode(source);
        }

        @Override
        public SalePageListReturnCode[] newArray(int size) {
            return new SalePageListReturnCode[size];
        }
    };

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public SalePageListData getData() {
        return Data;
    }

    public void setData(SalePageListData data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
