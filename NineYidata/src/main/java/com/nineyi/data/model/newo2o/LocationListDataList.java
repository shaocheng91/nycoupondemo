package com.nineyi.data.model.newo2o;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class LocationListDataList implements Parcelable{

    private static final String FIELD_GALLERY = "Gallery";
    private static final String FIELD_WEEKEND_TIME = "WeekendTime";
    private static final String FIELD_STATUS = "Status";
    private static final String FIELD_ZIP_CODE = "ZipCode";
    private static final String FIELD_WIFI = "Wifi";
    private static final String FIELD_START_DATE_TIME = "StartDateTime";
    private static final String FIELD_LATITUDE = "Latitude";
    private static final String FIELD_MOBILE = "Mobile";
    private static final String FIELD_PARKING = "Parking";
    private static final String FIELD_TEL = "Tel";
    private static final String FIELD_IMAGE_URL = "ImageUrl";
    private static final String FIELD_SEQUENCE = "Sequence";
    private static final String FIELD_UUID = "Uuid";
    private static final String FIELD_CITY_NAME = "CityName";
    private static final String FIELD_LONGITUDE = "Longitude";
    private static final String FIELD_GALLERY_COUNT = "GalleryCount";
    private static final String FIELD_DOMESTIC = "Domestic";
    private static final String FIELD_CITY_ID = "CityId";
    private static final String FIELD_SHOP_ID = "ShopId";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_AREA_NAME = "AreaName";
    private static final String FIELD_END_DATE_TIME = "EndDateTime";
    private static final String FIELD_OPERATION_TIME = "OperationTime";
    private static final String FIELD_CREDIT_CARD = "CreditCard";
    private static final String FIELD_TEL_PREPEND = "TelPrepend";
    private static final String FIELD_ADDRESS = "Address";
    private static final String FIELD_NORMAL_TIME = "NormalTime";
    private static final String FIELD_GALLERY_LIST = "GalleryList";
    private static final String FIELD_REMARK = "Remark";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_AREA_ID = "AreaId";
    private static final String FIELD_INTRODUCTION = "Introduction";
    private static final String FIELD_DISTANCE = "Distance";
    private static final String FIELD_ISAVAILABLELOCATIONPICKUP = "IsAvailableLocationPickup";

    @SerializedName(FIELD_GALLERY)
    private String mGallery;
    @SerializedName(FIELD_WEEKEND_TIME)
    private String mWeekendTime;
    @SerializedName(FIELD_STATUS)
    private String mStatus;
    @SerializedName(FIELD_ZIP_CODE)
    private int mZipCode;
    @SerializedName(FIELD_WIFI)
    private int mWifi;
    @SerializedName(FIELD_START_DATE_TIME)
    private String mStartDateTime;
    @SerializedName(FIELD_LATITUDE)
    private String mLatitude;
    @SerializedName(FIELD_MOBILE)
    private String mMobile;
    @SerializedName(FIELD_PARKING)
    private int mParking;
    @SerializedName(FIELD_TEL)
    private String mTel;
    @SerializedName(FIELD_IMAGE_URL)
    private String mImageUrl;
    @SerializedName(FIELD_SEQUENCE)
    private int mSequence;
    @SerializedName(FIELD_UUID)
    private String mUuid;
    @SerializedName(FIELD_CITY_NAME)
    private String mCityName;
    @SerializedName(FIELD_LONGITUDE)
    private String mLongitude;
    @SerializedName(FIELD_GALLERY_COUNT)
    private String mGalleryCount;
    @SerializedName(FIELD_DOMESTIC)
    private int mDomestic;
    @SerializedName(FIELD_CITY_ID)
    private int mCityId;
    @SerializedName(FIELD_SHOP_ID)
    private int mShopId;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_AREA_NAME)
    private String mAreaName;
    @SerializedName(FIELD_END_DATE_TIME)
    private String mEndDateTime;
    @SerializedName(FIELD_OPERATION_TIME)
    private String mOperationTime;
    @SerializedName(FIELD_CREDIT_CARD)
    private int mCreditCard;
    @SerializedName(FIELD_TEL_PREPEND)
    private String mTelPrepend;
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @SerializedName(FIELD_NORMAL_TIME)
    private String mNormalTime;
    @SerializedName(FIELD_GALLERY_LIST)
    private ArrayList<String> mGalleryLists;
    @SerializedName(FIELD_REMARK)
    private String mRemark;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_AREA_ID)
    private int mAreaId;
    @SerializedName(FIELD_INTRODUCTION)
    private String mIntroduction;
    @SerializedName(FIELD_DISTANCE)
    private String mDistance;
    @SerializedName(FIELD_ISAVAILABLELOCATIONPICKUP)
    private Boolean mIsAvailableLocationPickup;

    public Boolean getIsAvailableLocationPickup() {
        return mIsAvailableLocationPickup;
    }

    private int mType;
    public LocationListDataList(){

    }

    public void setGallery(String gallery) {
        mGallery = gallery;
    }

    public String getGallery() {
        return mGallery;
    }

    public void setWeekendTime(String weekendTime) {
        mWeekendTime = weekendTime;
    }

    public String getWeekendTime() {
        return mWeekendTime;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setZipCode(int zipCode) {
        mZipCode = zipCode;
    }

    public int getZipCode() {
        return mZipCode;
    }

    public void setWifi(int wifi) {
        mWifi = wifi;
    }

    public int getWifi() {
        return mWifi;
    }

    public void setStartDateTime(String startDateTime) {
        mStartDateTime = startDateTime;
    }

    public String getStartDateTime() {
        return mStartDateTime;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setParking(int parking) {
        mParking = parking;
    }

    public int getParking() {
        return mParking;
    }

    public void setTel(String tel) {
        mTel = tel;
    }

    public String getTel() {
        return mTel;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setSequence(int sequence) {
        mSequence = sequence;
    }

    public int getSequence() {
        return mSequence;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setCityName(String cityName) {
        mCityName = cityName;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setGalleryCount(String galleryCount) {
        mGalleryCount = galleryCount;
    }

    public String getGalleryCount() {
        return mGalleryCount;
    }

    public void setDomestic(int domestic) {
        mDomestic = domestic;
    }

    public int getDomestic() {
        return mDomestic;
    }

    public void setCityId(int cityId) {
        mCityId = cityId;
    }

    public int getCityId() {
        return mCityId;
    }

    public void setShopId(int shopId) {
        mShopId = shopId;
    }

    public int getShopId() {
        return mShopId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setAreaName(String areaName) {
        mAreaName = areaName;
    }

    public String getAreaName() {
        return mAreaName;
    }

    public void setEndDateTime(String endDateTime) {
        mEndDateTime = endDateTime;
    }

    public String getEndDateTime() {
        return mEndDateTime;
    }

    public void setOperationTime(String operationTime) {
        mOperationTime = operationTime;
    }

    public String getOperationTime() {
        return mOperationTime;
    }

    public void setCreditCard(int creditCard) {
        mCreditCard = creditCard;
    }

    public int getCreditCard() {
        return mCreditCard;
    }

    public void setTelPrepend(String telPrepend) {
        mTelPrepend = telPrepend;
    }

    public String getTelPrepend() {
        return mTelPrepend;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setNormalTime(String normalTime) {
        mNormalTime = normalTime;
    }

    public String getNormalTime() {
        return mNormalTime;
    }

    public void setGalleryLists(ArrayList<String> galleryLists) {
        mGalleryLists = galleryLists;
    }

    public ArrayList<String> getGalleryLists() {
        return mGalleryLists;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setAreaId(int areaId) {
        mAreaId = areaId;
    }

    public int getAreaId() {
        return mAreaId;
    }

    public void setIntroduction(String introduction) {
        mIntroduction = introduction;
    }

    public String getIntroduction() {
        return mIntroduction;
    }

    public void setDistance(String distance) {
        mDistance = distance;
    }

    public String getDistance() {
        return mDistance;
    }

    public void setType(int type) {
        mType = type;
    }

    public int getType() {
        return mType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mGallery);
        dest.writeString(this.mWeekendTime);
        dest.writeString(this.mStatus);
        dest.writeInt(this.mZipCode);
        dest.writeInt(this.mWifi);
        dest.writeString(this.mStartDateTime);
        dest.writeString(this.mLatitude);
        dest.writeString(this.mMobile);
        dest.writeInt(this.mParking);
        dest.writeString(this.mTel);
        dest.writeString(this.mImageUrl);
        dest.writeInt(this.mSequence);
        dest.writeString(this.mUuid);
        dest.writeString(this.mCityName);
        dest.writeString(this.mLongitude);
        dest.writeString(this.mGalleryCount);
        dest.writeInt(this.mDomestic);
        dest.writeInt(this.mCityId);
        dest.writeInt(this.mShopId);
        dest.writeInt(this.mId);
        dest.writeString(this.mAreaName);
        dest.writeString(this.mEndDateTime);
        dest.writeString(this.mOperationTime);
        dest.writeInt(this.mCreditCard);
        dest.writeString(this.mTelPrepend);
        dest.writeString(this.mAddress);
        dest.writeString(this.mNormalTime);
        dest.writeStringList(this.mGalleryLists);
        dest.writeString(this.mRemark);
        dest.writeString(this.mName);
        dest.writeInt(this.mAreaId);
        dest.writeString(this.mIntroduction);
        dest.writeString(this.mDistance);
        dest.writeValue(this.mIsAvailableLocationPickup);
        dest.writeInt(this.mType);
    }

    protected LocationListDataList(Parcel in) {
        this.mGallery = in.readString();
        this.mWeekendTime = in.readString();
        this.mStatus = in.readString();
        this.mZipCode = in.readInt();
        this.mWifi = in.readInt();
        this.mStartDateTime = in.readString();
        this.mLatitude = in.readString();
        this.mMobile = in.readString();
        this.mParking = in.readInt();
        this.mTel = in.readString();
        this.mImageUrl = in.readString();
        this.mSequence = in.readInt();
        this.mUuid = in.readString();
        this.mCityName = in.readString();
        this.mLongitude = in.readString();
        this.mGalleryCount = in.readString();
        this.mDomestic = in.readInt();
        this.mCityId = in.readInt();
        this.mShopId = in.readInt();
        this.mId = in.readInt();
        this.mAreaName = in.readString();
        this.mEndDateTime = in.readString();
        this.mOperationTime = in.readString();
        this.mCreditCard = in.readInt();
        this.mTelPrepend = in.readString();
        this.mAddress = in.readString();
        this.mNormalTime = in.readString();
        this.mGalleryLists = in.createStringArrayList();
        this.mRemark = in.readString();
        this.mName = in.readString();
        this.mAreaId = in.readInt();
        this.mIntroduction = in.readString();
        this.mDistance = in.readString();
        this.mIsAvailableLocationPickup = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mType = in.readInt();
    }

    public static final Creator<LocationListDataList> CREATOR = new Creator<LocationListDataList>() {
        @Override
        public LocationListDataList createFromParcel(Parcel source) {
            return new LocationListDataList(source);
        }

        @Override
        public LocationListDataList[] newArray(int size) {
            return new LocationListDataList[size];
        }
    };
}