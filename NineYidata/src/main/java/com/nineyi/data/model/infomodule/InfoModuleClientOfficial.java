package com.nineyi.data.model.infomodule;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by toby on 2015/3/11.
 */
public class InfoModuleClientOfficial implements Parcelable {

    public String InfoModuleId;

    public String InfoModuleType;

    public int Order;

    public InfoModuleClientOfficial() {
    }

    protected InfoModuleClientOfficial(Parcel in) {
        InfoModuleId = in.readString();
        InfoModuleType = in.readString();
        Order = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(InfoModuleId);
        dest.writeString(InfoModuleType);
        dest.writeInt(Order);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<InfoModuleClientOfficial> CREATOR = new Parcelable.Creator<InfoModuleClientOfficial>() {
        @Override
        public InfoModuleClientOfficial createFromParcel(Parcel in) {
            return new InfoModuleClientOfficial(in);
        }

        @Override
        public InfoModuleClientOfficial[] newArray(int size) {
            return new InfoModuleClientOfficial[size];
        }
    };
}
