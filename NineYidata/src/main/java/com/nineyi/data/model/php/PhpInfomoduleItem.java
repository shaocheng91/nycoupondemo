package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class PhpInfomoduleItem extends AbstractBaseModel implements Parcelable {

    public String introduction;
    public ArrayList<PhpInfomoduleItemElement> items;
    public String title;
    public String subtitle;
    public String cliplink;
    public String uuid;
    public PhpInfomoduleShopElement shop;
    public String published_date;
    public int theme;
    public ArrayList<String> gallery;

    public PhpInfomoduleItem() {
    }

    // Parcelable management
    private PhpInfomoduleItem(Parcel in) {
        introduction = in.readString();
        items = in.createTypedArrayList(PhpInfomoduleItemElement.CREATOR);
        title = in.readString();
        subtitle = in.readString();
        cliplink = in.readString();
        uuid = in.readString();
        shop = PhpInfomoduleShopElement.CREATOR.createFromParcel(in);
        published_date = in.readString();
        theme = in.readInt();
        gallery = in.createStringArrayList();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(introduction);
        dest.writeTypedList(items);
        dest.writeString(title);
        dest.writeString(subtitle);
        dest.writeString(cliplink);
        dest.writeString(uuid);
        shop.writeToParcel(dest, flags);
        dest.writeString(published_date);
        dest.writeInt(theme);
        dest.writeStringList(gallery);
    }

    public static final Parcelable.Creator<PhpInfomoduleItem> CREATOR = new Parcelable.Creator<PhpInfomoduleItem>() {
        public PhpInfomoduleItem createFromParcel(Parcel in) {
            return new PhpInfomoduleItem(in);
        }

        public PhpInfomoduleItem[] newArray(int size) {
            return new PhpInfomoduleItem[size];
        }
    };
}
