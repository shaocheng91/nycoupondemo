package com.nineyi.data.model.reward;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class RewardPointStoreList implements Parcelable{

    private static final String FIELD_LOCATION_ID = "LocationId";
    private static final String FIELD_LOCATION_NAME = "LocationName";


    @SerializedName(FIELD_LOCATION_ID)
    private int mLocationId;
    @SerializedName(FIELD_LOCATION_NAME)
    private String mLocationName;


    public RewardPointStoreList(){

    }

    public void setLocationId(int locationId) {
        mLocationId = locationId;
    }

    public int getLocationId() {
        return mLocationId;
    }

    public void setLocationName(String locationName) {
        mLocationName = locationName;
    }

    public String getLocationName() {
        return mLocationName;
    }

    public RewardPointStoreList(Parcel in) {
        mLocationId = in.readInt();
        mLocationName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RewardPointStoreList> CREATOR = new Creator<RewardPointStoreList>() {
        public RewardPointStoreList createFromParcel(Parcel in) {
            return new RewardPointStoreList(in);
        }

        public RewardPointStoreList[] newArray(int size) {
            return new RewardPointStoreList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mLocationId);
        dest.writeString(mLocationName);
    }


}