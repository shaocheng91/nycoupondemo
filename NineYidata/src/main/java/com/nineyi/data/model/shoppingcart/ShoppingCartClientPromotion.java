package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

import com.nineyi.data.model.promotion.PromotionTarget;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2015/11/4.
 */
public class ShoppingCartClientPromotion implements Parcelable {
    public String Id;
    public ArrayList<ShoppingCartCondition> Conditions;
    public ArrayList<PromotionTarget> Targets;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Id);
        dest.writeTypedList(Conditions);
        dest.writeTypedList(Targets);
    }

    public ShoppingCartClientPromotion() {
    }

    protected ShoppingCartClientPromotion(Parcel in) {
        this.Id = in.readString();
        this.Conditions = in.createTypedArrayList(ShoppingCartCondition.CREATOR);
        this.Targets = in.createTypedArrayList(PromotionTarget.CREATOR);
    }

    public static final Parcelable.Creator<ShoppingCartClientPromotion> CREATOR = new Parcelable.Creator<ShoppingCartClientPromotion>() {
        public ShoppingCartClientPromotion createFromParcel(Parcel source) {
            return new ShoppingCartClientPromotion(source);
        }

        public ShoppingCartClientPromotion[] newArray(int size) {
            return new ShoppingCartClientPromotion[size];
        }
    };
}