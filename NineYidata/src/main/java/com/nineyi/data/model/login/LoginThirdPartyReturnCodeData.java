package com.nineyi.data.model.login;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class LoginThirdPartyReturnCodeData implements Parcelable{

    private static final String FIELD_TOKEN = "authSessionToken";


    @SerializedName(FIELD_TOKEN)
    private String mToken;


    public LoginThirdPartyReturnCodeData(){

    }

    public void setToken(String token) {
        mToken = token;
    }

    public String getToken() {
        return mToken;
    }

    public LoginThirdPartyReturnCodeData(Parcel in) {
        mToken = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginThirdPartyReturnCodeData> CREATOR = new Creator<LoginThirdPartyReturnCodeData>() {
        public LoginThirdPartyReturnCodeData createFromParcel(Parcel in) {
            return new LoginThirdPartyReturnCodeData(in);
        }

        public LoginThirdPartyReturnCodeData[] newArray(int size) {
            return new LoginThirdPartyReturnCodeData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mToken);
    }


}