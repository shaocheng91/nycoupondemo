package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePageImage implements Parcelable {

    public int Index;
    public String Type;
    public int Id;
    public int Seq;
    public String Url;
    public String PicUrl;
    public String SKUPropertyNameSet;

    public SalePageImage() {
    }

    // Parcelable management
    private SalePageImage(Parcel in) {
        Index = in.readInt();
        Type = in.readString();
        Id = in.readInt();
        Seq = in.readInt();
        Url = in.readString();
        PicUrl = in.readString();
        SKUPropertyNameSet = in.readString();
    }

    public SalePageImage(String url) {
        PicUrl = url;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Index);
        dest.writeString(Type);
        dest.writeInt(Id);
        dest.writeInt(Seq);
        dest.writeString(Url);
        dest.writeString(PicUrl);
        dest.writeString(SKUPropertyNameSet);
    }

    public static final Parcelable.Creator<SalePageImage> CREATOR = new Parcelable.Creator<SalePageImage>() {
        public SalePageImage createFromParcel(Parcel in) {
            return new SalePageImage(in);
        }

        public SalePageImage[] newArray(int size) {
            return new SalePageImage[size];
        }
    };
}
