package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public final class PhpInfomoduleShopElement extends AbstractBaseModel implements Parcelable {

    public String theme_color;
    public String title;

    public PhpInfomoduleShopElement() {
    }

    // Parcelable management
    private PhpInfomoduleShopElement(Parcel in) {
        theme_color = in.readString();
        title = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(theme_color);
        dest.writeString(title);
    }

    public static final Parcelable.Creator<PhpInfomoduleShopElement> CREATOR
            = new Parcelable.Creator<PhpInfomoduleShopElement>() {
        public PhpInfomoduleShopElement createFromParcel(Parcel in) {
            return new PhpInfomoduleShopElement(in);
        }

        public PhpInfomoduleShopElement[] newArray(int size) {
            return new PhpInfomoduleShopElement[size];
        }
    };
}
