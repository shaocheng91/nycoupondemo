package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2015/11/4.
 */
public class ShoppingCartSignaturePlaintext implements Parcelable {
    public ArrayList<ShoppingCartClientPromotion> ShoppingCartClientPromotions;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ShoppingCartClientPromotions);
    }

    public ShoppingCartSignaturePlaintext() {
    }

    protected ShoppingCartSignaturePlaintext(Parcel in) {
        this.ShoppingCartClientPromotions = in.createTypedArrayList(ShoppingCartClientPromotion.CREATOR);
    }

    public static final Parcelable.Creator<ShoppingCartSignaturePlaintext> CREATOR = new Parcelable.Creator<ShoppingCartSignaturePlaintext>() {
        public ShoppingCartSignaturePlaintext createFromParcel(Parcel source) {
            return new ShoppingCartSignaturePlaintext(source);
        }

        public ShoppingCartSignaturePlaintext[] newArray(int size) {
            return new ShoppingCartSignaturePlaintext[size];
        }
    };
}
