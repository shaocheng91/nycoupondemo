package com.nineyi.data.model.installment;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class InstallmentShopInstallmentList implements Parcelable {

    private int Id;
    private int ShopId;
    private int InstallmentId;
    private int InstallmentDef;
    private boolean HasInterest;
    private double InstallmentRate;
    private int InstallmentAmountLimit;
    private ArrayList<String> BankList;
    private double EachInstallmentPrice;
    private String InterestDisplayName;
    private String DisplayName;

    public int getId() {
        return Id;
    }

    public int getShopId() {
        return ShopId;
    }

    public int getInstallmentId() {
        return InstallmentId;
    }

    public int getInstallmentDef() {
        return InstallmentDef;
    }

    public boolean isHasInterest() {
        return HasInterest;
    }

    public double getInstallmentRate() {
        return InstallmentRate;
    }

    public int getInstallmentAmountLimit() {
        return InstallmentAmountLimit;
    }

    public ArrayList<String> getBankList() {
        return BankList;
    }

    public double getEachInstallmentPrice() {
        return EachInstallmentPrice;
    }

    public String getInterestDisplayName() {
        return InterestDisplayName;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public InstallmentShopInstallmentList() {
    }

    // Parcelable management
    private InstallmentShopInstallmentList(Parcel in) {
        setId(in.readInt());
        setShopId(in.readInt());
        setInstallmentId(in.readInt());
        setInstallmentDef(in.readInt());
        setHasInterest(in.readInt() == 1);
        setInstallmentRate(in.readDouble());
        setInstallmentAmountLimit(in.readInt());
        setBankList(in.createStringArrayList());
        setEachInstallmentPrice(in.readDouble());
        setInterestDisplayName(in.readString());
        setDisplayName(in.readString());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getId());
        dest.writeInt(getShopId());
        dest.writeInt(getInstallmentId());
        dest.writeInt(getInstallmentDef());
        dest.writeInt(isHasInterest() ? 1 : 0);
        dest.writeDouble(getInstallmentRate());
        dest.writeInt(getInstallmentAmountLimit());
        dest.writeStringList(getBankList());
        dest.writeDouble(getEachInstallmentPrice());
        dest.writeString(getInterestDisplayName());
        dest.writeString(getDisplayName());
    }

    public static final Parcelable.Creator<InstallmentShopInstallmentList> CREATOR
            = new Parcelable.Creator<InstallmentShopInstallmentList>() {
        public InstallmentShopInstallmentList createFromParcel(Parcel in) {
            return new InstallmentShopInstallmentList(in);
        }

        public InstallmentShopInstallmentList[] newArray(int size) {
            return new InstallmentShopInstallmentList[size];
        }
    };

    public void setId(int id) {
        Id = id;
    }

    public void setShopId(int shopId) {
        ShopId = shopId;
    }

    public void setInstallmentId(int installmentId) {
        InstallmentId = installmentId;
    }

    public void setInstallmentDef(int installmentDef) {
        InstallmentDef = installmentDef;
    }

    public void setHasInterest(boolean hasInterest) {
        HasInterest = hasInterest;
    }

    public void setInstallmentRate(double installmentRate) {
        InstallmentRate = installmentRate;
    }

    public void setInstallmentAmountLimit(int installmentAmountLimit) {
        InstallmentAmountLimit = installmentAmountLimit;
    }

    public void setBankList(ArrayList<String> bankList) {
        BankList = bankList;
    }

    public void setEachInstallmentPrice(double eachInstallmentPrice) {
        EachInstallmentPrice = eachInstallmentPrice;
    }

    public void setInterestDisplayName(String interestDisplayName) {
        InterestDisplayName = interestDisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }
}
