package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


public class LocationListForPickUpData implements Parcelable {

    private static final String FIELD_LOCATION_GROUP_LIST = "LocationList";


    @SerializedName(FIELD_LOCATION_GROUP_LIST)
    private List<LocationList> mLocationGroupLists;


    public void setLocationLists(List<LocationList> locationGroupLists) {
        mLocationGroupLists = locationGroupLists;
    }

    public List<LocationList> getLocationLists() {
        return mLocationGroupLists;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.mLocationGroupLists);
    }

    protected LocationListForPickUpData(Parcel in) {
        this.mLocationGroupLists = in.createTypedArrayList(LocationList.CREATOR);
    }

    public static final Creator<LocationListForPickUpData> CREATOR = new Creator<LocationListForPickUpData>() {
        @Override
        public LocationListForPickUpData createFromParcel(Parcel source) {
            return new LocationListForPickUpData(source);
        }

        @Override
        public LocationListForPickUpData[] newArray(int size) {
            return new LocationListForPickUpData[size];
        }
    };
}