
package com.nineyi.data.model.promotion.discount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class PromotionDiscount implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private PromotionDiscountData data;

    /**
     * 
     * @return
     *     The returnCode
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * 
     * @param returnCode
     *     The ReturnCode
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The data
     */
    public PromotionDiscountData getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The Data
     */
    public void setData(PromotionDiscountData data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeString(this.message);
        dest.writeParcelable(this.data, flags);
    }

    public PromotionDiscount() {
    }

    protected PromotionDiscount(Parcel in) {
        this.returnCode = in.readString();
        this.message = in.readString();
        this.data = in.readParcelable(PromotionDiscountData.class.getClassLoader());
    }

    public static final Parcelable.Creator<PromotionDiscount> CREATOR = new Parcelable.Creator<PromotionDiscount>() {
        @Override
        public PromotionDiscount createFromParcel(Parcel source) {
            return new PromotionDiscount(source);
        }

        @Override
        public PromotionDiscount[] newArray(int size) {
            return new PromotionDiscount[size];
        }
    };
}
