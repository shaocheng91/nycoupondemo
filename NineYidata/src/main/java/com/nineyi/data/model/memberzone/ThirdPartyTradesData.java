package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ThirdPartyTradesData implements Parcelable {

    private static final String FIELD_HAS_THIRDPARTY_TRADE = "HasThirdPartyQueryOrder";
    private static final String FIELD_THIRDPARTY_QUERYORDER_DISPLAYNAME = "ThirdPartyQueryOrderDisplayName";
    @SerializedName(FIELD_HAS_THIRDPARTY_TRADE)
    private boolean mHasThirdPartyTrade;


    @SerializedName(FIELD_THIRDPARTY_QUERYORDER_DISPLAYNAME)
    private String mThirdPartyQueryOrderDisplayName;


    public ThirdPartyTradesData() {

    }

    public String getThirdPartyQueryOrderDisplayName() {
        return mThirdPartyQueryOrderDisplayName;
    }

    public void setThirdPartyTrade(boolean hasThirdPartyTrade) {
        mHasThirdPartyTrade = hasThirdPartyTrade;
    }

    public boolean isThirdPartyTrade() {
        return mHasThirdPartyTrade;
    }


    public ThirdPartyTradesData(Parcel in) {
        mHasThirdPartyTrade = in.readInt() == 1;
        mThirdPartyQueryOrderDisplayName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ThirdPartyTradesData> CREATOR = new Creator<ThirdPartyTradesData>() {
        public ThirdPartyTradesData createFromParcel(Parcel in) {
            return new ThirdPartyTradesData(in);
        }

        public ThirdPartyTradesData[] newArray(int size) {
            return new ThirdPartyTradesData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mHasThirdPartyTrade ? 1 : 0);
        dest.writeString(mThirdPartyQueryOrderDisplayName);
    }


}