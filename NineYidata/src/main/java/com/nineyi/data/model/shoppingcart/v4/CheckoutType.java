
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class CheckoutType implements Parcelable {
    public static final String DISPLAY_PAY_TYPE_LIST = "DisplayPayTypeList";
    public static final String DISPLAY_SHIPPING_TYPE_LIST = "DisplayShippingTypeList";

    @SerializedName(DISPLAY_PAY_TYPE_LIST)
    @Expose
    private List<DisplayPayTypeList> mDisplayPayTypeList = new ArrayList<DisplayPayTypeList>();
    @SerializedName(DISPLAY_SHIPPING_TYPE_LIST)
    @Expose
    private List<DisplayShippingTypeList> mDisplayShippingTypeList = new ArrayList<DisplayShippingTypeList>();
    @SerializedName("ShippingMaxPriceLimit")
    @Expose
    private Double mShippingMaxPriceLimit;
    @SerializedName("IsTotalPriceOverShippingLimit")
    @Expose
    private Boolean mIsTotalPriceOverShippingLimit;

    @SerializedName("MaxOverLimitPayTypePrice")
    @Expose
    private Double mMaxOverLimitPayTypePrice;

    @SerializedName("IsTotalPriceOverPayTypeLimit")
    @Expose
    private Boolean mIsTotalPriceOverPayTypeLimit;

    @SerializedName("DisplayMessage")
    @Expose
    private String mDisplayMessage;

    public String getDisplayMessage() {
        return mDisplayMessage;
    }

    public Double getMaxOverLimitPayTypePrice() {
        return mMaxOverLimitPayTypePrice;
    }

    public void setMaxOverLimitPayTypePrice(Double mMaxOverLimitPayTypePrice) {
        this.mMaxOverLimitPayTypePrice = mMaxOverLimitPayTypePrice;
    }

    public Boolean getIsTotalPriceOverPayTypeLimit() {
        return mIsTotalPriceOverPayTypeLimit;
    }

    public void setIsTotalPriceOverPayTypeLimit(Boolean mIsTotalPriceOverPayTypeLimit) {
        this.mIsTotalPriceOverPayTypeLimit = mIsTotalPriceOverPayTypeLimit;
    }

    /**
     * 
     * @return
     *     The DisplayPayTypeList
     */
    public List<DisplayPayTypeList> getDisplayPayTypeList() {
        return mDisplayPayTypeList;
    }

    /**
     * 
     * @param DisplayPayTypeList
     *     The DisplayPayTypeList
     */
    public void setDisplayPayTypeList(List<DisplayPayTypeList> DisplayPayTypeList) {
        this.mDisplayPayTypeList = DisplayPayTypeList;
    }

    /**
     * 
     * @return
     *     The DisplayShippingTypeList
     */
    public List<DisplayShippingTypeList> getDisplayShippingTypeList() {
        return mDisplayShippingTypeList;
    }

    /**
     * 
     * @param DisplayShippingTypeList
     *     The DisplayShippingTypeList
     */
    public void setDisplayShippingTypeList(List<DisplayShippingTypeList> DisplayShippingTypeList) {
        this.mDisplayShippingTypeList = DisplayShippingTypeList;
    }

    /**
     *
     * @return
     *     The shippingMaxPriceLimit
     */
    public Double getShippingMaxPriceLimit() {
        return mShippingMaxPriceLimit;
    }

    /**
     *
     * @param shippingMaxPriceLimit
     *     The ShippingMaxPriceLimit
     */
    public void setShippingMaxPriceLimit(Double shippingMaxPriceLimit) {
        this.mShippingMaxPriceLimit = shippingMaxPriceLimit;
    }

    /**
     *
     * @return
     *     The isTotalPriceOverShippingLimit
     */
    public Boolean getIsTotalPriceOverShippingLimit() {
        return mIsTotalPriceOverShippingLimit;
    }

    /**
     *
     * @param isTotalPriceOverShippingLimit
     *     The IsTotalPriceOverShippingLimit
     */
    public void setIsTotalPriceOverShippingLimit(Boolean isTotalPriceOverShippingLimit) {
        this.mIsTotalPriceOverShippingLimit = isTotalPriceOverShippingLimit;
    }

    public CheckoutType() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.mDisplayPayTypeList);
        dest.writeTypedList(this.mDisplayShippingTypeList);
        dest.writeValue(this.mShippingMaxPriceLimit);
        dest.writeValue(this.mIsTotalPriceOverShippingLimit);
        dest.writeValue(this.mMaxOverLimitPayTypePrice);
        dest.writeValue(this.mIsTotalPriceOverPayTypeLimit);
        dest.writeString(this.mDisplayMessage);
    }

    protected CheckoutType(Parcel in) {
        this.mDisplayPayTypeList = in.createTypedArrayList(DisplayPayTypeList.CREATOR);
        this.mDisplayShippingTypeList = in.createTypedArrayList(DisplayShippingTypeList.CREATOR);
        this.mShippingMaxPriceLimit = (Double) in.readValue(Double.class.getClassLoader());
        this.mIsTotalPriceOverShippingLimit = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mMaxOverLimitPayTypePrice = (Double) in.readValue(Double.class.getClassLoader());
        this.mIsTotalPriceOverPayTypeLimit = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mDisplayMessage = in.readString();
    }

    public static final Creator<CheckoutType> CREATOR = new Creator<CheckoutType>() {
        @Override
        public CheckoutType createFromParcel(Parcel source) {
            return new CheckoutType(source);
        }

        @Override
        public CheckoutType[] newArray(int size) {
            return new CheckoutType[size];
        }
    };
}
