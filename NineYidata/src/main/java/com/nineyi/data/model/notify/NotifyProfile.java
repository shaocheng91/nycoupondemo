package com.nineyi.data.model.notify;

import android.os.Parcel;
import android.os.Parcelable;


public final class NotifyProfile implements Parcelable {

    public String GUID;
    public String type;
    public boolean switchValue;

    public NotifyProfile() {
    }

    // Parcelable management
    private NotifyProfile(Parcel in) {
        GUID = in.readString();
        type = in.readString();
        switchValue = in.readInt() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(GUID);
        dest.writeString(type);
        dest.writeInt(switchValue ? 1 : 0);
    }

    public static final Parcelable.Creator<NotifyProfile> CREATOR = new Parcelable.Creator<NotifyProfile>() {
        public NotifyProfile createFromParcel(Parcel in) {
            return new NotifyProfile(in);
        }

        public NotifyProfile[] newArray(int size) {
            return new NotifyProfile[size];
        }
    };
}
