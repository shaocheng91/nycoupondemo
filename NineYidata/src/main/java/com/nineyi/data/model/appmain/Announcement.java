package com.nineyi.data.model.appmain;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


public class Announcement implements Parcelable {

    private List<AnnouncementAction> Actions = new ArrayList<AnnouncementAction>();

    public List<AnnouncementAction> getActions() {
        return Actions;
    }

    public void setActions(List<AnnouncementAction> actions) {
        this.Actions = actions;
    }

    public Announcement() {

    }

    protected Announcement(Parcel in) {
        if (in.readByte() == 0x01) {
            Actions = new ArrayList<AnnouncementAction>();
            in.readList(Actions, AnnouncementAction.class.getClassLoader());
        } else {
            Actions = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (Actions == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(Actions);
        }
    }

    public static final Parcelable.Creator<Announcement> CREATOR = new Parcelable.Creator<Announcement>() {
        @Override
        public Announcement createFromParcel(Parcel in) {
            return new Announcement(in);
        }

        @Override
        public Announcement[] newArray(int size) {
            return new Announcement[size];
        }
    };
}