package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2017/4/10.
 */
public class ShippingStatusData implements Parcelable {
    public int WaitingToShippingCount;
    public int ShippingFinishCount;
    public int ShippingArrivedCount;
    public boolean HasPartialShippingFinish;

    public ShippingStatusData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.WaitingToShippingCount);
        dest.writeInt(this.ShippingFinishCount);
        dest.writeInt(this.ShippingArrivedCount);
        dest.writeByte(this.HasPartialShippingFinish ? (byte) 1 : (byte) 0);
    }

    protected ShippingStatusData(Parcel in) {
        this.WaitingToShippingCount = in.readInt();
        this.ShippingFinishCount = in.readInt();
        this.ShippingArrivedCount = in.readInt();
        this.HasPartialShippingFinish = in.readByte() != 0;
    }

    public static final Creator<ShippingStatusData> CREATOR = new Creator<ShippingStatusData>() {
        @Override
        public ShippingStatusData createFromParcel(Parcel source) {
            return new ShippingStatusData(source);
        }

        @Override
        public ShippingStatusData[] newArray(int size) {
            return new ShippingStatusData[size];
        }
    };
}
