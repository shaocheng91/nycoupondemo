package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public abstract class AbstractBaseModel implements Parcelable {

    public int id;

    public int getId() {
        return id;
    }

    public int publish_id;
    public String type;
    public String status;
    public String start_date;
    public String end_date;
    public PhpContentElement content;

    public AbstractBaseModel() {
    }

    // Parcelable management
    protected AbstractBaseModel(Parcel in) {
        baseParcelIn(in);
    }

    public void baseParcelIn(Parcel in) {
        id = in.readInt();
        publish_id = in.readInt();
        type = in.readString();
        status = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        content = in.readParcelable(PhpContentElement.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(publish_id);
        dest.writeString(type);
        dest.writeString(status);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeParcelable(content, flags);
    }
}
