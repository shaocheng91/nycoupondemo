package com.nineyi.data.model.notify;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class NotifyProfileInputData implements Parcelable {

    public ArrayList<NotifyProfile> appPushProfileDataEntites;

    public NotifyProfileInputData() {
    }

    // Parcelable management
    private NotifyProfileInputData(Parcel in) {
        appPushProfileDataEntites = in.createTypedArrayList(NotifyProfile.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(appPushProfileDataEntites);
    }

    public static final Parcelable.Creator<NotifyProfileInputData> CREATOR
            = new Parcelable.Creator<NotifyProfileInputData>() {
        public NotifyProfileInputData createFromParcel(Parcel in) {
            return new NotifyProfileInputData(in);
        }

        public NotifyProfileInputData[] newArray(int size) {
            return new NotifyProfileInputData[size];
        }
    };
}
