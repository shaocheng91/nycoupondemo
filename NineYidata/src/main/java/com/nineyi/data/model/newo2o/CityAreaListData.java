package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import java.util.ArrayList;


public class CityAreaListData implements Parcelable{

    private static final String FIELD_LIST = "List";
    private static final String FIELD_OVERSEA_LOCATION_EXISTS = "OverseaLocationExists";


    @SerializedName(FIELD_LIST)
    private ArrayList<CityAreaListDataList> mLists;
    @SerializedName(FIELD_OVERSEA_LOCATION_EXISTS)
    private boolean mOverseaLocationExist;


    public CityAreaListData(){

    }

    public void setLists(ArrayList<CityAreaListDataList> lists) {
        mLists = lists;
    }

    public ArrayList<CityAreaListDataList> getLists() {
        return mLists;
    }

    public void setOverseaLocationExist(boolean overseaLocationExist) {
        mOverseaLocationExist = overseaLocationExist;
    }

    public boolean isOverseaLocationExist() {
        return mOverseaLocationExist;
    }

    public CityAreaListData(Parcel in) {
        new ArrayList<CityAreaListDataList>();
        in.readTypedList(mLists, CityAreaListDataList.CREATOR);
        mOverseaLocationExist = in.readInt() == 1 ? true: false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CityAreaListData> CREATOR = new Creator<CityAreaListData>() {
        public CityAreaListData createFromParcel(Parcel in) {
            return new CityAreaListData(in);
        }

        public CityAreaListData[] newArray(int size) {
            return new CityAreaListData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mLists);
        dest.writeInt(mOverseaLocationExist ? 1 : 0);
    }


}