package com.nineyi.data.model.login;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ThirdPartyAuthInfoData implements Parcelable {

    private static final String FIELD_HAS_THIRDPARTY_AUTHINFO = "EnableThirdpartyAuthMember";
    private static final String FIELD_THIRDPARTY_AUTH_TYPEDEF = "ThirdpartyAuthTypeDef";
    private static final String FIELD_THIRDPARTY_AUTH_BTN_CONTENT = "ThirdpartyAuthButtonContent";
    private static final String FIELD_THIRDPARTY_AUTH_URL = "ThirdPartyOAuthUrl";

    @SerializedName(FIELD_HAS_THIRDPARTY_AUTHINFO)
    private boolean mHasThirdPartyAuthMember;
    @SerializedName(FIELD_THIRDPARTY_AUTH_TYPEDEF)
    private String mThirdPartyAuthTypeDef;
    @SerializedName(FIELD_THIRDPARTY_AUTH_BTN_CONTENT)
    private String mThirdPartyAuthBtnContent;
    @SerializedName(FIELD_THIRDPARTY_AUTH_URL)
    private String mThirdPartyAuthUrl;

    public ThirdPartyAuthInfoData() {

    }

    public void setThirdPartyAuthNumber(boolean hasThirdPartyAuthMember) {
        mHasThirdPartyAuthMember = hasThirdPartyAuthMember;
    }

    public boolean isThirdPartyAuthNumber() {
        return mHasThirdPartyAuthMember;
    }

    public void setThirdPartyAuthTypeDef(String thirdPartyAuthTypeDef) {
        mThirdPartyAuthTypeDef = thirdPartyAuthTypeDef;
    }

    public String getThirdPartyAuthTypeDef() {
        return mThirdPartyAuthTypeDef;
    }

    public void setThirdPartyBtnContent(String thirdPartyBtnContent) {
        mThirdPartyAuthBtnContent = thirdPartyBtnContent;
    }

    public String getThirdPartyBtnContent() {
        return mThirdPartyAuthBtnContent;
    }

    public void setThirdPartyAuthUrl(String thirdPartyAuthUrl) {
        mThirdPartyAuthUrl = thirdPartyAuthUrl;
    }

    public String getThirdPartyAuthUrl() {
        return mThirdPartyAuthUrl;
    }

    public ThirdPartyAuthInfoData(Parcel in) {

        mHasThirdPartyAuthMember = in.readInt() == 1;
        mThirdPartyAuthTypeDef = in.readString();
        mThirdPartyAuthBtnContent = in.readString();
        mThirdPartyAuthUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ThirdPartyAuthInfoData> CREATOR = new Creator<ThirdPartyAuthInfoData>() {
        public ThirdPartyAuthInfoData createFromParcel(Parcel in) {
            return new ThirdPartyAuthInfoData(in);
        }

        public ThirdPartyAuthInfoData[] newArray(int size) {
            return new ThirdPartyAuthInfoData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mHasThirdPartyAuthMember ? 1 : 0);
        dest.writeString(mThirdPartyAuthTypeDef);
        dest.writeString(mThirdPartyAuthBtnContent);
        dest.writeString(mThirdPartyAuthUrl);
    }


}