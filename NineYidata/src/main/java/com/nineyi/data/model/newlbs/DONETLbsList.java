package com.nineyi.data.model.newlbs;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class DONETLbsList implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private LBSDatum mDatum;


    public DONETLbsList(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(LBSDatum datum) {
        mDatum = datum;
    }

    public LBSDatum getDatum() {
        return mDatum;
    }

    public DONETLbsList(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mDatum = in.readParcelable(LBSDatum.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DONETLbsList> CREATOR = new Creator<DONETLbsList>() {
        public DONETLbsList createFromParcel(Parcel in) {
            return new DONETLbsList(in);
        }

        public DONETLbsList[] newArray(int size) {
            return new DONETLbsList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        dest.writeParcelable(mDatum, flags);
    }


}