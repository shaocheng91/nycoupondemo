package com.nineyi.data.model.newlbs;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class LbsList implements Parcelable{

    private static final String FIELD_IMAGE_URL = "ImageUrl";
    private static final String FIELD_SEQUENCE = "Sequence";
    private static final String FIELD_UUID = "Uuid";
    private static final String FIELD_COUPON_ID = "CouponId";
    private static final String FIELD_PUSH_STATUS = "PushStatus";
    private static final String FIELD_STATUS = "Status";
    private static final String FIELD_SCHEDULE_OF_WEEK = "ScheduleOfWeek";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_SHOP_ID = "ShopId";
    private static final String FIELD_START_DATE_TIME = "StartDateTime";
    private static final String FIELD_END_DATE_TIME = "EndDateTime";
    private static final String FIELD_TYPE = "Type";
    private static final String FIELD_SCHEDULE_OF_DELAY = "ScheduleOfDelay";
    private static final String FIELD_NOTIFICATION = "Notification";
    private static final String FIELD_SCHEDULE_OF_START_TIME = "ScheduleOfStartTime";
    private static final String FIELD_TITLE = "Title";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_SCHEDULE_OF_END_TIME = "ScheduleOfEndTime";
    private static final String FIELD_INTRODUCTION = "Introduction";


    @SerializedName(FIELD_IMAGE_URL)
    private String mImageUrl;
    @SerializedName(FIELD_SEQUENCE)
    private int mSequence;
    @SerializedName(FIELD_UUID)
    private String mUuid;
    @SerializedName(FIELD_COUPON_ID)
    private int mCouponId;
    @SerializedName(FIELD_PUSH_STATUS)
    private String mPushStatus;
    @SerializedName(FIELD_STATUS)
    private String mStatus;
    @SerializedName(FIELD_SCHEDULE_OF_WEEK)
    private String mScheduleOfWeek;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_SHOP_ID)
    private int mShopId;
    @SerializedName(FIELD_START_DATE_TIME)
    private String mStartDateTime;
    @SerializedName(FIELD_END_DATE_TIME)
    private String mEndDateTime;
    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FIELD_SCHEDULE_OF_DELAY)
    private int mScheduleOfDelay;
    @SerializedName(FIELD_NOTIFICATION)
    private String mNotification;
    @SerializedName(FIELD_SCHEDULE_OF_START_TIME)
    private String mScheduleOfStartTime;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_SCHEDULE_OF_END_TIME)
    private String mScheduleOfEndTime;
    @SerializedName(FIELD_INTRODUCTION)
    private String mIntroduction;


    public LbsList(){

    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setSequence(int sequence) {
        mSequence = sequence;
    }

    public int getSequence() {
        return mSequence;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setCouponId(int couponId) {
        mCouponId = couponId;
    }

    public int getCouponId() {
        return mCouponId;
    }

    public void setPushStatus(String pushStatus) {
        mPushStatus = pushStatus;
    }

    public String getPushStatus() {
        return mPushStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setScheduleOfWeek(String scheduleOfWeek) {
        mScheduleOfWeek = scheduleOfWeek;
    }

    public String getScheduleOfWeek() {
        return mScheduleOfWeek;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setShopId(int shopId) {
        mShopId = shopId;
    }

    public int getShopId() {
        return mShopId;
    }

    public void setStartDateTime(String startDateTime) {
        mStartDateTime = startDateTime;
    }

    public String getStartDateTime() {
        return mStartDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        mEndDateTime = endDateTime;
    }

    public String getEndDateTime() {
        return mEndDateTime;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }

    public void setScheduleOfDelay(int scheduleOfDelay) {
        mScheduleOfDelay = scheduleOfDelay;
    }

    public int getScheduleOfDelay() {
        return mScheduleOfDelay;
    }

    public void setNotification(String notification) {
        mNotification = notification;
    }

    public String getNotification() {
        return mNotification;
    }

    public void setScheduleOfStartTime(String scheduleOfStartTime) {
        mScheduleOfStartTime = scheduleOfStartTime;
    }

    public String getScheduleOfStartTime() {
        return mScheduleOfStartTime;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setScheduleOfEndTime(String scheduleOfEndTime) {
        mScheduleOfEndTime = scheduleOfEndTime;
    }

    public String getScheduleOfEndTime() {
        return mScheduleOfEndTime;
    }

    public void setIntroduction(String introduction) {
        mIntroduction = introduction;
    }

    public String getIntroduction() {
        return mIntroduction;
    }

    public LbsList(Parcel in) {
        mImageUrl = in.readString();
        mSequence = in.readInt();
        mUuid = in.readString();
        mCouponId = in.readInt();
        mPushStatus = in.readString();
        mStatus = in.readString();
        mScheduleOfWeek = in.readString();
        mId = in.readInt();
        mShopId = in.readInt();
        mStartDateTime = in.readString();
        mEndDateTime = in.readString();
        mType = in.readString();
        mScheduleOfDelay = in.readInt();
        mNotification = in.readString();
        mScheduleOfStartTime = in.readString();
        mTitle = in.readString();
        mName = in.readString();
        mScheduleOfEndTime = in.readString();
        mIntroduction = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LbsList> CREATOR = new Creator<LbsList>() {
        public LbsList createFromParcel(Parcel in) {
            return new LbsList(in);
        }

        public LbsList[] newArray(int size) {
            return new LbsList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mImageUrl);
        dest.writeInt(mSequence);
        dest.writeString(mUuid);
        dest.writeInt(mCouponId);
        dest.writeString(mPushStatus);
        dest.writeString(mStatus);
        dest.writeString(mScheduleOfWeek);
        dest.writeInt(mId);
        dest.writeInt(mShopId);
        dest.writeString(mStartDateTime);
        dest.writeString(mEndDateTime);
        dest.writeString(mType);
        dest.writeInt(mScheduleOfDelay);
        dest.writeString(mNotification);
        dest.writeString(mScheduleOfStartTime);
        dest.writeString(mTitle);
        dest.writeString(mName);
        dest.writeString(mScheduleOfEndTime);
        dest.writeString(mIntroduction);
    }


}