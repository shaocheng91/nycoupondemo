package com.nineyi.data.model.login;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by tedliang on 2016/4/27.
 */
public class VipMemberBenefitsModel implements Parcelable {

    public String ReturnCode;
    public String Data;
    public String Message;

    public VipMemberBenefitsModel(Parcel in) {
        ReturnCode=in.readString();
        Data=in.readString();
        Message=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        dest.writeString(Data);
        dest.writeString(Message);
    }

    public static Parcelable.Creator<VipMemberBenefitsModel> CREATOR=new Parcelable.Creator<VipMemberBenefitsModel>(){
        @Override
        public VipMemberBenefitsModel createFromParcel(Parcel source) {
            return new VipMemberBenefitsModel(source);
        }

        @Override
        public VipMemberBenefitsModel[] newArray(int size) {
            return new VipMemberBenefitsModel[size];
        }
    };
}
