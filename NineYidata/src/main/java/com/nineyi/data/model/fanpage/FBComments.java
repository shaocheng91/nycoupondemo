package com.nineyi.data.model.fanpage;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


public class FBComments implements Parcelable {

    List<FBCommnetUser> data;

    protected FBComments(Parcel in) {
        if (in.readByte() == 0x01) {
            data = new ArrayList<FBCommnetUser>();
            in.readList(data, FBCommnetUser.class.getClassLoader());
        } else {
            data = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
    }

    public static final Parcelable.Creator<FBComments> CREATOR = new Parcelable.Creator<FBComments>() {
        @Override
        public FBComments createFromParcel(Parcel in) {
            return new FBComments(in);
        }

        @Override
        public FBComments[] newArray(int size) {
            return new FBComments[size];
        }
    };
}