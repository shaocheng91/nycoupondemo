package com.nineyi.data.model.installment;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class InstallmentShopServiceInfoEntity implements Parcelable {

    public ArrayList<InstallmentShopPayTypeList> ShopPayTypeList;
    public ArrayList<InstallmentShopInstallmentList> ShopInstallmentList;
    public ArrayList<InstallmentShopShippingTypeList> ShopShippingTypeList;

    public InstallmentShopServiceInfoEntity() {
    }

    // Parcelable management
    private InstallmentShopServiceInfoEntity(Parcel in) {
        ShopPayTypeList = in.createTypedArrayList(InstallmentShopPayTypeList.CREATOR);
        ShopInstallmentList = in.createTypedArrayList(InstallmentShopInstallmentList.CREATOR);
        ShopShippingTypeList = in.createTypedArrayList(InstallmentShopShippingTypeList.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ShopPayTypeList);
        //    dest.writeTypedList(ShopInstallmentList);
        //    dest.writeTypedList(ShopShippingTypeList);
    }

    public static final Parcelable.Creator<InstallmentShopServiceInfoEntity> CREATOR
            = new Parcelable.Creator<InstallmentShopServiceInfoEntity>() {
        public InstallmentShopServiceInfoEntity createFromParcel(Parcel in) {
            return new InstallmentShopServiceInfoEntity(in);
        }

        public InstallmentShopServiceInfoEntity[] newArray(int size) {
            return new InstallmentShopServiceInfoEntity[size];
        }
    };
}
