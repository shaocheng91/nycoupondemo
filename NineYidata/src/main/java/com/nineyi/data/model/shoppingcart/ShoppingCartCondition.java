package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShoppingCartCondition implements Parcelable {

    public int Id;
    public String TypeDef;
    public String TypeDefDesc;
    public int TotalQty;
    public double TotalPrice;
    public String DiscountTypeDef;
    public String DiscountTypeDefDesc;
    public double DiscountPrice;
    public float DiscountRate;
    public ArrayList<ShoppingCartProductSKUs> FreeGifts;

    public ShoppingCartCondition() {
    }

    // Parcelable management
    private ShoppingCartCondition(Parcel in) {
        Id = in.readInt();
        TypeDef = in.readString();
        TypeDefDesc = in.readString();
        TotalQty = in.readInt();
        TotalPrice = in.readDouble();
        DiscountTypeDef = in.readString();
        DiscountTypeDefDesc = in.readString();
        DiscountPrice = in.readDouble();
        DiscountRate = in.readFloat();
        FreeGifts = in.createTypedArrayList(ShoppingCartProductSKUs.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(TypeDef);
        dest.writeString(TypeDefDesc);
        dest.writeInt(TotalQty);
        dest.writeDouble(TotalPrice);
        dest.writeString(DiscountTypeDef);
        dest.writeString(DiscountTypeDefDesc);
        dest.writeDouble(DiscountPrice);
        dest.writeFloat(DiscountRate);
        dest.writeTypedList(FreeGifts);
    }

    public static final Parcelable.Creator<ShoppingCartCondition> CREATOR
            = new Parcelable.Creator<ShoppingCartCondition>() {
        public ShoppingCartCondition createFromParcel(Parcel in) {
            return new ShoppingCartCondition(in);
        }

        public ShoppingCartCondition[] newArray(int size) {
            return new ShoppingCartCondition[size];
        }
    };
}
