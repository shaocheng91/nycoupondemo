/**
 * for 'ShoppingCartV3/UpdateShoppingCartQty' web api
 */
package com.nineyi.data.model.apirequest;

public class ShoppingCartUpdateQtyValue {

    public int SalePageId;

    public int ShopId;

    public int SaleProductSKUId;

    public int SalePageGroupSeq;

    public int Qty;
}
