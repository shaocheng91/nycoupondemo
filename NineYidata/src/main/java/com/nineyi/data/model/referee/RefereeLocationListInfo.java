package com.nineyi.data.model.referee;

import android.os.Parcel;
import android.os.Parcelable;


public final class RefereeLocationListInfo implements Parcelable {

    public int Id;
    public String Name;
    public String Longitude;
    public String Latitude;

    public RefereeLocationListInfo(String mLocation_Name) {
        this.Name = mLocation_Name;
    }

    // Parcelable management
    private RefereeLocationListInfo(Parcel in) {
        Id = in.readInt();
        Name = in.readString();
        Longitude = in.readString();
        Latitude = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Name);
        dest.writeString(Longitude);
        dest.writeString(Latitude);
    }

    public static final Creator<RefereeLocationListInfo> CREATOR = new Creator<RefereeLocationListInfo>() {
        public RefereeLocationListInfo createFromParcel(Parcel in) {
            return new RefereeLocationListInfo(in);
        }

        public RefereeLocationListInfo[] newArray(int size) {
            return new RefereeLocationListInfo[size];
        }
    };
}
