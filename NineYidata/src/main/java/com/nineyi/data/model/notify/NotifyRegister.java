package com.nineyi.data.model.notify;

import android.os.Parcel;
import android.os.Parcelable;


public final class NotifyRegister implements Parcelable {

    public String UDID;
    public int ShopID;
    public String platformID;
    public String token;

    public NotifyRegister() {
    }

    // Parcelable management
    private NotifyRegister(Parcel in) {
        UDID = in.readString();
        ShopID = in.readInt();
        platformID = in.readString();
        token = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UDID);
        dest.writeInt(ShopID);
        dest.writeString(platformID);
        dest.writeString(token);
    }

    public static final Parcelable.Creator<NotifyRegister> CREATOR = new Parcelable.Creator<NotifyRegister>() {
        public NotifyRegister createFromParcel(Parcel in) {
            return new NotifyRegister(in);
        }

        public NotifyRegister[] newArray(int size) {
            return new NotifyRegister[size];
        }
    };
}
