package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class MatchedCondition implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("TypeDef")
    @Expose
    private String typeDef;
    @SerializedName("TotalQty")
    @Expose
    private Integer totalQty;
    @SerializedName("TotalPrice")
    @Expose
    private Integer totalPrice;
    @SerializedName("DiscountTypeDef")
    @Expose
    private String discountTypeDef;
    @SerializedName("DiscountPrice")
    @Expose
    private Integer discountPrice;
    @SerializedName("DiscountRate")
    @Expose
    private Double discountRate;
    @SerializedName("ReachQtyCount")
    @Expose
    private Integer reachQtyCount;
    @SerializedName("TotalPayment")
    @Expose
    private Integer totalPayment;
    @SerializedName("IsMatched")
    @Expose
    private Boolean isMatched;
    @SerializedName("SalePageTotalQty")
    @Expose
    private Integer salePageTotalQty;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The typeDef
     */
    public String getTypeDef() {
        return typeDef;
    }

    /**
     * @param typeDef The TypeDef
     */
    public void setTypeDef(String typeDef) {
        this.typeDef = typeDef;
    }

    /**
     * @return The totalQty
     */
    public Integer getTotalQty() {
        return totalQty;
    }

    /**
     * @param totalQty The TotalQty
     */
    public void setTotalQty(Integer totalQty) {
        this.totalQty = totalQty;
    }

    /**
     * @return The totalPrice
     */
    public Integer getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice The TotalPrice
     */
    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return The discountTypeDef
     */
    public String getDiscountTypeDef() {
        return discountTypeDef;
    }

    /**
     * @param discountTypeDef The DiscountTypeDef
     */
    public void setDiscountTypeDef(String discountTypeDef) {
        this.discountTypeDef = discountTypeDef;
    }

    /**
     * @return The discountPrice
     */
    public Integer getDiscountPrice() {
        return discountPrice;
    }

    /**
     * @param discountPrice The DiscountPrice
     */
    public void setDiscountPrice(Integer discountPrice) {
        this.discountPrice = discountPrice;
    }

    /**
     * @return The discountRate
     */
    public Double getDiscountRate() {
        return discountRate;
    }

    /**
     * @param discountRate The DiscountRate
     */
    public void setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
    }

    /**
     * @return The reachQtyCount
     */
    public Integer getReachQtyCount() {
        return reachQtyCount;
    }

    /**
     * @param reachQtyCount The ReachQtyCount
     */
    public void setReachQtyCount(Integer reachQtyCount) {
        this.reachQtyCount = reachQtyCount;
    }

    /**
     * @return The totalPayment
     */
    public Integer getTotalPayment() {
        return totalPayment;
    }

    /**
     * @param totalPayment The TotalPayment
     */
    public void setTotalPayment(Integer totalPayment) {
        this.totalPayment = totalPayment;
    }

    /**
     * @return The isMatched
     */
    public Boolean getIsMatched() {
        return isMatched;
    }

    /**
     * @param isMatched The IsMatched
     */
    public void setIsMatched(Boolean isMatched) {
        this.isMatched = isMatched;
    }

    /**
     * @return The salePageTotalQty
     */
    public Integer getSalePageTotalQty() {
        return salePageTotalQty;
    }

    /**
     * @param salePageTotalQty The SalePageTotalQty
     */
    public void setSalePageTotalQty(Integer salePageTotalQty) {
        this.salePageTotalQty = salePageTotalQty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.typeDef);
        dest.writeValue(this.totalQty);
        dest.writeValue(this.totalPrice);
        dest.writeString(this.discountTypeDef);
        dest.writeValue(this.discountPrice);
        dest.writeValue(this.discountRate);
        dest.writeValue(this.reachQtyCount);
        dest.writeValue(this.totalPayment);
        dest.writeValue(this.isMatched);
        dest.writeValue(this.salePageTotalQty);
    }

    protected MatchedCondition(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.typeDef = in.readString();
        this.totalQty = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.discountTypeDef = in.readString();
        this.discountPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.discountRate = (Double) in.readValue(Double.class.getClassLoader());
        this.reachQtyCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalPayment = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isMatched = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.salePageTotalQty = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<MatchedCondition> CREATOR = new Parcelable.Creator<MatchedCondition>() {
        @Override
        public MatchedCondition createFromParcel(Parcel source) {
            return new MatchedCondition(source);
        }

        @Override
        public MatchedCondition[] newArray(int size) {
            return new MatchedCondition[size];
        }
    };
}
