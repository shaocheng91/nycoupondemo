package com.nineyi.data.model.login;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kennylee on 2015/5/22.
 */
public class LoginReturnCode implements Parcelable {

    public String ReturnCode;
    public String Data;
    public String Message;

    public LoginReturnCode() {

    }

    public LoginReturnCode(Parcel in) {
        ReturnCode=in.readString();
        Data=in.readString();
        Message=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        dest.writeString(Data);
        dest.writeString(Message);
    }

    public static Creator<LoginReturnCode> CREATOR=new Creator<LoginReturnCode>(){
        @Override
        public LoginReturnCode createFromParcel(Parcel source) {
            return new LoginReturnCode(source);
        }

        @Override
        public LoginReturnCode[] newArray(int size) {
            return new LoginReturnCode[size];
        }
    };
}
