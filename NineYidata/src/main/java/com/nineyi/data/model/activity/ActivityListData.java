package com.nineyi.data.model.activity;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by iris on 15/6/23.
 */
public class ActivityListData implements Parcelable {

    private static final String FIELD_SHOP_ID = "Activity_ShopId";
    private static final String FIELD_SHOP_NAME = "Activity_ShopName";
    private static final String FIELD_ACTIVITYID = "Activity_Id";
    private static final String FIELD_ACTIVITYNAME = "Activity_Name";

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_SHOP_NAME)
    private String mShopName;

    @SerializedName(FIELD_ACTIVITYID)
    private String mActivityId;

    @SerializedName(FIELD_ACTIVITYNAME)
    private String mActivityName;

    public ActivityListData() {
    }

    public String getShopId() {
        return mShopId;
    }

    public String getShopName() {
        return mShopName;
    }

    public String getActivityId() {
        return mActivityId;
    }

    public String getActivityName() {
        return mActivityName;
    }

    public ActivityListData(Parcel in) {
        mShopId = in.readString();
        mShopName = in.readString();
        mActivityId = in.readString();
        mActivityName = in.readString();
    }

    public static final Creator<ActivityListData> CREATOR = new Creator<ActivityListData>() {
        public ActivityListData createFromParcel(Parcel in) {
            return new ActivityListData(in);
        }

        public ActivityListData[] newArray(int size) {
            return new ActivityListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mShopId);
        dest.writeString(mShopName);
        dest.writeString(mActivityId);
        dest.writeString(mActivityName);
    }


}
