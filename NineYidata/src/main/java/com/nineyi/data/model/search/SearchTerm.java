package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;


public final class SearchTerm implements Parcelable {

    public String SalePageTermText;
    public String ShopTermText;

    public SearchTerm() {
    }

    // Parcelable management
    private SearchTerm(Parcel in) {
        SalePageTermText = in.readString();
        ShopTermText = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(SalePageTermText);
        dest.writeString(ShopTermText);
    }

    public static final Parcelable.Creator<SearchTerm> CREATOR = new Parcelable.Creator<SearchTerm>() {
        public SearchTerm createFromParcel(Parcel in) {
            return new SearchTerm(in);
        }

        public SearchTerm[] newArray(int size) {
            return new SearchTerm[size];
        }
    };

    @Override
    public String toString() { // for 搜尋自動完成 adapter
        return SalePageTermText != null ? SalePageTermText : ShopTermText;
    }
}
