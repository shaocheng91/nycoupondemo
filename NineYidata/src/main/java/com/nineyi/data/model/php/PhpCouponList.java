package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class PhpCouponList implements Parcelable {

    public ArrayList<PhpCouponItem> feed;

    public PhpCouponList() {
    }

    // Parcelable management
    private PhpCouponList(Parcel in) {
        feed = in.createTypedArrayList(PhpCouponItem.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(feed);
    }

    public static final Parcelable.Creator<PhpCouponList> CREATOR = new Parcelable.Creator<PhpCouponList>() {
        public PhpCouponList createFromParcel(Parcel in) {
            return new PhpCouponList(in);
        }

        public PhpCouponList[] newArray(int size) {
            return new PhpCouponList[size];
        }
    };
}
