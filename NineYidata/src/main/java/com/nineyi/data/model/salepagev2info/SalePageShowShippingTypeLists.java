
package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalePageShowShippingTypeLists implements Parcelable {

    @SerializedName("Installment")
    @Expose
    private String installment;
    @SerializedName("Family")
    @Expose
    private String family;
    @SerializedName("SevenEleven")
    @Expose
    private String sevenEleven;

    protected SalePageShowShippingTypeLists(Parcel in) {
        installment = in.readString();
        family = in.readString();
        sevenEleven = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(installment);
        dest.writeString(family);
        dest.writeString(sevenEleven);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SalePageShowShippingTypeLists> CREATOR = new Parcelable.Creator<SalePageShowShippingTypeLists>() {
        @Override
        public SalePageShowShippingTypeLists createFromParcel(Parcel in) {
            return new SalePageShowShippingTypeLists(in);
        }

        @Override
        public SalePageShowShippingTypeLists[] newArray(int size) {
            return new SalePageShowShippingTypeLists[size];
        }
    };
}
