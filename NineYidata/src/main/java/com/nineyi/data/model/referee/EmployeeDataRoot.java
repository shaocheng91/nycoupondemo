package com.nineyi.data.model.referee;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class EmployeeDataRoot implements Parcelable {

    public ArrayList<RefereeEmployeeListInfo> EmployeeList;


    public EmployeeDataRoot() {

    }


    public EmployeeDataRoot(Parcel in) {

        EmployeeList = in.createTypedArrayList(RefereeEmployeeListInfo.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EmployeeDataRoot> CREATOR = new Creator<EmployeeDataRoot>() {
        public EmployeeDataRoot createFromParcel(Parcel in) {
            return new EmployeeDataRoot(in);
        }

        public EmployeeDataRoot[] newArray(int size) {
            return new EmployeeDataRoot[size];
        }
    };
    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeTypedList(EmployeeList);
    }


}