
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class DeliveryTypeList implements Parcelable {
    public static final String ID = "Id";
    public static final String IS_SELECTED = "IsSelected";

    @SerializedName(ID)
    @Expose
    private Integer Id;
    @SerializedName("FeeTypeDef")
    @Expose
    private String FeeTypeDef;
    @SerializedName("FeeTypeDefDesc")
    @Expose
    private String FeeTypeDefDesc;
    @SerializedName("Fee")
    @Expose
    private Double Fee;
    @SerializedName("OverPrice")
    @Expose
    private Double OverPrice;
    @SerializedName("TypeName")
    @Expose
    private String TypeName;
    @SerializedName("IsReachFreeFee")
    @Expose
    private Boolean IsReachFreeFee;
    @SerializedName("ShippingProfileTypeDef")
    @Expose
    private String ShippingProfileTypeDef;
    @SerializedName("TemperatureTypeDef")
    @Expose
    private String TemperatureTypeDef;
    @SerializedName("IsLowestFee")
    @Expose
    private Boolean IsLowestFee;
    @SerializedName(IS_SELECTED)
    @Expose
    private Boolean IsSelected;
    @SerializedName("TotalFee")
    @Expose
    private Integer TotalFee;

    /**
     * 
     * @return
     *     The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The FeeTypeDef
     */
    public String getFeeTypeDef() {
        return FeeTypeDef;
    }

    /**
     * 
     * @param FeeTypeDef
     *     The FeeTypeDef
     */
    public void setFeeTypeDef(String FeeTypeDef) {
        this.FeeTypeDef = FeeTypeDef;
    }

    /**
     * 
     * @return
     *     The FeeTypeDefDesc
     */
    public String getFeeTypeDefDesc() {
        return FeeTypeDefDesc;
    }

    /**
     * 
     * @param FeeTypeDefDesc
     *     The FeeTypeDefDesc
     */
    public void setFeeTypeDefDesc(String FeeTypeDefDesc) {
        this.FeeTypeDefDesc = FeeTypeDefDesc;
    }

    /**
     * 
     * @return
     *     The Fee
     */
    public Double getFee() {
        return Fee;
    }

    /**
     * 
     * @param Fee
     *     The Fee
     */
    public void setFee(Double Fee) {
        this.Fee = Fee;
    }

    /**
     * 
     * @return
     *     The OverPrice
     */
    public Double getOverPrice() {
        return OverPrice;
    }

    /**
     * 
     * @param OverPrice
     *     The OverPrice
     */
    public void setOverPrice(Double OverPrice) {
        this.OverPrice = OverPrice;
    }

    /**
     * 
     * @return
     *     The TypeName
     */
    public String getTypeName() {
        return TypeName;
    }

    /**
     * 
     * @param TypeName
     *     The TypeName
     */
    public void setTypeName(String TypeName) {
        this.TypeName = TypeName;
    }

    /**
     * 
     * @return
     *     The IsReachFreeFee
     */
    public Boolean getIsReachFreeFee() {
        return IsReachFreeFee;
    }

    /**
     * 
     * @param IsReachFreeFee
     *     The IsReachFreeFee
     */
    public void setIsReachFreeFee(Boolean IsReachFreeFee) {
        this.IsReachFreeFee = IsReachFreeFee;
    }

    /**
     * 
     * @return
     *     The ShippingProfileTypeDef
     */
    public String getShippingProfileTypeDef() {
        return ShippingProfileTypeDef;
    }

    /**
     * 
     * @param ShippingProfileTypeDef
     *     The ShippingProfileTypeDef
     */
    public void setShippingProfileTypeDef(String ShippingProfileTypeDef) {
        this.ShippingProfileTypeDef = ShippingProfileTypeDef;
    }

    /**
     * 
     * @return
     *     The TemperatureTypeDef
     */
    public String getTemperatureTypeDef() {
        return TemperatureTypeDef;
    }

    /**
     * 
     * @param TemperatureTypeDef
     *     The TemperatureTypeDef
     */
    public void setTemperatureTypeDef(String TemperatureTypeDef) {
        this.TemperatureTypeDef = TemperatureTypeDef;
    }

    /**
     * 
     * @return
     *     The IsLowestFee
     */
    public Boolean getIsLowestFee() {
        return IsLowestFee;
    }

    /**
     * 
     * @param IsLowestFee
     *     The IsLowestFee
     */
    public void setIsLowestFee(Boolean IsLowestFee) {
        this.IsLowestFee = IsLowestFee;
    }

    /**
     * 
     * @return
     *     The IsSelected
     */
    public Boolean getIsSelected() {
        return IsSelected;
    }

    /**
     * 
     * @param IsSelected
     *     The IsSelected
     */
    public void setIsSelected(Boolean IsSelected) {
        this.IsSelected = IsSelected;
    }

    /**
     * 
     * @return
     *     The TotalFee
     */
    public Integer getTotalFee() {
        return TotalFee;
    }

    /**
     * 
     * @param TotalFee
     *     The TotalFee
     */
    public void setTotalFee(Integer TotalFee) {
        this.TotalFee = TotalFee;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.Id);
        dest.writeString(this.FeeTypeDef);
        dest.writeString(this.FeeTypeDefDesc);
        dest.writeValue(this.Fee);
        dest.writeValue(this.OverPrice);
        dest.writeString(this.TypeName);
        dest.writeValue(this.IsReachFreeFee);
        dest.writeString(this.ShippingProfileTypeDef);
        dest.writeString(this.TemperatureTypeDef);
        dest.writeValue(this.IsLowestFee);
        dest.writeValue(this.IsSelected);
        dest.writeValue(this.TotalFee);
    }

    public DeliveryTypeList() {
    }

    protected DeliveryTypeList(Parcel in) {
        this.Id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.FeeTypeDef = in.readString();
        this.FeeTypeDefDesc = in.readString();
        this.Fee = (Double) in.readValue(Double.class.getClassLoader());
        this.OverPrice = (Double) in.readValue(Double.class.getClassLoader());
        this.TypeName = in.readString();
        this.IsReachFreeFee = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.ShippingProfileTypeDef = in.readString();
        this.TemperatureTypeDef = in.readString();
        this.IsLowestFee = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.IsSelected = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.TotalFee = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<DeliveryTypeList> CREATOR = new Parcelable.Creator<DeliveryTypeList>() {
        @Override
        public DeliveryTypeList createFromParcel(Parcel source) {
            return new DeliveryTypeList(source);
        }

        @Override
        public DeliveryTypeList[] newArray(int size) {
            return new DeliveryTypeList[size];
        }
    };
}
