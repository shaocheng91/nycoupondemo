package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class DistrictList implements Parcelable{

    private static final String FIELD_ZIP_CODE = "ZipCode";
    private static final String FIELD_DISTRICT = "District";


    @SerializedName(FIELD_ZIP_CODE)
    private int mZipCode;
    @SerializedName(FIELD_DISTRICT)
    private String mDistrict;


    public DistrictList(){

    }

    public void setZipCode(int zipCode) {
        mZipCode = zipCode;
    }

    public int getZipCode() {
        return mZipCode;
    }

    public void setDistrict(String district) {
        mDistrict = district;
    }

    public String getDistrict() {
        return mDistrict;
    }

    public DistrictList(Parcel in) {
        mZipCode = in.readInt();
        mDistrict = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<DistrictList> CREATOR = new Parcelable.Creator<DistrictList>() {
        public DistrictList createFromParcel(Parcel in) {
            return new DistrictList(in);
        }

        public DistrictList[] newArray(int size) {
            return new DistrictList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mZipCode);
        dest.writeString(mDistrict);
    }


}