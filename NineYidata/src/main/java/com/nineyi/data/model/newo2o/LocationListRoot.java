package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class LocationListRoot implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private LocationListData mData;


    public LocationListRoot(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(LocationListData datum) {
        mData = datum;
    }

    public LocationListData getDatum() {
        return mData;
    }

    public LocationListRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mData = in.readParcelable(LocationListData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationListRoot> CREATOR = new Creator<LocationListRoot>() {
        public LocationListRoot createFromParcel(Parcel in) {
            return new LocationListRoot(in);
        }

        public LocationListRoot[] newArray(int size) {
            return new LocationListRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        		dest.writeParcelable(mData, flags);
    }


}