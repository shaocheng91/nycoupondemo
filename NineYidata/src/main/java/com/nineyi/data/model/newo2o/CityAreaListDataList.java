package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import java.util.ArrayList;


public class CityAreaListDataList implements Parcelable{

    private static final String FIELD_ID = "Id";
    private static final String FIELD_TITLE = "Title";
    private static final String FIELD_AREA_LIST = "AreaList";


    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_AREA_LIST)
    private ArrayList<CityAreaDataAreaList> mCityAreaDataAreaLists;
    private int mType;

    public CityAreaListDataList(){

    }

    public void setType(int type) {
        mType = type;
    }

    public int getType() {
        return mType;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setCityAreaDataAreaLists(ArrayList<CityAreaDataAreaList> cityAreaDataAreaLists) {
        mCityAreaDataAreaLists = cityAreaDataAreaLists;
    }

    public ArrayList<CityAreaDataAreaList> getCityAreaDataAreaLists() {
        return mCityAreaDataAreaLists;
    }


    public CityAreaListDataList(Parcel in) {
        mId = in.readInt();
        mTitle = in.readString();
        new ArrayList<CityAreaDataAreaList>();
        in.readTypedList(mCityAreaDataAreaLists, CityAreaDataAreaList.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CityAreaListDataList> CREATOR = new Creator<CityAreaListDataList>() {
        public CityAreaListDataList createFromParcel(Parcel in) {
            return new CityAreaListDataList(in);
        }

        public CityAreaListDataList[] newArray(int size) {
            return new CityAreaListDataList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeTypedList(mCityAreaDataAreaLists);
    }


}