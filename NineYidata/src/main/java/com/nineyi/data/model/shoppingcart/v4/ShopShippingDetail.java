package com.nineyi.data.model.shoppingcart.v4;

import com.nineyi.data.model.gson.NineyiDate;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/9/7.
 */
public class ShopShippingDetail {
    public int ShopShippingTypeId;
    public String ShippingProfileTypeDef;
    public ArrayList<String> DisplayText;
    public String ColorCode;
    public NineyiDate DisplayStartDateTime;
    public NineyiDate DisplayEndDateTime;
}
