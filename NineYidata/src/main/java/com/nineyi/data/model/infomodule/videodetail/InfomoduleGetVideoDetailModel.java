package com.nineyi.data.model.infomodule.videodetail;

import com.nineyi.data.model.infomodule.DotNetModel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleGetVideoDetailModel extends DotNetModel {

    public InfomoduleGetVideoDetailDataModel Data;

    public InfomoduleGetVideoDetailModel(Parcel in) {
        super(in);
        Data = InfomoduleGetVideoDetailDataModel.CREATOR.createFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(Data, flags);
    }

    public static Parcelable.Creator<InfomoduleGetVideoDetailModel> CREATOR
            = new Parcelable.Creator<InfomoduleGetVideoDetailModel>() {
        @Override
        public InfomoduleGetVideoDetailModel createFromParcel(Parcel source) {
            return new InfomoduleGetVideoDetailModel(source);
        }

        @Override
        public InfomoduleGetVideoDetailModel[] newArray(int size) {
            return new InfomoduleGetVideoDetailModel[size];
        }
    };
}
