package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.nineyi.data.model.ecoupon.ECouponShopECoupon;
import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.data.model.installment.InstallmentShopInstallmentList;
import com.nineyi.data.model.salepage.SKUPropertySet;
import com.nineyi.data.model.salepage.SalePageGift;
import com.nineyi.data.model.salepage.SalePageImage;
import com.nineyi.data.model.salepage.SalePageMajor;
import com.nineyi.data.model.salepage.SalePageNotKeyProperty;
import com.nineyi.data.model.salepage.SalePageRealTimeDatum;
import com.nineyi.data.model.salepage.SalePageShippingType;

import java.util.ArrayList;
import java.util.List;

public class SalePageWrapper implements Parcelable {
    private SalePageV2Info mSalePageInfo;
    private SalePageAdditionalInfo mSalePageAdditionalInfo;

    public SalePageWrapper(SalePageV2Info salePageV2Info) {
        this.mSalePageInfo              = salePageV2Info;
    }

    public SalePageWrapper(SalePageV2Info salePageV2Info, SalePageAdditionalInfo salePageAdditionalInfo) {
        this.mSalePageInfo              = salePageV2Info;
        this.mSalePageAdditionalInfo    = salePageAdditionalInfo;
    }

    protected SalePageWrapper(Parcel in) {
        mSalePageInfo = (SalePageV2Info) in.readValue(SalePageV2Info.class.getClassLoader());
        mSalePageAdditionalInfo = (SalePageAdditionalInfo) in.readValue(SalePageAdditionalInfo.class.getClassLoader());
    }

    public String getSalePageV2InfoReturnCode() {
        return (mSalePageInfo != null) ? mSalePageInfo.getReturnCode() : null;
    }

    public String getSalePageAdditionalInfoReturnCode() {
        return (mSalePageAdditionalInfo != null) ? mSalePageAdditionalInfo.getReturnCode() : null;
    }

    public Double getPrice() {
        return (mSalePageInfo != null) ? mSalePageInfo.getPrice() : 0.d;
    }

    public Double getSuggestPrice() {
        return (mSalePageInfo != null) ? mSalePageInfo.getSuggestPrice() : 0.d;
    }

    public int getShopId() {
        return (mSalePageInfo != null) ? mSalePageInfo.getShopId() : 0;
    }

    public int getShopCategoryId() {
        return (mSalePageInfo != null) ? mSalePageInfo.getShopCategoryId() : 0;
    }

    public int getSalePageId() {
        return (mSalePageInfo != null) ? mSalePageInfo.getSalePageId() : 0;
    }

    public String getTitle() {
        return (mSalePageInfo != null) ? mSalePageInfo.getTitle() : "";
    }

    public ArrayList<SalePageImage> getImageList() {
        return (mSalePageInfo != null) ? new ArrayList<>(mSalePageInfo.getImageList()) : new ArrayList<SalePageImage>();
    }

    public String getPicURL() {
        return (mSalePageInfo != null) ? mSalePageInfo.getPicURL() : "";
    }

    public String getShopName() {
        return (mSalePageInfo != null) ? mSalePageInfo.getShopName() : "";
    }

    public String getSubDescript() {
        return (mSalePageInfo != null) ? mSalePageInfo.getSubDescript() : "";
    }

    public String getSaleProductDescContent() {
        return (mSalePageAdditionalInfo != null) ? mSalePageAdditionalInfo.getSaleProductDescContent() : "";
    }

    public boolean hasSKU() {
        if (mSalePageInfo != null) {
            return mSalePageInfo.hasSKU();
        } else {
            return false;
        }
    }

    public ArrayList<SalePageMajor> getMajorList() {
        return (mSalePageInfo != null) ? mSalePageInfo.getMajorList() : new ArrayList<SalePageMajor>();
    }

    public String getShopCategoryText() {
        return (mSalePageInfo != null) ? mSalePageInfo.getShopCategoryText() : null;
    }

    public String getShortDescription() {
        return (mSalePageInfo != null) ? mSalePageInfo.getShortDescription() : null;
    }

    public ArrayList<SKUPropertySet> getSKUPropertySetList() {
        return (mSalePageInfo != null) ? mSalePageInfo.getSKUPropertySetList() : new ArrayList<SKUPropertySet>();
    }

    public ArrayList<String> getPayProfileTypeDefList() {
        return (mSalePageInfo != null) ? new ArrayList<>(mSalePageInfo.getPayProfileTypeDefList()) : new ArrayList<String>();
    }

    public ArrayList<SalePageShippingType> getShippingTypeList() {
        return (mSalePageInfo != null) ? mSalePageInfo.getShippingTypeList() : new ArrayList<SalePageShippingType>();
    }

    public ArrayList<InstallmentShopInstallmentList> getInstallmentList() {
        return (mSalePageInfo != null) ? new ArrayList<>(mSalePageInfo.getInstallmentList()) : new ArrayList<InstallmentShopInstallmentList>();
    }

    public String getStatusDef() {
        return (mSalePageInfo != null) ? mSalePageInfo.getStatusDef() : "";
    }

    public boolean isDisplayExcludeECouponDiscount() {
        if (mSalePageInfo != null) {
            return mSalePageInfo.isDisplayExcludeECouponDiscount();
        } else {
            return false;
        }
    }

    public boolean isShowSoldQty() {
        if (mSalePageInfo != null) {
            return mSalePageInfo.isShowSoldQty();
        } else {
            return false;
        }
    }

    public int getSoldQty() {
        return (mSalePageInfo != null) ? mSalePageInfo.getSoldQty() : 0;
    }

    public boolean isShowTradesOrderList() {
        if (mSalePageInfo != null) {
            return mSalePageInfo.isShowTradesOrderList();
        } else {
            return false;
        }
    }

    public String getTradesOrderListUri() {
        return (mSalePageInfo != null) ? mSalePageInfo.getTradesOrderListUri() : "";
    }

    public NineyiDate getSellingStartDateTime() {
        return (mSalePageInfo != null) ? mSalePageInfo.getSellingStartDateTime() : null;
    }

    public void setStatusDef(String statusDef) {
        if (mSalePageInfo != null) {
            mSalePageInfo.setStatusDef(statusDef);
        }
    }

    public boolean isShareToBuy() {
        if (mSalePageInfo != null) {
            return mSalePageInfo.isShareToBuy();
        } else {
            return false;
        }
    }

    public List<Promotion> getPromotions() {
        return (mSalePageInfo != null) ? mSalePageInfo.getPromotions() : new ArrayList<Promotion>();
    }

    public List<SalePageGift> getGifts() {
        return (mSalePageInfo != null) ? mSalePageInfo.getGifts() : new ArrayList<SalePageGift>();
    }

    public List<ECouponShopECoupon> getECoupons() {
        return (mSalePageInfo != null) ? mSalePageInfo.getECoupons() : new ArrayList<ECouponShopECoupon>();
    }

    public String getFreeShippingTag() {
        return (mSalePageInfo != null) ? mSalePageInfo.getFreeShippingTag() : null;
    }

    public ArrayList<SalePageNotKeyProperty> getNotKeyPropertyList() {
        return (mSalePageAdditionalInfo != null) ? mSalePageAdditionalInfo.getNotKeyPropertyList() : new ArrayList<SalePageNotKeyProperty>();
    }

    @Nullable
    public String getSecondScreenYoutubeURL() {
        MainImageVideo secondScreenMainVideo = getSecondScreenMainVideo();

        return (secondScreenMainVideo != null) ? secondScreenMainVideo.getVideoUrl() : null;
    }

    private MainImageVideo getSecondScreenMainVideo() {
        return (mSalePageAdditionalInfo != null) ? mSalePageAdditionalInfo.getMainImageVideo() : null;
    }

    @Nullable
    public String getFirstScreenLeadingYoutubeURL() {
        MainImageVideo video = getMainImageVideo();

        return (video != null) ? video.getVideoUrl() : null;
    }

    private MainImageVideo getMainImageVideo() {
        return (mSalePageInfo != null) ? mSalePageInfo.getMainImageVideo() : null;
    }

    @Nullable
    public String getFirstScreenTailingYoutubeURL() {
        MainImageVideo video = getMainImageVideo();

        return (video != null) ? video.getVideoUrl() : null;
    }

    public void syncWithRealTimeData(SalePageRealTimeDatum salePageRealTime) {
        if (mSalePageInfo != null) {
            mSalePageInfo.setStatusDef(salePageRealTime.getStatusDef());
            mSalePageInfo.setPrice(salePageRealTime.getPrice());
            mSalePageInfo.setSuggestPrice(salePageRealTime.getSuggestPrice());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mSalePageInfo);
        dest.writeValue(mSalePageAdditionalInfo);
    }

    public static final Creator<SalePageWrapper> CREATOR = new Creator<SalePageWrapper>() {
        @Override
        public SalePageWrapper createFromParcel(Parcel source) {
            return new SalePageWrapper(source);
        }

        @Override
        public SalePageWrapper[] newArray(int size) {
            return new SalePageWrapper[size];
        }
    };
}
