package com.nineyi.data.model.ranking;

import android.os.Parcel;
import android.os.Parcelable;


public final class SaleRankingData implements Parcelable {

    public long HotSaleRanking_Id;
    public long HotSaleRanking_SalePageId;
    public String HotSaleRanking_PeriodDef;
    public int HotSaleRanking_Rank;
    public int HotSaleRanking_ShopId;
    public int HotSaleRanking_TotalSaleQty;
    public String HotSaleRanking_SalePageTitle;
    public String HotSaleRanking_PicUrl;
    public double HotSaleRanking_Price;
    public double HotSaleRanking_SuggestPrice;
    public String HotSaleRanking_Source;
    public boolean isFav;

    public SaleRankingData() {
    }

    // Parcelable management
    private SaleRankingData(Parcel in) {
        HotSaleRanking_Id = in.readLong();
        HotSaleRanking_SalePageId = in.readLong();
        HotSaleRanking_PeriodDef = in.readString();
        HotSaleRanking_Rank = in.readInt();
        HotSaleRanking_ShopId = in.readInt();
        HotSaleRanking_TotalSaleQty = in.readInt();
        HotSaleRanking_SalePageTitle = in.readString();
        HotSaleRanking_PicUrl = in.readString();
        HotSaleRanking_Price = in.readDouble();
        HotSaleRanking_SuggestPrice = in.readDouble();
        HotSaleRanking_Source = in.readString();
        isFav = in.readInt() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(HotSaleRanking_Id);
        dest.writeLong(HotSaleRanking_SalePageId);
        dest.writeString(HotSaleRanking_PeriodDef);
        dest.writeInt(HotSaleRanking_Rank);
        dest.writeInt(HotSaleRanking_ShopId);
        dest.writeInt(HotSaleRanking_TotalSaleQty);
        dest.writeString(HotSaleRanking_SalePageTitle);
        dest.writeString(HotSaleRanking_PicUrl);
        dest.writeDouble(HotSaleRanking_Price);
        dest.writeDouble(HotSaleRanking_SuggestPrice);
        dest.writeString(HotSaleRanking_Source);
        dest.writeInt(isFav ? 1 : 0);
    }

    public static final Parcelable.Creator<SaleRankingData> CREATOR
            = new Parcelable.Creator<SaleRankingData>() {
        public SaleRankingData createFromParcel(Parcel in) {
            return new SaleRankingData(in);
        }

        public SaleRankingData[] newArray(int size) {
            return new SaleRankingData[size];
        }
    };
}
