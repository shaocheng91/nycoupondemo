
package com.nineyi.data.model.infomodule.videolist;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class VideoModuleData implements Parcelable {

    @SerializedName("List")
    @Expose
    private ArrayList<VideoModuleDataList> list = new ArrayList<VideoModuleDataList>();
    @SerializedName("Shop")
    @Expose
    private VideoModuleDataShop shop;

    /**
     * 
     * @return
     *     The list
     */
    public ArrayList<VideoModuleDataList> getList() {
        return list;
    }

    /**
     * 
     * @param list
     *     The List
     */
    public void setList(ArrayList<VideoModuleDataList> list) {
        this.list = list;
    }

    /**
     * 
     * @return
     *     The shop
     */
    public VideoModuleDataShop getShop() {
        return shop;
    }

    /**
     * 
     * @param shop
     *     The Shop
     */
    public void setShop(VideoModuleDataShop shop) {
        this.shop = shop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.list);
        dest.writeParcelable(this.shop, flags);
    }

    public VideoModuleData() {
    }

    protected VideoModuleData(Parcel in) {
        this.list = new ArrayList<VideoModuleDataList>();
        in.readList(this.list, VideoModuleDataList.class.getClassLoader());
        this.shop = in.readParcelable(VideoModuleDataShop.class.getClassLoader());
    }

    public static final Parcelable.Creator<VideoModuleData> CREATOR = new Parcelable.Creator<VideoModuleData>() {
        @Override
        public VideoModuleData createFromParcel(Parcel source) {
            return new VideoModuleData(source);
        }

        @Override
        public VideoModuleData[] newArray(int size) {
            return new VideoModuleData[size];
        }
    };
}
