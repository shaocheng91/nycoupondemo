
package com.nineyi.data.model.infomodule.articlelist;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ArticleModuleData implements Parcelable {

    @SerializedName("List")
    @Expose
    private ArrayList<ArticleModuleDataList> list = new ArrayList<ArticleModuleDataList>();
    @SerializedName("Shop")
    @Expose
    private ArticleModuleDataShop shop;

    /**
     * 
     * @return
     *     The list
     */
    public ArrayList<ArticleModuleDataList> getList() {
        return list;
    }

    /**
     * 
     * @param list
     *     The List
     */
    public void setList(ArrayList<ArticleModuleDataList> list) {
        this.list = list;
    }

    /**
     * 
     * @return
     *     The shop
     */
    public ArticleModuleDataShop getShop() {
        return shop;
    }

    /**
     * 
     * @param shop
     *     The Shop
     */
    public void setShop(ArticleModuleDataShop shop) {
        this.shop = shop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.list);
        dest.writeParcelable(this.shop, flags);
    }

    public ArticleModuleData() {
    }

    protected ArticleModuleData(Parcel in) {
        this.list = new ArrayList<ArticleModuleDataList>();
        in.readList(this.list, ArticleModuleDataList.class.getClassLoader());
        this.shop = in.readParcelable(ArticleModuleDataShop.class.getClassLoader());
    }

    public static final Parcelable.Creator<ArticleModuleData> CREATOR = new Parcelable.Creator<ArticleModuleData>() {
        @Override
        public ArticleModuleData createFromParcel(Parcel source) {
            return new ArticleModuleData(source);
        }

        @Override
        public ArticleModuleData[] newArray(int size) {
            return new ArticleModuleData[size];
        }
    };
}
