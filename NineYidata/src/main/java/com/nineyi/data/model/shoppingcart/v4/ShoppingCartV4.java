
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ShoppingCartV4 implements Parcelable {

    public static final String DATA = "Data";

    @SerializedName("ReturnCode")
    @Expose
    private String ReturnCode;
    @SerializedName(DATA)
    @Expose
    private ShoppingCartData ShoppingCartData;
    @SerializedName("Message")
    @Expose
    private String Message;

    /**
     * 
     * @return
     *     The ReturnCode
     */
    public String getReturnCode() {
        return ReturnCode;
    }

    /**
     * 
     * @param ReturnCode
     *     The ReturnCode
     */
    public void setReturnCode(String ReturnCode) {
        this.ReturnCode = ReturnCode;
    }

    /**
     * 
     * @return
     *     The Data
     */
    public ShoppingCartData getShoppingCartData() {
        return ShoppingCartData;
    }

    /**
     * 
     * @param ShoppingCartData
     *     The Data
     */
    public void setShoppingCartData(ShoppingCartData ShoppingCartData) {
        this.ShoppingCartData = ShoppingCartData;
    }

    /**
     * 
     * @return
     *     The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * 
     * @param Message
     *     The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.ShoppingCartData, flags);
        dest.writeString(this.Message);
    }

    public ShoppingCartV4() {
    }

    protected ShoppingCartV4(Parcel in) {
        this.ReturnCode = in.readString();
        this.ShoppingCartData = in.readParcelable(ShoppingCartData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<ShoppingCartV4> CREATOR = new Parcelable.Creator<ShoppingCartV4>() {
        @Override
        public ShoppingCartV4 createFromParcel(Parcel source) {
            return new ShoppingCartV4(source);
        }

        @Override
        public ShoppingCartV4[] newArray(int size) {
            return new ShoppingCartV4[size];
        }
    };
}
