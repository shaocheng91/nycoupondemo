package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBCursors implements Parcelable {

    String after;
    String before;

    protected FBCursors(Parcel in) {
        after = in.readString();
        before = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(after);
        dest.writeString(before);
    }

    public static final Parcelable.Creator<FBCursors> CREATOR = new Parcelable.Creator<FBCursors>() {
        @Override
        public FBCursors createFromParcel(Parcel in) {
            return new FBCursors(in);
        }

        @Override
        public FBCursors[] newArray(int size) {
            return new FBCursors[size];
        }
    };
}