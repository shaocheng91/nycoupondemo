package com.nineyi.data.model.promotion;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by kelsey on 2016/12/15.
 */

public class PromotionListReturnCode implements Parcelable {
    private String ReturnCode;

    private ArrayList<Promotion> Data;

    private String Message;


    public PromotionListReturnCode() {
    }

    public static Creator<PromotionListReturnCode> getCREATOR() {
        return CREATOR;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.getReturnCode());
        dest.writeTypedList(this.getData());
        dest.writeString(this.getMessage());
    }

    protected PromotionListReturnCode(Parcel in) {
        this.setReturnCode(in.readString());
        this.setData(in.createTypedArrayList(Promotion.CREATOR));
        this.setMessage(in.readString());
    }

    private static final Creator<PromotionListReturnCode> CREATOR = new Creator<PromotionListReturnCode>() {
        @Override
        public PromotionListReturnCode createFromParcel(Parcel source) {
            return new PromotionListReturnCode(source);
        }

        @Override
        public PromotionListReturnCode[] newArray(int size) {
            return new PromotionListReturnCode[size];
        }
    };

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public ArrayList<Promotion> getData() {
        return Data;
    }

    public void setData(ArrayList<Promotion> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
