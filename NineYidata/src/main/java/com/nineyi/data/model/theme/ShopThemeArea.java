package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeArea implements Parcelable {
    public ArrayList<ShopThemeBlock> BlockList;

    public ShopThemeArea() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.BlockList);
    }

    protected ShopThemeArea(Parcel in) {
        this.BlockList = in.createTypedArrayList(ShopThemeBlock.CREATOR);
    }

    public static final Creator<ShopThemeArea> CREATOR = new Creator<ShopThemeArea>() {
        @Override
        public ShopThemeArea createFromParcel(Parcel source) {
            return new ShopThemeArea(source);
        }

        @Override
        public ShopThemeArea[] newArray(int size) {
            return new ShopThemeArea[size];
        }
    };
}
