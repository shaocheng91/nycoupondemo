package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePageMoreInfo implements Parcelable {

    public String SalePage_Id;
    public int SaleProduct_Id;
    public String SaleProduct_Title;
    public String SaleProductDesc_Content;

    public SalePageMoreInfo() {
    }

    // Parcelable management
    private SalePageMoreInfo(Parcel in) {
        SalePage_Id = in.readString();
        SaleProduct_Id = in.readInt();
        SaleProduct_Title = in.readString();
        SaleProductDesc_Content = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(SalePage_Id);
        dest.writeInt(SaleProduct_Id);
        dest.writeString(SaleProduct_Title);
        dest.writeString(SaleProductDesc_Content);
    }

    public static final Parcelable.Creator<SalePageMoreInfo> CREATOR = new Parcelable.Creator<SalePageMoreInfo>() {
        public SalePageMoreInfo createFromParcel(Parcel in) {
            return new SalePageMoreInfo(in);
        }

        public SalePageMoreInfo[] newArray(int size) {
            return new SalePageMoreInfo[size];
        }
    };
}
