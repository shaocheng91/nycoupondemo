package com.nineyi.data.model.shoppingcart;

import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.data.model.salepage.SalePageTag;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShoppingCartSalePageV3 implements Parcelable {

    public int SalePageGroupSeq;
    public String Type;
    public int SalePageId;
    public int SaleProductId;
    public int SaleProductSKUId;
    public int ShippingTypeDef;
    public String ShippingTypeDefDesc;
    public NineyiDate ShippingDate;
    public String ShippingWaitingDays;
    public String Title;
    public int Qty;
    public int QtyLimit;
    public double Price;
    public double ECouponDiscount;
    public int PromotionDiscount;
    public int ReachQtyPromotionDiscount;
    public double TotalDiscount;
    public double TotalPayment;
    public double TotalPrice;
    public ArrayList<ShoppingCartPayType> PayTypeList;
    public ArrayList<ShoppingCartDeliveryType> DeliveryTypeList;
    public String SKUPropertyDisplay;
    public ArrayList<String> SKUPropertyDisplayList;
    public String DateTime;
    public String PicUrl;
    public int SourceCategoryId;
    public int SourceShopCategoryId;
    public boolean IsSelected;
    public String Msg;
    public String UnloginId;
    public ArrayList<ShoppingCartMatchedPromotion> MatchedPromotions;
    public boolean IsLimit;
    public boolean HasPromotion;
    public boolean IsZeroInstallmentRate;
    public ArrayList<SalePageTag> Tags;
    public ArrayList<ShoppingCartTag> TagsV2;
    public String TaxTypeDef;
    public int[] ShopCategoryIdList;
    public NineyiDate ShippingEndDate;
    public String TemperatureTypeDef;
    public int Length;
    public int Width;
    public int Height;
    public int Weight;
    public NineyiDate TemperatureShippingStartDate;
    public NineyiDate TemperatureShippingEndDate;
    public String SupplierStoreProfileMasterCode;
    public String SupplierStoreProfileStoreSlaveCode;
    public ArrayList<ShoppingCartSalePageGift> SalePageGiftList;
    public boolean IsSalePageGift;
    public String SalePageGiftId;
    public String SalePageGiftSlaveId;
    public String SalePageGiftGroupSeq;
    public double AveragePayment;

    public ShoppingCartSalePageV3() {
    }

    @Override
    public boolean equals(Object that) {
        if (that instanceof ShoppingCartSalePageV3) {
            return this.SaleProductSKUId == ((ShoppingCartSalePageV3) that).SaleProductSKUId;
        }

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.SalePageGroupSeq);
        dest.writeString(this.Type);
        dest.writeInt(this.SalePageId);
        dest.writeInt(this.SaleProductId);
        dest.writeInt(this.SaleProductSKUId);
        dest.writeInt(this.ShippingTypeDef);
        dest.writeString(this.ShippingTypeDefDesc);
        dest.writeParcelable(this.ShippingDate, 0);
        dest.writeString(this.ShippingWaitingDays);
        dest.writeString(this.Title);
        dest.writeInt(this.Qty);
        dest.writeInt(this.QtyLimit);
        dest.writeDouble(this.Price);
        dest.writeDouble(this.ECouponDiscount);
        dest.writeInt(this.PromotionDiscount);
        dest.writeInt(this.ReachQtyPromotionDiscount);
        dest.writeDouble(this.TotalDiscount);
        dest.writeDouble(this.TotalPayment);
        dest.writeDouble(this.TotalPrice);
        dest.writeTypedList(PayTypeList);
        dest.writeTypedList(DeliveryTypeList);
        dest.writeString(this.SKUPropertyDisplay);
        dest.writeStringList(this.SKUPropertyDisplayList);
        dest.writeString(this.DateTime);
        dest.writeString(this.PicUrl);
        dest.writeInt(this.SourceCategoryId);
        dest.writeInt(this.SourceShopCategoryId);
        dest.writeByte(IsSelected ? (byte) 1 : (byte) 0);
        dest.writeString(this.Msg);
        dest.writeString(this.UnloginId);
        dest.writeTypedList(MatchedPromotions);
        dest.writeByte(IsLimit ? (byte) 1 : (byte) 0);
        dest.writeByte(HasPromotion ? (byte) 1 : (byte) 0);
        dest.writeByte(IsZeroInstallmentRate ? (byte) 1 : (byte) 0);
        dest.writeTypedList(Tags);
        dest.writeTypedList(TagsV2);
        dest.writeString(this.TaxTypeDef);
        dest.writeIntArray(this.ShopCategoryIdList);
        dest.writeParcelable(this.ShippingEndDate, 0);
        dest.writeString(this.TemperatureTypeDef);
        dest.writeInt(this.Length);
        dest.writeInt(this.Width);
        dest.writeInt(this.Height);
        dest.writeInt(this.Weight);
        dest.writeParcelable(this.TemperatureShippingStartDate, 0);
        dest.writeParcelable(this.TemperatureShippingEndDate, 0);
        dest.writeString(this.SupplierStoreProfileMasterCode);
        dest.writeString(this.SupplierStoreProfileStoreSlaveCode);
        dest.writeTypedList(SalePageGiftList);
        dest.writeByte(IsSalePageGift ? (byte) 1 : (byte) 0);
        dest.writeString(this.SalePageGiftId);
        dest.writeString(this.SalePageGiftSlaveId);
        dest.writeString(this.SalePageGiftGroupSeq);
        dest.writeDouble(this.AveragePayment);
    }

    protected ShoppingCartSalePageV3(Parcel in) {
        this.SalePageGroupSeq = in.readInt();
        this.Type = in.readString();
        this.SalePageId = in.readInt();
        this.SaleProductId = in.readInt();
        this.SaleProductSKUId = in.readInt();
        this.ShippingTypeDef = in.readInt();
        this.ShippingTypeDefDesc = in.readString();
        this.ShippingDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.ShippingWaitingDays = in.readString();
        this.Title = in.readString();
        this.Qty = in.readInt();
        this.QtyLimit = in.readInt();
        this.Price = in.readDouble();
        this.ECouponDiscount = in.readDouble();
        this.PromotionDiscount = in.readInt();
        this.ReachQtyPromotionDiscount = in.readInt();
        this.TotalDiscount = in.readDouble();
        this.TotalPayment = in.readDouble();
        this.TotalPrice = in.readDouble();
        this.PayTypeList = in.createTypedArrayList(ShoppingCartPayType.CREATOR);
        this.DeliveryTypeList = in.createTypedArrayList(ShoppingCartDeliveryType.CREATOR);
        this.SKUPropertyDisplay = in.readString();
        this.SKUPropertyDisplayList = in.createStringArrayList();
        this.DateTime = in.readString();
        this.PicUrl = in.readString();
        this.SourceCategoryId = in.readInt();
        this.SourceShopCategoryId = in.readInt();
        this.IsSelected = in.readByte() != 0;
        this.Msg = in.readString();
        this.UnloginId = in.readString();
        this.MatchedPromotions = in.createTypedArrayList(ShoppingCartMatchedPromotion.CREATOR);
        this.IsLimit = in.readByte() != 0;
        this.HasPromotion = in.readByte() != 0;
        this.IsZeroInstallmentRate = in.readByte() != 0;
        this.Tags = in.createTypedArrayList(SalePageTag.CREATOR);
        this.TagsV2 = in.createTypedArrayList(ShoppingCartTag.CREATOR);
        this.TaxTypeDef = in.readString();
        this.ShopCategoryIdList = in.createIntArray();
        this.ShippingEndDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.TemperatureTypeDef = in.readString();
        this.Length = in.readInt();
        this.Width = in.readInt();
        this.Height = in.readInt();
        this.Weight = in.readInt();
        this.TemperatureShippingStartDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.TemperatureShippingEndDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.SupplierStoreProfileMasterCode = in.readString();
        this.SupplierStoreProfileStoreSlaveCode = in.readString();
        this.SalePageGiftList = in.createTypedArrayList(ShoppingCartSalePageGift.CREATOR);
        this.IsSalePageGift = in.readByte() != 0;
        this.SalePageGiftId = in.readString();
        this.SalePageGiftSlaveId = in.readString();
        this.SalePageGiftGroupSeq = in.readString();
        this.AveragePayment = in.readDouble();
    }

    public static final Creator<ShoppingCartSalePageV3> CREATOR = new Creator<ShoppingCartSalePageV3>() {
        public ShoppingCartSalePageV3 createFromParcel(Parcel source) {
            return new ShoppingCartSalePageV3(source);
        }

        public ShoppingCartSalePageV3[] newArray(int size) {
            return new ShoppingCartSalePageV3[size];
        }
    };
}
