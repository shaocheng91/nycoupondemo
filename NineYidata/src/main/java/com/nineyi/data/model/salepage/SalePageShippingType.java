package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePageShippingType implements Parcelable {

    public String FeeTypeDef;
    public String FeeTypeDefDesc;
    public double OverPrice;
    public String TypeName;
    public double Fee;
    public int SupplierId;
    public int ShopId;
    public boolean IsEnabled;
    public boolean IsDefault;
    public String ShippingTypeDef;
    public String ShippingProfileTypeDef;
    public String DisplayString;

    public SalePageShippingType() {
    }

    // Parcelable management
    private SalePageShippingType(Parcel in) {
        FeeTypeDef = in.readString();
        FeeTypeDefDesc = in.readString();
        OverPrice = in.readDouble();
        TypeName = in.readString();
        Fee = in.readDouble();
        SupplierId = in.readInt();
        ShopId = in.readInt();
        IsEnabled = in.readInt() == 1;
        IsDefault = in.readInt() == 1;
        ShippingTypeDef = in.readString();
        DisplayString = in.readString();
        ShippingProfileTypeDef = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(FeeTypeDef);
        dest.writeString(FeeTypeDefDesc);
        dest.writeDouble(OverPrice);
        dest.writeString(TypeName);
        dest.writeDouble(Fee);
        dest.writeInt(SupplierId);
        dest.writeInt(ShopId);
        dest.writeInt(IsEnabled ? 1 : 0);
        dest.writeInt(IsDefault ? 1 : 0);
        dest.writeString(ShippingTypeDef);
        dest.writeString(DisplayString);
        dest.writeString(ShippingProfileTypeDef);
    }

    public static final Parcelable.Creator<SalePageShippingType> CREATOR
            = new Parcelable.Creator<SalePageShippingType>() {
        public SalePageShippingType createFromParcel(Parcel in) {
            return new SalePageShippingType(in);
        }

        public SalePageShippingType[] newArray(int size) {
            return new SalePageShippingType[size];
        }
    };
}
