package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;

public class PromotionTargetMemberTierForDisplay implements Parcelable {
    public Integer PromotionTargetMemberTierId;
    public String CrmShopMemberCardName;

    protected PromotionTargetMemberTierForDisplay(Parcel in) {
        PromotionTargetMemberTierId = in.readByte() == 0x00 ? null : in.readInt();
        CrmShopMemberCardName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (PromotionTargetMemberTierId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(PromotionTargetMemberTierId);
        }
        dest.writeString(CrmShopMemberCardName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PromotionTargetMemberTierForDisplay> CREATOR = new Parcelable.Creator<PromotionTargetMemberTierForDisplay>() {
        @Override
        public PromotionTargetMemberTierForDisplay createFromParcel(Parcel in) {
            return new PromotionTargetMemberTierForDisplay(in);
        }

        @Override
        public PromotionTargetMemberTierForDisplay[] newArray(int size) {
            return new PromotionTargetMemberTierForDisplay[size];
        }
    };
}
