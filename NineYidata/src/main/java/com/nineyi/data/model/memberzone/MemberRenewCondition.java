package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/11/7.
 */
public class MemberRenewCondition implements Parcelable {
    public String Type;
    public String UpgradeType;
    public Double Value;

    public MemberRenewCondition() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Type);
        dest.writeString(this.UpgradeType);
        dest.writeValue(this.Value);
    }

    protected MemberRenewCondition(Parcel in) {
        this.Type = in.readString();
        this.UpgradeType = in.readString();
        this.Value = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<MemberRenewCondition> CREATOR = new Creator<MemberRenewCondition>() {
        @Override
        public MemberRenewCondition createFromParcel(Parcel source) {
            return new MemberRenewCondition(source);
        }

        @Override
        public MemberRenewCondition[] newArray(int size) {
            return new MemberRenewCondition[size];
        }
    };
}
