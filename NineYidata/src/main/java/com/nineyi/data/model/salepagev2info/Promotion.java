
package com.nineyi.data.model.salepagev2info;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.promote.ConditionDiscountTypeEnum;
import com.nineyi.promote.ConditionTypeEnum;

public class Promotion implements Parcelable {

    @SerializedName("PromotionId")
    @Expose
    private Integer promotionId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("StartDateTime")
    @Expose
    private NineyiDate startDateTime;
    @SerializedName("EndDateTime")
    @Expose
    private NineyiDate endDateTime;
    @SerializedName("SalePageList")
    @Expose
    private List<SalePageList> salePageList = null;
    @SerializedName("TypeDef")
    @Expose
    private ConditionTypeEnum typeDef;
    @SerializedName("DiscountTypeDef")
    @Expose
    private ConditionDiscountTypeEnum discountTypeDef;
    @SerializedName("TargetTypeDef")
    @Expose
    private Integer targetTypeDef;
    @SerializedName("UpdatedDateTime")
    @Expose
    private NineyiDate updatedDateTime;
    @SerializedName("PromotionTargetMemberTierList")
    @Expose
    private List<PromotionTargetMemberTierForDisplay> promotionTargetMemberTierList = null;

    protected Promotion(Parcel in) {
        promotionId = in.readByte() == 0x00 ? null : in.readInt();
        name = in.readString();
        startDateTime = (NineyiDate) in.readValue(NineyiDate.class.getClassLoader());
        endDateTime = (NineyiDate) in.readValue(NineyiDate.class.getClassLoader());
        if (in.readByte() == 0x01) {
            salePageList = new ArrayList<SalePageList>();
            in.readList(salePageList, SalePageList.class.getClassLoader());
        } else {
            salePageList = null;
        }
        typeDef = (ConditionTypeEnum) in.readValue(ConditionTypeEnum.class.getClassLoader());
        discountTypeDef = (ConditionDiscountTypeEnum) in.readValue(ConditionDiscountTypeEnum.class.getClassLoader());
        targetTypeDef = in.readByte() == 0x00 ? null : in.readInt();
        updatedDateTime = (NineyiDate) in.readValue(NineyiDate.class.getClassLoader());
        if (in.readByte() == 0x01) {
            promotionTargetMemberTierList = new ArrayList<PromotionTargetMemberTierForDisplay>();
            in.readList(promotionTargetMemberTierList, PromotionTargetMemberTierForDisplay.class.getClassLoader());
        } else {
            promotionTargetMemberTierList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (promotionId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promotionId);
        }
        dest.writeString(name);
        dest.writeValue(startDateTime);
        dest.writeValue(endDateTime);
        if (salePageList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(salePageList);
        }
        dest.writeValue(typeDef);
        dest.writeValue(discountTypeDef);
        if (targetTypeDef == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(targetTypeDef);
        }
        dest.writeValue(updatedDateTime);
        if (promotionTargetMemberTierList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(promotionTargetMemberTierList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Promotion> CREATOR = new Parcelable.Creator<Promotion>() {
        @Override
        public Promotion createFromParcel(Parcel in) {
            return new Promotion(in);
        }

        @Override
        public Promotion[] newArray(int size) {
            return new Promotion[size];
        }
    };

    public Integer getPromotionId() {
        return promotionId;
    }

    public String getName() {
        return name;
    }

    public NineyiDate getStartDateTime() {
        return startDateTime;
    }

    public NineyiDate getEndDateTime() {
        return endDateTime;
    }

    public List<SalePageList> getSalePageList() {
        return salePageList;
    }

    public ConditionTypeEnum getTypeDef() {
        return typeDef;
    }

    public ConditionDiscountTypeEnum getDiscountTypeDef() {
        return discountTypeDef;
    }

    public Integer getTargetTypeDef() {
        return targetTypeDef;
    }

    public NineyiDate getUpdatedDateTime() {
        return updatedDateTime;
    }

    public List<PromotionTargetMemberTierForDisplay> getPromotionTargetMemberTierList() {
        return promotionTargetMemberTierList;
    }
}
