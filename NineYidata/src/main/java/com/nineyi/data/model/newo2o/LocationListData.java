package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import java.util.ArrayList;


public class LocationListData implements Parcelable{

    private static final String FIELD_STORE_SORT = "StoreSort";
    private static final String FIELD_LOCATION_COUNT = "LocationCount";
    private static final String FIELD_LIST = "List";


    @SerializedName(FIELD_STORE_SORT)
    private boolean mStoreSort;
    @SerializedName(FIELD_LOCATION_COUNT)
    private int mLocationCount;
    @SerializedName(FIELD_LIST)
    private ArrayList<LocationListDataList> mLists;


    public LocationListData(){

    }

    public void setStoreSort(boolean storeSort) {
        mStoreSort = storeSort;
    }

    public boolean isStoreSort() {
        return mStoreSort;
    }

    public void setLocationCount(int locationCount) {
        mLocationCount = locationCount;
    }

    public int getLocationCount() {
        return mLocationCount;
    }

    public void setLists(ArrayList<LocationListDataList> lists) {
        mLists = lists;
    }

    public ArrayList<LocationListDataList> getLists() {
        return mLists;
    }

    public LocationListData(Parcel in) {
        mStoreSort = in.readInt() == 1 ? true: false;
        mLocationCount = in.readInt();
        new ArrayList<LocationListDataList>();
        in.readTypedList(mLists, LocationListDataList.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationListData> CREATOR = new Creator<LocationListData>() {
        public LocationListData createFromParcel(Parcel in) {
            return new LocationListData(in);
        }

        public LocationListData[] newArray(int size) {
            return new LocationListData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mStoreSort ? 1 : 0);
        dest.writeInt(mLocationCount);
        dest.writeTypedList(mLists);
    }


}