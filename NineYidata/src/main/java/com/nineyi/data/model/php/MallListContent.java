package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class MallListContent implements Parcelable {

    private static final String FIELD_UUID = "uuid";
    private static final String FIELD_DISPLAY_DATE = "display_date";
    private static final String FIELD_ID = "id";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_INTRODUCTION = "introduction";


    @SerializedName(FIELD_UUID)
    private String mUuid;
    @SerializedName(FIELD_DISPLAY_DATE)
    private String mDisplayDate;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_INTRODUCTION)
    private String mIntroduction;


    public MallListContent() {

    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setDisplayDate(String displayDate) {
        mDisplayDate = displayDate;
    }

    public String getDisplayDate() {
        return mDisplayDate;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setIntroduction(String introduction) {
        mIntroduction = introduction;
    }

    public String getIntroduction() {
        return mIntroduction;
    }

    public MallListContent(Parcel in) {
        mUuid = in.readString();
        mDisplayDate = in.readString();
        mId = in.readInt();
        mTitle = in.readString();
        mIntroduction = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MallListContent> CREATOR = new Creator<MallListContent>() {
        public MallListContent createFromParcel(Parcel in) {
            return new MallListContent(in);
        }

        public MallListContent[] newArray(int size) {
            return new MallListContent[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUuid);
        dest.writeString(mDisplayDate);
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeString(mIntroduction);
    }


}