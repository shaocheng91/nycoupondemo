package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class LocationListForPickup implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private LocationListForPickUpData mLocationListForPickUpData;


    public LocationListForPickup(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setLocationListForPickUpData(LocationListForPickUpData locationListForPickUpData) {
        mLocationListForPickUpData = locationListForPickUpData;
    }

    public LocationListForPickUpData getLocationListForPickUpData() {
        return mLocationListForPickUpData;
    }

    public LocationListForPickup(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mLocationListForPickUpData = in.readParcelable(LocationListForPickUpData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationListForPickup> CREATOR = new Creator<LocationListForPickup>() {
        public LocationListForPickup createFromParcel(Parcel in) {
            return new LocationListForPickup(in);
        }

        public LocationListForPickup[] newArray(int size) {
            return new LocationListForPickup[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        		dest.writeParcelable(mLocationListForPickUpData, flags);
    }


}