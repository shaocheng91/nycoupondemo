package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;


public class VipMemberPostData implements Parcelable {

    private HashMap<String, String> vipMemberInfo;
    private String ShopId;
    private String memberCardId;

    public VipMemberPostData(HashMap<String, String> vipMemberInfo, int shopId, Long memberCardId) {
        this.vipMemberInfo = vipMemberInfo;
        ShopId = String.valueOf(shopId);
        this.memberCardId = String.valueOf(memberCardId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.vipMemberInfo);
        dest.writeString(this.ShopId);
        dest.writeString(this.memberCardId);
    }

    private VipMemberPostData(Parcel in) {
        this.vipMemberInfo = (HashMap<String, String>) in.readSerializable();
        this.ShopId = in.readString();
        this.memberCardId = in.readString();
    }

    public static final Creator<VipMemberPostData> CREATOR = new Creator<VipMemberPostData>() {
        public VipMemberPostData createFromParcel(Parcel source) {
            return new VipMemberPostData(source);
        }

        public VipMemberPostData[] newArray(int size) {
            return new VipMemberPostData[size];
        }
    };
}