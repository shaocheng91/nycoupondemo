package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/8/1.
 */
public class ShopCategoryListBySearch implements Parcelable {
    public String ReturnCode;
    public SearchShopCategoryData Data;
    public String Message;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.Message);
    }

    public ShopCategoryListBySearch() {
    }

    protected ShopCategoryListBySearch(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(SearchShopCategoryData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<ShopCategoryListBySearch> CREATOR = new Parcelable.Creator<ShopCategoryListBySearch>() {
        @Override
        public ShopCategoryListBySearch createFromParcel(Parcel source) {
            return new ShopCategoryListBySearch(source);
        }

        @Override
        public ShopCategoryListBySearch[] newArray(int size) {
            return new ShopCategoryListBySearch[size];
        }
    };
}



