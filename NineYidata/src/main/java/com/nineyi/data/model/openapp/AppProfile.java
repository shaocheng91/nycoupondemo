package com.nineyi.data.model.openapp;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;

/**
 * Created by Willy on 16/1/12.
 */
public class AppProfile implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private AppProfileData mAppProfileData;


    public AppProfile(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setmAppProfileData(AppProfileData appProfileData) {
        mAppProfileData = appProfileData;
    }

    public AppProfileData getAppProfileData() {
        return mAppProfileData;
    }

    public AppProfile(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mAppProfileData = in.readParcelable(AppProfileData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<AppProfile> CREATOR = new Parcelable.Creator<AppProfile>() {
        public AppProfile createFromParcel(Parcel in) {
            return new AppProfile(in);
        }

        public AppProfile[] newArray(int size) {
            return new AppProfile[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        dest.writeParcelable(mAppProfileData, flags);
    }


}
