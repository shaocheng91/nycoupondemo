
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class PayShippingMappingList implements Parcelable {

    @SerializedName("PayProfileTypeDef")
    @Expose
    private String PayProfileTypeDef;
    @SerializedName("ShippingProfileTypeDef")
    @Expose
    private String ShippingProfileTypeDef;
    @SerializedName("StatisticsTypeDef")
    @Expose
    private String StatisticsTypeDef;

    /**
     * 
     * @return
     *     The PayProfileTypeDef
     */
    public String getPayProfileTypeDef() {
        return PayProfileTypeDef;
    }

    /**
     * 
     * @param PayProfileTypeDef
     *     The PayProfileTypeDef
     */
    public void setPayProfileTypeDef(String PayProfileTypeDef) {
        this.PayProfileTypeDef = PayProfileTypeDef;
    }

    /**
     * 
     * @return
     *     The ShippingProfileTypeDef
     */
    public String getShippingProfileTypeDef() {
        return ShippingProfileTypeDef;
    }

    /**
     * 
     * @param ShippingProfileTypeDef
     *     The ShippingProfileTypeDef
     */
    public void setShippingProfileTypeDef(String ShippingProfileTypeDef) {
        this.ShippingProfileTypeDef = ShippingProfileTypeDef;
    }

    /**
     * 
     * @return
     *     The StatisticsTypeDef
     */
    public String getStatisticsTypeDef() {
        return StatisticsTypeDef;
    }

    /**
     * 
     * @param StatisticsTypeDef
     *     The StatisticsTypeDef
     */
    public void setStatisticsTypeDef(String StatisticsTypeDef) {
        this.StatisticsTypeDef = StatisticsTypeDef;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.PayProfileTypeDef);
        dest.writeString(this.ShippingProfileTypeDef);
        dest.writeString(this.StatisticsTypeDef);
    }

    public PayShippingMappingList() {
    }

    protected PayShippingMappingList(Parcel in) {
        this.PayProfileTypeDef = in.readString();
        this.ShippingProfileTypeDef = in.readString();
        this.StatisticsTypeDef = in.readString();
    }

    public static final Parcelable.Creator<PayShippingMappingList> CREATOR
            = new Parcelable.Creator<PayShippingMappingList>() {
        @Override
        public PayShippingMappingList createFromParcel(Parcel source) {
            return new PayShippingMappingList(source);
        }

        @Override
        public PayShippingMappingList[] newArray(int size) {
            return new PayShippingMappingList[size];
        }
    };
}
