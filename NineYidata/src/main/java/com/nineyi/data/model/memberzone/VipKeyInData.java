package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


public class VipKeyInData implements Parcelable {

    private static final String FIELD_MEMBER = "Member";
    private static final String FIELD_CITY_AREA = "CityArea";


    @SerializedName(FIELD_MEMBER)
    private List<VipMemberItemCommon> mMembers;

    @SerializedName(FIELD_CITY_AREA)
    private List<CityArea> mCityAreas;


    public void setMembers(List<VipMemberItemCommon> members) {
        mMembers = members;
    }

    public List<VipMemberItemCommon> getMembers() {
        return mMembers;
    }

    public void setCityAreas(List<CityArea> cityAreas) {
        mCityAreas = cityAreas;
    }

    public List<CityArea> getCityAreas() {
        return mCityAreas;
    }

    public VipKeyInData(Parcel in) {
        in.readTypedList(mMembers, VipMemberItemCommon.CREATOR);
        in.readTypedList(mCityAreas, CityArea.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<VipKeyInData> CREATOR = new Parcelable.Creator<VipKeyInData>() {
        public VipKeyInData createFromParcel(Parcel in) {
            return new VipKeyInData(in);
        }

        public VipKeyInData[] newArray(int size) {
            return new VipKeyInData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mMembers);
        dest.writeTypedList(mCityAreas);
    }


}