package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2015/6/24.
 */
public class PhpCouponUseCode implements Parcelable {

    public String serial_number_origin_type;
    public String serial_number_barcode_type;
    public int serial_number_limit;
    public String family_famiport_button_name;
    public String serial_number1;
    public String serial_number2;
    public String serial_number3;

    public PhpCouponUseCode() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.serial_number_origin_type);
        dest.writeString(this.serial_number_barcode_type);
        dest.writeInt(this.serial_number_limit);
        dest.writeString(this.family_famiport_button_name);
        dest.writeString(this.serial_number1);
        dest.writeString(this.serial_number2);
        dest.writeString(this.serial_number3);
    }

    protected PhpCouponUseCode(Parcel in) {
        this.serial_number_origin_type = in.readString();
        this.serial_number_barcode_type = in.readString();
        this.serial_number_limit = in.readInt();
        this.family_famiport_button_name = in.readString();
        this.serial_number1 = in.readString();
        this.serial_number2 = in.readString();
        this.serial_number3 = in.readString();
    }

    public static final Creator<PhpCouponUseCode> CREATOR = new Creator<PhpCouponUseCode>() {
        public PhpCouponUseCode createFromParcel(Parcel source) {
            return new PhpCouponUseCode(source);
        }

        public PhpCouponUseCode[] newArray(int size) {
            return new PhpCouponUseCode[size];
        }
    };
}
