package com.nineyi.data.model.infomodule;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by kennylee on 2015/8/24.
 */
public abstract class DotNetModel implements Parcelable {

    public String ReturnCode;
    public String Message;

    public DotNetModel(Parcel in) {
        ReturnCode = in.readString();
        Message = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        dest.writeString(Message);
    }
}
