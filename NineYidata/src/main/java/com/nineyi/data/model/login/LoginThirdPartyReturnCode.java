package com.nineyi.data.model.login;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class LoginThirdPartyReturnCode implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private LoginThirdPartyReturnCodeData mData;


    public LoginThirdPartyReturnCode(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(LoginThirdPartyReturnCodeData data) {
        mData = data;
    }

    public LoginThirdPartyReturnCodeData getData() {
        return mData;
    }

    public LoginThirdPartyReturnCode(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mData = in.readParcelable(LoginThirdPartyReturnCodeData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginThirdPartyReturnCode> CREATOR = new Creator<LoginThirdPartyReturnCode>() {
        public LoginThirdPartyReturnCode createFromParcel(Parcel in) {
            return new LoginThirdPartyReturnCode(in);
        }

        public LoginThirdPartyReturnCode[] newArray(int size) {
            return new LoginThirdPartyReturnCode[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        dest.writeParcelable(mData, flags);
    }


}