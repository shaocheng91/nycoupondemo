
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.ecoupon.ECouponCouponDetail;
import com.nineyi.data.model.promotion.v2.PromotionTargetMemberTierList;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartData implements Parcelable {

    public static final String CHECKOUT_TYPE = "CheckoutType";
    public static final String SELECT_CHECKOUT_PAY_TYPE_GROUP = "SelectedCheckoutPayTypeGroup";
    public static final String SALE_PAGE_GROUP_LIST = "SalePageGroupList";
    public static final String SELECTED_CHECKOUT_SHIPPING_TYPE_GROUP = "SelectedCheckoutShippingTypeGroup";
    public static final String SELECTED_ECOUPON_SLAVEID = "SelectedECouponSlaveId";
    public static final String ECOUPON_LIST = "ECouponList";
    public static final String SOLD_OUT_SALE_PAGE_LIST = "SoldoutSalePageList";
    public static final String UNMAPPING_SALE_LIST = "UnMappingCheckoutSalePageList";

    @SerializedName("ShopId")
    @Expose
    private Integer ShopId;
    @SerializedName("ShopName")
    @Expose
    private String ShopName;
    @SerializedName(SOLD_OUT_SALE_PAGE_LIST)
    @Expose
    private List<SalePageList> SoldoutSalePageList = new ArrayList<SalePageList>();
    @SerializedName(UNMAPPING_SALE_LIST)
    @Expose
    private List<SalePageList> UnMappingCheckoutSalePageList = new ArrayList<SalePageList>();
    @SerializedName(SALE_PAGE_GROUP_LIST)
    @Expose
    private List<SalePageGroupList> SalePageGroupList = new ArrayList<SalePageGroupList>();
    @SerializedName(CHECKOUT_TYPE)
    @Expose
    private CheckoutType CheckoutType;
    @SerializedName("InstallmentList")
    @Expose
    private List<InstallmentList> InstallmentList = new ArrayList<InstallmentList>();
    @SerializedName(SELECT_CHECKOUT_PAY_TYPE_GROUP)
    @Expose
    private DisplayPayTypeList SelectedCheckoutPayTypeGroup;
    @SerializedName(SELECTED_CHECKOUT_SHIPPING_TYPE_GROUP)
    @Expose
    private DisplayShippingTypeList SelectedCheckoutShippingTypeGroup;
    @SerializedName("SelectedInstallment")
    @Expose
    private Object SelectedInstallment;
    @SerializedName("PayShippingMappingList")
    @Expose
    private List<PayShippingMappingList> PayShippingMappingList = new ArrayList<PayShippingMappingList>();
    @SerializedName("TotalQty")
    @Expose
    private Integer TotalQty;
    @SerializedName("SalePageCount")
    @Expose
    private Integer SalePageCount;
    @SerializedName("TotalPrice")
    @Expose
    private Double TotalPrice;
    @SerializedName("TotalPayment")
    @Expose
    private Double TotalPayment;
    @SerializedName("TotalFee")
    @Expose
    private Double TotalFee;
    @SerializedName("SubTotalPayment")
    @Expose
    private Double subTotalPayment;

    @SerializedName(ECOUPON_LIST)
    @Expose
    private List<ECouponCouponDetail> mECouponCouponDetailList;

    @SerializedName("HasActiveEcouponCode")
    @Expose
    private Boolean hasActiveEcouponCode;

    @SerializedName("PromotionDiscount")
    @Expose
    private Double promotionDiscount;
    @SerializedName("ECouponDiscount")
    @Expose
    private Double eCouponDiscount;
    @SerializedName("ReachQtyPromotionDiscount")
    @Expose
    private Double reachQtyPromotionDiscount;

    @SerializedName(SELECTED_ECOUPON_SLAVEID)
    @Expose
    private Integer mSelectedECouponSlaveId;
    @SerializedName("PromotionList")
    @Expose
    private List<PromotionList> promotionList = new ArrayList<PromotionList>();

    @SerializedName("ReachQtyPromotionList")
    @Expose
    private List<ReachQtyPromotionList> reachQtyPromotionList;

    @SerializedName("IsEnableECouponSelect")
    @Expose
    private boolean isEnableECouponSelect;

    @SerializedName("HasCrmShopContract")
    @Expose
    private boolean hasCrmShopContract;

    @SerializedName("CrmMemberTier")
    @Expose
    private PromotionTargetMemberTierList crmMemberTier;

    @SerializedName("MemberTierPromotionDiscount")
    @Expose
    private Double memberTierPromotionDiscount;

    @SerializedName("TotalSalePageCount")
    @Expose
    private Integer totalSalePageCount;

    public boolean getEnableECouponSelect() {
        return isEnableECouponSelect;
    }

    public void setEnableECouponSelect(boolean enableECouponSelect) {
        isEnableECouponSelect = enableECouponSelect;
    }

    public List<ReachQtyPromotionList> getReachQtyPromotionList() {
        return reachQtyPromotionList;
    }
    
    public void setSelectedECouponSlaveId(Integer selectedECouponSlaveId) {
        mSelectedECouponSlaveId = selectedECouponSlaveId;
    }

    public void setECouponCouponDetailList(List<ECouponCouponDetail> ECouponCouponDetailList) {
        mECouponCouponDetailList = ECouponCouponDetailList;
    }

    public Integer getSelectedECouponSlaveId() {
        return mSelectedECouponSlaveId;
    }
    /**
     *
     * @return
     *     The ShopId
     */
    public Integer getShopId() {
        return ShopId;
    }

    /**
     *
     * @param ShopId
     *     The ShopId
     */
    public void setShopId(Integer ShopId) {
        this.ShopId = ShopId;
    }

    /**
     *
     * @return
     *     The ShopName
     */
    public String getShopName() {
        return ShopName;
    }

    /**
     *
     * @param ShopName
     *     The ShopName
     */
    public void setShopName(String ShopName) {
        this.ShopName = ShopName;
    }


    public List<ECouponCouponDetail> getECouponCouponDetailList() {
        return mECouponCouponDetailList;
    }
    /**
     *
     * @return
     *     The SoldoutSalePageList
     */
    public List<SalePageList> getSoldoutSalePageList() {
        return SoldoutSalePageList;
    }

    /**
     *
     * @param SoldoutSalePageList
     *     The SoldoutSalePageList
     */
    public void setSoldoutSalePageList(List<SalePageList> SoldoutSalePageList) {
        this.SoldoutSalePageList = SoldoutSalePageList;
    }

    /**
     *
     * @return
     *     The UnMappingCheckoutSalePageList
     */
    public List<SalePageList> getUnMappingCheckoutSalePageList() {
        return UnMappingCheckoutSalePageList;
    }

    /**
     *
     * @param UnMappingCheckoutSalePageList
     *     The UnMappingCheckoutSalePageList
     */
    public void setUnMappingCheckoutSalePageList(List<SalePageList> UnMappingCheckoutSalePageList) {
        this.UnMappingCheckoutSalePageList = UnMappingCheckoutSalePageList;
    }

    /**
     *
     * @return
     *     The SalePageGroupList
     */
    public List<SalePageGroupList> getSalePageGroupList() {
        return SalePageGroupList;
    }

    /**
     *
     * @param SalePageGroupList
     *     The SalePageGroupList
     */
    public void setSalePageGroupList(List<SalePageGroupList> SalePageGroupList) {
        this.SalePageGroupList = SalePageGroupList;
    }

    /**
     *
     * @return
     *     The CheckoutType
     */
    public CheckoutType getCheckoutType() {
        return CheckoutType;
    }

    /**
     *
     * @param CheckoutType
     *     The CheckoutType
     */
    public void setCheckoutType(CheckoutType CheckoutType) {
        this.CheckoutType = CheckoutType;
    }

    /**
     *
     * @return
     *     The InstallmentList
     */
    public List<InstallmentList> getInstallmentList() {
        return InstallmentList;
    }

    /**
     *
     * @param InstallmentList
     *     The InstallmentList
     */
    public void setInstallmentList(List<InstallmentList> InstallmentList) {
        this.InstallmentList = InstallmentList;
    }

    /**
     *
     * @return
     *     The SelectedCheckoutPayTypeGroup
     */
    public DisplayPayTypeList getSelectedCheckoutPayTypeGroup() {
        return SelectedCheckoutPayTypeGroup;
    }

    /**
     *
     * @param SelectedCheckoutPayTypeGroup
     *     The SelectedCheckoutPayTypeGroup
     */
    public void setSelectedCheckoutPayTypeGroup(DisplayPayTypeList SelectedCheckoutPayTypeGroup) {
        this.SelectedCheckoutPayTypeGroup = SelectedCheckoutPayTypeGroup;
    }

    /**
     *
     * @return
     *     The SelectedCheckoutShippingTypeGroup
     */
    public DisplayShippingTypeList getSelectedCheckoutShippingTypeGroup() {
        return SelectedCheckoutShippingTypeGroup;
    }

    /**
     *
     * @param SelectedCheckoutShippingTypeGroup
     *     The SelectedCheckoutShippingTypeGroup
     */
    public void setSelectedCheckoutShippingTypeGroup(DisplayShippingTypeList SelectedCheckoutShippingTypeGroup) {
        this.SelectedCheckoutShippingTypeGroup = SelectedCheckoutShippingTypeGroup;
    }

    /**
     *
     * @return
     *     The SelectedInstallment
     */
    public Object getSelectedInstallment() {
        return SelectedInstallment;
    }

    /**
     *
     * @param SelectedInstallment
     *     The SelectedInstallment
     */
    public void setSelectedInstallment(Object SelectedInstallment) {
        this.SelectedInstallment = SelectedInstallment;
    }

    /**
     *
     * @return
     *     The PayShippingMappingList
     */
    public List<PayShippingMappingList> getPayShippingMappingList() {
        return PayShippingMappingList;
    }

    /**
     *
     * @param PayShippingMappingList
     *     The PayShippingMappingList
     */
    public void setPayShippingMappingList(List<PayShippingMappingList> PayShippingMappingList) {
        this.PayShippingMappingList = PayShippingMappingList;
    }

    /**
     *
     * @return
     *     The TotalQty
     */
    public Integer getTotalQty() {
        return TotalQty;
    }

    /**
     *
     * @param TotalQty
     *     The TotalQty
     */
    public void setTotalQty(Integer TotalQty) {
        this.TotalQty = TotalQty;
    }

    /**
     *
     * @return
     *     The SalePageCount
     */
    public Integer getSalePageCount() {
        return SalePageCount;
    }

    /**
     *
     * @param SalePageCount
     *     The SalePageCount
     */
    public void setSalePageCount(Integer SalePageCount) {
        this.SalePageCount = SalePageCount;
    }

    /**
     *
     * @return
     *     The TotalPrice
     */
    public Double getTotalPrice() {
        return TotalPrice;
    }

    /**
     *
     * @param TotalPrice
     *     The TotalPrice
     */
    public void setTotalPrice(Double TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    /**
     *
     * @return
     *     The TotalPayment
     */
    public Double getTotalPayment() {
        return TotalPayment;
    }

    /**
     *
     * @param TotalPayment
     *     The TotalPayment
     */
    public void setTotalPayment(Double TotalPayment) {
        this.TotalPayment = TotalPayment;
    }

    /**
     *
     * @return
     *     The TotalFee
     */
    public Double getTotalFee() {
        return TotalFee;
    }

    /**
     *
     * @param TotalFee
     *     The TotalFee
     */
    public void setTotalFee(Double TotalFee) {
        this.TotalFee = TotalFee;
    }

    /**
     *
     * @return
     *     The subTotalPayment
     */
    public Double getSubTotalPayment() {
        return subTotalPayment;
    }

    /**
     *
     * @param subTotalPayment
     *     The SubTotalPayment
     */
    public void setSubTotalPayment(Double subTotalPayment) {
        this.subTotalPayment = subTotalPayment;
    }

    /**
     *
     * @return
     *     The hasActiveEcouponCode
     */
    public Boolean getHasActiveEcouponCode() {
        return hasActiveEcouponCode;
    }

    /**
     *
     * @param hasActiveEcouponCode
     *     The HasActiveEcouponCode
     */
    public void setHasActiveEcouponCode(Boolean hasActiveEcouponCode) {
        this.hasActiveEcouponCode = hasActiveEcouponCode;
    }

    /**
     *
     * @return
     *     The promotionDiscount
     */
    public Double getPromotionDiscount() {
        return promotionDiscount;
    }

    /**
     *
     * @param promotionDiscount
     *     The PromotionDiscount
     */
    public void setPromotionDiscount(Double promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    /**
     *
     * @return
     *     The eCouponDiscount
     */
    public Double getECouponDiscount() {
        return eCouponDiscount;
    }

    /**
     *
     * @param eCouponDiscount
     *     The ECouponDiscount
     */
    public void setECouponDiscount(Double eCouponDiscount) {
        this.eCouponDiscount = eCouponDiscount;
    }

    /**
     *
     * @return
     *     The reachQtyPromotionDiscount
     */
    public Double getReachQtyPromotionDiscount() {
        return reachQtyPromotionDiscount;
    }

    /**
     *
     * @param reachQtyPromotionDiscount
     *     The ReachQtyPromotionDiscount
     */
    public void setReachQtyPromotionDiscount(Double reachQtyPromotionDiscount) {
        this.reachQtyPromotionDiscount = reachQtyPromotionDiscount;
    }

    /**
     *
     * @return
     *     The promotionList
     */
    public List<PromotionList> getPromotionList() {
        return promotionList;
    }

    /**
     *
     * @param promotionList
     *     The PromotionList
     */
    public void setPromotionList(List<PromotionList> promotionList) {
        this.promotionList = promotionList;
    }

    public boolean hasCrmShopContract() {
        return hasCrmShopContract;
    }

    public void setHasCrmShopContract(boolean hasCrmShopContract) {
        this.hasCrmShopContract = hasCrmShopContract;
    }


    public PromotionTargetMemberTierList getCrmMemberTier() {
        return crmMemberTier;
    }

    public void setCrmMemberTier(PromotionTargetMemberTierList crmMemberTier) {
        this.crmMemberTier = crmMemberTier;
    }

    public Double getMemberTierPromotionDiscount() {
        return memberTierPromotionDiscount;
    }

    public void setMemberTierPromotionDiscount(Double memberTierPromotionDiscount) {
        this.memberTierPromotionDiscount = memberTierPromotionDiscount;
    }

    public Integer getTotalSalePageCount() {
        return totalSalePageCount;
    }

    public ShoppingCartData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.ShopId);
        dest.writeString(this.ShopName);
        dest.writeTypedList(this.SoldoutSalePageList);
        dest.writeTypedList(this.UnMappingCheckoutSalePageList);
        dest.writeTypedList(this.SalePageGroupList);
        dest.writeParcelable(this.CheckoutType, flags);
        dest.writeTypedList(this.InstallmentList);
        dest.writeParcelable(this.SelectedCheckoutPayTypeGroup, flags);
        dest.writeParcelable(this.SelectedCheckoutShippingTypeGroup, flags);
//        dest.writeParcelable(this.SelectedInstallment, flags);
        dest.writeTypedList(this.PayShippingMappingList);
        dest.writeValue(this.TotalQty);
        dest.writeValue(this.SalePageCount);
        dest.writeValue(this.TotalPrice);
        dest.writeValue(this.TotalPayment);
        dest.writeValue(this.TotalFee);
        dest.writeValue(this.subTotalPayment);
        dest.writeTypedList(this.mECouponCouponDetailList);
        dest.writeValue(this.hasActiveEcouponCode);
        dest.writeValue(this.promotionDiscount);
        dest.writeValue(this.eCouponDiscount);
        dest.writeValue(this.reachQtyPromotionDiscount);
        dest.writeValue(this.mSelectedECouponSlaveId);
        dest.writeTypedList(this.promotionList);
        dest.writeTypedList(this.reachQtyPromotionList);
        dest.writeByte(this.isEnableECouponSelect ? (byte) 1 : (byte) 0);
        dest.writeByte(this.hasCrmShopContract ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.crmMemberTier, flags);
        dest.writeValue(this.memberTierPromotionDiscount);
        dest.writeValue(this.totalSalePageCount);
    }

    protected ShoppingCartData(Parcel in) {
        this.ShopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.ShopName = in.readString();
        this.SoldoutSalePageList = in.createTypedArrayList(SalePageList.CREATOR);
        this.UnMappingCheckoutSalePageList = in.createTypedArrayList(SalePageList.CREATOR);
        this.SalePageGroupList = in.createTypedArrayList(com.nineyi.data.model.shoppingcart.v4.SalePageGroupList.CREATOR);
        this.CheckoutType = in.readParcelable(com.nineyi.data.model.shoppingcart.v4.CheckoutType.class.getClassLoader());
        this.InstallmentList = in.createTypedArrayList(com.nineyi.data.model.shoppingcart.v4.InstallmentList.CREATOR);
        this.SelectedCheckoutPayTypeGroup = in.readParcelable(DisplayPayTypeList.class.getClassLoader());
        this.SelectedCheckoutShippingTypeGroup = in.readParcelable(DisplayShippingTypeList.class.getClassLoader());
//        this.SelectedInstallment = in.readParcelable(Object.class.getClassLoader());
        this.PayShippingMappingList = in.createTypedArrayList(com.nineyi.data.model.shoppingcart.v4.PayShippingMappingList.CREATOR);
        this.TotalQty = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SalePageCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.TotalPrice = (Double) in.readValue(Double.class.getClassLoader());
        this.TotalPayment = (Double) in.readValue(Double.class.getClassLoader());
        this.TotalFee = (Double) in.readValue(Double.class.getClassLoader());
        this.subTotalPayment = (Double) in.readValue(Double.class.getClassLoader());
        this.mECouponCouponDetailList = in.createTypedArrayList(ECouponCouponDetail.CREATOR);
        this.hasActiveEcouponCode = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.promotionDiscount = (Double) in.readValue(Double.class.getClassLoader());
        this.eCouponDiscount = (Double) in.readValue(Double.class.getClassLoader());
        this.reachQtyPromotionDiscount = (Double) in.readValue(Double.class.getClassLoader());
        this.mSelectedECouponSlaveId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.promotionList = in.createTypedArrayList(PromotionList.CREATOR);
        this.reachQtyPromotionList = in.createTypedArrayList(ReachQtyPromotionList.CREATOR);
        this.isEnableECouponSelect = in.readByte() != 0;
        this.hasCrmShopContract = in.readByte() != 0;
        this.crmMemberTier = in.readParcelable(PromotionTargetMemberTierList.class.getClassLoader());
        this.memberTierPromotionDiscount = (Double) in.readValue(Double.class.getClassLoader());
        this.totalSalePageCount = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<ShoppingCartData> CREATOR = new Creator<ShoppingCartData>() {
        @Override
        public ShoppingCartData createFromParcel(Parcel source) {
            return new ShoppingCartData(source);
        }

        @Override
        public ShoppingCartData[] newArray(int size) {
            return new ShoppingCartData[size];
        }
    };
}
