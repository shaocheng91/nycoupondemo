package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/7/27.
 */
public class PayAndShippingList implements Parcelable {
    public ArrayList<PayAndShippingType> PayTypeList;
    public ArrayList<PayAndShippingType> ShippingTypeList;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.PayTypeList);
        dest.writeList(this.ShippingTypeList);
    }

    public PayAndShippingList() {
    }

    protected PayAndShippingList(Parcel in) {
        this.PayTypeList = new ArrayList<PayAndShippingType>();
        in.readList(this.PayTypeList, PayAndShippingType.class.getClassLoader());
        this.ShippingTypeList = new ArrayList<PayAndShippingType>();
        in.readList(this.ShippingTypeList, PayAndShippingType.class.getClassLoader());
    }

    public static final Parcelable.Creator<PayAndShippingList> CREATOR = new Parcelable.Creator<PayAndShippingList>() {
        @Override
        public PayAndShippingList createFromParcel(Parcel source) {
            return new PayAndShippingList(source);
        }

        @Override
        public PayAndShippingList[] newArray(int size) {
            return new PayAndShippingList[size];
        }
    };
}
