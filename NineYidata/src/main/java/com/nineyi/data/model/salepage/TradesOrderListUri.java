package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by tedliang on 15/5/27.
 */
public class TradesOrderListUri implements Parcelable {
    public String Domain;
    public String Path;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Domain);
        dest.writeString(this.Path);
    }

    public TradesOrderListUri() {
    }

    private TradesOrderListUri(Parcel in) {
        this.Domain = in.readString();
        this.Path = in.readString();
    }

    public static final Parcelable.Creator<TradesOrderListUri> CREATOR = new Parcelable.Creator<TradesOrderListUri>() {
        public TradesOrderListUri createFromParcel(Parcel source) {
            return new TradesOrderListUri(source);
        }

        public TradesOrderListUri[] newArray(int size) {
            return new TradesOrderListUri[size];
        }
    };
}
