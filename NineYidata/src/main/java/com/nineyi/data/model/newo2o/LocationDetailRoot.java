package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class LocationDetailRoot implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private LocationListDataList mData;


    public LocationDetailRoot(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(LocationListDataList data) {
        mData = data;
    }

    public LocationListDataList getDatum() {
        return mData;
    }

    public LocationDetailRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mData = in.readParcelable(LocationListDataList.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationDetailRoot> CREATOR = new Creator<LocationDetailRoot>() {
        public LocationDetailRoot createFromParcel(Parcel in) {
            return new LocationDetailRoot(in);
        }

        public LocationDetailRoot[] newArray(int size) {
            return new LocationDetailRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        		dest.writeParcelable(mData, flags);
    }


}