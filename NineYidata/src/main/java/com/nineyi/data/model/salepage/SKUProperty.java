package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SKUProperty implements Parcelable {

    public String Name;
    public String PropertySet;
    public String PropertyNameSet;

    public SKUProperty() {
    }

    // Parcelable management
    private SKUProperty(Parcel in) {
        Name = in.readString();
        PropertySet = in.readString();
        PropertyNameSet = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
        dest.writeString(PropertySet);
        dest.writeString(PropertyNameSet);
    }

    public static final Parcelable.Creator<SKUProperty> CREATOR = new Parcelable.Creator<SKUProperty>() {
        public SKUProperty createFromParcel(Parcel in) {
            return new SKUProperty(in);
        }

        public SKUProperty[] newArray(int size) {
            return new SKUProperty[size];
        }
    };
}
