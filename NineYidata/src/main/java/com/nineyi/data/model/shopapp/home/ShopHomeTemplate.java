package com.nineyi.data.model.shopapp.home;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by ReeceCheng on 2017/4/11.
 */
public class ShopHomeTemplate implements Parcelable {
    public ArrayList<ShopHomeTemplateComponent> ComponentList;

    public ShopHomeTemplate() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.ComponentList);
    }

    protected ShopHomeTemplate(Parcel in) {
        this.ComponentList = in.createTypedArrayList(ShopHomeTemplateComponent.CREATOR);
    }

    public static final Creator<ShopHomeTemplate> CREATOR = new Creator<ShopHomeTemplate>() {
        @Override
        public ShopHomeTemplate createFromParcel(Parcel source) {
            return new ShopHomeTemplate(source);
        }

        @Override
        public ShopHomeTemplate[] newArray(int size) {
            return new ShopHomeTemplate[size];
        }
    };
}
