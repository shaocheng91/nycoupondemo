package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2017/11/6.
 */

public class NyBarCode implements Parcelable {

    public String BarCode;
    public String BarCodeTypeDef;
    public long BarCodeTimeStamp;

    public NyBarCode() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.BarCode);
        dest.writeString(this.BarCodeTypeDef);
        dest.writeLong(this.BarCodeTimeStamp);
    }

    protected NyBarCode(Parcel in) {
        this.BarCode = in.readString();
        this.BarCodeTypeDef = in.readString();
        this.BarCodeTimeStamp = in.readLong();
    }

    public static final Creator<NyBarCode> CREATOR = new Creator<NyBarCode>() {
        @Override
        public NyBarCode createFromParcel(Parcel source) {
            return new NyBarCode(source);
        }

        @Override
        public NyBarCode[] newArray(int size) {
            return new NyBarCode[size];
        }
    };
}
