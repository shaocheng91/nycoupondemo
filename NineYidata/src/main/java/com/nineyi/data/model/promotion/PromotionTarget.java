package com.nineyi.data.model.promotion;

import android.os.Parcel;
import android.os.Parcelable;


public final class PromotionTarget implements Parcelable {

    public String TargetType;
    public int TargetId;

    public PromotionTarget() {
    }

    // Parcelable management
    private PromotionTarget(Parcel in) {
        TargetType = in.readString();
        TargetId = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(TargetType);
        dest.writeInt(TargetId);
    }

    public static final Parcelable.Creator<PromotionTarget> CREATOR = new Parcelable.Creator<PromotionTarget>() {
        public PromotionTarget createFromParcel(Parcel in) {
            return new PromotionTarget(in);
        }

        public PromotionTarget[] newArray(int size) {
            return new PromotionTarget[size];
        }
    };
}
