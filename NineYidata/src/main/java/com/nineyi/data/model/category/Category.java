package com.nineyi.data.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by kelsey on 2017/1/9.
 */

public class Category implements Parcelable{
    @SerializedName(value = "CategoryId", alternate = {"Id", "ShopCategory_Id"})
    private int CategoryId;
    @SerializedName(value = "Name", alternate = {"Title", "ShopCategory_Name"})
    private String Name;
    @SerializedName(value = "Sort", alternate = {"ShopCategory_Sort"})
    private int Sort;
    @SerializedName(value = "Count", alternate = {"SalePageCount"})
    private int Count;
    @SerializedName(value = "ChildList", alternate = {"SubShopCategories"})
    private ArrayList<Category> ChildList;
    @SerializedName(value = "IsParent", alternate = {"ShopCategory_IsParent"})
    private boolean IsParent;

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getSort() {
        return Sort;
    }

    public void setSort(int sort) {
        Sort = sort;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public ArrayList<Category> getChildList() {
        return ChildList;
    }

    public void setChildList(ArrayList<Category> childList) {
        ChildList = childList;
    }

    public boolean isParent() {
        return IsParent;
    }

    public void setParent(boolean parent) {
        IsParent = parent;
    }

    public Category() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.CategoryId);
        dest.writeString(this.Name);
        dest.writeInt(this.Sort);
        dest.writeInt(this.Count);
        dest.writeTypedList(this.ChildList);
        dest.writeByte(this.IsParent ? (byte) 1 : (byte) 0);
    }

    protected Category(Parcel in) {
        this.CategoryId = in.readInt();
        this.Name = in.readString();
        this.Sort = in.readInt();
        this.Count = in.readInt();
        this.ChildList = in.createTypedArrayList(Category.CREATOR);
        this.IsParent = in.readByte() != 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
