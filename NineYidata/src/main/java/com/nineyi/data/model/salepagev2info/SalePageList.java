
package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.gson.NineyiDate;

public class SalePageList implements Parcelable {

    @SerializedName("SalePageId")
    @Expose
    private Integer salePageId;
    @SerializedName("SalePageImageUrl")
    @Expose
    private String salePageImageUrl;
    @SerializedName("PromotionId")
    @Expose
    private Integer promotionId;
    @SerializedName("PromotionHotSaleRankingUpdatedDateTime")
    @Expose
    private NineyiDate promotionHotSaleRankingUpdatedDateTime;
    @SerializedName("Rank")
    @Expose
    private Integer rank;

    protected SalePageList(Parcel in) {
        salePageId = in.readByte() == 0x00 ? null : in.readInt();
        salePageImageUrl = in.readString();
        promotionId = in.readByte() == 0x00 ? null : in.readInt();
        promotionHotSaleRankingUpdatedDateTime = (NineyiDate) in.readValue(NineyiDate.class.getClassLoader());
        rank = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (salePageId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(salePageId);
        }
        dest.writeString(salePageImageUrl);
        if (promotionId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promotionId);
        }
        dest.writeValue(promotionHotSaleRankingUpdatedDateTime);
        if (rank == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(rank);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SalePageList> CREATOR = new Parcelable.Creator<SalePageList>() {
        @Override
        public SalePageList createFromParcel(Parcel in) {
            return new SalePageList(in);
        }

        @Override
        public SalePageList[] newArray(int size) {
            return new SalePageList[size];
        }
    };
}
