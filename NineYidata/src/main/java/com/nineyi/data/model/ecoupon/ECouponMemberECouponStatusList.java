package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;


public final class ECouponMemberECouponStatusList implements Parcelable {

    public long ECouponId;
    public boolean HasNormalCoupon;
    public String CouponCode;
    public boolean IsUsing;

    public ECouponMemberECouponStatusList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.ECouponId);
        dest.writeByte(this.HasNormalCoupon ? (byte) 1 : (byte) 0);
        dest.writeString(this.CouponCode);
        dest.writeByte(this.IsUsing ? (byte) 1 : (byte) 0);
    }

    protected ECouponMemberECouponStatusList(Parcel in) {
        this.ECouponId = in.readLong();
        this.HasNormalCoupon = in.readByte() != 0;
        this.CouponCode = in.readString();
        this.IsUsing = in.readByte() != 0;
    }

    public static final Creator<ECouponMemberECouponStatusList> CREATOR
            = new Creator<ECouponMemberECouponStatusList>() {
        @Override
        public ECouponMemberECouponStatusList createFromParcel(Parcel source) {
            return new ECouponMemberECouponStatusList(source);
        }

        @Override
        public ECouponMemberECouponStatusList[] newArray(int size) {
            return new ECouponMemberECouponStatusList[size];
        }
    };
}
