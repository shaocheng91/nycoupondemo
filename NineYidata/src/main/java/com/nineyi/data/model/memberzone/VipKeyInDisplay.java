package com.nineyi.data.model.memberzone;

import java.util.List;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;

import java.util.ArrayList;


public class VipKeyInDisplay implements Parcelable {

    private static final String FIELD_DROP_DOWN_ENTITIES = "DropDownEntities";
    private static final String FIELD_NOTE = "Note";
    private static final String FIELD_COLUMN_NAME = "ColumnName";


    @SerializedName(FIELD_DROP_DOWN_ENTITIES)
    private List<VipKeyInDropDownEntity> mDropDownEntities;
    @SerializedName(FIELD_NOTE)
    private String mNote;
    @SerializedName(FIELD_COLUMN_NAME)
    private String mColumnName;


    public VipKeyInDisplay() {

    }

    public void setDropDownEntities(List<VipKeyInDropDownEntity> dropDownEntities) {
        mDropDownEntities = dropDownEntities;
    }

    public List<VipKeyInDropDownEntity> getDropDownEntities() {
        return mDropDownEntities;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getNote() {
        return mNote;
    }

    public void setColumnName(String columnName) {
        mColumnName = columnName;
    }

    public String getColumnName() {
        return mColumnName;
    }

    public VipKeyInDisplay(Parcel in) {
        new ArrayList<VipKeyInDropDownEntity>();
        in.readTypedList(mDropDownEntities, VipKeyInDropDownEntity.CREATOR);
        mNote = in.readString();
        mColumnName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipKeyInDisplay> CREATOR = new Creator<VipKeyInDisplay>() {
        public VipKeyInDisplay createFromParcel(Parcel in) {
            return new VipKeyInDisplay(in);
        }

        public VipKeyInDisplay[] newArray(int size) {
            return new VipKeyInDisplay[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mDropDownEntities);
        dest.writeString(mNote);
        dest.writeString(mColumnName);
    }


}