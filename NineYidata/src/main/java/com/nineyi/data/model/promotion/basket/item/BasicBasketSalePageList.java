
package com.nineyi.data.model.promotion.basket.item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class BasicBasketSalePageList implements Parcelable {

    @SerializedName("SalePageId")
    @Expose
    private long salePageId;
    @SerializedName("SalePageImageUrl")
    @Expose
    private String salePageImageUrl;
    @SerializedName("SkuProperty")
    @Expose
    private String skuProperty;
    @SerializedName("SaleProductSKUId")
    @Expose
    private long saleProductSKUId;
    @SerializedName("Qty")
    @Expose
    private int qty;
    @SerializedName("Price")
    @Expose
    private double price;

    /**
     * 
     * @return
     *     The salePageId
     */
    public long getSalePageId() {
        return salePageId;
    }

    /**
     * 
     * @param salePageId
     *     The SalePageId
     */
    public void setSalePageId(long salePageId) {
        this.salePageId = salePageId;
    }

    /**
     * 
     * @return
     *     The salePageImageUrl
     */
    public String getSalePageImageUrl() {
        return salePageImageUrl;
    }

    /**
     * 
     * @param salePageImageUrl
     *     The SalePageImageUrl
     */
    public void setSalePageImageUrl(String salePageImageUrl) {
        this.salePageImageUrl = salePageImageUrl;
    }

    /**
     * 
     * @return
     *     The skuProperty
     */
    public String getSkuProperty() {
        return skuProperty;
    }

    /**
     * 
     * @param skuProperty
     *     The SkuProperty
     */
    public void setSkuProperty(String skuProperty) {
        this.skuProperty = skuProperty;
    }

    /**
     * 
     * @return
     *     The saleProductSKUId
     */
    public long getSaleProductSKUId() {
        return saleProductSKUId;
    }

    /**
     * 
     * @param saleProductSKUId
     *     The SaleProductSKUId
     */
    public void setSaleProductSKUId(long saleProductSKUId) {
        this.saleProductSKUId = saleProductSKUId;
    }

    /**
     * 
     * @return
     *     The qty
     */
    public int getQty() {
        return qty;
    }

    /**
     * 
     * @param qty
     *     The Qty
     */
    public void setQty(int qty) {
        this.qty = qty;
    }

    /**
     * 
     * @return
     *     The price
     */
    public double getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The Price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    public BasicBasketSalePageList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.salePageId);
        dest.writeString(this.salePageImageUrl);
        dest.writeString(this.skuProperty);
        dest.writeLong(this.saleProductSKUId);
        dest.writeInt(this.qty);
        dest.writeDouble(this.price);
    }

    protected BasicBasketSalePageList(Parcel in) {
        this.salePageId = in.readLong();
        this.salePageImageUrl = in.readString();
        this.skuProperty = in.readString();
        this.saleProductSKUId = in.readLong();
        this.qty = in.readInt();
        this.price = in.readDouble();
    }

    public static final Creator<BasicBasketSalePageList> CREATOR = new Creator<BasicBasketSalePageList>() {
        @Override
        public BasicBasketSalePageList createFromParcel(Parcel source) {
            return new BasicBasketSalePageList(source);
        }

        @Override
        public BasicBasketSalePageList[] newArray(int size) {
            return new BasicBasketSalePageList[size];
        }
    };
}
