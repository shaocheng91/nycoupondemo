package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBAttachment implements Parcelable {

    FBSubPhoto subattachments;

    public FBSubPhoto getSubAttachment() {
        return subattachments;
    }

    protected FBAttachment(Parcel in) {
        subattachments = (FBSubPhoto) in.readValue(FBSubPhoto.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(subattachments);
    }

    public static final Parcelable.Creator<FBAttachment> CREATOR = new Parcelable.Creator<FBAttachment>() {
        @Override
        public FBAttachment createFromParcel(Parcel in) {
            return new FBAttachment(in);
        }

        @Override
        public FBAttachment[] newArray(int size) {
            return new FBAttachment[size];
        }
    };
}