package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShoppingCartShopShort implements Parcelable {

    public int ShopId;
    public String ShopName;
    public int TotalQty;
    public int TotalPrice;
    public ArrayList<ShoppingCartSalePageShort> SalePageList;
    public ArrayList<ShoppingCartPromotion> Promotion;

    public ShoppingCartShopShort() {
    }

    // Parcelable management
    private ShoppingCartShopShort(Parcel in) {
        ShopId = in.readInt();
        ShopName = in.readString();
        TotalQty = in.readInt();
        TotalPrice = in.readInt();
        SalePageList = in.createTypedArrayList(ShoppingCartSalePageShort.CREATOR);
        Promotion = in.createTypedArrayList(ShoppingCartPromotion.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopId);
        dest.writeString(ShopName);
        dest.writeInt(TotalQty);
        dest.writeInt(TotalPrice);
        dest.writeTypedList(SalePageList);
        dest.writeTypedList(Promotion);
    }

    public static final Parcelable.Creator<ShoppingCartShopShort> CREATOR
            = new Parcelable.Creator<ShoppingCartShopShort>() {
        public ShoppingCartShopShort createFromParcel(Parcel in) {
            return new ShoppingCartShopShort(in);
        }

        public ShoppingCartShopShort[] newArray(int size) {
            return new ShoppingCartShopShort[size];
        }
    };
}
