
package com.nineyi.data.model.notify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class EmailNotificationData implements Parcelable {

    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("IsEnableEDM")
    @Expose
    private Boolean isEnableEDM;
    @SerializedName("IsEnableTradesOrder")
    @Expose
    private Boolean isEnableTradesOrder;
    @SerializedName("IsEnablePriceReduction")
    @Expose
    private Boolean isEnablePriceReduction;
    @SerializedName("IsEnableECouponExpire")
    @Expose
    private Boolean isEnableECouponExpire;
    @SerializedName("IsEnableCustomerServerReply")
    @Expose
    private Boolean isEnableCustomerServerReply;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsEnableEDM() {
        return isEnableEDM;
    }

    public void setIsEnableEDM(Boolean isEnableEDM) {
        this.isEnableEDM = isEnableEDM;
    }

    public Boolean getIsEnableTradesOrder() {
        return isEnableTradesOrder;
    }

    public void setIsEnableTradesOrder(Boolean isEnableTradesOrder) {
        this.isEnableTradesOrder = isEnableTradesOrder;
    }

    public Boolean getIsEnablePriceReduction() {
        return isEnablePriceReduction;
    }

    public void setIsEnablePriceReduction(Boolean isEnablePriceReduction) {
        this.isEnablePriceReduction = isEnablePriceReduction;
    }

    public Boolean getIsEnableECouponExpire() {
        return isEnableECouponExpire;
    }

    public void setIsEnableECouponExpire(Boolean isEnableECouponExpire) {
        this.isEnableECouponExpire = isEnableECouponExpire;
    }

    public Boolean getIsEnableCustomerServerReply() {
        return isEnableCustomerServerReply;
    }

    public void setIsEnableCustomerServerReply(Boolean isEnableCustomerServerReply) {
        this.isEnableCustomerServerReply = isEnableCustomerServerReply;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeValue(this.isEnableEDM);
        dest.writeValue(this.isEnableTradesOrder);
        dest.writeValue(this.isEnablePriceReduction);
        dest.writeValue(this.isEnableECouponExpire);
        dest.writeValue(this.isEnableCustomerServerReply);
    }

    public EmailNotificationData() {
    }

    protected EmailNotificationData(Parcel in) {
        this.email = in.readString();
        this.isEnableEDM = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isEnableTradesOrder = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isEnablePriceReduction = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isEnableECouponExpire = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isEnableCustomerServerReply = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<EmailNotificationData> CREATOR
            = new Parcelable.Creator<EmailNotificationData>() {
        @Override
        public EmailNotificationData createFromParcel(Parcel source) {
            return new EmailNotificationData(source);
        }

        @Override
        public EmailNotificationData[] newArray(int size) {
            return new EmailNotificationData[size];
        }
    };
}
