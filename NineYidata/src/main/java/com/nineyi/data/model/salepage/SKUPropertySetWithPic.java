package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SKUPropertySetWithPic extends SKUPropertySet implements Parcelable {

    public String PicUrl;

    public SKUPropertySetWithPic() {
    }

    // Parcelable management
    private SKUPropertySetWithPic(Parcel in) {
        super(in);

        PicUrl = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(PicUrl);
    }

    public static final Parcelable.Creator<SKUPropertySetWithPic> CREATOR
            = new Parcelable.Creator<SKUPropertySetWithPic>() {
        public SKUPropertySetWithPic createFromParcel(Parcel in) {
            return new SKUPropertySetWithPic(in);
        }

        public SKUPropertySetWithPic[] newArray(int size) {
            return new SKUPropertySetWithPic[size];
        }
    };
}
