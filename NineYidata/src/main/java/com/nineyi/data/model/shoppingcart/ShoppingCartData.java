package com.nineyi.data.model.shoppingcart;

import com.nineyi.data.model.ecoupon.ECouponCouponDetail;
import com.nineyi.data.model.installment.InstallmentShopInstallmentList;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShoppingCartData implements Parcelable {

    public ArrayList<ShoppingCartSalePageV3> SalePageList;
    public ArrayList<ShoppingCartSalePageV3> SoldoutSalePageList;
    public ArrayList<ShoppingCartPayType> PayTypeList;
    public ArrayList<ShoppingCartDeliveryType> DeliveryTypeList;
    public ArrayList<ShoppingCartPromotion> PromotionList;
    public ArrayList<ShoppingCartReachQty> ReachQtyPromotionList;
    public ArrayList<ECouponCouponDetail> ECouponList;
    public ArrayList<ShoppingCartCheckoutTypeV3> CheckoutTypeList;
    public ArrayList<ShoppingCartFreeGiftPromotion> FreeGiftPromotionList;
    public boolean HasActiveEcouponCode;
    public String EcoupontTitle;
    public ArrayList<InstallmentShopInstallmentList> InstallmentList;
    public double TotalPrice;
    public int TotalQty;
    public double TotalPayment;
    public double TotalPriceWithReachQtyDiscount;
    public int ECouponDiscount;
    public int PromotionDiscount;
    public int TotalPromotionDiscount;
    public int TotalDiscount;
    public double ShippingFee;
    public int SelectedShopShippingTypeId;
    public String SelectedShopShippingTypeTypeDef;
    public String SelectedPayType;
    public int SelectedECouponSlaveId;
    public int SelectedInstallmentId;
    public ShoppingCartCheckoutTypeV3 SelectedCheckoutType;
    public boolean IsSelected;
    public int ShopId;
    public String ShopName;
    public String TradesOrderCode;
    public int ShopInstallmentLowestLimitAmt;
    public String TrackAppVersion;
    public int TrackSourceTypeDef;
    public int TrackChannelTypeDef;
    public int TrackDeviceTypeDef;
    public int MemberShopId;
    public int TotalFreeGiftCount;
    public boolean HasLowTemperatureDeliverySaleProduct;
    public String SupplierStoreProfileMasterCode;
    public String SupplierStoreProfileStoreSlaveCode;
    public ArrayList<ShoppingCartSupplierStoreProfile> SupplierStoreProfileList;
    public boolean HasStorePaymentLimit;
    public ShoppingCartSignatureInformation SignatureInformation;

    public ShoppingCartData() {
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(SalePageList);
        dest.writeTypedList(SoldoutSalePageList);
        dest.writeTypedList(PayTypeList);
        dest.writeTypedList(DeliveryTypeList);
        dest.writeTypedList(PromotionList);
        dest.writeTypedList(ReachQtyPromotionList);
        dest.writeTypedList(ECouponList);
        dest.writeTypedList(CheckoutTypeList);
        dest.writeTypedList(FreeGiftPromotionList);
        dest.writeByte(HasActiveEcouponCode ? (byte) 1 : (byte) 0);
        dest.writeString(this.EcoupontTitle);
        dest.writeTypedList(InstallmentList);
        dest.writeDouble(this.TotalPrice);
        dest.writeInt(this.TotalQty);
        dest.writeDouble(this.TotalPayment);
        dest.writeDouble(this.TotalPriceWithReachQtyDiscount);
        dest.writeInt(this.ECouponDiscount);
        dest.writeInt(this.PromotionDiscount);
        dest.writeInt(this.TotalPromotionDiscount);
        dest.writeInt(this.TotalDiscount);
        dest.writeDouble(this.ShippingFee);
        dest.writeInt(this.SelectedShopShippingTypeId);
        dest.writeString(this.SelectedShopShippingTypeTypeDef);
        dest.writeString(this.SelectedPayType);
        dest.writeInt(this.SelectedECouponSlaveId);
        dest.writeInt(this.SelectedInstallmentId);
        dest.writeParcelable(this.SelectedCheckoutType, 0);
        dest.writeByte(IsSelected ? (byte) 1 : (byte) 0);
        dest.writeInt(this.ShopId);
        dest.writeString(this.ShopName);
        dest.writeString(this.TradesOrderCode);
        dest.writeInt(this.ShopInstallmentLowestLimitAmt);
        dest.writeString(this.TrackAppVersion);
        dest.writeInt(this.TrackSourceTypeDef);
        dest.writeInt(this.TrackChannelTypeDef);
        dest.writeInt(this.TrackDeviceTypeDef);
        dest.writeInt(this.MemberShopId);
        dest.writeInt(this.TotalFreeGiftCount);
        dest.writeInt(this.HasLowTemperatureDeliverySaleProduct ? 1 : 0);
        dest.writeString(this.SupplierStoreProfileMasterCode);
        dest.writeString(this.SupplierStoreProfileStoreSlaveCode);
        dest.writeTypedList(this.SupplierStoreProfileList);
        dest.writeByte(HasStorePaymentLimit ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.SignatureInformation, 0);
    }

    protected ShoppingCartData(Parcel in) {
        this.SalePageList = in.createTypedArrayList(ShoppingCartSalePageV3.CREATOR);
        this.SoldoutSalePageList = in.createTypedArrayList(ShoppingCartSalePageV3.CREATOR);
        this.PayTypeList = in.createTypedArrayList(ShoppingCartPayType.CREATOR);
        this.DeliveryTypeList = in.createTypedArrayList(ShoppingCartDeliveryType.CREATOR);
        this.PromotionList = in.createTypedArrayList(ShoppingCartPromotion.CREATOR);
        this.ReachQtyPromotionList = in.createTypedArrayList(ShoppingCartReachQty.CREATOR);
        this.ECouponList = in.createTypedArrayList(ECouponCouponDetail.CREATOR);
        this.CheckoutTypeList = in.createTypedArrayList(ShoppingCartCheckoutTypeV3.CREATOR);
        this.FreeGiftPromotionList = in.createTypedArrayList(ShoppingCartFreeGiftPromotion.CREATOR);
        this.HasActiveEcouponCode = in.readByte() != 0;
        this.EcoupontTitle = in.readString();
        this.InstallmentList = in.createTypedArrayList(InstallmentShopInstallmentList.CREATOR);
        this.TotalPrice = in.readDouble();
        this.TotalQty = in.readInt();
        this.TotalPayment = in.readDouble();
        this.TotalPriceWithReachQtyDiscount = in.readDouble();
        this.ECouponDiscount = in.readInt();
        this.PromotionDiscount = in.readInt();
        this.TotalPromotionDiscount = in.readInt();
        this.TotalDiscount = in.readInt();
        this.ShippingFee = in.readDouble();
        this.SelectedShopShippingTypeId = in.readInt();
        this.SelectedShopShippingTypeTypeDef = in.readString();
        this.SelectedPayType = in.readString();
        this.SelectedECouponSlaveId = in.readInt();
        this.SelectedInstallmentId = in.readInt();
        this.SelectedCheckoutType = in.readParcelable(ShoppingCartCheckoutTypeV3.class.getClassLoader());
        this.IsSelected = in.readByte() != 0;
        this.ShopId = in.readInt();
        this.ShopName = in.readString();
        this.TradesOrderCode = in.readString();
        this.ShopInstallmentLowestLimitAmt = in.readInt();
        this.TrackAppVersion = in.readString();
        this.TrackSourceTypeDef = in.readInt();
        this.TrackChannelTypeDef = in.readInt();
        this.TrackDeviceTypeDef = in.readInt();
        this.MemberShopId = in.readInt();
        this.TotalFreeGiftCount = in.readInt();
        this.HasLowTemperatureDeliverySaleProduct = in.readInt() == 1;
        this.SupplierStoreProfileMasterCode = in.readString();
        this.SupplierStoreProfileStoreSlaveCode = in.readString();
        this.SupplierStoreProfileList = in.createTypedArrayList(ShoppingCartSupplierStoreProfile.CREATOR);
        this.HasStorePaymentLimit = in.readByte() != 0;
        this.SignatureInformation = in.readParcelable(ShoppingCartSignatureInformation.class.getClassLoader());
    }

    public static final Creator<ShoppingCartData> CREATOR = new Creator<ShoppingCartData>() {
        public ShoppingCartData createFromParcel(Parcel source) {
            return new ShoppingCartData(source);
        }

        public ShoppingCartData[] newArray(int size) {
            return new ShoppingCartData[size];
        }
    };
}
