package com.nineyi.data.model.shoppingcart.v4;

/**
 * Created by tedliang on 2016/5/24.
 */
public enum StatisticsTypeDef {
    StorePay,
    CreditCardInstallment,
    CreditCardOnce,
    ATM,
    CashOnDelivery
}
