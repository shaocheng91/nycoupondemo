package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class VipMemberData implements Parcelable {

    private static final String FIELD_VIP_MEMBER_INFO = "VipMemberInfo";
    private static final String FIELD_VIP_SHOP_MEMBER_CARD = "VipShopMemberCard";
    private static final String FIELD_TRADES_INFO = "TradesInfo";
    private static final String FIELD_VIP_MEMBER = "VipMember";
    private static final String FIELD_VIP_MEMBER_CUSTOM_RULE_LIST = "VipMemberCustomRuleList";
    private static final String FIELD_VIP_MEMBER_CHANNEL_LIST = "VipMemberChannelList";
    private static final String FIELD_MEMBER_CODE = "MemberCode";


    @SerializedName(FIELD_VIP_MEMBER_INFO)
    private VipMemberInfo mVipMemberInfo;
    @SerializedName(FIELD_VIP_SHOP_MEMBER_CARD)
    private VipShopMemberCard mVipShopMemberCard;
    @SerializedName(FIELD_TRADES_INFO)
    private TradesInfo mTradesInfo;
    @SerializedName(FIELD_VIP_MEMBER)
    private VipMember mVipMember;
    @SerializedName(FIELD_VIP_MEMBER_CUSTOM_RULE_LIST)
    public ArrayList<VipMemberCustomRuleList> mVipMemberCustomRuleList;
    @SerializedName(FIELD_VIP_MEMBER_CHANNEL_LIST)
    public ArrayList<VipMemberChannelList> mVipMemberChannelList;

    public String getMemberCode() {
        return mMemberCode;
    }

    @SerializedName(FIELD_MEMBER_CODE)
    public String mMemberCode;

    public VipMemberData() {

    }

    public void setVipMemberInfo(VipMemberInfo vipMemberInfo) {
        mVipMemberInfo = vipMemberInfo;
    }

    public VipMemberInfo getVipMemberInfo() {
        return mVipMemberInfo;
    }

    public void setVipShopMemberCard(VipShopMemberCard vipShopMemberCard) {
        mVipShopMemberCard = vipShopMemberCard;
    }

    public VipShopMemberCard getVipShopMemberCard() {
        return mVipShopMemberCard;
    }

    public void setTradesInfo(TradesInfo tradesInfo) {
        mTradesInfo = tradesInfo;
    }

    public TradesInfo getTradesInfo() {
        return mTradesInfo;
    }

    public void setVipMember(VipMember vipMember) {
        mVipMember = vipMember;
    }

    public VipMember getVipMember() {
        return mVipMember;
    }

    public VipMemberData(Parcel in) {
        mVipMemberInfo = in.readParcelable(VipMemberInfo.class.getClassLoader());
        mVipShopMemberCard = in.readParcelable(VipShopMemberCard.class.getClassLoader());
        mTradesInfo = in.readParcelable(TradesInfo.class.getClassLoader());
        mVipMember = in.readParcelable(VipMember.class.getClassLoader());
        mVipMemberCustomRuleList = in.createTypedArrayList(VipMemberCustomRuleList.CREATOR);
        mVipMemberChannelList = in.createTypedArrayList(VipMemberChannelList.CREATOR);
        mMemberCode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipMemberData> CREATOR = new Creator<VipMemberData>() {
        public VipMemberData createFromParcel(Parcel in) {
            return new VipMemberData(in);
        }

        public VipMemberData[] newArray(int size) {
            return new VipMemberData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mVipMemberInfo, flags);
        dest.writeParcelable(mVipShopMemberCard, flags);
        dest.writeParcelable(mTradesInfo, flags);
        dest.writeParcelable(mVipMember, flags);
        dest.writeTypedList(mVipMemberCustomRuleList);
        dest.writeTypedList(mVipMemberChannelList);
        dest.writeString(mMemberCode);
    }


}