package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBCommnetUser implements Parcelable {

    String id;
    FBCommentFrom from;
    String message;
    String can_remove;
    String created_time;
    String like_count;
    String user_likes;

    protected FBCommnetUser(Parcel in) {
        id = in.readString();
        from = (FBCommentFrom) in.readValue(FBCommentFrom.class.getClassLoader());
        message = in.readString();
        can_remove = in.readString();
        created_time = in.readString();
        like_count = in.readString();
        user_likes = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeValue(from);
        dest.writeString(message);
        dest.writeString(can_remove);
        dest.writeString(created_time);
        dest.writeString(like_count);
        dest.writeString(user_likes);
    }

    public static final Parcelable.Creator<FBCommnetUser> CREATOR = new Parcelable.Creator<FBCommnetUser>() {
        @Override
        public FBCommnetUser createFromParcel(Parcel in) {
            return new FBCommnetUser(in);
        }

        @Override
        public FBCommnetUser[] newArray(int size) {
            return new FBCommnetUser[size];
        }
    };
}