package com.nineyi.data.model.mallapp;

import android.os.Parcel;
import android.os.Parcelable;


public final class PromoteApp implements Parcelable {

    public int ShopId;
    public String AppName;
    public int CategoryId;
    public String CategoryName;
    public String IconURL;
    public String OSType;
    public String PackageName;
    public String APPId;

    public PromoteApp() {
    }

    // Parcelable management
    private PromoteApp(Parcel in) {
        ShopId = in.readInt();
        AppName = in.readString();
        CategoryId = in.readInt();
        CategoryName = in.readString();
        IconURL = in.readString();
        OSType = in.readString();
        PackageName = in.readString();
        APPId = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopId);
        dest.writeString(AppName);
        dest.writeInt(CategoryId);
        dest.writeString(CategoryName);
        dest.writeString(IconURL);
        dest.writeString(OSType);
        dest.writeString(PackageName);
        dest.writeString(APPId);
    }

    public static final Parcelable.Creator<PromoteApp> CREATOR = new Parcelable.Creator<PromoteApp>() {
        public PromoteApp createFromParcel(Parcel in) {
            return new PromoteApp(in);
        }

        public PromoteApp[] newArray(int size) {
            return new PromoteApp[size];
        }
    };
}
