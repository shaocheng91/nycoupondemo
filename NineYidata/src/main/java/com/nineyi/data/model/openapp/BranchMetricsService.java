package com.nineyi.data.model.openapp;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;

/**
 * Created by Willy on 16/1/12.
 */
public class BranchMetricsService implements Parcelable{

    private static final String FIELD_IS_ENABLED = "IsEnabled";


    @SerializedName(FIELD_IS_ENABLED)
    private boolean mIsEnabled;


    public BranchMetricsService(){

    }

    public void setIsEnabled(boolean isEnabled) {
        mIsEnabled = isEnabled;
    }

    public boolean isIsEnabled() {
        return mIsEnabled;
    }

    public BranchMetricsService(Parcel in) {
        mIsEnabled = in.readInt() == 1 ? true: false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<BranchMetricsService> CREATOR = new Parcelable.Creator<BranchMetricsService>() {
        public BranchMetricsService createFromParcel(Parcel in) {
            return new BranchMetricsService(in);
        }

        public BranchMetricsService[] newArray(int size) {
            return new BranchMetricsService[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mIsEnabled ? 1 : 0);
    }


}
