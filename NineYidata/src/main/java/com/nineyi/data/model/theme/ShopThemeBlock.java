package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeBlock implements Parcelable {
    public int Sort;
    public boolean Enable;
    public ShopThemeComponent Component;

    public ShopThemeBlock() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Sort);
        dest.writeByte(this.Enable ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.Component, flags);
    }

    protected ShopThemeBlock(Parcel in) {
        this.Sort = in.readInt();
        this.Enable = in.readByte() != 0;
        this.Component = in.readParcelable(ShopThemeComponent.class.getClassLoader());
    }

    public static final Creator<ShopThemeBlock> CREATOR = new Creator<ShopThemeBlock>() {
        @Override
        public ShopThemeBlock createFromParcel(Parcel source) {
            return new ShopThemeBlock(source);
        }

        @Override
        public ShopThemeBlock[] newArray(int size) {
            return new ShopThemeBlock[size];
        }
    };
}
