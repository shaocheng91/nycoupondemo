package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/11/7.
 */
public class MemberUpgradeCondition implements Parcelable {
    public String Type;
    public int Value;
    public String UpgradeType;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Type);
        dest.writeInt(this.Value);
        dest.writeString(this.UpgradeType);
    }

    public MemberUpgradeCondition() {
    }

    protected MemberUpgradeCondition(Parcel in) {
        this.Type = in.readString();
        this.Value = in.readInt();
        this.UpgradeType = in.readString();
    }

    public static final Parcelable.Creator<MemberUpgradeCondition> CREATOR
            = new Parcelable.Creator<MemberUpgradeCondition>() {
        @Override
        public MemberUpgradeCondition createFromParcel(Parcel source) {
            return new MemberUpgradeCondition(source);
        }

        @Override
        public MemberUpgradeCondition[] newArray(int size) {
            return new MemberUpgradeCondition[size];
        }
    };
}
