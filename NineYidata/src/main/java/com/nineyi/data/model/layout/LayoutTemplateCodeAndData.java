package com.nineyi.data.model.layout;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class LayoutTemplateCodeAndData implements Parcelable {

    public String Code;
    public ArrayList<LayoutTemplateData> Data;

    public LayoutTemplateCodeAndData() {
    }

    // Parcelable management
    private LayoutTemplateCodeAndData(Parcel in) {
        Code = in.readString();
        Data = in.createTypedArrayList(LayoutTemplateData.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Code);
        dest.writeTypedList(Data);
    }

    public static final Parcelable.Creator<LayoutTemplateCodeAndData> CREATOR
            = new Parcelable.Creator<LayoutTemplateCodeAndData>() {
        public LayoutTemplateCodeAndData createFromParcel(Parcel in) {
            return new LayoutTemplateCodeAndData(in);
        }

        public LayoutTemplateCodeAndData[] newArray(int size) {
            return new LayoutTemplateCodeAndData[size];
        }
    };
}
