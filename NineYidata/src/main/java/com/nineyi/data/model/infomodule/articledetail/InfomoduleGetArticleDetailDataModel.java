package com.nineyi.data.model.infomodule.articledetail;

import com.nineyi.data.model.infomodule.InfomoduleCommonDetailDataItemListModel;
import com.nineyi.data.model.infomodule.InfomoduleCommonDetailDataModel;
import com.nineyi.data.model.infomodule.InfomoduleCommonShopSimpleInfoModel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleGetArticleDetailDataModel extends InfomoduleCommonDetailDataModel {

    public int Id;
    public String Type;
    public String Title;
    public String Subtitle;
    public String Uuid;
    public String ImageUrl;
    public String PublishedDate;
    public String Introduction;
    public ArrayList<InfomoduleCommonDetailDataItemListModel> ItemList;
    public InfomoduleCommonShopSimpleInfoModel Shop;

    public InfomoduleGetArticleDetailDataModel(Parcel in) {
        super(in);
        Id = in.readInt();
        Type = in.readString();
        Title = in.readString();
        Subtitle = in.readString();
        Uuid = in.readString();
        ImageUrl = in.readString();
        PublishedDate = in.readString();
        Introduction = in.readString();
        ItemList = in.createTypedArrayList(InfomoduleCommonDetailDataItemListModel.CREATOR);
        Shop = InfomoduleCommonShopSimpleInfoModel.CREATOR.createFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(Id);
        dest.writeString(Type);
        dest.writeString(Title);
        dest.writeString(Subtitle);
        dest.writeString(Uuid);
        dest.writeString(ImageUrl);
        dest.writeString(PublishedDate);
        dest.writeString(Introduction);
        dest.writeTypedList(ItemList);
        dest.writeParcelable(Shop, flags);
    }

    public static Parcelable.Creator<InfomoduleGetArticleDetailDataModel> CREATOR
            = new Parcelable.Creator<InfomoduleGetArticleDetailDataModel>() {
        @Override
        public InfomoduleGetArticleDetailDataModel createFromParcel(Parcel source) {
            return new InfomoduleGetArticleDetailDataModel(source);
        }

        @Override
        public InfomoduleGetArticleDetailDataModel[] newArray(int size) {
            return new InfomoduleGetArticleDetailDataModel[size];
        }
    };
}
