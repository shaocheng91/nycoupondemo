package com.nineyi.data.model.ecoupon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ECouponMemberList implements Parcelable {
    public static final String ECOUPONLIST = "ECouponList";
    @SerializedName(ECOUPONLIST)
    @Expose
    public ArrayList<ECouponCouponInfo> ECouponList;

    public ECouponMemberList() {
    }

    // Parcelable management
    private ECouponMemberList(Parcel in) {
        ECouponList = in.createTypedArrayList(ECouponCouponInfo.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ECouponList);
    }

    public static final Parcelable.Creator<ECouponMemberList> CREATOR = new Parcelable.Creator<ECouponMemberList>() {
        public ECouponMemberList createFromParcel(Parcel in) {
            return new ECouponMemberList(in);
        }

        public ECouponMemberList[] newArray(int size) {
            return new ECouponMemberList[size];
        }
    };
}
