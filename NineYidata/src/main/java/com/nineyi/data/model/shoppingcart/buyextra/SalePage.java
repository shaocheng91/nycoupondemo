package com.nineyi.data.model.shoppingcart.buyextra;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/**
 * Created by shaocheng on 2017/9/14.
 */

public class SalePage {
    private Integer Id;
    private Integer ShopId;
    private String Title;
    private Double Price;
    private String SellingStartDateTime;
    private String PicUrl;
    private Double SuggestPrice;
    private String UpdatedDateTime;

    private SalePage() {
    }

    private SalePage(Builder builder) {
        Id = builder.Id;
        ShopId = builder.ShopId;
        Title = builder.Title;
        Price = builder.Price;
        SellingStartDateTime = builder.SellingStartDateTime;
        PicUrl = builder.PicUrl;
        SuggestPrice = builder.SuggestPrice;
        UpdatedDateTime = builder.UpdatedDateTime;
    }

    public Integer getId() {
        return Id;
    }

    public Integer getShopId() {
        return ShopId;
    }

    public String getTitle() {
        return Title;
    }

    public Double getPrice() {
        return Price;
    }

    public String getSellingStartDateTime() {
        return SellingStartDateTime;
    }

    public String getPicUrl() {
        return PicUrl;
    }

    public Double getSuggestPrice() {
        return SuggestPrice;
    }

    public String getUpdatedDateTime() {
        return UpdatedDateTime;
    }

    public final static Creator<SalePage> CREATOR = new Creator<SalePage>() {

        @SuppressWarnings({
                "unchecked"
        })
        public SalePage createFromParcel(Parcel in) {
            SalePage instance = new SalePage();
            instance.Id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.ShopId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.Title = ((String) in.readValue((String.class.getClassLoader())));
            instance.Price = ((Double) in.readValue((Double.class.getClassLoader())));
            instance.SellingStartDateTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.PicUrl = ((String) in.readValue((String.class.getClassLoader())));
            instance.SuggestPrice = ((Double) in.readValue((Double.class.getClassLoader())));
            instance.UpdatedDateTime = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SalePage[] newArray(int size) {
            return (new SalePage[size]);
        }

    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(Id);
        dest.writeValue(ShopId);
        dest.writeValue(Title);
        dest.writeValue(Price);
        dest.writeValue(SellingStartDateTime);
        dest.writeValue(PicUrl);
        dest.writeValue(SuggestPrice);
        dest.writeValue(UpdatedDateTime);
    }

    public int describeContents() {
        return 0;
    }

    public static final class Builder {
        private Integer Id;
        private Integer ShopId;
        private String Title;
        private Double Price;
        private String SellingStartDateTime;
        private String PicUrl;
        private Double SuggestPrice;
        private String UpdatedDateTime;

        public Builder() {
        }

        public Builder Id(Integer val) {
            Id = val;
            return this;
        }

        public Builder ShopId(Integer val) {
            ShopId = val;
            return this;
        }

        public Builder Title(String val) {
            Title = val;
            return this;
        }

        public Builder Price(Double val) {
            Price = val;
            return this;
        }

        public Builder SellingStartDateTime(String val) {
            SellingStartDateTime = val;
            return this;
        }

        public Builder PicUrl(String val) {
            PicUrl = val;
            return this;
        }

        public Builder SuggestPrice(Double val) {
            SuggestPrice = val;
            return this;
        }

        public Builder UpdatedDateTime(String val) {
            UpdatedDateTime = val;
            return this;
        }

        public SalePage build() {
            return new SalePage(this);
        }
    }
}
