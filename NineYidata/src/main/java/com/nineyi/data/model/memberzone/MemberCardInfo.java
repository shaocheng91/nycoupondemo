package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by ReeceCheng on 2016/11/7.
 */
public class MemberCardInfo implements Parcelable {
    public int Id;
    public String Name;
    public int Level;
    public String ImagePath;
    public ArrayList<MemberUpgradeCondition> UpgradeConditionList;
    public ArrayList<MemberRenewCondition> RenewConditionList;
    public int ExpireDay;
    public String Description;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.Name);
        dest.writeInt(this.Level);
        dest.writeString(this.ImagePath);
        dest.writeTypedList(this.UpgradeConditionList);
        dest.writeTypedList(this.RenewConditionList);
        dest.writeInt(this.ExpireDay);
        dest.writeString(this.Description);
    }

    public MemberCardInfo() {
    }

    protected MemberCardInfo(Parcel in) {
        this.Id = in.readInt();
        this.Name = in.readString();
        this.Level = in.readInt();
        this.ImagePath = in.readString();
        this.UpgradeConditionList = in.createTypedArrayList(MemberUpgradeCondition.CREATOR);
        this.RenewConditionList = in.createTypedArrayList(MemberRenewCondition.CREATOR);
        this.ExpireDay = in.readInt();
        this.Description = in.readString();
    }

    public static final Parcelable.Creator<MemberCardInfo> CREATOR = new Parcelable.Creator<MemberCardInfo>() {
        @Override
        public MemberCardInfo createFromParcel(Parcel source) {
            return new MemberCardInfo(source);
        }

        @Override
        public MemberCardInfo[] newArray(int size) {
            return new MemberCardInfo[size];
        }
    };
}
