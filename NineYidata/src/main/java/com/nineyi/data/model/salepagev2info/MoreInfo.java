
package com.nineyi.data.model.salepagev2info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoreInfo {

    @SerializedName("SalePage_Id")
    @Expose
    private String salePageId;
    @SerializedName("SaleProduct_Id")
    @Expose
    private Integer saleProductId;
    @SerializedName("SaleProduct_Title")
    @Expose
    private String saleProductTitle;
    @SerializedName("SaleProductDesc_Content")
    @Expose
    private String saleProductDescContent;

    public String getSalePageId() {
        return salePageId;
    }

    public void setSalePageId(String salePageId) {
        this.salePageId = salePageId;
    }

    public Integer getSaleProductId() {
        return saleProductId;
    }

    public void setSaleProductId(Integer saleProductId) {
        this.saleProductId = saleProductId;
    }

    public String getSaleProductTitle() {
        return saleProductTitle;
    }

    public void setSaleProductTitle(String saleProductTitle) {
        this.saleProductTitle = saleProductTitle;
    }

    public String getSaleProductDescContent() {
        return saleProductDescContent;
    }

    public void setSaleProductDescContent(String saleProductDescContent) {
        this.saleProductDescContent = saleProductDescContent;
    }

}
