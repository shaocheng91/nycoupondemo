package com.nineyi.data.model.fanpage;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


public class FBData implements Parcelable {

    private String id;
    private FBFrom from;
    private String message;
    private String picture;
    private String link;
    private String source;
    private String name;
    private String caption;
    private String description;
    private String icon;
    private List<FBAction> actions;
    private FBPrivacy privacy;
    private String type;
    private String status_type;
    private String object_id;
    private String created_time;
    private String updated_time;
    private FBShareCount shares;
    private FBLikes likes;
    private FBComments comments;
    private FBPlace place;
    private ArrayList<FBChildAttachment> child_attachments;

    public String getCaption() {
        return caption;
    }

    public FBPlace getPlace() {
        return place;
    }

    public String getId() {
        return id;
    }

    public FBFrom getFrom() {
        return from;
    }

    public String getMessage() {
        return message;
    }

    public String getPicture() {
        return picture;
    }

    public String getLink() {
        return link;
    }

    public String getSource() {
        return source;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public List<FBAction> getActions() {
        return actions;
    }

    public FBPrivacy getPrivacy() {
        return privacy;
    }

    public String getType() {
        return type;
    }

    public String getStatusType() {
        return status_type;
    }

    public String getObjectId() {
        return object_id;
    }

    public String getCreatedTime() {
        return created_time;
    }

    public String getUpdatedTime() {
        return updated_time;
    }

    public FBShareCount getSahres() {
        return shares;
    }

    public FBLikes getLikes() {
        return likes;
    }

    public FBComments getComments() {
        return comments;
    }

    public ArrayList<FBChildAttachment> getChildAttachments() {
        return child_attachments;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.from, flags);
        dest.writeString(this.message);
        dest.writeString(this.picture);
        dest.writeString(this.link);
        dest.writeString(this.source);
        dest.writeString(this.name);
        dest.writeString(this.caption);
        dest.writeString(this.description);
        dest.writeString(this.icon);
        dest.writeTypedList(actions);
        dest.writeParcelable(this.privacy, flags);
        dest.writeString(this.type);
        dest.writeString(this.status_type);
        dest.writeString(this.object_id);
        dest.writeString(this.created_time);
        dest.writeString(this.updated_time);
        dest.writeParcelable(this.shares, flags);
        dest.writeParcelable(this.likes, flags);
        dest.writeParcelable(this.comments, flags);
        dest.writeParcelable(this.place, flags);
        dest.writeTypedList(child_attachments);
    }

    public FBData() {
    }

    protected FBData(Parcel in) {
        this.id = in.readString();
        this.from = in.readParcelable(FBFrom.class.getClassLoader());
        this.message = in.readString();
        this.picture = in.readString();
        this.link = in.readString();
        this.source = in.readString();
        this.name = in.readString();
        this.caption = in.readString();
        this.description = in.readString();
        this.icon = in.readString();
        this.actions = in.createTypedArrayList(FBAction.CREATOR);
        this.privacy = in.readParcelable(FBPrivacy.class.getClassLoader());
        this.type = in.readString();
        this.status_type = in.readString();
        this.object_id = in.readString();
        this.created_time = in.readString();
        this.updated_time = in.readString();
        this.shares = in.readParcelable(FBShareCount.class.getClassLoader());
        this.likes = in.readParcelable(FBLikes.class.getClassLoader());
        this.comments = in.readParcelable(FBComments.class.getClassLoader());
        this.place = in.readParcelable(FBPlace.class.getClassLoader());
        this.child_attachments = in.createTypedArrayList(FBChildAttachment.CREATOR);
    }

    public static final Creator<FBData> CREATOR = new Creator<FBData>() {
        @Override
        public FBData createFromParcel(Parcel source) {
            return new FBData(source);
        }

        @Override
        public FBData[] newArray(int size) {
            return new FBData[size];
        }
    };
}










