package com.nineyi.data.model.o2o;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class LocationListData implements Parcelable{

    private static final String FIELD_STORE_SORT = "StoreSort";
    private static final String FIELD_LIST = "List";


    @SerializedName(FIELD_STORE_SORT)
    private boolean mStoreSort;
    @SerializedName(FIELD_LIST)
    private ArrayList<LocationListItem> mLists;


    public LocationListData(){

    }

    public void setStoreSort(boolean storeSort) {
        mStoreSort = storeSort;
    }

    public boolean isStoreSort() {
        return mStoreSort;
    }

    public void setLists(ArrayList<LocationListItem> lists) {
        mLists = lists;
    }

    public ArrayList<LocationListItem> getLists() {
        return mLists;
    }

    public LocationListData(Parcel in) {
        mStoreSort = in.readInt() == 1;
        in.readTypedList(mLists, LocationListItem.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationListData> CREATOR = new Creator<LocationListData>() {
        public LocationListData createFromParcel(Parcel in) {
            return new LocationListData(in);
        }

        public LocationListData[] newArray(int size) {
            return new LocationListData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mStoreSort ? 1 : 0);
        dest.writeTypedList(mLists);
    }
}