
package com.nineyi.data.model.notify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class EmailNotification implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private EmailNotificationData data;
    @SerializedName("Messagae")
    @Expose
    private String messagae;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public EmailNotificationData getData() {
        return data;
    }

    public void setData(EmailNotificationData data) {
        this.data = data;
    }

    public String getMessagae() {
        return messagae;
    }

    public void setMessagae(String messagae) {
        this.messagae = messagae;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.messagae);
    }

    public EmailNotification() {
    }

    protected EmailNotification(Parcel in) {
        this.returnCode = in.readString();
        this.data = in.readParcelable(EmailNotificationData.class.getClassLoader());
        this.messagae = in.readString();
    }

    public static final Parcelable.Creator<EmailNotification> CREATOR = new Parcelable.Creator<EmailNotification>() {
        @Override
        public EmailNotification createFromParcel(Parcel source) {
            return new EmailNotification(source);
        }

        @Override
        public EmailNotification[] newArray(int size) {
            return new EmailNotification[size];
        }
    };
}
