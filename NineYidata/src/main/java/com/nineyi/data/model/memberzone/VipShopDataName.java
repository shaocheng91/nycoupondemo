package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


public class VipShopDataName implements Parcelable {

    private String LocationBindingButtonName;


    public VipShopDataName() {

    }

    public VipShopDataName(Parcel in) {
        LocationBindingButtonName = in.readString();
    }

    public String getLocationBindingButtonName() {
        return LocationBindingButtonName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipShopDataName> CREATOR = new Creator<VipShopDataName>() {
        public VipShopDataName createFromParcel(Parcel in) {
            return new VipShopDataName(in);
        }

        public VipShopDataName[] newArray(int size) {
            return new VipShopDataName[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(LocationBindingButtonName);
    }

}