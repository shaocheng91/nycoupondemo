package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2015/12/11.
 */
public class ShoppingCartSalePageGift implements Parcelable {
    public int SalePageId;
    public int SalePageGiftId;
    public int SalePageGiftSlaveId;
    public int SaleProductSKUId;
    public String Title;
    public int Qty;
    public int SellingQty;

    public ShoppingCartSalePageGift() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.SalePageId);
        dest.writeInt(this.SalePageGiftId);
        dest.writeInt(this.SalePageGiftSlaveId);
        dest.writeInt(this.SaleProductSKUId);
        dest.writeString(this.Title);
        dest.writeInt(this.Qty);
        dest.writeInt(this.SellingQty);
    }

    protected ShoppingCartSalePageGift(Parcel in) {
        this.SalePageId = in.readInt();
        this.SalePageGiftId = in.readInt();
        this.SalePageGiftSlaveId = in.readInt();
        this.SaleProductSKUId = in.readInt();
        this.Title = in.readString();
        this.Qty = in.readInt();
        this.SellingQty = in.readInt();
    }

    public static final Creator<ShoppingCartSalePageGift> CREATOR = new Creator<ShoppingCartSalePageGift>() {
        public ShoppingCartSalePageGift createFromParcel(Parcel source) {
            return new ShoppingCartSalePageGift(source);
        }

        public ShoppingCartSalePageGift[] newArray(int size) {
            return new ShoppingCartSalePageGift[size];
        }
    };
}
