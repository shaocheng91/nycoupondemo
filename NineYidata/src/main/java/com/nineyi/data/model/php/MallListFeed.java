package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class MallListFeed implements Parcelable {

    private static final String FIELD_PUBLISH_ID = "publish_id";
    private static final String FIELD_COUNT = "count";
    private static final String FIELD_LATEST = "latest";


    @SerializedName(FIELD_PUBLISH_ID)
    private int mPublishId;
    @SerializedName(FIELD_COUNT)
    private int mCount;
    @SerializedName(FIELD_LATEST)
    private List<PhpCouponItem> mMallListLatests;


    public MallListFeed() {

    }

    public void setPublishId(int publishId) {
        mPublishId = publishId;
    }

    public int getPublishId() {
        return mPublishId;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public int getCount() {
        return mCount;
    }

    public void setLatests(List<PhpCouponItem> mallListLatests) {
        mMallListLatests = mallListLatests;
    }

    public List<PhpCouponItem> getLatests() {
        return mMallListLatests;
    }

    public MallListFeed(Parcel in) {
        mPublishId = in.readInt();
        mCount = in.readInt();
        new ArrayList<PhpCouponItem>();
        in.readTypedList(mMallListLatests, PhpCouponItem.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MallListFeed> CREATOR = new Creator<MallListFeed>() {
        public MallListFeed createFromParcel(Parcel in) {
            return new MallListFeed(in);
        }

        public MallListFeed[] newArray(int size) {
            return new MallListFeed[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mPublishId);
        dest.writeInt(mCount);
        dest.writeTypedList(mMallListLatests);
    }


}