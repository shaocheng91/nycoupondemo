package com.nineyi.data.model.shoppingcart.v4;

/**
 * Created by tedliang on 2016/5/23.
 */
public enum  TemperatureTypeDef {
    Normal,
    Freezer,     //冷凍
    Refrigerator, //冷藏
    None
}
