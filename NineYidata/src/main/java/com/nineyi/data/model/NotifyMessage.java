package com.nineyi.data.model;

import com.nineyi.data.model.gson.NineyiDate;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;


public final class NotifyMessage implements Parcelable {

    public String IconUrl;
    public NineyiDate DateTime;
    public String Title;
    public String Content;
    public String Type;
    public String TargetUrl;
    public String TargetType;
    public int TargetId;
    public String CustomField1;
    public String CustomField2;
    public String TraceFR;
    public HashMap<String, String> Cbd;

    public NotifyMessage() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.IconUrl);
        dest.writeParcelable(this.DateTime, flags);
        dest.writeString(this.Title);
        dest.writeString(this.Content);
        dest.writeString(this.Type);
        dest.writeString(this.TargetUrl);
        dest.writeString(this.TargetType);
        dest.writeInt(this.TargetId);
        dest.writeString(this.CustomField1);
        dest.writeString(this.CustomField2);
        dest.writeString(this.TraceFR);
        dest.writeSerializable(this.Cbd);
    }

    protected NotifyMessage(Parcel in) {
        this.IconUrl = in.readString();
        this.DateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.Title = in.readString();
        this.Content = in.readString();
        this.Type = in.readString();
        this.TargetUrl = in.readString();
        this.TargetType = in.readString();
        this.TargetId = in.readInt();
        this.CustomField1 = in.readString();
        this.CustomField2 = in.readString();
        this.TraceFR = in.readString();
        this.Cbd = (HashMap<String, String>) in.readSerializable();
    }

    public static final Creator<NotifyMessage> CREATOR = new Creator<NotifyMessage>() {
        @Override
        public NotifyMessage createFromParcel(Parcel source) {
            return new NotifyMessage(source);
        }

        @Override
        public NotifyMessage[] newArray(int size) {
            return new NotifyMessage[size];
        }
    };
}
