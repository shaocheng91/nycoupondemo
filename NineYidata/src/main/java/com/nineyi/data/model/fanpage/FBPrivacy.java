package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBPrivacy implements Parcelable {

    String value;

    protected FBPrivacy(Parcel in) {
        value = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
    }

    public static final Parcelable.Creator<FBPrivacy> CREATOR = new Parcelable.Creator<FBPrivacy>() {
        @Override
        public FBPrivacy createFromParcel(Parcel in) {
            return new FBPrivacy(in);
        }

        @Override
        public FBPrivacy[] newArray(int size) {
            return new FBPrivacy[size];
        }
    };
}