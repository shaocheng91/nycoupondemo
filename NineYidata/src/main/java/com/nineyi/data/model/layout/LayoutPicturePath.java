package com.nineyi.data.model.layout;

import android.os.Parcel;
import android.os.Parcelable;


public final class LayoutPicturePath implements Parcelable {

    public String FullUrl;
    public String RootUrl;
    public String ImagePath;
    public String LastUpdatedTime;

    public LayoutPicturePath() {
    }

    // Parcelable management
    private LayoutPicturePath(Parcel in) {
        FullUrl = in.readString();
        RootUrl = in.readString();
        ImagePath = in.readString();
        LastUpdatedTime = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(FullUrl);
        dest.writeString(RootUrl);
        dest.writeString(ImagePath);
        dest.writeString(LastUpdatedTime);
    }

    public static final Parcelable.Creator<LayoutPicturePath> CREATOR = new Parcelable.Creator<LayoutPicturePath>() {
        public LayoutPicturePath createFromParcel(Parcel in) {
            return new LayoutPicturePath(in);
        }

        public LayoutPicturePath[] newArray(int size) {
            return new LayoutPicturePath[size];
        }
    };
}
