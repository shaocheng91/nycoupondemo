
package com.nineyi.data.model.shopintroduction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ShopInformationData implements Parcelable {

    @SerializedName("ShopIntroduceData")
    @Expose
    private ShopIntroduceData shopIntroduceData;
    @SerializedName("ShopShoppingNoticeData")
    @Expose
    private ShopShoppingNoticeData shopShoppingNoticeData;
    @SerializedName("ShopCustomerServiceData")
    @Expose
    private ShopCustomerServiceData shopCustomerServiceData;

    public ShopIntroduceData getShopIntroduceData() {
        return shopIntroduceData;
    }

    public void setShopIntroduceData(ShopIntroduceData shopIntroduceData) {
        this.shopIntroduceData = shopIntroduceData;
    }

    public ShopShoppingNoticeData getShopShoppingNoticeData() {
        return shopShoppingNoticeData;
    }

    public void setShopShoppingNoticeData(ShopShoppingNoticeData shopShoppingNoticeData) {
        this.shopShoppingNoticeData = shopShoppingNoticeData;
    }

    public ShopCustomerServiceData getShopCustomerServiceData() {
        return shopCustomerServiceData;
    }

    public void setShopCustomerServiceData(ShopCustomerServiceData shopCustomerServiceData) {
        this.shopCustomerServiceData = shopCustomerServiceData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.shopIntroduceData, flags);
        dest.writeParcelable(this.shopShoppingNoticeData, flags);
        dest.writeParcelable(this.shopCustomerServiceData, flags);
    }

    public ShopInformationData() {
    }

    protected ShopInformationData(Parcel in) {
        this.shopIntroduceData = in.readParcelable(ShopIntroduceData.class.getClassLoader());
        this.shopShoppingNoticeData = in.readParcelable(ShopShoppingNoticeData.class.getClassLoader());
        this.shopCustomerServiceData = in.readParcelable(ShopCustomerServiceData.class.getClassLoader());
    }

    public static final Parcelable.Creator<ShopInformationData> CREATOR
            = new Parcelable.Creator<ShopInformationData>() {
        @Override
        public ShopInformationData createFromParcel(Parcel source) {
            return new ShopInformationData(source);
        }

        @Override
        public ShopInformationData[] newArray(int size) {
            return new ShopInformationData[size];
        }
    };
}
