package com.nineyi.data.model.infomodule.articledetail;

import com.nineyi.data.model.infomodule.DotNetModel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleGetArticleDetailModel extends DotNetModel {

    public InfomoduleGetArticleDetailDataModel Data;

    public InfomoduleGetArticleDetailModel(Parcel in) {
        super(in);
        Data = InfomoduleGetArticleDetailDataModel.CREATOR.createFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(Data, flags);
    }

    public static Parcelable.Creator<InfomoduleGetArticleDetailModel> CREATOR
            = new Parcelable.Creator<InfomoduleGetArticleDetailModel>() {
        @Override
        public InfomoduleGetArticleDetailModel createFromParcel(Parcel source) {
            return new InfomoduleGetArticleDetailModel(source);
        }

        @Override
        public InfomoduleGetArticleDetailModel[] newArray(int size) {
            return new InfomoduleGetArticleDetailModel[size];
        }
    };
}
