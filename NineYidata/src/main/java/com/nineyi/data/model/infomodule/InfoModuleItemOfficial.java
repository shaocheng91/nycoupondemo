package com.nineyi.data.model.infomodule;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by toby on 2015/3/12.
 */
public class InfoModuleItemOfficial implements Parcelable {

    public int ModuleId;

    public int InfoModuleType;

    public String InfoModuleTypeDesc;

    public String MainPicFileName;

    public String MainPicURL;

    public String DetailLink;

    public String ModuleTitle;

    public String ClipLink;

    public String YouTubeVideoId;

    public int Order;

    protected InfoModuleItemOfficial(Parcel in) {
        ModuleId = in.readInt();
        InfoModuleType = in.readInt();
        InfoModuleTypeDesc = in.readString();
        MainPicFileName = in.readString();
        MainPicURL = in.readString();
        DetailLink = in.readString();
        ModuleTitle = in.readString();
        ClipLink = in.readString();
        YouTubeVideoId = in.readString();
        Order = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ModuleId);
        dest.writeInt(InfoModuleType);
        dest.writeString(InfoModuleTypeDesc);
        dest.writeString(MainPicFileName);
        dest.writeString(MainPicURL);
        dest.writeString(DetailLink);
        dest.writeString(ModuleTitle);
        dest.writeString(ClipLink);
        dest.writeString(YouTubeVideoId);
        dest.writeInt(Order);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<InfoModuleItemOfficial> CREATOR
            = new Parcelable.Creator<InfoModuleItemOfficial>() {
        @Override
        public InfoModuleItemOfficial createFromParcel(Parcel in) {
            return new InfoModuleItemOfficial(in);
        }

        @Override
        public InfoModuleItemOfficial[] newArray(int size) {
            return new InfoModuleItemOfficial[size];
        }
    };
}
