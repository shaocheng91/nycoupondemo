
package com.nineyi.data.model.promotion.discount;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class PromotionDiscountData implements Parcelable {

    @SerializedName("PromotionCount")
    @Expose
    private int promotionCount;
    @SerializedName("PromotionList")
    @Expose
    private List<PromotionDiscountList> promotionList = new ArrayList<PromotionDiscountList>();

    /**
     * 
     * @return
     *     The promotionCount
     */
    public int getPromotionCount() {
        return promotionCount;
    }

    /**
     * 
     * @param promotionCount
     *     The PromotionCount
     */
    public void setPromotionCount(int promotionCount) {
        this.promotionCount = promotionCount;
    }

    /**
     * 
     * @return
     *     The promotionList
     */
    public List<PromotionDiscountList> getPromotionList() {
        return promotionList;
    }

    /**
     * 
     * @param promotionList
     *     The PromotionList
     */
    public void setPromotionList(List<PromotionDiscountList> promotionList) {
        this.promotionList = promotionList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.promotionCount);
        dest.writeList(this.promotionList);
    }

    public PromotionDiscountData() {
    }

    protected PromotionDiscountData(Parcel in) {
        this.promotionCount = in.readInt();
        this.promotionList = new ArrayList<PromotionDiscountList>();
        in.readList(this.promotionList, PromotionDiscountList.class.getClassLoader());
    }

    public static final Parcelable.Creator<PromotionDiscountData> CREATOR
            = new Parcelable.Creator<PromotionDiscountData>() {
        @Override
        public PromotionDiscountData createFromParcel(Parcel source) {
            return new PromotionDiscountData(source);
        }

        @Override
        public PromotionDiscountData[] newArray(int size) {
            return new PromotionDiscountData[size];
        }
    };
}
