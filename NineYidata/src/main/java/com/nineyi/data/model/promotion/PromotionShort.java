package com.nineyi.data.model.promotion;

import android.os.Parcel;
import android.os.Parcelable;


public final class PromotionShort implements Parcelable {

    public int Id;
    public String Title;
    public int ShopId;
    public String ShopName;
    public int CategoryId;
    public String CategoryName;
    public String IconURL;
    public String StartTime;
    public String EndTime;

    public PromotionShort() {
    }

    // Parcelable management
    private PromotionShort(Parcel in) {
        Id = in.readInt();
        Title = in.readString();
        ShopId = in.readInt();
        ShopName = in.readString();
        CategoryId = in.readInt();
        CategoryName = in.readString();
        IconURL = in.readString();
        StartTime = in.readString();
        EndTime = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeInt(ShopId);
        dest.writeString(ShopName);
        dest.writeInt(CategoryId);
        dest.writeString(CategoryName);
        dest.writeString(IconURL);
        dest.writeString(StartTime);
        dest.writeString(EndTime);
    }

    public static final Parcelable.Creator<PromotionShort> CREATOR = new Parcelable.Creator<PromotionShort>() {
        public PromotionShort createFromParcel(Parcel in) {
            return new PromotionShort(in);
        }

        public PromotionShort[] newArray(int size) {
            return new PromotionShort[size];
        }
    };
}
