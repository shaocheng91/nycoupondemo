package com.nineyi.data.model.memberzone;

import com.nineyi.data.model.gson.NineyiDate;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/10/21.
 */
public class CrmMemberTier implements Parcelable {
    public int ShopId;
    public int MemberId;
    public int MemberCardId;
    public String MemberCardName;
    public int MemberCardLevel;
    public String MemberCardImagePath;
    public NineyiDate StartDateTime;
    public NineyiDate EndDateTime;
    public DailySummary DailySummary;

    public CrmMemberTier() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ShopId);
        dest.writeInt(this.MemberId);
        dest.writeInt(this.MemberCardId);
        dest.writeString(this.MemberCardName);
        dest.writeInt(this.MemberCardLevel);
        dest.writeString(this.MemberCardImagePath);
        dest.writeParcelable(this.StartDateTime, flags);
        dest.writeParcelable(this.EndDateTime, flags);
        dest.writeParcelable(this.DailySummary, flags);
    }

    protected CrmMemberTier(Parcel in) {
        this.ShopId = in.readInt();
        this.MemberId = in.readInt();
        this.MemberCardId = in.readInt();
        this.MemberCardName = in.readString();
        this.MemberCardLevel = in.readInt();
        this.MemberCardImagePath = in.readString();
        this.StartDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.EndDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.DailySummary = in.readParcelable(DailySummary.class.getClassLoader());
    }

    public static final Creator<CrmMemberTier> CREATOR = new Creator<CrmMemberTier>() {
        @Override
        public CrmMemberTier createFromParcel(Parcel source) {
            return new CrmMemberTier(source);
        }

        @Override
        public CrmMemberTier[] newArray(int size) {
            return new CrmMemberTier[size];
        }
    };
}
