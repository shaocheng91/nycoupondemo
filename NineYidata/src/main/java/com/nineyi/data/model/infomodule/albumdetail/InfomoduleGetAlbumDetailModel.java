package com.nineyi.data.model.infomodule.albumdetail;

import com.nineyi.data.model.infomodule.DotNetModel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleGetAlbumDetailModel extends DotNetModel {

    public InfomoduleGetAlbumDetailDataModel Data;

    public InfomoduleGetAlbumDetailModel(Parcel in) {
        super(in);
        Data = InfomoduleGetAlbumDetailDataModel.CREATOR.createFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(Data, flags);
    }

    public static Parcelable.Creator<InfomoduleGetAlbumDetailModel> CREATOR
            = new Parcelable.Creator<InfomoduleGetAlbumDetailModel>() {
        @Override
        public InfomoduleGetAlbumDetailModel createFromParcel(Parcel source) {
            return new InfomoduleGetAlbumDetailModel(source);
        }

        @Override
        public InfomoduleGetAlbumDetailModel[] newArray(int size) {
            return new InfomoduleGetAlbumDetailModel[size];
        }
    };
}
