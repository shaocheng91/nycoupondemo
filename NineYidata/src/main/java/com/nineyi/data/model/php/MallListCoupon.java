package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class MallListCoupon implements Parcelable {

    private static final String FIELD_KIND = "kind";
    private static final String FIELD_DISPLAY_COUNT_LIMIT = "display_count_limit";
    private static final String FIELD_USAGE_LIMIT = "usage_limit";
    private static final String FIELD_ID = "id";
    private static final String FIELD_USE_START_DATE = "use_start_date";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_IS_ENABLED = "isEnabled";
    private static final String FIELD_USE_END_DATE = "use_end_date";
    private static final String FIELD_COUNT_LIMIT = "count_limit";
    private static final String FIELD_TYPE_ID = "type_id";
    private static final String FIELD_REMARK = "remark";
    private static final String FIELD_STORE = "store";
    private static final String FIELD_NAME = "name";


    @SerializedName(FIELD_KIND)
    private String mKind;
    @SerializedName(FIELD_DISPLAY_COUNT_LIMIT)
    private int mDisplayCountLimit;
    @SerializedName(FIELD_USAGE_LIMIT)
    private int mUsageLimit;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_USE_START_DATE)
    private String mUseStartDate;
    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;
    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FIELD_IS_ENABLED)
    private boolean mIsEnabled;
    @SerializedName(FIELD_USE_END_DATE)
    private String mUseEndDate;
    @SerializedName(FIELD_COUNT_LIMIT)
    private int mCountLimit;
    @SerializedName(FIELD_TYPE_ID)
    private int mTypeId;
    @SerializedName(FIELD_REMARK)
    private String mRemark;
    @SerializedName(FIELD_STORE)
    private String mStore;
    @SerializedName(FIELD_NAME)
    private String mName;


    public MallListCoupon() {

    }

    public void setKind(String kind) {
        mKind = kind;
    }

    public String getKind() {
        return mKind;
    }

    public void setDisplayCountLimit(int displayCountLimit) {
        mDisplayCountLimit = displayCountLimit;
    }

    public int getDisplayCountLimit() {
        return mDisplayCountLimit;
    }

    public void setUsageLimit(int usageLimit) {
        mUsageLimit = usageLimit;
    }

    public int getUsageLimit() {
        return mUsageLimit;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setUseStartDate(String useStartDate) {
        mUseStartDate = useStartDate;
    }

    public String getUseStartDate() {
        return mUseStartDate;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }

    public void setIsEnabled(boolean isEnabled) {
        mIsEnabled = isEnabled;
    }

    public boolean isIsEnabled() {
        return mIsEnabled;
    }

    public void setUseEndDate(String useEndDate) {
        mUseEndDate = useEndDate;
    }

    public String getUseEndDate() {
        return mUseEndDate;
    }

    public void setCountLimit(int countLimit) {
        mCountLimit = countLimit;
    }

    public int getCountLimit() {
        return mCountLimit;
    }

    public void setTypeId(int typeId) {
        mTypeId = typeId;
    }

    public int getTypeId() {
        return mTypeId;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setStore(String store) {
        mStore = store;
    }

    public String getStore() {
        return mStore;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public MallListCoupon(Parcel in) {
        mKind = in.readString();
        mDisplayCountLimit = in.readInt();
        mUsageLimit = in.readInt();
        mId = in.readInt();
        mUseStartDate = in.readString();
        mDescription = in.readString();
        mType = in.readString();
        mIsEnabled = in.readInt() == 1;
        mUseEndDate = in.readString();
        mCountLimit = in.readInt();
        mTypeId = in.readInt();
        mRemark = in.readString();
        mStore = in.readString();
        mName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MallListCoupon> CREATOR = new Creator<MallListCoupon>() {
        public MallListCoupon createFromParcel(Parcel in) {
            return new MallListCoupon(in);
        }

        public MallListCoupon[] newArray(int size) {
            return new MallListCoupon[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKind);
        dest.writeInt(mDisplayCountLimit);
        dest.writeInt(mUsageLimit);
        dest.writeInt(mId);
        dest.writeString(mUseStartDate);
        dest.writeString(mDescription);
        dest.writeString(mType);
        dest.writeInt(mIsEnabled ? 1 : 0);
        dest.writeString(mUseEndDate);
        dest.writeInt(mCountLimit);
        dest.writeInt(mTypeId);
        dest.writeString(mRemark);
        dest.writeString(mStore);
        dest.writeString(mName);
    }


}