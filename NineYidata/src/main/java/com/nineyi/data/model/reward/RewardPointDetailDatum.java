package com.nineyi.data.model.reward;

import java.util.List;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.gson.NineyiDate;

import android.os.Parcel;
import java.util.ArrayList;


public class RewardPointDetailDatum implements Parcelable{

    private static final String FIELD_IMAGE_URL = "ImageUrl";
    private static final String FIELD_EXCHANGE_START_DATE = "ExchangeStartDate";
    private static final String FIELD_REWARD_POINT_GIFT_LIST = "RewardPointGiftList";
    private static final String FIELD_START_DATE = "StartDate";
    private static final String FIELD_END_DATE = "EndDate";
    private static final String FIELD_ID = "Id";
    private static final String FIELD_NOTE = "Note";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_DESCRIPTION = "Description";
    private static final String FIELD_REWARD_POINT_STORE_LIST = "RewardPointStoreList";
    private static final String FIELD_EXCHANGE_END_DATE = "ExchangeEndDate";


    @SerializedName(FIELD_IMAGE_URL)
    private String mImageUrl;
    @SerializedName(FIELD_EXCHANGE_START_DATE)
    private NineyiDate mExchangeStartDate;
    @SerializedName(FIELD_REWARD_POINT_GIFT_LIST)
    private ArrayList<RewardPointGiftList> mRewardPointGiftLists;
    @SerializedName(FIELD_START_DATE)
    private NineyiDate mStartDate;
    @SerializedName(FIELD_END_DATE)
    private NineyiDate mEndDate;
    @SerializedName(FIELD_ID)
    private int mId;
    @SerializedName(FIELD_NOTE)
    private String mNote;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;
    @SerializedName(FIELD_REWARD_POINT_STORE_LIST)
    private ArrayList<RewardPointStoreList> mRewardPointStoreLists;
    @SerializedName(FIELD_EXCHANGE_END_DATE)
    private NineyiDate mExchangeEndDate;


    public RewardPointDetailDatum(){

    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setExchangeStartDate(NineyiDate exchangeStartDate) {
        mExchangeStartDate = exchangeStartDate;
    }

    public NineyiDate getExchangeStartDate() {
        return mExchangeStartDate;
    }

    public void setRewardPointGiftLists(ArrayList<RewardPointGiftList> rewardPointGiftLists) {
        mRewardPointGiftLists = rewardPointGiftLists;
    }

    public ArrayList<RewardPointGiftList> getRewardPointGiftLists() {
        return mRewardPointGiftLists;
    }

    public NineyiDate getEndDate() {
        return mEndDate;
    }

    public void setEndDate(NineyiDate mEndDate) {
        this.mEndDate = mEndDate;
    }

    public NineyiDate getStartDate() {
        return mStartDate;
    }

    public void setStartDate(NineyiDate mStartDate) {
        this.mStartDate = mStartDate;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getNote() {
        return mNote;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setRewardPointStoreLists(ArrayList<RewardPointStoreList> rewardPointStoreLists) {
        mRewardPointStoreLists = rewardPointStoreLists;
    }

    public ArrayList<RewardPointStoreList> getRewardPointStoreLists() {
        return mRewardPointStoreLists;
    }

    public void setExchangeEndDate(NineyiDate exchangeEndDate) {
        mExchangeEndDate = exchangeEndDate;
    }

    public NineyiDate getExchangeEndDate() {
        return mExchangeEndDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mImageUrl);
        dest.writeParcelable(this.mExchangeStartDate, flags);
        dest.writeTypedList(this.mRewardPointGiftLists);
        dest.writeParcelable(this.mStartDate, flags);
        dest.writeParcelable(this.mEndDate, flags);
        dest.writeInt(this.mId);
        dest.writeString(this.mNote);
        dest.writeString(this.mName);
        dest.writeString(this.mDescription);
        dest.writeTypedList(this.mRewardPointStoreLists);
        dest.writeParcelable(this.mExchangeEndDate, flags);
    }

    protected RewardPointDetailDatum(Parcel in) {
        this.mImageUrl = in.readString();
        this.mExchangeStartDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.mRewardPointGiftLists = in.createTypedArrayList(RewardPointGiftList.CREATOR);
        this.mStartDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.mEndDate = in.readParcelable(NineyiDate.class.getClassLoader());
        this.mId = in.readInt();
        this.mNote = in.readString();
        this.mName = in.readString();
        this.mDescription = in.readString();
        this.mRewardPointStoreLists = in.createTypedArrayList(RewardPointStoreList.CREATOR);
        this.mExchangeEndDate = in.readParcelable(NineyiDate.class.getClassLoader());
    }

    public static final Creator<RewardPointDetailDatum> CREATOR = new Creator<RewardPointDetailDatum>() {
        @Override
        public RewardPointDetailDatum createFromParcel(Parcel source) {
            return new RewardPointDetailDatum(source);
        }

        @Override
        public RewardPointDetailDatum[] newArray(int size) {
            return new RewardPointDetailDatum[size];
        }
    };
}