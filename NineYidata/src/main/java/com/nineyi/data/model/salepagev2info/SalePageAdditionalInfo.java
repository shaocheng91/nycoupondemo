
package com.nineyi.data.model.salepagev2info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.salepage.SalePageNotKeyProperty;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class SalePageAdditionalInfo implements Parcelable {

    public String getReturnCode() {
        return returnCode;
    }

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private SalePageAdditionalInfoData data;
    @SerializedName("Message")
    @Expose
    private String message;

    protected SalePageAdditionalInfo(Parcel in) {
        returnCode = in.readString();
        data = (SalePageAdditionalInfoData) in.readValue(SalePageAdditionalInfoData.class.getClassLoader());
        message = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(returnCode);
        dest.writeValue(data);
        dest.writeString(message);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SalePageAdditionalInfo> CREATOR = new Parcelable.Creator<SalePageAdditionalInfo>() {
        @Override
        public SalePageAdditionalInfo createFromParcel(Parcel in) {
            return new SalePageAdditionalInfo(in);
        }

        @Override
        public SalePageAdditionalInfo[] newArray(int size) {
            return new SalePageAdditionalInfo[size];
        }
    };

    String getSaleProductDescContent() {
        if (data == null || data.getMoreInfo() == null) {
            return "";
        } else {
            return data.getMoreInfo().SaleProductDesc_Content;
        }
    }

    ArrayList<SalePageNotKeyProperty> getNotKeyPropertyList() {
        if (data == null || data.getMoreInfo() == null) {
            return new ArrayList<>();
        } else {
            return data.getNotKeyPropertyList();
        }
    }

    MainImageVideo getMainImageVideo() {
        if (data == null) {
            return null;
        } else {
            return data.getMainImageVideo();
        }
    }
}
