
package com.nineyi.data.model.salepagev2info;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.ecoupon.ECouponShopECoupon;
import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.data.model.installment.InstallmentShopInstallmentList;
import com.nineyi.data.model.salepage.SKUPropertySet;
import com.nineyi.data.model.salepage.SalePageGift;
import com.nineyi.data.model.salepage.SalePageImage;
import com.nineyi.data.model.salepage.SalePageMajor;
import com.nineyi.data.model.salepage.SalePageShippingType;
import com.nineyi.data.model.salepage.TradesOrderListUri;

public class Data implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ShopId")
    @Expose
    private Integer shopId;
    @SerializedName("ShopName")
    @Expose
    private String shopName;
    @SerializedName("ShopCategoryId")
    @Expose
    private Integer shopCategoryId;
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("CategoryLevelName")
    @Expose
    private CategoryLevelName categoryLevelName;
    @SerializedName("SubTitle")
    @Expose
    private String subTitle;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ShortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("SubDescript")
    @Expose
    private String subDescript;
    @SerializedName("SuggestPrice")
    @Expose
    private Double suggestPrice;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("SellingStartDateTime")
    @Expose
    private NineyiDate sellingStartDateTime;
    @SerializedName("CreateDateTime")
    @Expose
    private NineyiDate createDateTime;
    @SerializedName("ImageCount")
    @Expose
    private Integer imageCount;
    @SerializedName("ImageList")
    @Expose
    private List<SalePageImage> imageList = null;
    @SerializedName("MainImageVideo")
    @Expose
    private MainImageVideo mainImageVideo;
    @SerializedName("HasSKU")
    @Expose
    private Boolean hasSKU;
    @SerializedName("StatusDef")
    @Expose
    private String statusDef;
    @SerializedName("SaleProductSKUIdList")
    @Expose
    private List<Integer> saleProductSKUIdList = null;
    @SerializedName("SKUPropertySetList")
    @Expose
    private List<SKUPropertySet> sKUPropertySetList = null;
    @SerializedName("IsLimit")
    @Expose
    private Boolean isLimit;
    @SerializedName("IsShareToBuy")
    @Expose
    private Boolean isShareToBuy;
    @SerializedName("IsAPPOnly")
    @Expose
    private Boolean isAPPOnly;
    @SerializedName("MajorList")
    @Expose
    private List<SalePageMajor> majorList = null;
    @SerializedName("SalePageGiftList")
    @Expose
    private List<SalePageGift> salePageGiftList = null;
    @SerializedName("UpdatedDateTime")
    @Expose
    private NineyiDate updatedDateTime;
    @SerializedName("PayProfileTypeDefList")
    @Expose
    private List<String> payProfileTypeDefList = null;
    @SerializedName("ShippingTypeList")
    @Expose
    private List<SalePageShippingType> shippingTypeList = null;
    @SerializedName("InstallmentList")
    @Expose
    private List<InstallmentShopInstallmentList> installmentList = null;
    @SerializedName("SalePageShowShippingTypeLists")
    @Expose
    private SalePageShowShippingTypeLists salePageShowShippingTypeLists;
    @SerializedName("IsShowSoldQty")
    @Expose
    private Boolean isShowSoldQty;
    @SerializedName("SoldQty")
    @Expose
    private Integer soldQty;
    @SerializedName("IsShowTradesOrderList")
    @Expose
    private Boolean isShowTradesOrderList;
    @SerializedName("TradesOrderListUri")
    @Expose
    private TradesOrderListUri tradesOrderListUri;
    @SerializedName("IsExcludeECouponDiscount")
    @Expose
    private Boolean isExcludeECouponDiscount;
    @SerializedName("IsDisplayExcludeECouponDiscount")
    @Expose
    private Boolean isDisplayExcludeECouponDiscount;
    @SerializedName("Promotions")
    @Expose
    private List<Promotion> promotions = null;
    @SerializedName("ECoupons")
    @Expose
    private List<ECouponShopECoupon> eCoupons = null;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("FreeShippingTypeTag")
    @Expose
    private FreeShippingTypeTag freeShippingTypeTag;

    protected Data(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        shopId = in.readByte() == 0x00 ? null : in.readInt();
        shopName = in.readString();
        shopCategoryId = in.readByte() == 0x00 ? null : in.readInt();
        categoryId = in.readByte() == 0x00 ? null : in.readInt();
        categoryName = in.readString();
        categoryLevelName = (CategoryLevelName) in.readValue(CategoryLevelName.class.getClassLoader());
        subTitle = in.readString();
        title = in.readString();
        shortDescription = in.readString();
        subDescript = in.readString();
        suggestPrice = in.readByte() == 0x00 ? null : in.readDouble();
        price = in.readByte() == 0x00 ? null : in.readDouble();
        sellingStartDateTime = (NineyiDate) in.readValue(NineyiDate.class.getClassLoader());
        createDateTime = (NineyiDate) in.readValue(NineyiDate.class.getClassLoader());
        imageCount = in.readByte() == 0x00 ? null : in.readInt();
        if (in.readByte() == 0x01) {
            imageList = new ArrayList<SalePageImage>();
            in.readList(imageList, SalePageImage.class.getClassLoader());
        } else {
            imageList = null;
        }
        mainImageVideo = (MainImageVideo) in.readValue(MainImageVideo.class.getClassLoader());
        byte hasSKUVal = in.readByte();
        hasSKU = hasSKUVal == 0x02 ? null : hasSKUVal != 0x00;
        statusDef = in.readString();
        if (in.readByte() == 0x01) {
            saleProductSKUIdList = new ArrayList<Integer>();
            in.readList(saleProductSKUIdList, Integer.class.getClassLoader());
        } else {
            saleProductSKUIdList = null;
        }
        if (in.readByte() == 0x01) {
            sKUPropertySetList = new ArrayList<SKUPropertySet>();
            in.readList(sKUPropertySetList, SKUPropertySet.class.getClassLoader());
        } else {
            sKUPropertySetList = null;
        }
        byte isLimitVal = in.readByte();
        isLimit = isLimitVal == 0x02 ? null : isLimitVal != 0x00;
        byte isShareToBuyVal = in.readByte();
        isShareToBuy = isShareToBuyVal == 0x02 ? null : isShareToBuyVal != 0x00;
        byte isAPPOnlyVal = in.readByte();
        isAPPOnly = isAPPOnlyVal == 0x02 ? null : isAPPOnlyVal != 0x00;
        if (in.readByte() == 0x01) {
            majorList = new ArrayList<SalePageMajor>();
            in.readList(majorList, SalePageMajor.class.getClassLoader());
        } else {
            majorList = null;
        }
        if (in.readByte() == 0x01) {
            salePageGiftList = new ArrayList<SalePageGift>();
            in.readList(salePageGiftList, SalePageGift.class.getClassLoader());
        } else {
            salePageGiftList = null;
        }
        updatedDateTime = (NineyiDate) in.readValue(NineyiDate.class.getClassLoader());
        if (in.readByte() == 0x01) {
            payProfileTypeDefList = new ArrayList<String>();
            in.readList(payProfileTypeDefList, String.class.getClassLoader());
        } else {
            payProfileTypeDefList = null;
        }
        if (in.readByte() == 0x01) {
            shippingTypeList = new ArrayList<SalePageShippingType>();
            in.readList(shippingTypeList, SalePageShippingType.class.getClassLoader());
        } else {
            shippingTypeList = null;
        }
        if (in.readByte() == 0x01) {
            installmentList = new ArrayList<InstallmentShopInstallmentList>();
            in.readList(installmentList, InstallmentShopInstallmentList.class.getClassLoader());
        } else {
            installmentList = null;
        }
        salePageShowShippingTypeLists = (SalePageShowShippingTypeLists) in.readValue(SalePageShowShippingTypeLists.class.getClassLoader());
        byte isShowSoldQtyVal = in.readByte();
        isShowSoldQty = isShowSoldQtyVal == 0x02 ? null : isShowSoldQtyVal != 0x00;
        soldQty = in.readByte() == 0x00 ? null : in.readInt();
        byte isShowTradesOrderListVal = in.readByte();
        isShowTradesOrderList = isShowTradesOrderListVal == 0x02 ? null : isShowTradesOrderListVal != 0x00;
        tradesOrderListUri = (TradesOrderListUri) in.readValue(TradesOrderListUri.class.getClassLoader());
        byte isExcludeECouponDiscountVal = in.readByte();
        isExcludeECouponDiscount = isExcludeECouponDiscountVal == 0x02 ? null : isExcludeECouponDiscountVal != 0x00;
        byte isDisplayExcludeECouponDiscountVal = in.readByte();
        isDisplayExcludeECouponDiscount = isDisplayExcludeECouponDiscountVal == 0x02 ? null : isDisplayExcludeECouponDiscountVal != 0x00;
        if (in.readByte() == 0x01) {
            promotions = new ArrayList<Promotion>();
            in.readList(promotions, Promotion.class.getClassLoader());
        } else {
            promotions = null;
        }
        if (in.readByte() == 0x01) {
            eCoupons = new ArrayList<ECouponShopECoupon>();
            in.readList(eCoupons, ECouponShopECoupon.class.getClassLoader());
        } else {
            eCoupons = null;
        }
        description = in.readString();
        freeShippingTypeTag = (FreeShippingTypeTag) in.readValue(FreeShippingTypeTag.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        if (shopId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(shopId);
        }
        dest.writeString(shopName);
        if (shopCategoryId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(shopCategoryId);
        }
        if (categoryId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(categoryId);
        }
        dest.writeString(categoryName);
        dest.writeValue(categoryLevelName);
        dest.writeString(subTitle);
        dest.writeString(title);
        dest.writeString(shortDescription);
        dest.writeString(subDescript);
        if (suggestPrice == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(suggestPrice);
        }
        if (price == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(price);
        }
        dest.writeValue(sellingStartDateTime);
        dest.writeValue(createDateTime);
        if (imageCount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(imageCount);
        }
        if (imageList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(imageList);
        }
        dest.writeValue(mainImageVideo);
        if (hasSKU == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (hasSKU ? 0x01 : 0x00));
        }
        dest.writeString(statusDef);
        if (saleProductSKUIdList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(saleProductSKUIdList);
        }
        if (sKUPropertySetList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sKUPropertySetList);
        }
        if (isLimit == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isLimit ? 0x01 : 0x00));
        }
        if (isShareToBuy == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isShareToBuy ? 0x01 : 0x00));
        }
        if (isAPPOnly == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isAPPOnly ? 0x01 : 0x00));
        }
        if (majorList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(majorList);
        }
        if (salePageGiftList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(salePageGiftList);
        }
        dest.writeValue(updatedDateTime);
        if (payProfileTypeDefList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(payProfileTypeDefList);
        }
        if (shippingTypeList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(shippingTypeList);
        }
        if (installmentList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(installmentList);
        }
        dest.writeValue(salePageShowShippingTypeLists);
        if (isShowSoldQty == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isShowSoldQty ? 0x01 : 0x00));
        }
        if (soldQty == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(soldQty);
        }
        if (isShowTradesOrderList == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isShowTradesOrderList ? 0x01 : 0x00));
        }
        dest.writeValue(tradesOrderListUri);
        if (isExcludeECouponDiscount == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isExcludeECouponDiscount ? 0x01 : 0x00));
        }
        if (isDisplayExcludeECouponDiscount == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isDisplayExcludeECouponDiscount ? 0x01 : 0x00));
        }
        if (promotions == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(promotions);
        }
        if (eCoupons == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(eCoupons);
        }
        dest.writeString(description);
        dest.writeValue(freeShippingTypeTag);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };


    int getShopCategoryId() {
        return shopCategoryId;
    }

    int getSalePageId() {
        return id;
    }

    String getStatusDef() {
        return statusDef;
    }

    void setStatusDef(String statusDef) {
        this.statusDef = statusDef;
    }

    String getTitle() {
        return title;
    }

    Double getPrice() {
        return price;
    }

    void setPrice(Double newPrice) {
        price = newPrice;
    }

    String getShopName() {
        return shopName;
    }

    String getFirstImageUrl() {
        if (imageList != null && imageList.size() >= 1) {
            return imageList.get(0).PicUrl;
        } else {
            return "";
        }
    }

    Double getSuggestPrice() {
        return suggestPrice;
    }

    void setSuggestPrice(Double newSuggestPrice) {
        suggestPrice = newSuggestPrice;
    }

    String getSubDescript() {
        return subDescript;
    }

    List<SalePageGift> getSalePageGiftList() {
        return salePageGiftList;
    }

    TradesOrderListUri getTradesOrderListUri() {
        return tradesOrderListUri;
    }

    List<SalePageImage> getImageList() {
        return imageList;
    }

    Integer getShopId() {
        return shopId;
    }

    boolean getHasSKU() {
        return hasSKU;
    }

    List<SalePageMajor> getMajorList() {
        return majorList;
    }

    List<SKUPropertySet> getSKUPropertySetList() {
        return sKUPropertySetList;
    }

    List<String> getPayProfileTypeDefList() {
        return payProfileTypeDefList;
    }

    List<SalePageShippingType> getShippingTypeList() {
        return shippingTypeList;
    }

    List<InstallmentShopInstallmentList> getInstallmentList() {
        return installmentList;
    }

    boolean getIsDisplayExcludeECouponDiscount() {
        return isDisplayExcludeECouponDiscount;
    }

    Boolean getIsShowSoldQty() {
        return isShowSoldQty;
    }

    Integer getSoldQty() {
        return soldQty;
    }

    Boolean getIsShowTradesOrderList() {
        return isShowTradesOrderList;
    }

    NineyiDate getSellingStartDateTime() {
        return sellingStartDateTime;
    }

    String getCategoryText() {
        return categoryName;
    }

    Boolean getIsShareToBuy() {
        return isShareToBuy;
    }

    List<Promotion> getPromotions() {
        return promotions;
    }

    List<ECouponShopECoupon> getECoupons() {
        return eCoupons;
    }

    String freeShippingTag() {
        if (freeShippingTypeTag != null && freeShippingTypeTag.getHasFreeShippingTypeTag()) {
            return freeShippingTypeTag.getBestFreeShippingTypeTag();
        } else {
            return null;
        }
    }

    MainImageVideo getMainImageVideo() {
        return mainImageVideo;
    }

    String getShortDescription() {
        return shortDescription;
    }
}
