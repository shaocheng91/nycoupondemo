package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/7/28.
 */
public class SearchPriceRange implements Parcelable {
    public double Min;
    public double Max;

    public SearchPriceRange() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.Min);
        dest.writeDouble(this.Max);
    }

    protected SearchPriceRange(Parcel in) {
        this.Min = in.readDouble();
        this.Max = in.readDouble();
    }

    public static final Creator<SearchPriceRange> CREATOR = new Creator<SearchPriceRange>() {
        @Override
        public SearchPriceRange createFromParcel(Parcel source) {
            return new SearchPriceRange(source);
        }

        @Override
        public SearchPriceRange[] newArray(int size) {
            return new SearchPriceRange[size];
        }
    };
}
