package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeComponent implements Parcelable {
    public int ComponentId;
    public String ComponentName;
    public String ViewPath;
    public String Type;
    public HashMap<String, String> ConfigList;

    public ShopThemeComponent() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ComponentId);
        dest.writeString(this.ComponentName);
        dest.writeString(this.ViewPath);
        dest.writeString(this.Type);
        dest.writeSerializable(this.ConfigList);
    }

    protected ShopThemeComponent(Parcel in) {
        this.ComponentId = in.readInt();
        this.ComponentName = in.readString();
        this.ViewPath = in.readString();
        this.Type = in.readString();
        this.ConfigList = (HashMap<String, String>) in.readSerializable();
    }

    public static final Creator<ShopThemeComponent> CREATOR = new Creator<ShopThemeComponent>() {
        @Override
        public ShopThemeComponent createFromParcel(Parcel source) {
            return new ShopThemeComponent(source);
        }

        @Override
        public ShopThemeComponent[] newArray(int size) {
            return new ShopThemeComponent[size];
        }
    };
}
