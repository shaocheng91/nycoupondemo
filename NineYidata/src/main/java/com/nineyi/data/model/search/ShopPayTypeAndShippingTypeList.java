package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/7/27.
 */
public class ShopPayTypeAndShippingTypeList implements Parcelable {
    public String ReturnCode;
    public PayAndShippingList Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.Message);
    }

    public ShopPayTypeAndShippingTypeList() {
    }

    protected ShopPayTypeAndShippingTypeList(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(PayAndShippingList.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<ShopPayTypeAndShippingTypeList> CREATOR = new Parcelable.Creator<ShopPayTypeAndShippingTypeList>() {
        @Override
        public ShopPayTypeAndShippingTypeList createFromParcel(Parcel source) {
            return new ShopPayTypeAndShippingTypeList(source);
        }

        @Override
        public ShopPayTypeAndShippingTypeList[] newArray(int size) {
            return new ShopPayTypeAndShippingTypeList[size];
        }
    };
}
