package com.nineyi.data.model.referee;

import android.os.Parcel;
import android.os.Parcelable;


public final class RefereeSettingInfo implements Parcelable {

    public boolean IsEmployeeColumnVisible;
    public boolean IsRequireLogin;

    public RefereeSettingInfo() {
    }

    // Parcelable management
    private RefereeSettingInfo(Parcel in) {
        IsEmployeeColumnVisible = in.readInt() == 1;
        IsRequireLogin = in.readInt() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(IsEmployeeColumnVisible ? 1 : 0);
        dest.writeInt(IsRequireLogin ? 1 : 0);
    }

    public static final Creator<RefereeSettingInfo> CREATOR = new Creator<RefereeSettingInfo>() {
        public RefereeSettingInfo createFromParcel(Parcel in) {
            return new RefereeSettingInfo(in);
        }

        public RefereeSettingInfo[] newArray(int size) {
            return new RefereeSettingInfo[size];
        }
    };
}
