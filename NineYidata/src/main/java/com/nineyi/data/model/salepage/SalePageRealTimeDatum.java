package com.nineyi.data.model.salepage;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class SalePageRealTimeDatum implements Parcelable{

    private static final String FIELD_PRICE = "Price";
    private static final String FIELD_STATUS_DEF = "StatusDef";
    private static final String FIELD_SUGGEST_PRICE = "SuggestPrice";


    @SerializedName(FIELD_PRICE)
    private double mPrice;
    @SerializedName(FIELD_STATUS_DEF)
    private String mStatusDef;
    @SerializedName(FIELD_SUGGEST_PRICE)
    private double mSuggestPrice;


    public SalePageRealTimeDatum(){

    }

    public void setPrice(int price) {
        mPrice = price;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setStatusDef(String statusDef) {
        mStatusDef = statusDef;
    }

    public String getStatusDef() {
        return mStatusDef;
    }

    public void setSuggestPrice(double suggestPrice) {
        mSuggestPrice = suggestPrice;
    }

    public double getSuggestPrice() {
        return mSuggestPrice;
    }

    public SalePageRealTimeDatum(Parcel in) {
        mPrice = in.readDouble();
        mStatusDef = in.readString();
        mSuggestPrice = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SalePageRealTimeDatum> CREATOR = new Creator<SalePageRealTimeDatum>() {
        public SalePageRealTimeDatum createFromParcel(Parcel in) {
            return new SalePageRealTimeDatum(in);
        }

        public SalePageRealTimeDatum[] newArray(int size) {
            return new SalePageRealTimeDatum[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mPrice);
        dest.writeString(mStatusDef);
        dest.writeDouble(mSuggestPrice);
    }


}