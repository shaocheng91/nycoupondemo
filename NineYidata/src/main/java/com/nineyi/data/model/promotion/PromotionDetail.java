package com.nineyi.data.model.promotion;

import com.nineyi.data.model.category.SalePageListData;
import com.nineyi.data.model.salepage.SalePageShort;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class PromotionDetail implements Parcelable {

    public Promotion PromotionDetail;
    public ArrayList<SalePageShort> TargetSalePageList;//test
    public ArrayList<ShopCategorySalePageListData> ShopCategorySalePageList;

    public PromotionDetail() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.PromotionDetail, flags);
        dest.writeTypedList(this.TargetSalePageList);
        dest.writeTypedList(this.ShopCategorySalePageList);
    }

    protected PromotionDetail(Parcel in) {
        this.PromotionDetail = in.readParcelable(Promotion.class.getClassLoader());
        this.TargetSalePageList = in.createTypedArrayList(SalePageShort.CREATOR);
        this.ShopCategorySalePageList = in.createTypedArrayList(ShopCategorySalePageListData.CREATOR);
    }

    public static final Creator<PromotionDetail> CREATOR = new Creator<PromotionDetail>() {
        @Override
        public PromotionDetail createFromParcel(Parcel source) {
            return new PromotionDetail(source);
        }

        @Override
        public PromotionDetail[] newArray(int size) {
            return new PromotionDetail[size];
        }
    };
}
