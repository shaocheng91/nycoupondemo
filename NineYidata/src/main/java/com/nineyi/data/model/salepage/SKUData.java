package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class SKUData implements Parcelable {

    public String Title;
    public ArrayList<SKUProperty> PropertyList;
    public String DisplayPropertyName;

    public SKUData() {
    }

    // Parcelable management
    private SKUData(Parcel in) {
        Title = in.readString();
        PropertyList = in.createTypedArrayList(SKUProperty.CREATOR);
        DisplayPropertyName = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Title);
        dest.writeTypedList(PropertyList);
        dest.writeString(DisplayPropertyName);
    }

    public static final Parcelable.Creator<SKUData> CREATOR = new Parcelable.Creator<SKUData>() {
        public SKUData createFromParcel(Parcel in) {
            return new SKUData(in);
        }

        public SKUData[] newArray(int size) {
            return new SKUData[size];
        }
    };
}
