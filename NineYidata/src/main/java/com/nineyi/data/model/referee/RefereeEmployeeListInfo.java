package com.nineyi.data.model.referee;

import android.os.Parcel;
import android.os.Parcelable;


public final class RefereeEmployeeListInfo implements Parcelable {

    public int Id;
    public String FullName;


    public RefereeEmployeeListInfo(String mEmployee_Name) {
        this.FullName = mEmployee_Name;
    }

    // Parcelable management
    private RefereeEmployeeListInfo(Parcel in) {
        Id = in.readInt();
        FullName = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(FullName);
    }

    public static final Creator<RefereeEmployeeListInfo> CREATOR = new Creator<RefereeEmployeeListInfo>() {
        public RefereeEmployeeListInfo createFromParcel(Parcel in) {
            return new RefereeEmployeeListInfo(in);
        }

        public RefereeEmployeeListInfo[] newArray(int size) {
            return new RefereeEmployeeListInfo[size];
        }
    };
}
