package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2017/11/6.
 */

public class ECouponVerify implements Parcelable {
    public String ReturnCode;
    public ECouponVerifyData Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.Message);
    }

    public ECouponVerify() {
    }

    protected ECouponVerify(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(ECouponVerifyData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Creator<ECouponVerify> CREATOR = new Creator<ECouponVerify>() {
        @Override
        public ECouponVerify createFromParcel(Parcel source) {
            return new ECouponVerify(source);
        }

        @Override
        public ECouponVerify[] newArray(int size) {
            return new ECouponVerify[size];
        }
    };
}
