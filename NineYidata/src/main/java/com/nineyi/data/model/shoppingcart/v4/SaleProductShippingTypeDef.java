package com.nineyi.data.model.shoppingcart.v4;

/**
 * Created by Willy on 16/6/16.
 */
public enum SaleProductShippingTypeDef {
    Normal,
    PreOrderWithStartDate,
    PreOrderWithStartDateAndEndDate,
    CustomMade,
    CustomerAppointment
}
