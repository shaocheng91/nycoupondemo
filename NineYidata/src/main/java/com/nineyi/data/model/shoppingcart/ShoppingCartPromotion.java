package com.nineyi.data.model.shoppingcart;

import com.nineyi.data.model.promotion.PromotionTarget;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShoppingCartPromotion implements Parcelable {

    public int Id;
    public String Title;
    public String DiscountTitle;
    public String PromotionTypeTitle;
    public boolean IsPromotionMatchCondition;
    public boolean IsShowFreeGiftSoldOut;
    public ArrayList<ShoppingCartCondition> Conditions;
    public ArrayList<PromotionTarget> Targets;

    public ShoppingCartPromotion() {
    }

    // Parcelable management
    private ShoppingCartPromotion(Parcel in) {
        Id = in.readInt();
        Title = in.readString();
        DiscountTitle = in.readString();
        PromotionTypeTitle = in.readString();
        Conditions = in.createTypedArrayList(ShoppingCartCondition.CREATOR);
        Targets = in.createTypedArrayList(PromotionTarget.CREATOR);
        IsPromotionMatchCondition = in.readInt() == 1;
        IsShowFreeGiftSoldOut = in.readInt() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeString(DiscountTitle);
        dest.writeString(PromotionTypeTitle);
        dest.writeTypedList(Conditions);
        dest.writeTypedList(Targets);
        dest.writeInt(IsPromotionMatchCondition ? 1 : 0);
        dest.writeInt(IsShowFreeGiftSoldOut ? 1 : 0);
    }

    public static final Parcelable.Creator<ShoppingCartPromotion> CREATOR
            = new Parcelable.Creator<ShoppingCartPromotion>() {
        public ShoppingCartPromotion createFromParcel(Parcel in) {
            return new ShoppingCartPromotion(in);
        }

        public ShoppingCartPromotion[] newArray(int size) {
            return new ShoppingCartPromotion[size];
        }
    };
}
