
package com.nineyi.data.model.promotion.basket.item;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class BasicBasketItem implements Parcelable {

    @SerializedName("PromotionId")
    @Expose
    private int promotionId;
    @SerializedName("ShopId")
    @Expose
    private int shopId;
    @SerializedName("SalePageList")
    @Expose
    private List<BasicBasketSalePageList> salePageList = new ArrayList<BasicBasketSalePageList>();

    /**
     * 
     * @return
     *     The promotionId
     */
    public int getPromotionId() {
        return promotionId;
    }

    /**
     * 
     * @param promotionId
     *     The PromotionId
     */
    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    /**
     *
     * @return
     *     The shopId
     */
    public int getShopId() {
        return shopId;
    }

    /**
     *
     * @param shopId
     *     The shopId
     */
    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    /**
     * 
     * @return
     *     The salePageList
     */
    public List<BasicBasketSalePageList> getSalePageList() {
        return salePageList;
    }

    /**
     * 
     * @param salePageList
     *     The SalePageList
     */
    public void setSalePageList(List<BasicBasketSalePageList> salePageList) {
        this.salePageList = salePageList;
    }

    public BasicBasketItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.promotionId);
        dest.writeInt(this.shopId);
        dest.writeTypedList(this.salePageList);
    }

    protected BasicBasketItem(Parcel in) {
        this.promotionId = in.readInt();
        this.shopId = in.readInt();
        this.salePageList = in.createTypedArrayList(BasicBasketSalePageList.CREATOR);
    }

    public static final Creator<BasicBasketItem> CREATOR = new Creator<BasicBasketItem>() {
        @Override
        public BasicBasketItem createFromParcel(Parcel source) {
            return new BasicBasketItem(source);
        }

        @Override
        public BasicBasketItem[] newArray(int size) {
            return new BasicBasketItem[size];
        }
    };
}
