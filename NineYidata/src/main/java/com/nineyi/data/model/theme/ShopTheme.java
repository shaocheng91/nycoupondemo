package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopTheme implements Parcelable {
    public String ReturnCode;
    public ShopThemeData Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, 0);
        dest.writeString(this.Message);
    }

    public ShopTheme() {
    }

    protected ShopTheme(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(ShopThemeData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<ShopTheme> CREATOR = new Parcelable.Creator<ShopTheme>() {
        public ShopTheme createFromParcel(Parcel source) {
            return new ShopTheme(source);
        }

        public ShopTheme[] newArray(int size) {
            return new ShopTheme[size];
        }
    };
}
