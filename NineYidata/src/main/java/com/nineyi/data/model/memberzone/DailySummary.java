package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/10/21.
 */
public class DailySummary implements Parcelable {
    public Double TradesSum;
    public String UpdatedDate;

    public DailySummary() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.TradesSum);
        dest.writeString(this.UpdatedDate);
    }

    protected DailySummary(Parcel in) {
        this.TradesSum = (Double) in.readValue(Double.class.getClassLoader());
        this.UpdatedDate = in.readString();
    }

    public static final Creator<DailySummary> CREATOR = new Creator<DailySummary>() {
        @Override
        public DailySummary createFromParcel(Parcel source) {
            return new DailySummary(source);
        }

        @Override
        public DailySummary[] newArray(int size) {
            return new DailySummary[size];
        }
    };
}
