package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShopShort implements Parcelable {

    public int ShopId;
    public String ShopName;
    public String ShopUrl;
    public String LogoUrl;

    public ShopShort() {
    }

    // Parcelable management
    private ShopShort(Parcel in) {
        ShopId = in.readInt();
        ShopName = in.readString();
        ShopUrl = in.readString();
        LogoUrl = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopId);
        dest.writeString(ShopName);
        dest.writeString(ShopUrl);
        dest.writeString(LogoUrl);
    }

    public static final Parcelable.Creator<ShopShort> CREATOR = new Parcelable.Creator<ShopShort>() {
        public ShopShort createFromParcel(Parcel in) {
            return new ShopShort(in);
        }

        public ShopShort[] newArray(int size) {
            return new ShopShort[size];
        }
    };
}
