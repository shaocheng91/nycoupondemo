package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShoppingCartPreview implements Parcelable {

    public String ReturnCode;
    public ShoppingCartShopShortData Data;
    public String Message;

    public ShoppingCartPreview() {
    }

    // Parcelable management
    private ShoppingCartPreview(Parcel in) {
        ReturnCode = in.readString();
        Data = ShoppingCartShopShortData.CREATOR.createFromParcel(in);
        Message = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        Data.writeToParcel(dest, flags);
        dest.writeString(Message);
    }

    public static final Parcelable.Creator<ShoppingCartPreview> CREATOR
            = new Parcelable.Creator<ShoppingCartPreview>() {
        public ShoppingCartPreview createFromParcel(Parcel in) {
            return new ShoppingCartPreview(in);
        }

        public ShoppingCartPreview[] newArray(int size) {
            return new ShoppingCartPreview[size];
        }
    };
}
