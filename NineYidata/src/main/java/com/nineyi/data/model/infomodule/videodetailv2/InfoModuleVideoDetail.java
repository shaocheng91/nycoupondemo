
package com.nineyi.data.model.infomodule.videodetailv2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoModuleVideoDetail implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private InfoModuleVideoDetailData data;
    @SerializedName("Message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The returnCode
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * 
     * @param returnCode
     *     The ReturnCode
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * 
     * @return
     *     The data
     */
    public InfoModuleVideoDetailData getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The Data
     */
    public void setData(InfoModuleVideoDetailData data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.message);
    }

    public InfoModuleVideoDetail() {
    }

    protected InfoModuleVideoDetail(Parcel in) {
        this.returnCode = in.readString();
        this.data = in.readParcelable(InfoModuleVideoDetailData.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Parcelable.Creator<InfoModuleVideoDetail> CREATOR
            = new Parcelable.Creator<InfoModuleVideoDetail>() {
        @Override
        public InfoModuleVideoDetail createFromParcel(Parcel source) {
            return new InfoModuleVideoDetail(source);
        }

        @Override
        public InfoModuleVideoDetail[] newArray(int size) {
            return new InfoModuleVideoDetail[size];
        }
    };
}
