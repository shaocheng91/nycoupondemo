package com.nineyi.data.model.installment;

import android.os.Parcel;
import android.os.Parcelable;


public final class InstallmentServiceInfoData implements Parcelable {

    public InstallmentShopServiceInfoEntity ShopServiceInfoEntity;

    public InstallmentServiceInfoData() {
    }

    // Parcelable management
    private InstallmentServiceInfoData(Parcel in) {
        ShopServiceInfoEntity = InstallmentShopServiceInfoEntity.CREATOR.createFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        ShopServiceInfoEntity.writeToParcel(dest, flags);
    }

    public static final Parcelable.Creator<InstallmentServiceInfoData> CREATOR
            = new Parcelable.Creator<InstallmentServiceInfoData>() {
        public InstallmentServiceInfoData createFromParcel(Parcel in) {
            return new InstallmentServiceInfoData(in);
        }

        public InstallmentServiceInfoData[] newArray(int size) {
            return new InstallmentServiceInfoData[size];
        }
    };
}
