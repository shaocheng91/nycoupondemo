package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePageNotKeyProperty implements Parcelable {

    public String Title;
    public String[] ContentList;

    public SalePageNotKeyProperty() {
    }

    // Parcelable management
    private SalePageNotKeyProperty(Parcel in) {
        Title = in.readString();
        ContentList = in.createStringArray();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Title);
        dest.writeStringArray(ContentList);
    }

    public static final Parcelable.Creator<SalePageNotKeyProperty> CREATOR
            = new Parcelable.Creator<SalePageNotKeyProperty>() {
        public SalePageNotKeyProperty createFromParcel(Parcel in) {
            return new SalePageNotKeyProperty(in);
        }

        public SalePageNotKeyProperty[] newArray(int size) {
            return new SalePageNotKeyProperty[size];
        }
    };
}
