package com.nineyi.data.model.referee;

import android.os.Parcel;
import android.os.Parcelable;


public final class RefereeInsertInfo implements Parcelable {

    public String LocationName;
    public String Name;

    // Parcelable management
    private RefereeInsertInfo(Parcel in) {
        LocationName = in.readString();
        Name = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(LocationName);
        dest.writeString(Name);
    }

    public static final Creator<RefereeInsertInfo> CREATOR = new Creator<RefereeInsertInfo>() {
        public RefereeInsertInfo createFromParcel(Parcel in) {
            return new RefereeInsertInfo(in);
        }

        public RefereeInsertInfo[] newArray(int size) {
            return new RefereeInsertInfo[size];
        }
    };
}
