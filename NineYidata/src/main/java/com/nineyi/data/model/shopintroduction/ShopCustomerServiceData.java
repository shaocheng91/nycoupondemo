
package com.nineyi.data.model.shopintroduction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ShopCustomerServiceData implements Parcelable {

    @SerializedName("PhoneCRM")
    @Expose
    private String phoneCRM;
    @SerializedName("PhoneCRMExt")
    @Expose
    private String phoneCRMExt;
    @SerializedName("PhoneCRMTimeData")
    @Expose
    private String phoneCRMTimeData;
    @SerializedName("OnlineCRM")
    @Expose
    private String onlineCRM;
    @SerializedName("OnlineCRMCode")
    @Expose
    private String onlineCRMCode;
    @SerializedName("OnlineCRMTimeData")
    @Expose
    private String onlineCRMTimeData;

    public String getPhoneCRM() {
        return phoneCRM;
    }

    public String getPhoneCRMExt() {
        return phoneCRMExt;
    }

    public void setPhoneCRM(String phoneCRM) {
        this.phoneCRM = phoneCRM;
    }

    public String getPhoneCRMTimeData() {
        return phoneCRMTimeData;
    }

    public void setPhoneCRMTimeData(String phoneCRMTimeData) {
        this.phoneCRMTimeData = phoneCRMTimeData;
    }

    public String getOnlineCRM() {
        return onlineCRM;
    }

    public void setOnlineCRM(String onlineCRM) {
        this.onlineCRM = onlineCRM;
    }

    public String getOnlineCRMCode() {
        return onlineCRMCode;
    }

    public void setOnlineCRMCode(String onlineCRMCode) {
        this.onlineCRMCode = onlineCRMCode;
    }

    public String getOnlineCRMTimeData() {
        return onlineCRMTimeData;
    }

    public void setOnlineCRMTimeData(String onlineCRMTimeData) {
        this.onlineCRMTimeData = onlineCRMTimeData;
    }

    public ShopCustomerServiceData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phoneCRM);
        dest.writeString(this.phoneCRMExt);
        dest.writeString(this.phoneCRMTimeData);
        dest.writeString(this.onlineCRM);
        dest.writeString(this.onlineCRMCode);
        dest.writeString(this.onlineCRMTimeData);
    }

    protected ShopCustomerServiceData(Parcel in) {
        this.phoneCRM = in.readString();
        this.phoneCRMExt = in.readString();
        this.phoneCRMTimeData = in.readString();
        this.onlineCRM = in.readString();
        this.onlineCRMCode = in.readString();
        this.onlineCRMTimeData = in.readString();
    }

    public static final Creator<ShopCustomerServiceData> CREATOR = new Creator<ShopCustomerServiceData>() {
        @Override
        public ShopCustomerServiceData createFromParcel(Parcel source) {
            return new ShopCustomerServiceData(source);
        }

        @Override
        public ShopCustomerServiceData[] newArray(int size) {
            return new ShopCustomerServiceData[size];
        }
    };
}
