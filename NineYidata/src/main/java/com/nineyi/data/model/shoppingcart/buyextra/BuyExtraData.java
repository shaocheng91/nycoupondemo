package com.nineyi.data.model.shoppingcart.buyextra;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shaocheng on 2017/9/14.
 */
public class BuyExtraData implements Parcelable {

    private String Title;
    private List<SalePage> SalePageList = null;

    private BuyExtraData() {
    }

    private BuyExtraData(Builder builder) {
        Title = builder.Title;
        SalePageList = builder.SalePageList;
    }

    public String getTitle() {
        return Title;
    }

    public List<SalePage> getSalePageList() {
        return SalePageList;
    }

    public final static Creator<BuyExtraData> CREATOR = new Creator<BuyExtraData>() {

        @SuppressWarnings({
                "unchecked"
        })
        public BuyExtraData createFromParcel(Parcel in) {
            BuyExtraData instance = new BuyExtraData();
            instance.Title = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.SalePageList, (SalePage.class.getClassLoader()));
            return instance;
        }

        public BuyExtraData[] newArray(int size) {
            return (new BuyExtraData[size]);
        }

    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(Title);
        dest.writeList(SalePageList);
    }

    public int describeContents() {
        return 0;
    }

    public static final class Builder {
        private String Title;
        private List<SalePage> SalePageList;

        public Builder() {
        }

        public Builder Title(String val) {
            Title = val;
            return this;
        }

        public Builder SalePageList(List<SalePage> val) {
            SalePageList = val;
            return this;
        }

        public BuyExtraData build() {
            return new BuyExtraData(this);
        }
    }
}