package com.nineyi.data.model.trace;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


public final class TraceSalePageList implements Parcelable {

    public String msg;
    public List<TraceSalePage> data;

    public TraceSalePageList() {
    }

    // Parcelable management
    private TraceSalePageList(Parcel in) {
        msg = in.readString();
        data = in.createTypedArrayList(TraceSalePage.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(msg);
        dest.writeTypedList(data);
    }

    public static final Parcelable.Creator<TraceSalePageList> CREATOR = new Parcelable.Creator<TraceSalePageList>() {
        public TraceSalePageList createFromParcel(Parcel in) {
            return new TraceSalePageList(in);
        }

        public TraceSalePageList[] newArray(int size) {
            return new TraceSalePageList[size];
        }
    };
}
