package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class SalePagePromotionList implements Parcelable {

    @SerializedName("PromotionId")
    @Expose
    private Integer promotionId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("MatchedCondition")
    @Expose
    private MatchedCondition matchedCondition;
    @SerializedName("DiscountAllocated")
    @Expose
    private Integer discountAllocated;
    @SerializedName("DiscountAllocatedQty")
    @Expose
    private Integer discountAllocatedQty;
    @SerializedName("IsConditionMatched")
    @Expose
    private Boolean isConditionMatched;
    @SerializedName("PromotionConditionTypeDef")
    @Expose
    private String promotionConditionTypeDef;
    @SerializedName("PromotionConditionDiscountTypeDef")
    @Expose
    private String promotionConditionDiscountTypeDef;


    /**
     * @return The promotionId
     */
    public Integer getPromotionId() {
        return promotionId;
    }

    /**
     * @param promotionId The PromotionId
     */
    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The matchedCondition
     */
    public MatchedCondition getMatchedCondition() {
        return matchedCondition;
    }

    /**
     * @param matchedCondition The MatchedCondition
     */
    public void setMatchedCondition(MatchedCondition matchedCondition) {
        this.matchedCondition = matchedCondition;
    }

    /**
     * @return The discountAllocated
     */
    public Integer getDiscountAllocated() {
        return discountAllocated;
    }

    /**
     * @param discountAllocated The DiscountAllocated
     */
    public void setDiscountAllocated(Integer discountAllocated) {
        this.discountAllocated = discountAllocated;
    }

    /**
     * @return The discountAllocatedQty
     */
    public Integer getDiscountAllocatedQty() {
        return discountAllocatedQty;
    }

    /**
     * @param discountAllocatedQty The DiscountAllocatedQty
     */
    public void setDiscountAllocatedQty(Integer discountAllocatedQty) {
        this.discountAllocatedQty = discountAllocatedQty;
    }

    /**
     * @return The isConditionMatched
     */
    public Boolean getIsConditionMatched() {
        return isConditionMatched;
    }

    /**
     * @param isConditionMatched The IsConditionMatched
     */
    public void setIsConditionMatched(Boolean isConditionMatched) {
        this.isConditionMatched = isConditionMatched;
    }

    /**
     *
     * @return
     *     The promotionConditionTypeDef
     */
    public String getPromotionConditionTypeDef() {
        return promotionConditionTypeDef;
    }

    /**
     *
     * @param promotionConditionTypeDef
     *     The PromotionConditionTypeDef
     */
    public void setPromotionConditionTypeDef(String promotionConditionTypeDef) {
        this.promotionConditionTypeDef = promotionConditionTypeDef;
    }

    /**
     *
     * @return
     *     The promotionConditionDiscountTypeDef
     */
    public String getPromotionConditionDiscountTypeDef() {
        return promotionConditionDiscountTypeDef;
    }

    /**
     *
     * @param promotionConditionDiscountTypeDef
     *     The PromotionConditionDiscountTypeDef
     */
    public void setPromotionConditionDiscountTypeDef(String promotionConditionDiscountTypeDef) {
        this.promotionConditionDiscountTypeDef = promotionConditionDiscountTypeDef;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.promotionId);
        dest.writeString(this.title);
        dest.writeParcelable(this.matchedCondition, flags);
        dest.writeValue(this.discountAllocated);
        dest.writeValue(this.discountAllocatedQty);
        dest.writeValue(this.isConditionMatched);
        dest.writeString(this.promotionConditionTypeDef);
        dest.writeString(this.promotionConditionDiscountTypeDef);
    }

    protected SalePagePromotionList(Parcel in) {
        this.promotionId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.matchedCondition = in.readParcelable(MatchedCondition.class.getClassLoader());
        this.discountAllocated = (Integer) in.readValue(Integer.class.getClassLoader());
        this.discountAllocatedQty = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isConditionMatched = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.promotionConditionTypeDef = in.readString();
        this.promotionConditionDiscountTypeDef = in.readString();
    }

    public static final Parcelable.Creator<SalePagePromotionList> CREATOR = new Parcelable.Creator<SalePagePromotionList>() {
        @Override
        public SalePagePromotionList createFromParcel(Parcel source) {
            return new SalePagePromotionList(source);
        }

        @Override
        public SalePagePromotionList[] newArray(int size) {
            return new SalePagePromotionList[size];
        }
    };
}
