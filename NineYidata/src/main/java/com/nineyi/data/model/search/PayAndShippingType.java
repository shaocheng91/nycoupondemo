package com.nineyi.data.model.search;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/7/27.
 */
public class PayAndShippingType implements Parcelable {
    public String TypeDef;
    public String TypeDefDesc;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.TypeDef);
        dest.writeString(this.TypeDefDesc);
    }

    public PayAndShippingType() {
    }

    protected PayAndShippingType(Parcel in) {
        this.TypeDef = in.readString();
        this.TypeDefDesc = in.readString();
    }

    public static final Parcelable.Creator<PayAndShippingType> CREATOR = new Parcelable.Creator<PayAndShippingType>() {
        @Override
        public PayAndShippingType createFromParcel(Parcel source) {
            return new PayAndShippingType(source);
        }

        @Override
        public PayAndShippingType[] newArray(int size) {
            return new PayAndShippingType[size];
        }
    };
}
