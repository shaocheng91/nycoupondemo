package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SalePageShort implements Parcelable {
    @SerializedName("ShopId")
    public int ShopId;
    @SerializedName(value="SalePageId", alternate={"Id"})
    public int SalePageId;
    @SerializedName("Title")
    public String Title;
    @SerializedName("SubTitle")
    public String SubTitle;
    @SerializedName("Price")
    public double Price;
    @SerializedName("IsFav")
    public boolean IsFav;
    @SerializedName("SellingStartDateTime")
    public String SellingStartDateTime;
    @SerializedName("Sort")
    public int Sort;
    @SerializedName("PV")
    public int PV;
    @SerializedName("PicUrl")
    public String PicUrl;
    @SerializedName("PicList")
    public String[] PicList;
    @SerializedName("SuggestPrice")
    public double SuggestPrice;
    @SerializedName("ImageCount")
    public int ImageCount;
    @SerializedName("IsFreeFee")
    public boolean IsFreeFee;
    @SerializedName("IsSoldOut")
    public boolean IsSoldOut;
    @SerializedName("IsHasStoreDelivery")
    public boolean IsHasStoreDelivery;
    @SerializedName("IsInstallment")
    public boolean IsInstallment;
    @SerializedName("Tags")
    public ArrayList<SalePageTag> Tags;

    public SalePageShort() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ShopId);
        dest.writeInt(this.SalePageId);
        dest.writeString(this.Title);
        dest.writeString(this.SubTitle);
        dest.writeDouble(this.Price);
        dest.writeByte(this.IsFav ? (byte) 1 : (byte) 0);
        dest.writeString(this.SellingStartDateTime);
        dest.writeInt(this.Sort);
        dest.writeInt(this.PV);
        dest.writeString(this.PicUrl);
        dest.writeStringArray(this.PicList);
        dest.writeDouble(this.SuggestPrice);
        dest.writeInt(this.ImageCount);
        dest.writeByte(this.IsFreeFee ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsSoldOut ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsHasStoreDelivery ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsInstallment ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.Tags);
    }

    protected SalePageShort(Parcel in) {
        this.ShopId = in.readInt();
        this.SalePageId = in.readInt();
        this.Title = in.readString();
        this.SubTitle = in.readString();
        this.Price = in.readDouble();
        this.IsFav = in.readByte() != 0;
        this.SellingStartDateTime = in.readString();
        this.Sort = in.readInt();
        this.PV = in.readInt();
        this.PicUrl = in.readString();
        this.PicList = in.createStringArray();
        this.SuggestPrice = in.readDouble();
        this.ImageCount = in.readInt();
        this.IsFreeFee = in.readByte() != 0;
        this.IsSoldOut = in.readByte() != 0;
        this.IsHasStoreDelivery = in.readByte() != 0;
        this.IsInstallment = in.readByte() != 0;
        this.Tags = in.createTypedArrayList(SalePageTag.CREATOR);
    }

    public static final Creator<SalePageShort> CREATOR = new Creator<SalePageShort>() {
        @Override
        public SalePageShort createFromParcel(Parcel source) {
            return new SalePageShort(source);
        }

        @Override
        public SalePageShort[] newArray(int size) {
            return new SalePageShort[size];
        }
    };
}
