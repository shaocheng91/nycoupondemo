package com.nineyi.data.model.layout;

import android.os.Parcel;
import android.os.Parcelable;


public final class LayoutTemplateData implements Parcelable {

    public String Title;
    public String SlaveTitle;
    public String SalePageId;
    public String Link;
    public double Price;
    public double SuggestPrice;
    public LayoutPicturePath PicturePath;
    public LayoutTargetInfo TargetInfo;
    public String DisplayDef;
    public int Order;
    public String Act;
    public String Color;
    public int PicHeight;

    public LayoutTemplateData() {
    }

    // Parcelable management
    private LayoutTemplateData(Parcel in) {
        Title = in.readString();
        SlaveTitle = in.readString();
        SalePageId = in.readString();
        Link = in.readString();
        Price = in.readDouble();
        SuggestPrice = in.readDouble();
        PicturePath = PicturePath.CREATOR.createFromParcel(in);
        TargetInfo = TargetInfo.CREATOR.createFromParcel(in);
        DisplayDef = in.readString();
        Order = in.readInt();
        Act = in.readString();
        Color = in.readString();
        PicHeight = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Title);
        dest.writeString(SlaveTitle);
        dest.writeString(SalePageId);
        dest.writeString(Link);
        dest.writeDouble(Price);
        dest.writeDouble(SuggestPrice);
        PicturePath.writeToParcel(dest, flags);
        TargetInfo.writeToParcel(dest, flags);
        dest.writeString(DisplayDef);
        dest.writeInt(Order);
        dest.writeString(Act);
        dest.writeString(Color);
        dest.writeInt(PicHeight);
    }

    public static final Parcelable.Creator<LayoutTemplateData> CREATOR = new Parcelable.Creator<LayoutTemplateData>() {
        public LayoutTemplateData createFromParcel(Parcel in) {
            return new LayoutTemplateData(in);
        }

        public LayoutTemplateData[] newArray(int size) {
            return new LayoutTemplateData[size];
        }
    };
}
