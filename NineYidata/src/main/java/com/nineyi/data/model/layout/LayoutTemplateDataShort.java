package com.nineyi.data.model.layout;

import android.os.Parcel;
import android.os.Parcelable;


public final class LayoutTemplateDataShort implements Parcelable {

    public String ID;
    public int[] PicHeight;
    public int Count;

    public LayoutTemplateDataShort() {
    }

    // Parcelable management
    private LayoutTemplateDataShort(Parcel in) {
        ID = in.readString();
        PicHeight = in.createIntArray();
        Count = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeIntArray(PicHeight);
        dest.writeInt(Count);
    }

    public static final Parcelable.Creator<LayoutTemplateDataShort> CREATOR
            = new Parcelable.Creator<LayoutTemplateDataShort>() {
        public LayoutTemplateDataShort createFromParcel(Parcel in) {
            return new LayoutTemplateDataShort(in);
        }

        public LayoutTemplateDataShort[] newArray(int size) {
            return new LayoutTemplateDataShort[size];
        }
    };
}
