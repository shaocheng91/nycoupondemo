package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class VipKeyInDropDownEntity implements Parcelable {

    private static final String FIELD_VALUE = "Value";
    private static final String FIELD_DESC = "Desc";


    @SerializedName(FIELD_VALUE)
    private String mValue;
    @SerializedName(FIELD_DESC)
    private String mDesc;


    public VipKeyInDropDownEntity() {

    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public String getDesc() {
        return mDesc;
    }

    public VipKeyInDropDownEntity(Parcel in) {
        mValue = in.readString();
        mDesc = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipKeyInDropDownEntity> CREATOR = new Creator<VipKeyInDropDownEntity>() {
        public VipKeyInDropDownEntity createFromParcel(Parcel in) {
            return new VipKeyInDropDownEntity(in);
        }

        public VipKeyInDropDownEntity[] newArray(int size) {
            return new VipKeyInDropDownEntity[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mValue);
        dest.writeString(mDesc);
    }


}