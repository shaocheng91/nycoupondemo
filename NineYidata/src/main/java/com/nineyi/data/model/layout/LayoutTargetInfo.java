package com.nineyi.data.model.layout;

import android.os.Parcel;
import android.os.Parcelable;


public final class LayoutTargetInfo implements Parcelable {

    public String TargetType;
    public String TargetId;
    public String TargetProperty;
    public String TargetAct;
    public String TargetPosition;

    public LayoutTargetInfo() {
    }

    // Parcelable management
    private LayoutTargetInfo(Parcel in) {
        TargetType = in.readString();
        TargetId = in.readString();
        TargetProperty = in.readString();
        TargetAct = in.readString();
        TargetPosition = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(TargetType);
        dest.writeString(TargetId);
        dest.writeString(TargetProperty);
        dest.writeString(TargetAct);
        dest.writeString(TargetPosition);
    }

    public static final Parcelable.Creator<LayoutTargetInfo> CREATOR = new Parcelable.Creator<LayoutTargetInfo>() {
        public LayoutTargetInfo createFromParcel(Parcel in) {
            return new LayoutTargetInfo(in);
        }

        public LayoutTargetInfo[] newArray(int size) {
            return new LayoutTargetInfo[size];
        }
    };
}
