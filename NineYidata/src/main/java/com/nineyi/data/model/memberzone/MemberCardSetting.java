package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/11/7.
 */
public class MemberCardSetting implements Parcelable {
    public String Description;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Description);
    }

    public MemberCardSetting() {
    }

    protected MemberCardSetting(Parcel in) {
        this.Description = in.readString();
    }

    public static final Parcelable.Creator<MemberCardSetting> CREATOR = new Parcelable.Creator<MemberCardSetting>() {
        @Override
        public MemberCardSetting createFromParcel(Parcel source) {
            return new MemberCardSetting(source);
        }

        @Override
        public MemberCardSetting[] newArray(int size) {
            return new MemberCardSetting[size];
        }
    };
}
