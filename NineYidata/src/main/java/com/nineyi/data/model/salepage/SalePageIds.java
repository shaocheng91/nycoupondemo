package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePageIds implements Parcelable {

    public int[] ids;

    public SalePageIds() {
    }

    // Parcelable management
    private SalePageIds(Parcel in) {
        ids = in.createIntArray();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeIntArray(ids);
    }

    public static final Parcelable.Creator<SalePageIds> CREATOR = new Parcelable.Creator<SalePageIds>() {
        public SalePageIds createFromParcel(Parcel in) {
            return new SalePageIds(in);
        }

        public SalePageIds[] newArray(int size) {
            return new SalePageIds[size];
        }
    };
}
