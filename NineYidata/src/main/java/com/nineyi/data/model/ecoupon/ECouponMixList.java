package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;


public final class ECouponMixList implements Parcelable {

    public String StatusTypeDef;
    public boolean IsUsing;
    public long Id;
    public String TypeDef;
    public int ShopId;
    public String ECouponTypeDef;
    public String StatusUpdateDateTime;
    public boolean IsMix;
    public String Code;
    public boolean IsTake;
    public boolean isGift;
    public int DiscountPrice;
    public long ECouponId;
    public long MemberId;
    public String StartDateTime;
    public String EndDateTime;
    public String UsingEndDateTime;
    public String UsingStartDateTime;

    public ECouponMixList() {
    }

    // Parcelable management
    private ECouponMixList(Parcel in) {
        StatusTypeDef = in.readString();
        IsUsing = in.readInt() == 1;
        Id = in.readLong();
        TypeDef = in.readString();
        ShopId = in.readInt();
        ECouponTypeDef = in.readString();
        StatusUpdateDateTime = in.readString();
        IsMix = in.readInt() == 1;
        Code = in.readString();
        IsTake = in.readInt() == 1;
        isGift = in.readInt() == 1;
        DiscountPrice = in.readInt();
        ECouponId = in.readLong();
        MemberId = in.readLong();
        StartDateTime = in.readString();
        EndDateTime = in.readString();
        UsingEndDateTime = in.readString();
        UsingStartDateTime = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(StatusTypeDef);
        dest.writeInt(IsUsing ? 1 : 0);
        dest.writeLong(Id);
        dest.writeString(TypeDef);
        dest.writeInt(ShopId);
        dest.writeString(ECouponTypeDef);
        dest.writeString(StatusUpdateDateTime);
        dest.writeInt(IsMix ? 1 : 0);
        dest.writeString(Code);
        dest.writeInt(IsTake ? 1 : 0);
        dest.writeInt(isGift ? 1 : 0);
        dest.writeInt(DiscountPrice);
        dest.writeLong(ECouponId);
        dest.writeLong(MemberId);
        dest.writeString(StartDateTime);
        dest.writeString(EndDateTime);
        dest.writeString(UsingEndDateTime);
        dest.writeString(UsingStartDateTime);
    }

    public static final Parcelable.Creator<ECouponMixList> CREATOR = new Parcelable.Creator<ECouponMixList>() {
        public ECouponMixList createFromParcel(Parcel in) {
            return new ECouponMixList(in);
        }

        public ECouponMixList[] newArray(int size) {
            return new ECouponMixList[size];
        }
    };
}
