package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;


public final class PhpCouponShopElement extends AbstractBaseModel implements Parcelable {

    public String getTitle() {
        return title;
    }

    public String title;
    public int theme_color;

    public PhpCouponShopElement() {
    }

    // Parcelable management
    private PhpCouponShopElement(Parcel in) {
        title = in.readString();
        theme_color = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(theme_color);
    }

    public static final Parcelable.Creator<PhpCouponShopElement> CREATOR
            = new Parcelable.Creator<PhpCouponShopElement>() {
        public PhpCouponShopElement createFromParcel(Parcel in) {
            return new PhpCouponShopElement(in);
        }

        public PhpCouponShopElement[] newArray(int size) {
            return new PhpCouponShopElement[size];
        }
    };
}
