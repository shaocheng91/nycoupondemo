package com.nineyi.data.model.ranking;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class SaleRanking implements Parcelable {

    public String ReturnCode;
    public ArrayList<SaleRankingData> data;

    public SaleRanking() {
    }

    // Parcelable management
    private SaleRanking(Parcel in) {
        ReturnCode = in.readString();
        data = in.createTypedArrayList(SaleRankingData.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        dest.writeTypedList(data);
    }

    public static final Parcelable.Creator<SaleRanking> CREATOR = new Parcelable.Creator<SaleRanking>() {
        public SaleRanking createFromParcel(Parcel in) {
            return new SaleRanking(in);
        }

        public SaleRanking[] newArray(int size) {
            return new SaleRanking[size];
        }
    };
}
