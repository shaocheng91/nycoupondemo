package com.nineyi.data.model.promotion.v2;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by tedliang on 2016/8/22.
 */
public class PromotionConditionList implements Parcelable {

    private int TotalQty;
    private int TotalPrice;
    private int DiscountPrice;
    private double DiscountRate;

    public int getTotalQty() {
        return TotalQty;
    }

    public void setTotalQty(int TotalQty) {
        this.TotalQty = TotalQty;
    }

    public int getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(int TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    public int getDiscountPrice() {
        return DiscountPrice;
    }

    public void setDiscountPrice(int DiscountPrice) {
        this.DiscountPrice = DiscountPrice;
    }

    public double getDiscountRate() {
        return DiscountRate;
    }

    public void setDiscountRate(double DiscountRate) {
        this.DiscountRate = DiscountRate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.TotalQty);
        dest.writeInt(this.TotalPrice);
        dest.writeInt(this.DiscountPrice);
        dest.writeDouble(this.DiscountRate);
    }

    public PromotionConditionList() {
    }

    protected PromotionConditionList(Parcel in) {
        this.TotalQty = in.readInt();
        this.TotalPrice = in.readInt();
        this.DiscountPrice = in.readInt();
        this.DiscountRate = in.readDouble();
    }

    public static final Parcelable.Creator<PromotionConditionList> CREATOR
            = new Parcelable.Creator<PromotionConditionList>() {
        @Override
        public PromotionConditionList createFromParcel(Parcel source) {
            return new PromotionConditionList(source);
        }

        @Override
        public PromotionConditionList[] newArray(int size) {
            return new PromotionConditionList[size];
        }
    };
}
