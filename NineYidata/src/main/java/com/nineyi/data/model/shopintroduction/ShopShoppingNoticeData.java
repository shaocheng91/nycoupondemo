
package com.nineyi.data.model.shopintroduction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ShopShoppingNoticeData implements Parcelable {

    @SerializedName("ShopId")
    @Expose
    private Integer shopId;
    @SerializedName("PaymentContent")
    @Expose
    private String paymentContent;
    @SerializedName("ShippingContent")
    @Expose
    private String shippingContent;
    @SerializedName("ChangeReturnContent")
    @Expose
    private String changeReturnContent;
    @SerializedName("CustomTitle1")
    @Expose
    private String customTitle1;
    @SerializedName("CustomContent1")
    @Expose
    private String customContent1;
    @SerializedName("CustomTitle2")
    @Expose
    private String customTitle2;
    @SerializedName("CustomContent2")
    @Expose
    private String customContent2;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getPaymentContent() {
        return paymentContent;
    }

    public void setPaymentContent(String paymentContent) {
        this.paymentContent = paymentContent;
    }

    public String getShippingContent() {
        return shippingContent;
    }

    public void setShippingContent(String shippingContent) {
        this.shippingContent = shippingContent;
    }

    public String getChangeReturnContent() {
        return changeReturnContent;
    }

    public void setChangeReturnContent(String changeReturnContent) {
        this.changeReturnContent = changeReturnContent;
    }

    public String getCustomTitle1() {
        return customTitle1;
    }

    public void setCustomTitle1(String customTitle1) {
        this.customTitle1 = customTitle1;
    }

    public String getCustomContent1() {
        return customContent1;
    }

    public void setCustomContent1(String customContent1) {
        this.customContent1 = customContent1;
    }

    public String getCustomTitle2() {
        return customTitle2;
    }

    public void setCustomTitle2(String customTitle2) {
        this.customTitle2 = customTitle2;
    }

    public String getCustomContent2() {
        return customContent2;
    }

    public void setCustomContent2(String customContent2) {
        this.customContent2 = customContent2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.shopId);
        dest.writeString(this.paymentContent);
        dest.writeString(this.shippingContent);
        dest.writeString(this.changeReturnContent);
        dest.writeString(this.customTitle1);
        dest.writeString(this.customContent1);
        dest.writeString(this.customTitle2);
        dest.writeString(this.customContent2);
    }

    public ShopShoppingNoticeData() {
    }

    protected ShopShoppingNoticeData(Parcel in) {
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.paymentContent = in.readString();
        this.shippingContent = in.readString();
        this.changeReturnContent = in.readString();
        this.customTitle1 = in.readString();
        this.customContent1 = in.readString();
        this.customTitle2 = in.readString();
        this.customContent2 = in.readString();
    }

    public static final Parcelable.Creator<ShopShoppingNoticeData> CREATOR
            = new Parcelable.Creator<ShopShoppingNoticeData>() {
        @Override
        public ShopShoppingNoticeData createFromParcel(Parcel source) {
            return new ShopShoppingNoticeData(source);
        }

        @Override
        public ShopShoppingNoticeData[] newArray(int size) {
            return new ShopShoppingNoticeData[size];
        }
    };
}
