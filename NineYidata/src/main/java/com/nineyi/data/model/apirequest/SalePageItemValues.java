/**
 * Data holder for 'TraceSalePageList/InsertItem' and 
 *
 * 'ShoppingCartV2/RemoveItem' web api call
 *
 */
package com.nineyi.data.model.apirequest;

import android.os.Parcel;
import android.os.Parcelable;

import com.nineyi.data.model.shoppingcart.ShoppingCartSalePageV3;


public final class SalePageItemValues implements Parcelable {

    public String shopId;
    public String salePageId;
    public ShoppingCartSalePageV3 theSalePage; // for 購物車 trace then remove

    public SalePageItemValues() {
    }

    /** Parcelable management * */
    private SalePageItemValues(Parcel in) {
        shopId = in.readString();
        salePageId = in.readString();
        theSalePage = (ShoppingCartSalePageV3) in.readValue(ShoppingCartSalePageV3.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shopId);
        dest.writeString(salePageId);
        dest.writeValue(theSalePage);
    }

    public static final Parcelable.Creator<SalePageItemValues> CREATOR = new Parcelable.Creator<SalePageItemValues>() {
        public SalePageItemValues createFromParcel(Parcel in) {
            return new SalePageItemValues(in);
        }

        public SalePageItemValues[] newArray(int size) {
            return new SalePageItemValues[size];
        }
    };
}