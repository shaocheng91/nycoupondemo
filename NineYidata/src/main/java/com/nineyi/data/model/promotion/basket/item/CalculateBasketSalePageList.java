
package com.nineyi.data.model.promotion.basket.item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class CalculateBasketSalePageList implements Parcelable {

    @SerializedName("SalePageId")
    @Expose
    private long salePageId;
    @SerializedName("SaleProductSKUId")
    @Expose
    private long saleProductSKUId;
    @SerializedName("Qty")
    @Expose
    private int qty;
    @SerializedName("Price")
    @Expose
    private double price;

    /**
     * 
     * @return
     *     The salePageId
     */
    public long getSalePageId() {
        return salePageId;
    }

    /**
     * 
     * @param salePageId
     *     The SalePageId
     */
    public void setSalePageId(long salePageId) {
        this.salePageId = salePageId;
    }

    /**
     * 
     * @return
     *     The saleProductSKUId
     */
    public long getSaleProductSKUId() {
        return saleProductSKUId;
    }

    /**
     * 
     * @param saleProductSKUId
     *     The SaleProductSKUId
     */
    public void setSaleProductSKUId(long saleProductSKUId) {
        this.saleProductSKUId = saleProductSKUId;
    }

    /**
     * 
     * @return
     *     The qty
     */
    public int getQty() {
        return qty;
    }

    /**
     * 
     * @param qty
     *     The Qty
     */
    public void setQty(int qty) {
        this.qty = qty;
    }

    /**
     * 
     * @return
     *     The price
     */
    public double getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The Price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.salePageId);
        dest.writeLong(this.saleProductSKUId);
        dest.writeInt(this.qty);
        dest.writeDouble(this.price);
    }

    public CalculateBasketSalePageList() {
    }

    protected CalculateBasketSalePageList(Parcel in) {
        this.salePageId = in.readLong();
        this.saleProductSKUId = in.readLong();
        this.qty = in.readInt();
        this.price = in.readDouble();
    }

    public static final Parcelable.Creator<CalculateBasketSalePageList> CREATOR
            = new Parcelable.Creator<CalculateBasketSalePageList>() {
        @Override
        public CalculateBasketSalePageList createFromParcel(Parcel source) {
            return new CalculateBasketSalePageList(source);
        }

        @Override
        public CalculateBasketSalePageList[] newArray(int size) {
            return new CalculateBasketSalePageList[size];
        }
    };
}
