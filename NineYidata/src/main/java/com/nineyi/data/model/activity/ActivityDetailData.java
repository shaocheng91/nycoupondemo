package com.nineyi.data.model.activity;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by iris on 15/6/23.
 */
public class ActivityDetailData implements Parcelable {

    private static final String FIELD_SHOP_ID = "Activity_ShopId";
    private static final String FIELD_SHOP_NAME = "Activity_ShopName";
    private static final String FIELD_ACTIVITYID = "Activity_Id";
    private static final String FIELD_ACTIVITYNAME = "Activity_Name";
    private static final String FIELD_ACTIVITYURL = "Activity_Url";
    private static final String FIELD_ISOFFICIAL = "IsOfficial";

    @SerializedName(FIELD_SHOP_ID)
    private String mShopId;

    @SerializedName(FIELD_SHOP_NAME)
    private String mShopName;

    @SerializedName(FIELD_ACTIVITYID)
    private String mActivityId;

    @SerializedName(FIELD_ACTIVITYNAME)
    private String mActivityName;

    @SerializedName(FIELD_ACTIVITYURL)
    private String mActivityUrl;

    @SerializedName(FIELD_ISOFFICIAL)
    private boolean mIsOfficial;



    public ActivityDetailData() {
    }

    public String getShopId() {
        return mShopId;
    }

    public String getShopName() {
        return mShopName;
    }

    public String getActivityId() {
        return mActivityId;
    }

    public String getActivityName() {
        return mActivityName;
    }

    public String getActivityUrl() {
        return mActivityUrl;
    }

    public boolean getIsOfficial() {
        return mIsOfficial;
    }

    public ActivityDetailData(Parcel in) {
        mShopId = in.readString();
        mShopName = in.readString();
        mActivityId = in.readString();
        mActivityName = in.readString();
        mActivityUrl = in.readString();
        mIsOfficial = in.readInt() == 1;
    }

    public static final Creator<ActivityDetailData> CREATOR = new Creator<ActivityDetailData>() {
        public ActivityDetailData createFromParcel(Parcel in) {
            return new ActivityDetailData(in);
        }

        public ActivityDetailData[] newArray(int size) {
            return new ActivityDetailData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mShopId);
        dest.writeString(mShopName);
        dest.writeString(mActivityId);
        dest.writeString(mActivityName);
        dest.writeString(mActivityUrl);
        dest.writeInt(mIsOfficial ? 1 : 0);
    }

}