package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/10/21.
 */
public class CrmMemberTierData implements Parcelable {
    public String ReturnCode;
    public CrmMemberTier Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.Message);
    }

    public CrmMemberTierData() {
    }

    protected CrmMemberTierData(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(CrmMemberTier.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<CrmMemberTierData> CREATOR = new Parcelable.Creator<CrmMemberTierData>() {
        @Override
        public CrmMemberTierData createFromParcel(Parcel source) {
            return new CrmMemberTierData(source);
        }

        @Override
        public CrmMemberTierData[] newArray(int size) {
            return new CrmMemberTierData[size];
        }
    };
}
