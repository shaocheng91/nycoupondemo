package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBPaging implements Parcelable {

    FBCursors cursors;

    protected FBPaging(Parcel in) {
        cursors = (FBCursors) in.readValue(FBCursors.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cursors);
    }

    public static final Parcelable.Creator<FBPaging> CREATOR = new Parcelable.Creator<FBPaging>() {
        @Override
        public FBPaging createFromParcel(Parcel in) {
            return new FBPaging(in);
        }

        @Override
        public FBPaging[] newArray(int size) {
            return new FBPaging[size];
        }
    };
}