package com.nineyi.data.model.openapp;

import com.nineyi.data.model.php.AbstractBaseModel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by daisishu on 15/3/19.
 */
public final class AppNotificationData extends AbstractBaseModel implements Parcelable {

    public String Status;
    public String OfflineMessage;

    public AppNotificationData() {
    }

    // Parcelable management
    private AppNotificationData(Parcel in) {
        Status = in.readString();
        OfflineMessage = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Status);
        dest.writeString(OfflineMessage);
    }

    public static final Creator<AppNotificationData> CREATOR = new Creator<AppNotificationData>() {
        public AppNotificationData createFromParcel(Parcel in) {
            return new AppNotificationData(in);
        }

        public AppNotificationData[] newArray(int size) {
            return new AppNotificationData[size];
        }
    };
}
