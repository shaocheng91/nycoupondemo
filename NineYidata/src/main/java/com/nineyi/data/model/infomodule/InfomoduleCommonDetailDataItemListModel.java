package com.nineyi.data.model.infomodule;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleCommonDetailDataItemListModel implements Parcelable {

    public int SalePageId;
    public String SubTitle;
    public String Title;
    public double SuggestPrice;
    public double Price;
    public int Sort;
    public int ShopId;
    public String SellingStartDateTime;
    public ArrayList<String> Tags;
    public ArrayList<String> PicList;
    public String PicUrl;

    public InfomoduleCommonDetailDataItemListModel(Parcel in) {
        SalePageId = in.readInt();
        SubTitle = in.readString();
        Title = in.readString();
        SuggestPrice = in.readDouble();
        Price = in.readDouble();
        Sort = in.readInt();
        ShopId = in.readInt();
        SellingStartDateTime = in.readString();
        Tags = in.createStringArrayList();
        PicList = in.createStringArrayList();
        PicUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SalePageId);
        dest.writeString(SubTitle);
        dest.writeString(Title);
        dest.writeDouble(SuggestPrice);
        dest.writeDouble(Price);
        dest.writeInt(Sort);
        dest.writeInt(ShopId);
        dest.writeString(SellingStartDateTime);
        dest.writeStringList(Tags);
        dest.writeStringList(PicList);
        dest.writeString(PicUrl);
    }

    public static Parcelable.Creator<InfomoduleCommonDetailDataItemListModel> CREATOR
            = new Parcelable.Creator<InfomoduleCommonDetailDataItemListModel>() {
        @Override
        public InfomoduleCommonDetailDataItemListModel createFromParcel(Parcel source) {
            return new InfomoduleCommonDetailDataItemListModel(source);
        }

        @Override
        public InfomoduleCommonDetailDataItemListModel[] newArray(int size) {
            return new InfomoduleCommonDetailDataItemListModel[size];
        }
    };
}
