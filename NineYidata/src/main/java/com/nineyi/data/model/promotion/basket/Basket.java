
package com.nineyi.data.model.promotion.basket;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class Basket implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private BasketData data;
    @SerializedName("Message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The returnCode
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * 
     * @param returnCode
     *     The ReturnCode
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * 
     * @return
     *     The data
     */
    public BasketData getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The Data
     */
    public void setData(BasketData data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.message);
    }

    public Basket() {
    }

    protected Basket(Parcel in) {
        this.returnCode = in.readString();
        this.data = in.readParcelable(BasketData.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Parcelable.Creator<Basket> CREATOR = new Parcelable.Creator<Basket>() {
        @Override
        public Basket createFromParcel(Parcel source) {
            return new Basket(source);
        }

        @Override
        public Basket[] newArray(int size) {
            return new Basket[size];
        }
    };
}
