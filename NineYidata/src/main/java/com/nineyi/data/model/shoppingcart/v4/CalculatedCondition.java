
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class CalculatedCondition implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Long id;
//    @SerializedName("TypeDef")
//    @Expose
//    private Object typeDef;
    @SerializedName("TotalQty")
    @Expose
    private Long totalQty;
    @SerializedName("TotalPrice")
    @Expose
    private Long totalPrice;
//    @SerializedName("DiscountTypeDef")
//    @Expose
//    private Object discountTypeDef;
    @SerializedName("DiscountPrice")
    @Expose
    private Double discountPrice;
    @SerializedName("DiscountRate")
    @Expose
    private Long discountRate;
    @SerializedName("ReachQtyCount")
    @Expose
    private Long reachQtyCount;
    @SerializedName("TotalPayment")
    @Expose
    private Double totalPayment;
    @SerializedName("IsMatched")
    @Expose
    private Boolean isMatched;
    @SerializedName("SalePageTotalQty")
    @Expose
    private Long salePageTotalQty;
//    @SerializedName("PromotionFreeGiftList")
//    @Expose
//    private List<Object> promotionFreeGiftList = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The Id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The totalQty
     */
    public Long getTotalQty() {
        return totalQty;
    }

    /**
     * 
     * @param totalQty
     *     The TotalQty
     */
    public void setTotalQty(Long totalQty) {
        this.totalQty = totalQty;
    }

    /**
     * 
     * @return
     *     The totalPrice
     */
    public Long getTotalPrice() {
        return totalPrice;
    }

    /**
     * 
     * @param totalPrice
     *     The TotalPrice
     */
    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * 
     * @return
     *     The discountPrice
     */
    public Double getDiscountPrice() {
        return discountPrice;
    }

    /**
     * 
     * @param discountPrice
     *     The DiscountPrice
     */
    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    /**
     * 
     * @return
     *     The discountRate
     */
    public Long getDiscountRate() {
        return discountRate;
    }

    /**
     * 
     * @param discountRate
     *     The DiscountRate
     */
    public void setDiscountRate(Long discountRate) {
        this.discountRate = discountRate;
    }

    /**
     * 
     * @return
     *     The reachQtyCount
     */
    public Long getReachQtyCount() {
        return reachQtyCount;
    }

    /**
     * 
     * @param reachQtyCount
     *     The ReachQtyCount
     */
    public void setReachQtyCount(Long reachQtyCount) {
        this.reachQtyCount = reachQtyCount;
    }

    /**
     * 
     * @return
     *     The totalPayment
     */
    public Double getTotalPayment() {
        return totalPayment;
    }

    /**
     * 
     * @param totalPayment
     *     The TotalPayment
     */
    public void setTotalPayment(Double totalPayment) {
        this.totalPayment = totalPayment;
    }

    /**
     * 
     * @return
     *     The isMatched
     */
    public Boolean getIsMatched() {
        return isMatched;
    }

    /**
     * 
     * @param isMatched
     *     The IsMatched
     */
    public void setIsMatched(Boolean isMatched) {
        this.isMatched = isMatched;
    }

    /**
     * 
     * @return
     *     The salePageTotalQty
     */
    public Long getSalePageTotalQty() {
        return salePageTotalQty;
    }

    /**
     * 
     * @param salePageTotalQty
     *     The SalePageTotalQty
     */
    public void setSalePageTotalQty(Long salePageTotalQty) {
        this.salePageTotalQty = salePageTotalQty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
//        dest.writeParcelable(this.typeDef, flags);
        dest.writeValue(this.totalQty);
        dest.writeValue(this.totalPrice);
//        dest.writeParcelable(this.discountTypeDef, flags);
        dest.writeValue(this.discountPrice);
        dest.writeValue(this.discountRate);
        dest.writeValue(this.reachQtyCount);
        dest.writeValue(this.totalPayment);
        dest.writeValue(this.isMatched);
        dest.writeValue(this.salePageTotalQty);
//        dest.writeList(this.promotionFreeGiftList);
    }

    public CalculatedCondition() {
    }

    protected CalculatedCondition(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
//        this.typeDef = in.readParcelable(Object.class.getClassLoader());
        this.totalQty = (Long) in.readValue(Long.class.getClassLoader());
        this.totalPrice = (Long) in.readValue(Long.class.getClassLoader());
//        this.discountTypeDef = in.readParcelable(Object.class.getClassLoader());
        this.discountPrice = (Double) in.readValue(Long.class.getClassLoader());
        this.discountRate = (Long) in.readValue(Long.class.getClassLoader());
        this.reachQtyCount = (Long) in.readValue(Long.class.getClassLoader());
        this.totalPayment = (Double) in.readValue(Double.class.getClassLoader());
        this.isMatched = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.salePageTotalQty = (Long) in.readValue(Long.class.getClassLoader());
//        in.readList(this.promotionFreeGiftList, Object.class.getClassLoader());
    }

    public static final Parcelable.Creator<CalculatedCondition> CREATOR
            = new Parcelable.Creator<CalculatedCondition>() {
        @Override
        public CalculatedCondition createFromParcel(Parcel source) {
            return new CalculatedCondition(source);
        }

        @Override
        public CalculatedCondition[] newArray(int size) {
            return new CalculatedCondition[size];
        }
    };
}
