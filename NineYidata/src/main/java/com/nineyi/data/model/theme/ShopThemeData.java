package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeData implements Parcelable {
    public int ShopId;
    public int ThemeId;
    public String ThemeName;
    public String DeviceMode;
    public HashMap ThemeConfigList;
    public ArrayList<ShopThemeTemplate> TemplateList;

    public ShopThemeData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ShopId);
        dest.writeInt(this.ThemeId);
        dest.writeString(this.ThemeName);
        dest.writeString(this.DeviceMode);
        dest.writeSerializable(this.ThemeConfigList);
        dest.writeTypedList(this.TemplateList);
    }

    protected ShopThemeData(Parcel in) {
        this.ShopId = in.readInt();
        this.ThemeId = in.readInt();
        this.ThemeName = in.readString();
        this.DeviceMode = in.readString();
        this.ThemeConfigList = (HashMap) in.readSerializable();
        this.TemplateList = in.createTypedArrayList(ShopThemeTemplate.CREATOR);
    }

    public static final Creator<ShopThemeData> CREATOR = new Creator<ShopThemeData>() {
        @Override
        public ShopThemeData createFromParcel(Parcel source) {
            return new ShopThemeData(source);
        }

        @Override
        public ShopThemeData[] newArray(int size) {
            return new ShopThemeData[size];
        }
    };
}
