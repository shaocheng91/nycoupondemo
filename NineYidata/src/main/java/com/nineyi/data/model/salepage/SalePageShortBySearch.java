package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class SalePageShortBySearch implements Parcelable {

    public int ShopId;
    public int Id;
    public String Title;
    public String SubTitle;
    public double Price;
    public boolean IsFav;
    public String SellingStartDateTime;
    public int Sort;
    public int PV;
    public String PicUrl;
    public String[] PicList;
    public double SuggestPrice;
    public int ImageCount;
    public boolean IsFreeFee;
    public boolean IsHasStoreDelivery;
    public boolean IsHasLowTemperature;
    public boolean IsHasPromotion;
    public boolean IsInstallment;
    public boolean IsZeroInstallmentRate;
    public String WebSiteDefSet;
    public ArrayList<SalePageTag> Tags;
    public boolean IsSoldOut;

    public SalePageShortBySearch() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ShopId);
        dest.writeInt(this.Id);
        dest.writeString(this.Title);
        dest.writeString(this.SubTitle);
        dest.writeDouble(this.Price);
        dest.writeByte(this.IsFav ? (byte) 1 : (byte) 0);
        dest.writeString(this.SellingStartDateTime);
        dest.writeInt(this.Sort);
        dest.writeInt(this.PV);
        dest.writeString(this.PicUrl);
        dest.writeStringArray(this.PicList);
        dest.writeDouble(this.SuggestPrice);
        dest.writeInt(this.ImageCount);
        dest.writeByte(this.IsFreeFee ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsHasStoreDelivery ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsHasLowTemperature ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsHasPromotion ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsInstallment ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsZeroInstallmentRate ? (byte) 1 : (byte) 0);
        dest.writeString(this.WebSiteDefSet);
        dest.writeTypedList(this.Tags);
        dest.writeByte(this.IsSoldOut ? (byte) 1 : (byte) 0);
    }

    protected SalePageShortBySearch(Parcel in) {
        this.ShopId = in.readInt();
        this.Id = in.readInt();
        this.Title = in.readString();
        this.SubTitle = in.readString();
        this.Price = in.readDouble();
        this.IsFav = in.readByte() != 0;
        this.SellingStartDateTime = in.readString();
        this.Sort = in.readInt();
        this.PV = in.readInt();
        this.PicUrl = in.readString();
        this.PicList = in.createStringArray();
        this.SuggestPrice = in.readDouble();
        this.ImageCount = in.readInt();
        this.IsFreeFee = in.readByte() != 0;
        this.IsHasStoreDelivery = in.readByte() != 0;
        this.IsHasLowTemperature = in.readByte() != 0;
        this.IsHasPromotion = in.readByte() != 0;
        this.IsInstallment = in.readByte() != 0;
        this.IsZeroInstallmentRate = in.readByte() != 0;
        this.WebSiteDefSet = in.readString();
        this.Tags = in.createTypedArrayList(SalePageTag.CREATOR);
        this.IsSoldOut = in.readByte() != 0;
    }

    public static final Creator<SalePageShortBySearch> CREATOR = new Creator<SalePageShortBySearch>() {
        @Override
        public SalePageShortBySearch createFromParcel(Parcel source) {
            return new SalePageShortBySearch(source);
        }

        @Override
        public SalePageShortBySearch[] newArray(int size) {
            return new SalePageShortBySearch[size];
        }
    };
}
