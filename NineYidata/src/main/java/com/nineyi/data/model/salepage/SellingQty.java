package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SellingQty implements Parcelable {

    public int GoodsSKUId;
    public int SellingQty;
    public int SaleProductSKUId;

    public SellingQty() {
    }

    // Parcelable management
    private SellingQty(Parcel in) {
        GoodsSKUId = in.readInt();
        SellingQty = in.readInt();
        SaleProductSKUId = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(GoodsSKUId);
        dest.writeInt(SellingQty);
        dest.writeInt(SaleProductSKUId);
    }

    public static final Parcelable.Creator<SellingQty> CREATOR = new Parcelable.Creator<SellingQty>() {
        public SellingQty createFromParcel(Parcel in) {
            return new SellingQty(in);
        }

        public SellingQty[] newArray(int size) {
            return new SellingQty[size];
        }
    };
}
