package com.nineyi.data.model.activity;

import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.category.MallCategory;
import com.nineyi.data.model.referee.EmployeeDataRoot;
import com.nineyi.data.model.referee.RefereeEmployeeListInfo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by iris on 15/6/23.
 */
public class ActivityList implements Parcelable {

    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_DATA)
    private ArrayList<ActivityListData> mDatum;

    public String getReturnCode() {
        return mReturnCode;
    }

    public ActivityList(Parcel in) {
        mMessage = in.readString();
        mReturnCode = in.readString();
        mDatum = in.createTypedArrayList(ActivityListData.CREATOR);
    }

    public ArrayList<ActivityListData> getActivityListData() {
        return mDatum;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mMessage);
        parcel.writeString(mReturnCode);
        parcel.writeTypedList(mDatum);
    }

    public static final Parcelable.Creator<ActivityList> CREATOR = new Parcelable.Creator<ActivityList>() {
        public ActivityList createFromParcel(Parcel in) {
            return new ActivityList(in);
        }

        public ActivityList[] newArray(int size) {
            return new ActivityList[size];
        }
    };
}
