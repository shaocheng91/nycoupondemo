package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;


public final class SalePageTag implements Parcelable {

    public String Color;
    public String TypeDef;
    public String TypeDefDesc;
    public boolean IsMatchCondition;

    public SalePageTag() {
    }

    // Parcelable management
    private SalePageTag(Parcel in) {
        Color = in.readString();
        TypeDef = in.readString();
        TypeDefDesc = in.readString();
        IsMatchCondition = in.readInt() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Color);
        dest.writeString(TypeDef);
        dest.writeString(TypeDefDesc);
        dest.writeInt(IsMatchCondition ? 1 : 0);
    }

    public static final Parcelable.Creator<SalePageTag> CREATOR = new Parcelable.Creator<SalePageTag>() {
        public SalePageTag createFromParcel(Parcel in) {
            return new SalePageTag(in);
        }

        public SalePageTag[] newArray(int size) {
            return new SalePageTag[size];
        }
    };
}
