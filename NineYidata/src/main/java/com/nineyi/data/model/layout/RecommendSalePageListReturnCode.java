package com.nineyi.data.model.layout;

import com.nineyi.data.model.salepage.SalePageShort;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


/**
 * Created by toby on 2015/3/11.
 */
public class RecommendSalePageListReturnCode implements Parcelable {

    public String ReturnCode;

    public List<SalePageShort> Data;

    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeTypedList(this.Data);
        dest.writeString(this.Message);
    }

    public RecommendSalePageListReturnCode() {
    }

    protected RecommendSalePageListReturnCode(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.createTypedArrayList(SalePageShort.CREATOR);
        this.Message = in.readString();
    }

    public static final Creator<RecommendSalePageListReturnCode> CREATOR
            = new Creator<RecommendSalePageListReturnCode>() {
        @Override
        public RecommendSalePageListReturnCode createFromParcel(Parcel source) {
            return new RecommendSalePageListReturnCode(source);
        }

        @Override
        public RecommendSalePageListReturnCode[] newArray(int size) {
            return new RecommendSalePageListReturnCode[size];
        }
    };
}
