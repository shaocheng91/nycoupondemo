package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBPhotoData implements Parcelable {

    FBMedia media;
    FBTarget target;
    String type;
    String url;

    public FBMedia getMedia() {
        return media;
    }

    protected FBPhotoData(Parcel in) {
        media = (FBMedia) in.readValue(FBMedia.class.getClassLoader());
        target = (FBTarget) in.readValue(FBTarget.class.getClassLoader());
        type = in.readString();
        url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(media);
        dest.writeValue(target);
    }

    public static final Parcelable.Creator<FBPhotoData> CREATOR = new Parcelable.Creator<FBPhotoData>() {
        @Override
        public FBPhotoData createFromParcel(Parcel in) {
            return new FBPhotoData(in);
        }

        @Override
        public FBPhotoData[] newArray(int size) {
            return new FBPhotoData[size];
        }
    };
}