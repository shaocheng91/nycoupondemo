package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeTemplate implements Parcelable {
    public int Id;
    public String Name;
    public ShopThemeAreaList Areas;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.Name);
        dest.writeParcelable(this.Areas, 0);
    }

    public ShopThemeTemplate() {
    }

    protected ShopThemeTemplate(Parcel in) {
        this.Id = in.readInt();
        this.Name = in.readString();
        this.Areas = in.readParcelable(ShopThemeAreaList.class.getClassLoader());
    }

    public static final Parcelable.Creator<ShopThemeTemplate> CREATOR = new Parcelable.Creator<ShopThemeTemplate>() {
        public ShopThemeTemplate createFromParcel(Parcel source) {
            return new ShopThemeTemplate(source);
        }

        public ShopThemeTemplate[] newArray(int size) {
            return new ShopThemeTemplate[size];
        }
    };
}
