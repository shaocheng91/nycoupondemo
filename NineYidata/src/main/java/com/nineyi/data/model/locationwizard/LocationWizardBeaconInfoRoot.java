package com.nineyi.data.model.locationwizard;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Qoo on 15/1/2.
 */
public class LocationWizardBeaconInfoRoot implements Parcelable {

    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_DATA)
    private LocationWizardBeaconInfo mLocationWizardBeaconInfo;


    public LocationWizardBeaconInfoRoot() {

    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setLocationWizardBeaconInfo(LocationWizardBeaconInfo locationWizardBeaconInfo) {
        mLocationWizardBeaconInfo = locationWizardBeaconInfo;
    }

    public LocationWizardBeaconInfo getLocationWizardBeaconInfo() {
        return mLocationWizardBeaconInfo;
    }

    public LocationWizardBeaconInfoRoot(Parcel in) {
        mMessage = in.readString();
        mReturnCode = in.readString();
        mLocationWizardBeaconInfo = in.readParcelable(LocationWizardBeaconInfo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<LocationWizardBeaconInfoRoot> CREATOR
            = new Parcelable.Creator<LocationWizardBeaconInfoRoot>() {
        public LocationWizardBeaconInfoRoot createFromParcel(Parcel in) {
            return new LocationWizardBeaconInfoRoot(in);
        }

        public LocationWizardBeaconInfoRoot[] newArray(int size) {
            return new LocationWizardBeaconInfoRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMessage);
        dest.writeString(mReturnCode);
        dest.writeParcelable(mLocationWizardBeaconInfo, flags);
    }

}
