
package com.nineyi.data.model.shopintroduction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ShopInformation implements Parcelable {

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private ShopInformationData data;
    @SerializedName("Message")
    @Expose
    private String message;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public ShopInformationData getData() {
        return data;
    }

    public void setData(ShopInformationData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.returnCode);
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.message);
    }

    public ShopInformation() {
    }

    protected ShopInformation(Parcel in) {
        this.returnCode = in.readString();
        this.data = in.readParcelable(ShopInformationData.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Parcelable.Creator<ShopInformation> CREATOR = new Parcelable.Creator<ShopInformation>() {
        @Override
        public ShopInformation createFromParcel(Parcel source) {
            return new ShopInformation(source);
        }

        @Override
        public ShopInformation[] newArray(int size) {
            return new ShopInformation[size];
        }
    };
}
