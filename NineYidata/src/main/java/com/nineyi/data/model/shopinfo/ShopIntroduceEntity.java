package com.nineyi.data.model.shopinfo;

import com.nineyi.data.model.ecoupon.ECouponDetail;
import com.nineyi.data.model.promotion.Promotion;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShopIntroduceEntity implements Parcelable {

    public String Info;
    public int Count;
    public String ShippingContent;
    public String PaymentContent;
    public String ChangeReturnContent;
    public String CustomTitle1;
    public String CustomContent1;
    public String CustomTitle2;
    public String CustomContent2;
    public String PhoneCRM;
    public String OnlineCRM;
    public String OnlineCRMCode;
    public String PackageName;
    public String APPId;
    public boolean AndroidAPPDownloadEnable;
    public boolean IsLBS;
    public ArrayList<Promotion> Promotions;
    public String IconUrl;
    public ArrayList<String> ServiceInfoList;
    public int ShopId;
    public ArrayList<ECouponDetail> ECouponList;

    public String ShopName;
    public String LogoUrl;
    public String ShopNote;
    public String SpSignboardUrl;
    public int CategoryId;
    public int Status;
    public String ShopUrl;
    public String FanGroupUrl;
    public ArrayList<Promotion> PromotionList;

    public ShopIntroduceEntity() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Info);
        dest.writeInt(this.Count);
        dest.writeString(this.ShippingContent);
        dest.writeString(this.PaymentContent);
        dest.writeString(this.ChangeReturnContent);
        dest.writeString(this.CustomTitle1);
        dest.writeString(this.CustomContent1);
        dest.writeString(this.CustomTitle2);
        dest.writeString(this.CustomContent2);
        dest.writeString(this.PhoneCRM);
        dest.writeString(this.OnlineCRM);
        dest.writeString(this.OnlineCRMCode);
        dest.writeString(this.PackageName);
        dest.writeString(this.APPId);
        dest.writeInt(AndroidAPPDownloadEnable ? (byte) 1 : (byte) 0);
        dest.writeInt(IsLBS ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.Promotions);
        dest.writeString(this.IconUrl);
        dest.writeStringList(this.ServiceInfoList);
        dest.writeInt(this.ShopId);
        dest.writeTypedList(this.ECouponList);
        dest.writeString(this.ShopName);
        dest.writeString(this.LogoUrl);
        dest.writeString(this.ShopNote);
        dest.writeString(this.SpSignboardUrl);
        dest.writeInt(this.CategoryId);
        dest.writeInt(this.Status);
        dest.writeString(this.ShopUrl);
        dest.writeString(this.FanGroupUrl);
        dest.writeTypedList(this.PromotionList);
    }

    private ShopIntroduceEntity(Parcel in) {
        this.Info = in.readString();
        this.Count = in.readInt();
        this.ShippingContent = in.readString();
        this.PaymentContent = in.readString();
        this.ChangeReturnContent = in.readString();
        this.CustomTitle1 = in.readString();
        this.CustomContent1 = in.readString();
        this.CustomTitle2 = in.readString();
        this.CustomContent2 = in.readString();
        this.PhoneCRM = in.readString();
        this.OnlineCRM = in.readString();
        this.OnlineCRMCode = in.readString();
        this.PackageName = in.readString();
        this.APPId = in.readString();
        this.AndroidAPPDownloadEnable = in.readInt() != 0;
        this.IsLBS = in.readInt() != 0;
        this.Promotions = in.createTypedArrayList(Promotion.CREATOR);
        this.IconUrl = in.readString();
        this.ServiceInfoList = in.createStringArrayList();
        this.ShopId = in.readInt();
        this.ECouponList = in.createTypedArrayList(ECouponDetail.CREATOR);
        this.ShopName = in.readString();
        this.LogoUrl = in.readString();
        this.ShopNote = in.readString();
        this.SpSignboardUrl = in.readString();
        this.CategoryId = in.readInt();
        this.Status = in.readInt();
        this.ShopUrl = in.readString();
        this.FanGroupUrl = in.readString();
        this.PromotionList = in.createTypedArrayList(Promotion.CREATOR);
    }

    public static final Creator<ShopIntroduceEntity> CREATOR = new Creator<ShopIntroduceEntity>() {
        public ShopIntroduceEntity createFromParcel(Parcel source) {
            return new ShopIntroduceEntity(source);
        }

        public ShopIntroduceEntity[] newArray(int size) {
            return new ShopIntroduceEntity[size];
        }
    };
}
