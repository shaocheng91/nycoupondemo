package com.nineyi.data.model.promotion;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.nineyi.data.model.salepage.SalePageShort;

import java.util.ArrayList;

/**
 * Created by kelsey on 2017/1/20.
 */

public class ShopCategorySalePageListData implements Parcelable {
    @SerializedName("data")
    private ArrayList<SalePageShort> data;
    @SerializedName("name")
    private String name;
    @SerializedName("count")
    private int count;
    @SerializedName("parentCategoryText")
    private String parentCategoryText;
    @SerializedName("parentCategoryId")
    private int parentCategoryId;
    @SerializedName("displayCategoryText")
    private String displayCategoryText;
    @SerializedName("listmode")
    private String listmode;
    @SerializedName("statusdef")
    private String statusdef;

    public ArrayList<SalePageShort> getData() {
        return data;
    }

    public void setData(ArrayList<SalePageShort> data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getParentCategoryText() {
        return parentCategoryText;
    }

    public void setParentCategoryText(String parentCategoryText) {
        this.parentCategoryText = parentCategoryText;
    }

    public int getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(int parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getDisplayCategoryText() {
        return displayCategoryText;
    }

    public void setDisplayCategoryText(String displayCategoryText) {
        this.displayCategoryText = displayCategoryText;
    }

    public String getListmode() {
        return listmode;
    }

    public void setListmode(String listmode) {
        this.listmode = listmode;
    }

    public String getStatusdef() {
        return statusdef;
    }

    public void setStatusdef(String statusdef) {
        this.statusdef = statusdef;
    }

    public ShopCategorySalePageListData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.data);
        dest.writeString(this.name);
        dest.writeInt(this.count);
        dest.writeString(this.parentCategoryText);
        dest.writeInt(this.parentCategoryId);
        dest.writeString(this.displayCategoryText);
        dest.writeString(this.listmode);
        dest.writeString(this.statusdef);
    }

    protected ShopCategorySalePageListData(Parcel in) {
        this.data = in.createTypedArrayList(SalePageShort.CREATOR);
        this.name = in.readString();
        this.count = in.readInt();
        this.parentCategoryText = in.readString();
        this.parentCategoryId = in.readInt();
        this.displayCategoryText = in.readString();
        this.listmode = in.readString();
        this.statusdef = in.readString();
    }

    public static final Creator<ShopCategorySalePageListData> CREATOR = new Creator<ShopCategorySalePageListData>() {
        @Override
        public ShopCategorySalePageListData createFromParcel(Parcel source) {
            return new ShopCategorySalePageListData(source);
        }

        @Override
        public ShopCategorySalePageListData[] newArray(int size) {
            return new ShopCategorySalePageListData[size];
        }
    };
}
