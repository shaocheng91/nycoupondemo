package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBLikeData implements Parcelable {

    String id;
    String name;

    protected FBLikeData(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    public static final Parcelable.Creator<FBLikeData> CREATOR = new Parcelable.Creator<FBLikeData>() {
        @Override
        public FBLikeData createFromParcel(Parcel in) {
            return new FBLikeData(in);
        }

        @Override
        public FBLikeData[] newArray(int size) {
            return new FBLikeData[size];
        }
    };
}