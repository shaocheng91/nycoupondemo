package com.nineyi.data.model.newo2o;

import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;


public class CityAreaListRoot implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private CityAreaListData mCityAreaListData;


    public CityAreaListRoot(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setCityAreaListData(CityAreaListData cityAreaListData) {
        mCityAreaListData = cityAreaListData;
    }

    public CityAreaListData getCityAreaListData() {
        return mCityAreaListData;
    }

    public CityAreaListRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mCityAreaListData = in.readParcelable(CityAreaListData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CityAreaListRoot> CREATOR = new Creator<CityAreaListRoot>() {
        public CityAreaListRoot createFromParcel(Parcel in) {
            return new CityAreaListRoot(in);
        }

        public CityAreaListRoot[] newArray(int size) {
            return new CityAreaListRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        		dest.writeParcelable(mCityAreaListData, flags);
    }


}