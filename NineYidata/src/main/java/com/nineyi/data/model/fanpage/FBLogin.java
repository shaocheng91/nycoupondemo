package com.nineyi.data.model.fanpage;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by tedliang on 2016/6/29.
 */
public class FBLogin implements Parcelable {

    private static final String ACCESS_TOKEN = "access_token";
    private static final String TOKEN_TYPE = "token_type";

    @SerializedName(ACCESS_TOKEN)
    private String mAccessToken;
    @SerializedName(TOKEN_TYPE)
    private String mTokenType;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAccessToken);
        dest.writeString(this.mTokenType);
    }

    public FBLogin() {
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    protected FBLogin(Parcel in) {
        this.mAccessToken = in.readString();
        this.mTokenType = in.readString();
    }

    public static final Parcelable.Creator<FBLogin> CREATOR = new Parcelable.Creator<FBLogin>() {
        @Override
        public FBLogin createFromParcel(Parcel source) {
            return new FBLogin(source);
        }

        @Override
        public FBLogin[] newArray(int size) {
            return new FBLogin[size];
        }
    };
}
