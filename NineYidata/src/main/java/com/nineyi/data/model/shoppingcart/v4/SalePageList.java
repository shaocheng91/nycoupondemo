
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.nio.charset.IllegalCharsetNameException;
import java.util.ArrayList;
import java.util.List;


public class SalePageList implements Parcelable {

    public static final String SALE_PAGE_ID = "SalePageId";
    public static final String QTY = "Qty";
    public static final String SALE_PRODUCT_SKU_ID = "SaleProductSKUId";
    public static final String SALE_PAGE_GROUP_SEQ = "SalePageGroupSeq";

    public Double getAveragePayment() {
        return mAveragePayment;
    }

    @SerializedName("AveragePayment")
    @Expose
    private Double mAveragePayment;
    @SerializedName(SALE_PAGE_GROUP_SEQ)
    @Expose
    private Integer SalePageGroupSeq;
    @SerializedName(SALE_PAGE_ID)
    @Expose
    private Integer SalePageId;
    @SerializedName("SaleProductId")
    @Expose
    private Integer SaleProductId;
    @SerializedName(SALE_PRODUCT_SKU_ID)
    @Expose
    private Integer SaleProductSKUId;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName(QTY)
    @Expose
    private Integer Qty;
    @SerializedName("QtyLimit")
    @Expose
    private Integer QtyLimit;
    @SerializedName("Price")
    @Expose
    private Integer Price;
    @SerializedName("ECouponDiscount")
    @Expose
    private Integer ECouponDiscount;
    @SerializedName("PromotionDiscount")
    @Expose
    private Integer PromotionDiscount;
    @SerializedName("ReachQtyPromotionDiscount")
    @Expose
    private Integer ReachQtyPromotionDiscount;
    @SerializedName("TotalDiscount")
    @Expose
    private Double TotalDiscount;
    @SerializedName("TotalPayment")
    @Expose
    private Double TotalPayment;
    @SerializedName("TotalPrice")
    @Expose
    private Integer TotalPrice;
    @SerializedName("PayTypeList")
    @Expose
    private List<PayTypeList> mPayTypeList = new ArrayList<PayTypeList>();
    @SerializedName("DeliveryTypeList")
    @Expose
    private List<DeliveryTypeList> mDeliveryTypeList = new ArrayList<DeliveryTypeList>();
    @SerializedName("SKUPropertyDisplay")
    @Expose
    private String SKUPropertyDisplay;
    @SerializedName("SKUPropertyDisplayList")
    @Expose
    private Object SKUPropertyDisplayList;
    @SerializedName("PicUrl")
    @Expose
    private String PicUrl;
    @SerializedName("IsLimit")
    @Expose
    private Boolean IsLimit;
    @SerializedName("TemperatureTypeDef")
    @Expose
    private String TemperatureTypeDef;
    @SerializedName("Length")
    @Expose
    private Integer Length;
    @SerializedName("Width")
    @Expose
    private Integer Width;
    @SerializedName("Height")
    @Expose
    private Integer Height;
    @SerializedName("Weight")
    @Expose
    private Integer Weight;
    @SerializedName("SalePageGiftList")
    @Expose
    private List<SalePageGiftList> salePageGiftList = new ArrayList<SalePageGiftList>();
    @SerializedName("PromotionList")
    @Expose
    private List<SalePagePromotionList> promotionList = new ArrayList<SalePagePromotionList>();
    @SerializedName("SaleProductShippingTypeDef")
    @Expose
    private String saleProductShippingTypeDef;

    public Integer getSourceShopCategoryId() {
        return mSourceShopCategoryId;
    }

    @SerializedName("SourceShopCategoryId")
    @Expose
    private Integer mSourceShopCategoryId;


    @SerializedName("IsExcludeECouponDiscount")
    @Expose
    private boolean isExcludeECouponDiscount;

    /**
     * 
     * @return
     *     The SalePageGroupSeq
     */
    public Integer getSalePageGroupSeq() {
        return SalePageGroupSeq;
    }

    /**
     * 
     * @param SalePageGroupSeq
     *     The SalePageGroupSeq
     */
    public void setSalePageGroupSeq(Integer SalePageGroupSeq) {
        this.SalePageGroupSeq = SalePageGroupSeq;
    }

    /**
     * 
     * @return
     *     The SalePageId
     */
    public Integer getSalePageId() {
        return SalePageId;
    }

    /**
     * 
     * @param SalePageId
     *     The SalePageId
     */
    public void setSalePageId(Integer SalePageId) {
        this.SalePageId = SalePageId;
    }

    /**
     * 
     * @return
     *     The SaleProductId
     */
    public Integer getSaleProductId() {
        return SaleProductId;
    }

    /**
     * 
     * @param SaleProductId
     *     The SaleProductId
     */
    public void setSaleProductId(Integer SaleProductId) {
        this.SaleProductId = SaleProductId;
    }

    /**
     * 
     * @return
     *     The SaleProductSKUId
     */
    public Integer getSaleProductSKUId() {
        return SaleProductSKUId;
    }

    /**
     * 
     * @param SaleProductSKUId
     *     The SaleProductSKUId
     */
    public void setSaleProductSKUId(Integer SaleProductSKUId) {
        this.SaleProductSKUId = SaleProductSKUId;
    }

    /**
     * 
     * @return
     *     The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * 
     * @param Title
     *     The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * 
     * @return
     *     The Qty
     */
    public Integer getQty() {
        return Qty;
    }

    /**
     * 
     * @param Qty
     *     The Qty
     */
    public void setQty(Integer Qty) {
        this.Qty = Qty;
    }

    /**
     * 
     * @return
     *     The QtyLimit
     */
    public Integer getQtyLimit() {
        return QtyLimit;
    }

    /**
     * 
     * @param QtyLimit
     *     The QtyLimit
     */
    public void setQtyLimit(Integer QtyLimit) {
        this.QtyLimit = QtyLimit;
    }

    /**
     * 
     * @return
     *     The Price
     */
    public Integer getPrice() {
        return Price;
    }

    /**
     * 
     * @param Price
     *     The Price
     */
    public void setPrice(Integer Price) {
        this.Price = Price;
    }

    /**
     * 
     * @return
     *     The ECouponDiscount
     */
    public Integer getECouponDiscount() {
        return ECouponDiscount;
    }

    /**
     * 
     * @param ECouponDiscount
     *     The ECouponDiscount
     */
    public void setECouponDiscount(Integer ECouponDiscount) {
        this.ECouponDiscount = ECouponDiscount;
    }

    /**
     * 
     * @return
     *     The PromotionDiscount
     */
    public Integer getPromotionDiscount() {
        return PromotionDiscount;
    }

    /**
     * 
     * @param PromotionDiscount
     *     The PromotionDiscount
     */
    public void setPromotionDiscount(Integer PromotionDiscount) {
        this.PromotionDiscount = PromotionDiscount;
    }

    /**
     * 
     * @return
     *     The ReachQtyPromotionDiscount
     */
    public Integer getReachQtyPromotionDiscount() {
        return ReachQtyPromotionDiscount;
    }

    /**
     * 
     * @param ReachQtyPromotionDiscount
     *     The ReachQtyPromotionDiscount
     */
    public void setReachQtyPromotionDiscount(Integer ReachQtyPromotionDiscount) {
        this.ReachQtyPromotionDiscount = ReachQtyPromotionDiscount;
    }

    /**
     * 
     * @return
     *     The TotalDiscount
     */
    public Double getTotalDiscount() {
        return TotalDiscount;
    }

    /**
     * 
     * @param TotalDiscount
     *     The TotalDiscount
     */
    public void setTotalDiscount(Double TotalDiscount) {
        this.TotalDiscount = TotalDiscount;
    }

    /**
     * 
     * @return
     *     The TotalPayment
     */
    public Double getTotalPayment() {
        return TotalPayment;
    }

    /**
     * 
     * @param TotalPayment
     *     The TotalPayment
     */
    public void setTotalPayment(Double TotalPayment) {
        this.TotalPayment = TotalPayment;
    }

    /**
     * 
     * @return
     *     The TotalPrice
     */
    public Integer getTotalPrice() {
        return TotalPrice;
    }

    /**
     * 
     * @param TotalPrice
     *     The TotalPrice
     */
    public void setTotalPrice(Integer TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    /**
     * 
     * @return
     *     The mPayTypeList
     */
    public List<PayTypeList> getPayTypeList() {
        return mPayTypeList;
    }

    /**
     * 
     * @param PayTypeList
     *     The mPayTypeList
     */
    public void setPayTypeList(List<PayTypeList> PayTypeList) {
        this.mPayTypeList = PayTypeList;
    }

    /**
     * 
     * @return
     *     The mDeliveryTypeList
     */
    public List<DeliveryTypeList> getDeliveryTypeList() {
        return mDeliveryTypeList;
    }

    /**
     * 
     * @param DeliveryTypeList
     *     The mDeliveryTypeList
     */
    public void setDeliveryTypeList(List<DeliveryTypeList> DeliveryTypeList) {
        this.mDeliveryTypeList = DeliveryTypeList;
    }

    /**
     * 
     * @return
     *     The SKUPropertyDisplay
     */
    public String getSKUPropertyDisplay() {
        return SKUPropertyDisplay;
    }

    /**
     * 
     * @param SKUPropertyDisplay
     *     The SKUPropertyDisplay
     */
    public void setSKUPropertyDisplay(String SKUPropertyDisplay) {
        this.SKUPropertyDisplay = SKUPropertyDisplay;
    }

    /**
     * 
     * @return
     *     The SKUPropertyDisplayList
     */
    public Object getSKUPropertyDisplayList() {
        return SKUPropertyDisplayList;
    }

    /**
     * 
     * @param SKUPropertyDisplayList
     *     The SKUPropertyDisplayList
     */
    public void setSKUPropertyDisplayList(Object SKUPropertyDisplayList) {
        this.SKUPropertyDisplayList = SKUPropertyDisplayList;
    }

    /**
     * 
     * @return
     *     The PicUrl
     */
    public String getPicUrl() {
        return PicUrl;
    }

    /**
     * 
     * @param PicUrl
     *     The PicUrl
     */
    public void setPicUrl(String PicUrl) {
        this.PicUrl = PicUrl;
    }

    /**
     * 
     * @return
     *     The IsLimit
     */
    public Boolean getIsLimit() {
        return IsLimit;
    }

    /**
     * 
     * @param IsLimit
     *     The IsLimit
     */
    public void setIsLimit(Boolean IsLimit) {
        this.IsLimit = IsLimit;
    }

    /**
     * 
     * @return
     *     The TemperatureTypeDef
     */
    public String getTemperatureTypeDef() {
        return TemperatureTypeDef;
    }

    /**
     * 
     * @param TemperatureTypeDef
     *     The TemperatureTypeDef
     */
    public void setTemperatureTypeDef(String TemperatureTypeDef) {
        this.TemperatureTypeDef = TemperatureTypeDef;
    }

    /**
     * 
     * @return
     *     The Length
     */
    public Integer getLength() {
        return Length;
    }

    /**
     * 
     * @param Length
     *     The Length
     */
    public void setLength(Integer Length) {
        this.Length = Length;
    }

    /**
     * 
     * @return
     *     The Width
     */
    public Integer getWidth() {
        return Width;
    }

    /**
     * 
     * @param Width
     *     The Width
     */
    public void setWidth(Integer Width) {
        this.Width = Width;
    }

    /**
     * 
     * @return
     *     The Height
     */
    public Integer getHeight() {
        return Height;
    }

    /**
     * 
     * @param Height
     *     The Height
     */
    public void setHeight(Integer Height) {
        this.Height = Height;
    }

    /**
     * 
     * @return
     *     The Weight
     */
    public Integer getWeight() {
        return Weight;
    }

    /**
     * 
     * @param Weight
     *     The Weight
     */
    public void setWeight(Integer Weight) {
        this.Weight = Weight;
    }

    /**
     *
     * @return
     *     The salePageGiftList
     */
    public List<SalePageGiftList> getSalePageGiftList() {
        return salePageGiftList;
    }

    /**
     *
     * @param salePageGiftList
     *     The SalePageGiftList
     */
    public void setSalePageGiftList(List<SalePageGiftList> salePageGiftList) {
        this.salePageGiftList = salePageGiftList;
    }

    /**
     *
     * @return
     *     The promotionList
     */
    public List<SalePagePromotionList> getPromotionList() {
        return promotionList;
    }

    /**
     *
     * @param promotionList
     *     The PromotionList
     */
    public void setPromotionList(List<SalePagePromotionList> promotionList) {
        this.promotionList = promotionList;
    }

    /**
     *
     * @return
     *     The saleProductShippingTypeDef
     */
    public String getSaleProductShippingTypeDef() {
        return saleProductShippingTypeDef;
    }

    /**
     *
     * @param saleProductShippingTypeDef
     *     The SaleProductShippingTypeDef
     */
    public void setSaleProductShippingTypeDef(String saleProductShippingTypeDef) {
        this.saleProductShippingTypeDef = saleProductShippingTypeDef;
    }

    /**
     *
     * @return
     *     The isExcludeECouponDiscount
     */
    public boolean getIsExcludeECouponDiscount() {
        return isExcludeECouponDiscount;
    }

    /**
     *
     * @param isExcludeECouponDiscount
     *     The IsExcludeECouponDiscount
     */
    public void setIsExcludeECouponDiscount(boolean isExcludeECouponDiscount) {
        this.isExcludeECouponDiscount = isExcludeECouponDiscount;
    }


    public SalePageList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mAveragePayment);
        dest.writeValue(this.SalePageGroupSeq);
        dest.writeValue(this.SalePageId);
        dest.writeValue(this.SaleProductId);
        dest.writeValue(this.SaleProductSKUId);
        dest.writeString(this.Title);
        dest.writeValue(this.Qty);
        dest.writeValue(this.QtyLimit);
        dest.writeValue(this.Price);
        dest.writeValue(this.ECouponDiscount);
        dest.writeValue(this.PromotionDiscount);
        dest.writeValue(this.ReachQtyPromotionDiscount);
        dest.writeValue(this.TotalDiscount);
        dest.writeValue(this.TotalPayment);
        dest.writeValue(this.TotalPrice);
        dest.writeTypedList(this.mPayTypeList);
        dest.writeTypedList(this.mDeliveryTypeList);
        dest.writeString(this.SKUPropertyDisplay);
//        dest.writeParcelable(this.SKUPropertyDisplayList, flags);
        dest.writeString(this.PicUrl);
        dest.writeValue(this.IsLimit);
        dest.writeString(this.TemperatureTypeDef);
        dest.writeValue(this.Length);
        dest.writeValue(this.Width);
        dest.writeValue(this.Height);
        dest.writeValue(this.Weight);
        dest.writeTypedList(this.salePageGiftList);
        dest.writeTypedList(this.promotionList);
        dest.writeString(this.saleProductShippingTypeDef);
        dest.writeValue(this.mSourceShopCategoryId);
        dest.writeByte(this.isExcludeECouponDiscount ? (byte) 1 : (byte) 0);
    }

    protected SalePageList(Parcel in) {
        this.mAveragePayment = (Double) in.readValue(Double.class.getClassLoader());
        this.SalePageGroupSeq = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SalePageId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SaleProductId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SaleProductSKUId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Title = in.readString();
        this.Qty = (Integer) in.readValue(Integer.class.getClassLoader());
        this.QtyLimit = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Price = (Integer) in.readValue(Integer.class.getClassLoader());
        this.ECouponDiscount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.PromotionDiscount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.ReachQtyPromotionDiscount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.TotalDiscount = (Double) in.readValue(Double.class.getClassLoader());
        this.TotalPayment = (Double) in.readValue(Double.class.getClassLoader());
        this.TotalPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mPayTypeList = in.createTypedArrayList(PayTypeList.CREATOR);
        this.mDeliveryTypeList = in.createTypedArrayList(DeliveryTypeList.CREATOR);
        this.SKUPropertyDisplay = in.readString();
//        this.SKUPropertyDisplayList = in.readParcelable(Object.class.getClassLoader());
        this.PicUrl = in.readString();
        this.IsLimit = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.TemperatureTypeDef = in.readString();
        this.Length = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Width = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Height = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Weight = (Integer) in.readValue(Integer.class.getClassLoader());
        this.salePageGiftList = in.createTypedArrayList(SalePageGiftList.CREATOR);
        this.promotionList = in.createTypedArrayList(SalePagePromotionList.CREATOR);
        this.saleProductShippingTypeDef = in.readString();
        this.mSourceShopCategoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isExcludeECouponDiscount = in.readByte() != 0;
    }

    public static final Creator<SalePageList> CREATOR = new Creator<SalePageList>() {
        @Override
        public SalePageList createFromParcel(Parcel source) {
            return new SalePageList(source);
        }

        @Override
        public SalePageList[] newArray(int size) {
            return new SalePageList[size];
        }
    };
}
