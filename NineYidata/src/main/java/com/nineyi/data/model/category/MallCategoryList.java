package com.nineyi.data.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class MallCategoryList implements Parcelable {

    public ArrayList<MallCategory> data;

    public MallCategoryList() {
    }

    // Parcelable management
    private MallCategoryList(Parcel in) {
        data = in.createTypedArrayList(MallCategory.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }

    public static final Parcelable.Creator<MallCategoryList> CREATOR = new Parcelable.Creator<MallCategoryList>() {
        public MallCategoryList createFromParcel(Parcel in) {
            return new MallCategoryList(in);
        }

        public MallCategoryList[] newArray(int size) {
            return new MallCategoryList[size];
        }
    };
}
