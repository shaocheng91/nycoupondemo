package com.nineyi.data.model.theme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/3/18.
 */
public class ShopThemeTemplateList implements Parcelable {
    public int Id;
    public String Name;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.Name);
    }

    public ShopThemeTemplateList() {
    }

    protected ShopThemeTemplateList(Parcel in) {
        this.Id = in.readInt();
        this.Name = in.readString();
    }

    public static final Creator<ShopThemeTemplateList> CREATOR = new Creator<ShopThemeTemplateList>() {
        public ShopThemeTemplateList createFromParcel(Parcel source) {
            return new ShopThemeTemplateList(source);
        }

        public ShopThemeTemplateList[] newArray(int size) {
            return new ShopThemeTemplateList[size];
        }
    };
}
