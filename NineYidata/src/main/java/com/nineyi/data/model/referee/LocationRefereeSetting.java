package com.nineyi.data.model.referee;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class LocationRefereeSetting implements Parcelable {

    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_RETURN_CODE = "ReturnCode";


    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    public RefereeSettingInfo Data;


    public LocationRefereeSetting() {

    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public boolean getIsEmployeeColumnVisible() {
        return Data.IsEmployeeColumnVisible;
    }

    public boolean getIsRequireLogin() {
        return Data.IsRequireLogin;
    }

    public LocationRefereeSetting(Parcel in) {
        mMessage = in.readString();
        mReturnCode = in.readString();
        Data = in.readParcelable(RefereeSettingInfo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<LocationRefereeSetting> CREATOR = new Parcelable.Creator<LocationRefereeSetting>() {
        public LocationRefereeSetting createFromParcel(Parcel in) {
            return new LocationRefereeSetting(in);
        }

        public LocationRefereeSetting[] newArray(int size) {
            return new LocationRefereeSetting[size];
        }
    };
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMessage);
        dest.writeString(mReturnCode);
        dest.writeParcelable(Data, flags);
    }


}