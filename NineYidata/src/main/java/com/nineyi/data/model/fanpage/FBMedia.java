package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBMedia implements Parcelable {

    FBImage image;

    public FBImage getImage() {
        return image;
    }

    protected FBMedia(Parcel in) {

        image = (FBImage) in.readValue(FBImage.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(image);
    }

    public static final Parcelable.Creator<FBMedia> CREATOR = new Parcelable.Creator<FBMedia>() {
        @Override
        public FBMedia createFromParcel(Parcel in) {
            return new FBMedia(in);
        }

        @Override
        public FBMedia[] newArray(int size) {
            return new FBMedia[size];
        }
    };
}