package com.nineyi.data.model.promotion;

import com.nineyi.data.model.salepage.SalePageData;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2015/10/7.
 */
public class FreeGiftSalePage implements Parcelable{

    public String ReturnCode;
    public SalePageData Data;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, 0);
    }

    public FreeGiftSalePage() {
    }

    protected FreeGiftSalePage(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(SalePageData.class.getClassLoader());
    }

    public static final Creator<FreeGiftSalePage> CREATOR = new Creator<FreeGiftSalePage>() {
        public FreeGiftSalePage createFromParcel(Parcel source) {
            return new FreeGiftSalePage(source);
        }

        public FreeGiftSalePage[] newArray(int size) {
            return new FreeGiftSalePage[size];
        }
    };
}
