package com.nineyi.data.model.gson;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.Override;
import java.lang.String;


/**
 * Created by ted on 15/1/28.
 */
public class NineyiDate implements Parcelable {

    public static final String DEFAULT_TIMEZONE = "+0800";

    public String getRaw() {
        return raw;
    }

    public String getTime() {
        return time;
    }

    public long getTimeLong() {
        if(time != null && !("").equals(time)) {
            return Long.valueOf(time);
        } else {
            return 0;
        }
    }

    public String getTimezone() {
        if(timezone == null || "".equals(timezone)) {
            return DEFAULT_TIMEZONE;
        } else {
            return timezone;
        }
    }

    private String raw;
    private String time;
    private String timezone;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.raw);
        dest.writeString(this.time);
        dest.writeString(this.timezone);
    }

    public NineyiDate(String _raw, String _time, String _timezone) {
        raw = _raw;
        time = _time;
        timezone = _timezone;
    }

    private NineyiDate(Parcel in) {
        this.raw = in.readString();
        this.time = in.readString();
        this.timezone = in.readString();
    }

    public static final Parcelable.Creator<NineyiDate> CREATOR = new Parcelable.Creator<NineyiDate>() {
        public NineyiDate createFromParcel(Parcel source) {
            return new NineyiDate(source);
        }
        public NineyiDate[] newArray(int size) {
            return new NineyiDate[size];
        }
    };
}
