
package com.nineyi.data.model.infomodule.videolist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class VideoModuleDataList implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Subtitle")
    @Expose
    private String subtitle;
    @SerializedName("ClipLink")
    @Expose
    private String clipLink;

    public String getPublishedDate() {
        return publishedDate;
    }

    @SerializedName("PublishedDate")
    @Expose
    private String publishedDate;
    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The Type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * 
     * @param subtitle
     *     The Subtitle
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * 
     * @return
     *     The clipLink
     */
    public String getClipLink() {
        return clipLink;
    }

    /**
     * 
     * @param clipLink
     *     The ClipLink
     */
    public void setClipLink(String clipLink) {
        this.clipLink = clipLink;
    }

    public VideoModuleDataList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.type);
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeString(this.clipLink);
        dest.writeString(this.publishedDate);
    }

    protected VideoModuleDataList(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = in.readString();
        this.title = in.readString();
        this.subtitle = in.readString();
        this.clipLink = in.readString();
        this.publishedDate = in.readString();
    }

    public static final Creator<VideoModuleDataList> CREATOR = new Creator<VideoModuleDataList>() {
        @Override
        public VideoModuleDataList createFromParcel(Parcel source) {
            return new VideoModuleDataList(source);
        }

        @Override
        public VideoModuleDataList[] newArray(int size) {
            return new VideoModuleDataList[size];
        }
    };
}
