package com.nineyi.data.model.promotion;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class PromotionAllList implements Parcelable {

    public ArrayList<PromotionShort> PromotionList;

    public PromotionAllList() {
    }

    // Parcelable management
    private PromotionAllList(Parcel in) {
        PromotionList = in.createTypedArrayList(PromotionShort.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(PromotionList);
    }

    public static final Parcelable.Creator<PromotionAllList> CREATOR = new Parcelable.Creator<PromotionAllList>() {
        public PromotionAllList createFromParcel(Parcel in) {
            return new PromotionAllList(in);
        }

        public PromotionAllList[] newArray(int size) {
            return new PromotionAllList[size];
        }
    };
}
