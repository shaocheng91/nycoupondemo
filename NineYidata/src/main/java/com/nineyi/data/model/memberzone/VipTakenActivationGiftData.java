package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class VipTakenActivationGiftData implements Parcelable {

    private static final String FIELD_HAS_TAKEN = "hasTaken";
    private static final String FIELD_MSG = "msg";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_GIFTID = "giftId";
    private static final String FIELD_ISUSING = "isUsing";
    @SerializedName(FIELD_HAS_TAKEN)
    private boolean mHasTaken;
    @SerializedName(FIELD_MSG)
    private String mMsg;
    @SerializedName(FIELD_STATUS)
    private String mStatus;
    @SerializedName(FIELD_ISUSING)
    private boolean mIsUsing;

    public boolean ismIsUsing() {
        return mIsUsing;
    }

    public String getGiftId() {
        return mGiftId;
    }


    @SerializedName(FIELD_GIFTID)
    private String mGiftId;


    public VipTakenActivationGiftData() {

    }

    public void setHasTaken(boolean hasTaken) {
        mHasTaken = hasTaken;
    }

    public boolean isHasTaken() {
        return mHasTaken;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStatus() {
        return mStatus;
    }

    public VipTakenActivationGiftData(Parcel in) {
        mHasTaken = in.readInt() == 1;
        mIsUsing = in.readInt() == 1;
        mMsg = in.readString();
        mStatus = in.readString();
        mGiftId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipTakenActivationGiftData> CREATOR = new Creator<VipTakenActivationGiftData>() {
        public VipTakenActivationGiftData createFromParcel(Parcel in) {
            return new VipTakenActivationGiftData(in);
        }

        public VipTakenActivationGiftData[] newArray(int size) {
            return new VipTakenActivationGiftData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mHasTaken ? 1 : 0);
        dest.writeInt(mIsUsing ? 1 : 0);
        dest.writeString(mMsg);
        dest.writeString(mStatus);
        dest.writeString(mGiftId);
    }


}