package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2017/4/10.
 */
public class ShippingStatus implements Parcelable {
    public ShippingStatusData Data;
    public String ReturnCode;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.ReturnCode);
        dest.writeString(this.Message);
    }

    public ShippingStatus() {
    }

    protected ShippingStatus(Parcel in) {
        this.Data = in.readParcelable(ShippingStatusData.class.getClassLoader());
        this.ReturnCode = in.readString();
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<ShippingStatus> CREATOR = new Parcelable.Creator<ShippingStatus>() {
        @Override
        public ShippingStatus createFromParcel(Parcel source) {
            return new ShippingStatus(source);
        }

        @Override
        public ShippingStatus[] newArray(int size) {
            return new ShippingStatus[size];
        }
    };
}
