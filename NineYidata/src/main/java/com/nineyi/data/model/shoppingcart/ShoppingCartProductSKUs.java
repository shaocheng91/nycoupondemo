package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2015/9/22.
 */
public class ShoppingCartProductSKUs implements Parcelable {

    public int Id;
    public int PromotionId;
    public int PromotionConditionId;
    public int SalePageId;
    public int SaleProductSKUId;
    public String Title;
    public String PicUrl;
    public int Qty;
    public int Price;
    public boolean IsSoldOut;

    public ShoppingCartProductSKUs() {

    }

    public static final Parcelable.Creator<ShoppingCartProductSKUs> CREATOR
            = new Parcelable.Creator<ShoppingCartProductSKUs>() {
        public ShoppingCartProductSKUs createFromParcel(Parcel in) {
            return new ShoppingCartProductSKUs(in);
        }

        public ShoppingCartProductSKUs[] newArray(int size) {
            return new ShoppingCartProductSKUs[size];
        }
    };

    // Parcelable management
    private ShoppingCartProductSKUs(Parcel in) {
        Id = in.readInt();
        PromotionId = in.readInt();
        PromotionConditionId = in.readInt();
        SalePageId = in.readInt();
        SaleProductSKUId = in.readInt();
        Title = in.readString();
        PicUrl = in.readString();
        Qty = in.readInt();
        Price = in.readInt();
        IsSoldOut = in.readInt() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(PromotionId);
        dest.writeInt(PromotionConditionId);
        dest.writeInt(SalePageId);
        dest.writeInt(SaleProductSKUId);
        dest.writeString(Title);
        dest.writeString(PicUrl);
        dest.writeInt(Qty);
        dest.writeInt(Price);
        dest.writeInt(IsSoldOut ? 1 : 0);
    }

}
