package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;

import com.nineyi.data.model.gson.NineyiDate;


public class ECouponCouponDetail implements Parcelable {

    public String StatusTypeDef;
    public boolean IsUsing;
    public int Id;
    public String TypeDef;
    public int ShopId;
    public String ECouponTypeDef;
    public String StatusUpdateDateTime;
    public boolean IsMix;
    public String Code;
    public boolean IsTake;
    public double DiscountPrice;
    public int ECouponId;
    public int MemberId;
    public NineyiDate UsingEndDateTime;
    public NineyiDate UsingStartDateTime;
    public String UseEndTimeWarningText;
    public double ECouponMaxDiscountLimit;
    public double ECouponUsingMinPrice;
    public boolean IsAchieveUsingMinPrice;

    public ECouponCouponDetail() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.StatusTypeDef);
        dest.writeByte(this.IsUsing ? (byte) 1 : (byte) 0);
        dest.writeInt(this.Id);
        dest.writeString(this.TypeDef);
        dest.writeInt(this.ShopId);
        dest.writeString(this.ECouponTypeDef);
        dest.writeString(this.StatusUpdateDateTime);
        dest.writeByte(this.IsMix ? (byte) 1 : (byte) 0);
        dest.writeString(this.Code);
        dest.writeByte(this.IsTake ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.DiscountPrice);
        dest.writeInt(this.ECouponId);
        dest.writeInt(this.MemberId);
        dest.writeParcelable(this.UsingEndDateTime, flags);
        dest.writeParcelable(this.UsingStartDateTime, flags);
        dest.writeString(this.UseEndTimeWarningText);
        dest.writeDouble(this.ECouponMaxDiscountLimit);
        dest.writeDouble(this.ECouponUsingMinPrice);
        dest.writeByte(this.IsAchieveUsingMinPrice ? (byte) 1 : (byte) 0);
    }

    protected ECouponCouponDetail(Parcel in) {
        this.StatusTypeDef = in.readString();
        this.IsUsing = in.readByte() != 0;
        this.Id = in.readInt();
        this.TypeDef = in.readString();
        this.ShopId = in.readInt();
        this.ECouponTypeDef = in.readString();
        this.StatusUpdateDateTime = in.readString();
        this.IsMix = in.readByte() != 0;
        this.Code = in.readString();
        this.IsTake = in.readByte() != 0;
        this.DiscountPrice = in.readDouble();
        this.ECouponId = in.readInt();
        this.MemberId = in.readInt();
        this.UsingEndDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.UsingStartDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.UseEndTimeWarningText = in.readString();
        this.ECouponMaxDiscountLimit = in.readDouble();
        this.ECouponUsingMinPrice = in.readDouble();
        this.IsAchieveUsingMinPrice = in.readByte() != 0;
    }

    public static final Creator<ECouponCouponDetail> CREATOR = new Creator<ECouponCouponDetail>() {
        @Override
        public ECouponCouponDetail createFromParcel(Parcel source) {
            return new ECouponCouponDetail(source);
        }

        @Override
        public ECouponCouponDetail[] newArray(int size) {
            return new ECouponCouponDetail[size];
        }
    };
}
