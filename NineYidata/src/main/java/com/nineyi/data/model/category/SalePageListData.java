package com.nineyi.data.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.salepage.SalePageShort;

import java.util.ArrayList;

/**
 * Created by kelsey on 2016/12/7.
 */

public final class SalePageListData implements Parcelable{
    public static final String SALE_PAGE_LIST = "salePageList";
    public static final String TOTAL_SIZE = "totalSize";
    public static final String SHOP_CATEGORY_ID = "shopCategoryId";
    public static final String SHOP_CATEGORY_NAME = "shopCategoryName";
    public static final String STATUS_DEF = "statusDef";
    public static final String LIST_MODE_DEF = "listModeDef";
    public static final String ORDER_BY_DEF = "orderByDef";
    public static final String DATA_SOURCE = "dataSource";

    @SerializedName("SalePageList")
    public ArrayList<SalePageShort> salePageList;
    @SerializedName("TotalSize")
    public int totalSize;
    @SerializedName("ShopCategoryId")
    public int shopCategoryId;
    @SerializedName("ShopCategoryName")
    public String shopCategoryName;
    @SerializedName("StatusDef")
    public String statusDef;
    @SerializedName("ListModeDef")
    public String listModeDef;
    @SerializedName("OrderByDef")
    public String orderByDef;
    @SerializedName("DataSource")
    public String dataSource;

    public SalePageListData(){ }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.salePageList);
        dest.writeInt(this.totalSize);
        dest.writeInt(this.shopCategoryId);
        dest.writeString(this.shopCategoryName);
        dest.writeString(this.statusDef);
        dest.writeString(this.listModeDef);
        dest.writeString(this.orderByDef);
        dest.writeString(this.dataSource);
    }

    protected SalePageListData(Parcel in) {
        this.salePageList = in.createTypedArrayList(SalePageShort.CREATOR);
        this.totalSize = in.readInt();
        this.shopCategoryId = in.readInt();
        this.shopCategoryName = in.readString();
        this.statusDef = in.readString();
        this.listModeDef = in.readString();
        this.orderByDef = in.readString();
        this.dataSource = in.readString();
    }

    public static final Creator<SalePageListData> CREATOR = new Creator<SalePageListData>() {
        @Override
        public SalePageListData createFromParcel(Parcel source) {
            return new SalePageListData(source);
        }

        @Override
        public SalePageListData[] newArray(int size) {
            return new SalePageListData[size];
        }
    };
}
