package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public final class ShoppingCartPayType implements Parcelable {

    public String TypeDef;
    public String Name;

    public ShoppingCartPayType() {
    }

    // Parcelable management
    private ShoppingCartPayType(Parcel in) {
        TypeDef = in.readString();
        Name = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(TypeDef);
        dest.writeString(Name);
    }

    public static final Parcelable.Creator<ShoppingCartPayType> CREATOR
            = new Parcelable.Creator<ShoppingCartPayType>() {
        public ShoppingCartPayType createFromParcel(Parcel in) {
            return new ShoppingCartPayType(in);
        }

        public ShoppingCartPayType[] newArray(int size) {
            return new ShoppingCartPayType[size];
        }
    };
}
