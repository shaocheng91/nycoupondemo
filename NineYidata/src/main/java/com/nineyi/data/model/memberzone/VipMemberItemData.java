package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class VipMemberItemData implements Parcelable {

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private VipKeyInData mVipKeyInData;


    public VipMemberItemData() {

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(VipKeyInData vipKeyInData) {
        mVipKeyInData = vipKeyInData;
    }

    public VipKeyInData getDatum() {
        return mVipKeyInData;
    }

    public VipMemberItemData(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mVipKeyInData = in.readParcelable(VipKeyInData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipMemberItemData> CREATOR = new Creator<VipMemberItemData>() {
        public VipMemberItemData createFromParcel(Parcel in) {
            return new VipMemberItemData(in);
        }

        public VipMemberItemData[] newArray(int size) {
            return new VipMemberItemData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        dest.writeParcelable(mVipKeyInData, flags);
    }


}