package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class InstallmentTypeData implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("ShopId")
    @Expose
    private Integer ShopId;
    @SerializedName("InstallmentId")
    @Expose
    private Integer InstallmentId;
    @SerializedName("InstallmentDef")
    @Expose
    private Integer InstallmentDef;
    @SerializedName("HasInterest")
    @Expose
    private Boolean HasInterest;
    @SerializedName("InstallmentRate")
    @Expose
    private Double InstallmentRate;
    @SerializedName("InstallmentAmountLimit")
    @Expose
    private Integer InstallmentAmountLimit;
    @SerializedName("BankList")
    @Expose
    private List<String> BankList = new ArrayList<String>();
    @SerializedName("EachInstallmentPrice")
    @Expose
    private Integer EachInstallmentPrice;
    @SerializedName("InterestDisplayName")
    @Expose
    private String InterestDisplayName;
    @SerializedName("DisplayName")
    @Expose
    private String DisplayName;

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The ShopId
     */
    public Integer getShopId() {
        return ShopId;
    }

    /**
     * @param ShopId The ShopId
     */
    public void setShopId(Integer ShopId) {
        this.ShopId = ShopId;
    }

    /**
     * @return The InstallmentId
     */
    public Integer getInstallmentId() {
        return InstallmentId;
    }

    /**
     * @param InstallmentId The InstallmentId
     */
    public void setInstallmentId(Integer InstallmentId) {
        this.InstallmentId = InstallmentId;
    }

    /**
     * @return The InstallmentDef
     */
    public Integer getInstallmentDef() {
        return InstallmentDef;
    }

    /**
     * @param InstallmentDef The InstallmentDef
     */
    public void setInstallmentDef(Integer InstallmentDef) {
        this.InstallmentDef = InstallmentDef;
    }

    /**
     * @return The HasInterest
     */
    public Boolean getHasInterest() {
        return HasInterest;
    }

    /**
     * @param HasInterest The HasInterest
     */
    public void setHasInterest(Boolean HasInterest) {
        this.HasInterest = HasInterest;
    }

    /**
     * @return The InstallmentRate
     */
    public Double getInstallmentRate() {
        return InstallmentRate;
    }

    /**
     * @param InstallmentRate The InstallmentRate
     */
    public void setInstallmentRate(Double InstallmentRate) {
        this.InstallmentRate = InstallmentRate;
    }

    /**
     * @return The InstallmentAmountLimit
     */
    public Integer getInstallmentAmountLimit() {
        return InstallmentAmountLimit;
    }

    /**
     * @param InstallmentAmountLimit The InstallmentAmountLimit
     */
    public void setInstallmentAmountLimit(Integer InstallmentAmountLimit) {
        this.InstallmentAmountLimit = InstallmentAmountLimit;
    }

    /**
     * @return The BankList
     */
    public List<String> getBankList() {
        return BankList;
    }

    /**
     * @param BankList The BankList
     */
    public void setBankList(List<String> BankList) {
        this.BankList = BankList;
    }

    /**
     * @return The EachInstallmentPrice
     */
    public Integer getEachInstallmentPrice() {
        return EachInstallmentPrice;
    }

    /**
     * @param EachInstallmentPrice The EachInstallmentPrice
     */
    public void setEachInstallmentPrice(Integer EachInstallmentPrice) {
        this.EachInstallmentPrice = EachInstallmentPrice;
    }

    /**
     * @return The InterestDisplayName
     */
    public String getInterestDisplayName() {
        return InterestDisplayName;
    }

    /**
     * @param InterestDisplayName The InterestDisplayName
     */
    public void setInterestDisplayName(String InterestDisplayName) {
        this.InterestDisplayName = InterestDisplayName;
    }

    /**
     * @return The DisplayName
     */
    public String getDisplayName() {
        return DisplayName;
    }

    /**
     * @param DisplayName The DisplayName
     */
    public void setDisplayName(String DisplayName) {
        this.DisplayName = DisplayName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.Id);
        dest.writeValue(this.ShopId);
        dest.writeValue(this.InstallmentId);
        dest.writeValue(this.InstallmentDef);
        dest.writeValue(this.HasInterest);
        dest.writeValue(this.InstallmentRate);
        dest.writeValue(this.InstallmentAmountLimit);
        dest.writeStringList(this.BankList);
        dest.writeValue(this.EachInstallmentPrice);
        dest.writeString(this.InterestDisplayName);
        dest.writeString(this.DisplayName);
    }

    public InstallmentTypeData() {
    }

    protected InstallmentTypeData(Parcel in) {
        this.Id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.ShopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.InstallmentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.InstallmentDef = (Integer) in.readValue(Integer.class.getClassLoader());
        this.HasInterest = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.InstallmentRate = (Double) in.readValue(Double.class.getClassLoader());
        this.InstallmentAmountLimit = (Integer) in.readValue(Integer.class.getClassLoader());
        this.BankList = in.createStringArrayList();
        this.EachInstallmentPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.InterestDisplayName = in.readString();
        this.DisplayName = in.readString();
    }

    public static final Parcelable.Creator<InstallmentTypeData> CREATOR
            = new Parcelable.Creator<InstallmentTypeData>() {
        @Override
        public InstallmentTypeData createFromParcel(Parcel source) {
            return new InstallmentTypeData(source);
        }

        @Override
        public InstallmentTypeData[] newArray(int size) {
            return new InstallmentTypeData[size];
        }
    };
}