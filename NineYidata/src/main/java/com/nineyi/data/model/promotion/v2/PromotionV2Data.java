package com.nineyi.data.model.promotion.v2;

import com.nineyi.data.model.category.Category;
import com.nineyi.data.model.gson.NineyiDate;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by tedliang on 2016/8/22.
 */
public class PromotionV2Data implements Parcelable {

    private int PromotionId;
    private String Name;
    private String Description;
    private String TypeDef;
    private String DiscountTypeDef;
    private NineyiDate StartDateTime;
    private NineyiDate EndDateTime;
    private String TargetTypeDef;
    private String PromotionImageUrl;

    private boolean HasExcludedSalePage;

    private int TotalSalePageCountForAllCategory;

    private boolean HasPromotionImage;

    private boolean IsRegular;

    private ArrayList<PromotionTargetMemberTierList> PromotionTargetMemberTierList;
    /**
     * Type : TotalQty
     * DiscountTypeDef : DiscountRate
     * TotalQty : 5
     * TotalPrice : 0
     * DiscountPrice : 0
     * DiscountRate : 0.8
     */

    private List<PromotionConditionList> ConditionList;
    /**
     * CategoryId : 21788
     * Title : 人氣推薦
     * SalePageCount : 5
     * ParentId : 0
     * Sort : 0
     * IsParent : false
     * Level : 1
     * ChildList : []
     */

    private List<Category> CategoryList;

    public boolean isHasPromotionImage() {
        return HasPromotionImage;
    }

    public int getTotalSalePageCountForAllCategory() {
        return TotalSalePageCountForAllCategory;
    }

    public int getPromotionId() {
        return PromotionId;
    }

    public void setPromotionId(int PromotionId) {
        this.PromotionId = PromotionId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public NineyiDate getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(NineyiDate StartDateTime) {
        this.StartDateTime = StartDateTime;
    }

    public NineyiDate getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(NineyiDate EndDateTime) {
        this.EndDateTime = EndDateTime;
    }

    public String getTargetTypeDef() {
        return TargetTypeDef;
    }

    public void setTargetTypeDef(String PromotionTargetType) {
        this.TargetTypeDef = PromotionTargetType;
    }

    public String getPromotionImageUrl() {
        return PromotionImageUrl;
    }

    public void setPromotionImageUrl(String PromotionImageUrl) {
        this.PromotionImageUrl = PromotionImageUrl;
    }

    public List<PromotionConditionList> getConditionList() {
        return ConditionList;
    }

    public void setConditionList(List<PromotionConditionList> ConditionList) {
        this.ConditionList = ConditionList;
    }

    public List<Category> getCategory() {
        return CategoryList;
    }

    public void setCategoryList(List<Category> CategoryList) {
        this.CategoryList = CategoryList;
    }

    public String getDiscountTypeDef() {
        return DiscountTypeDef;
    }

    public String getTypeDef() {
        return TypeDef;
    }

    public boolean isHasExcludedSalePage() {
        return HasExcludedSalePage;
    }

    public boolean isRegular() {
        return IsRegular;
    }

    public void setRegular(boolean regular) {
        IsRegular = regular;
    }

    public ArrayList<PromotionTargetMemberTierList> getPromotionTargetMemberTierList() {
        return PromotionTargetMemberTierList;
    }

    public void setPromotionTargetMemberTierList(ArrayList<PromotionTargetMemberTierList> promotionTargetMemberTierList) {
        PromotionTargetMemberTierList = promotionTargetMemberTierList;
    }

    public PromotionV2Data() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.PromotionId);
        dest.writeString(this.Name);
        dest.writeString(this.Description);
        dest.writeString(this.TypeDef);
        dest.writeString(this.DiscountTypeDef);
        dest.writeParcelable(this.StartDateTime, flags);
        dest.writeParcelable(this.EndDateTime, flags);
        dest.writeString(this.TargetTypeDef);
        dest.writeString(this.PromotionImageUrl);
        dest.writeByte(this.HasExcludedSalePage ? (byte) 1 : (byte) 0);
        dest.writeInt(this.TotalSalePageCountForAllCategory);
        dest.writeByte(this.HasPromotionImage ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsRegular ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.PromotionTargetMemberTierList);
        dest.writeTypedList(this.ConditionList);
        dest.writeTypedList(this.CategoryList);
    }

    protected PromotionV2Data(Parcel in) {
        this.PromotionId = in.readInt();
        this.Name = in.readString();
        this.Description = in.readString();
        this.TypeDef = in.readString();
        this.DiscountTypeDef = in.readString();
        this.StartDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.EndDateTime = in.readParcelable(NineyiDate.class.getClassLoader());
        this.TargetTypeDef = in.readString();
        this.PromotionImageUrl = in.readString();
        this.HasExcludedSalePage = in.readByte() != 0;
        this.TotalSalePageCountForAllCategory = in.readInt();
        this.HasPromotionImage = in.readByte() != 0;
        this.IsRegular = in.readByte() != 0;
        this.PromotionTargetMemberTierList = in.createTypedArrayList(com.nineyi.data.model.promotion.v2.PromotionTargetMemberTierList.CREATOR);
        this.ConditionList = in.createTypedArrayList(PromotionConditionList.CREATOR);
        this.CategoryList = in.createTypedArrayList(Category.CREATOR);
    }

    public static final Creator<PromotionV2Data> CREATOR = new Creator<PromotionV2Data>() {
        @Override
        public PromotionV2Data createFromParcel(Parcel source) {
            return new PromotionV2Data(source);
        }

        @Override
        public PromotionV2Data[] newArray(int size) {
            return new PromotionV2Data[size];
        }
    };
}
