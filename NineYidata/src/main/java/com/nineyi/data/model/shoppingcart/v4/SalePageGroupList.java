
package com.nineyi.data.model.shoppingcart.v4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class SalePageGroupList implements Parcelable {

    public static final String SALE_PAGE_LIST = "SalePageList";

    @SerializedName("TemperatureTypeDef")
    @Expose
    private String TemperatureTypeDef;
    @SerializedName(SALE_PAGE_LIST)
    @Expose
    private List<SalePageList> SalePageList = new ArrayList<SalePageList>();
    @SerializedName("TotalPayment")
    @Expose
    private Double TotalPayment;
    @SerializedName("TotalQty")
    @Expose
    private Integer TotalQty;
    @SerializedName("TotalPrice")
    @Expose
    private Double TotalPrice;
    @SerializedName("TotalFee")
    @Expose
    private Double TotalFee;

    /**
     * 
     * @return
     *     The TemperatureTypeDef
     */
    public String getTemperatureTypeDef() {
        return TemperatureTypeDef;
    }

    /**
     * 
     * @param TemperatureTypeDef
     *     The TemperatureTypeDef
     */
    public void setTemperatureTypeDef(String TemperatureTypeDef) {
        this.TemperatureTypeDef = TemperatureTypeDef;
    }

    /**
     * 
     * @return
     *     The SalePageList
     */
    public List<SalePageList> getSalePageList() {
        return SalePageList;
    }

    /**
     * 
     * @param SalePageList
     *     The SalePageList
     */
    public void setSalePageList(List<SalePageList> SalePageList) {
        this.SalePageList = SalePageList;
    }

    /**
     * 
     * @return
     *     The TotalPayment
     */
    public Double getTotalPayment() {
        return TotalPayment;
    }

    /**
     * 
     * @param TotalPayment
     *     The TotalPayment
     */
    public void setTotalPayment(Double TotalPayment) {
        this.TotalPayment = TotalPayment;
    }

    /**
     * 
     * @return
     *     The TotalQty
     */
    public Integer getTotalQty() {
        return TotalQty;
    }

    /**
     * 
     * @param TotalQty
     *     The TotalQty
     */
    public void setTotalQty(Integer TotalQty) {
        this.TotalQty = TotalQty;
    }

    /**
     * 
     * @return
     *     The TotalPrice
     */
    public Double getTotalPrice() {
        return TotalPrice;
    }

    /**
     * 
     * @param TotalPrice
     *     The TotalPrice
     */
    public void setTotalPrice(Double TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    /**
     * 
     * @return
     *     The TotalFee
     */
    public Double getTotalFee() {
        return TotalFee;
    }

    /**
     * 
     * @param TotalFee
     *     The TotalFee
     */
    public void setTotalFee(Double TotalFee) {
        this.TotalFee = TotalFee;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.TemperatureTypeDef);
        dest.writeList(this.SalePageList);
        dest.writeValue(this.TotalPayment);
        dest.writeValue(this.TotalQty);
        dest.writeValue(this.TotalPrice);
        dest.writeValue(this.TotalFee);
    }

    public SalePageGroupList() {
    }

    protected SalePageGroupList(Parcel in) {
        this.TemperatureTypeDef = in.readString();
        this.SalePageList = new ArrayList<SalePageList>();
        in.readList(this.SalePageList, SalePageList.class.getClassLoader());
        this.TotalPayment = (Double) in.readValue(Integer.class.getClassLoader());
        this.TotalQty = (Integer) in.readValue(Integer.class.getClassLoader());
        this.TotalPrice = (Double) in.readValue(Integer.class.getClassLoader());
        this.TotalFee = (Double) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<SalePageGroupList> CREATOR = new Parcelable.Creator<SalePageGroupList>() {
        @Override
        public SalePageGroupList createFromParcel(Parcel source) {
            return new SalePageGroupList(source);
        }

        @Override
        public SalePageGroupList[] newArray(int size) {
            return new SalePageGroupList[size];
        }
    };
}
