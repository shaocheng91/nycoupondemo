package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public class ShoppingCartTag implements Parcelable {

    public String Color;

    public String TypeDef;

    public String TypeDefDesc;

    public boolean IsMatchCondition;


    protected ShoppingCartTag(Parcel in) {
        Color = in.readString();
        TypeDef = in.readString();
        TypeDefDesc = in.readString();
        IsMatchCondition = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Color);
        dest.writeString(TypeDef);
        dest.writeString(TypeDefDesc);
        dest.writeByte((byte) (IsMatchCondition ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ShoppingCartTag> CREATOR = new Parcelable.Creator<ShoppingCartTag>() {
        @Override
        public ShoppingCartTag createFromParcel(Parcel in) {
            return new ShoppingCartTag(in);
        }

        @Override
        public ShoppingCartTag[] newArray(int size) {
            return new ShoppingCartTag[size];
        }
    };
}
