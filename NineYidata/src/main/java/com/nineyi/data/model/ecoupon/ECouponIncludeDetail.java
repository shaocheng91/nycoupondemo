package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;


public final class ECouponIncludeDetail implements Parcelable {

    public String ReturnCode;
    public ECouponDetail ECouponDetail;

    public ECouponIncludeDetail() {
    }

    // Parcelable management
    private ECouponIncludeDetail(Parcel in) {
        ReturnCode = in.readString();
        ECouponDetail = ECouponDetail.CREATOR.createFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ReturnCode);
        ECouponDetail.writeToParcel(dest, flags);
    }

    public static final Parcelable.Creator<ECouponIncludeDetail> CREATOR
            = new Parcelable.Creator<ECouponIncludeDetail>() {
        public ECouponIncludeDetail createFromParcel(Parcel in) {
            return new ECouponIncludeDetail(in);
        }

        public ECouponIncludeDetail[] newArray(int size) {
            return new ECouponIncludeDetail[size];
        }
    };
}
