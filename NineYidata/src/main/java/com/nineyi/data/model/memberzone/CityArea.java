package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class CityArea implements Parcelable{

    private static final String FIELD_CITY_AREA_INFO_LIST = "DistrictList";
    private static final String FIELD_CITY = "City";


    @SerializedName(FIELD_CITY_AREA_INFO_LIST)
    private List<DistrictList> mDistrictLists;
    @SerializedName(FIELD_CITY)
    private String mCity;


    public CityArea(){

    }

    public void setDistrictLists(List<DistrictList> districtLists) {
        mDistrictLists = districtLists;
    }

    public List<DistrictList> getDistrictLists() {
        return mDistrictLists;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCity() {
        return mCity;
    }

    public CityArea(Parcel in) {
new ArrayList<DistrictList>();
        in.readTypedList(mDistrictLists, DistrictList.CREATOR);
        mCity = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<CityArea> CREATOR = new Parcelable.Creator<CityArea>() {
        public CityArea createFromParcel(Parcel in) {
            return new CityArea(in);
        }

        public CityArea[] newArray(int size) {
            return new CityArea[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mDistrictLists);
        dest.writeString(mCity);
    }


}