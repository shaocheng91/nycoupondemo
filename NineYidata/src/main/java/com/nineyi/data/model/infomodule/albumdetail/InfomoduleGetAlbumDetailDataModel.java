package com.nineyi.data.model.infomodule.albumdetail;

import com.nineyi.data.model.infomodule.InfomoduleCommonDetailDataItemListModel;
import com.nineyi.data.model.infomodule.InfomoduleCommonDetailDataModel;
import com.nineyi.data.model.infomodule.InfomoduleCommonShopSimpleInfoModel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


/**
 * Created by kennylee on 2015/8/24.
 */
public class InfomoduleGetAlbumDetailDataModel extends InfomoduleCommonDetailDataModel {

    public int Id;
    public String Type;
    public String Title;
    public String Subtitle;
    public String Uuid;
    public String ImageUrl;
    public int BackgroundColor;
    public String Gallery;
    public ArrayList<String> GalleryList;
    public String PublishedDate;
    public ArrayList<InfomoduleCommonDetailDataItemListModel> ItemList;
    public InfomoduleCommonShopSimpleInfoModel Shop;

    public InfomoduleGetAlbumDetailDataModel(Parcel in) {
        super(in);
        Id = in.readInt();
        Type = in.readString();
        Title = in.readString();
        Subtitle = in.readString();
        Uuid = in.readString();
        ImageUrl = in.readString();
        BackgroundColor = in.readInt();
        Gallery = in.readString();
        GalleryList = in.createStringArrayList();
        PublishedDate = in.readString();
        ItemList = in.createTypedArrayList(InfomoduleCommonDetailDataItemListModel.CREATOR);
        Shop = InfomoduleCommonShopSimpleInfoModel.CREATOR.createFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeInt(Id);
        dest.writeString(Type);
        dest.writeString(Title);
        dest.writeString(Subtitle);
        dest.writeString(Uuid);
        dest.writeString(ImageUrl);
        dest.writeInt(BackgroundColor);
        dest.writeString(Gallery);
        dest.writeStringList(GalleryList);
        dest.writeString(PublishedDate);
        dest.writeTypedList(ItemList);
        dest.writeParcelable(Shop, flags);
    }

    public static Parcelable.Creator<InfomoduleGetAlbumDetailDataModel> CREATOR
            = new Parcelable.Creator<InfomoduleGetAlbumDetailDataModel>() {
        @Override
        public InfomoduleGetAlbumDetailDataModel createFromParcel(Parcel source) {
            return new InfomoduleGetAlbumDetailDataModel(source);
        }

        @Override
        public InfomoduleGetAlbumDetailDataModel[] newArray(int size) {
            return new InfomoduleGetAlbumDetailDataModel[size];
        }
    };
}
