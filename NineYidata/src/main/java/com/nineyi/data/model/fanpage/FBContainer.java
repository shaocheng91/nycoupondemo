package com.nineyi.data.model.fanpage;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


public class FBContainer implements Parcelable {

    public static final String VIDEO = "video";
    public static final String STATUS = "status";
    public static final String LINK = "link";
    public static final String PHOTO = "photo";
    public static final String ADD_VIDEO = "added_video";
    public List<FBData> data;

    protected FBContainer(Parcel in) {
        if (in.readByte() == 0x01) {
            data = new ArrayList<FBData>();
            in.readList(data, FBData.class.getClassLoader());
        } else {
            data = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
    }


    public static final Parcelable.Creator<FBContainer> CREATOR = new Parcelable.Creator<FBContainer>() {
        @Override
        public FBContainer createFromParcel(Parcel in) {
            return new FBContainer(in);
        }

        @Override
        public FBContainer[] newArray(int size) {
            return new FBContainer[size];
        }
    };
}