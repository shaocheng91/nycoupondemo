package com.nineyi.data.model.login;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class ThirdPartyAuthInfoRoot implements Parcelable{

    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_DATA)
    private ThirdPartyAuthInfoData mData;


    public ThirdPartyAuthInfoRoot(){

    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setDatum(ThirdPartyAuthInfoData data) {
        mData = data;
    }

    public ThirdPartyAuthInfoData getData() {
        return mData;
    }

    public ThirdPartyAuthInfoRoot(Parcel in) {
        mReturnCode = in.readString();
        mMessage = in.readString();
        mData = in.readParcelable(ThirdPartyAuthInfoData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ThirdPartyAuthInfoRoot> CREATOR = new Creator<ThirdPartyAuthInfoRoot>() {
        public ThirdPartyAuthInfoRoot createFromParcel(Parcel in) {
            return new ThirdPartyAuthInfoRoot(in);
        }

        public ThirdPartyAuthInfoRoot[] newArray(int size) {
            return new ThirdPartyAuthInfoRoot[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReturnCode);
        dest.writeString(mMessage);
        dest.writeParcelable(mData, flags);
    }


}