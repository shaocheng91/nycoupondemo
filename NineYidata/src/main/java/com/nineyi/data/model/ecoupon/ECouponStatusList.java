package com.nineyi.data.model.ecoupon;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ECouponStatusList implements Parcelable {

    public ArrayList<ECouponMemberECouponStatusList> MemberECouponStatusList;

    public ECouponStatusList() {
    }

    // Parcelable management
    private ECouponStatusList(Parcel in) {
        MemberECouponStatusList = in.createTypedArrayList(ECouponMemberECouponStatusList.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(MemberECouponStatusList);
    }

    public static final Parcelable.Creator<ECouponStatusList> CREATOR = new Parcelable.Creator<ECouponStatusList>() {
        public ECouponStatusList createFromParcel(Parcel in) {
            return new ECouponStatusList(in);
        }

        public ECouponStatusList[] newArray(int size) {
            return new ECouponStatusList[size];
        }
    };
}
