package com.nineyi.data.model.openapp;

import com.nineyi.data.model.php.AbstractBaseModel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by daisishu on 15/3/19.
 */
public final class CDNDomainData extends AbstractBaseModel implements Parcelable {

    public String CDNDomain;

    public CDNDomainData() {
    }

    // Parcelable management
    private CDNDomainData(Parcel in) {
        CDNDomain = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CDNDomain);
    }

    public static final Creator<CDNDomainData> CREATOR = new Creator<CDNDomainData>() {
        public CDNDomainData createFromParcel(Parcel in) {
            return new CDNDomainData(in);
        }

        public CDNDomainData[] newArray(int size) {
            return new CDNDomainData[size];
        }
    };
}
