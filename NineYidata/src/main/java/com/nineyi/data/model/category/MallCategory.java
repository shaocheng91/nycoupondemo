package com.nineyi.data.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class MallCategory implements Parcelable {

    public int Id;
    public int ParentId;
    public String Name;
    public int Sort;
    public int Level;
    public String Note;
    public boolean HighLight;
    public String PUrl;
    public ArrayList<MallCategory> Data;

    public MallCategory() {
    }

    // Parcelable management
    protected MallCategory(Parcel in) {
        Id = in.readInt();
        ParentId = in.readInt();
        Name = in.readString();
        Sort = in.readInt();
        Level = in.readInt();
        Note = in.readString();
        HighLight = in.readInt() == 1;
        PUrl = in.readString();
        Data = in.createTypedArrayList(MallCategory.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(ParentId);
        dest.writeString(Name);
        dest.writeInt(Sort);
        dest.writeInt(Level);
        dest.writeString(Note);
        dest.writeInt(HighLight ? 1 : 0);
        dest.writeString(PUrl);
        dest.writeTypedList(Data);
    }

    public static final Parcelable.Creator<MallCategory> CREATOR = new Parcelable.Creator<MallCategory>() {
        public MallCategory createFromParcel(Parcel in) {
            return new MallCategory(in);
        }

        public MallCategory[] newArray(int size) {
            return new MallCategory[size];
        }
    };

}
