
package com.nineyi.data.model.salepagev2info;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.nineyi.data.model.ecoupon.ECouponShopECoupon;
import com.nineyi.data.model.gson.NineyiDate;
import com.nineyi.data.model.installment.InstallmentShopInstallmentList;
import com.nineyi.data.model.salepage.SKUPropertySet;
import com.nineyi.data.model.salepage.SalePageGift;
import com.nineyi.data.model.salepage.SalePageImage;
import com.nineyi.data.model.salepage.SalePageMajor;
import com.nineyi.data.model.salepage.SalePageNotKeyProperty;
import com.nineyi.data.model.salepage.SalePageShippingType;

import java.util.ArrayList;
import java.util.List;

public class SalePageV2Info implements Parcelable
{

    @SerializedName("ReturnCode")
    @Expose
    private String returnCode;
    @SerializedName("Data")
    @Expose
    private Data data;
    @SerializedName("Message")
    @Expose
    private String message;
    public final static Parcelable.Creator<SalePageV2Info> CREATOR = new Creator<SalePageV2Info>() {


        @SuppressWarnings({
            "unchecked"
        })
        public SalePageV2Info createFromParcel(Parcel in) {
            SalePageV2Info instance = new SalePageV2Info();
            instance.returnCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.data = ((Data) in.readValue((Data.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SalePageV2Info[] newArray(int size) {
            return (new SalePageV2Info[size]);
        }

    }
    ;

    String getReturnCode() {
        return returnCode;
    }

    void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    Data getData() {
        return data;
    }

    void setData(Data data) {
        this.data = data;
    }

    String getMessage() {
        return message;
    }

    void setMessage(String message) {
        this.message = message;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(returnCode);
        dest.writeValue(data);
        dest.writeValue(message);
    }

    public int describeContents() {
        return  0;
    }

    Double getPrice() {
        return (data != null) ? data.getPrice() : 0.d;
    }

    Double getSuggestPrice() {
        return (data != null) ? data.getSuggestPrice() : 0.d;
    }

    int getShopId() {
        return (data != null) ? data.getShopId() : 0;
    }

    int getShopCategoryId() {
        return (data != null) ? data.getShopCategoryId() : 0;
    }

    int getSalePageId() {
        return (data != null) ? data.getSalePageId() : 0;
    }

    String getTitle() {
        return (data != null) ? data.getTitle() : "";
    }

    ArrayList<SalePageImage> getImageList() {
        return (data != null) ? new ArrayList<>(data.getImageList()) : new ArrayList<SalePageImage>();
    }

    String getPicURL() {
        return (data != null) ? data.getFirstImageUrl() : "";
    }

    String getShopName() {
        return (data != null) ? data.getShopName() : "";
    }

    String getSubDescript() {
        return (data != null) ? data.getSubDescript() : "";
    }

    boolean hasSKU() {
        if (data != null) {
            return data.getHasSKU();
        } else {
            return false;
        }
    }

    ArrayList<SalePageMajor> getMajorList() {
        return (data != null) ? new ArrayList<>(data.getMajorList()) : new ArrayList<SalePageMajor>();
    }

    String getShopCategoryText() {
        return (data != null) ? data.getCategoryText() : null;
    }

    String getShortDescription() {
        return (data != null) ? data.getShortDescription() : null;
    }

    ArrayList<SKUPropertySet> getSKUPropertySetList() {
        return (data != null) ? new ArrayList<>(data.getSKUPropertySetList()) : new ArrayList<SKUPropertySet>();
    }

    ArrayList<String> getPayProfileTypeDefList() {
        return (data != null) ? new ArrayList<>(data.getPayProfileTypeDefList()) : new ArrayList<String>();
    }

    ArrayList<SalePageShippingType> getShippingTypeList() {
        return (data != null) ? new ArrayList<>(data.getShippingTypeList()) : new ArrayList<SalePageShippingType>();
    }

    ArrayList<InstallmentShopInstallmentList> getInstallmentList() {
        return (data != null) ? new ArrayList<>(data.getInstallmentList()) : new ArrayList<InstallmentShopInstallmentList>();
    }

    String getStatusDef() {
        return (data != null) ? data.getStatusDef() : "";
    }

    boolean isDisplayExcludeECouponDiscount() {
        if (data != null) {
            return data.getIsDisplayExcludeECouponDiscount();
        } else {
            return false;
        }
    }

    boolean isShowSoldQty() {
        if (data != null) {
            return data.getIsShowSoldQty();
        } else {
            return false;
        }
    }

    int getSoldQty() {
        return (data != null) ? data.getSoldQty() : 0;
    }

    boolean isShowTradesOrderList() {
        return (data != null) ? data.getIsShowTradesOrderList() : false;
    }

    String getTradesOrderListUri() {
        return (data != null) ? getBuyerListUrl() : "";
    }

    NineyiDate getSellingStartDateTime() {
        return (data != null) ? data.getSellingStartDateTime() : null;
    }

    boolean isShareToBuy() {
        return (data != null) ? data.getIsShareToBuy() : false;
    }

    List<Promotion> getPromotions() {
        return (data != null) ? data.getPromotions() : new ArrayList<Promotion>();
    }

    List<SalePageGift> getGifts() {
        return (data != null) ? getSalePageGiftList() : new ArrayList<SalePageGift>();
    }

    List<ECouponShopECoupon> getECoupons() {
        return (data != null) ? data.getECoupons() : new ArrayList<ECouponShopECoupon>();
    }

    String getFreeShippingTag() {
        return (data != null) ? data.freeShippingTag() : null;
    }

    MainImageVideo getMainImageVideo() {
        return (data != null) ? data.getMainImageVideo() : null;
    }

    private List<SalePageGift> getSalePageGiftList() {
        return (data != null) ? data.getSalePageGiftList() : new ArrayList<SalePageGift>();
    }

    private String getBuyerListUrl() {
        if (data != null && data.getTradesOrderListUri() != null) {
            return data.getTradesOrderListUri().Path;
        } else {
            return "";
        }
    }

    void setStatusDef(String statusDef) {
        if (data != null) {
            data.setStatusDef(statusDef);
        }
    }

    void setPrice(Double price) {
        if (data != null) {
            data.setPrice(price);
        }
    }

    void setSuggestPrice(Double suggestPrice) {
        if (data != null ) {
            data.setSuggestPrice(suggestPrice);
        }
    }
}
