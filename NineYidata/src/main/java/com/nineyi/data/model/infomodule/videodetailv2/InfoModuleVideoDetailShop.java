
package com.nineyi.data.model.infomodule.videodetailv2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoModuleVideoDetailShop implements Parcelable {

    @SerializedName("ShopId")
    @Expose
    private Integer shopId;
    @SerializedName("ShopName")
    @Expose
    private String shopName;
    @SerializedName("HeaderStyle")
    @Expose
    private String headerStyle;

    /**
     * 
     * @return
     *     The shopId
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 
     * @param shopId
     *     The ShopId
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    /**
     * 
     * @return
     *     The shopName
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * 
     * @param shopName
     *     The ShopName
     */
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    /**
     * 
     * @return
     *     The headerStyle
     */
    public String getHeaderStyle() {
        return headerStyle;
    }

    /**
     * 
     * @param headerStyle
     *     The HeaderStyle
     */
    public void setHeaderStyle(String headerStyle) {
        this.headerStyle = headerStyle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.shopId);
        dest.writeString(this.shopName);
        dest.writeString(this.headerStyle);
    }

    public InfoModuleVideoDetailShop() {
    }

    protected InfoModuleVideoDetailShop(Parcel in) {
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.shopName = in.readString();
        this.headerStyle = in.readString();
    }

    public static final Parcelable.Creator<InfoModuleVideoDetailShop> CREATOR
            = new Parcelable.Creator<InfoModuleVideoDetailShop>() {
        @Override
        public InfoModuleVideoDetailShop createFromParcel(Parcel source) {
            return new InfoModuleVideoDetailShop(source);
        }

        @Override
        public InfoModuleVideoDetailShop[] newArray(int size) {
            return new InfoModuleVideoDetailShop[size];
        }
    };
}
