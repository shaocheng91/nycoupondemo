package com.nineyi.data.model.memberzone;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by ReeceCheng on 2016/11/7.
 */
public class CrmShopMemberCard implements Parcelable {
    public String ReturnCode;
    public CrmShopMemberCardData Data;
    public String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ReturnCode);
        dest.writeParcelable(this.Data, flags);
        dest.writeString(this.Message);
    }

    public CrmShopMemberCard() {
    }

    protected CrmShopMemberCard(Parcel in) {
        this.ReturnCode = in.readString();
        this.Data = in.readParcelable(CrmShopMemberCardData.class.getClassLoader());
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<CrmShopMemberCard> CREATOR = new Parcelable.Creator<CrmShopMemberCard>() {
        @Override
        public CrmShopMemberCard createFromParcel(Parcel source) {
            return new CrmShopMemberCard(source);
        }

        @Override
        public CrmShopMemberCard[] newArray(int size) {
            return new CrmShopMemberCard[size];
        }
    };
}
