package com.nineyi.data.model.installment;

import android.os.Parcel;
import android.os.Parcelable;


public final class InstallmentShopShippingTypeList implements Parcelable {

    public int Id;
    public String FeeTypeDef;
    public String FeeTypeDefDesc;
    public int OverPrice;
    public String TypeName;
    public String TypeDef;
    public int Fee;
    public int SupplierId;
    public int ShopId;
    public boolean IsEnabled;
    public boolean IsDefault;
    public String DisplayString;

    public InstallmentShopShippingTypeList() {
    }

    // Parcelable management
    private InstallmentShopShippingTypeList(Parcel in) {
        Id = in.readInt();
        FeeTypeDef = in.readString();
        FeeTypeDefDesc = in.readString();
        OverPrice = in.readInt();
        TypeName = in.readString();
        TypeDef = in.readString();
        Fee = in.readInt();
        SupplierId = in.readInt();
        ShopId = in.readInt();
        IsEnabled = in.readInt() == 1;
        IsDefault = in.readInt() == 1;
        DisplayString = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(FeeTypeDef);
        dest.writeString(FeeTypeDefDesc);
        dest.writeInt(OverPrice);
        dest.writeString(TypeName);
        dest.writeString(TypeDef);
        dest.writeInt(Fee);
        dest.writeInt(SupplierId);
        dest.writeInt(ShopId);
        dest.writeInt(IsEnabled ? 1 : 0);
        dest.writeInt(IsDefault ? 1 : 0);
        dest.writeString(DisplayString);
    }

    public static final Parcelable.Creator<InstallmentShopShippingTypeList> CREATOR
            = new Parcelable.Creator<InstallmentShopShippingTypeList>() {
        public InstallmentShopShippingTypeList createFromParcel(Parcel in) {
            return new InstallmentShopShippingTypeList(in);
        }

        public InstallmentShopShippingTypeList[] newArray(int size) {
            return new InstallmentShopShippingTypeList[size];
        }
    };
}
