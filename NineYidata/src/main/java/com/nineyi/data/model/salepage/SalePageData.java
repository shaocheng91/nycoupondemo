package com.nineyi.data.model.salepage;

import com.nineyi.data.model.ecoupon.ECouponShopECoupon;
import com.nineyi.data.model.installment.InstallmentShopInstallmentList;
import com.nineyi.data.model.promotion.Promotion;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class SalePageData implements Parcelable {

    public ArrayList<SKUPropertySet> SKUPropertySetList;
    public int Id;
    public String SubTitle;
    public String Title;
    public String SubDescript;
    public int RangeDef;
    public double SuggestPrice;
    public double Price;
    public int OptionalGiftCount;
    public String ShortDescription;
    public int ImageCount;
    public String SellingStartDateTime;
    public ArrayList<SalePageImage> ImageList;
    public int[] GoodsSKUIdList;
    public ArrayList<SalePageMajor> MajorList;
    public String[] ExtraList;
    public String[] GiftList;
    public String[] OptionalGiftList;
    public String CreateDateTime;
    public ArrayList<SalePageNotKeyProperty> NotKeyPropertyList;
    public int[] MajorSKUIdList;
    public String CategoryText;
    public int CategoryId;
    public int ShopCategoryId;
    public boolean HasMoreInfo;
    public boolean HasSKU;
    public String StatusDef;
    public int Sort;
    public String SalePageCreateDateTime;
    public String[] DeliverTypeList;
    public String DeliverTypeSet;
    public ArrayList<SalePageShippingType> ShippingTypeList;
    public String[] PayTypeList;
    public ArrayList<String> PayTypeDefList;
    public int NextSalePageId;
    public int PrevSalePageId;
    public int ShopId;
    public String ShopName;
    public String ShopIcon;
    public boolean IsShareToBuy;
    public ArrayList<Promotion> Promotions;
    public ArrayList<String> ServiceInfoList;
    public ArrayList<InstallmentShopInstallmentList> InstallmentList;
    public ArrayList<ECouponShopECoupon> ECoupons;
    public boolean IsShowSoldQty;
    public boolean IsShowTradesOrderList;
    public TradesOrderListUri TradesOrderListUri;
    public int SoldQty;
    public ArrayList<SalePageGift> SalePageGiftList;
    public ArrayList<String> PayProfileTypeDefList;
    public boolean IsDisplayExcludeECouponDiscount;

    public SalePageData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(SKUPropertySetList);
        dest.writeInt(this.Id);
        dest.writeString(this.SubTitle);
        dest.writeString(this.Title);
        dest.writeString(this.SubDescript);
        dest.writeInt(this.RangeDef);
        dest.writeDouble(this.SuggestPrice);
        dest.writeDouble(this.Price);
        dest.writeInt(this.OptionalGiftCount);
        dest.writeString(this.ShortDescription);
        dest.writeInt(this.ImageCount);
        dest.writeString(this.SellingStartDateTime);
        dest.writeTypedList(ImageList);
        dest.writeIntArray(this.GoodsSKUIdList);
        dest.writeTypedList(MajorList);
        dest.writeStringArray(this.ExtraList);
        dest.writeStringArray(this.GiftList);
        dest.writeStringArray(this.OptionalGiftList);
        dest.writeString(this.CreateDateTime);
        dest.writeTypedList(NotKeyPropertyList);
        dest.writeIntArray(this.MajorSKUIdList);
        dest.writeString(this.CategoryText);
        dest.writeInt(this.CategoryId);
        dest.writeInt(this.ShopCategoryId);
        dest.writeByte(HasMoreInfo ? (byte) 1 : (byte) 0);
        dest.writeByte(HasSKU ? (byte) 1 : (byte) 0);
        dest.writeString(this.StatusDef);
        dest.writeInt(this.Sort);
        dest.writeString(this.SalePageCreateDateTime);
        dest.writeStringArray(this.DeliverTypeList);
        dest.writeString(this.DeliverTypeSet);
        dest.writeTypedList(ShippingTypeList);
        dest.writeStringArray(this.PayTypeList);
        dest.writeStringList(this.PayTypeDefList);
        dest.writeInt(this.NextSalePageId);
        dest.writeInt(this.PrevSalePageId);
        dest.writeInt(this.ShopId);
        dest.writeString(this.ShopName);
        dest.writeString(this.ShopIcon);
        dest.writeByte(IsShareToBuy ? (byte) 1 : (byte) 0);
        dest.writeTypedList(Promotions);
        dest.writeStringList(this.ServiceInfoList);
        dest.writeTypedList(InstallmentList);
        dest.writeTypedList(ECoupons);
        dest.writeByte(IsShowSoldQty ? (byte) 1 : (byte) 0);
        dest.writeByte(IsShowTradesOrderList ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.TradesOrderListUri, 0);
        dest.writeInt(this.SoldQty);
        dest.writeTypedList(SalePageGiftList);
        dest.writeStringList(this.PayProfileTypeDefList);
        dest.writeByte(this.IsDisplayExcludeECouponDiscount ? (byte) 1 : (byte) 0);
    }

    protected SalePageData(Parcel in) {
        this.SKUPropertySetList = in.createTypedArrayList(SKUPropertySet.CREATOR);
        this.Id = in.readInt();
        this.SubTitle = in.readString();
        this.Title = in.readString();
        this.SubDescript = in.readString();
        this.RangeDef = in.readInt();
        this.SuggestPrice = in.readDouble();
        this.Price = in.readDouble();
        this.OptionalGiftCount = in.readInt();
        this.ShortDescription = in.readString();
        this.ImageCount = in.readInt();
        this.SellingStartDateTime = in.readString();
        this.ImageList = in.createTypedArrayList(SalePageImage.CREATOR);
        this.GoodsSKUIdList = in.createIntArray();
        this.MajorList = in.createTypedArrayList(SalePageMajor.CREATOR);
        this.ExtraList = in.createStringArray();
        this.GiftList = in.createStringArray();
        this.OptionalGiftList = in.createStringArray();
        this.CreateDateTime = in.readString();
        this.NotKeyPropertyList = in.createTypedArrayList(SalePageNotKeyProperty.CREATOR);
        this.MajorSKUIdList = in.createIntArray();
        this.CategoryText = in.readString();
        this.CategoryId = in.readInt();
        this.ShopCategoryId = in.readInt();
        this.HasMoreInfo = in.readByte() != 0;
        this.HasSKU = in.readByte() != 0;
        this.StatusDef = in.readString();
        this.Sort = in.readInt();
        this.SalePageCreateDateTime = in.readString();
        this.DeliverTypeList = in.createStringArray();
        this.DeliverTypeSet = in.readString();
        this.ShippingTypeList = in.createTypedArrayList(SalePageShippingType.CREATOR);
        this.PayTypeList = in.createStringArray();
        this.PayTypeDefList = in.createStringArrayList();
        this.NextSalePageId = in.readInt();
        this.PrevSalePageId = in.readInt();
        this.ShopId = in.readInt();
        this.ShopName = in.readString();
        this.ShopIcon = in.readString();
        this.IsShareToBuy = in.readByte() != 0;
        this.Promotions = in.createTypedArrayList(Promotion.CREATOR);
        this.ServiceInfoList = in.createStringArrayList();
        this.InstallmentList = in.createTypedArrayList(InstallmentShopInstallmentList.CREATOR);
        this.ECoupons = in.createTypedArrayList(ECouponShopECoupon.CREATOR);
        this.IsShowSoldQty = in.readByte() != 0;
        this.IsShowTradesOrderList = in.readByte() != 0;
        this.TradesOrderListUri = in.readParcelable(com.nineyi.data.model.salepage.TradesOrderListUri.class.getClassLoader());
        this.SoldQty = in.readInt();
        this.SalePageGiftList = in.createTypedArrayList(SalePageGift.CREATOR);
        this.PayProfileTypeDefList = in.createStringArrayList();
        this.IsDisplayExcludeECouponDiscount = in.readByte() != 0;
    }

    public static final Creator<SalePageData> CREATOR = new Creator<SalePageData>() {
        public SalePageData createFromParcel(Parcel source) {
            return new SalePageData(source);
        }

        public SalePageData[] newArray(int size) {
            return new SalePageData[size];
        }
    };
}
