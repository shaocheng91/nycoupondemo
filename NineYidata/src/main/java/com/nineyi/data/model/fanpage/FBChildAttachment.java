package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2016/4/13.
 */
public class FBChildAttachment implements Parcelable {
    public String id;
    public String picture;
    public String name;
    public String link;
    public String caption;
    public String description;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.picture);
        dest.writeString(this.name);
        dest.writeString(this.link);
        dest.writeString(this.caption);
        dest.writeString(this.description);
    }

    public FBChildAttachment() {
    }

    protected FBChildAttachment(Parcel in) {
        this.id = in.readString();
        this.picture = in.readString();
        this.name = in.readString();
        this.link = in.readString();
        this.caption = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<FBChildAttachment> CREATOR = new Parcelable.Creator<FBChildAttachment>() {
        @Override
        public FBChildAttachment createFromParcel(Parcel source) {
            return new FBChildAttachment(source);
        }

        @Override
        public FBChildAttachment[] newArray(int size) {
            return new FBChildAttachment[size];
        }
    };
}
