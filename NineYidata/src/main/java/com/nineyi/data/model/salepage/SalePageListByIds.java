package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class SalePageListByIds implements Parcelable {

    public ArrayList<SalePageShort> SalepageList;

    public SalePageListByIds() {
    }

    // Parcelable management
    private SalePageListByIds(Parcel in) {
        SalepageList = in.createTypedArrayList(SalePageShort.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(SalepageList);
    }

    public static final Parcelable.Creator<SalePageListByIds> CREATOR = new Parcelable.Creator<SalePageListByIds>() {
        public SalePageListByIds createFromParcel(Parcel in) {
            return new SalePageListByIds(in);
        }

        public SalePageListByIds[] newArray(int size) {
            return new SalePageListByIds[size];
        }
    };
}
