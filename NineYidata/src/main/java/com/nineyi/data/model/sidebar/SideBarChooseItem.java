package com.nineyi.data.model.sidebar;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by daisishu on 15/7/13.
 */
public class SideBarChooseItem implements Parcelable {

    public int choosePosition;
    private static final int ORIGINAL_POSITION = -1;

    private SideBarChooseItem(Parcel in) {
        choosePosition = in.readInt();
    }

    public SideBarChooseItem() {
        choosePosition = ORIGINAL_POSITION;
    }

    public void saveChoose(int position) {
        this.choosePosition = position;
    }

    public void removeChoose() {
        choosePosition = ORIGINAL_POSITION;
    }

    public int getChoosePosition() {
        return choosePosition;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(choosePosition);
    }

    public static final Parcelable.Creator<SideBarChooseItem> CREATOR = new Parcelable.Creator<SideBarChooseItem>() {
        public SideBarChooseItem createFromParcel(Parcel in) {
            return new SideBarChooseItem(in);
        }

        public SideBarChooseItem[] newArray(int size) {
            return new SideBarChooseItem[size];
        }
    };
}
