package com.nineyi.data.model.ecoupon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ECouponCouponInfo implements Parcelable {
    public static final String ECOUPONLIST = "ECouponList";

    public int ShopId;
    public String ShopName;

    @SerializedName(ECOUPONLIST)
    @Expose
    public ArrayList<ECouponCouponDetail> ECouponList;

    public ECouponCouponInfo() {
    }

    // Parcelable management
    private ECouponCouponInfo(Parcel in) {
        ShopId = in.readInt();
        ShopName = in.readString();
        ECouponList = in.createTypedArrayList(ECouponCouponDetail.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ShopId);
        dest.writeString(ShopName);
        dest.writeTypedList(ECouponList);
    }

    public static final Parcelable.Creator<ECouponCouponInfo> CREATOR = new Parcelable.Creator<ECouponCouponInfo>() {
        public ECouponCouponInfo createFromParcel(Parcel in) {
            return new ECouponCouponInfo(in);
        }

        public ECouponCouponInfo[] newArray(int size) {
            return new ECouponCouponInfo[size];
        }
    };
}
