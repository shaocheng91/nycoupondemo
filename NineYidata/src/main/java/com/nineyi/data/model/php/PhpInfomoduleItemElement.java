package com.nineyi.data.model.php;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class PhpInfomoduleItemElement extends AbstractBaseModel implements Parcelable {

    public String image_url;
    public String item_id;
    public String title;
    public double price;
    public ArrayList<String> PicList;
    public String PicUrl;
    public double Price;
    public int SalePageId;
    public String SellingStartDateTime;
    public int ShopId;
    public int Sort;
    public String SubTitle;
    public double SuggestPrice;
    public ArrayList<String> Tags;
    public String Title;

    public PhpInfomoduleItemElement() {
    }

    // Parcelable management
    private PhpInfomoduleItemElement(Parcel in) {
        image_url = in.readString();
        item_id = in.readString();
        title = in.readString();
        price = in.readDouble();
        PicList = in.createStringArrayList();
        PicUrl = in.readString();
        Price = in.readDouble();
        SalePageId = in.readInt();
        SellingStartDateTime = in.readString();
        ShopId = in.readInt();
        Sort = in.readInt();
        SubTitle = in.readString();
        SuggestPrice = in.readDouble();
        Tags = in.createStringArrayList();
        Title = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image_url);
        dest.writeString(item_id);
        dest.writeString(title);
        dest.writeDouble(price);
        dest.writeStringList(PicList);
        dest.writeString(PicUrl);
        dest.writeDouble(Price);
        dest.writeInt(SalePageId);
        dest.writeString(SellingStartDateTime);
        dest.writeInt(ShopId);
        dest.writeInt(Sort);
        dest.writeString(SubTitle);
        dest.writeDouble(SuggestPrice);
        dest.writeStringList(Tags);
        dest.writeString(Title);
    }

    public static final Parcelable.Creator<PhpInfomoduleItemElement> CREATOR
            = new Parcelable.Creator<PhpInfomoduleItemElement>() {
        public PhpInfomoduleItemElement createFromParcel(Parcel in) {
            return new PhpInfomoduleItemElement(in);
        }

        public PhpInfomoduleItemElement[] newArray(int size) {
            return new PhpInfomoduleItemElement[size];
        }
    };
}
