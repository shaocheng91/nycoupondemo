package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class SalePageHotList implements Parcelable {

    public int count;
    public ArrayList<ArrayList<SalePageShort>> data;

    public SalePageHotList() {
    }

    // Parcelable management
    public SalePageHotList(Parcel in) {
        count = in.readInt();

        int outerListSize = in.readInt();
        if (outerListSize < 0) {
            data = null;
        } else {
            data = new ArrayList<ArrayList<SalePageShort>>(outerListSize);
            while (outerListSize > 0) {
                int innerListSize = in.readInt();
                if (outerListSize < 0) {
                    data.add(null);
                } else {
                    ArrayList<SalePageShort> innerList = new ArrayList<SalePageShort>(innerListSize);
                    data.add(innerList);
                    while (innerListSize > 0) {
                        if (in.readInt() != 0) {
                            innerList.add(SalePageShort.CREATOR.createFromParcel(in));
                        } else {
                            innerList.add(null);
                        }
                        innerListSize--;
                    }
                }
                outerListSize--;
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);

        if (data == null) {
            dest.writeInt(-1);
        } else {
            int outerListSize = data.size();
            int outerListIndex = 0;
            dest.writeInt(outerListSize);
            while (outerListIndex < outerListSize) {
                ArrayList<SalePageShort> innerList = data.get(outerListIndex);
                if (innerList == null) {
                    dest.writeInt(-1);
                } else {
                    int innerListSize = innerList.size();
                    int innerListIndex = 0;
                    dest.writeInt(innerListSize);
                    while (innerListIndex < innerListSize) {
                        SalePageShort item = innerList.get(innerListIndex);
                        if (item != null) {
                            dest.writeInt(1);
                            item.writeToParcel(dest, flags);
                        } else {
                            dest.writeInt(0);
                        }
                        innerListIndex++;
                    }
                }
                outerListIndex++;
            }
        }
    }

    public static final Parcelable.Creator<SalePageHotList> CREATOR = new Parcelable.Creator<SalePageHotList>() {
        public SalePageHotList createFromParcel(Parcel in) {
            return new SalePageHotList(in);
        }

        public SalePageHotList[] newArray(int size) {
            return new SalePageHotList[size];
        }
    };
}
