package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public final class ShoppingCartShopShortData implements Parcelable {

    public ArrayList<ShoppingCartShopShort> ShopList;

    public ShoppingCartShopShortData() {
    }

    // Parcelable management
    private ShoppingCartShopShortData(Parcel in) {
        ShopList = in.createTypedArrayList(ShoppingCartShopShort.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ShopList);
    }

    public static final Parcelable.Creator<ShoppingCartShopShortData> CREATOR
            = new Parcelable.Creator<ShoppingCartShopShortData>() {
        public ShoppingCartShopShortData createFromParcel(Parcel in) {
            return new ShoppingCartShopShortData(in);
        }

        public ShoppingCartShopShortData[] newArray(int size) {
            return new ShoppingCartShopShortData[size];
        }
    };
}
