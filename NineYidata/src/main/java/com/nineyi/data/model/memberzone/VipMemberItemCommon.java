package com.nineyi.data.model.memberzone;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class VipMemberItemCommon implements Parcelable {

    public static final String CELLPHONE = "CellPhone";
    public static final String EMAIL = "Email";
    public static final String PASSWORD = "OuterPassword";
    public static final String BIRTHDAY = "Birthday";
    public static final String IDENTITY = "IdentityCardId";
    public static final String ADDRESS_CITY = "LocationState";
    public static final String ADDRESS_DISTRICT = "LocationDistrict";
    public static final String ADDRESS_ZIPCODE = "LocationZipCode";

    private static final String FIELD_CUSTOM_NAME = "CustomName";
    private static final String FIELD_COLUMN_HINT = "ColumnHint";
    private static final String FIELD_IS_READ_ONLY = "IsReadOnly";
    private static final String FIELD_DISPLAY = "Display";
    private static final String FIELD_IS_REQUIRE = "IsRequire";
    private static final String FIELD_IS_USING = "IsUsing";
    private static final String FIELD_VALUE = "Value";
    private static final String FIELD_IS_BOUND = "IsBound";
    private static final String FIELD_COLUMN_NAME = "ColumnName";
    private static final String FIELD_COLUMN_TYPE = "ColumnDataType";


    @SerializedName(FIELD_CUSTOM_NAME)
    private String mCustomName;
    @SerializedName(FIELD_COLUMN_HINT)
    private String mColumnHint;
    @SerializedName(FIELD_IS_READ_ONLY)
    private boolean mIsReadOnly;
    @SerializedName(FIELD_DISPLAY)
    private VipKeyInDisplay mVipKeyInDisplay;
    @SerializedName(FIELD_IS_REQUIRE)
    private boolean mIsRequire;
    @SerializedName(FIELD_IS_USING)
    private boolean mIsUsing;
    @SerializedName(FIELD_VALUE)
    private String mValue;
    @SerializedName(FIELD_IS_BOUND)
    private boolean mIsBound;
    @SerializedName(FIELD_COLUMN_NAME)
    private String mColumnName;
    @SerializedName(FIELD_COLUMN_TYPE)
    private String mColumnType;


    public VipMemberItemCommon() {

    }

    public void setCustomName(String customName) {
        mCustomName = customName;
    }

    public String getCustomName() {
        return mCustomName;
    }

    public void setColumnHint(String columnHint) {
        mColumnHint = columnHint;
    }

    public String getColumnHint() {
        return mColumnHint;
    }

    public void setIsReadOnly(boolean isReadOnly) {
        mIsReadOnly = isReadOnly;
    }

    public boolean isIsReadOnly() {
        return mIsReadOnly;
    }

    public void setDisplay(VipKeyInDisplay vipKeyInDisplay) {
        mVipKeyInDisplay = vipKeyInDisplay;
    }

    public VipKeyInDisplay getDisplay() {
        return mVipKeyInDisplay;
    }

    public void setIsRequire(boolean isRequire) {
        mIsRequire = isRequire;
    }

    public boolean isIsRequire() {
        return mIsRequire;
    }

    public void setIsUsing(boolean isUsing) {
        mIsUsing = isUsing;
    }

    public boolean isIsUsing() {
        return mIsUsing;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public void setIsBound(boolean isBound) {
        mIsBound = isBound;
    }

    public boolean isIsBound() {
        return mIsBound;
    }

    public void setColumnName(String columnName) {
        mColumnName = columnName;
    }

    public String getColumnName() {
        return mColumnName;
    }

    public String getColumnType() {
        return mColumnType;
    }

    public void setmColumnType(String mColumnType) {
        this.mColumnType = mColumnType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mCustomName);
        dest.writeString(this.mColumnHint);
        dest.writeByte(this.mIsReadOnly ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.mVipKeyInDisplay, flags);
        dest.writeByte(this.mIsRequire ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mIsUsing ? (byte) 1 : (byte) 0);
        dest.writeString(this.mValue);
        dest.writeByte(this.mIsBound ? (byte) 1 : (byte) 0);
        dest.writeString(this.mColumnName);
        dest.writeString(this.mColumnType);
    }

    protected VipMemberItemCommon(Parcel in) {
        this.mCustomName = in.readString();
        this.mColumnHint = in.readString();
        this.mIsReadOnly = in.readByte() != 0;
        this.mVipKeyInDisplay = in.readParcelable(VipKeyInDisplay.class.getClassLoader());
        this.mIsRequire = in.readByte() != 0;
        this.mIsUsing = in.readByte() != 0;
        this.mValue = in.readString();
        this.mIsBound = in.readByte() != 0;
        this.mColumnName = in.readString();
        this.mColumnType = in.readString();
    }

    public static final Creator<VipMemberItemCommon> CREATOR = new Creator<VipMemberItemCommon>() {
        @Override
        public VipMemberItemCommon createFromParcel(Parcel source) {
            return new VipMemberItemCommon(source);
        }

        @Override
        public VipMemberItemCommon[] newArray(int size) {
            return new VipMemberItemCommon[size];
        }
    };
}