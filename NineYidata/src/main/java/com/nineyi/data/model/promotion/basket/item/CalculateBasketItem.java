
package com.nineyi.data.model.promotion.basket.item;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class CalculateBasketItem implements Parcelable {

    @SerializedName("PromotionId")
    @Expose
    private int promotionId;
    @SerializedName("ShopId")
    @Expose
    private int shopId;
    @SerializedName("SalePageList")
    @Expose
    private List<CalculateBasketSalePageList> salePageList = new ArrayList<CalculateBasketSalePageList>();

    /**
     * 
     * @return
     *     The promotionId
     */
    public int getPromotionId() {
        return promotionId;
    }

    /**
     * 
     * @param promotionId
     *     The PromotionId
     */
    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    /**
     *
     * @return
     *     The shopId
     */
    public int getShopId() {
        return shopId;
    }

    /**
     *
     * @param shopId
     *     The shopId
     */
    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    /**
     * 
     * @return
     *     The salePageList
     */
    public List<CalculateBasketSalePageList> getSalePageList() {
        return salePageList;
    }

    /**
     * 
     * @param salePageList
     *     The SalePageList
     */
    public void setSalePageList(List<CalculateBasketSalePageList> salePageList) {
        this.salePageList = salePageList;
    }

    public CalculateBasketItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.promotionId);
        dest.writeInt(this.shopId);
        dest.writeTypedList(this.salePageList);
    }

    protected CalculateBasketItem(Parcel in) {
        this.promotionId = in.readInt();
        this.shopId = in.readInt();
        this.salePageList = in.createTypedArrayList(CalculateBasketSalePageList.CREATOR);
    }

    public static final Creator<CalculateBasketItem> CREATOR = new Creator<CalculateBasketItem>() {
        @Override
        public CalculateBasketItem createFromParcel(Parcel source) {
            return new CalculateBasketItem(source);
        }

        @Override
        public CalculateBasketItem[] newArray(int size) {
            return new CalculateBasketItem[size];
        }
    };
}
