package com.nineyi.data.model.promotion.v2;


/**
 * Created by tedliang on 2016/8/26.
 */
public class PromoteSalePage {

    /**
     * ReturnCode : API0001
     * Message : Success
     * Data : {"SalePageList":[{"SalePageId":12331,"Title":"商品1","SalePageImageUrl":"http://mobilewebmall.tw.qa/webapi/imagesV3/Original/SalePage/67378/0/125533?v=1","Price":199,"SuggestPrice":350,"IsSoldOut":false},{"SalePageId":12332,"Title":"商品2","SalePageImageUrl":"http://mobilewebmall.tw.qa/webapi/imagesV3/Original/SalePage/67378/0/125533?v=1","Price":189,"SuggestPrice":360,"IsSoldOut":false},{"SalePageId":12333,"Title":"商品3","SalePageImageUrl":"http://mobilewebmall.tw.qa/webapi/imagesV3/Original/SalePage/67378/0/125533?v=1","Price":179,"SuggestPrice":370,"IsSoldOut":true},{"SalePageId":12334,"Title":"商品4","SalePageImageUrl":"http://mobilewebmall.tw.qa/webapi/imagesV3/Original/SalePage/67378/0/125533?v=1","Price":169,"SuggestPrice":380,"IsSoldOut":false},{"SalePageId":12335,"Title":"商品5","SalePageImageUrl":"http://mobilewebmall.tw.qa/webapi/imagesV3/Original/SalePage/67378/0/125533?v=1","Price":159,"SuggestPrice":390,"IsSoldOut":true}]}
     */

    private String ReturnCode;
    private String Message;
    private PromoteSalePageListData Data;

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String ReturnCode) {
        this.ReturnCode = ReturnCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public PromoteSalePageListData getData() {
        return Data;
    }

    public void setData(PromoteSalePageListData Data) {
        this.Data = Data;
    }

}
