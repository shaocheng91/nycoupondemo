package com.nineyi.data.model.referee;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;


public class RefereeLocationList implements Parcelable {

    private static final String FIELD_MESSAGE = "Message";
    private static final String FIELD_RETURN_CODE = "ReturnCode";
    private static final String FIELD_DATA = "Data";


    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_RETURN_CODE)
    private String mReturnCode;
    @SerializedName(FIELD_DATA)
    private LocationDataRoot mDatum;


    public RefereeLocationList() {

    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setDatum(LocationDataRoot datum) {
        mDatum = datum;
    }

    public LocationDataRoot getDatum() {
        return mDatum;
    }

    public RefereeLocationList(Parcel in) {
        mMessage = in.readString();
        mReturnCode = in.readString();
        mDatum = in.readParcelable(LocationDataRoot.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RefereeLocationList> CREATOR = new Creator<RefereeLocationList>() {
        public RefereeLocationList createFromParcel(Parcel in) {
            return new RefereeLocationList(in);
        }

        public RefereeLocationList[] newArray(int size) {
            return new RefereeLocationList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMessage);
        dest.writeString(mReturnCode);
        dest.writeParcelable(mDatum, flags);
    }


}