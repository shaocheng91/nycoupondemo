package com.nineyi.data.model.salepage;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ReeceCheng on 2015/12/9.
 */
public class SalePageGift implements Parcelable {

    public int SalePageId;
    public String PicUrl;
    public int SalePageGiftId;
    public int SalePageGiftSlaveId;
    public int SaleProductSKUId;
    public String Title;
    public int SellingQty;

    public SalePageGift() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.SalePageId);
        dest.writeString(this.PicUrl);
        dest.writeInt(this.SalePageGiftId);
        dest.writeInt(this.SalePageGiftSlaveId);
        dest.writeInt(this.SaleProductSKUId);
        dest.writeString(this.Title);
        dest.writeInt(this.SellingQty);
    }

    protected SalePageGift(Parcel in) {
        this.SalePageId = in.readInt();
        this.PicUrl = in.readString();
        this.SalePageGiftId = in.readInt();
        this.SalePageGiftSlaveId = in.readInt();
        this.SaleProductSKUId = in.readInt();
        this.Title = in.readString();
        this.SellingQty = in.readInt();
    }

    public static final Creator<SalePageGift> CREATOR = new Creator<SalePageGift>() {
        public SalePageGift createFromParcel(Parcel source) {
            return new SalePageGift(source);
        }

        public SalePageGift[] newArray(int size) {
            return new SalePageGift[size];
        }
    };
}
