package com.nineyi.data.model.fanpage;

import android.os.Parcel;
import android.os.Parcelable;


public class FBLocation implements Parcelable {

    String street;
    String city;
    String state;
    String country;
    String zip;
    String latitude;
    String longitude;

    protected FBLocation(Parcel in) {
        street = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        zip = in.readString();
        latitude = in.readString();
        longitude = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(zip);
        dest.writeString(latitude);
        dest.writeString(longitude);
    }

    public static final Parcelable.Creator<FBLocation> CREATOR = new Parcelable.Creator<FBLocation>() {
        @Override
        public FBLocation createFromParcel(Parcel in) {
            return new FBLocation(in);
        }

        @Override
        public FBLocation[] newArray(int size) {
            return new FBLocation[size];
        }
    };
}