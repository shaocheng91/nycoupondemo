package com.nineyi.data.model.shoppingcart;

import android.os.Parcel;
import android.os.Parcelable;


public class ShoppingCartFreeGiftSalePage implements Parcelable{

    public int Price;
    public int SaleProductSKUId;
    public String PicUrl;
    public int SalePageId;
    public int Id;
    public String Title;
    public int Qty;
    public int PromotionConditionId;
    public int PromotionId;
    public boolean IsSoldOut;

    public ShoppingCartFreeGiftSalePage() {

    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof ShoppingCartFreeGiftSalePage){
            return this.Id == ((ShoppingCartFreeGiftSalePage) obj).Id;
        }
        return false;
    }

    public ShoppingCartFreeGiftSalePage(Parcel in) {
        Price = in.readInt();
        SaleProductSKUId = in.readInt();
        PicUrl = in.readString();
        SalePageId = in.readInt();
        Id = in.readInt();
        Title = in.readString();
        Qty = in.readInt();
        PromotionConditionId = in.readInt();
        PromotionId = in.readInt();
        IsSoldOut = in.readInt() == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShoppingCartFreeGiftSalePage> CREATOR = new Creator<ShoppingCartFreeGiftSalePage>() {
        public ShoppingCartFreeGiftSalePage createFromParcel(Parcel in) {
            return new ShoppingCartFreeGiftSalePage(in);
        }

        public ShoppingCartFreeGiftSalePage[] newArray(int size) {
            return new ShoppingCartFreeGiftSalePage[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Price);
        dest.writeInt(SaleProductSKUId);
        dest.writeString(PicUrl);
        dest.writeInt(SalePageId);
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeInt(Qty);
        dest.writeInt(PromotionConditionId);
        dest.writeInt(PromotionId);
        dest.writeInt(IsSoldOut ? 1 : 0);
    }


}