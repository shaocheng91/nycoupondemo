package com.nineyi.data.model.apirequest;

import com.nineyi.data.model.infomodule.InfoModuleClientOfficial;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by toby on 2015/4/9.
 */
public class InfoModuleOfficialValues implements Parcelable {

    public int shopId;

    public List<InfoModuleClientOfficial> infoModuleClients = new ArrayList<>();


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.shopId);
        dest.writeTypedList(infoModuleClients);
    }

    public InfoModuleOfficialValues() {
    }

    private InfoModuleOfficialValues(Parcel in) {
        this.shopId = in.readInt();
        in.readTypedList(infoModuleClients, InfoModuleClientOfficial.CREATOR);
    }

    public static final Creator<InfoModuleOfficialValues> CREATOR = new Creator<InfoModuleOfficialValues>() {
        public InfoModuleOfficialValues createFromParcel(Parcel source) {
            return new InfoModuleOfficialValues(source);
        }

        public InfoModuleOfficialValues[] newArray(int size) {
            return new InfoModuleOfficialValues[size];
        }
    };
}
