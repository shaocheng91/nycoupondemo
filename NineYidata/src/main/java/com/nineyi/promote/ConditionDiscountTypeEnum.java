package com.nineyi.promote;

/**
 * Created by kelsey on 2017/2/6.
 */

public enum ConditionDiscountTypeEnum {
    DiscountPrice(ConditionDiscountTypeEnum.DISCOUNT_PRICE),
    DiscountRate(ConditionDiscountTypeEnum.DISCOUNT_RATE),
    FixedPrice(ConditionDiscountTypeEnum.FIXED_PRICE),
    FreeGift(ConditionDiscountTypeEnum.FREE_GIFT);

    ConditionDiscountTypeEnum(int def) {
        mDef = def;
    }

    private int mDef;

    private static final int DISCOUNT_PRICE = 1;
    private static final int DISCOUNT_RATE = 2;
    private static final int FIXED_PRICE = 4;
    private static final int FREE_GIFT = 8;

    public static ConditionDiscountTypeEnum from(int def) {
        switch (def) {
            case DISCOUNT_PRICE:
                return DiscountPrice;
            case DISCOUNT_RATE:
                return DiscountRate;
            case FIXED_PRICE:
                return FixedPrice;
            case FREE_GIFT:
                return FreeGift;
            default:
                return null;
        }
    }

    public int toInt() {
        return mDef;
    }
}
