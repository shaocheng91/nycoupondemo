package com.nineyi.promote;

/**
 * Created by kelsey on 2017/2/6.
 */

public enum ConditionTypeEnum {
    ReachQty(ConditionTypeEnum.REACH_QTY),
    TotalQty(ConditionTypeEnum.TOTAL_QTY),
    TotalQtyV2(ConditionTypeEnum.TOTAL_QTY_V2),
    TotalPrice(ConditionTypeEnum.TOTAL_PRICE),
    TotalPriceV2(ConditionTypeEnum.TOTAL_PRICE_V2),
    CrmMemberTierTotalPrice(ConditionTypeEnum.CRM_MEMBERTIER_TOTAL_PRICE);

    ConditionTypeEnum(int def) {
        mConditionType = def;
    }

    private int mConditionType;

    private static final int TOTAL_PRICE = 1;
    private static final int TOTAL_QTY = 2;
    private static final int REACH_QTY = 4;
    private static final int TOTAL_PRICE_V2 = 8;
    private static final int TOTAL_QTY_V2 = 16;
    private static final int CRM_MEMBERTIER_TOTAL_PRICE = 32;

    public static ConditionTypeEnum from(int conditionType) {
        switch (conditionType) {
            case TOTAL_PRICE:
                return TotalPrice;
            case TOTAL_QTY:
                return TotalQty;
            case REACH_QTY:
                return ReachQty;
            case TOTAL_PRICE_V2:
                return TotalPriceV2;
            case TOTAL_QTY_V2:
                return TotalQtyV2;
            case CRM_MEMBERTIER_TOTAL_PRICE:
                return CrmMemberTierTotalPrice;
            default:
                return null;
        }
    }

    public int toInt() {
        return mConditionType;
    }
}