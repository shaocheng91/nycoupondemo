package com.nineyi.autoargument.processor;

import com.google.auto.service.AutoService;

import com.nineyi.autoargument.AutoArgument;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;


/**
 * Created by ReeceCheng on 2017/9/8.
 */

@AutoService(Processor.class)
public class AutoArgumentClassProcessor extends AbstractProcessor{

    public static final String ANDROID_OS_PARCELABLE = "android.os.Parcelable";
    public static final String JAVA_IO_SERIALIZABLE = "java.io.Serializable";
    public static final String JAVA_UTIL_ARRAY_LIST = "java.util.ArrayList";
    public static final String JAVA_LANG_STRING = "java.lang.String";
    public static final String JAVA_BUNDLE = "android.os.Bundle";

    private Types mTypeUtils;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        mTypeUtils = processingEnv.getTypeUtils();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        for(Element element : roundEnv.getElementsAnnotatedWith(AutoArgument.class)) {

            if(element.getKind() != ElementKind.CLASS) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "ElementKind error");
                return true;
            }

            ArrayList<FieldSpec> keyFieldSpecs = new ArrayList<>();
            ArrayList<MethodSpec> getterMethodSpecs = new ArrayList<>();
            ArrayList<MethodSpec> setterMethodSpecs = new ArrayList<>();

            TypeElement typeElement = (TypeElement) element;
            String providerClassName = getProviderClassName(typeElement);

            // fetch field declare in class
            for(Element enclosed : element.getEnclosedElements()) {
                if(enclosed.getKind() == ElementKind.FIELD) {

                    // Key
                    FieldSpec fieldSpec = getKeyFieldSpec(enclosed, providerClassName);
                    keyFieldSpecs.add(fieldSpec);

                    // getter
                    MethodSpec getterSpec = getGetterMethodSpec(enclosed);
                    getterMethodSpecs.add(getterSpec);

                    // getter with default value
                    if(!isArrayList(getTypeName(enclosed)) && !isBundleType(getTypeName(enclosed))) {
                        MethodSpec getterWithDefaultSpec = getGetterWithDefaultMethodSpec(enclosed);
                        getterMethodSpecs.add(getterWithDefaultSpec);
                    }

                    // setter
                    MethodSpec setterSpec = getSetterMethodSpec(enclosed);
                    setterMethodSpecs.add(setterSpec);
                }
            }

            // builder
            TypeSpec builderSpec = createBuilder(element, setterMethodSpecs);

            // provider class
            TypeSpec typeSpec = TypeSpec.classBuilder(providerClassName)
                    .addModifiers(Modifier.PUBLIC)
                    .addFields(keyFieldSpecs)
                    .addField(getBundleFieldSpec())
                    .addMethod(getConstructorMethodSpec())
                    .addMethods(getterMethodSpecs)
                    .addType(builderSpec)
                    .build();

            JavaFile javaFile = JavaFile.builder(getPackageName(element), typeSpec)
                    .build();
            try {
                javaFile.writeTo(processingEnv.getFiler());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        return false;
    }

    private String getProviderClassName(TypeElement typeElement)  {
        return typeElement.getSimpleName().toString() + "Provider";
    }

    /**
     * Create like this:
     * private static final String BUNDLE_KEY_SALEPAGEID = "cc.reece.autoparameter.provider.ProductArgumentProvider.SalePageId";
     */
    private FieldSpec getKeyFieldSpec(Element element, String className) {
        return FieldSpec.builder(String.class, getKeyName(element))
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                .initializer(getKeyNameInit(element, className))
                .build();
    }

    /**
     * Create this:
     * private Bundle mBundle;
     */
    private FieldSpec getBundleFieldSpec() {
        return FieldSpec.builder(getAndroidBundleClassName(), "mBundle")
                .addModifiers(Modifier.PRIVATE)
                .build();
    }

    /**
     * Create like this:
     * public int getSalePageId() {
     *     return mBundle.getInt(SalePageId_Key);
     * }
     */
    private MethodSpec getGetterMethodSpec(Element element) {
        String name = String.format("get%s", element.getSimpleName().toString());

        MethodSpec.Builder specBuilder = MethodSpec.methodBuilder(name);
        specBuilder.addModifiers(Modifier.PUBLIC);

        if(isPrimitiveType(element) || isStringType(element)) {
            specBuilder.returns(TypeName.get(element.asType()));
            specBuilder.addCode(getGetterCode(element));

        } else {
            TypeName typeName = getTypeName(element);

            if (isSerializableType(typeName)) {
                specBuilder.returns(typeName);
                specBuilder.addCode(getGetterCode(element));

            } else if(isArrayList(typeName)) {
                TypeMirror arrayListArgumentTypeMirror = getArrayListArgumentTypeMirror(element);

                if(isParcelableType(arrayListArgumentTypeMirror)) {
                    TypeName arrayListArgumentTypeName = getTypeName(((DeclaredType) arrayListArgumentTypeMirror).asElement());

                    /**
                     * public <T extends Parcelable> ArrayList<T> getXXX()
                     */
                    TypeVariableName typeVariableName = TypeVariableName.get("T", arrayListArgumentTypeName);
                    ParameterizedTypeName parameterizedTypeName = ParameterizedTypeName.get(ClassName.get("java.util", "ArrayList"), TypeVariableName.get("T"));

                    specBuilder.addTypeVariable(typeVariableName);
                    specBuilder.returns(parameterizedTypeName);
                } else {
                    /**
                     * public ArrayList<TYPE> getXXX()
                     */
                    ParameterizedTypeName parameterizedTypeName = ParameterizedTypeName.get(ClassName.get("java.util", "ArrayList"), TypeName.get(arrayListArgumentTypeMirror));
                    specBuilder.returns(parameterizedTypeName);
                }

                /**
                 *  return mIntent.getExtras().get[TYPE]ArrayList(KEY);
                 */
                specBuilder.addCode(getArrayListGetterCode(element, arrayListArgumentTypeMirror));
            } else if (isBundleType(element)){
                specBuilder.returns(typeName);
                specBuilder.addCode(getGetterCode(element));
            } else {
                /**
                 *  public <T extends Parcelable> T getXXX()
                 */
                TypeVariableName typeVariableName = TypeVariableName.get("T", typeName);

                specBuilder.addTypeVariable(typeVariableName);
                specBuilder.returns(TypeVariableName.get("T"));
                specBuilder.addCode(getGetterCode(element));
            }
        }

        return specBuilder.build();
    }

    /**
     * Create like this:
     * public int getSalePageId() {
     *     return mBundle.getInt(SalePageId_Key);
     * }
     */
    private MethodSpec getGetterWithDefaultMethodSpec(Element element) {
        String name = String.format("get%s", element.getSimpleName().toString());

        MethodSpec.Builder specBuilder = MethodSpec.methodBuilder(name);
        specBuilder.addModifiers(Modifier.PUBLIC);
        specBuilder.addParameter(TypeName.get(element.asType()), "defaultValue");

        if(isPrimitiveType(element) || isStringType(element)) {
            specBuilder.returns(TypeName.get(element.asType()));
            specBuilder.addCode(getGetterWithDefaultCode(element));

        } else {
            TypeName typeName = getTypeName(element);

            if (isSerializableType(typeName)) {
                specBuilder.returns(typeName);
                specBuilder.addCode(getGetterWithDefaultCode(element));

            } else {
                /**
                 *  public <T extends Parcelable> T getXXX()
                 */
                TypeVariableName typeVariableName = TypeVariableName.get("T", typeName);

                specBuilder.addTypeVariable(typeVariableName);
                specBuilder.returns(TypeVariableName.get("T"));
                specBuilder.addCode(getGetterWithDefaultCode(element));
            }
        }

        return specBuilder.build();
    }

    /**
     * Create like this:
     * public Builder addOtherBundle(Parcelable OtherBundle) {
     *     mArguments.putParcelable(OtherBundle_Key, OtherBundle);
     *     return this;
     * }
     */
    private MethodSpec getSetterMethodSpec(Element element){
        String name = String.format("add%s", element.getSimpleName().toString());
        ClassName returnBuilder = getBuilderClassName();

        MethodSpec.Builder specBuilder =  MethodSpec.methodBuilder(name);
        specBuilder.addModifiers(Modifier.PUBLIC);
        specBuilder.returns(returnBuilder);
        specBuilder.addParameter(TypeName.get(element.asType()), element.getSimpleName().toString());

        TypeName typeName = getTypeName(element);

        if(isArrayList(typeName)) {
            TypeMirror arrayListArgumentTypeMirror = getArrayListArgumentTypeMirror(element);
            specBuilder.addCode(getArrayListSetterCode(element, arrayListArgumentTypeMirror));
        } else {
            specBuilder.addCode(getSetterCode(element));
        }

        return specBuilder.build();
    }

    /**
     * Create this:
     * public ProductArgumentProvider(Bundle bundle) {
     *     mBundle = bundle;
     * }
     */
    private MethodSpec getConstructorMethodSpec() {
        return MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .addParameter(getAndroidBundleClassName(), "bundle")
                .addCode("mBundle = bundle;\n")
                .build();
    }

    /**
     * Create builder class:
     * public static class Builder {
     *     ...
     * }
     */
    private TypeSpec createBuilder(Element element, ArrayList<MethodSpec> setterMethodSpecs) {
        FieldSpec argumentsSpec = getArgumentsFieldSpec();

        MethodSpec builderConstructorMethodSpec = getBuilderConstructorMethodSpec();
        MethodSpec existMethodSpec = getExistMethodSpec();
        MethodSpec buildMethodSpec = getBuildMethodSpec();

        return TypeSpec.classBuilder("BundleBuilder")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addField(argumentsSpec)
                .addMethod(builderConstructorMethodSpec)
                .addMethod(existMethodSpec)
                .addMethods(setterMethodSpecs)
                .addMethod(buildMethodSpec)
                .build();
    }

    /**
     * Create this:
     * private Bundle mArguments;
     */
    private FieldSpec getArgumentsFieldSpec() {
        return FieldSpec.builder(getAndroidBundleClassName(), "mArguments")
                .addModifiers(Modifier.PRIVATE)
                .build();
    }

    /**
     * Create this:
     * public Builder() {
     *     mArguments = new Bundle();
     * }
     */
    private MethodSpec getBuilderConstructorMethodSpec() {
        return MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .addCode("mArguments = new Bundle();\n")
                .build();
    }
    /**
     * Create this:
     * public Builder exist(Bundle bundle) {
     *     mArguments.putAll(bundle);
     *     return this;
     * }
     */
    private MethodSpec getExistMethodSpec() {
        return MethodSpec.methodBuilder("exist")
                .addModifiers(Modifier.PUBLIC)
                .returns(getBuilderClassName())
                .addParameter(getAndroidBundleClassName(), "bundle")
                .addCode("mArguments.putAll(bundle);\nreturn this;\n")
                .build();
    }

    /**
     * Create this:
     * public Bundle build() {
     *     return mArguments;
     * }
     */
    private MethodSpec getBuildMethodSpec() {
        return MethodSpec.methodBuilder("build")
                .addModifiers(Modifier.PUBLIC)
                .returns(getAndroidBundleClassName())
                .addCode("return mArguments;\n")
                .build();
    }

    /**
     * get TypeName, and return the type of their parent or interface if pass white list.
     * If can't find the TypeName, get the Element's TypeName to return.
     */
    private TypeName getTypeName(Element element) {
        TypeName typeName = findTypeNameJavaKnown(element);

        if(typeName != null) {
            return typeName;
        } else {
            typeName = findTypeNameWhiteList(element);

            if(typeName != null) {
                return typeName;
            } else {
                for(TypeMirror typeMirror :  mTypeUtils.directSupertypes(element.asType())) {
                    typeName = findTypeNameWhiteList(mTypeUtils.asElement(typeMirror));

                    if(typeName != null) {
                        return typeName;
                    }
                }
            }
        }

        return TypeName.get(element.asType());
    }

    private TypeName findTypeNameWhiteList(Element element) {
        if (isParcelableType(element) && isSerializableType(element)) {
            return TypeName.get(element.asType());
        } else {
            return null;
        }
    }

    private boolean isPrimitiveType(Element element) {
        TypeName typeName = TypeName.get(element.asType());
        return typeName != TypeName.VOID && (typeName.isPrimitive() || typeName.isBoxedPrimitive());
    }

    private boolean isStringType(Element element) {
        return getQualifiedName(element).equals(JAVA_LANG_STRING);
    }

    private boolean isBundleType(Element element) {
        return getQualifiedName(element).equals(JAVA_BUNDLE);
    }

    private boolean isBundleType(TypeName typeName) {
        return typeName.toString().equals(JAVA_BUNDLE);
    }

    private boolean isSerializableType(Element element) {
        return getQualifiedName(element).equals(JAVA_IO_SERIALIZABLE);
    }

    private boolean isSerializableType(TypeName typeName) {
        return typeName.toString().equals(JAVA_IO_SERIALIZABLE);
    }

    private boolean isParcelableType(Element element) {
        return getQualifiedName(element).equals(ANDROID_OS_PARCELABLE);
    }

    private boolean isParcelableType(TypeMirror typeMirror) {
        TypeName typeName = getTypeName(((DeclaredType) typeMirror).asElement());
        return typeName.toString().equals(ANDROID_OS_PARCELABLE);
    }

    private boolean isArrayList(Element element) {
        return ((DeclaredType)element.asType()).asElement().toString().equals(JAVA_UTIL_ARRAY_LIST);
    }

    private boolean isArrayList(TypeName typeName) {
        if(typeName instanceof ParameterizedTypeName) {
            return ((ParameterizedTypeName) typeName).rawType.toString().equals(JAVA_UTIL_ARRAY_LIST);
        } else {
            return false;
        }
    }

    private TypeName findTypeNameJavaKnown(Element element) {
        TypeName typeName = TypeName.get(element.asType());

        if(isPrimitiveType(element) || isStringType(element) || isArrayList(element)) {
            return typeName;
        } else {
            return null;
        }
    }

    private String getQualifiedName(Element element) {
        return element.asType().toString();
    }

    private String getGetterCode(Element element) {
        String pattern = "return mBundle.get%s(%s);\n";
        return String.format(pattern, getSimpleNameForGetterAndSetter(element), getKeyName(element));
    }

    private String getGetterWithDefaultCode(Element element) {
        String pattern = "return mBundle.get%s(%s, defaultValue);\n";
        return String.format(pattern, getSimpleNameForGetterAndSetter(element), getKeyName(element));
    }

    private String getArrayListGetterCode(Element element, TypeMirror arrayListArgumentTypeMirror) {
        String arrayListArgumentSimpleName = getSimpleNameForGetterAndSetter(((DeclaredType) arrayListArgumentTypeMirror).asElement());

        String pattern = "return mBundle.get%sArrayList(%s);\n";
        return String.format(pattern, arrayListArgumentSimpleName, getKeyName(element));
    }

    private String getSetterCode(Element element) {
        String pattern = "mArguments.put%s(%s, %s);\nreturn this;\n";
        return String.format(pattern, getSimpleNameForGetterAndSetter(element), getKeyName(element), element.getSimpleName());
    }

    private String getArrayListSetterCode(Element element, TypeMirror arrayListArgumentTypeMirror) {
        String arrayListArgumentSimpleName = getSimpleNameForGetterAndSetter(((DeclaredType) arrayListArgumentTypeMirror).asElement());

        String pattern = "mArguments.put%sArrayList(%s, %s);\nreturn this;\n";
        return String.format(pattern, arrayListArgumentSimpleName, getKeyName(element), element.getSimpleName());
    }

    private TypeMirror getArrayListArgumentTypeMirror(Element element) {
        if(!((DeclaredType) element.asType()).getTypeArguments().isEmpty()) {
            return ((DeclaredType) element.asType()).getTypeArguments().get(0);
        } else {
            return element.asType();
        }
    }

    /**
     * 取得 SimpleName 給 getter 與 setter，
     * 更正確來說，是取得 SimpleName 給 Bundle 的 put() 與 get() 使用。
     */
    private String getSimpleNameForGetterAndSetter(Element element) {
        TypeName typeName = getTypeName(element);
        return getSimpleNameForGetterAndSetter(typeName);
    }

    private String getSimpleNameForGetterAndSetter(TypeName typeName) {
        if(typeName != TypeName.VOID && (typeName.isPrimitive() || typeName.isBoxedPrimitive())) {
            if (typeName == TypeName.BOOLEAN || typeName.unbox() == TypeName.BOOLEAN) {
                return "Boolean";
            } else if (typeName == TypeName.BYTE || typeName.unbox() == TypeName.BYTE) {
                return "Byte";
            } else if (typeName == TypeName.SHORT || typeName.unbox() == TypeName.SHORT) {
                return "Short";
            } else if (typeName == TypeName.INT || typeName.unbox() == TypeName.INT) {
                return "Int";
            } else if (typeName == TypeName.LONG || typeName.unbox() == TypeName.LONG) {
                return "Long";
            } else if (typeName == TypeName.CHAR || typeName.unbox() == TypeName.CHAR) {
                return "Char";
            } else if (typeName == TypeName.FLOAT || typeName.unbox() == TypeName.FLOAT) {
                return "Float";
            } else if (typeName == TypeName.DOUBLE || typeName.unbox() == TypeName.DOUBLE) {
                return "Double";
            } else {
                return "";
            }

        } else {
            if(typeName instanceof ParameterizedTypeName) {
                return ((ParameterizedTypeName) typeName).rawType.simpleName();
            } else {
                return ((ClassName) typeName).simpleName();
            }
        }
    }

    private String getKeyName(Element element) {
        String pattern = "BUNDLE_KEY_%s";
        return String.format(pattern, getFieldName(element)).toUpperCase();
    }

    /**
     * packageName.ClassName(thisClassName).elementName
     *
     * example:
     * [packagename].                       [ClassName].                [elementName]
     * cc.reece.autoparameter.provider   .  ProductArgumentProvider  .  SalePageId
     */
    private String getKeyNameInit(Element element, String className) {
        String pattern = "\"%s.%s.%s\"";
        return String.format(pattern, getPackageName(element), className, getFieldName(element));
    }

    private String getFieldName(Element element) {
        return element.getSimpleName().toString();
    }

    private String getPackageName(Element element) {
        PackageElement packageElement = processingEnv.getElementUtils().getPackageOf(element);
        return packageElement.getQualifiedName().toString() + ".provider";
    }

    private ClassName getAndroidIntentClassName() {
       return ClassName.get("android.content", "Intent");
    }

    private ClassName getAndroidBundleClassName() {
        return ClassName.get("android.os", "Bundle");
    }

    private ClassName getBuilderClassName() {
        return ClassName.get("","BundleBuilder");
    }

    private boolean checkType(AnnotationMirror mirror, Class c) {
        return mirror.getAnnotationType().toString().equals(c.getCanonicalName());
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(AutoArgument.class.getCanonicalName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return super.getSupportedSourceVersion();
    }

}
