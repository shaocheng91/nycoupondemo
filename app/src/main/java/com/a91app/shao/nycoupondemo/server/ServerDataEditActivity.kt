package com.a91app.shao.nycoupondemo.server

import android.content.Context
import android.database.DataSetObserver
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.a91app.shao.nycoupondemo.*


/**
 * Created by shaocheng on 2017/11/6.
 */
class ServerDataEditActivity : AppCompatActivity() {
    private var id: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server_coupon_edit)

        id = intent.getIntExtra("id", -1)

        MockServer.demoCoupons.find { it.id == id }?.let { init(it) }
    }

    private fun init(demoCoupon: DemoCoupon) {
        val idEditText = findViewById(R.id.edit_text_id) as TextView
        idEditText.text = demoCoupon.id.toString()

        val typeSpinner = findViewById(R.id.spinner_coupon_edit_type) as Spinner
        typeSpinner.adapter = TypeSpinnerAdapter(this)

        val discountInput = findViewById(R.id.coupon_edit_input_discount_price) as EditText
        discountInput.setText(demoCoupon.discountPrice.toString())

        val countInput = findViewById(R.id.coupon_edit_input_count) as EditText
        countInput.setText(demoCoupon.couponTotalCount.toString())

        val hasCouponToggle = findViewById(R.id.coupon_edit_input_has_coupon) as Switch
        hasCouponToggle.isChecked = demoCoupon.hasCoupon

        val isUsedToggle = findViewById(R.id.coupon_edit_input_is_used) as Switch
        isUsedToggle.isChecked = demoCoupon.isUsed

        val startTimeInput = findViewById(R.id.coupon_edit_input_start_time) as EditText
        startTimeInput.setText(demoCoupon.startDateTime.toSimpleString())

        val endTimeInput = findViewById(R.id.coupon_edit_input_end_time) as EditText
        endTimeInput.setText(demoCoupon.endDateTime.toSimpleString())

        val usingStartTimeInput = findViewById(R.id.coupon_edit_input_using_start_time) as EditText
        usingStartTimeInput.setText(demoCoupon.usingStartDateTime.toSimpleString())

        val usingEndTimeInput = findViewById(R.id.coupon_edit_input_using_end_time) as EditText
        usingEndTimeInput.setText(demoCoupon.usingEndDateTime.toSimpleString())

        val maxDiscountLimitInput = findViewById(R.id.coupon_edit_input_max_discount_limit) as EditText
        maxDiscountLimitInput.setText(demoCoupon.eCouponMaxDiscountLimit.toString())

        val saveButton = findViewById(R.id.coupon_edit_button_save)
        saveButton.setOnClickListener {
            MockServer.demoCoupons = MockServer.demoCoupons.map {
                if (it.id == id) {
                    it.copy(
                            type = (typeSpinner.selectedItem as Type).demoCouponType,
                            discountPrice = discountInput.text.toString().toDouble(),
                            couponTotalCount = countInput.text.toString().toInt(),
                            hasCoupon = hasCouponToggle.isChecked,
                            isUsed = isUsedToggle.isChecked,
                            startDateTime = startTimeInput.text.toString().toSimpleDate(),
                            endDateTime = endTimeInput.text.toString().toSimpleDate(),
                            usingStartDateTime = usingStartTimeInput.text.toString().toSimpleDate(),
                            usingEndDateTime =  usingEndTimeInput.text.toString().toSimpleDate(),
                            eCouponMaxDiscountLimit = maxDiscountLimitInput.text.toString().toDouble()
                    )
                } else {
                    it
                }
            }
            this@ServerDataEditActivity.finish()
        }
    }
}

class TypeSpinnerAdapter(val context: Context) : SpinnerAdapter {

    val types = DemoCouponType.values().toList().map {
        when (it) {
            DemoCouponType.DRAW_OUT -> Type(it, "一般卷")
            DemoCouponType.FIRST_DOWNLOAD -> Type(it, "首下載")
            DemoCouponType.CODE -> Type(it, "卷號型")
            DemoCouponType.OPEN_CARD -> Type(it, "開卡禮")
            DemoCouponType.BIRTHDAY -> Type(it, "生日禮")
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(android.R.layout.simple_spinner_item, parent, false)

        (view as TextView).text = types[position].displayWording

        return view
    }

    override fun getItem(position: Int) = types[position]

    override fun getItemId(position: Int) = types[position].demoCouponType.ordinal.toLong()

    override fun getCount() = types.size

    override fun isEmpty(): Boolean = types.isEmpty()

    override fun registerDataSetObserver(observer: DataSetObserver?) = Unit

    override fun getItemViewType(position: Int): Int = 0

    override fun getViewTypeCount(): Int = 1

    override fun hasStableIds(): Boolean = true

    override fun unregisterDataSetObserver(observer: DataSetObserver?) = Unit

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)

        (view as TextView).text = types[position].displayWording

        return view
    }
}

data class Type(val demoCouponType: DemoCouponType, val displayWording: String)
