package com.a91app.shao.nycoupondemo.server

import com.a91app.shao.nycoupondemo.server.MockServer.API_DELAY
import com.nineyi.data.ReturnCodeType
import com.nineyi.data.model.apiresponse.ReturnCode
import com.nineyi.data.model.ecoupon.*
import com.nineyi.module.coupon.service.CouponService
import com.nineyi.module.coupon.service.LoginManager
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import java.lang.Math.random
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by shaocheng on 2017/11/3.
 */
object MockServer : CouponService, LoginManager {
    override fun setECouponVerify(shopId: Int, couponId: Int): Flowable<ECouponVerify> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val API_DELAY = 1L

    var demoCoupons: List<DemoCoupon> = listOf(
            DemoCoupon(
                    id = 100,
                    type = DemoCouponType.DRAW_OUT,
                    discountPrice = 100.0,
                    couponTotalCount = 1,
                    description = "我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊...",
                    name = "請完整顯示活動名稱共四十個字元，請完整顯示活動名稱共40個字元請完整顯示活動名稱",
                    eCouponMaxDiscountLimit = 0.11
            ),
            DemoCoupon(
                    id = 200,
                    type = DemoCouponType.DRAW_OUT,
                    discountPrice = 200.0,
                    couponTotalCount = 1,
                    description = "我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊...",
                    name = "請完整顯示活動名稱共四十個字元，請完整顯示活動名稱共40個字元請完整顯示活動名稱",
                    eCouponMaxDiscountLimit = 0.11
            ),
            DemoCoupon(
                    id = 300,
                    type = DemoCouponType.DRAW_OUT,
                    discountPrice = 300.0,
                    couponTotalCount = 0,
                    description = "我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊我是活動說明這一塊的邏輯照舊...",
                    name = "請完整顯示活動名稱共四十個字元，請完整顯示活動名稱共40個字元請完整顯示活動名稱",
                    eCouponMaxDiscountLimit = 0.11
            )
    )

    fun createNewCoupon(): Int {
        demoCoupons.map { it -> it.id }.max()?.plus(1)?.let {
            val newCoupon = DemoCoupon(id = it)
            demoCoupons = demoCoupons.plus(newCoupon)
            return it
        }
        return -1
    }

    var isLoggedIn: Boolean = true

    override fun isLogin(): Boolean = isLoggedIn

    override fun getECouponListWithoutCode(shopId: Int): Flowable<ECouponShopECouponList> {
        val eCouponList = demoCoupons.map { it.toECouponDetail() }

        val eCouponShopECoupon = ECouponShopECoupon()
        eCouponShopECoupon.ECouponList = ArrayList(eCouponList)

        val shopECouponList = ArrayList<ECouponShopECoupon>()
        shopECouponList.add(eCouponShopECoupon)

        val eCouponShopECouponList = ECouponShopECouponList()

        eCouponShopECouponList.ReturnCode = "API0001"
        eCouponShopECouponList.ShopECouponList = shopECouponList

        return justFlowable(eCouponShopECouponList)
    }

    override fun getMemberECouponStatusList(eCouponIdListString: String?, fbId: String?): Flowable<ECouponStatusList> {
        val eCouponStatusList = ECouponStatusList()

        eCouponStatusList.MemberECouponStatusList = ArrayList()

        if (isLoggedIn) {

            val split = eCouponIdListString?.split(",".toRegex())?.dropLastWhile { it.isEmpty() }?.toTypedArray()

            if (split != null) {
                for (s in split) {

                    val eCouponMemberECouponStatusList = ECouponMemberECouponStatusList()
                    eCouponMemberECouponStatusList.ECouponId = demoCoupons.find { it.id == s.toInt() }?.id?.toLong() ?: -1
                    eCouponMemberECouponStatusList.IsUsing = demoCoupons.find { it.id == s.toInt() }?.isUsed ?: false
                    eCouponMemberECouponStatusList.HasNormalCoupon = demoCoupons.find { it.id == s.toInt() }?.hasCoupon ?: false

                    eCouponStatusList.MemberECouponStatusList.add(eCouponMemberECouponStatusList)
                }
            }

        }

        return justFlowable(eCouponStatusList)
    }

    override fun setMemberECouponByCode(shopId: Int, code: String?, guid: String?): Flowable<ReturnCode> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setMemberECouponByECouponId(ecouponId: Int, guid: String?): Flowable<ReturnCode> {
        val returnCode = ReturnCode()


        demoCoupons.find { it.id == ecouponId}?.let {
            if (DateTime.now().isAfter(it.endDateTime)) {
                returnCode.ReturnCode = ReturnCodeType.API0002.toString()
                return justFlowable(returnCode)
            }
        }

        if (demoCoupons.find { it.id == ecouponId }?.hasCoupon == true) {
            returnCode.ReturnCode = ReturnCodeType.API0002.toString()
        } else {
            demoCoupons = demoCoupons.map {
                if (it.id == ecouponId) {
                    it.copy(hasCoupon = true)
                } else {
                    it
                }
            }

            returnCode.ReturnCode = ReturnCodeType.API0001.toString()
        }

        return justFlowable(returnCode)
    }

    override fun setMemberFirstDownloadECouponByECouponId(ecouponId: Int, guid: String?): Flowable<ReturnCode> {
        val returnCode = ReturnCode()

        if (demoCoupons.find { it.id == ecouponId }?.hasCoupon == true) {
            returnCode.ReturnCode = ReturnCodeType.API0002.toString()
        } else {
            demoCoupons = demoCoupons.map {
                if (it.id == ecouponId) {
                    it.copy(hasCoupon = true)
                } else {
                    it
                }
            }

            returnCode.ReturnCode = ReturnCodeType.API0001.toString()
        }

        return justFlowable(returnCode)
    }

    override fun getECouponDetail(ecouponId: Int): Flowable<ECouponIncludeDetail> {
        val eCouponIncludeDetail = ECouponIncludeDetail()

        eCouponIncludeDetail.ReturnCode = ReturnCodeType.API0001.toString()
        eCouponIncludeDetail.ECouponDetail = demoCoupons.find {it.id == ecouponId}?.toECouponDetail()

        return justFlowable(eCouponIncludeDetail)
    }

    override fun getMemberECouponListByUsingDateTime(shopId: Int): Flowable<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getMemberECouponList(shopId: Int): Flowable<ECouponMemberList> {

        return if (isLoggedIn) {
            val eCouponList = demoCoupons.filter { it.hasCoupon }.map { it -> it.toECouponCouponDetail() }

            val eCouponCouponInfo = ECouponCouponInfo()
            eCouponCouponInfo.ECouponList = ArrayList(eCouponList)

            val eCouponMemberList = ECouponMemberList()
            eCouponMemberList.ECouponList = arrayListOf(eCouponCouponInfo)

            justFlowable(eCouponMemberList)
        } else {
            justFlowable(ECouponMemberList())
        }
    }


}


inline fun <T> justFlowable(obj: T): Flowable<T> {
    return Flowable.just(obj)
            .delay(API_DELAY, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
}