package com.a91app.shao.nycoupondemo

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.a91app.shao.nycoupondemo.server.ServerSetupActivity
import com.nineyi.module.coupon.ui.list.CouponListActivity
import com.nineyi.module.coupon.ui.my.MyCouponActivity

/**
 * Created by shaocheng on 2017/11/3.
 */
class DemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)

        val serverCouponListButton = findViewById(R.id.button_server_setup) as Button
        serverCouponListButton.setOnClickListener { startActivity(Intent(this, ServerSetupActivity::class.java)) }

        val couponListButton = findViewById(R.id.button_coupon_list) as Button
        couponListButton.setOnClickListener { startActivity(Intent(this, CouponListActivity::class.java)) }

        findViewById(R.id.demo_main_button_my_coupon_list).setOnClickListener {
            startActivity(Intent(this, MyCouponActivity::class.java))
        }
    }
}