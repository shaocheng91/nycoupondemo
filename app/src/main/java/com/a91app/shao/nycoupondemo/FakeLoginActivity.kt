package com.a91app.shao.nycoupondemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.a91app.shao.nycoupondemo.server.MockServer

/**
 * Created by shaocheng on 2017/11/6.
 */
class FakeLoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fake_login)

        findViewById(R.id.fake_login_button_login).setOnClickListener {
            MockServer.isLoggedIn = true
            finish()
        }
    }
}