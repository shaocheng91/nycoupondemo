package com.a91app.shao.nycoupondemo

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import com.a91app.shao.nycoupondemo.server.MockServer
import com.nineyi.data.AppBuildConfig
import com.nineyi.data.model.shoppingcart.v4.ShoppingCartData
import com.nineyi.module.base.provider.analytics.AnalyticsProvider
import com.nineyi.module.base.provider.analytics.IAnalyticsProvider
import com.nineyi.module.base.provider.application.ApplicationProvider
import com.nineyi.module.base.provider.application.IApplicationProvider
import com.nineyi.module.base.provider.tint.ITintProvider
import com.nineyi.module.base.provider.tint.TintProvider
import com.nineyi.module.coupon.CouponComponent
import com.nineyi.module.coupon.service.*
import org.joda.time.DateTime
import java.util.*

/**
 * Created by shaocheng on 2017/11/3.
 */
class DemoApp : Application() {
    override fun onCreate() {

        super.onCreate()
        // Have to do this to prevent NPE
        AnalyticsProvider.getInstance().init(object : IAnalyticsProvider {
            override fun clearScreenName() {}
            override fun setScreenNameWithPageTitle(screenName: String, id: String, name: String) {}
            override fun sendScreenView(screenName: String) {}
            override fun sendEventHit(category: String, action: String, label: String) {}
            override fun sendEventHit(category: String, action: String) {}
            override fun sendTimingHit(category: String, name: String, label: String, loadTime: Long?) {}
            override fun sendFacebookRegisterEvent(context: Context) {}
            override fun send91RegisterEvent(context: Context) {}
        })

        ApplicationProvider.getInstance().init(object : IApplicationProvider {
            override fun getProviderTag(): String? {
                return null
            }

            override fun getProviderAppContext(): Context {
                return this@DemoApp
            }

            override fun getProviderAppResources(): Resources {
                return this@DemoApp.resources
            }

            override fun getProviderAppBuildConfig(): AppBuildConfig? {
                return AppBuildConfig.createDefault()
            }

            override fun getProviderAppLogo(): Drawable? {
                return null
            }
        })

        TintProvider.getInstance().init(object : ITintProvider {
            override fun setBackGroundTint(v: View, pressedColor: Int, normalColor: Int) {}
            override fun setToolbarTint(toolbar: Toolbar) {}
            override fun getTintDrawable(d: Drawable, pressColor: Int, normalColor: Int): Drawable? {
                return null
            }
        })

        val migrationManager = MigrationManager { DateTime.now().millis }

        val idManager = CouponIdManager { UUID.randomUUID().toString() }

        val navManager = object : NavManager {
            override fun navigateLogin(context: Context?) {
                startActivity(Intent(context, FakeLoginActivity::class.java))
            }
        }

        val couponShoppingCartDataManager = object : CouponShoppingCartDataManager {
            override fun updateEcouponList(couponList: String) {}
            override fun updateSelectedEcouponSlaveId(couponId: Int) {}
            override fun setShouldRefreshShoppingCart() {}
            override fun getShoppingCartData(): ShoppingCartData? {
                return null
            }
        }

        val couponShareHelper = object : CouponShareHelper {
            override fun shareToFb(activity: Activity, id: Int, imgUrl: String, shopName: String, discoutPrice: Double) {

            }

            override fun shareToLine(activity: Activity, id: Int, shopName: String, discountPrice: Double) {

            }

            override fun shareToSMS(activity: Activity, id: Int, shopName: String, discountPrice: Double) {

            }

            override fun shareToMail(activity: Activity, id: Int, shopName: String, discountPrice: Double) {

            }
        }

        val couponTint = object : CouponTint {
            override fun setImageDrawableTint(v: ImageView?, pressedColor: Int, normalColor: Int) {
            }
        }

        CouponComponent.init(this, 64, MockServer,
                migrationManager, MockServer, idManager, navManager,
                couponShoppingCartDataManager, couponShareHelper, couponTint);
    }
}
