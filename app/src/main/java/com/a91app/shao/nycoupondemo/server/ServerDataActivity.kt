package com.a91app.shao.nycoupondemo.server

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import com.a91app.shao.nycoupondemo.R
import com.a91app.shao.nycoupondemo.dp
import com.a91app.shao.nycoupondemo.toPx

/**
 * Created by shaocheng on 2017/11/6.
 */
class ServerDataActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server_data)

        title = "Server Data"

        recyclerView = findViewById(R.id.server_data_list) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
                outRect?.bottom = dp(8).toPx()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        recyclerView.adapter = ServerDataAdapter(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menu?.add(0, 0, 0, "Add new data")
                ?.setIcon(R.drawable.ic_add_black)
                ?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            0 -> {
                val newCouponId = MockServer.createNewCoupon()

                val intent = Intent(this, ServerDataEditActivity::class.java)
                intent.putExtra("id", newCouponId)

                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

class ServerDataAdapter(val context: Context) : RecyclerView.Adapter<ServerDataViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ServerDataViewHolder {
        val serverDataView = TextView(context)
        serverDataView.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        return ServerDataViewHolder(serverDataView)
    }

    override fun onBindViewHolder(holder: ServerDataViewHolder?, position: Int) {
        holder?.infoTextView?.text = MockServer.demoCoupons[position].toString()
        holder?.view?.setOnClickListener {
            val demoCoupons = MockServer.demoCoupons[position]

            val intent = Intent(context, ServerDataEditActivity::class.java)
            intent.putExtra("id", demoCoupons.id)

            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return MockServer.demoCoupons.size
    }
}

class ServerDataViewHolder(serverDataView: View) : RecyclerView.ViewHolder(serverDataView) {
    val view = serverDataView as TextView
    val infoTextView = serverDataView as TextView
}
