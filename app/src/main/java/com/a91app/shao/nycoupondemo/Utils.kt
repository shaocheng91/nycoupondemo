package com.a91app.shao.nycoupondemo

import android.content.Context
import android.content.res.Resources

/**
 * Created by shaocheng on 2017/11/7.
 */
fun Context.dp(unit: Int): DP = DP(this, unit)

fun DP.toPx(): Int = (value * context.resources.displayMetrics.density).toInt()

data class DP(val context: Context, val value: Int)
