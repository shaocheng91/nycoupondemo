package com.a91app.shao.nycoupondemo.server

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Switch
import com.a91app.shao.nycoupondemo.R

/**
 * Created by shaocheng on 2017/11/7.
 */
class ServerSetupActivity : AppCompatActivity() {
    private lateinit var isLoggedInInput: Switch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server_setup)

        (findViewById(R.id.server_setup_view_data_button) as Button).setOnClickListener {
            startActivity(Intent(this, ServerDataActivity::class.java))
        }

        isLoggedInInput = findViewById(R.id.server_setup_input_is_logged_in) as Switch
        isLoggedInInput.setOnCheckedChangeListener { _, isChecked ->
            MockServer.isLoggedIn = isChecked
        }
    }

    override fun onResume() {
        super.onResume()
        isLoggedInInput.isChecked = MockServer.isLoggedIn
    }
}