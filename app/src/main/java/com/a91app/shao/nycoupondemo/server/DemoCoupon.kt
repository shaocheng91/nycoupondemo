package com.a91app.shao.nycoupondemo.server

import com.nineyi.data.model.ecoupon.ECouponCouponDetail
import com.nineyi.data.model.ecoupon.ECouponDetail
import com.nineyi.data.model.gson.NineyiDate
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/**
 * Created by shaocheng on 2017/11/3.
 */
data class DemoCoupon(
        val id: Int,
        val type: DemoCouponType = DemoCouponType.DRAW_OUT,
        val discountPrice: Double = 100.0,
        val couponTotalCount: Int = 100,
        val startDateTime: DateTime = DateTime.now().minusDays(7).withTimeAtStartOfDay(),
        val endDateTime: DateTime = DateTime.now().plusDays(7).withTimeAtStartOfDay(),
        val usingStartDateTime: DateTime = DateTime.now().minusDays(7).withTimeAtStartOfDay(),
        val usingEndDateTime: DateTime = DateTime.now().plusDays(14).withTimeAtStartOfDay(),
        val description: String = "活動說明",
        val name: String = "活動名稱",
        val eCouponMaxDiscountLimit: Double = 0.01,
        val isOnline: Boolean = true,
        val isOffline: Boolean = false,
        val hasCoupon: Boolean = false,
        val isUsed: Boolean = false
) {

    override fun toString(): String {
        return """
            |id: $id,
            |  type: $type,
            |  price: $discountPrice
            |  count: $couponTotalCount
            |  start date: ${startDateTime.toSimpleString()}
            |  end date: ${endDateTime.toSimpleString()}
            |  using start date: ${usingStartDateTime.toSimpleString()}
            |  using end date: ${usingEndDateTime.toSimpleString()}
            |  max discount limit: $eCouponMaxDiscountLimit
            |  has coupon: $hasCoupon
            |  isUsed: $isUsed
            """.trimMargin()
    }
}

enum class DemoCouponType {
    DRAW_OUT, FIRST_DOWNLOAD, CODE, OPEN_CARD, BIRTHDAY
}

fun DemoCoupon.toECouponDetail(): ECouponDetail {

    val eCouponDetail = ECouponDetail()

    eCouponDetail.Id = id
    eCouponDetail.TypeDef = when (type) {
        DemoCouponType.DRAW_OUT -> "drawout"
        DemoCouponType.FIRST_DOWNLOAD -> "firstdownload"
        DemoCouponType.CODE -> "code"
        DemoCouponType.OPEN_CARD -> "opencard"
        DemoCouponType.BIRTHDAY -> "birthday"
    }
    eCouponDetail.DiscountPrice = discountPrice
    eCouponDetail.CouponTotalCount = couponTotalCount
    eCouponDetail.StartDateTime = startDateTime.toNineYiDate()
    eCouponDetail.EndDateTime = endDateTime.toNineYiDate()
    eCouponDetail.UsingStartDateTime = usingStartDateTime.toNineYiDate()
    eCouponDetail.UsingEndDateTime = usingEndDateTime.toNineYiDate()
    eCouponDetail.Description = description
    eCouponDetail.Name = name
    eCouponDetail.ECouponMaxDiscountLimit = eCouponMaxDiscountLimit
    eCouponDetail.IsOnline = isOnline
    eCouponDetail.IsOffline = isOffline

    return eCouponDetail
}

fun DemoCoupon.toECouponCouponDetail(): ECouponCouponDetail {

    val eCouponCouponDetail = ECouponCouponDetail()

    eCouponCouponDetail.ECouponId = id
    eCouponCouponDetail.TypeDef = when (type) {
        DemoCouponType.DRAW_OUT -> "drawout"
        DemoCouponType.FIRST_DOWNLOAD -> "firstdownload"
        DemoCouponType.CODE -> "code"
        DemoCouponType.OPEN_CARD -> "opencard"
        DemoCouponType.BIRTHDAY -> "birthday"
    }
    eCouponCouponDetail.DiscountPrice = discountPrice
    eCouponCouponDetail.UsingStartDateTime = usingStartDateTime.toNineYiDate()
    eCouponCouponDetail.UsingEndDateTime = usingEndDateTime.toNineYiDate()
    eCouponCouponDetail.ECouponMaxDiscountLimit = eCouponMaxDiscountLimit

    return eCouponCouponDetail
}

val SIMPLE_DATE_FORMATTER: DateTimeFormatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss")

fun DateTime.toSimpleString(): String {
    return SIMPLE_DATE_FORMATTER.print(this)
}

fun String.toSimpleDate(): DateTime {
    return SIMPLE_DATE_FORMATTER.parseDateTime(this)
}

fun DateTime.toNineYiDate(): NineyiDate {
    return NineyiDate("", this.millis.toString(), "")
}