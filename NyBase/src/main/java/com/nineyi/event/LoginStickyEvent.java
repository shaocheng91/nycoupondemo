package com.nineyi.event;

import android.os.Bundle;


public class LoginStickyEvent {

    private Bundle b;

    public void setParams(Bundle _b) {
        b = _b;
    }

    public Bundle getParams() {
        return b;
    }

    public LoginStickyEvent(Bundle _b) {
        b = _b;
    }

}
