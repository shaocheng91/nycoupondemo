package com.nineyi.module.base.retrofit;

import io.reactivex.subscribers.DisposableSubscriber;


/**
 * Created by tedliang on 2016/6/24.
 */
public abstract class NySubscriber<T> extends DisposableSubscriber<T> {


    @Override
    public void onComplete() {

    }

    @Override
    public void onError(Throwable throwable) {
        ApiErrorHandler.handleApiError(throwable);
    }
}
