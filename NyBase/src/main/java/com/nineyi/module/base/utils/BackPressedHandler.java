package com.nineyi.module.base.utils;

public interface BackPressedHandler {

    boolean onBackPressed();
}
