package com.nineyi.module.base.helper;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;


/**
 * Created by tedliang on 2016/10/4.
 */

public class NetworkHelper {
    public interface OnReceiveListener {
        void onReceive(Context context);
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public void regist(Activity activity, final OnReceiveListener onReceiveListener){
        if(mRegistrationBroadcastReceiver != null){
            activity.unregisterReceiver(mRegistrationBroadcastReceiver);
        }
        
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onReceiveListener.onReceive(context);

            }
        };
        activity.registerReceiver(mRegistrationBroadcastReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void unRegist(Activity activity){
        if(mRegistrationBroadcastReceiver != null) {
            activity.unregisterReceiver(mRegistrationBroadcastReceiver);
            mRegistrationBroadcastReceiver = null;
        }
    }
}
