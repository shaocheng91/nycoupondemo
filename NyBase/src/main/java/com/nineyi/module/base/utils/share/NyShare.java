package com.nineyi.module.base.utils.share;

import android.app.Activity;


public class NyShare {

    private ShareStrategy strategy;
    private Activity mActivity;

    public NyShare(ShareStrategy strategy, Activity activity) {
        this.strategy = strategy;
        this.mActivity = activity;
    }

    public void ShareIntent() {
        mActivity.startActivity(strategy.CreateIntent());
    }
}
