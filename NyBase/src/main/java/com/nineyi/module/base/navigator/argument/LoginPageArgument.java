package com.nineyi.module.base.navigator.argument;

import com.nineyi.autoargument.AutoArgument;

import android.os.Bundle;


/**
 * Created by tedliang on 2017/10/23.
 */

@AutoArgument
public class LoginPageArgument {
    String RealFragment;
    Bundle RealArgument;
    boolean IsPostSticky;
}
