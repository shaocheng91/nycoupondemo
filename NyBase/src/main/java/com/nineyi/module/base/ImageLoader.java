package com.nineyi.module.base;


import com.jakewharton.picasso.OkHttp3Downloader;
import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.crashlytics.NyCrashlyticsMessage;
import com.nineyi.data.AppBuildConfig;
import com.nineyi.module.base.okhttp.NyOkHttpClientFactory;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.HttpResponseCache;
import android.widget.ImageView;

import java.io.IOException;

import okhttp3.OkHttpClient;


/**
 * Created by tedliang on 15/12/30.
 */
public class ImageLoader {

    static volatile ImageLoader sInstance;
    private Downloader mDownloader;
    private LruCache mPicassoLruCache;
    private Picasso mPicasso;
    private int mPlaceHolderRes;
    private Context mContext;

    public ImageLoader(Context context, int placeHolderRes) {
        mContext = context;

        mPicassoLruCache = new LruCache(context);
        mDownloader = new OkHttp3Downloader(getHttpClient(ApplicationProvider.getInstance().getProviderAppBuildConfig()));
        mPicasso = new Picasso.Builder(context).memoryCache(mPicassoLruCache).downloader(mDownloader)
                .defaultBitmapConfig(Bitmap.Config.RGB_565).build();

        mPlaceHolderRes = placeHolderRes;
    }

    private OkHttpClient getHttpClient(AppBuildConfig appBuildConfig) {
        return NyOkHttpClientFactory.create(appBuildConfig);
    }

    public static void init(Context c, int placeHolderRes) {
        sInstance = new ImageLoader(c, placeHolderRes);
    }

    public static ImageLoader getInstance(Context context) {
        if (sInstance == null) {
            synchronized (ImageLoader.class) {
                if (sInstance == null) {
                    sInstance = new ImageLoader(context, 0);
                }
            }
        }

        return sInstance;
    }


    private RequestCreator createRequestCreator(String url ,int placeHolderRes){
        RequestCreator requestCreator = mPicasso.load(url).tag(mContext).placeholder(placeHolderRes).error(placeHolderRes);
        return requestCreator;
    }

    private RequestCreator createRequestCreatorTransform(String url) {
        RequestCreator requestCreator = mPicasso.load(url).tag(mContext).transform(new Transformation() {
            @Override
            public Bitmap transform(Bitmap source) {


                return null;
            }

            @Override
            public String key() {
                return null;
            }
        });
        return requestCreator;
    }

    public void displayImage(String url, ImageView imageView) {
        if(url != null && !"".equals(url)) {
            createRequestCreator(url, mPlaceHolderRes).into(imageView);
        } else {
            imageView.setImageResource(mPlaceHolderRes);
        }
    }

    public void displayImage(String url, ImageView imageView, int placeHolder) {
        if(url != null && !"".equals(url)) {
            createRequestCreator(url, placeHolder).into(imageView);
        } else {
            imageView.setImageResource(placeHolder);
        }
    }

    public void displayImage(String url, Target callback) {
        if(url != null && !"".equals(url)) {
            createRequestCreator(url, mPlaceHolderRes).into(callback);
        } else {
            callback.onBitmapFailed(mContext.getResources().getDrawable(mPlaceHolderRes));
        }
    }

    //推播專用，因有加確認問題Log，請勿混用
    public Bitmap loadImageSync(String url) {
        try {
            if(url != null && !"".equals(url)) {
                return createRequestCreator(url, mPlaceHolderRes).get();
            } else {
                return BitmapFactory.decodeResource(mContext.getResources(), mPlaceHolderRes);
            }
        } catch (IOException e) {
            NyCrashlyticsMessage.logLoadImageSyncFailed(url, e);
            e.printStackTrace();
        }

        //TODO: 須移除，僅用來確認GCM圖檔讀取失敗與OkHttp是否相關
        try {
            Picasso.with(mContext)
                    .load(url).get();
            NyCrashlyticsMessage.logPicassoLoadImageSuccess();
        } catch (IOException e) {
            e.printStackTrace();
            NyCrashlyticsMessage.logPicassoLoadImageFailed();
        }

        return null;
    }

    public Bitmap loadImageSync(String url, int widthPx, int heightPx) {
        try {
            if(url != null && !"".equals(url)) {
                return createRequestCreator(url, mPlaceHolderRes).resize(widthPx, heightPx).get();
            } else {
                return BitmapFactory.decodeResource(mContext.getResources(), mPlaceHolderRes);
            }
        } catch (IOException e) {
            NyCrashlyticsMessage.logLoadImageSyncFailed(url, e);
            e.printStackTrace();
        }

        return null;
    }


    public void clearCache() {
        clearMemoryCache();
        clearDiskCache();
    }

    public void clearMemoryCache(){
        mPicassoLruCache.clear();
    }

    public void clearDiskCache(){
        try {
            HttpResponseCache.getInstalled().delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelRequest(ImageView pic) {
        mPicasso.cancelRequest(pic);
    }


    public void resumeLoading() {
        mPicasso.resumeTag(mContext);
    }

    public void pasueLoading() {
        mPicasso.pauseTag(mContext);
    }

}
