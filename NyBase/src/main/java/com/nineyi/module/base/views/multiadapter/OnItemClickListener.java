package com.nineyi.module.base.views.multiadapter;

public interface OnItemClickListener<D> {
    void onItemClick(D item, int position);
}
