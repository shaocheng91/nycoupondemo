package com.nineyi.module.base.utils.share;

import android.content.Intent;


public class ShareString extends ShareStrategy {

    public ShareString(Object obj) {
        super(obj);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Intent CreateIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, (String) obj);
        sendIntent.setType("text/plain");
        return sendIntent;
    }

}
