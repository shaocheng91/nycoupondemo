package com.nineyi.module.base.provider.application;

import com.nineyi.data.AppBuildConfig;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;


/**
 * Created by ReeceCheng on 2017/8/11.
 */

public interface IApplicationProvider {
    String getProviderTag();
    Context getProviderAppContext();
    Resources getProviderAppResources();
    AppBuildConfig getProviderAppBuildConfig();
    Drawable getProviderAppLogo();
}
