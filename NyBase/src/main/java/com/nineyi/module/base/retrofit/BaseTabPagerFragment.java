package com.nineyi.module.base.retrofit;

import com.nineyi.module.base.retrofit.RetrofitActionBarFragment;
import com.nineyi.module.base.toolbartab.SlidingTabLayout;
import com.nineyi.module.base.ui.Elevation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


abstract public class BaseTabPagerFragment extends RetrofitActionBarFragment implements ViewPager.OnPageChangeListener {

    public abstract View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    private static final String TAG = BaseTabPagerFragment.class.getSimpleName();
    private static final String SAVED_STATE_SELECTED_NAVIGATION_ITEM = "savedStateSelectedNavigationItem";

    protected ViewPager mViewPager;
    private TabPagerActivity.TabsPagerAdapter mTabsPagerAdapter;
    protected SlidingTabLayout mSlidingTabLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTabsPagerAdapter = new TabPagerActivity.TabsPagerAdapter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return createView(inflater, container, savedInstanceState);
    }

    public void setCurrentItem(int item) {
        if (mViewPager == null) {
            Log.d(TAG, String.format("setCurrentItem %d. But ViewPager is null, ignore this operation", item));
            return;
        }
        mViewPager.setCurrentItem(item);
    }

    public int getCurrentItem() {
        if (mViewPager == null) {
            Log.d(TAG, "getCurrentItem. But ViewPager is null, ignore this operation");
            return 0;
        }
        return mViewPager.getCurrentItem();
    }

    public int getTabCount() {
        if (mViewPager == null) {
            Log.d(TAG, "getTabCount. But ViewPager is null, ignore this operation");
            return 0;
        }

        if (mViewPager.getAdapter() == null) {
            Log.d(TAG, "getTabCount. But Adapter of ViewPager is null, ignore this operation");
            return 0;
        }

        return mViewPager.getAdapter().getCount();
    }

    public CharSequence getTabText(int position) {
        if (mSlidingTabLayout == null) {
            Log.d(TAG, String.format("getTabText %d. But SlidingTabLayout is null, ignore this operation", position));
            return "";
        }
        return mSlidingTabLayout.getTabText(position);
    }

    public void setTabText(String title, int position) {
        if (mSlidingTabLayout == null) {
            Log.d(TAG,
                    String.format("setTabText %d as %s. But SlidingTabLayout is null, ignore this operation", position,
                            title));
            return;
        }
        mSlidingTabLayout.setTabText(title, position);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewPager.setAdapter(mTabsPagerAdapter);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnPageChangeListener(this);
        if (savedInstanceState != null && savedInstanceState.containsKey(SAVED_STATE_SELECTED_NAVIGATION_ITEM)) {
            mViewPager.setCurrentItem(savedInstanceState.getInt(SAVED_STATE_SELECTED_NAVIGATION_ITEM));
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mViewPager != null) {
            outState.putInt(SAVED_STATE_SELECTED_NAVIGATION_ITEM, mViewPager.getCurrentItem());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        mTabsPagerAdapter.removeAllTabs();
        mTabsPagerAdapter.notifyDataSetChanged();
        mViewPager = null;
        super.onDestroyView();
    }

    @Override
    protected Elevation provideActionBarElevation() {
        return Elevation.LevelZero;
    }

    protected void addTab(String title, Class<?> fragCls, Bundle args) {
        mTabsPagerAdapter.addTab(fragCls, args, title);

    }

    protected void notifyTabPageChanged() {
        mTabsPagerAdapter.notifyDataSetChanged();
        mSlidingTabLayout.setViewPager(mViewPager);

    }

    protected ArrayList<Fragment> getChildFragments(){
        ArrayList<Fragment> list = new ArrayList<>();
        for (int i = 0; i < getTabCount(); i++) {
            list.add(mTabsPagerAdapter.getItem(i));
        }

        return list;
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    public void onPageSelected(int position) {
    }

    public void onPageScrollStateChanged(int state) {
    }
}
