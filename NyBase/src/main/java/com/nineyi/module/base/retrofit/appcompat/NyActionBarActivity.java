package com.nineyi.module.base.retrofit.appcompat;

import com.nineyi.module.base.appcompat.ActionBarModeHandler;
import com.nineyi.module.base.appcompat.IActionBarFragment;
import com.nineyi.module.base.appcompat.NyBaseActionBarActivity;
import com.nineyi.module.base.provider.analytics.AnalyticsProvider;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;


/**
 * Base Activity for NineYi Shopping. This class provided basic ActionBar control like
 * {@link #setActionBarTitle(String)}, {@link #setActionBarTitle(int)}, {@link #setActionBarWithCustomView(View)},
 * {@link #setActionBarNaviLogo()} and {@link #setHomeAsUp(Toolbar)} ()}
 */
public abstract class NyActionBarActivity extends NyBaseActionBarActivity implements IActionBarFragment {

    ActionBarModeHandler mMode = new ActionBarModeHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMode.onCreate();
    }

    @Override
    public void setActionBarTitle(@NonNull String title) {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mMode.setActionBarTitle(title, this);
    }

    @Override
    public void setActionBarTitle(@StringRes int stringRes) {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mMode.setActionBarTitle(getString(stringRes), this);
    }

    @Override
    public void setActionBarWithCustomView(View view) {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mMode.setCustomViewToActionBar(view, this);
    }

    @Override
    public void setActionBarNaviLogo() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mMode.setNaviLogo(this);
    }

    protected void setHomeAsUp(Toolbar toolbar) {
        ActionBar actionBar = getSupportActionBar();

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        mMode.setMode(ActionBarModeHandler.ActionBarMode.ShowAsBack);
        // hack:
        // previous Activity would be destroyed if previous Activity is not set as 'singleTop' while 'Back' ActionButton is clicked
        // see http://blog.csdn.net/mengweiqi33/article/details/41285699 for detail
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NyActionBarActivity.this.onBackPressed();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        AnalyticsProvider.getInstance().clearScreenName();
    }

    public void setScreenNameAndView(String s) {
        AnalyticsProvider.getInstance().sendScreenView(s);
    }

}
