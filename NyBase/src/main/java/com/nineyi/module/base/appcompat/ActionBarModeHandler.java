package com.nineyi.module.base.appcompat;

import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.ui.NineYiColor;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;

import con.nineyi.module.base.R;


public class ActionBarModeHandler {

    public enum ActionBarMode {
        ShowAsHome,
        ShowAsBack,
        KeepOriginal
    }

    private ActionBarMode mMode;

    // start of class logic
    private static final String TAG = ActionBarModeHandler.class.getSimpleName();
    private static final String BUNDLE_ACTIONBAR_MODE = TAG + "com.nineyi.retrofit.appcompat.actionbar.mode";
    private static final ActionBarMode DEFAULT_MODE = ActionBarMode.KeepOriginal;

    private String mFragmentName;

    public ActionBarModeHandler() {
    }

    public ActionBarModeHandler(ActionBarMode mode) {
        mMode = mode;
    }

    /**
     * Display Options
     */
    public int attachOptions(int options) {
        switch (mMode) {
            case ShowAsHome:
                options |= ActionBar.DISPLAY_SHOW_HOME;
                break;
            case ShowAsBack:
                options |= ActionBar.DISPLAY_HOME_AS_UP;
                break;
            case KeepOriginal:
            default:
                break;
        }
        return options;
    }

    private int attachOptionsWithShowCustom() {
        int options = ActionBar.DISPLAY_SHOW_CUSTOM;
        switch (mMode) {
            case ShowAsHome:
                options |= ActionBar.DISPLAY_SHOW_HOME;
                break;
            case ShowAsBack:
                options |= ActionBar.DISPLAY_HOME_AS_UP;
                break;
            case KeepOriginal:
            default:
                break;
        }
        return options;
    }

    /**
     * Parcelable processing
     */
    public final void fromSerializable(Serializable serializable) {
        ActionBarMode mode;
        if (serializable == null) {
            mode = ActionBarMode.KeepOriginal;
        } else {
            try {
                mode = (ActionBarMode) serializable;
                setMode(mode);
                return;
            } catch (ClassCastException classCastException) {
                Log.e(TAG, "cannot cast to ActionBarMode, please check input Serializable");
                mode = DEFAULT_MODE;
            }
        }
        setMode(mode);
    }

    public void setMode(ActionBarMode mode) {
        mMode = mode;
    }

    public static final void addShowAsBackActionBarMode(Bundle arguments) throws NullPointerException {
        if (arguments == null) {
            throw new NullPointerException("argument is NULL");
        }

        arguments.putSerializable(BUNDLE_ACTIONBAR_MODE, ActionBarMode.ShowAsBack);
    }

    public static final void addShowAsHomeActionBarMode(Bundle arguments) throws NullPointerException {
        if (arguments == null) {
            throw new NullPointerException("argument is NULL");
        }

        arguments.putSerializable(BUNDLE_ACTIONBAR_MODE, ActionBarMode.ShowAsHome);
    }

    public static void addShowAsBackActionBarMode(Fragment targetFragment) throws NullPointerException {
        Bundle arguments = targetFragment.getArguments();
        if (arguments == null) {
            arguments = new Bundle();
            targetFragment.setArguments(arguments);
        }

        arguments.putSerializable(BUNDLE_ACTIONBAR_MODE, ActionBarMode.ShowAsBack);
    }

    public static void addShowAsHomeActionBarMode(Fragment targetFragment) throws NullPointerException {
        Bundle arguments = targetFragment.getArguments();
        if (arguments == null) {
            arguments = new Bundle();
            targetFragment.setArguments(arguments);
        }

        arguments.putSerializable(BUNDLE_ACTIONBAR_MODE, ActionBarMode.ShowAsHome);
    }

    /**
     * lifecycle-specific functions
     */
    public void onCreate(Bundle arguments, Fragment hostFragment) {
        String fragmentName = hostFragment.getClass().getName();
        ActionBarMode mode = ActionBarMode.KeepOriginal;
        if (hostFragment.getParentFragment() != null) {
            Log.e(TAG, "Fragment " + fragmentName + " is children fragment. ActionBar setting would be ignore.");
            mode = DEFAULT_MODE;
            setMode(mode);
        } else if (arguments == null || !arguments.containsKey(BUNDLE_ACTIONBAR_MODE)) {
            Log.e(TAG, "Cannot find ActionBarMode parameter in Fragment " + fragmentName
                    + " . Please use addActionBarMode add necessary Mode");
            if (ApplicationProvider.getInstance().getProviderAppBuildConfig().isDebug) {
                // xxx:
                // we should throw Exception in Debug
                //throw new RuntimeException(
                //        "Cannot find ActionBarMode parameter. Please use addActionBarMode add necessary Mode");
                mode = DEFAULT_MODE;
            } else {
                mode = DEFAULT_MODE;
            }
            setMode(mode);
        } else {
            fromSerializable(arguments.getSerializable(BUNDLE_ACTIONBAR_MODE));
            Log.e(TAG, "ActionBarMode of Fragment " + fragmentName + " is set as " + mode.name());
        }

        mFragmentName = fragmentName;
    }

    public void onCreate() {
        setMode(ActionBarMode.ShowAsHome);
    }

    public void onResume(Fragment hostFragment, AppCompatActivity hostActivity) {
        // if this fragment belong to another Fragment. Dont change ActionBar. i.e. ViewPager
        if (hostFragment.getParentFragment() != null) {
            Log.e(TAG, "fragment " + mFragmentName + " is children fragment skip onResume operation");
            return;
        }
        if (hostActivity instanceof IDrawerActivity) {
            int opt = hostActivity.getSupportActionBar().getDisplayOptions() | ActionBar.DISPLAY_SHOW_CUSTOM;
            hostActivity.getSupportActionBar().setDisplayOptions(opt);
            if (mMode == ActionBarMode.ShowAsBack) {
                ((IDrawerActivity) hostActivity).setDrawerAsUp();
            } else if (mMode == ActionBarMode.ShowAsHome) {
                ((IDrawerActivity) hostActivity).setDrawerAsHamburger();
            }
        } else {
            if (mMode != ActionBarMode.KeepOriginal) {
                int opt = attachOptionsWithShowCustom();
                hostActivity.getSupportActionBar().setDisplayOptions(opt);
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item, AppCompatActivity hostActivity) {
        if (item.getItemId() == android.R.id.home && mMode == ActionBarMode.ShowAsBack) {
            hostActivity.onBackPressed();
            return true;
        } else {
            return false;
        }
    }

    /**
     * ActionBar UI setting
     */
    public void setActionBarTitle(@NonNull String title, AppCompatActivity hostActivity) {
        if ( hostActivity == null || hostActivity.getSupportActionBar() == null ) {
            return;
        }
        TextView titleTextView = new TextView(hostActivity);
        titleTextView.setText(title);
        titleTextView.setSingleLine(true);
        titleTextView.setEllipsize(TextUtils.TruncateAt.END);
        titleTextView.setTextAppearance(hostActivity, R.style.MyTitleText);
        titleTextView.setTextColor(NineYiColor.getGlobalHeaderLinkFontColor());

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.LEFT);

        hostActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        hostActivity.getSupportActionBar().setCustomView(titleTextView, params);
    }

    public void setCustomViewToActionBar(View view, AppCompatActivity hostActivity) {
        if ( hostActivity == null || hostActivity.getSupportActionBar() == null ) {
            return;
        }
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.LEFT);

        ActionBar actionBar = hostActivity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(view, params);
    }

    public void setNaviLogo(AppCompatActivity hostActivity) {
        if (hostActivity == null) {
            return;
        }
        ImageView mallLogo = new ImageView(hostActivity);
        mallLogo.setImageDrawable(ApplicationProvider.getInstance().getProviderAppLogo());

        setCustomViewToActionBar(mallLogo, hostActivity);
    }

    public interface IDrawerActivity {

        void setDrawerAsUp();

        void setDrawerAsHamburger();
    }
}
