package com.nineyi.module.base.retrofit;

import com.nineyi.module.base.toolbartab.SlidingTabLayout;
import com.nineyi.module.base.ui.Elevation;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import con.nineyi.module.base.R;


abstract public class TabPagerFragment extends BaseTabPagerFragment {

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_pager, container, false);

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.slidingtab_tab);

        Elevation.elevate(mSlidingTabLayout, Elevation.LevelOne);
        return view;
    }
}
