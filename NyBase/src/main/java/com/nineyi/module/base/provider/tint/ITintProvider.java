package com.nineyi.module.base.provider.tint;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/8/22.
 */

public interface ITintProvider {
    void setBackGroundTint(View v, int pressedColor, int normalColor);
    void setToolbarTint(Toolbar toolbar);
    Drawable getTintDrawable(Drawable d, int pressColor, int normalColor);
}
