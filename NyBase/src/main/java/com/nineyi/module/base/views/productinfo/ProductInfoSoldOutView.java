package com.nineyi.module.base.views.productinfo;

import com.nineyi.module.base.ui.UiUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import con.nineyi.module.base.R;


/**
 * Created by kelsey on 2017/3/21.
 */

public class ProductInfoSoldOutView extends TextView {
    private static final int DEFAULT_TEXT_SIZE_SP = 13;

    public ProductInfoSoldOutView(Context context) {
        this(context, null);
    }

    public ProductInfoSoldOutView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductInfoSoldOutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        parseAttrs(attrs);
    }

    public void setSoldOutVisibility(IProductInfoSoldOut data) {
        if (data.isSoldOut()) {
            this.setVisibility(View.VISIBLE);
        } else {
            this.setVisibility(View.GONE);
        }
    }

    private void init() {
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.setLayoutParams(params);
        this.setText(R.string.salepage_sale_out);
        this.setTextColor(Color.WHITE);
        this.setBackgroundColor(Color.parseColor("#99000000"));
        this.setGravity(Gravity.CENTER);
        this.setVisibility(View.GONE);
    }

    private void parseAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NyProductInfoSoldOut);

        UiUtils.setTextSize(typedArray, this, R.styleable.NyProductInfoSoldOut_npisoTextSize, ProductInfoSoldOutView.DEFAULT_TEXT_SIZE_SP);

        if (typedArray != null) {
            typedArray.recycle();
        }
    }
}
