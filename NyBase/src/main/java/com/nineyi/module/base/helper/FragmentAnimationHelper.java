package com.nineyi.module.base.helper;

import java.util.HashMap;


public class FragmentAnimationHelper {

    public enum FragmentMove {
        FromBottomToTop,
        FromTopToBottom,
        FromLeftToRight,
        FromRightToLeft,
        None

    }

    public static String ANIMATION_ENTER = "enter";
    public static String ANIMATION_LEAVE = "leave";
    public static String ANIMATION_ENTER_POP = "enter_pop";
    public static String ANIMATION_LEAVE_POP = "leave_pop";

    public HashMap<String, Integer> createAnimation(Integer arg0, Integer arg1, Integer arg2, Integer arg3) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put(ANIMATION_ENTER, arg0);
        map.put(ANIMATION_LEAVE, arg1);
        map.put(ANIMATION_ENTER_POP, arg2);
        map.put(ANIMATION_LEAVE_POP, arg3);
        return map;
    }
}
