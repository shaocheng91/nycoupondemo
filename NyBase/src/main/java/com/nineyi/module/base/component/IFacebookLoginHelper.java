package com.nineyi.module.base.component;

import com.facebook.Profile;

import android.app.Activity;
import android.content.Intent;


/**
 * Created by tedliang on 2017/10/18.
 */
public interface IFacebookLoginHelper {

    boolean hasToken();

    String getAccessToken();

    void stopTracking();

    void doLogin(Activity loginMainActivity);

    int getLoginRequestCode();

    void onActivityResult(int requestCode, int resultCode, Intent data);

    interface LoginCallback {

        void onSuccess();

        void onCancel();

        void onProfileChanged();

        void onError();
    }
    Profile getProfile();
    void setLoginCallback(IFacebookLoginHelper.LoginCallback loginCallback);


}
