package com.nineyi.module.base.menu;


import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;

import con.nineyi.module.base.R;


/**
 * Created by ted on 15/3/9.
 */
public class MenuHelper implements IMenuHelper {

    private MenuItem mShoppingCartItem;
    private MenuItem mSearchItem;

    public MenuHelper(Menu menu) {
        mShoppingCartItem = menu.findItem(R.id.action_cart);
        mSearchItem = menu.findItem(R.id.action_search);
    }

    /*
    * 此method請由activity執行否則會出問題
    * */
    @Override
    public void refreshCount() {
        if (mShoppingCartItem != null) {
            ActionProvider actionProvider = MenuItemCompat.getActionProvider(mShoppingCartItem);

            if(actionProvider instanceof IShoppingCartActionProvider) {
                ((IShoppingCartActionProvider) actionProvider).refreshCartCount();
            }
        }
    }

    @Override
    public void intMenu(boolean isShoppingcartVisible, boolean isSearchVisible) {
        if (mShoppingCartItem != null) {
            mShoppingCartItem.setVisible(isShoppingcartVisible);
        }
        if (mSearchItem != null) {
            mSearchItem.setVisible(isSearchVisible);
        }
    }
}
