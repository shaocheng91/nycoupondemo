package com.nineyi.module.base.config;


import com.nineyi.module.base.provider.application.ApplicationProvider;

import con.nineyi.module.base.R;


public class NineYiConstant {

    private NineYiConstant() {
        // No public constructor
    }

    public static final String FILE_EXT_PNG = ".png";
    private static final int TIME_FORMAT = R.string.date_format_yyyy_mm_dd_aa_hh_mm;
    private static final int TIME_FORMAT_24HOUR = R.string.date_format_yyyy_mm_dd_hh_mm_ss_1;
    public static final String ENCODING_UTF_8 = "UTF-8";

    // for 瀏覽紀錄
    public static final String PREF_KEY_BROWSE_RECORD = "com.nineyi.browse.record";

    public static final String BUNDLE_KEY_LOAD_HOME_BTN = "com.nineyi.extra.loadHomeBtn";

    // for 本地推播
    public static final String BUNDLE_KEY_LBS_NAME = "com.nineyi.o2o.lbsId";
    public static final String BUNDLE_KEY_LOCATION_NAME = "com.nineyi.o2o.locationId";

    public static final String BUNDLE_KEY_COUPON_ID = "com.nineyi.coupon.couponId";

    public static final String PREF_KEY_GCM_TOKEN = "com.nineyi.gcm.token";
    public static final String PREF_KEY_SHOP_ONLINE_CRM_CODE = "com.nineyi.shop.onlineCRMCode";

    public static final String MALL_APP_PACKAGE_NAME = "com.nineyi.mall";

    public static final String SIDEBAR_USER = "com.nineyi.sidebar.user";

    public static final double DEFAULT_O2O_MAP_CENTER_LATITUDE = 25.038141;
    public static final double DEFAULT_O2O_MAP_CENTER_LONGITUDE = 121.550246;

    public static final int COUNT_DOWN_TIME_SECOND = 600;
    
    public static final String APP_SECRETE = "07cf154385bc79c6dd7d79b315be2400";//"07cf154385bc79c6dd7d79b315be2400";
    public static final String APP_ID = "164329253731720";//"164329253731720";

    private static String getString(int resId) {
        return ApplicationProvider.getInstance().getProviderAppResources().getString(resId);
    }

    public static String getTimeFormat() {
        return getString(TIME_FORMAT);
    }

    public static String getTimeFormat24hour() {
        return getString(TIME_FORMAT_24HOUR);
    }
}