package com.nineyi.module.base.views.productinfo;

import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.base.utils.StringUtils;
import com.nineyi.module.base.views.FavoritePopBox;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import con.nineyi.module.base.R;


/**
 * Created by kelsey on 2017/3/20.
 */

public class ProductInfoTitleAndPriceLayout extends RelativeLayout {
    private static final int DEFAULT_TITLE_TEXT_COLOR = 0xff4a4e5c;
    private static final int DEFAULT_TITLE_TEXT_SIZE_SP = 13;
    private static final int DEFAULT_TITLE_MAX_LINES = 2;
    private static final int DEFAULT_PRICE_TEXT_SIZE_SP = 15;
    private static final int DEFAULT_SUGGEST_PRICE_TEXT_COLOR = 0xff999999;
    private static final int DEFAULT_SUGGEST_PRICE_TEXT_SIZE_SP = 13;

    private RelativeLayout mLayout;
    private TextView mTitle, mPrice, mSuggestPrice;
    private FavoritePopBox mFavoriteBtn;
    private int mPriceTextColor;

    public ProductInfoTitleAndPriceLayout(Context context) {
        this(context, null);
    }

    public ProductInfoTitleAndPriceLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductInfoTitleAndPriceLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        parseAttrs(attrs);
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.product_info_title_and_price_layout, this, true);
        mLayout = (RelativeLayout) view.findViewById(R.id.product_info_text_layout);
        mTitle = (TextView) view.findViewById(R.id.product_info_title);
        mPrice = (TextView) view.findViewById(R.id.product_info_price);
        mSuggestPrice = (TextView) view.findViewById(R.id.product_info_suggest_price);
        mSuggestPrice.setPaintFlags(mSuggestPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        mFavoriteBtn = (FavoritePopBox) view.findViewById(R.id.product_info_fav_chkbox);
        mFavoriteBtn.setMode(FavoritePopBox.FAVORITE_MODE_SALEPAGE);
    }

    private void parseAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NyProductInfo);

        setLayoutPadding(typedArray.getDimensionPixelSize(R.styleable.NyProductInfo_npiLayoutRightAndLeftPadding,
                (int)getContext().getResources().getDimension(R.dimen.small_margin_right)));

        mTitle.setTextColor(typedArray.getColor(R.styleable.NyProductInfo_npiTitleColor,
                ProductInfoTitleAndPriceLayout.DEFAULT_TITLE_TEXT_COLOR));

        UiUtils.setTextSize(typedArray, mTitle, R.styleable.NyProductInfo_npiTitleTextSize, DEFAULT_TITLE_TEXT_SIZE_SP);

        setTitleMaxLines(typedArray.getInt(R.styleable.NyProductInfo_npiTitleMaxLines,
                ProductInfoTitleAndPriceLayout.DEFAULT_TITLE_MAX_LINES));

        mPriceTextColor = typedArray.getColor(R.styleable.NyProductInfo_npiPriceColor, getResources().getColor(R.color.font_price));
        mPrice.setTextColor(mPriceTextColor);
        UiUtils.setTextSize(typedArray, mPrice, R.styleable.NyProductInfo_npiPriceTextSize, DEFAULT_PRICE_TEXT_SIZE_SP);

        mSuggestPrice.setTextColor(typedArray.getColor(R.styleable.NyProductInfo_npiSuggestPriceColor,
                ProductInfoTitleAndPriceLayout.DEFAULT_SUGGEST_PRICE_TEXT_COLOR));
        UiUtils.setTextSize(typedArray, mSuggestPrice, R.styleable.NyProductInfo_npiSuggestPriceTextSize, DEFAULT_SUGGEST_PRICE_TEXT_SIZE_SP);

        mFavoriteBtn.setFavoritePopBoxColor(typedArray.getColor(R.styleable.NyProductInfo_npiFavColor,
                ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_fav_selected)),
                ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_fav));

        if (typedArray != null) {
            typedArray.recycle();
        }
    }

    private void setTitleMaxLines(int lines) {
        if (lines == 1) {
            mTitle.setSingleLine();
        } else {
            mTitle.setMaxLines(lines);
        }
    }

    private void setLayoutPadding(int padding) {
        mLayout.setPadding(padding, 0, padding, 0);
    }

    public void setData(@NonNull IProductInfoTextData data) {
        setTitle(data.getTitle());
        setPrice(data.getPrice());
        setSuggestPrice(data.getSuggestPrice());
        setFavCheckedWithId(data.getSalePageId());
    }

    public void setPriceColorBySoldOut(@NonNull IProductInfoSoldOut data) {
        if (mPrice != null) {
            if (data.isSoldOut()) {
                mPrice.setTextColor(Color.parseColor("#999999"));
            } else {
                mPrice.setTextColor(mPriceTextColor);
            }
        }
    }

    private void setTitle(String title) {
        if (mTitle != null) {
            mTitle.setText(title);
        }
    }

    private void setPrice(Double price) {
        if (mPrice != null) {
            String priceStr = getContext().getString(R.string.dollar_sign) +
                    StringUtils.truncatePrice(String.format(getContext().getString(R.string.price_format), price), 6);
            mPrice.setText(priceStr);
        }
    }

    private void setSuggestPrice(Double suggestPrice) {
        if (mSuggestPrice != null) {
            mSuggestPrice.setText(getContext().getString(R.string.price_format_with_dollar_sign, suggestPrice));
        }
    }

    private void setFavCheckedWithId(int salePageId) {
        if (mFavoriteBtn != null) {
            mFavoriteBtn.setCheckedWithId(salePageId, false);
        }
    }
}
