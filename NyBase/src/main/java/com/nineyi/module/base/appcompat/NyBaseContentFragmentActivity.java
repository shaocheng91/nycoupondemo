package com.nineyi.module.base.appcompat;

import com.nineyi.module.base.FinishHandler;
import com.nineyi.module.base.events.ShoppingCartCountEvent;
import com.nineyi.module.base.menu.BasicMenuInflater;
import com.nineyi.module.base.menu.MenuHelper;
import com.nineyi.module.base.navigator.FragmentNavigator;
import com.nineyi.module.base.ui.BaseTintUtils;
import com.nineyi.module.base.ui.NineYiColor;
import com.nineyi.module.base.utils.BackPressedHandler;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import con.nineyi.module.base.R;
import de.greenrobot.event.EventBus;


/**
 * Created by ReeceCheng on 2017/8/31.
 */

public abstract class NyBaseContentFragmentActivity extends NyBaseActionBarActivity {
    public abstract Fragment onCreateContentFragment();

    private MenuHelper mNineYiMenuHelper;
    private MenuItem mMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // This has to be called before setContentView and you must use the
        // class in com.actionbarsherlock.view and NOT android.view
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_holder);

        Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(
                BaseTintUtils.getTintDrawable(getResources().getDrawable(R.drawable.btn_navi_back),
                        NineYiColor.getGlobalNaviIconColor(),
                        NineYiColor.getGlobalNaviIconColor()));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NyBaseContentFragmentActivity.this.onBackPressed();
            }
        });

        Fragment fragment = onCreateContentFragment();

        if(fragment != null) {
            FragmentNavigator.build().
                    fragment(fragment).
                    replace(R.id.content_frame).
                    customAnimations(R.anim.enter_right, R.anim.leave_left, R.anim.enter_left, R.anim.leave_right)
                    .actionbarHomeAsUp()
                    .doNavigation(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mNineYiMenuHelper != null) {
            mNineYiMenuHelper.refreshCount();
        }
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        searchViewCollapse();
    }

    private void searchViewCollapse() {
        if (mMenuItem != null && mMenuItem.expandActionView()) {
            mMenuItem.collapseActionView();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (fragment != null && fragment instanceof BackPressedHandler) {
            if (((BackPressedHandler) fragment).onBackPressed()) {
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        BasicMenuInflater.inflateBasicMenu(this, menu);
        mNineYiMenuHelper = new MenuHelper(menu);
        mMenuItem = menu.findItem(R.id.action_search);
        return true;
    }



    public void onEventMainThread(ShoppingCartCountEvent event) {
        if(mNineYiMenuHelper != null) {
            mNineYiMenuHelper.refreshCount();
        }
    }

    @Override
    public void finish() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (fragment != null && fragment instanceof FinishHandler) {
            ((FinishHandler) fragment).finish();
        }
        super.finish();
    }
}
