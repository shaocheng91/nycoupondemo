package com.nineyi.module.base.component;

import com.nineyi.data.model.layout.LayoutTemplateData;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;


/**
 * Created by tedliang on 2017/10/18.
 */
public interface INavigateManager {

    void navigateToWebViewControls(Activity activity, String webLoginInfoUrl);
    void navigateToLayoutTemplateTarget(Activity activity, int shopId, LayoutTemplateData data);

    //ActivityNavUtils.navigateToWebview(fragment.getActivity(), ((WebViewWithControlsFragment) fr).getClass(), args);
    void navigateToWebview(FragmentActivity activity, Fragment fr, Bundle args);

    //ActivityNavigator.build().target((Class<Activity>) cfr).doNavigation(fragment.getActivity());
    void navigateShoppingCart(FragmentActivity activity);

    //RedirectActivityNavigator.buildRedirectNavigator(fr.getClass()).extras(args).doNavigation(fragment.getActivity());
    void navigateActivity(FragmentActivity activity, Class<? extends Fragment> aClass, Bundle args);

    // ActivityNavUtils.navigatePrivacyFragment(LoginMainActivity.this);
    void navigatePrivacyFragment(FragmentActivity activity);

    //ActivityNavUtils.navigateToVipMemberBenefits(LoginMainActivity.this, memberRightHtml);
    void navigateToVipMemberBenefits(FragmentActivity activity, String memberRightHtml);

    //ActivityNavUtils.navigateToLoginWebViewControls(LoginMainActivity.this, url);
    void navigateToLoginWebViewControls(FragmentActivity activity, String url);
}
