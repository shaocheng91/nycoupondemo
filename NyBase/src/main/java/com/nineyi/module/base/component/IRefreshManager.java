package com.nineyi.module.base.component;

/**
 * Created by tedliang on 2017/10/19.
 */
public interface IRefreshManager {
    void refreshCookie();
}
