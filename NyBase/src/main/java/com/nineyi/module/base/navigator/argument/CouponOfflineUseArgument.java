package com.nineyi.module.base.navigator.argument;

import com.nineyi.autoargument.AutoArgument;


/**
 * Created by ReeceCheng on 2017/11/6.
 */

@AutoArgument
public class CouponOfflineUseArgument {
    int CouponId;
    int CouponSlaveId;
}
