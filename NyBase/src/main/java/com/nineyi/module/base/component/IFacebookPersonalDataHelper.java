package com.nineyi.module.base.component;

/**
 * Created by tedliang on 2017/10/18.
 */
public interface IFacebookPersonalDataHelper {

    void setId(String id);

    void setName(String name);
}
