package com.nineyi.module.base.appcompat.scroll;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import con.nineyi.module.base.R;


/**
 * Created by ReeceCheng on 2017/1/10.
 */
public class ShopHomeScrollingUtils {

    public static int getScrollY(RecyclerView view, int headerSpace) {
        View c = view.getChildAt(0);

        if (c == null) {
            return 0;
        }

        int firstVisiblePosition = 0;

        if (view.getLayoutManager() instanceof GridLayoutManager) {
            firstVisiblePosition = ((GridLayoutManager) view.getLayoutManager()).findFirstVisibleItemPosition();
        } else if (view.getLayoutManager() instanceof LinearLayoutManager) {
            firstVisiblePosition = ((LinearLayoutManager) view.getLayoutManager()).findFirstVisibleItemPosition();
        }


        int top = c.getTop() - headerSpace;

        int headerHeight;

        // 因為是在RecyclerView上方加上Decoration，所以在算scroll y時，
        // 如果現在畫面上的第一個item是position 1或以上，代表現在RecyclerView已經被往上滑，
        // 所以要加上Header的高度
        if (firstVisiblePosition >= 1) {
            headerHeight = view.getResources().getDimensionPixelSize(R.dimen.header_height);
        } else {
            headerHeight = 0 ;
        }

        return -top + (firstVisiblePosition * c.getHeight()) + headerHeight;
    }

}
