package com.nineyi.module.base.location;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.config.NineYiConstant;
import com.nineyi.module.base.utils.NyGooglePlayServiceUtils;
import com.nineyi.module.base.utils.TimeUtils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.PermissionChecker;
import android.util.Log;

import java.util.List;


/**
 * When use this class in Activity or Fragment, please dependency with lifecycle,
 * remove location request with {@link #stopLocationUpdates} when onLocationChange call.
 *
 * {@link #onStart(OnLocationStatusListener listener)} and {@link #onStop()} use to connect and disconnect with google play service,
 * {@link #onResume()} and {@link #onPause()} use to start and stop location update request.
 *
 * ---
 *
 * If use in other place, just call {@link #doConnect()} and {@link #doDisconnect()}
 *
 * ---
 *
 */
public class NyLocationManager
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    public interface OnLocationStatusListener {
        void onConnected(Bundle result);
        void onConnectionFailed(ConnectionResult result);
        void onLocationChanged(Location location);
    }

    private static final long FOREGROUND_UPDATE_INTERVAL = TimeUtils.convertSecondToMilliseconds(10);
    private GoogleApiClient mGoogleApiClient;
    private Location mLastUpdateLocation;
    private boolean mHasPermission = false;
    private OnLocationStatusListener mOnLocationStatusListener;

    private static volatile NyLocationManager sInstance;

    public static NyLocationManager getInstance() {
        if (sInstance == null) {
            synchronized (NyLocationManager.class) {
                if (sInstance == null) {
                    sInstance = new NyLocationManager();
                }
            }
        }
        return sInstance;
    }

    private NyLocationManager() {
        mHasPermission = hasPermission(ApplicationProvider.getInstance().getProviderAppContext());
        setupGoogleApiClient();
    }

    private void setupGoogleApiClient() {
        Context context = ApplicationProvider.getInstance().getProviderAppContext();

        if (NyGooglePlayServiceUtils.isServiceAvailable(context)) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }

    private boolean hasPermission(Context context) {
        return PermissionChecker.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isGetLocationAvailable() {
        LocationManager manager = (LocationManager) ApplicationProvider.getInstance().getProviderAppContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> list = manager.getProviders(true);
        for (String provider : list) {
            if (provider.indexOf(LocationManager.GPS_PROVIDER) >= 0
                    || provider.indexOf(LocationManager.NETWORK_PROVIDER) >= 0) {
                return true;
            }
        }

        return false;
    }

    public Location getLastUpdateLocation() {
        if (mLastUpdateLocation == null) {
            return createDefaultLocation();
        }
        return mLastUpdateLocation;
    }

    public boolean hasUpdateLocation() {
        return mLastUpdateLocation != null;
    }

    public Location createDefaultLocation() {
        Location loc = new Location(LocationManager.GPS_PROVIDER);
        loc.setLatitude(NineYiConstant.DEFAULT_O2O_MAP_CENTER_LATITUDE);
        loc.setLongitude(NineYiConstant.DEFAULT_O2O_MAP_CENTER_LONGITUDE);
        return loc;
    }

    public boolean isConnect() {
        if (mGoogleApiClient != null) {
            return mGoogleApiClient.isConnected();
        }

        return false;
    }
    
    public void doConnect() {
        if (mHasPermission) {
            if (isConnect()) {
                if(mOnLocationStatusListener != null) {
                    mOnLocationStatusListener.onConnected(new Bundle());
                }
            } else {
                doConnectInternal();
            }
        } else {
            Log.e("NyLocationManager",
                    "no permission \"android.permission.ACCESS_FINE_LOCATION\" \"android.permission.ACCESS_COARSE_LOCATION");
        }
    }

    private void doConnectInternal() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }
    
    public void doDisconnect() {
        if (mGoogleApiClient != null && isConnect()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        if (mOnLocationStatusListener != null) {
            mOnLocationStatusListener.onConnectionFailed(arg0);
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        startLocationUpdates();

        if (mOnLocationStatusListener != null) {
            mOnLocationStatusListener.onConnected(arg0);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        // do nothing.
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastUpdateLocation = location;

        if (mOnLocationStatusListener != null) {
            mOnLocationStatusListener.onLocationChanged(location);
        }
    }

    public void startLocationUpdates() {
        if(mGoogleApiClient != null && isConnect()) {
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, setupLocationRequest(), this);
        }
    }

    private LocationRequest setupLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        //PRIORITY_BALANCED_POWER_ACCURACY 若位置的mod為GPS 則不會更新
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(FOREGROUND_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FOREGROUND_UPDATE_INTERVAL);

        return locationRequest;
    }

    public void stopLocationUpdates() {
        if(mGoogleApiClient != null && isConnect()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public void onResume() {
        startLocationUpdates();
    }

    public void onPause() {
        stopLocationUpdates();
    }

    public void onStart(OnLocationStatusListener status) {
        mOnLocationStatusListener = status;
        doConnect();
    }

    public void onStop() {
        doDisconnect();
        mOnLocationStatusListener = null;
    }

}