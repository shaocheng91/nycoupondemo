package com.nineyi.module.base.ui.countdown;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by ReeceCheng on 2017/10/12.
 */

public class CountdownManager implements CountdownSubject {

    private boolean isCountdownRunning;

    private Timer mTimer;
    private ArrayList<WeakReference<CountdownObserver>> mCountdownObservers;

    public CountdownManager() {
        mCountdownObservers = new ArrayList<>();
        isCountdownRunning = false;
    }

    private void startCountdown() {
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                sendCountdownHandler.sendEmptyMessage(1);
            }
        }, 0, 1000);

        isCountdownRunning = true;
    }

    private Handler sendCountdownHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if(msg.what == 1) {
                notifyCountdown();
            }

            return false;
        }
    });

    private void notifyCountdown() {
        Date curDate = new Date(System.currentTimeMillis());
        long time = curDate.getTime();

        for (int i=0; i<mCountdownObservers.size(); i++) {
            CountdownObserver o = mCountdownObservers.get(i).get();

            if(o != null) {
                o.countdown(time);
            }
        }
    }

    private void stopCountdown() {
        if(mTimer != null) {
            mTimer.cancel();
        }

        isCountdownRunning = false;
    }

    @Override
    public void register(WeakReference<CountdownObserver> o) {
        if(mCountdownObservers == null) {
            mCountdownObservers = new ArrayList<>();
        }

        if(!mCountdownObservers.contains(o)) {
            mCountdownObservers.add(o);

            if(!mCountdownObservers.isEmpty() && !isCountdownRunning) {
                startCountdown();
            }
        } else {
            Log.e(CountdownManager.class.toString(), "CountdownObserver: " + o.toString() + " ALREADY REGISTER!!");
        }

    }

    @Override
    public void unregister(WeakReference<CountdownObserver> o) {
        if(mCountdownObservers != null && mCountdownObservers.contains(o)) {
            mCountdownObservers.remove(o);

            if(mCountdownObservers.isEmpty() && isCountdownRunning){
                stopCountdown();
            }
        }
    }

    public void clear() {
        mCountdownObservers.clear();
        stopCountdown();
    }

}
