package com.nineyi.module.base.views;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


/**
 * Created by toby on 2015/1/19.
 */
public class NewLoadMoreScrollListener extends RecyclerView.OnScrollListener {

    public interface Loadable {

        void loadMore();
    }

    protected static final int ITEM_LEFT_TO_LOAD_MORE = 10;

    protected final Loadable mLoadable;
    protected RecyclerView.OnScrollListener mExternalOnScrollListener;

    protected boolean mIsLoading;

    protected int mPreviousTotal;


    public NewLoadMoreScrollListener(Loadable loadable) {
        this(loadable, null);
    }

    public NewLoadMoreScrollListener(Loadable loadable, RecyclerView.OnScrollListener externalOnScrollListener) {
        mLoadable = loadable;
        mExternalOnScrollListener = externalOnScrollListener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        final int totalItemCount = layoutManager.getItemCount();
        int lastVisibleItemPosition;
        if (layoutManager instanceof GridLayoutManager) {
            lastVisibleItemPosition = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
        } else { //如果錯誤就讓他crash，debug的時候就應該發現
            lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        }

        if (mIsLoading) {
            if (totalItemCount > mPreviousTotal) {
                mIsLoading = false;
            }
        } else if ((totalItemCount - lastVisibleItemPosition) <= ITEM_LEFT_TO_LOAD_MORE) {

            if (mLoadable != null && dy > 0) {
                mIsLoading = true;
                mLoadable.loadMore();
            } else {
                mIsLoading = false;
            }
        }

        mPreviousTotal = totalItemCount;

        if (mExternalOnScrollListener != null) {
            mExternalOnScrollListener.onScrolled(recyclerView, dx, dy);
        }
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        if (mExternalOnScrollListener != null) {
            mExternalOnScrollListener.onScrollStateChanged(recyclerView, newState);
        }
    }

    public void resetLoadMoreStatus() {
        this.mIsLoading = false;
        this.mPreviousTotal = 0;
    }
}
