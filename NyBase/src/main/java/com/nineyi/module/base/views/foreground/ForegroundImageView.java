package com.nineyi.module.base.views.foreground;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import con.nineyi.module.base.R;


public class ForegroundImageView extends ImageView {

    private ForegroundWrapper mForegroundWrapper;

    public ForegroundImageView(Context context) {
        this(context, null);
    }

    public ForegroundImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ForegroundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NYforeground, defStyleAttr, 0);

        Drawable foreground = typedArray.getDrawable(R.styleable.NYforeground_ny_foreground);
        mForegroundWrapper = ForegroundWrapper.from(foreground, this);

        typedArray.recycle();
    }

    public ForegroundImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        TypedArray typedArray = context
                .obtainStyledAttributes(attrs, R.styleable.NYforeground, defStyleAttr, defStyleRes);

        Drawable foreground = typedArray.getDrawable(R.styleable.NYforeground_ny_foreground);

        mForegroundWrapper = ForegroundWrapper.from(foreground, this);

        typedArray.recycle();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        mForegroundWrapper.draw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mForegroundWrapper.onSizeChanged(w, h, oldw, oldh, this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mForegroundWrapper.onMeasure(widthMeasureSpec, heightMeasureSpec, this);
    }

    @Override
    protected boolean verifyDrawable(Drawable who) {
        return super.verifyDrawable(who) || mForegroundWrapper.verifyDrawable(who);
    }

    @Override
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        mForegroundWrapper.jumpDrawablesToCurrentState();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        mForegroundWrapper.drawableStateChanged(this);
    }

    @Override
    public void drawableHotspotChanged(float x, float y) {
        super.drawableHotspotChanged(x, y);

        mForegroundWrapper.drawableHotspotChanged(x, y);
    }
}
