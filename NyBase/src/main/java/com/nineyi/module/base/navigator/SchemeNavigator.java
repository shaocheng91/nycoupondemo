package com.nineyi.module.base.navigator;

import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.utils.MemoryUtils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import con.nineyi.module.base.R;


/**
 * Created by ReeceCheng on 2017/9/1.
 */

public class SchemeNavigator implements INavigator {
    /**
     *  Scheme Pattern:
     *  PACKAGE_NAME + ".scheme.module." + MODULE_NAME + "://"
     *
     *  example:
     *  com.nineyi.demoshop.scheme.module://hotsale
     */
    private static final String SCHEME_PACKAGE = ApplicationProvider.getInstance().getProviderAppContext().getPackageName();
    private static final String SCHEME_PATTERN = SCHEME_PACKAGE +".scheme.module://%1$s/";

    private Intent mIntent;

    public SchemeNavigator(Intent intent) {
        mIntent = intent;
    }

    @Override
    public void doNavigation(Context context) {
        if(MemoryUtils.isLowMemory()) {
            Intent intent = getLowMemoryIntent(context);
            Toast.makeText(context, R.string.low_memory, Toast.LENGTH_SHORT).show();
            context.startActivity(intent);
        }

        context.startActivity(mIntent);
    }

    public void doNavigationForResult(Fragment fragment, int requestCode) {
        Context context = fragment.getActivity();
        if(MemoryUtils.isLowMemory()) {
            Intent intent = getLowMemoryIntent(context);
            Toast.makeText(context, R.string.low_memory, Toast.LENGTH_SHORT).show();
            context.startActivity(intent);
        }

        fragment.startActivityForResult(mIntent, requestCode);
    }

    public Intent getIntent() {
        return mIntent;
    }

    private Intent getLowMemoryIntent(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getHome(context)));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return intent;
    }

    private String getHome(Context context) {
        return String.format(SCHEME_PATTERN, context.getString(R.string.scheme_homepage));
    }

    @Override
    public boolean shouldNavigateAfterGetFocus() {
        return false;
    }

    @Override
    public INavigator shouldNavigateAfterHasFocus(boolean shouldNavigateAfterHasFocus) {
        return null;
    }

    @Override
    public String getTargetName() {
        return null;
    }

    public static class Builder {

        private String mTarget;
        private String mPath;
        private Bundle mExtras;

        public Builder(String target) {
            mTarget = target;
        }

        public Builder path(String path) {
            mPath = path;
            return this;
        }

        public Builder extras(Bundle extras) {
            mExtras = extras;
            return this;
        }

        public SchemeNavigator build() {
            Intent intent = new Intent(Intent.ACTION_VIEW);

            String scheme = String.format(SCHEME_PATTERN, mTarget);

            if(mPath != null) {
                scheme += mPath;
            }

            intent.setData(Uri.parse(scheme));
            intent.addCategory(Intent.CATEGORY_DEFAULT);

            if (mExtras != null) {
                intent.putExtras(mExtras);
            }

            return new SchemeNavigator(intent);
        }
    }

}
