package com.nineyi.module.base.views;


import com.nineyi.module.base.ui.UiUtils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import con.nineyi.module.base.R;


/**
 * Created by tedliang on 2016/10/11.
 */

public abstract class CommonEmptyView extends RelativeLayout {

    private ImageView mEmptyImageView;
    private TextView mBigTitle;
    private TextView mSmallTitle;
    private TextView mEmptyClickBtn;

    public CommonEmptyView(Context context) {
        super(context);
        init(null);
    }

    public CommonEmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CommonEmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CommonEmptyView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.nineyi_empty_view, this);
        setBackgroundColor(Color.parseColor("#f0f0f0"));
        mEmptyImageView = (ImageView) view.findViewById(R.id.nineyi_empty_imgview);
        mBigTitle = (TextView) view.findViewById(R.id.nineyi_empty_big_title);
        mSmallTitle = (TextView) view.findViewById(R.id.nineyi_empty_small_title);
        mEmptyClickBtn = (TextView)view.findViewById(R.id.nineyi_empty_btn);


        mEmptyClickBtn.setTextColor(getGlobalStyleColor());
        if(isJellyBeanOrLater()){
            mEmptyClickBtn.setBackground(getGradientDrawable());
        } else {
            mEmptyClickBtn.setBackgroundDrawable(getGradientDrawable());
        }

        if(attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CommonEmptyView,
                    0, 0);

            Drawable d = a.getDrawable(R.styleable.CommonEmptyView_empty_img);
            String title = a.getString(R.styleable.CommonEmptyView_empty_title);
            String subTitle = a.getString(R.styleable.CommonEmptyView_empty_subtitle);
            String clickWording = a.getString(R.styleable.CommonEmptyView_empty_btn_text);
            boolean isShow = a.getBoolean(R.styleable.CommonEmptyView_empty_btn_visibility, false);

            if(d != null){
                mEmptyImageView.setImageDrawable(d);
            }

            mBigTitle.setText(title);
            mSmallTitle.setText(subTitle);
            if(isShow) {
                mEmptyClickBtn.setVisibility(VISIBLE);
            } else {
                mEmptyClickBtn.setVisibility(GONE);
            }

            mEmptyClickBtn.setText(clickWording);

            a.recycle();

        }

    }

    public void setOnEmptyBtnClickListener(OnClickListener listener){
        mEmptyClickBtn.setOnClickListener(listener);
    }

    public void setSubTitle(@StringRes int res){
        mSmallTitle.setText(res);
    }

    public void setTitle(@StringRes int res){
        mBigTitle.setText(res);
    }

    public void setEmptyImage(@DrawableRes int res){
        mEmptyImageView.setImageResource(res);
    }

    /**
     * API level 16
     */
    public static boolean isJellyBeanOrLater() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN;
    }

    private GradientDrawable getGradientDrawable() {
        GradientDrawable gd = new GradientDrawable();
        gd.setStroke(UiUtils.convertDipToPixel(1.5f, getContext().getResources().getDisplayMetrics()), getGlobalStyleColor());
        gd.setColor(Color.TRANSPARENT);
        gd.setCornerRadius(90);
        return gd;
    }

    abstract public int getGlobalStyleColor();
}
