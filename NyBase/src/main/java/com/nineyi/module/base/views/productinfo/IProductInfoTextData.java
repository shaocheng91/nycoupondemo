package com.nineyi.module.base.views.productinfo;

/**
 * Created by kelsey on 2017/3/20.
 */

public interface IProductInfoTextData {
    String getTitle();
    Double getPrice();
    Double getSuggestPrice();
    int getSalePageId();
}
