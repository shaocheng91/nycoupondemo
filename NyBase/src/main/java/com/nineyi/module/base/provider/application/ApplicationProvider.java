package com.nineyi.module.base.provider.application;

import com.nineyi.data.AppBuildConfig;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;


/**
 * Created by ReeceCheng on 2017/8/11.
 */

public class ApplicationProvider implements IApplicationProvider {
    private static volatile ApplicationProvider sInstance;

    private IApplicationProvider mProvider;

    public void init(IApplicationProvider provider) {
        this.mProvider = provider;
    }

    public static ApplicationProvider getInstance() {
        if (sInstance == null) {
            synchronized (ApplicationProvider.class) {
                if (sInstance == null) {
                    sInstance = new ApplicationProvider();
                }
            }
        }

        return sInstance;
    }

    @Override
    public String getProviderTag() {
        return mProvider.getProviderTag();
    }

    @Override
    public Context getProviderAppContext() {
        return mProvider.getProviderAppContext();
    }

    @Override
    public Resources getProviderAppResources() {
        return mProvider.getProviderAppResources();
    }

    @Override
    public AppBuildConfig getProviderAppBuildConfig() {
        return mProvider.getProviderAppBuildConfig();
    }

    @Override
    public Drawable getProviderAppLogo() {
        return mProvider.getProviderAppLogo();
    }
}
