package com.nineyi.module.base.appcompat;

import com.facebook.appevents.AppEventsLogger;
import com.nineyi.module.base.helper.NetworkHelper;
import com.nineyi.module.base.ui.BaseTintUtils;
import com.nineyi.module.base.utils.NetworkUtils;
import com.nineyi.module.base.ui.NineYiColor;
import com.nineyi.module.base.ui.Elevation;
import com.nineyi.module.base.utils.SDKVersionChecker;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;


/**
 * Created by tedliang on 15/5/18.
 */
public class NyBaseActionBarActivity extends AppCompatActivity {
    private NetworkHelper mNetworkHelper = new NetworkHelper();
    private NetworkSnackBar mNetworkSnackBar = new NetworkSnackBar();

    private boolean mIsAllowNetworkSnackBar = true;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (SDKVersionChecker.isLOLLIPOPOrLater()) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(NineYiColor.getGlobalNaviStatus());
        }
    }

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        BaseTintUtils.setToolbarTine(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);

        Elevation.elevate(this, provideActionBarElevation());
    }


    protected Elevation provideActionBarElevation() {
        return Elevation.LevelOne;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            if(mIsAllowNetworkSnackBar) {
                mNetworkHelper.regist(this, new NetworkHelper.OnReceiveListener() {
                    @Override
                    public void onReceive(Context context) {
                        boolean isNetworkConnected = NetworkUtils.isNetworkConnected(context);
                        if (!isNetworkConnected) {
                            showNetworkSnackBar(context);
                        } else {
                            hideNetworkSnackBar();
                        }
                    }
                });
            }
        }
    }

    private void showNetworkSnackBar(Context activity) {
        mNetworkSnackBar.show(activity);
    }

    private void hideNetworkSnackBar() {
        mNetworkSnackBar.hide();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        hideNetworkSnackBar();
        mNetworkHelper.unRegist(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void setAllowNetworkSnackBar(boolean isAllow) {
        mIsAllowNetworkSnackBar = isAllow;
    }

}
