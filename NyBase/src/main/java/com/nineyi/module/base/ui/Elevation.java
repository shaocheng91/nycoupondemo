package com.nineyi.module.base.ui;

import android.content.Context;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import con.nineyi.module.base.R;


public enum Elevation {
    DontChange(R.dimen.elevation_level_dont_change),
    LevelZero(R.dimen.elevation_level_0),
    LevelOne(R.dimen.elevation_level_1);
    @DimenRes private int mLevel;

    Elevation(@DimenRes int level) {
        mLevel = level;
    }

    private float getElevationInPx(@NonNull Context c) {
        return c.getResources().getDimension(mLevel);
    }

    public static void elevate(AppCompatActivity activity, Elevation level) {
        if (activity == null) {
            return;
        }
        if (activity.getSupportActionBar() == null) {
            return;
        }

        if (level == null || level == DontChange) {
            return;
        }

        ActionBar ab = activity.getSupportActionBar();
        try { // actionbar.setElevation may throw Exception
            float dimenInPx = level.getElevationInPx(activity);
            ab.setElevation(dimenInPx);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void elevate(View v, Elevation level) {
        if (level == null || level == DontChange || v == null) {
            return;
        }

        float dimenInPx = level.getElevationInPx(v.getContext());
        ViewCompat.setElevation(v, dimenInPx);
    }
}
