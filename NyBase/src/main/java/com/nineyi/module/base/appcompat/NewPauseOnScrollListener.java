package com.nineyi.module.base.appcompat;

import com.nineyi.module.base.ImageLoader;

import android.support.v7.widget.RecyclerView;


public class NewPauseOnScrollListener extends RecyclerView.OnScrollListener {


    private final RecyclerView.OnScrollListener mExternalListener;

    public NewPauseOnScrollListener() {
        this(null);
    }

    public NewPauseOnScrollListener(RecyclerView.OnScrollListener customListener) {
        mExternalListener = customListener;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        ImageLoader imageLoader = ImageLoader.getInstance(recyclerView.getContext());
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            imageLoader.resumeLoading();
        } else {
            imageLoader.pasueLoading();
        }

        if (mExternalListener != null) {
            mExternalListener.onScrollStateChanged(recyclerView, newState);
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (mExternalListener != null) {
            mExternalListener.onScrolled(recyclerView, dx, dy);
        }
    }
}
