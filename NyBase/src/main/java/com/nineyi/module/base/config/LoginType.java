package com.nineyi.module.base.config;

/**
 * Created by tedliang on 15/12/23.
 */
public enum LoginType {
    Facebook(0),
    Nineyi(1),
    ThirdParty(2),
    None(3);

    private final int value;

    private LoginType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
