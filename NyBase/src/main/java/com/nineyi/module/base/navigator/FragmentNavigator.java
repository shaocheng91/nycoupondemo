package com.nineyi.module.base.navigator;

import com.nineyi.module.base.appcompat.ActionBarModeHandler;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import con.nineyi.module.base.R;


public class FragmentNavigator implements INavigator {

    private static final String TAG = FragmentNavigator.class.getSimpleName();
    private Fragment mFragment;
    private int mReplaceId;
    private boolean mAddToStack = false;
    private String mTag = null;
    private String mStackName = null;

    private int mEnter = -1;
    private int mExit = -1;
    private int mPopEnter = -1;
    private int mPopExit = -1;

    private boolean mShouldNavigateAfterHasFocus = false;

    @Override
    public void doNavigation(Context hostActivity) {
        if (hostActivity instanceof FragmentActivity) {
            FragmentManager fragmentManager = ((FragmentActivity) hostActivity).getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            if (mEnter != -1 && mExit != -1 && mPopEnter != -1 && mPopExit != -1) {
                transaction.setCustomAnimations(mEnter, mExit, mPopEnter, mPopExit);
            }

            if (mTag != null) {
                transaction.replace(mReplaceId, mFragment, mTag);
            } else {
                transaction.replace(mReplaceId, mFragment);
            }

            if (mAddToStack) {
                transaction.addToBackStack(mStackName);
            } else {
                ((FragmentActivity) hostActivity).getSupportFragmentManager()
                        .popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

            transaction.commitAllowingStateLoss();
        }
    }

    @Override
    public boolean shouldNavigateAfterGetFocus() {
        return mShouldNavigateAfterHasFocus;
    }

    @Override
    public FragmentNavigator shouldNavigateAfterHasFocus(boolean shouldNavigateAfterHasFocus) {
        mShouldNavigateAfterHasFocus = shouldNavigateAfterHasFocus;
        return this;
    }

    @Override
    public String getTargetName() {
        return mFragment.getClass().getName();
    }

    public static final FragmentNavigator build() {
        return new FragmentNavigator();
    }

    public static final FragmentNavigator buildWithDefaultSetting(@IdRes int contentId) {
        return new FragmentNavigator()
                .customAnimations(R.anim.enter_right, R.anim.leave_left, R.anim.enter_left, R.anim.leave_right)
                .replace(contentId).addToStack(true);
    }

    public FragmentNavigator fragment(Fragment fragment) {
        mFragment = fragment;
        return this;
    }

    public FragmentNavigator replace(int id) {
        mReplaceId = id;
        return this;
    }

    public FragmentNavigator addToStack(boolean addToStack) {
        mAddToStack = addToStack;
        return this;
    }

    public FragmentNavigator stackName(String stackName) {
        mStackName = stackName;
        return this;
    }

    public FragmentNavigator tag(String tag) {
        mTag = tag;
        return this;
    }

    public FragmentNavigator customAnimations(int enter, int exit, int popEnter, int popExit) {
        mEnter = enter;
        mExit = exit;
        mPopEnter = popEnter;
        mPopExit = popExit;

        return this;
    }

    public FragmentNavigator actionbarHomeAsUp() {
        if (mFragment == null) {
            Log.e(TAG, "call FragmentNavigator.actionbarHomeAsUp but mFragment is null. Ignore.");
            return this;
        }

        Bundle argument = mFragment.getArguments();
        if (argument == null) {
            argument = new Bundle();
        }

        ActionBarModeHandler.addShowAsBackActionBarMode(argument);
        mFragment.setArguments(argument);

        return this;
    }

    public int replace() {
        return mReplaceId;
    }

    public boolean addToStack() {
        return mAddToStack;
    }

    public String tag() {
        return mTag;
    }

    public int[] customAnimations() {
        return new int[]{mEnter, mExit, mPopEnter, mPopExit};
    }

    public String stackName() {
        return mStackName;
    }

    public Bundle arguments() {
        return mFragment.getArguments();
    }

}
