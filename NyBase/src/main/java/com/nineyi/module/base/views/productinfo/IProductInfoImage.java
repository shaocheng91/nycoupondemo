package com.nineyi.module.base.views.productinfo;

/**
 * Created by kelsey on 2017/3/21.
 */

public interface IProductInfoImage {
    String getImageUrl();
}
