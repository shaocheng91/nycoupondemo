package com.nineyi.module.base.retrofit;

import com.nineyi.module.base.retrofit.CompositeDisposableHelper;
import com.nineyi.module.base.retrofit.appcompat.NyActionBarActivity;

import android.support.annotation.IdRes;
import android.support.v7.widget.Toolbar;

import io.reactivex.disposables.Disposable;

/**
 * Created by tedliang on 2016/9/19.
 */
public class RetrofitActivity extends NyActionBarActivity {
    CompositeDisposableHelper mCompositeDisposableHelper = new CompositeDisposableHelper();

    @Override
    public void onStop() {
        super.onStop();
        mCompositeDisposableHelper.clear();
    }

    public Toolbar findToolbar(@IdRes int resId) {
        return (Toolbar) findViewById(resId);
    }

    protected void addCompositeDisposable(Disposable d){
        mCompositeDisposableHelper.add(d);
    }

    protected void clearCompositeDisposable(){
        mCompositeDisposableHelper.clear();
    }

    protected boolean hasCompositeDisposable(){
        return mCompositeDisposableHelper.hasCompositeDisposables();
    }
}
