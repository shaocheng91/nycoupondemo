package com.nineyi.module.base.views;

import com.nineyi.module.base.ui.UiUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;


/**
 * Created by ReeceCheng on 2016/7/27.
 */
public class AverageLayout extends LinearLayout {

    public static final int DEFAULT_COLUMN = 3;

    private int mColumn;
    private int mMargin;

    public AverageLayout(Context context) {
        super(context);
        init();
    }

    public AverageLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AverageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("NewApi")
    public AverageLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mColumn = DEFAULT_COLUMN;
        mMargin = UiUtils.convertDipToPixel(13, getContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int painterX = 0;
        int painterY = 0;

        int childCount = getChildCount();
        for ( int i = 0; i < childCount; i++ ) {

            View child = getChildAt(i);

            int width  = child.getMeasuredWidth();
            int height = child.getMeasuredHeight();

            // 換行
            if (i != 0 && i % mColumn == 0) {
                painterX = 0;
                painterY += (height + mMargin);
            }

            // 畫 child
            child.layout(painterX,
                    painterY,
                    painterX + width,
                    painterY + height);

            // 更新 x position
            painterX += (width + mMargin);
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int childCount = getChildCount();

        int defaultWidth = getMeasuredWidth();
        int width  = (defaultWidth - ((mColumn-1) * mMargin)) / mColumn;

        // 先算child
        for ( int i = 0; i < childCount; i++ ) {
            View child = getChildAt(i);

            int spec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
            measureChild(child, spec, heightMeasureSpec);
        }

        // 再算這個View
        if(childCount > 0) {
            int row = childCount / mColumn;

            if (childCount % mColumn != 0) {
                row++;
            }

            int height = getChildAt(0).getMeasuredHeight();

            int totalHeight = height * row;

            if(row > 1) {
                totalHeight += (mMargin * row-1);
            }

            setMeasuredDimension(resolveSize(getMeasuredWidth(), widthMeasureSpec), resolveSize(totalHeight, heightMeasureSpec));
        }
    }

    public void setColumn(int column) {
        this.mColumn = column;
    }

}
