package com.nineyi.module.base.views.productinfo;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;


public class ProductInfoSmallImageView extends ImageView {

    public ProductInfoSmallImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = widthSize;

        setMeasuredDimension(widthSize, heightSize);
    }
}
