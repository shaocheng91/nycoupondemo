package com.nineyi.module.base.retrofit;

import com.nineyi.module.base.toolbartab.SlidingTabLayout;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;


abstract public class TabPagerActivity extends RetrofitActivity implements ViewPager.OnPageChangeListener {

    private static final String SAVED_STATE_SELECTED_NAVIGATION_ITEM = "savedStateSelectedNavigationItem";

    private ViewPager mViewPager;
    private TabsPagerAdapter mTabsPagerAdapter;
    private SlidingTabLayout mSlidingTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // setTheme(SampleList.THEME); // Used for theme switching in samples
        super.onCreate(savedInstanceState);
        mTabsPagerAdapter = new TabsPagerAdapter(this);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mSlidingTabLayout = (SlidingTabLayout) findViewById(getSlidingTabId());
        mViewPager = (ViewPager) findViewById(getViewPagerTabId());
        mViewPager.setAdapter(mTabsPagerAdapter);
        mViewPager.setCurrentItem(-1);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnPageChangeListener(this);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey(SAVED_STATE_SELECTED_NAVIGATION_ITEM)) {
            if (mTabsPagerAdapter.getCount() > 0) {
                mViewPager.setCurrentItem(savedInstanceState.getInt(SAVED_STATE_SELECTED_NAVIGATION_ITEM));
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(SAVED_STATE_SELECTED_NAVIGATION_ITEM, mViewPager.getCurrentItem());

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState);
    }

    //	@Override
    //	public void onRequestFinished(Request request, Bundle resultData) {
    //		// TODO Auto-generated method stub
    //
    //	}

    protected void addTab(String title, Class<?> fragClss, Bundle args) {
        mTabsPagerAdapter.addTab(fragClss, args, title);

    }

    protected void notifyTabPageChanged() {
        mTabsPagerAdapter.notifyDataSetChanged();
        mSlidingTabLayout.setViewPager(mViewPager);
    }


    public static class TabsPagerAdapter extends FragmentPagerAdapter {

        private final Context mContext;
        private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

        static final class TabInfo {

            private final Class<?> clss;
            private final Bundle args;
            private final String title;

            TabInfo(Class<?> _class, Bundle _args, String _title) {
                clss = _class;
                args = _args;
                if (_title == null) {
                    _title = "";
                }
                title = _title;
            }
        }

        public TabsPagerAdapter(FragmentActivity activity) {
            super(activity.getSupportFragmentManager());
            mContext = activity;
        }

        public TabsPagerAdapter(Fragment fragment) {
            super(fragment.getChildFragmentManager());
            mContext = fragment.getActivity();
        }

        @Override
        public Fragment getItem(int position) {
            //			Log.d(NineYiApplication.TAG, "TabsPagerAdapter/ getItem(" + position + ")");
            TabInfo info = mTabs.get(position);
            return Fragment.instantiate(mContext, info.clss.getName(), info.args);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            TabInfo info = mTabs.get(position);
            return info.title;
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        //		@Override
        //		public CharSequence getPageTitle(int position) {
        //			return mTabs.get(position).title;
        //		}

        public void addTab(Class<?> clss, Bundle args, String tabTitle) {
            TabInfo info = new TabInfo(clss, args, tabTitle);
            mTabs.add(info);
            notifyDataSetChanged();
        }

        public void removeAllTabs() {
            mTabs.clear();
        }
    }

    abstract protected int getSlidingTabId();

    abstract protected int getViewPagerTabId();

    public CharSequence getTabText(int position) {
        return mTabsPagerAdapter.getPageTitle(position);
    }

    // ViewPager.OnPageChangeListener
    // Please note that onPageSelected(0) would no called in first time
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

