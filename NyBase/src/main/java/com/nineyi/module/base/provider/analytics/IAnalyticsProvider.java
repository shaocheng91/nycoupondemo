package com.nineyi.module.base.provider.analytics;

import android.content.Context;


/**
 * Created by ReeceCheng on 2017/8/17.
 */

public interface IAnalyticsProvider {
    void clearScreenName();
    void setScreenNameWithPageTitle(String screenName, String id, String name);
    void sendScreenView(String screenName);
    void sendEventHit(String category, String action, String label);
    void sendEventHit(String category, String action);

    void sendTimingHit(String category, String name, String label, Long loadTime);

    void sendFacebookRegisterEvent(Context context);

    void send91RegisterEvent(Context context);
}
