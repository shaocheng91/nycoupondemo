package com.nineyi.module.base.config;


public class NineYiLayoutTemplates {

    public static final String HIGH_LIGHT = "HighLight";
    public static final String LAYOUT_TEMPLATE_MALL_APP_PR = "MallApppr";

    public static final String PREFIX_SP_ITEM_AD_SHOP_CATEGORY = "SpItemAd_ShopCategory_";

    public static final String MOBILE_HOME_SP_IMAGE = "MobileHome_SpImage";
    public static final String MOBILE_HOME_SP_BANNER = "MobileHome_SpBanner";
    public static final String MALL_CAT_AD_CATEGORY = "MallCatAd_Category_";
    public static final String SP_CAT_AD_SHOP_CATEGORY = "SpCatAd_ShopCategory_";
    public static final String MOBILE_HOME_SP_CAROUSEL = "MobileHome_SpCarousel";

}
