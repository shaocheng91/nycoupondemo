package com.nineyi.module.base.appcompat.scroll;

import com.nineyi.module.base.ui.UiUtils;
import com.nineyi.module.base.appcompat.PullToRefreshFragmentV3;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/1/23.
 */
public abstract class ShopHomeScrollingFragment extends PullToRefreshFragmentV3 implements IShopHomeScrollingChild {

    public static final int DEFAULT_PULL_TO_REFRESH_PROGRESS_PADDING = 10;

    protected ShopHomeScrollingHelper mShopHomeScrollingHelper;
    protected IShopHomeScrollingParent mIShopHomeScrollingParent;
    protected int mShopHomeHeaderHeight;

    protected ShopHomeScrollingOnScrollListener mOnScrollListener;
    protected RecyclerView mShopHomeScrollingRecyclerView;

    private int mViewCountForFirstRow;

    public ShopHomeScrollingFragment() {
        mShopHomeScrollingHelper = new ShopHomeScrollingHelper();
        mOnScrollListener = new ShopHomeScrollingOnScrollListener();

        // 大部分 Page 的第一列都只有一個View，因此預設成1
        mViewCountForFirstRow = 1;
    }

    @Override
    public void setPagePosition(int position) {
        mShopHomeScrollingHelper.setPagePosition(position);
    }

    @Override
    public void setScrollingParent(IShopHomeScrollingParent scrolling) {
        mIShopHomeScrollingParent = scrolling;
    }

    @Override
    public void adjustScroll(int headerHeight, float translationY, boolean isHeaderExpanded) {
        mShopHomeHeaderHeight = headerHeight;
        mShopHomeScrollingHelper.adjustScroll(mShopHomeScrollingRecyclerView, headerHeight, translationY, mViewCountForFirstRow, isHeaderExpanded);
    }

    @Override
    public void onHeaderTransition(int transition) {

    }

    protected void setupShopHomeScrollingWithRecyclerView(RecyclerView recyclerView) {
        if(recyclerView != null) {
            recyclerView.clearOnScrollListeners();
            recyclerView.addOnScrollListener(mOnScrollListener);
            mShopHomeScrollingRecyclerView = recyclerView;
        }
    }

    protected void setViewCountForFirstRow(int viewCountForFirstRow) {
        mViewCountForFirstRow = viewCountForFirstRow;
    }

    protected void setupViewOffset(int offset, View... views) {
        for(View v : views) {
            v.setY(offset);
        }
    }

    protected void setupProgressViewOffset(int offset, SwipeRefreshLayout layout) {
        int space = UiUtils.convertDipToPixel(DEFAULT_PULL_TO_REFRESH_PROGRESS_PADDING, getResources().getDisplayMetrics());
        layout.setProgressViewOffset(false, 0, offset+space);
    }

    protected void setupOnScrollListener(RecyclerView.OnScrollListener listener) {
        mOnScrollListener.setExternalOnScrollListener(listener);
    }

    class ShopHomeScrollingOnScrollListener extends RecyclerView.OnScrollListener {

        private RecyclerView.OnScrollListener mOnScrollListener;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if(mOnScrollListener != null) {
                mOnScrollListener.onScrollStateChanged(recyclerView, newState);
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            mShopHomeScrollingHelper.onScrolled(mIShopHomeScrollingParent, recyclerView, mShopHomeHeaderHeight, dy);

            if(mOnScrollListener != null) {
                mOnScrollListener.onScrolled(recyclerView, dx, dy);
            }
        }

        public void setExternalOnScrollListener(RecyclerView.OnScrollListener listener) {
            mOnScrollListener = listener;
        }
    }
}
