package com.nineyi.module.base.utils.share;

import android.content.Intent;


abstract class ShareStrategy {

    protected Object obj;

    public ShareStrategy(Object obj) {
        this.obj = obj;
    }

    abstract public Intent CreateIntent();
}

