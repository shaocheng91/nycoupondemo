package com.nineyi.module.base.tint;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;


/**
 * Created by tedliang on 2017/10/19.
 */

public class NyTintHelper {

    private Drawable getTintDrawable(Drawable d, ColorStateList colorStateList){
        Drawable tintDrawable =  DrawableCompat.wrap(d).mutate();
        DrawableCompat.setTintList(tintDrawable, colorStateList);
        return tintDrawable;
    }

    public Drawable getTintDrawable(Drawable d, int pressColor, int normalColor) {
        Drawable tintDrawable =  DrawableCompat.wrap(d).mutate();
        DrawableCompat.setTintList(tintDrawable, getBackgroundTint(pressColor, normalColor));
        return tintDrawable;
    }

    public final ColorStateList getBackgroundTint(int pressedColor, int normalColor) {
        ColorStateList themeColorStateList = new ColorStateList(
                new int[][]{
                        new int[] { android.R.attr.state_pressed },
                        new int[] { android.R.attr.state_active },
                        new int[] { android.R.attr.state_selected },
                        new int[] { android.R.attr.state_checked },
                        new int[] {}
                },
                new int[]{
                        pressedColor,
                        pressedColor,
                        pressedColor,
                        pressedColor,
                        normalColor
                }
        );

        return themeColorStateList;
    }
}
