package com.nineyi.module.base.retrofit;

import com.google.gson.JsonSyntaxException;

import java.net.URL;


public final class NyJsonSyntaxException extends RuntimeException {

    private String mBody;
    private URL mUrl;

    public NyJsonSyntaxException(JsonSyntaxException e) {
        super(e);
    }
    
    public URL getUrl() {
        return mUrl;
    }

    public void setUrl(URL url) {
        this.mUrl = url;
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    public String getBody() {
        return mBody;
    }


}