package com.nineyi.module.base.menu;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;

import con.nineyi.module.base.R;


// basic menu: R.menu.basic_menu
public class BasicMenuInflater {

    public static MenuItem inflateBasicMenu(Activity activity, Menu menu) {
        return inflateNormalBasicMenu(activity, menu);
    }

    private static MenuItem inflateNormalBasicMenu(Activity activity, Menu menu) {
        activity.getMenuInflater().inflate(R.menu.basic_menu, menu);
        return null;
    }
}
