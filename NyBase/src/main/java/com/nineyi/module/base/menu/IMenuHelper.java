package com.nineyi.module.base.menu;

/**
 * Created by tedliang on 15/4/28.
 */
public interface IMenuHelper {

    /*
        * 此method請由activity執行否則會出問題
        * */
    void refreshCount();

    void intMenu(boolean isShoppingcartVisible, boolean isSearchVisible);
}
