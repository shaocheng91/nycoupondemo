package com.nineyi.module.base.navigator;


import com.nineyi.module.base.CategoryType;
import com.nineyi.module.base.navigator.argument.provider.CouponOfflineUseArgumentProvider;
import com.nineyi.module.base.navigator.argument.provider.LoginPageArgumentProvider;
import com.nineyi.module.base.navigator.argument.provider.CouponDetailArgumentProvider;
import com.nineyi.module.base.navigator.argument.provider.ProductPageArgumentProvider;
import com.nineyi.module.base.provider.application.ApplicationProvider;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import con.nineyi.module.base.R;


/**
 * Created by ReeceCheng on 2017/8/31.
 */

public class SchemeRouter {
    private static final Resources mResource = ApplicationProvider.getInstance().getProviderAppResources();

    public static SchemeNavigator getNavToHome() {
        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_homepage)).build();
    }

    public static void navToHome(Context context) {
        getNavToHome().doNavigation(context);
    }

    public static SchemeNavigator getNavToHotSale() {
        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_hotsale)).build();
    }

    public static void navToHotSale(Context context) {
        getNavToHotSale().doNavigation(context);
    }

    public static SchemeNavigator getNavToProductPage(int salePageId, String searchText) {

        Bundle extras = new ProductPageArgumentProvider.BundleBuilder()
                .addSalePageId(salePageId)
                .addSearchText(searchText)
                .build();

        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_productpage))
                .extras(extras)
                .build();
    }

    public static void navToProductPage(Context context, int salePageId, int categoryId, CategoryType categoryType,
            @Nullable String searchText) {
        getNavToProductPage(salePageId, searchText).doNavigation(context);
    }

    public static SchemeNavigator getNavToLogin(String realFragment, Bundle realArgs) {
        Bundle extras = new LoginPageArgumentProvider.BundleBuilder()
                .addRealFragment(realFragment)
                .addRealArgument(realArgs)
                .build();

        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_login))
                .extras(extras)
                .build();
    }

    public static void navToLogin(Context context, String realFragment, Bundle realArgs) {
        getNavToLogin(realFragment, realArgs).doNavigation(context);
    }

    public static SchemeNavigator getNavToLogin(String realFragment, Bundle realArgs, boolean isPostSticky) {
        Bundle extras = new LoginPageArgumentProvider.BundleBuilder()
                .addRealFragment(realFragment)
                .addRealArgument(realArgs)
                .addIsPostSticky(isPostSticky)
                .build();

        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_login))
                .extras(extras)
                .build();
    }

    public static void navToLogin(Context context, String realFragment, Bundle realArgs, boolean isPostSticky) {
        getNavToLogin(realFragment, realArgs, isPostSticky).doNavigation(context);
    }

    public static void navToLoginOnResult(Fragment fragment, String realFragment, Bundle realArgs, int requestCode) {
        getNavToLogin(realFragment, realArgs).doNavigationForResult(fragment, requestCode);
    }

    public static SchemeNavigator getNavToCouponOnlineUse() {
        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_coupon_online_use)).build();
    }

    public static void navToCouponOnlineUse(Context context) {
        getNavToCouponOnlineUse().doNavigation(context);
    }

    public static SchemeNavigator getNavToCouponDetail(int couponId, String from) {

        Bundle extras = new CouponDetailArgumentProvider.BundleBuilder()
                .addCouponId(couponId)
                .addFrom(from)
                .build();

        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_coupon_detail))
                .extras(extras)
                .build();
    }

    public static void navToCouponDetail(Context context, int couponId, String from) {
        getNavToCouponDetail(couponId, from).doNavigation(context);
    }

    public static SchemeNavigator getNavToCouponList() {
        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_coupon_list)).build();
    }

    public static void navToCouponList(Context context) {
        getNavToCouponList().doNavigation(context);
    }

    public static SchemeNavigator getNavToMyCoupon() {
        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_my_coupon)).build();
    }

    public static void navToMyCoupon(Context context) {
        getNavToMyCoupon().doNavigation(context);
    }

    public static SchemeNavigator getNavToCouponOfflineUse(int couponId) {

        Bundle extras = new CouponOfflineUseArgumentProvider.BundleBuilder()
                .addCouponId(couponId)
                .build();

        return new SchemeNavigator.Builder(mResource.getString(R.string.scheme_coupon_offline_use)).extras(extras).build();
    }

    public static void navToCouponOfflineUse(Context context, int couponId) {
        getNavToCouponOfflineUse(couponId).doNavigation(context);
    }

}
