package com.nineyi.module.base.appcompat;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;


// Define what functions Fragment should provided to change UI ( mostly ActionBar )
public interface IActionBarFragment {

    void setActionBarTitle(@NonNull String title);

    void setActionBarTitle(@StringRes int stringRes);

    void setActionBarWithCustomView(View view);

    void setActionBarNaviLogo();
}
