package com.nineyi.module.base.ui;

import android.support.v4.view.ViewCompat;
import android.view.View;


/**
 * Created by tedliang on 2017/4/25.
 */

public class NineYiElevation {
    public static void setCardViewElevation(View view){
        ViewCompat.setElevation(view, 2);
    }
}
