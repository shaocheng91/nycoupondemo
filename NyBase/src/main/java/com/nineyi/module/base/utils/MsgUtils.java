package com.nineyi.module.base.utils;

import android.content.Context;
import android.widget.Toast;

import con.nineyi.module.base.R;


/**
 * Created by ReeceCheng on 2017/8/14.
 */

public class MsgUtils {

    public static void showNetworkErrorToast(Context context) {
        showLongToast(context, context.getText(R.string.network_error));
    }

    public static void showDataErrorToast(Context context) {
        showLongToast(context, context.getText(R.string.data_error));
    }

    public static void showLongToast(Context context, CharSequence text) {
        Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }

    public static void showShortToast(Context context, CharSequence text) {
        Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }
}
