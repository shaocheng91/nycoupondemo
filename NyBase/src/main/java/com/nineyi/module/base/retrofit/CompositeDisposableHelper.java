package com.nineyi.module.base.retrofit;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Created by ReeceCheng on 2017/7/4.
 */

public class CompositeDisposableHelper {
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public void add(Disposable d) {
        mCompositeDisposable.add(d);
    }

    public void clear(){
        mCompositeDisposable.clear();
    }

    public boolean hasCompositeDisposables(){
        return mCompositeDisposable.size() > 0;
    }
}
