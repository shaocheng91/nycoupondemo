package com.nineyi.module.base.navigator.argument;

import com.nineyi.autoargument.AutoArgument;

/**
 * Created by shaocheng on 2017/10/19.
 */

@AutoArgument
public class CouponDetailArgument {
    int CouponId;
    String From;
}
