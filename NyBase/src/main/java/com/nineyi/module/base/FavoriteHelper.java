/*
 * 1.改成 singleton？
 * 2.可改用 SharedPreferences.Editor putStringSet()
 */
package com.nineyi.module.base;

import com.nineyi.data.model.trace.TraceSalePage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.List;


public class FavoriteHelper {

    private SharedPreferences mPrefs;

    public FavoriteHelper(Context context) {
        mPrefs = context.getSharedPreferences("FavoritePreference", Context.MODE_PRIVATE);
    }

    public void add(int salePageId) {
        mPrefs.edit().putInt(String.valueOf(salePageId), salePageId).apply();
    }

    public void removeAll() {
        mPrefs.edit().clear().apply();
    }

    public void remove(int salePageId) {
        mPrefs.edit().remove(String.valueOf(salePageId)).apply();
    }

    public void updateBatchFrom(List<TraceSalePage> values) {
        Editor editor = mPrefs.edit();
        editor.clear();

        for (TraceSalePage value : values) {
            editor.putInt(String.valueOf(value.SalePageId), value.SalePageId);
        }

        editor.apply();
    }

    public boolean isFavorite(int salePageId) {
        return mPrefs.contains(String.valueOf(salePageId));
    }

    public boolean hasFavorite() {
        return mPrefs.getAll().size() > 0;
    }
}