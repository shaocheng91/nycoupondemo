package com.nineyi.module.base.component;

import android.content.Context;
import android.support.v4.app.Fragment;


/**
 * Created by tedliang on 2017/10/19.
 */
public interface ILoginHelper {

    void contentRedirectTo(Fragment fragment);

    void initialCookies(Context context);
}
