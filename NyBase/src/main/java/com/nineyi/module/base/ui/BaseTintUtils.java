package com.nineyi.module.base.ui;

import com.nineyi.module.base.provider.tint.TintProvider;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/8/22.
 */

public class BaseTintUtils {
    public static void setBackGroundTint(View v, int pressedColor, int normalColor) {
        TintProvider.getInstance().setBackGroundTint(v, pressedColor, normalColor);
    }

    public static void setToolbarTine(Toolbar toolbar) {
        TintProvider.getInstance().setToolbarTint(toolbar);
    }

    public static Drawable getTintDrawable(Drawable d, int pressColor, int normalColor) {
        return TintProvider.getInstance().getTintDrawable(d, pressColor, normalColor);
    }
}
