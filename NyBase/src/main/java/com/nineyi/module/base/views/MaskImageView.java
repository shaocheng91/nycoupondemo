package com.nineyi.module.base.views;


import com.nineyi.module.base.ui.UiUtils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import con.nineyi.module.base.R;


/**
 * Created by ReeceCheng on 2015/12/10.
 */
public class MaskImageView extends FrameLayout {

    protected ImageView mImageView;
    protected TextView mMask;

    public MaskImageView(Context context) {
        super(context);
        init(null);
    }

    public MaskImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MaskImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(21)
    public MaskImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        mImageView = new ImageView(getContext());
        mImageView.setLayoutParams(params);

        mMask = new TextView(getContext());
        mMask.setLayoutParams(params);
        mMask.setGravity(Gravity.CENTER);

        if(attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.MaskImageView,
                    0, 0);

            String maskTitle = a.getString(R.styleable.MaskImageView_maskTitle);
            int titleColor = a.getColor(R.styleable.MaskImageView_maskTitleColor, 0xFFFFFFFF);
            UiUtils.setTextSize(getContext(), a, mMask, R.styleable.MaskImageView_maskTitleTextSize, 10);
            int maskColor = a.getColor(R.styleable.MaskImageView_maskColor, 0x00000000);
            boolean isShowMask = a.getBoolean(R.styleable.MaskImageView_maskIsShow, false);

            if(maskTitle != null) {
                mMask.setText(maskTitle);
            }

            mMask.setTextColor(titleColor);

            mMask.setBackgroundColor(maskColor);

            if(isShowMask) {
                mMask.setVisibility(View.VISIBLE);
            } else {
                mMask.setVisibility(View.GONE);
            }

            a.recycle();

        }

        addView(mImageView);
        addView(mMask);
    }

    public ImageView getImageView() {
        return mImageView;
    }

    public TextView getMask() {
        return mMask;
    }

    public void setMaskVisibility(int visibility) {
        if(mMask != null) {
            mMask.setVisibility(visibility);
        }
    }
}
