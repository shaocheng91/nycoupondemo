package com.nineyi.module.base.views.foreground;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;


// a class wrap view's lifecycle ( or Callback ) like onMeasure, onDraw, etc.
// TODO:
// maybe we can create a WeakReference<View> to easy View related callback
public class ForegroundWrapper {

    private Drawable mForegroundDrawable;

    private ForegroundWrapper() {
    }

    /**
     * Please note {@param hostView} would NOT be cached in {@link ForegroundWrapper} class
     */
    protected static final ForegroundWrapper from(Drawable foreground, View hostView) {
        ForegroundWrapper f = new ForegroundWrapper();
        f.mForegroundDrawable = foreground;

        if (foreground == null) {
            hostView.setWillNotDraw(true);
            return f;
        }

        foreground.setCallback(hostView);
        hostView.setWillNotDraw(false);
        hostView.requestLayout();
        hostView.invalidate();

        return f;
    }

    /** View related callback start */
    protected void draw(Canvas canvas) {
        if (mForegroundDrawable != null) {
            mForegroundDrawable.draw(canvas);
        }
    }

    /**
     * Please note {@param hostView} would NOT be cached in {@link ForegroundWrapper} class
     */
    protected void onSizeChanged(int w, int h, int oldw, int oldh, View hostView) {
        if (mForegroundDrawable != null) {
            mForegroundDrawable.setBounds(0, 0, w, h);
            hostView.invalidate();
        }
    }

    /**
     * Please note {@param hostView} would NOT be cached in {@link ForegroundWrapper} class
     */
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec, View hostView) {
        if (mForegroundDrawable != null) {
            mForegroundDrawable.setBounds(0, 0, hostView.getMeasuredWidth(), hostView.getMeasuredHeight());
            hostView.invalidate();
        }
    }

    protected boolean verifyDrawable(Drawable who) {
        return (who == mForegroundDrawable);
    }

    public void jumpDrawablesToCurrentState() {
        if (mForegroundDrawable != null) {
            mForegroundDrawable.jumpToCurrentState();
        }
    }

    protected void drawableStateChanged(View hostView) {
        if (mForegroundDrawable != null && mForegroundDrawable.isStateful()) {
            mForegroundDrawable.setState(hostView.getDrawableState());
        }
    }

    public void drawableHotspotChanged(float x, float y) {
        if (mForegroundDrawable != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mForegroundDrawable.setHotspot(x, y);
            }
        }
    }

    /** View related callback end */

    public void recycle() {
        if (mForegroundDrawable != null) {
            mForegroundDrawable.setCallback(null);
        }
        mForegroundDrawable = null;
    }
}
