package com.nineyi.module.base.appcompat.scroll;

import android.support.v7.widget.RecyclerView;


/**
 * Created by ReeceCheng on 2017/1/10.
 */
public class ShopHomeScrollingHelper {
    private ShopHomeScrollingDecoration mScrollingDecoration;
    private int mHeaderHeight;
    private int mPagePosition;

    /**
     * @param view
     * @param headerHeight Height of Header.
     * @param translationY TransitionY of Header.
     * @param viewCountForFirstRow
     * @param isHeaderExpanded
     */
    public void adjustScroll(RecyclerView view, int headerHeight, float translationY, int viewCountForFirstRow,
            boolean isHeaderExpanded) {
        mHeaderHeight = headerHeight;

        if(mScrollingDecoration != null) {
            view.removeItemDecoration(mScrollingDecoration);
        }

        mScrollingDecoration = new ShopHomeScrollingDecoration(headerHeight, viewCountForFirstRow);
        view.addItemDecoration(mScrollingDecoration);

        int scrollOffset = ShopHomeScrollingUtils.getScrollY(view, headerHeight);

        if(isHeaderExpanded || scrollOffset < Math.abs(translationY)) {
            view.scrollBy(0, -scrollOffset); // 先調整RecyclerView的y到0
            view.scrollBy(0, (int) -translationY); // 再移到目前Header的y
        }
    }

    public void updateDecoration(RecyclerView view, int viewCountForFirstRow) {
        if(mScrollingDecoration != null) {
            view.removeItemDecoration(mScrollingDecoration);
        }

        mScrollingDecoration = new ShopHomeScrollingDecoration(mHeaderHeight, viewCountForFirstRow);
        view.addItemDecoration(mScrollingDecoration);
    }

    public void onScrolled(IShopHomeScrollingParent scrolling, RecyclerView view, int headerSpace, int dy) {
        if (scrolling != null) {
            scrolling.onScroll(view, mPagePosition, headerSpace, dy);
        }
    }

    public void setPagePosition(int position) {
        mPagePosition = position;
    }
}
