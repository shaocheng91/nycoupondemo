package com.nineyi.module.base.retrofit;

import com.nineyi.module.base.appcompat.ActionBarListFragment;

import android.os.Bundle;
import android.view.View;

import io.reactivex.disposables.Disposable;


/**
 * Created by tedliang on 2016/9/29.
 */

public class RetrofitActionBarListFragment extends ActionBarListFragment {

    protected CompositeDisposableHelper mCompositeDisposableHelper = new CompositeDisposableHelper();

    protected void addCompositeSubscription(Disposable d) {
        mCompositeDisposableHelper.add(d);
    }

    protected void clearCompositeSubscription() {
        mCompositeDisposableHelper.clear();
    }

    protected boolean hasCompositeSubscription() {
        return mCompositeDisposableHelper.hasCompositeDisposables();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onStop() {
        super.onStop();
        mCompositeDisposableHelper.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}