/**
 * 目前是綁 container id: R.id.content_frame
 */
package com.nineyi.module.base.appcompat;

import com.nineyi.module.base.helper.FragmentAnimationHelper;
import com.nineyi.module.base.helper.FragmentAnimationHelper.FragmentMove;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.HashMap;

import con.nineyi.module.base.R;


public class FragmentTransactionBuilder {

    private String mStackName;
    private HashMap<String, Integer> mAnimationList;
    private Fragment mTargetFragment;
    private boolean mAddToStack;
    private FragmentActivity mActivity;
    private FragmentAnimationHelper mHelper;
    private int mId;

    public FragmentTransactionBuilder(FragmentActivity activity) {
        mActivity = activity;
        mAddToStack = false;
        mHelper = new FragmentAnimationHelper();
        mAnimationList = mHelper
                .createAnimation(R.anim.enter_right, R.anim.leave_left, R.anim.enter_left, R.anim.leave_right);
    }

    public FragmentTransactionBuilder setStackName(String stackName) {
        mStackName = stackName;
        return this;
    }

    public FragmentTransactionBuilder setAnimationList(FragmentMove move) {

        if (move.equals(FragmentMove.FromBottomToTop)) {
            mAnimationList = mHelper
                    .createAnimation(R.anim.enter_bottom, R.anim.leave_top, R.anim.enter_top, R.anim.leave_bottom);
        } else if (move.equals(FragmentMove.FromTopToBottom)) {
            mAnimationList = mHelper
                    .createAnimation(R.anim.enter_top, R.anim.leave_bottom, R.anim.enter_bottom, R.anim.leave_top);
        } else if (move.equals(FragmentMove.FromLeftToRight)) {
            mAnimationList = mHelper
                    .createAnimation(R.anim.enter_left, R.anim.leave_right, R.anim.enter_right, R.anim.leave_left);
        } else if (move.equals(FragmentMove.FromRightToLeft)) {
            mAnimationList = mHelper
                    .createAnimation(R.anim.enter_right, R.anim.leave_left, R.anim.enter_left, R.anim.leave_right);
        } else if (move.equals(FragmentMove.None)) {
            mAnimationList = null;
        }

        return this;
    }

    public FragmentTransactionBuilder setTargetFragment(Fragment targetFragment) {
        mTargetFragment = targetFragment;
        return this;
    }

    public Fragment getTargetFragment() {
        return mTargetFragment;
    }

    public FragmentTransactionBuilder setAddToStack(boolean addToStack) {
        mAddToStack = addToStack;
        return this;
    }

    public FragmentTransactionBuilder setContainerViewId(int id) {
        mId = id;
        return this;
    }

    public FragmentTransaction build() {
        FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
        if (mAnimationList != null) {
            if (mAnimationList.containsKey(FragmentAnimationHelper.ANIMATION_ENTER) && mAnimationList
                    .containsKey(FragmentAnimationHelper.ANIMATION_LEAVE) && mAnimationList
                    .containsKey(FragmentAnimationHelper.ANIMATION_ENTER_POP) && mAnimationList
                    .containsKey(FragmentAnimationHelper.ANIMATION_LEAVE_POP)) {
                ft.setCustomAnimations(mAnimationList.get(FragmentAnimationHelper.ANIMATION_ENTER),
                        mAnimationList.get(FragmentAnimationHelper.ANIMATION_LEAVE),
                        mAnimationList.get(FragmentAnimationHelper.ANIMATION_ENTER_POP),
                        mAnimationList.get(FragmentAnimationHelper.ANIMATION_LEAVE_POP));
            }
        }

        ft.replace(mId, mTargetFragment);

        if (mAddToStack) {
            ft.addToBackStack(mStackName);
        } else {
            mActivity.getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        ft.commitAllowingStateLoss();
        return ft;
    }

}
