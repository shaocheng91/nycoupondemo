package com.nineyi.module.base.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;


public class TouchDetectRelativeLayout extends RelativeLayout {

    private boolean mIsHandle = true;

    public void setIsHandle(boolean isHandle) {
        mIsHandle = isHandle;

    }

    public boolean getIsHandle() {
        return mIsHandle;
    }

    public TouchDetectRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}