package com.nineyi.module.base.utils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import android.content.Context;


public class NyGooglePlayServiceUtils {

    public static boolean isServiceAvailable(Context context) {
        return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS;
    }

    public static int isGooglePlayServicesAvailable(Context context){
        return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
    }

}
