/*
 * 支援內嵌 ViewPager，能讓 inner ViewPager 自行處理水平 scroll
 */
package com.nineyi.module.base.ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;


public class SupportEmbeddedViewPager extends ViewPager {

    public SupportEmbeddedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        if (v != this && v instanceof ViewPager) {
            return true;
        }
        return super.canScroll(v, checkV, dx, x, y);
    }
}
