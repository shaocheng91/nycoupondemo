package com.nineyi.module.base.appcompat;

import com.nineyi.module.base.ui.UiUtils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import con.nineyi.module.base.R;


/**
 * Created by tedliang on 2016/10/4.
 * 這個class 只應該被NyBaseActionBarActivity所使用，不應該被其他class使用，否則會造成SnackBar不會消失
 */

class NetworkSnackBar {

    private View mView;

    public void show(Context context){
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        handleHide(windowManager);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        mView = layoutInflater.inflate(R.layout.network_snackbar,null);
        mView.findViewById(R.id.network_snackbar_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleHide(windowManager);
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.height = UiUtils.convertDipToPixel(50, context.getResources().getDisplayMetrics());
        lp.gravity = Gravity.BOTTOM;
        lp.type = WindowManager.LayoutParams.TYPE_APPLICATION_PANEL;
        lp.windowAnimations = R.style.NySnackBarAnimation;
        lp.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        windowManager.addView(mView, lp);
    }

    private void handleHide(WindowManager windowManager) {
        if(mView != null){
            if (mView.getParent() != null) {
                windowManager.removeView(mView);
            }
            mView = null;
        }
    }

    public void hide() {
        if(mView != null) {
            final WindowManager windowManager = (WindowManager) mView.getContext().getSystemService(Context.WINDOW_SERVICE);
            handleHide(windowManager);
        }
    }
}
