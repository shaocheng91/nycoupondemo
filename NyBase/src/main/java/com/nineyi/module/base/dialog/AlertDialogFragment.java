package com.nineyi.module.base.dialog;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


public final class AlertDialogFragment extends DialogFragment {

    private static final String FRAGMENT_TAG = "com.nineyi.dialogs.alertDialog";

    private static final String BUNDLE_EXTRA_TITLE = "title";
    private static final String BUNDLE_EXTRA_MESSAGE = "message";
    private static final String BUNDLE_EXTRA_CANCELABLE = "cancelable";
    private static final String BUNDLE_EXTRA_POSITIVE_BUTTON_TEXT = "positiveButtonText";
    private static final String BUNDLE_EXTRA_NEGATIVE_BUTTON_TEXT = "negativeButtonText";

    private OnClickListener mPositiveOnClickListener;
    private OnClickListener mNegativeOnClickListener;

    private static AlertDialogFragment newInstance(
            String title,
            String message,
            boolean cancelable,
            String positiveButtonText,
            OnClickListener positiveOnClickListener,
            String negativeButtonText,
            OnClickListener negativeOnClickListener) {

        AlertDialogFragment dialogFragment = new AlertDialogFragment();

        Bundle args = new Bundle();
        args.putString(BUNDLE_EXTRA_TITLE, title);
        args.putString(BUNDLE_EXTRA_MESSAGE, message);
        args.putBoolean(BUNDLE_EXTRA_CANCELABLE, cancelable);
        args.putString(BUNDLE_EXTRA_POSITIVE_BUTTON_TEXT, positiveButtonText);
        args.putString(BUNDLE_EXTRA_NEGATIVE_BUTTON_TEXT, negativeButtonText);
        dialogFragment.setArguments(args);

        dialogFragment.mPositiveOnClickListener = positiveOnClickListener;
        dialogFragment.mNegativeOnClickListener = negativeOnClickListener;
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        Builder b = new Builder(getActivity());
        b.setTitle(args.getString(BUNDLE_EXTRA_TITLE));
        b.setMessage(args.getString(BUNDLE_EXTRA_MESSAGE));
        setCancelable(args.getBoolean(BUNDLE_EXTRA_CANCELABLE));
        b.setPositiveButton(args.getString(BUNDLE_EXTRA_POSITIVE_BUTTON_TEXT), mPositiveOnClickListener);
        b.setNegativeButton(args.getString(BUNDLE_EXTRA_NEGATIVE_BUTTON_TEXT), mNegativeOnClickListener);
        return b.create();
    }

    public static class AlertDialogFragmentBuilder {

        private FragmentActivity mActivity;
        private String mTitle;
        private String mMessage;
        private boolean mCancelable;
        private String mPositiveButtonText;
        private OnClickListener mPositiveButtonOnClickListener;
        private String mNegativeButtonText;
        private OnClickListener mNegativeButtonOnClickListener;

        public AlertDialogFragmentBuilder(FragmentActivity activity) {
            mActivity = activity;
        }

        public AlertDialogFragmentBuilder setTitle(int resId) {
            mTitle = mActivity.getString(resId);
            return this;
        }

        public AlertDialogFragmentBuilder setTitle(String text) {
            mTitle = text;
            return this;
        }

        public AlertDialogFragmentBuilder setMessage(int resId) {
            mMessage = mActivity.getString(resId);
            return this;
        }

        public AlertDialogFragmentBuilder setMessage(String text) {
            mMessage = text;
            return this;
        }

        public AlertDialogFragmentBuilder setCancelable(boolean cancelable) {
            mCancelable = cancelable;
            return this;
        }

        public AlertDialogFragmentBuilder setPositiveButton(int resId, OnClickListener onClickListener) {
            return setPositiveButton(mActivity.getString(resId), onClickListener);
        }

        public AlertDialogFragmentBuilder setPositiveButton(String text, OnClickListener onClickListener) {
            mPositiveButtonText = text;
            mPositiveButtonOnClickListener = onClickListener;
            return this;
        }

        public AlertDialogFragmentBuilder setNegativeButton(int resId, OnClickListener onClickListener) {
            return setNegativeButton(mActivity.getString(resId), onClickListener);
        }

        public AlertDialogFragmentBuilder setNegativeButton(String text, OnClickListener onClickListener) {
            mNegativeButtonText = text;
            mNegativeButtonOnClickListener = onClickListener;
            return this;
        }

        public void show() {
            FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            Fragment prev = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
            if (prev != null) {
                fragmentTransaction.remove(prev);
            }
            fragmentTransaction.addToBackStack(null);

            AlertDialogFragment.newInstance(
                    mTitle,
                    mMessage,
                    mCancelable,
                    mPositiveButtonText,
                    mPositiveButtonOnClickListener,
                    mNegativeButtonText,
                    mNegativeButtonOnClickListener).show(fragmentManager, FRAGMENT_TAG);
        }

    }

    public static void dismiss(FragmentActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment prev = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
        if (prev != null) {
            fragmentTransaction.remove(prev);
        }
        fragmentTransaction.commit();
    }
}