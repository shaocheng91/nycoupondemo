package com.nineyi.module.base.retrofit;

import android.util.Log;

import com.nineyi.module.base.crashlytics.NyCrashlyticsMessage;
import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.utils.MsgUtils;
import com.nineyi.module.base.utils.NetworkUtils;

import java.net.UnknownHostException;

/**
 * Created by shaocheng on 2017/11/7.
 */

public class ApiErrorHandler {
    public static void handleApiError(Throwable throwable) {
        if (throwable != null && throwable.getMessage() != null) {
            Log.e(ApplicationProvider.getInstance().getProviderTag(), throwable.getMessage());
        }

        boolean isNetworkConnected = NetworkUtils.isNetworkConnected(ApplicationProvider.getInstance().getProviderAppContext());

        if(isNetworkConnected) {
            if (throwable instanceof UnknownHostException) {
                MsgUtils.showNetworkErrorToast(ApplicationProvider.getInstance().getProviderAppContext());
            } else if (throwable instanceof NyJsonSyntaxException) {
                NyCrashlyticsMessage.logRetrofitApiFailed((NyJsonSyntaxException) throwable);
                MsgUtils.showDataErrorToast(ApplicationProvider.getInstance().getProviderAppContext());
            } else {
                MsgUtils.showDataErrorToast(ApplicationProvider.getInstance().getProviderAppContext());
            }
        }
    }
}
