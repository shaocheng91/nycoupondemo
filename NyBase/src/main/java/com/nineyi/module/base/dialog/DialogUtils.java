package com.nineyi.module.base.dialog;

import com.nineyi.module.base.utils.StringUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.View;

import con.nineyi.module.base.R;


/**
 * Created by ted on 15/4/15.
 */
public class DialogUtils {

    public static void showLoginProcessBreakOffDialogConfirmOrCancel(Context context, String title, String msg,
            DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener negListener) {
        showCommonDialog(context, title, msg, context.getString(R.string.login_process_confirm), posListener,
                context.getString(R.string.login_process_cancel), negListener, null);
    }

    public static void showCommonDialog(Context context, String title, String msg, String posClick,
            DialogInterface.OnClickListener posListener, String negClick, DialogInterface.OnClickListener negListener,
            DialogInterface.OnCancelListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!StringUtils.isNullOrSpace(title)) {
            builder.setTitle(title);
        }

        if (!StringUtils.isNullOrSpace(msg)) {
            builder.setMessage(msg);
        }

        if (posListener != null) {
            builder.setPositiveButton(StringUtils.isNullOrSpace(posClick) ? context.getString(R.string.ok) : posClick,
                    posListener);
        }

        if (negClick != null) {
            builder.setNegativeButton(
                    StringUtils.isNullOrSpace(negClick) ? context.getString(R.string.cancel) : negClick, negListener);
        }

        if (cancelListener != null) {
            builder.setOnCancelListener(cancelListener);
        }

        builder.setCancelable(false);
        builder.show();
    }

    public static AlertDialog showMoveToFavAndDeleteDialogue(Context context,
            String message,
            String positiveText,
            DialogInterface.OnClickListener positiveClick,
            String negativeText,
            DialogInterface.OnClickListener negativeClick,
            String neutralText,
            DialogInterface.OnClickListener neutralClick,
            DialogInterface.OnCancelListener cancelListener) {

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(positiveText, positiveClick)
                .setNegativeButton(negativeText, negativeClick)
                .setNeutralButton(neutralText, neutralClick)
                .setOnCancelListener(cancelListener).show();

        alertDialog.setCanceledOnTouchOutside(true);

        return alertDialog;

    }

    public static AlertDialog showConfirmMessageDialogue(Context context,
                                                         String message,
                                                         DialogInterface.OnClickListener positiveClick) {

        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), positiveClick)
                .setCancelable(false).show();
    }

    public static AlertDialog showShoppingCartNaviToHomeDialogue(Context context,
            String message,
            DialogInterface.OnClickListener positiveClick,
            DialogInterface.OnClickListener neutralClick) {

        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), positiveClick)
                .setNeutralButton(context.getString(R.string.shopping_cart_navi_to_home), neutralClick)
                .setCancelable(false).show();
    }

    public static AlertDialog showNextStepSoldOutAlert(Context context,
            String message,
            String positiveText,
            DialogInterface.OnClickListener positiveClick,
            String neutralText,
            DialogInterface.OnClickListener neutralClick) {

        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(positiveText, positiveClick)
                .setNeutralButton(neutralText, neutralClick).show();
    }

    public static AlertDialog showInfoModuleApiErrorDialogue(Context context,
            String message) {

        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), null)
                .setCancelable(false).show();
    }

    public static AlertDialog showShopHomeDialogue(Context context,
            View view,
            String positiveText,
            DialogInterface.OnClickListener positiveClick,
            String negativeText,
            DialogInterface.OnClickListener negativeClick) {

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(true)
                .setPositiveButton(positiveText, positiveClick)
                .setNegativeButton(negativeText, negativeClick).show();

        alertDialog.setCanceledOnTouchOutside(true);

        return alertDialog;

    }

    public static AlertDialog showSuggestUpdateDialog(Context context,
                                                      DialogInterface.OnClickListener positiveClick,
                                                      DialogInterface.OnClickListener neutralClick) {
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.suggest_update_dialog_title))
                .setMessage(context.getString(R.string.suggest_update_dialog_message))
                .setPositiveButton(context.getString(R.string.ok), positiveClick)
                .setNeutralButton(context.getString(R.string.suggest_update_dialog_update_button), neutralClick)
                .setCancelable(false).show();
    }

    public static AlertDialog showProductMemberDialog(Context context,
            DialogInterface.OnClickListener positiveClick,
            DialogInterface.OnClickListener negativeClick) {

        return new AlertDialog.Builder(context)
                .setTitle(Html.fromHtml(context.getString(R.string.product_member_alert_title)))
                .setMessage(context.getString(R.string.product_member_alert_message))
                .setPositiveButton(context.getString(R.string.product_member_alert_positive_text), positiveClick)
                .setNegativeButton(context.getString(R.string.product_member_alert_negative_text), negativeClick)
                .setCancelable(false).show();
    }
}
