package com.nineyi.module.base.views.foreground;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import con.nineyi.module.base.R;


public class ForegroundRelativeLayout extends RelativeLayout {

    private ForegroundWrapper mForegroundWrapper;

    public ForegroundRelativeLayout(Context context) {
        this(context, null);
    }

    public ForegroundRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ForegroundRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NYforeground, defStyleAttr, 0);

        Drawable foreground = typedArray.getDrawable(R.styleable.NYforeground_ny_foreground);
        mForegroundWrapper = ForegroundWrapper.from(foreground, this);

        typedArray.recycle();
    }

    public ForegroundRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        TypedArray typedArray = context
                .obtainStyledAttributes(attrs, R.styleable.NYforeground, defStyleAttr, defStyleRes);

        Drawable foreground = typedArray.getDrawable(R.styleable.NYforeground_ny_foreground);

        mForegroundWrapper = ForegroundWrapper.from(foreground, this);
        typedArray.recycle();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        mForegroundWrapper.draw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mForegroundWrapper.onSizeChanged(w, h, oldw, oldh, this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mForegroundWrapper.onMeasure(widthMeasureSpec, heightMeasureSpec, this);
    }

    // copy from FrameLayout
    //   If your view subclass is displaying its own Drawable objects,
    // it should override this function and return true for any Drawable it is displaying.
    @Override
    protected boolean verifyDrawable(Drawable who) {
        return super.verifyDrawable(who) || mForegroundWrapper.verifyDrawable(who);
    }

    // Drawable.jumpToCurrentState
    //   If this Drawable does transition animations between states,
    // ask that it immediately jump to the current state and skip any active animations.

    // Call Drawable.jumpToCurrentState() on all Drawable objects associated with this view.
    @Override
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        mForegroundWrapper.jumpDrawablesToCurrentState();
    }

    //This function is called whenever the state of the view changes in such a way that it impacts the state of drawables being shown.
    //If the View has a StateListAnimator, it will also be called to run necessary state change animations.
    //Be sure to call through to the superclass when overriding this function.
    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        mForegroundWrapper.drawableStateChanged(this);
    }

    @Override
    public void drawableHotspotChanged(float x, float y) {
        super.drawableHotspotChanged(x, y);

        mForegroundWrapper.drawableHotspotChanged(x, y);
    }
}
