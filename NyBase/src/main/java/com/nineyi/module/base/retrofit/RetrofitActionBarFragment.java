package com.nineyi.module.base.retrofit;

import com.nineyi.module.base.appcompat.ActionBarFragment;
import com.nineyi.module.base.menu.IMenuHelper;
import com.nineyi.module.base.menu.MenuHelper;

import android.view.Menu;

import io.reactivex.disposables.Disposable;

/**
 * Created by tedliang on 2016/9/14.
 */
public class RetrofitActionBarFragment extends ActionBarFragment {
    protected CompositeDisposableHelper mCompositeDisposableHelper = new CompositeDisposableHelper();

    @Override
    public void onStop() {
        super.onStop();
        mCompositeDisposableHelper.clear();
    }

    protected void addCompositeSubscription(Disposable d) {
        mCompositeDisposableHelper.add(d);
    }

    protected void clearCompositeSubscription(){
        mCompositeDisposableHelper.clear();
    }

    protected boolean hasCompositeSubscription(){
        return mCompositeDisposableHelper.hasCompositeDisposables();
    }

    @Override
    public IMenuHelper provideMenuHelper(Menu menu) {
        return new MenuHelper(menu);
    }
}
