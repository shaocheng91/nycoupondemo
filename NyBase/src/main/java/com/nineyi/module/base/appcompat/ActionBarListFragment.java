package com.nineyi.module.base.appcompat;

import com.nineyi.module.base.menu.MenuHelper;
import com.nineyi.module.base.provider.analytics.AnalyticsProvider;
import com.nineyi.module.base.ui.Elevation;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;


public class ActionBarListFragment extends ListFragment implements IActionBarFragment {

    private AppCompatActivity mActivity;
    private ActionBarModeHandler mActionBarModeHandler = new ActionBarModeHandler(
            ActionBarModeHandler.ActionBarMode.KeepOriginal);
    protected MenuHelper mNineyiMenuHelper;

    public AppCompatActivity getActionBarActivity() {
        return mActivity;
    }

    /**
     * lifecycle, menu related implementation
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        mActionBarModeHandler.onCreate(arguments, this);
        setHasOptionsMenu(true);
    }

    // Change ActionBarMode. MUST BE CALLED BEFORE onActivityCreated
    protected void setActionBarModeHandler(ActionBarModeHandler actionBarModeHandler) {
        mActionBarModeHandler = actionBarModeHandler;
    }

    @Override
    public void onAttach(Activity activity) {
        if (!(activity instanceof AppCompatActivity)) {
            throw new IllegalStateException(getClass().getSimpleName() + " must be attached to a AppCompatActivity.");
        }
        mActivity = (AppCompatActivity) activity;

        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActionBarModeHandler.onResume(this, mActivity);

        Elevation.elevate(getActionBarActivity(), provideActionBarElevation());
    }

    // set ActionBar elevate, return DontChange if you dont want to change ActionBar elevate
    protected Elevation provideActionBarElevation() {
        if (getParentFragment() != null) {
            return Elevation.DontChange;
        } else {
            return Elevation.LevelOne;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        AnalyticsProvider.getInstance().clearScreenName();
    }

    public void setScreenNameAndView(String s) {
        AnalyticsProvider.getInstance().sendScreenView(s);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mActionBarModeHandler.onOptionsItemSelected(item, mActivity);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mNineyiMenuHelper = new MenuHelper(menu);
    }

    // XXX
    // ugly fix. should find better solution
    public void setSupportProgressBarIndeterminateVisibility() {
        //getActionBarActivity().setSupportProgressBarIndeterminateVisibility(visibility);
    }

    /**
     * {@link IActionBarFragment} implementation
     */
    @Override
    public void setActionBarTitle(@NonNull String title) {
        if(isAdded()) {
            mActionBarModeHandler.setActionBarTitle(title, mActivity);
        }
    }

    @Override
    public void setActionBarTitle(@StringRes int stringRes) {
        if (isAdded()) {
            String titleStr = mActivity.getString(stringRes);
            mActionBarModeHandler.setActionBarTitle(titleStr, mActivity);
        }
    }

    @Override
    public void setActionBarWithCustomView(View view) {
        if(isAdded()) {
            mActionBarModeHandler.setCustomViewToActionBar(view, mActivity);
        }
    }

    @Override
    public void setActionBarNaviLogo() {
        if(isAdded()) {
            mActionBarModeHandler.setNaviLogo(mActivity);
        }
    }
}
