package com.nineyi.module.base.component;

import android.support.v4.app.Fragment;


/**
 * Created by tedliang on 2017/10/18.
 */
public interface INavTypeManager {

    //TODO  navigateClass instanceof ShoppingCartActivity)
    boolean isShoppingCart(Object navigateClass);

    //TODO fr instanceof WebViewWithControlsFragment
    boolean isWebView(Fragment fr);
}
