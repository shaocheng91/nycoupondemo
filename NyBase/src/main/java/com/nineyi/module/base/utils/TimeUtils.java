package com.nineyi.module.base.utils;

import com.nineyi.module.base.provider.application.ApplicationProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import con.nineyi.module.base.R;


public class TimeUtils {

    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int SECOND_PER_MINUTE = 60;
    private static final int MINUTE_PER_HOUR = 60;
    private static final long ONE_DAY_TALLY_SECS = 86400L;
    private static final int DATE_FORMAT_YEAR_MONTH_DAY = R.string.date_format_yyyy_mm_dd_1;
    private static final int DATE_FORMAT_YEAR_MONTH_DAY_DEATAIL = R.string.date_format_yyyy_mm_dd_hh_mm_ss;

    private static final int DATE_FORMAT_YEAR_MONTH_DAY_SLASH = R.string.date_format_yyyy_mm_dd;
    public static final String GMT = "GMT";


    public static long convertSecondToMilliseconds(long val) {
        return val * MILLISECONDS_PER_SECOND;
    }

    public static long convertMinuteToMilliseconds(long val) {
        return val * SECOND_PER_MINUTE * MILLISECONDS_PER_SECOND;
    }

    public static long convertHourToMilliseconds(long val) {
        return val * MINUTE_PER_HOUR * SECOND_PER_MINUTE * MILLISECONDS_PER_SECOND;
    }

    public static long convertDayToMilliseconds(long val) {
        return val * ONE_DAY_TALLY_SECS * MILLISECONDS_PER_SECOND;
    }

    public static long formatDateToTimeMilliseconds(String dateString) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(getDateFormatYearMonthDay());
        Date date = sdf.parse(dateString);
        long timeMills = date.getTime();
        return timeMills;
    }

    public static long formatDateDetailToTimeMilliseconds(String dateString) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(getDateFormatYearMonthDayDetail());
        Date date = sdf.parse(dateString);
        long timeMills = date.getTime();
        return timeMills;
    }

    public static int diffDate(long currentDate, long canUseDate) {
        return (int) ((canUseDate - currentDate) / (ONE_DAY_TALLY_SECS * MILLISECONDS_PER_SECOND));
    }

    public static long getDateFormat(long timeMilliseconds) throws ParseException {
        Date d = new Date();
        d.setTime(timeMilliseconds);
        SimpleDateFormat formatter = new SimpleDateFormat(getDateFormatYearMonthDay());
        return formatDateToTimeMilliseconds(formatter.format(d));
    }

    public static long getDateDetailFormat(long timeMilliseconds) throws ParseException {
        Date d = new Date();
        d.setTime(timeMilliseconds);
        SimpleDateFormat formatter = new SimpleDateFormat(getDateFormatYearMonthDayDetail());
        return formatDateDetailToTimeMilliseconds(formatter.format(d));
    }

    public static boolean isInDate(long start, long end) {
        long now = new Date().getTime();
        return start <= now && now <= end;
    }

    /**
     * milliseconds convert to any date format
     * Example:
     *  > milliseconds = 1471489200000
     *  > format = "yyyy/MM/dd"
     *  > return = 2016/08/18
     */
    public static String getDateFormat(long milliseconds, String format) {
        Date d = new Date();
        d.setTime(milliseconds);
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
        return formatter.format(d);
    }

    public static String getDateFormat(long milliseconds, String format, TimeZone timeZone) {
        Date d = new Date();
        d.setTime(milliseconds);
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());

        if(timeZone != null) {
            formatter.setTimeZone(timeZone);
        }
        return formatter.format(d);
    }

    public static long getSeveralDaysAfter(int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, days);
        return cal.getTimeInMillis();
    }

    public static int getYear(long milliseconds) {
        Calendar cc = Calendar.getInstance();
        cc.setTimeInMillis(milliseconds);
        return cc.get(Calendar.YEAR);
    }

    public static boolean isWithinDays(long milliseconds, int days) {
        return milliseconds < TimeUtils.getSeveralDaysAfter(days);
    }

    private static String getString(int resId) {
        return ApplicationProvider.getInstance().getProviderAppResources().getString(resId);
    }

    public static String getDateFormatYearMonthDaySlash() {
        return getString(DATE_FORMAT_YEAR_MONTH_DAY_SLASH);
    }

    private static String getDateFormatYearMonthDay() {
        return getString(DATE_FORMAT_YEAR_MONTH_DAY);
    }

    private static String getDateFormatYearMonthDayDetail() {
        return getString(DATE_FORMAT_YEAR_MONTH_DAY_DEATAIL);
    }

}
