package com.nineyi.module.base.ui;

import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.utils.SDKVersionChecker;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DimenRes;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * Created by ReeceCheng on 2015/12/15.
 */
public class UiUtils {

    /**
     * Converts dp (dip) into its equivalent px
     */
    public static int convertDipToPixel(float dp, DisplayMetrics metrics) {
        return Math.round(TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics));
    }

    /**
     * Converts sp into its equivalent px
     */

    public static int convertSpToPixel(float sp, DisplayMetrics metrics) {
        return Math.round(TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, metrics));
    }

    public static int convertPixelToDip(float px, DisplayMetrics metrics) {
        return Math.round(px / metrics.density);
    }

    /**
     * Converts dimen resource into its equivalent px
     */
    public static int convertDimenResToPixel(@DimenRes int dimenRes) {
        return ApplicationProvider.getInstance().getProviderAppResources().getDimensionPixelSize(dimenRes);
    }

    public static void setTextSize(TypedArray typedArray, TextView tv, int index, int defaultSizeSp) {
        float textSize = typedArray.getDimension(index, -1);
        if (textSize == -1) {
            //default size sp to pixel
            textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, defaultSizeSp,
                    ApplicationProvider.getInstance().getProviderAppResources().getDisplayMetrics());
        }
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }

    // Used in ProductPage 3.0. Do not remove it.
    // Must use activity context never use application context
    public static int getToolbarHeight(Context context) {
        int toolbarPaddingTop = 0;
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            toolbarPaddingTop = TypedValue
                    .complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
            if (!SDKVersionChecker.isLOLLIPOPOrLater()) {
                toolbarPaddingTop += 12;
            }
        }
        return toolbarPaddingTop;
    }

    public static void setTextSize(Context context, TypedArray typedArray, TextView tv, int index, int defaultSizeSp) {
        float textSize = typedArray.getDimension(index, -1);
        if (textSize == -1) {
            //default size sp to pixel
            textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, defaultSizeSp,
                    context.getResources().getDisplayMetrics());
        }
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }
}
