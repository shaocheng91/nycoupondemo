package com.nineyi.module.base.ui;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import con.nineyi.module.base.R;


/**
 * Created by kennylee on 2015/6/10.
 */
public class AnimationInputLayout extends RelativeLayout {

    // com.nineyi.module.base.ui.AnimationInputLayout

    private Context mContext;
    private EditText mEtInput;
    private ImageView mImgEye;

    private int mCurrentInputTextMode;
    private InputTextMode mInputTextMode = new InputTextMode();

    private boolean isErrorStatus;

    private List<TextWatcher> mTextWatcherPlusList = new ArrayList<>();
    private Handler mHandler;
    private ShowKeyboardRunable mRunalbe;

    public AnimationInputLayout(Context context) {
        super(context);
        initials(context, null, 0);
    }

    public AnimationInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initials(context, attrs, 0);
    }

    public AnimationInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initials(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("NewApi")
    public AnimationInputLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initials(context, attrs, defStyleAttr);
    }

    private void initials(Context context, AttributeSet attrs, int defStyleAttr) {
        this.mContext = context;
        View view = inflate(context, R.layout.login_input_cutom_layout, this);
        mEtInput = (EditText) view.findViewById(R.id.id_et_input);
        mImgEye = (ImageView) view.findViewById(R.id.id_img_eye);
        mEtInput.setSingleLine();
        mEtInput.setFocusable(true);
        mEtInput.setFocusableInTouchMode(true);
        mEtInput.requestFocus();

        mHandler = new Handler();
        mRunalbe = new ShowKeyboardRunable();

        setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);


        TypedArray ta = null;

        try {
            ta = context.obtainStyledAttributes(attrs, R.styleable.AnimationInputLayout, defStyleAttr, 0);

            String hint = ta.getString(R.styleable.AnimationInputLayout_inputHint);
            boolean isSingleLine = ta.getBoolean(R.styleable.AnimationInputLayout_inputSingleline, true);
            int maxLength = ta.getInt(R.styleable.AnimationInputLayout_inputMaxLength, 20);
            Drawable drawable = ta.getDrawable(R.styleable.AnimationInputLayout_inputIcon);

            Log.d(getClass().getName(), "---> hint : " + hint);

            mEtInput.setHint(hint);
            mEtInput.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
            if (isSingleLine) {
                mEtInput.setSingleLine();
            }
            InputFilter[] filters = new InputFilter[1];
            filters[0] = new InputFilter.LengthFilter(maxLength);
            mEtInput.setFilters(filters);

        } finally {
            if (ta != null) {
                ta.recycle();
            }
        }

        mEtInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                for (TextWatcher tw : mTextWatcherPlusList) {
                    tw.beforeTextChanged(s, start, count, after);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // before: 刪除字時， before=1,count=0
                // count:  輸入時，before=0,count=1
                Log.d(getClass().getName(), " ---> onTextChanged: s: " + s + " ,start: " + start + " , before: " + before + " , count: "
                        + count);
                if (s.length() != 0) {
                    if (isEnterKey(before,count)) {
                        resetInputLayoutStyle();
                    }
                } else {
                    resetInputLayoutStyle();
                }

                for (TextWatcher tw : mTextWatcherPlusList) {
                    tw.onTextChanged(s, start, before, count);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                for (TextWatcher tw : mTextWatcherPlusList) {
                    tw.afterTextChanged(s);
                }
            }
        });

        resetInputLayoutStyle();
    }

    private boolean isEnterKey(int before,int count){
        return before < count;
    }

    public void shockErrorInput(Animator.AnimatorListener listener) {
        mEtInput.setBackgroundResource(R.drawable.bg_login_input_phone_error_et);
        mEtInput.setTextColor(Color.RED);
        if (mImgEye.getVisibility() == View.VISIBLE) {
            isErrorStatus = true;

            if (hasTextMask()) {
                setTextNoMask();
            }
        }

        ObjectAnimator anim = ObjectAnimator.ofFloat(this, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
        anim.addListener(listener);
        anim.setDuration(500);
        anim.start();
    }

    public void resetInputLayoutStyle() {
        setNormal();

        if (mImgEye.getVisibility() == View.VISIBLE) {
            if (!hasTextMask() && isErrorStatus) {
                setTextMask();
                isErrorStatus = false;
            }
        }
    }

    public void setNormal() {
        mEtInput.setBackgroundResource(R.drawable.bg_login_input_phone_et);
        mEtInput.setTextColor(Color.BLACK);
    }

    public void setOnEditListener(TextView.OnEditorActionListener listener){
        mEtInput.setOnEditorActionListener(listener);
    }

    public void setText(String msg) {
        mEtInput.setText(msg);
    }

    public String getText() {
        return mEtInput.getText().toString();
    }

    public void setHint(String hint) {
        mEtInput.setHint(hint);
    }

    public void requestInputFocus(){
        mEtInput.requestFocus();
    }

    public void clearInputFocus(){
        mEtInput.clearFocus();
    }

    public void showEyes(OnClickListener listener) {
        mImgEye.setVisibility(View.VISIBLE);
        mImgEye.setOnClickListener(listener);
    }

    public void hideEyes() {
        mImgEye.setVisibility(View.GONE);
    }

    public void hideKeyboard() {
        mHandler.removeCallbacks(mRunalbe);
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(mEtInput.getWindowToken(), 0);
        }
    }

    public void showKeyboard() {
        /**
         * http://blog.csdn.net/ccpat/article/details/46717573
         * Layout必須已經完成加載，如果還未繪製完成，則showSoftInput()方法不起作用。
         * 透過postDelayed的方式來延遲執行showSoftInput()
         */
        mHandler.postDelayed(mRunalbe, 100);
    }

    private class ShowKeyboardRunable implements Runnable {
        @Override
        public void run() {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if(imm != null) {
                imm.showSoftInput(mEtInput, InputMethodManager.SHOW_IMPLICIT);
            }
        }
    }

    public void setTextMask() {
        setInputTypeEyeOn(mInputTextMode.textMask);
    }

    public void setTextNoMask() {
        setInputTypeEyeOff(mInputTextMode.textNoMask);
    }

    public void setNumberMask() {
        setInputTypeEyeOn(mInputTextMode.numberMask);
    }

    public void setNumberNoMask() {
        setInputTypeEyeOff(mInputTextMode.numberNoMask);
    }

    public boolean hasTextMask() {
        return getInputType() == mInputTextMode.textMask || getInputType() == mInputTextMode.numberMask;
    }

    /**
     * to set a new TextWatcher on AnimationInputLayout
     *
     * @param textWatcher
     */
    public void setTextChangedListener(TextWatcher textWatcher) {
        mEtInput.addTextChangedListener(textWatcher);
    }

    /**
     * to add a new textWatcher into TextWatcherPlusList
     *
     * @param textWatcher
     */
    public void addTextChangedListener(TextWatcher textWatcher) {
        mTextWatcherPlusList.add(textWatcher);
    }

    private void setInputTypeEyeOn(int type) {
        setInputType(type, true);
    }

    private void setInputTypeEyeOff(int type) {
        setInputType(type, false);
    }

    private void setInputType(int type, boolean eyesOnOrOff) {
        mCurrentInputTextMode = type;
        mEtInput.setInputType(type);
        mEtInput.setSelection(mEtInput.getText().length());
        if (eyesOnOrOff) {
            mImgEye.setImageResource(R.drawable.btn_login_eye);
        } else {
            mImgEye.setImageResource(R.drawable.btn_login_eyeclose);
        }
    }

    private int getInputType() {
        return mCurrentInputTextMode;
    }

    class InputTextMode {

        public final int textMask = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;
        public final int textNoMask = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL;
        public final int numberMask =  InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD | InputType.TYPE_CLASS_PHONE;
        public final int numberNoMask = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL | InputType.TYPE_CLASS_PHONE;
    }

}
