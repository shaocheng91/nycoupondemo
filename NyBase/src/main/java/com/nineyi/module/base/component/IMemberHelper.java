package com.nineyi.module.base.component;

import com.nineyi.data.model.memberzone.VipMemberDataRoot;


/**
 * Created by tedliang on 2017/10/18.
 */
public interface IMemberHelper {

    void setMemberData(VipMemberDataRoot memberData);
}
