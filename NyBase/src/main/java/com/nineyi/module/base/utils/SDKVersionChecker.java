package com.nineyi.module.base.utils;


import android.os.Build;


/**
 * Helper class for SDK version check.
 */
public final class SDKVersionChecker {

    /**
     * API level 11
     */
    public static boolean isHoneyCombOrLater() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * API level 13
     */
    public static boolean isHoneyCombMr2OrLater() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2;
    }

    /**
     * API level 14
     */
    public static boolean isIceCreamSandwichOrLater() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }


    /**
     * API level 16
     */
    public static boolean isJellyBeanOrLater() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN;
    }

    /**
     * API level 19
     */
    public static boolean isKitkatOrLater() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT;
    }

    /**
     * API level 21
     */
    public static boolean isLOLLIPOPOrLater() {
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }
}
