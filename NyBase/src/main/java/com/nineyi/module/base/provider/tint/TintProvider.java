package com.nineyi.module.base.provider.tint;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/8/22.
 */

public class TintProvider implements ITintProvider {

    private static volatile TintProvider sInstance;

    private ITintProvider mProvider;

    public void init(ITintProvider provider) {
        this.mProvider = provider;
    }

    public static TintProvider getInstance() {
        if (sInstance == null) {
            synchronized (TintProvider.class) {
                if (sInstance == null) {
                    sInstance = new TintProvider();
                }
            }
        }

        return sInstance;
    }

    @Override
    public void setBackGroundTint(View v, int pressedColor, int normalColor) {
        mProvider.setBackGroundTint(v, pressedColor, normalColor);
    }

    @Override
    public void setToolbarTint(Toolbar toolbar) {
        mProvider.setToolbarTint(toolbar);
    }

    @Override
    public Drawable getTintDrawable(Drawable d, int pressColor, int normalColor) {
        return mProvider.getTintDrawable(d, pressColor, normalColor);
    }
}
