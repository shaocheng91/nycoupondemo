package com.nineyi.module.base.views.multiadapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class MultiViewHolder<T> extends RecyclerView.ViewHolder {
    @Nullable private OnViewHolderClickListener mOnItemClickListener;
    public MultiViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onClick(MultiViewHolder.this.getAdapterPosition());
                }
            }
        });
    }

    public abstract void onBind(T element, int position);


    public void setOnItemClickListener(OnViewHolderClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    interface OnViewHolderClickListener {
        void onClick(int position);
    }
}