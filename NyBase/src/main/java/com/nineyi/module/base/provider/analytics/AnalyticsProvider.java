package com.nineyi.module.base.provider.analytics;


import android.content.Context;


/**
 * Created by ReeceCheng on 2017/8/17.
 */

public class AnalyticsProvider implements IAnalyticsProvider {

    private static volatile AnalyticsProvider sInstance;

    private IAnalyticsProvider mProvider;

    public void init(IAnalyticsProvider provider) {
        this.mProvider = provider;
    }

    public static AnalyticsProvider getInstance() {
        if (sInstance == null) {
            synchronized (AnalyticsProvider.class) {
                if (sInstance == null) {
                    sInstance = new AnalyticsProvider();
                }
            }
        }

        return sInstance;
    }


    @Override
    public void clearScreenName() {
        mProvider.clearScreenName();
    }

    @Override
    public void setScreenNameWithPageTitle(String screenName, String id, String name) {
        mProvider.setScreenNameWithPageTitle(screenName, id, name);
    }

    @Override
    public void sendScreenView(String screenName) {
        mProvider.sendScreenView(screenName);
    }

    @Override
    public void sendEventHit(String category, String action, String label) {
        mProvider.sendEventHit(category, action, label);
    }

    @Override
    public void sendEventHit(String category, String action) {
        mProvider.sendEventHit(category, action);
    }

    @Override
    public void sendTimingHit(String category, String name, String label, Long loadTime) {
        mProvider.sendTimingHit(category, name, label, loadTime);
    }

    @Override
    public void sendFacebookRegisterEvent(Context context) {
        mProvider.sendFacebookRegisterEvent(context);
    }

    @Override
    public void send91RegisterEvent(Context context) {
        mProvider.send91RegisterEvent(context);
    }
}
