package com.nineyi.module.base.menu;

/**
 * Created by ReeceCheng on 2017/8/17.
 */

public interface IShoppingCartActionProvider {
    void refreshCartCount();
}
