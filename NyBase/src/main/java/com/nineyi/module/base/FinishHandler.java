package com.nineyi.module.base;

/**
 * Created by tedliang on 2016/6/20.
 */
public interface FinishHandler {
    void finish();
}
