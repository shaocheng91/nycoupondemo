package com.nineyi.module.base.config;


public class NineYiLocalConfig {

    public static final int MALL_SHOP_ID = 0;
    public static final long HTTP_CACHE_MB_SIZE = 50 * 1024 * 1024;
    public static final String NINEYI_GCM_SENDER_ID = "101545754833";
    public static final int BROWSW_RECORD_LIMITATION = 20;
    public static final String HTTP_CACHE_DIR_NAME = "http";
    public static final String HYPERLINK_PATTERN
            = "\\b(https?|http)://[-a-zA-Z0-9\u4e00-\u9fa5+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9\u4e00-\u9fa5+&@#/%=~_|]";
    public static final String ALL_NUM = "\\d+";
}
