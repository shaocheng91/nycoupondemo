package com.nineyi.module.base.utils;

import com.nineyi.module.base.component.IReplaceImgManager;


/**
 * Created by tedliang on 2017/10/20.
 */

public class UrlReplaceImgManager implements IReplaceImgManager{

    @Override
    public String replace(String url) {
        return url.replace("/t/", "/o/");
    }
}
