package com.nineyi.module.base.ui.countdown;

/**
 * Created by ReeceCheng on 2017/10/12.
 */

public interface CountdownObserver {
    void countdown(long systemTime);
}
