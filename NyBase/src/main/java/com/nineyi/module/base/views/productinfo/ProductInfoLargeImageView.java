package com.nineyi.module.base.views.productinfo;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;


public class ProductInfoLargeImageView extends ImageView {

    public ProductInfoLargeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        float xPad = (float) (getPaddingLeft() + getPaddingRight());
        int widthSize = Math.round(MeasureSpec.getSize(widthMeasureSpec) - xPad);
        int heightSize = widthSize;

        setMeasuredDimension(widthSize, heightSize);
    }
}
