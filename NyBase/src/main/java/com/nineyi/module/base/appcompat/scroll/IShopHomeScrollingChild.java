package com.nineyi.module.base.appcompat.scroll;

/**
 * Created by ReeceCheng on 2017/1/6.
 */
public interface IShopHomeScrollingChild {
    void adjustScroll(int headerHeight, float translationY, boolean isHeaderExpanded);
    void setScrollingParent(IShopHomeScrollingParent scrolling);
    void onHeaderTransition(int transition);
    void setPagePosition(int position);
}
