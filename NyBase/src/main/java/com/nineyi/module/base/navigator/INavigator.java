package com.nineyi.module.base.navigator;


import android.content.Context;


public interface INavigator {

    void doNavigation(Context hostActivity);

    boolean shouldNavigateAfterGetFocus();

    INavigator shouldNavigateAfterHasFocus(boolean shouldNavigateAfterHasFocus);

    String getTargetName();

}
