/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nineyi.module.base.toolbartab;

import com.nineyi.module.base.ui.NineYiColor;
import com.nineyi.module.base.ui.UiUtils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import con.nineyi.module.base.R;


/**
 * To be used with ViewPager to provide a tab indicator component which give constant feedback as to
 * the user's scroll progress.
 * <p>
 * To use the component, simply add it to your view hierarchy. Then in your
 * {@link android.app.Activity} or {@link android.support.v4.app.Fragment} call
 * {@link #setViewPager(android.support.v4.view.ViewPager)} providing it the ViewPager this layout is being used for.
 * <p>
 * The colors can be customized in two ways. The first and simplest is to provide an array of colors
 * via {@link #setSelectedIndicatorColors(int...)} and {@link #setDividerColors(int...)}. The
 * alternative is via the {@link com.nineyi.toolbartab.SlidingTabLayout.TabColorizer} interface which provides you
 * complete control over
 * which color is used for any individual position.
 * <p>
 * The views used as tabs can be customized by calling {@link #setCustomTabView(int, int)},
 * providing the layout ID of your custom layout.
 */
public class SlidingTabLayout extends HorizontalScrollView {

    static final int DEFAULT_SELECTED_INDICATOR_COLOR = NineYiColor.getGlobalTabIndicatorColor();

    private static final int DEFAULT_BACKGROUND_COLOR = 0xFFFFFFFF;
    private static final int DEFAULT_TAB_DIVIDER_DOT_COLOR = 0xFFE1E1E1;
    private static final int DEFAULT_TAB_SELECTED_TEXT_COLOR_RES = R.color.font_tab_seg_selected;
    private static final int DEFAULT_TAB_UNSELECTED_TEXT_COLOR = 0XFFb2b2b2;
    private static final TextColor DEFAULT_TAB_TEXT_COLORS = new TextColor(DEFAULT_TAB_SELECTED_TEXT_COLOR_RES,
            DEFAULT_TAB_UNSELECTED_TEXT_COLOR);
    private static final int DEFAULT_INDICATOR_HEIGHT_RES = R.dimen.indicatorHeight;
    private static final int DEFAULT_TAB_TEXT_SIZE_RES = R.dimen.indicator_textSize;
    private Margins mMargins = new Margins(0, 0);

    /**
     * Allows complete control over the colors drawn in the tab layout. Set with
     * {@link #setCustomTabColorizer(com.nineyi.toolbartab.SlidingTabLayout.TabColorizer)}.
     */
    public interface TabColorizer {

        /**
         * @return return the color of the indicator used when {@code position} is selected.
         */
        int getIndicatorColor(int position);

        /**
         * @return return the color of the divider drawn to the right of {@code position}.
         */
        int getDividerColor(int position);

    }

    public enum TabIndicatorWidthEnum {
        Full,
        Proportion
    }

    private static final int TITLE_OFFSET_DIPS = 24;
    private static final int TAB_VIEW_TEXT_SIZE_SP = 12;

    private int mTitleOffset;

    private int mTabViewLayoutId;
    private int mTabViewTextViewId;
    private DistributeEnum mDistribute = DistributeEnum.Evenly;
    public enum DistributeEnum {
        Evenly,
        WrapWithMargin
    }

    private ViewPager mViewPager;
    private SparseArray<String> mContentDescriptions = new SparseArray<String>();
    private ViewPager.OnPageChangeListener mViewPagerPageChangeListener;

    private final SlidingTabStrip mTabStrip;
    private OnClickListener mCustomTabClickListener;

    private TextColor[] mTabTextColors;
    private int mIndicatorHeightInPixel;
    private float mTabTextSize;

    public SlidingTabLayout(Context context) {
        this(context, null);
    }

    public SlidingTabLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlidingTabLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // Disable the Scroll Bar
        setHorizontalScrollBarEnabled(false);
        // Make sure that the Tab Strips fills this View
        setFillViewport(true);

        mTitleOffset = (int) (TITLE_OFFSET_DIPS * getResources().getDisplayMetrics().density);

        mTabStrip = new SlidingTabStrip(context);
        parseAttrs(attrs);
        addView(mTabStrip, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    private void parseAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NySlidingTab);

        int defaultSelectedIndicatorColor = DEFAULT_SELECTED_INDICATOR_COLOR;
        int defaultDividerColor = DEFAULT_TAB_DIVIDER_DOT_COLOR;
        int defaultTabSelectedTextColor = getResources().getColor(DEFAULT_TAB_SELECTED_TEXT_COLOR_RES);
        int defaultTabUnSelectedTextColor = DEFAULT_TAB_UNSELECTED_TEXT_COLOR;
        int defaultIndicatorHeight = UiUtils.convertDimenResToPixel(DEFAULT_INDICATOR_HEIGHT_RES);
        float defaultTabTextSize = getResources().getDimension(DEFAULT_TAB_TEXT_SIZE_RES);

        setSelectedIndicatorColors(typedArray.getColor(R.styleable.NySlidingTab_nstIndicatorColor, defaultSelectedIndicatorColor));
        setDividerColors(typedArray.getColor(R.styleable.NySlidingTab_nstDividerDotColor, defaultDividerColor));

        mTabTextColors = new TextColor[1];
        int selectedColor = typedArray
                .getColor(R.styleable.NySlidingTab_nstTabTextSelectedColor, defaultTabSelectedTextColor);
        int unSelectedColor = typedArray.getColor(R.styleable.NySlidingTab_nstTabTextUnSelectedColor, defaultTabUnSelectedTextColor);

        mTabTextColors[0] = new TextColor(selectedColor, unSelectedColor);

        mIndicatorHeightInPixel = typedArray.getDimensionPixelSize(R.styleable.NySlidingTab_nstIndicatorHeight, defaultIndicatorHeight);

        mTabStrip.setSelectedIndicatorThickness(mIndicatorHeightInPixel);

        mTabTextSize = typedArray.getDimension(R.styleable.NySlidingTab_nstTabTextSize, defaultTabTextSize);

        if (getBackground() == null) {
            setBackgroundColor(DEFAULT_BACKGROUND_COLOR);
        }

        if (typedArray != null) {
            typedArray.recycle();
        }
    }

    /**
     * Set the custom {@link TabColorizer} to be used.
     *
     * If you only require simple custmisation then you can use
     * {@link #setSelectedIndicatorColors(int...)} to achieve
     * similar effects.
     */
    public void setCustomTabColorizer(TabColorizer tabColorizer) {
        mTabStrip.setCustomTabColorizer(tabColorizer);
    }

    public void setEvenly(){
        mDistribute = DistributeEnum.Evenly;
    }

    public void setWrapWithMargin(int leftMargin, int rightMargin){
        mDistribute = DistributeEnum.WrapWithMargin;
        mMargins = new Margins(leftMargin, rightMargin);
    }

    /**
     * Sets the colors to be used for indicating the selected tab. These colors are treated as a
     * circular array. Providing one color will mean that all tabs are indicated with the same color.
     */
    public void setSelectedIndicatorColors(int... colors) {
        mTabStrip.setSelectedIndicatorColors(colors);
    }

    public void setSelectedIndicatorWidth(TabIndicatorWidthEnum indicatorType) {
        mTabStrip.setSelectedIndicatorWidth(indicatorType);
    }

    /**
     * Sets the colors to be used for tab dividers. These colors are treated as a circular array.
     * Providing one color will mean that all tabs are indicated with the same color.
     */
    public void setDividerColors(int... colors) {
        mTabStrip.setDividerColors(colors);
    }

    /**
     * Set the {@link android.support.v4.view.ViewPager.OnPageChangeListener}. When using {@link
     * com.nineyi.toolbartab.SlidingTabLayout} you are
     * required to set any {@link android.support.v4.view.ViewPager.OnPageChangeListener} through this method. This is
     * so
     * that the layout can update it's scroll position correctly.
     *
     * @see android.support.v4.view.ViewPager#setOnPageChangeListener(android.support.v4.view.ViewPager.OnPageChangeListener)
     */
    public void setOnPageChangeListener(ViewPager.OnPageChangeListener listener) {
        mViewPagerPageChangeListener = listener;
    }

    /**
     * Set the custom layout to be inflated for the tab views.
     *
     * @param layoutResId Layout id to be inflated
     * @param textViewId  id of the {@link android.widget.TextView} in the inflated view
     */
    public void setCustomTabView(int layoutResId, int textViewId) {
        mTabViewLayoutId = layoutResId;
        mTabViewTextViewId = textViewId;
    }

    public void setCustomTabClickListener(OnClickListener listener) {
        mCustomTabClickListener = listener;
    }

    /**
     * Sets the associated view pager. Note that the assumption here is that the pager content
     * (number of tabs and tab titles) does not change after this call has been made.
     */
    public void setViewPager(ViewPager viewPager) {
        mTabStrip.removeAllViews();

        mViewPager = viewPager;
        if (viewPager != null) {
            viewPager.setOnPageChangeListener(new InternalViewPagerListener());
            populateTabStrip();
        }
    }

    /**
     * Create a default view to be used for tabs. This is called if a custom tab view is not set via
     * {@link #setCustomTabView(int, int)}.
     */
    protected TextView createDefaultTabView(Context context) {
        TextView textView = new TextView(context);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, TAB_VIEW_TEXT_SIZE_SP);
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // If we're running on Honeycomb or newer, then we can use the Theme's
            // selectableItemBackground to ensure that the View has a pressed state
            TypedValue outValue = new TypedValue();
            getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
            textView.setBackgroundResource(outValue.resourceId);
        }

        return textView;
    }

    private void populateTabStrip() {
        final PagerAdapter adapter = mViewPager.getAdapter();
        final OnClickListener tabClickListener = new TabClickListener();

        for (int i = 0; i < adapter.getCount(); i++) {
            View tabView = null;
            TextView tabTitleView = null;

            if (mTabViewLayoutId != 0) {
                // If there is a custom tab view layout id set, try and inflate it
                tabView = LayoutInflater.from(getContext()).inflate(mTabViewLayoutId, mTabStrip, false);
                tabTitleView = (TextView) tabView.findViewById(mTabViewTextViewId);
            }

            if (tabView == null) {
                tabView = createDefaultTabView(getContext());
            }

            if (tabTitleView == null && TextView.class.isInstance(tabView)) {
                tabTitleView = (TextView) tabView;
            }

            switch (mDistribute) {
                case Evenly:
                    tabView.setLayoutParams(createEvenlyParams(tabView));
                    break;
                case WrapWithMargin:
                    tabView.setLayoutParams(createWrapWithMarginParams(tabView));
                    break;
                default:
                    tabView.setLayoutParams(createEvenlyParams(tabView));
                    break;
            }

            tabTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTabTextSize);
            tabTitleView.setText(adapter.getPageTitle(i));
            if (i == 0) {
                tabTitleView.setTextColor(getTabTextColorAt(i).selectedColor);
                tabTitleView.setTypeface(Typeface.DEFAULT_BOLD);
            } else {
                tabTitleView.setTextColor(getTabTextColorAt(i).unSelectedColor);
            }

            tabView.setOnClickListener(tabClickListener);
            String desc = mContentDescriptions.get(i, null);
            if (desc != null) {
                tabView.setContentDescription(desc);
            }

            mTabStrip.addView(tabView);
        }
    }

    @NonNull
    private LinearLayout.LayoutParams createEvenlyParams(View tabView) {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tabView.getLayoutParams();
        if (lp == null) {
            lp = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
        }
        lp.width = 0;
        lp.weight = 1;
        return lp;
    }

    @NonNull
    private LinearLayout.LayoutParams createWrapWithMarginParams(View tabView) {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tabView.getLayoutParams();
        if (lp == null) {
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        }
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.leftMargin = mMargins.leftMargin;
        lp.rightMargin = mMargins.rightMargin;
        return lp;
    }

    public void setContentDescription(int i, String desc) {
        mContentDescriptions.put(i, desc);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (mViewPager != null) {
            scrollToTab(mViewPager.getCurrentItem(), 0);
        }
    }

    private void scrollToTab(int tabIndex, int positionOffset) {
        final int tabStripChildCount = mTabStrip.getChildCount();
        if (tabStripChildCount == 0 || tabIndex < 0 || tabIndex >= tabStripChildCount) {
            return;
        }

        View selectedChild = mTabStrip.getChildAt(tabIndex);
        if (selectedChild != null) {
            int targetScrollX = selectedChild.getLeft() + positionOffset;

            if (tabIndex > 0 || positionOffset > 0) {
                // If we're not at the first child and are mid-scroll, make sure we obey the offset
                targetScrollX -= mTitleOffset;
            }

            scrollTo(targetScrollX, 0);
        }
    }

    public void setSelectedIndicatorThickness(int height) {
        mIndicatorHeightInPixel = height;
        mTabStrip.setSelectedIndicatorThickness(height);
    }

    public void setSelectedIndicatorBottomBorderThinkness(int height) {
        mTabStrip.setBottomBorderThinkness(height);
    }

    /*
     * slidingTabLayout.setTabTextColors(
     *    r.getColor(R.color.cart_red),
     *    r.getColor(R.color.black));
    * */
    public void setTabTextColors(TextColor... colors) {
        mTabTextColors = null;
        mTabTextColors = colors;
    }

    private TextColor getTabTextColorAt(int position) {
        if (position < 0) {
            return SlidingTabLayout.DEFAULT_TAB_TEXT_COLORS;
        }
        if (mTabTextColors == null) {
            return SlidingTabLayout.DEFAULT_TAB_TEXT_COLORS;
        }
        if (position >= mTabTextColors.length) {
            return mTabTextColors[mTabTextColors.length - 1]; // last color in mTabTextColors[]
        }

        return mTabTextColors[position];
    }

    public int getTabCount() {
        return mTabStrip.getChildCount();
    }

    private class InternalViewPagerListener implements ViewPager.OnPageChangeListener {

        private int mScrollState;

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            int tabStripChildCount = mTabStrip.getChildCount();
            if ((tabStripChildCount == 0) || (position < 0) || (position >= tabStripChildCount)) {
                return;
            }

            mTabStrip.onViewPagerPageChanged(position, positionOffset);

            View selectedTitle = mTabStrip.getChildAt(position);
            int extraOffset = (selectedTitle != null) ? (int) (positionOffset * selectedTitle.getWidth()) : 0;
            scrollToTab(position, extraOffset);

            if (mViewPagerPageChangeListener != null) {
                mViewPagerPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            mScrollState = state;

            if (mViewPagerPageChangeListener != null) {
                mViewPagerPageChangeListener.onPageScrollStateChanged(state);
            }
        }

        @Override
        public void onPageSelected(int position) {
            if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                mTabStrip.onViewPagerPageChanged(position, 0f);
                scrollToTab(position, 0);
            }
            for (int i = 0, size = mViewPager.getAdapter().getCount(); i < size; i++) {
                if (i == position) {
                    setTabTextColor(i, getTabTextColorAt(i).selectedColor);
                    getTabTextView(i).setTypeface(Typeface.DEFAULT_BOLD);
                } else {
                    setTabTextColor(i, getTabTextColorAt(i).unSelectedColor);
                    getTabTextView(i).setTypeface(Typeface.DEFAULT);
                }
            }
            if (mViewPagerPageChangeListener != null) {
                mViewPagerPageChangeListener.onPageSelected(position);
            }
        }

    }

    private class TabClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            for (int i = 0; i < mTabStrip.getChildCount(); i++) {
                if (v == mTabStrip.getChildAt(i)) {
                    mViewPager.setCurrentItem(i);
                    break;
                }
            }

            if (mCustomTabClickListener != null) {
                mCustomTabClickListener.onClick(v);
            }
        }
    }

    public void setTabText(String text, int position) {
        if (mTabStrip == null) {
            return;
        }

        int childCount = mTabStrip.getChildCount();
        if (position > childCount) {
            return;
        }

        TextView targetTextView = getTabTextView(position);

        targetTextView.setText(text);
    }

    public final CharSequence getTabText(int position) {
        return getTabTextView(position).getText();
    }

    private final TextView getTabTextView(int position) {
        if (mTabViewLayoutId != 0) {
            return (TextView) mTabStrip.getChildAt(position).findViewById(mTabViewTextViewId);
        } else {
            return (TextView) mTabStrip.getChildAt(position);
        }
    }

    public final View getTabView(int position) {
        return mTabStrip.getChildAt(position);
    }

    private final void setTabTextColor(int position, int color) {
        getTabTextView(position).setTextColor(color);
    }

    public static final class TextColor {

        int selectedColor;
        int unSelectedColor;

        public TextColor(int selectedColor, int unSelectedColor) {
            this.selectedColor = selectedColor;
            this.unSelectedColor = unSelectedColor;
        }
    }

    private static class Margins {
        public int leftMargin;

        public Margins(int _leftMargin, int _rightMargin) {
            this.leftMargin = _leftMargin;
            this.rightMargin = _rightMargin;
        }

        public int rightMargin;
    }
}
