package com.nineyi.module.base.appcompat;

import com.nineyi.module.base.retrofit.RetrofitActionBarFragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import con.nineyi.module.base.R;


/**
 * Created by tedliang on 2016/9/13.
 */
public abstract class PullToRefreshFragmentV3 extends RetrofitActionBarFragment implements
        SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mPullToRefreshLayout;

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return mPullToRefreshLayout;
    }

    @Override
    public void onDestroyView() {
        mPullToRefreshLayout = null;
        super.onDestroyView();
    }

    protected SwipeRefreshLayout createPullRefreshLayout(LayoutInflater inflater, ViewGroup container) {
        mPullToRefreshLayout = (SwipeRefreshLayout) inflater.inflate(R.layout.swipe_refresh_widget, container, false);
        return mPullToRefreshLayout;
    }

    protected void setSwipRefreshLayout(SwipeRefreshLayout layout) {
        mPullToRefreshLayout = layout;
    }

    protected void setupPullRefreshLayout() {
        mPullToRefreshLayout.setColorSchemeColors(getContext().getResources().getColor(R.color.bg_common_pullrefresh));
        mPullToRefreshLayout.setOnRefreshListener(this);
    }

    protected void stopPullRefreshing(){
        mPullToRefreshLayout.setRefreshing(false);
    }
}
