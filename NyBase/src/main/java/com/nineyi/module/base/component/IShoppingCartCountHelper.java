package com.nineyi.module.base.component;

/**
 * Created by tedliang on 2017/10/18.
 */
public interface IShoppingCartCountHelper {
    interface OnItemCountCallListener {
        void onSuccess();
    }
    void callShoppingCartItemCount(OnItemCountCallListener onItemCountCallListener);
}
