package com.nineyi.module.base.views;

import com.nineyi.module.base.FavoriteHelper;
import com.nineyi.module.base.events.StringEvents;
import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.retrofit.NineYiApiClient;
import com.nineyi.module.base.retrofit.NySubscriber;
import com.nineyi.module.base.ui.BaseTintUtils;
import com.nineyi.module.base.utils.MsgUtils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import con.nineyi.module.base.R;
import de.greenrobot.event.EventBus;


/**
 * Created by ReeceCheng on 2015/2/6.
 */
public class FavoritePopBox extends FrameLayout
        implements View.OnClickListener, Animator.AnimatorListener {

    public static final int FAVORITE_MODE_SALEPAGE = 0;

    private Context mContext;

    private ImageView mHeart;
    private ImageView mHeartBorder;

    private static final int DEFAULT_DURATION_TIME = 500;
    private static final int ACTION_FILL_COLOR = 0;
    private static final int ACTION_UN_FILL_COLOR = 1;
    private int mAction;

    private AnimatorSet mAnimatorSet;

    private FavoriteHelper mFavHelper;

    private int mMode;
    private int mId;
    private boolean mIsChecked = false;

    ObjectAnimator xAnimator, yAnimator;

    public interface ExternalClickListener {

        void onClick(View v, boolean isChecked);
    }

    private ExternalClickListener mExternalClickListener = null;

    public FavoritePopBox(Context context) {
        this(context, null);
    }

    public FavoritePopBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoritePopBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FavoritePopBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mContext = context;
        init();
    }

    private void init() {
        setOnClickListener(this);

        mHeartBorder = new ImageView(mContext);
        mHeart = new ImageView(mContext);

        this.addView(mHeartBorder);
        this.addView(mHeart);

        mHeartBorder.setBackgroundResource(R.drawable.btn_item_fav);
        mHeart.setBackgroundResource(R.drawable.btn_item_fav_selected);

        mHeartBorder.setVisibility(View.VISIBLE);
        mHeart.setVisibility(View.GONE);

        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.addListener(this);

        xAnimator = ObjectAnimator.ofFloat(mHeartBorder, "ScaleX", 0.8f, 1.2f, 1);
        yAnimator = ObjectAnimator.ofFloat(mHeartBorder, "ScaleY", 0.8f, 1.2f, 1);

        mFavHelper = new FavoriteHelper(mContext);

        setFavoritePopBoxColorDefault();
    }

    private void setFavoritePopBoxColorDefault(){
        setFavoritePopBoxColor(ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_fav_selected),
                ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_fav));
    }

    public void setFavoritePopBoxColor(int selectedColor, int unSelectedColor) {
        BaseTintUtils.setBackGroundTint(mHeart, selectedColor, selectedColor);
        BaseTintUtils.setBackGroundTint(mHeartBorder, unSelectedColor, unSelectedColor);
    }

    private void setAnimation(int action) {
        mAction = action;

        if (action == ACTION_FILL_COLOR) {
            mAnimatorSet.setDuration(DEFAULT_DURATION_TIME);
            mAnimatorSet.setInterpolator(null);

            xAnimator.setInterpolator(new AnticipateOvershootInterpolator());
            yAnimator.setInterpolator(new AnticipateOvershootInterpolator());

            mAnimatorSet.playTogether(ObjectAnimator.ofFloat(mHeart, "alpha", 0, 0, 1),
                    ObjectAnimator.ofFloat(mHeart, "ScaleX", 1, 1, 1),
                    ObjectAnimator.ofFloat(mHeart, "ScaleY", 1, 1, 1), ObjectAnimator.ofFloat(mHeartBorder, "alpha", 1),
                    xAnimator, yAnimator);

        } else {
            mAnimatorSet.setDuration(DEFAULT_DURATION_TIME);
            mAnimatorSet.setInterpolator(new DecelerateInterpolator());

            mAnimatorSet.playTogether(ObjectAnimator.ofFloat(mHeart, "alpha", 1, 0),
                    ObjectAnimator.ofFloat(mHeart, "ScaleX", 1, 0), ObjectAnimator.ofFloat(mHeart, "ScaleY", 1, 0),
                    ObjectAnimator.ofFloat(mHeartBorder, "alpha", 0, 1), ObjectAnimator.ofFloat(this, "ScaleX", 1, 1),
                    ObjectAnimator.ofFloat(this, "ScaleY", 1, 1));
        }

    }

    @Override
    public void onClick(View v) {
        if (mAnimatorSet.isRunning()) {
            return;
        }

        if (mExternalClickListener != null) {
            mExternalClickListener.onClick(this, mIsChecked);
        }

        changeCheckedState();
        doClick();
    }

    private void changeCheckedState() {
        setChecked(!mIsChecked, true, false);
    }

    private void doClick() {
        if (mMode == FAVORITE_MODE_SALEPAGE) {
            onChangeSalePage(this.isChecked());
        }
    }

    private void runAnimation(boolean isChecked) {
        if (mAnimatorSet.isRunning()) {
            return;
        }

        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.addListener(this);

        if (isChecked) {
            setAnimation(ACTION_FILL_COLOR);
            mAnimatorSet.start();
        } else {
            setAnimation(ACTION_UN_FILL_COLOR);
            mAnimatorSet.start();
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {

        if (mAction == ACTION_FILL_COLOR) {
            mHeart.setVisibility(View.VISIBLE);
            mHeartBorder.setVisibility(View.VISIBLE);
        } else {
            mHeart.setVisibility(View.VISIBLE);
            mHeartBorder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(Animator animation) {

        if (mAction == ACTION_FILL_COLOR) {
            mHeart.setVisibility(View.VISIBLE);
            mHeartBorder.setVisibility(View.GONE);
        } else {
            mHeart.setVisibility(View.GONE);
            mHeartBorder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked, boolean isRunAnimation, boolean isInit) {
        this.mIsChecked = checked;

        if (isRunAnimation) {
            // 初始化時check == false不跑動畫
            if (isInit) {
                if (checked) {
                    runAnimation(checked);
                } else {
                    changeImage(checked);
                }
            } else {
                runAnimation(checked);
            }
        } else {
            changeImage(checked);
        }
    }

    private void changeImage(boolean checked) {
        if (checked) {
            mHeartBorder.setVisibility(View.GONE);
            mHeart.setVisibility(View.VISIBLE);
            mHeart.setScaleX(1);
            mHeart.setScaleY(1);
            mHeartBorder.setAlpha(0f);
            mHeart.setAlpha(1f);
        } else {
            mHeart.setVisibility(View.GONE);
            mHeartBorder.setVisibility(View.VISIBLE);
            mHeart.setAlpha(0f);
            mHeartBorder.setAlpha(1f);
        }
    }

    public void setMode(int mode) {
        this.mMode = mode;
    }

    /**
     * Set id and check state, for salepage.
     */
    public void setCheckedWithId(int id, boolean isRunAnimation) {
        this.mId = id;

        if (mMode == FAVORITE_MODE_SALEPAGE) {
            setChecked(mFavHelper.isFavorite(id), isRunAnimation, true);
        }
    }

    private void onChangeSalePage(boolean isChecked) {
        if (isChecked) {
            // Add fav
            callAddFav(mId);
        } else {
            // Remove fav
            callRemoveFav(mId);
        }
    }

    private void callAddFav(final int id) {
        NineYiApiClient.getTraceSalePageInsertItem(id, ApplicationProvider.getInstance().getProviderAppBuildConfig().shopId).subscribeWith(new NySubscriber<String>() {
            @Override
            public void onNext(String s) {
                // local saving
                mFavHelper.add(id);
                MsgUtils.showShortToast(getContext(), getContext().getString(R.string.toast_favorite_add_success));
                EventBus.getDefault().post(StringEvents.EVENT_ON_TRACE_ITEM_ADD_OR_REMOVE_SUCCESS);
            }
        });
    }

    private void callRemoveFav(final int id) {
        NineYiApiClient.deleteItem(id, ApplicationProvider.getInstance().getProviderAppBuildConfig().shopId).subscribeWith(new NySubscriber<String>() {
            @Override
            public void onNext(String aBoolean) {
                // local saving
                mFavHelper.remove(id);

                MsgUtils.showShortToast(getContext(), getContext().getString(R.string.toast_favorite_cancel));
                EventBus.getDefault().post(StringEvents.EVENT_ON_TRACE_ITEM_ADD_OR_REMOVE_SUCCESS);
            }
        });
    }

    public void setExternalOnClickListener(ExternalClickListener listener) {
        mExternalClickListener = listener;
    }

}