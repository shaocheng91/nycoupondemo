package com.nineyi.module.base.appcompat.scroll;

import android.support.v4.view.ScrollingView;


/**
 * Created by ReeceCheng on 2017/1/23.
 */
public interface IShopHomeScrollingParent {
    void onScroll(ScrollingView view, int pagePosition, int headerHeight, int dy);
}
