package com.nineyi.module.base.helper;

import android.net.Uri;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class YoutubeVideoHelper {
    
    private final static String YOUTUBE_EMBED = "(https|http)://www.youtube.com/embed/(.*[^/])/{0,1}";  //https://www.youtube.com/embed/58NX8iSuVPs
    private final static String YOUTUBE_EMBED_PATH = "/embed/(.*[^/])/{0,1}";  // /embed/58NX8iSuVPs

    private final static String YOUTUBE_WATCH = "(https|http)://www.youtube.com/watch\\?v=(.*[^/])/{0,1}"; // http://www.youtube.com/watch?v=wBlCOe1bN6E

    public String getVideoId(String url) {
        Uri uri = Uri.parse(url);

        String videoId = "";
        if (uri.getHost() != null && url.matches(YOUTUBE_WATCH)) {
            videoId = uri.getQueryParameter("v");
        } else if(uri.getHost() != null && url.matches(YOUTUBE_EMBED)){
            Pattern p = Pattern.compile(YOUTUBE_EMBED_PATH);
            Matcher m = p.matcher(uri.getPath());

            while(m.find()){
                videoId = m.group(1);
            }
        }
        else if (uri.getHost() != null && uri.getHost().contains("youtu.be")) {
            videoId = uri.getPath().replace("/", "");
        }

        return videoId;
    }

    public String getThumbnail(String url) {
        return String.format("http://img.youtube.com/vi/%s/0.jpg", getVideoId(url));
    }

    public boolean isYoutubeVideo(String url) {
        Uri uri = Uri.parse(url);
        if (uri.getHost().indexOf("youtube") >= 0) {
            return true;
        }

        return uri.getHost().indexOf("youtu.be") >= 0;
    }
}