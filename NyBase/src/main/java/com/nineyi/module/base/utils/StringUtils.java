package com.nineyi.module.base.utils;

import com.nineyi.module.base.provider.application.ApplicationProvider;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import con.nineyi.module.base.R;


public class StringUtils {

    /**
     * 取得價錢digit的長度 例如 getPriceDigitLength("123,456",6) 回傳結果為7
     *
     * @param obj       比對字串
     * @param minlength 比對的數字數量
     * @return 字串長度
     */
    public static int getPriceDigitLength(Object obj, int minlength) {

        int result = minlength;
        String str = obj.toString();
        if (str.length() > minlength) {
            int number = 0;
            for (int i = 0; i < str.length(); i++) {
                if (android.text.TextUtils.isDigitsOnly(Character.toString(str.charAt(i)))) {
                    number++;
                }

                result = i + 1;
                if (number == minlength) {
                    break;
                }
            }

        }

        return result;
    }

    public static String truncatePrice(Object obj, int maxLength) {
        int length = getPriceDigitLength(obj, maxLength);
        return truncate(obj, length, null);
    }

    public static String truncate(Object obj, int maxLength, String suffix) {
        if (obj == null || maxLength == 0) {
            return "";
        }
        String str = obj.toString();
        if (str.length() <= maxLength) {
            return str;
        }
        if (suffix == null) {
            suffix = ApplicationProvider.getInstance().getProviderAppResources().getString(R.string.more_text);
        }
        return str.substring(0, maxLength) + suffix;
    }

    public static boolean isNullOrSpace(String str) {
        return str == null || "".equals(str.trim());
    }

    public static String formatNumber(Object obj) {
        // The rules of pattern look this:
        // https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html
        return formatNumber(obj, "0.0");
    }

    public static String formatNumber(Object obj, String pattern) {
        NumberFormat formatter = new DecimalFormat(pattern);
        return formatter.format(obj);
    }

    public static String getText(String txt, String defaultValue) {
        if (isNullOrSpace(txt)) {
            return defaultValue;
        }

        return txt;
    }

    public static String getText(String txt) {
        return getText(txt, "");
    }

    public static String getText(Number number, String defaultValue) {
        if (number.intValue() == 0) {
            return "";
        }

        return defaultValue;
    }

    public static boolean isEquals(String str, String pattern) {
        return pattern.equals(str);
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isValidPhone(CharSequence target, Context ctx) {

        if (target == null) {
            return false;
        } else {
            return target.toString().matches(ctx.getString(R.string.user_login_id_validate));
        }
    }

    public final static boolean isValidIdentityCard(CharSequence target, Context ctx) {
        if (target == null) {
            return false;
        } else {
            return target.toString().matches("[a-zA-Z]{1}\\d{9}");
        }
    }

    public final static boolean isValidPassword(CharSequence charSequence){
        if(charSequence == null){
            return false;
        } else {
            return charSequence.toString().matches("[A-Za-z0-9]{6,20}");
        }
    }

    public static Spanned DotStringParser(List<String> list) {
        String result = "";
        for (int i = 0; i < list.size(); i++) {
            result += "&#8226;  ";
            result += list.get(i);
            result += "<br/>";
        }
        return Html.fromHtml(result);

    }

}
