package com.nineyi.module.base.ui;

import con.nineyi.module.base.R;

import com.nineyi.module.base.provider.application.ApplicationProvider;
import android.graphics.Color;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by tedliang on 2016/3/15.
 */
public class NineYiColor {
    private static final String SUPPLEMENTARY_COLOR = "SupplementaryColor";
    public static final String PRIMARY_COLOR = "PrimaryColor";
    public static final String GLOBAL_BODY_BACKGROUND_COLOR = "GlobalBodyBackgroundColor";
    public static final String GLOBAL_BODY_FONT_COLOR = "GlobalBodyFontColor";
    public static final String GLOBAL_CATEGORY_TREE_BACKGROUND_COLOR = "GlobalCategoryTreeBackgroundColor";
    public static final String GLOBAL_CATEGORY_TREE_L1_FONT_COLOR = "GlobalCategoryTreeL1FontColor";
    public static final String GLOBAL_CATEGORY_TREE_L2_FONT_COLOR = "GlobalCategoryTreeL2FontColor";
    public static final String GLOBAL_HEADER_BACKGROUND_COLOR = "GlobalHeaderBackgroundColor";
    public static final String GLOBAL_HEADER_LINK_FONT_COLOR = "GlobalHeaderLinkFontColor";
    public static final String GLOBAL_NAVI_ICON_COLOR = "GlobalNaviIconColor";
    public static final String GLOBAL_PRIMARY_COLOR = "GlobalPrimaryColor";
    public static final String GLOBAL_SECTION_HEADER_BACKGROUND_COLOR = "GlobalSectionHeaderBackgroundColor";
    public static final String GLOBAL_SECTION_HEADER_FONT_COLOR = "GlobalSectionHeaderFontColor";
    public static final String GLOBAL_SECTION_HEADER_LINK_FONT_COLOR = "GlobalSectionHeaderLinkFontColor";
    public static final String GLOBAL_SIDE_MENU_BACKGROUND_COLOR = "GlobalSideMenuBackgroundColor";
    public static final String GLOBAL_SIDE_MENU_FONT_COLOR = "GlobalSideMenuFontColor";
    public static final String GLOBAL_SIDE_MENU_ICON_COLOR = "GlobalSideMenuIconColor";
    public static final String GLOBAL_SIDE_MENU_SELECTED_FONT_COLOR = "GlobalSideMenuSelectedFontColor";
    public static final String GLOBAL_TABINDICATOR_COLOR = "GlobalTabIndicatorColor";
    public static final String GLOBAL_PRIMARY_ALPHA_FONT_COLOR ="GlobalPrimaryalphaFontColor";
    public static final String MASTER_SETTING_COLOR = "MasterSettingColor";
    public static final String PRIMARY_BUTTON_BACKGROUND_COLOR = "PrimaryButtonBackgroundColor";
    public static final String PRIMARY_BUTTON_FONT_COLOR = "PrimaryButtonFontColor";
    public static final String PRIMARY_BUTTON_HOVER_BACKGROUND_COLOR = "PrimaryButtonHoverBackgroundColor";
    public static final String RELATED_SALES_COLOR = "RelatedSalesColor";
    public static final String SECONDARY_BUTTON_BACKGROUND_COLOR = "SecondaryButtonBackgroundColor";
    public static final String SECONDARY_BUTTON_FONT_COLOR = "SecondaryButtonFontColor";
    public static final String BTN_DISCOUNT = "BtnDiscount";
    public static final String PROMOTION_DISCOUNT_UNSELECTED = "PromotionDiscountUnSelected";
    public static final String GLOBAL_EXPLAIN_FONT_COLOR = "GlobalExplainFontColor";
    public static final String GLOBAL_STYLE_COLOR = "GlobalStyleColor";
    public static final String GRADIENT_COLOR = "GradientColor";
    public static final String GLOBAL_LIGHT_BG_FONT_COLOR = "GlobalLightBGFontColor";
    public static final String GLOBAL_MEMBER_LEVEL_CARD = "GlobalMemberLevelCard";
    public static final String GLOBAL_MEMBER_CARD_ANNOUNCE_COLOR = "GlobalMemberCardAnnounceColor";
    public static final String GLOBAL_MEMBER_CARD_FONT = "GlobalMemberCardFont";
    public static final String GLOBAL_STYLE_COLOR_LIGHT = "GlobalStyleColorLight";
    public static final String GLOBAL_FONT_HIGHLIGHT = "GlobalFontHighLight";
    public static final String GLOBAL_NAVI_STATUS = "GlobalNaviStatus";
    public static final String GLOBAL_PAGER_CONTROLLER_INDICATOR_COLOR = "GlobalPagecontrollerIndicatorColor";

    public static final String TO_BE_CONFIRMED_A = "ToBeConfirmedA";
    public static final String TO_BE_CONFIRMED_B = "ToBeConfirmedB";
    public static final String TO_BE_CONFIRMED_C = "ToBeConfirmedC";
    public static final String TO_BE_CONFIRMED_D = "ToBeConfirmedD";
    public static final String TO_BE_CONFIRMED_E = "ToBeConfirmedE";
    public static final String TO_BE_CONFIRMED_F = "ToBeConfirmedF";
    public static final String TO_BE_CONFIRMED_H = "ToBeConfirmedH";
    public static final String TO_BE_CONFIRMED_I = "ToBeConfirmedI";
    public static final String TO_BE_CONFIRMED_J = "ToBeConfirmedJ";
    public static final String TO_BE_CONFIRMED_K = "ToBeConfirmedK";
    public static final String TO_BE_CONFIRMED_L = "ToBeConfirmedL";
    public static final String TO_BE_CONFIRMED_M = "ToBeConfirmedM";
    public static final String TO_BE_CONFIRMED_N = "ToBeConfirmedN";
    public static final String TO_BE_CONFIRMED_O = "ToBeConfirmedO";
    public static final String TO_BE_CONFIRMED_P = "ToBeConfirmedP";
    public static final String TO_BE_CONFIRMED_Q = "ToBeConfirmedQ";
    public static final String TO_BE_CONFIRMED_R = "ToBeConfirmedR";
    public static final String TO_BE_CONFIRMED_S = "ToBeConfirmedS";
    public static final String TO_BE_CONFIRMED_T = "ToBeConfirmedT";
    public static final String TO_BE_CONFIRMED_U = "ToBeConfirmedU";
    public static final String TO_BE_CONFIRMED_V = "ToBeConfirmedV";
    public static final String TO_BE_CONFIRMED_X = "ToBeConfirmedX";
    public static final String TO_BE_CONFIRMED_Y = "ToBeConfirmedY";
    public static final String TO_BE_CONFIRMED_Z = "ToBeConfirmedZ";

    public static final String ASIDEBAR_PROMOTION_TAG_BACKGROUND_COLOR = "AsidebarPromotionTagBackgroundColor";
    public static final String ASIDEBAR_PROMOTION_TAG_FONT_COLOR = "AsidebarPromotionTagFontColor";
    public static final String SHOPPING_CART_COUNT_BACKGOURND_COLOR = "ShoppingCartCountBackgroundColor";

    public static final String FIXED_COLOR_FF2750 = "FixedColorFF2750";
    public static final String FIXED_COLOR_333333 = "FixedColor333333";
    public static final String FIXED_COLOR_F5F5F5 = "FixedColorF5F5F5";
    public static final String FIXED_COLOR_999999 = "FixedColor999999";
    public static final String FIXED_COLOR_E5E5E5 = "FixedColorE5E5E5";
    public static final String FIXED_COLOR_FCFCFC = "FixedColorFCFCFC";
    public static final String FIXED_COLOR_000000 = "FixedColor000000";
    public static final String FIXED_COLOR_FFFFFF = "FixedColorFFFFFF";
    public static final String FIXED_COLOR_878787 = "FixedColor878787";
    public static final String FIXED_COLOR_FF9933 = "FixedColorFF9933";
    public static final String FIXED_COLOR_B4B4B4 = "FixedColorB4B4B4";
    public static final String FIXED_COLOR_909091 = "FixedColor909091";
    private static final String GLOBAL_BRIGHT_FONT_COLOR = "GlobalBrightFontColor";
    private static final String GLOBAL_BRIGHT_COLOR = "GlobalBrightColor";
    private static final String GLOBAL_FULL_FONT_COLOR = "GlobalFullFontColor";
    private static final String GLOBAL_PRICE_COLOR = "GlobalPriceColor";
    private static final String GLOBAL_CATEGORY_TREE_FONT_COLOR = "GlobalCategoryTreeFontColor";
    private static final String GLOBAL_PAGE_CONTROLLER_INDICAOTR_SELECTED_COLOR = "GlobalItemlistPagecontrollerIndicatorColor";
    private static final String GLOBAL_PAGE_CONTROLLER_INDICAOTR_COLOR = "GlobalItemlistPagecontrollerColor";


    private static HashMap<String, String> mColorMap;

    public static void injectColor(HashMap<String, String> map) {
        mColorMap = map;
    }

    private static int getColorByKey(String key, int nonThemeColor) {
        if(mColorMap != null && mColorMap.containsKey(key)) {
            return Color.parseColor(mColorMap.get(key));
        } else {
            return nonThemeColor;
        }
    }

    private static int getColorByParse(String color) {
        if(isColor(color)) {
            return Color.parseColor(color);
        } else {
            return Color.parseColor("#FFFFFF");
        }
    }

    public static boolean isColor(String color) {

        Pattern pattern = Pattern.compile("#([A-F]|[a-f]|[0-9]){6}");
        Matcher matcher = pattern.matcher(color);

        return matcher.matches();
    }

    public static int getPrimaryColor() {

        return getColorByKey(PRIMARY_COLOR, getBgStyleColor());
    }

    private static int getBgStyleColor() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_stylecolor);
    }

    public static int getSupplementaryColor() {
        return getColorByKey(SUPPLEMENTARY_COLOR, getBgSplashGradient());
    }

    public static int getGlobalStyleColor() {
        return getColorByKey(GLOBAL_STYLE_COLOR, getColorBorderStyle());
    }

    private static int getBgSplashGradient() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_splash_gradient);
    }

    public static int getGlobalBrightFontColor() {
        return getColorByKey(GLOBAL_BRIGHT_FONT_COLOR, getFontSplash());
    }

    private static int getFontSplash() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_splash);
    }

    public static int getGlobalBrightColor() {
        return getColorByKey(GLOBAL_BRIGHT_COLOR, getColorBright());
    }

    private static int getColorBright() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.color_bright);
    }

    public static int getGlobalFullFontColor() {
        return getColorByKey(GLOBAL_FULL_FONT_COLOR, getFontBtnFull());
    }

    public static int getFontBtnFull() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_btnfull);
    }

    public static int getColorBorderStyle() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.color_borderstyle);
    }

    public static int getBtnNaviCardQa() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_navi_cardqa);
    }

    public static int getGlobalBodyBackgroundColor() {
        return getColorByKey(GLOBAL_BODY_BACKGROUND_COLOR, getBgBody());
    }

    public static int getGlobalPageControllerIndicatorSelectedColor (){
        return getColorByKey(GLOBAL_PAGE_CONTROLLER_INDICAOTR_SELECTED_COLOR, getPageControllerIndicatorSelectedColor());
    }

    public static int getGlobalPageControllerIndicatorColor (){
        return getColorByKey(GLOBAL_PAGE_CONTROLLER_INDICAOTR_COLOR, getPageControllerIndicatorColor());
    }

    public static int getGlobalBodyFontColor() {
        return getColorByKey(GLOBAL_BODY_FONT_COLOR, getFontCardProfile());
    }

    public static int getGlobalCategoryTreeBackgroundColor() {
        return getColorByKey(GLOBAL_CATEGORY_TREE_BACKGROUND_COLOR, getBgItemCategory());
    }

    public static int getGlobalCategoryTreeL1FontColor() {
        return getColorByKey(GLOBAL_CATEGORY_TREE_L1_FONT_COLOR, getFontTreeL1Select());
    }

    public static int getGlobalCategoryTreeL2FontColor() {
        return getColorByKey(GLOBAL_CATEGORY_TREE_L2_FONT_COLOR, getFontTreeL2Select());
    }

    public static int getGlobalHeaderBackgroundColor() {
        return getColorByKey(GLOBAL_HEADER_BACKGROUND_COLOR, getBgNavi());
    }

    public static int getGlobalHeaderLinkFontColor() {
        return getColorByKey(GLOBAL_HEADER_LINK_FONT_COLOR, getFontNavi());
    }

    public static int getGlobalNaviIconColor() {
        return getColorByKey(GLOBAL_NAVI_ICON_COLOR, getBgNaviIcon());
    }

    public static int getGlobalPrimaryColor() {
        return getColorByKey(GLOBAL_PRIMARY_COLOR, getBgCommonPullrefresh());
    }

    public static int getGlobalSectionHeaderBackgroundColor() {
        return getColorByKey(GLOBAL_SECTION_HEADER_BACKGROUND_COLOR, getBgShopHomePromotion());
    }

    public static int getGlobalSectionHeaderFontColor() {
        return getColorByKey(GLOBAL_SECTION_HEADER_FONT_COLOR, getColorByParse("#333333"));
    }

    public static int getGlobalSectionHeaderLinkFontColor() {
        return getColorByKey(GLOBAL_SECTION_HEADER_LINK_FONT_COLOR, getFontCommonMorelink());
    }

    public static int getGlobalSideMenuBackgroundColor() {
        return getColorByKey(GLOBAL_SIDE_MENU_BACKGROUND_COLOR, getBgSideMember());
    }

    public static int getGlobalSideMenuFontColor() {
        return getColorByKey(GLOBAL_SIDE_MENU_FONT_COLOR, getIconSidebarAfterLogin());
    }

    public static int getGlobalSideMenuIconColor() {
        return getColorByKey(GLOBAL_SIDE_MENU_ICON_COLOR, getIconSideMember());
    }

    public static int getGlobalTabIndicatorColor() {
        return getColorByKey(GLOBAL_TABINDICATOR_COLOR, getNaviBarline());
    }

    public static int getMasterSettingColor() {
        return getColorByKey(MASTER_SETTING_COLOR, getBtnCouponKeyNumber());
    }

    public static int getPrimaryButtonBackgroundColor() {
        return getColorByKey(PRIMARY_BUTTON_BACKGROUND_COLOR, getIconSidebarAllSelected());
    }

    public static int getPrimaryButtonFontColor() {
        return getColorByKey(PRIMARY_BUTTON_FONT_COLOR, getFontItemAddtocart());
    }

    public static int getPrimaryButtonHoverBackgroundColor() {
        return getColorByKey(PRIMARY_BUTTON_HOVER_BACKGROUND_COLOR, getBtnItemAddtocartPressed());
    }

    public static int getRelatedSalesColor() {
        return getColorByKey(RELATED_SALES_COLOR, getFontColorPrice());
    }

    public static int getSecondaryButtonBackgroundColor() {
        return getColorByKey(SECONDARY_BUTTON_BACKGROUND_COLOR, getBtnItemCheckout());
    }

    public static int getSecondaryButtonFontColor() {
        return getColorByKey(SECONDARY_BUTTON_FONT_COLOR, getFontCartCheckout());
    }

    public static int getPromotionDiscountFontNonSeleted() {
        return getColorByKey(GLOBAL_PRIMARY_ALPHA_FONT_COLOR, getFontNonSelected());
    }

    public static int getPromotionDiscountFontTotalcoupon() {
        return getColorByKey(GLOBAL_EXPLAIN_FONT_COLOR, getFontTotalcoupon());
    }

    public static int getGlobalLightBGFontColor() {
        return getColorByKey(GLOBAL_LIGHT_BG_FONT_COLOR, getFontSubTitleOn11Color());
    }

    public static int getGlobalMemberCardAnnounceColor() {
        return getColorByKey(GLOBAL_MEMBER_CARD_ANNOUNCE_COLOR, getBgForStyleAnnounce());
    }

    public static int getGlobalPagecontrollerIndicatorColor() {
        return getColorByKey(GLOBAL_PAGER_CONTROLLER_INDICATOR_COLOR, getPageControllerSelected());
    }

    public static int getGlobalMemberCardFont() {
        return getColorByKey(GLOBAL_MEMBER_CARD_FONT, getFontForStyleBg());
    }

    public static int getGlobalPriceFontColor() {
        return getColorByKey(GLOBAL_PRICE_COLOR, getFontPriceColor());
    }

    public static int getGlobalStyleColorLight() {
        return getColorByKey(GLOBAL_STYLE_COLOR_LIGHT, getBgStyleColorLight());
    }

    public static int getGlobalFontHighLight() {
        return getColorByKey(GLOBAL_FONT_HIGHLIGHT, getFontHighLight());
    }


    public static int getGlobalMemberLevelCard() {
        return getColorByKey(GLOBAL_MEMBER_LEVEL_CARD, getBgLevel());
    }

    public static int getToBeConfirmedA() {
        return getColorByKey(TO_BE_CONFIRMED_A, getFontNaviBadgeNumber());
    }

    public static int getToBeConfirmedB() {
        return getColorByKey(TO_BE_CONFIRMED_B, getFontNaviSearch());
    }

    public static int getToBeConfirmedC() {
        return getColorByKey(TO_BE_CONFIRMED_C, getBorderNavi());
    }

    public static int getToBeConfirmedD() {
        return getColorByKey(TO_BE_CONFIRMED_D, getBarNaviRefresh());
    }

    public static int getToBeConfirmedE() {
        return getColorByKey(TO_BE_CONFIRMED_E, getBgNaviTriangle());
    }

    public static int getToBeConfirmedF() {
        return getColorByKey(TO_BE_CONFIRMED_F, getBgNaviTriangleBytitle());
    }

    public static int getGlobalNaviStatus() {
        return getColorByKey(GLOBAL_NAVI_STATUS, getBgStatusbar());
    }

    public static int getToBeConfirmedH() {
        return getColorByKey(TO_BE_CONFIRMED_H, getBgShophomeTagFrame());
    }

    public static int getToBeConfirmedI() {
        return getColorByKey(TO_BE_CONFIRMED_I, getBgSellpageTab());
    }

    public static int getToBeConfirmedJ() {
        return getColorByKey(TO_BE_CONFIRMED_J, getBgSellpageTabSelect());
    }

    public static int getToBeConfirmedK() {
        return getColorByKey(TO_BE_CONFIRMED_K, getBgCard());
    }

    public static int getToBeConfirmedL() {
        return getColorByKey(TO_BE_CONFIRMED_L, getBgCardShadow());
    }

    public static int getToBeConfirmedM() {
        return getColorByKey(TO_BE_CONFIRMED_M, getBgBuythengetBadge());
    }

    public static int getToBeConfirmedN() {
        return getColorByKey(TO_BE_CONFIRMED_N, getBtnCartIncreaseSelected());
    }

    public static int getToBeConfirmedO() {
        return getColorByKey(TO_BE_CONFIRMED_O, getBtnCartReduceSelected());
    }

    public static int getToBeConfirmedP() {
        return getColorByKey(TO_BE_CONFIRMED_P, getBgLocationWizard());
    }

    public static int getToBeConfirmedQ() {
        return getColorByKey(TO_BE_CONFIRMED_Q, getBgShopmatch());
    }

    public static int getToBeConfirmedR() {
        return getColorByKey(TO_BE_CONFIRMED_R, getFontShopmatchOk());
    }

    public static int getToBeConfirmedS() {
        return getColorByKey(TO_BE_CONFIRMED_S, getBtnColorFull());
    }

    public static int getToBeConfirmedT() {
        return getColorByKey(TO_BE_CONFIRMED_T, getFontColorFull());
    }

    public static int getToBeConfirmedU() {
        return getColorByKey(TO_BE_CONFIRMED_U, getBtnColorHalf());
    }

    public static int getToBeConfirmedV() {
        return getColorByKey(TO_BE_CONFIRMED_V, getFontEcouponSharepriceScript());
    }

    public static int getToBeConfirmedX() {
        return getColorByKey(TO_BE_CONFIRMED_X, getFontNormalPrice());
    }

    public static int getToBeConfirmedY() {
        return getColorByKey(TO_BE_CONFIRMED_Y, getBgCouponExchangebar());
    }

    public static int getToBeConfirmedZ() {
        return getColorByKey(TO_BE_CONFIRMED_Z, getOutlinePointFinished());
    }

    public static int getAsidebarPromotionTagBackgroundColor() {
        return getColorByKey(ASIDEBAR_PROMOTION_TAG_BACKGROUND_COLOR, getFontShophomePromotion());
    }

    public static int getAsidebarPromotionTagFontColor() {
        return getColorByKey(ASIDEBAR_PROMOTION_TAG_FONT_COLOR, getFontShophomeTagPromotion());
    }

    public static int getShoppingCartCountBackgourndColor() {
        return getColorByKey(SHOPPING_CART_COUNT_BACKGOURND_COLOR, getFontTabItemlist());
    }

    // 固定
    public static int getColorFF2750() {
        return getColorByKey(FIXED_COLOR_FF2750, getBgNaviNumber());
    }

    public static int getColor333333() {
        return getColorByKey(FIXED_COLOR_333333, getFontItemCategory());
    }

    public static int getColorF5F5F5() {
        return getColorByKey(FIXED_COLOR_F5F5F5, getBgCommonHeader());
    }

    public static int getColor999999() {
        return getColorByKey(FIXED_COLOR_999999, getFontCommonDisabled());
    }

    public static int getColorE5E5E5() {
        return getColorByKey(FIXED_COLOR_E5E5E5, getBtnCommonDisabled());
    }

    public static int getColorFCFCFC() {
        return getColorByKey(FIXED_COLOR_FCFCFC, getBtnCommonDelete());
    }

    public static int getColor000000() {
        return getColorByKey(FIXED_COLOR_000000, getFontSideItem());
    }

    public static int getColorFFFFFF() {
        return getColorByKey(FIXED_COLOR_FFFFFF, getFontSideBadge());
    }

    public static int getColor878787() {
        return getColorByKey(FIXED_COLOR_878787, getIconSidebarAll());
    }

    public static int getColorFF9933() {
        return getColorByKey(FIXED_COLOR_FF9933, getIconSidebarPromo());
    }

    public static int getColorB4B4B4() {
        return getColorByKey(FIXED_COLOR_B4B4B4, getBtnItemNormal());
    }

    public static int getColor909091() {
        return getColorByKey(FIXED_COLOR_909091, getFontSideHeader());
    }




    // TODO ===== Old color getter under line =====


    private static int getFontNavi() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_navi);
    }

    private static int getFontNaviBadgeNumber() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_navi_badge_number);
    }

    private static int getFontNaviSearch() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_navi_search);
    }

    private static int getBgNavi() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_navi);
    }

    private static int getBgNaviNumber() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_navi_number);
    }

    private static int getBgNaviNumberBorder() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_navi_number_border);
    }

    private static int getBorderNavi() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.border_navi);
    }

    private static int getBarNaviRefresh() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bar_navi_refresh);
    }

    private static int getBgNaviTriangle() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_navi_triangle);
    }

    private static int getBgNaviTriangleBytitle() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_navi_triangle_bytitle);
    }

    private static int getNaviBarline() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_navi_barline);
    }

    private static int getBgNaviIcon() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_navi_icon);
    }

    private static int getBgStatusbar() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_statusbar);
    }

    private static int getBgItemCategory() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_item_category);
    }

    private static int getFontItemCategory() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_item_category);
    }

    private static int getBgCommonPullrefresh() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_common_pullrefresh);
    }

    //TODO UI說key要等App3.0等定稿後才會定
    private static int getPageControllerSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.color_pagecontroller_selected);
    }

    public static int getFontTabSegSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_tab_seg_selected);
    }

    //TODO UI說key要等App3.0等定稿後才會定
    public static int getPageController() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.color_pagecontroller);
    }

    //TODO UI說key要等App3.0等定稿後才會定
    public static int getFontSideMember() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_member);
    }

    private static int getLabelTabSeg() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.label_tab_seg);
    }

    private static int getLabelTabSegSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.label_tab_seg_selected);
    }

    private static int getFontTabItemlist() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_tab_itemlist);
    }

    private static int getFontCommonClickable() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_common_clickable);
    }

    private static int getBgCommonHeader() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_common_header);
    }

    private static int getFontCommonDesc() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_common_desc);
    }

    private static int getFontColorPrice() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_color_price);
    }

    private static int getBtnColorFull() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_color_full);
    }

    private static int getFontColorFull() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_color_full);

    }

    private static int getBtnColorHalf() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_color_half);
    }

    private static int getBtnColorHalfPressed() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_color_half_pressed);
    }

    private static int getFontCommonDisabled() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_common_disabled);
    }

    private static int getBtnCommonDisabled() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_common_disabled);
    }

    public static int getBtnItemAddtocart() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_addtocart);
    }

    private static int getBtnItemAddtocartPressed() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_addtocart_pressed);
    }

    private static int getFontItemAddtocart() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_item_addtocart);
    }

    private static int getBtnItemCheckout() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_checkout);
    }

    private static int getBtnCommonDelete() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_common_delete);
    }

    private static int getFontCommonDelete() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_common_delete);
    }

    private static int getBtnCouponKeyNumber() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_coupon_keynumber);
    }


    private static int getFontCartCheckout() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_cart_checkout);
    }

    private static int getBgSellpageSelltime() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_sellpage_selltime);
    }

    private static int getFontSellpageSelltime() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_sellpage_selltime);
    }

    private static int getFontSellpageCalendar() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_sellpage_Calendar);
    }

    private static int getFontEcouponNormalcatch() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_ecoupon_normalcatch);
    }

    private static int getFontEcouponSharepriceScript() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_ecoupon_shareprice_script);
    }

    private static int getFontNormalPrice() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_normal_price);
    }

    private static int getBgEcouponGround() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_ecoupon_ground);
    }

    private static int getBgCouponExchangebar() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_coupon_exchangebar);
    }

    private static int getOutlinePointFinished() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.outline_point_finished);
    }

    private static int getFontSideMenu() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_menu);
    }

    private static int getFontSideMenuSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_menu_selected);
    }

    private static int getFontSideNumber() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_number);
    }

    private static int getFontSideNumberSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_number_selected);
    }

    private static int getFontSideHeader() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_header);
    }

    private static int getBgSideMenu() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_side_menu);
    }

    private static int getBgSideMenuSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_side_menu_selected);
    }

    private static int getBgSideNumber() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_side_number);
    }

    private static int getBgSideNumberSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_side_number_selected);
    }

    private static int getBgSideHeader() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_side_header);
    }

    private static int getBgSideMember() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_side_member);
    }

    private static int getFontSideItem() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_item);
    }

    private static int getBgSideBadge() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_side_badge);
    }

    private static int getFontSideBadge() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_side_badge);
    }

    private static int getBgShopHomePromotion() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_shophome_promotion);
    }

    private static int getFontShophomePromotion() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_shophome_promotion);
    }

    private static int getFontSellpagePromotion() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_sellpage_promotion);
    }

    private static int getBgShophomeTagPromotion() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_shophome_tag_promotion);
    }

    private static int getBgSellpageTagPromotion() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_sellpage_tag_promotion);
    }

    private static int getBgShophomeTagFrame() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_shophome_tag_frame);
    }

    private static int getFontShophomeTagPromotion() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_shophome_tag_promotion);
    }

    private static int getFontSellpageTagPromotion() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_sellpage_tag_promotion);
    }

    private static int getFontCommonMorelink() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_common_morelink);
    }

    private static int getBgSellpageTab() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_sellpage_tab);
    }

    private static int getBgSellpageTabSelect() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_sellpage_tab_select);
    }

    private static int getFontTreeL1Select() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_tree_L1_select);
    }

    private static int getFontTreeL2Select() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_tree_L2_select);
    }

    private static int getFontCardProfile() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_card_profile);
    }

    private static int getFontCardType() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_card_type);
    }

    private static int getFontCardDate() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_card_date);
    }

    private static int getBgCard() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_card);
    }

    private static int getBgCardShadow() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_card_shadow);
    }

    private static int getIconSidebarAll() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.icon_sidebar_all);
    }

    private static int getIconSidebarAllSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.icon_sidebar_all_selected);
    }

    private static int getIconSidebarAfterLogin() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.icon_sidebar_afterlogin);
    }

    private static int getIconSidebarPromo() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.icon_sidebar_promo);
    }

    private static int getBtnItemNormal() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_item_normal);
    }

    private static int getBgLocationWizard() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_location_wizard);
    }

    private static int getBgShopmatch() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_shopmatch);
    }

    private static int getIconSideMember() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.icon_side_member);
    }

    private static int getFontShopmatchOk() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_shopmatch_ok);
    }

    private static int getBgBuythengetBadge() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_buythenget_badge);
    }

    private static int getIconCartCheckbox() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.icon_cart_checkbox);
    }

    private static int getBtnCartIncreaseSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_cart_increase_selected);
    }

    private static int getBtnCartReduceSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.btn_cart_reduce_selected);
    }

    private static int getFontNonSelected() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_nonselected);
    }

    private static int getFontTotalcoupon() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_totalcoupon);
    }

    private static int getFontSubTitleOn11Color() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_SubTitle_on11Color);
    }

    private static int getBgLevel() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_level);
    }

    private static int getBgForStyleAnnounce() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_forstyleannounce);
    }

    private static int getFontForStyleBg() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_forstylebg);
    }

    private static int getFontPriceColor() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_price);
    }

    private static int getBgStyleColorLight() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_stylecolor_light);
    }

    private static int getFontHighLight() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.font_highlight);
    }

    private static int getBgBody() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.bg_body);
    }

    private static int getPageControllerIndicatorSelectedColor() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.color_itemlist_pagecontroller_selected);
    }

    private static int getPageControllerIndicatorColor() {
        return ApplicationProvider.getInstance().getProviderAppResources().getColor(R.color.color_itemlist_pagecontroller);
    }
}
