package com.nineyi.module.base.navigator.argument;

import com.nineyi.autoargument.AutoArgument;


/**
 * For ProductPageActivity
 *
 * Created by ReeceCheng on 2017/9/18.
 */

@AutoArgument
public class ProductPageArgument {
    int SalePageId;
    String SearchText;
    boolean IsShoppingCart;
}
