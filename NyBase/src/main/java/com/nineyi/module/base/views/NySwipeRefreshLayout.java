package com.nineyi.module.base.views;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;


/**
 * Created by toby on 2015/3/30.
 */
public class NySwipeRefreshLayout extends SwipeRefreshLayout{

    private View mScrollableChild;

    public NySwipeRefreshLayout(Context context) {
        super(context);
    }

    public NySwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean canChildScrollUp() {
        if (mScrollableChild == null) {
            return false;
        }

        if (android.os.Build.VERSION.SDK_INT < 14) {
            if (mScrollableChild instanceof AbsListView) {
                final AbsListView absListView = (AbsListView) mScrollableChild;
                return absListView.getChildCount() > 0 && (absListView.getFirstVisiblePosition() > 0
                        || absListView.getChildAt(0).getTop() < absListView.getPaddingTop());
            } else {
                return mScrollableChild.getScrollY() > 0;
            }
        } else {
            return ViewCompat.canScrollVertically(mScrollableChild, -1);
//            return true;
        }
    }

    public void setScrollableChild(View scrollableChild) {
        mScrollableChild = scrollableChild;
    }

    public View getScrollableChild() {
        return mScrollableChild;
    }
}
