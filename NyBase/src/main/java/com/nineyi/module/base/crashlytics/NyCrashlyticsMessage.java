package com.nineyi.module.base.crashlytics;

import com.crashlytics.android.Crashlytics;
import com.nineyi.module.base.retrofit.NyJsonSyntaxException;


/**
 * Created by daisishu on 15/6/2.
 */
public class NyCrashlyticsMessage {
    public static void handleMessage(String url, String packagename){
        Crashlytics.log("WebView Log , Packagename:" + packagename + ",Url:"+url);
    }

    public static void logNyNaviInfoClassifierParseError(Exception e) {
        Crashlytics.logException(new Exception(e));
    }

    public static void logBannerViewFlipperTouchEventErrException(Exception e) {
        Crashlytics.logException(new Exception("BannerViewFlipper_onTouchEvent: " + e));
    }

    public static void logBannerViewFlipperInterceptTouchEventErrException(Exception e) {
        Crashlytics.logException(new Exception("BannerViewFlipper_onInterceptTouchEvent: " + e));
    }

    public static void logNineyiGalleryViewPagerTouchEventErrException(Exception e) {
        Crashlytics.logException(new Exception("NineyiGalleryViewPager_onTouchEvent: " + e));
    }

    public static void logNineyiGalleryViewPagerInterceptTouchEventErrException(Exception e) {
        Crashlytics.logException(new Exception("NineyiGalleryViewPager_onInterceptTouchEvent: " + e));
    }

    public static void logRetrofitApiFailed(NyJsonSyntaxException exception){
        Crashlytics.logException(new Exception("Cause: " + exception.getCause().toString()
                + " Request: " + exception.getUrl()
                + " Response: " + exception.getBody()));
    }

    public static void logLoadImageSyncFailed(String url, Exception e) {
        Crashlytics.logException(new Exception("LoadImageSyncException , Url:" + url + ",Exception:" + e));
    }

    public static void logPicassoLoadImageSuccess() {
        Crashlytics.logException(new Exception("PicassoLoadImageSuccess"));
    }

    public static void logPicassoLoadImageFailed() {
        Crashlytics.logException(new Exception("PicassoLoadImageFailed"));
    }

    public static void logTLSFailed(Throwable e){
        Crashlytics.logException(new Exception("TLS Failed , Exception:"+ e));
    }

    public static void logDeviceId() {
        Crashlytics.logException(new Exception("Device Id call"));
    }
}
