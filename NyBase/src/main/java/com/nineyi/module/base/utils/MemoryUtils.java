package com.nineyi.module.base.utils;

/**
 * Created by tedliang on 2016/3/11.
 */
public class MemoryUtils {
    private static final double LOW_MEMORY_THRESHOLD = 0.9;
    public static boolean isLowMemory() {
        long memoryLeft = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long maxMemory = Runtime.getRuntime().maxMemory();
        double ratio = (double) memoryLeft / (double) maxMemory;

        return ratio > LOW_MEMORY_THRESHOLD;
    }
}
