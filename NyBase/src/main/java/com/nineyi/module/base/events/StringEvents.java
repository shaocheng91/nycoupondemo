package com.nineyi.module.base.events;

public final class StringEvents {

    private StringEvents() {} // No public constructor

    // TODO 事件的命名應該是描述當時發生的「事件」，而不是當事件發生時，要執行的「做法」

    // 使用者登入／登出相關
    public static final String EVENT_ON_USER_LOGGED_IN = "onUserLoggedIn";
    public static final String EVENT_ON_RECEIVE_GCM_MESSAGE = "onReceiveGcmMessage";

    // 收藏商品／商店
    public static final String EVENT_ON_TRACE_ITEM_ADD_OR_REMOVE_SUCCESS = "onTraceItemAddOrRemoveSuccess";
    public static final String EVENT_ON_TRACE_SHOP_ADD_OR_REMOVE_SUCCESS = "onTraceShopAddOrRemoveSuccess";
    public static final String EVENT_ON_SLIDING_MENU_OPENED = "onSlidingMenuOpened";

    // 目前用於商店側邊欄
    public static final String EVENT_ON_ONLINE_CRM_CODE_AVAILABLE = "onOnlineCRMCodeAvailable";

}