package com.nineyi.module.base.appcompat.scroll;

import com.nineyi.module.base.provider.application.ApplicationProvider;
import com.nineyi.module.base.ui.UiUtils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Created by ReeceCheng on 2017/1/6.
 */
public class ShopHomeScrollingDecoration extends RecyclerView.ItemDecoration {
    private int mSpace = 0;
    private int mViewCountForFirstRow;

    public ShopHomeScrollingDecoration(int headerSpace, int viewCountForFirstRow) {
        super();
        mSpace = headerSpace;
        mViewCountForFirstRow = viewCountForFirstRow;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(parent.getChildAdapterPosition(view) < mViewCountForFirstRow) {
            outRect.top = mSpace;
        }
    }

    public void setSpaceByDp(int space) {
        this.mSpace = UiUtils.convertDipToPixel(space, ApplicationProvider.getInstance().getProviderAppResources().getDisplayMetrics());
    }

    public void setSpace(int space) {
        this.mSpace = space;
    }
}
