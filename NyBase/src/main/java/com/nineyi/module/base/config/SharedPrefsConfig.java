package com.nineyi.module.base.config;

public final class SharedPrefsConfig {

    private SharedPrefsConfig() {
        // No public constructor
    }

    //public static final String SHARED_PREFS_FILENAME = "com.nineyi.config.sp";

    public static final String NINE_YI_SHARED_PREFS = "com.nineyi.shared.preference";

    public static final String PREF_KEY_LAST_UPGRADED_APP_VERSION = "lastUpgradedAppVersion";

    //public static final String SHARED_PREFS_USER_ID = "userId"; // sample

    public static final String PREF_KEY_COOKIE_U_AUTH = "com.nineyi.cookie.uauth";
    public static final String PREF_KEY_COOKIE_AUTH = "com.nineyi.cookie.auth";

    //public static final String PREF_KEY_LAST_USE_TIME = "com.nineyi.last.use.time"; for 30天後自動登出功能

    public static final String PREF_KEY_USER_NAME = "com.nineyi.user.name";
    public static final String PREF_KEY_USER_PICTURE = "com.nineyi.user.pic";

    public static final String PREF_KEY_APP_GUID = "com.nineyi.app.guid";

    public static final String MEMBER_CARD_ID = "com.login.member.member.cardid";

    public static final String MEMBER_ID_GA = "com.login.member.member.id";

    public static final String MEMBER_CODE_GA = "com.login.member.member.code";

    public static final String PREF_KEY_MEMBERCARD_FIRST_TIME = "memberCardFirstTime";

    // TODO 找時間要拿掉此設定
    public static final String PREF_KEY_OPENSETTING = "com.nineyi.open.setting";

    public static final String PREF_KEY_CALENDARED_SALEPAGE_IDS = "calendaredSalePageIds";

    public static final String PREF_KEY_REFEREE_NAME = "refereeName";

    public static final String PREF_KEY_NEW_REFEREE_STORE = "newrefereeStore";
    public static final String PREF_KEY_NEW_REFEREE_NAME = "newrefereeName";
    public static final String PREF_KEY_NEW_REFEREE_LOGIN = "newrefereelogin";

    public static final String PREF_KEY_UDID = "udidhelper.udidsharepreference";
    public static final String PREF_KEY_UDID_PARAM = "udidhelper.udid.param";

    public static final String PREF_KEY_FAVORITE = "FavoritePreference";

    public static final String PREF_KEY_SHOPINFO = "com.nineyi.ShopInfo";
    public static final String PREF_KEY_NOTIFY_MESSAGE = "com.nineyi.notify.message";

    public static final String PREF_KEY_FACEBOOK_PERSONAL_DATA = "FacebookPersonalData";

    public static final String PREF_KEY_GCM_REGISTER = "NineyiGCMRegister";

    public static final String PREF_KEY_LBS_DATE = "LbsDate";

    public static final String PREF_KEY_CBC = "CBCShare";

    public static final String PREF_KEY_VERSION_CONTROL = "com.nineyi.smartwifi.prefs";

    public static final String PREF_KEY_APP_RATER = "com.nineyi.utils.appRater";

    public static final String PREF_KEY_APP_MIGRATION = "com.nineyi.app.migration";

    public static final String SEARCHVIEW_HISTORY = "com.nineyi.searchview.searchResult";

    public static final String PREF_KEY_MEMBER_ZONE_DATA = "com.nineyi.memberzone.v2.memberzonedatasaver.memberzonedata";

    public static final String PREF_KEY_SHOP_CONTRACT_SETTING_UTILS = "com.nineyi.shopapp.ShopContractSettingUtils";

    public static final String PREF_KEY_DEVICE_ID = "com.nineyi.shopapp.pref.key.device.id";

    public static final String PREF_KEY_PRODUCT_MEMBER_DATA = "com.nineyi.product.member.data";
}
