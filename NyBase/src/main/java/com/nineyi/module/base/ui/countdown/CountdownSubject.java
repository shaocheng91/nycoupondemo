package com.nineyi.module.base.ui.countdown;

import java.lang.ref.WeakReference;


/**
 * Created by ReeceCheng on 2017/10/12.
 */

public interface CountdownSubject {
    void register(WeakReference<CountdownObserver> o);
    void unregister(WeakReference<CountdownObserver> o);
}