package com.nineyi.module.base.navigator.argument;

import com.nineyidata.BuildConfig;

import android.content.Context;

import con.nineyi.module.base.R;


/**
 * Created by tedliang on 2017/9/21.
 */

public enum EcommercePageFromWhere {
    NONE(0),
    BuyExtra(R.string.ec_buyextra_zone),
    Category(R.string.ec_category),
    History(R.string.ec_history),
    HomeRecommendSalePage(R.string.ec_home_recommand_salepage),
    HomeHotSalePage(R.string.ec_home_hot_salepage),
    ProductHotSale(R.string.ec_productpage_hotsale);


    private int mWording;
    EcommercePageFromWhere(int wording){
        mWording = wording;
    }


    public String format(Context context, String... strings) {
        if(BuildConfig.DEBUG && this.equals(Category) && strings.length == 0 ){
            throw new RuntimeException("should have strings");
        }
        return String.format(context.getString(mWording), strings);
    }

}
