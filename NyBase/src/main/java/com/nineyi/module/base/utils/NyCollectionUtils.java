package com.nineyi.module.base.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class NyCollectionUtils {

    private NyCollectionUtils(){}
    
    public interface Predicate<T> {
        boolean evaluate(T element);
    }

    public interface Transformer<I, O> {
        O transform(I element);
    }

    public static <T> void filter(Collection<T> collection, Predicate<T> predicate) {
        if(collection != null && predicate != null) {
            Iterator<T> it = collection.iterator();

            while(it.hasNext()) {
                if(!predicate.evaluate(it.next())) {
                    it.remove();
                }
            }
        }
    }

    public static <T> T find(Collection<T> collection, Predicate<T> predicate) {
        if(collection != null && predicate != null) {
            Iterator<T> iter = collection.iterator();

            while(iter.hasNext()) {
                T item = iter.next();
                if(predicate.evaluate(item)) {
                    return item;
                }
            }
        }

        return null;
    }

    public static <T> int countMatches(Collection<T> inputCollection, Predicate<T> predicate) {
        int count = 0;
        if(inputCollection != null && predicate != null) {
            Iterator<T> it = inputCollection.iterator();

            while(it.hasNext()) {
                if(predicate.evaluate(it.next())) {
                    ++count;
                }
            }
        }

        return count;
    }


    public static <I, O> void collect(Collection<I> inputCollection, Transformer<I, O> transformer, Collection<O> outputCollection) {
        if (inputCollection == null || outputCollection == null || transformer == null) {
            return;
        }

        Iterator<I> inputIterator = inputCollection.iterator();
        while(inputIterator.hasNext()) {
            I item = inputIterator.next();
            O value = transformer.transform(item);
            outputCollection.add(value);
        }
    }

    public static <T> ArrayList<T> select(Collection<T> inputCollection, Predicate<T> predicate) {
        if (inputCollection == null || predicate == null) {
            return new ArrayList<T>();
        } else {
            ArrayList<T> outputCollection = new ArrayList<>(inputCollection.size());
            Iterator<T> iter = inputCollection.iterator();

            while(iter.hasNext()) {
                T item = iter.next();
                if(predicate.evaluate(item)) {
                    outputCollection.add(item);
                }
            }

            return outputCollection;
        }
    }
}
