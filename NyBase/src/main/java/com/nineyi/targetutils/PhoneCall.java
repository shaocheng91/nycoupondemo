package com.nineyi.targetutils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;


/**
 * Created by Willy on 17/02/2017.
 */

public class PhoneCall {

    private CharSequence mPhoneNumber;

    public PhoneCall(CharSequence phoneNumber) {
        this.mPhoneNumber = phoneNumber;
    }

    public void doNavigation(Context activity) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mPhoneNumber));
        activity.startActivity(intent);
    }

}
