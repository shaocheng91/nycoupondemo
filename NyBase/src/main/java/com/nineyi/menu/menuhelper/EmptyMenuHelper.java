package com.nineyi.menu.menuhelper;

import com.nineyi.module.base.menu.IMenuHelper;


/**
 * Created by tedliang on 15/4/28.
 */
public class EmptyMenuHelper implements IMenuHelper {

    @Override
    public void refreshCount() {

    }

    @Override
    public void intMenu(boolean isShoppingcartVisible, boolean isSearchVisible) {

    }
}
